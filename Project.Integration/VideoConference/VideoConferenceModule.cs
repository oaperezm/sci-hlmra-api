﻿using Autofac;
using Project.Application.Management;
using Project.Application.Core;
using Project.Infraestructure.Repositories.VideoConference;
using Project.Repositories.Courses;
using Project.Domain.Management;





namespace Project.Integration.VideoConference
{
    public class VideoConferenceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VideoConferenceApplication>().As<IVideoConferenceApplication>().InstancePerRequest();
            builder.RegisterType< Domain.Management.VideoConference >().As<Domain.Core.IVideoconference>().InstancePerRequest();
            builder.RegisterType<VideoConferenceRepository>().As<IVideoConferenceRepository>().InstancePerRequest();
        }
    }
}
