﻿using Autofac;
using Project.Application.Core;
using Project.Application.Home;
using Project.Domain.Core;
using Project.Domain.Home;
using Project.Infraestructure.Repositories.Home;
using Project.Infraestructure.Services;
using Project.Repositories.Home;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Home
{
    public class TipModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TipApplication>().As<ITipApplication>().InstancePerRequest();
            builder.RegisterType<Tip>().As<ITip>().InstancePerRequest();
            builder.RegisterType<TipRepository>().As<ITipRepository>().InstancePerRequest();            
        }
    }

}
