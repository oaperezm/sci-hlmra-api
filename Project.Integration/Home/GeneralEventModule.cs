﻿using Autofac;
using Project.Application.Core;
using Project.Application.Home;
using Project.Domain.Core;
using Project.Domain.Home;
using Project.Infraestructure.Repositories.Home;
using Project.Repositories.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Home
{
    public class GeneralEventModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GeneralEventApplication>().As<IGeneralEventApplication>().InstancePerRequest();
            builder.RegisterType<GeneralEvent>().As<IGeneralEvent>().InstancePerRequest();
            builder.RegisterType<GeneralEventRepository>().As<IGeneralEventRepository>().InstancePerRequest();
        }
    }
}
