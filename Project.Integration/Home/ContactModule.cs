﻿using Autofac;
using Project.Application.Core;
using Project.Application.Home;
using Project.Domain.Core;
using Project.Domain.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Home
{
    public class ContactModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContactApplication>().As<IContactApplication>().InstancePerRequest();
            builder.RegisterType<Contact>().As<IContact>().InstancePerRequest();
           
        }
    }

  
}
