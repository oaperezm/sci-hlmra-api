IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[V_Courses]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[V_CoordinatorReport]
AS
select  CCT.CCT,
 u.Email
,u.FullName
,u.Institution,
 cct.RolId
,n.Description
,c.Name as curso
,sp.Description as grupo
,sp.id as idgrupo
,qb.Id
,count(s.id) as students  
,count(a.id) as activities
,count(es.id) as exams
,count(q.id) as questionsbanks
,qt.total as questionTeacher
,qe.total as totalquestions

,n.id
,g.Description as grade
,signature.Description as [signature]
,field.Description as [field]
from dbo.Users u 
inner join [dbo].[UserCCT] cct on  u.Id=cct.UserId
inner  join dbo.Roles r  on  r.Id=u.RoleId
left join dbo.Courses c on c.UserId=u.Id
left join [dbo].[Nodes] n  on c.NodeId=n.Id 
left join dbo.StudentGroups sp on sp.CourseId= c.Id
left join dbo.Students s on sp.Id= s.StudentGroupId and s.StudentStatusId=2
left join  [dbo].[Activities] a on a.StudentGroupId= sp.Id
left join [dbo].[ExamSchedules] es on ExamScheduleTypeId=1 and es.StudentGroupId=sp.Id and es.UserId= u.Id
left join [dbo].[TeacherExams] te on te.Id= es.TeacherExamId
left join dbo.TeacherExamQuestions teq on teq.TeacherExamId=te.Id
left join dbo.Questions q on  teq.QuestionId=q.Id
left join [dbo].[QuestionBanks] qb on  q.QuestionBankId=qb.Id

outer apply(
select distinct ff.Description 
from dbo.NodeContents nc
inner join dbo.Contents c on nc.ContentId= c.Id
inner join dbo.FormativeFields ff on ff.Id= c.FormativeFieldId
 where nc.NodeId=n.Id
)field


cross apply(
select count(id) as total from [dbo].[QuestionBanks] 
 where UserId=u.Id) qt

 cross apply(
select count(q2.id) as total from 
[dbo].[ExamSchedules] est
left join [dbo].[TeacherExams] te on te.Id= est.TeacherExamId
left join dbo.TeacherExamQuestions teq2 on teq2.TeacherExamId=te.Id
left join dbo.Questions q2 on  teq2.QuestionId=q2.Id
 where   est.StudentGroupId=sp.Id ) qe

 cross apply(
 
 select Description from dbo.Nodes where ParentId=n.Id
 ) g
  cross apply(
 
 select Description from dbo.Nodes where ParentId=148
 ) signature





where  u.systemId=2  
group by CCT.CCT,
 u.Email
,u.FullName
,u.Institution,
 cct.RolId
--,pp.Description
,n.Description
,c.Name 
,sp.Description 
,sp.id 
,qb.id
,qt.total
,qe.total
,n.Description
,n.Id
,g.Description
,signature.Description
,field.Description'