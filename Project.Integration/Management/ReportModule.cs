﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Entities;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class ReportModule :Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<ReportApplication>().As<IReportApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<Report>().As<IReport>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<ReportRepository>().As<IReportRepository>().InstancePerRequest();
        }
    }
}
