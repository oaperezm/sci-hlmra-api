﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class LearningexpectedModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<LearningexpectedApplication>().As<ILearningexpectedApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<Learningexpected>().As<ILearningexpected>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<LearningexpectedRepository>().As<ILearningexpectedRepository>().InstancePerRequest();
        }
    }
}
