﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class GradeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GradeApplication>().As<IGradeApplication>().InstancePerRequest();
            builder.RegisterType<Grade>().As<IGrade>().InstancePerRequest();
            builder.RegisterType<GradeRepository>().As<IGradeRepository>().InstancePerRequest();
        }
    }
}