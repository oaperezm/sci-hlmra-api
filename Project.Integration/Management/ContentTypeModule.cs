﻿using Autofac;
using Project.Application.Core;
using Project.Application.Management;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class ContentTypeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContentTypeApplication>().As<IContentTypeApplication>().InstancePerRequest();
            builder.RegisterType<ContentType>().As<IContentType>().InstancePerRequest();
            builder.RegisterType<ContentTypeRepository>().As<IContentTypeRepository>().InstancePerRequest();
            
            builder.RegisterType<ContentResourceTypeApplication>().As<IContentResourceTypeApplication>().InstancePerRequest();
            builder.RegisterType<ContentResourceType>().As<IContentResourceType>().InstancePerRequest();
            builder.RegisterType<ContentResourceTypeRepository>().As<IContentResourceTypeRepository>().InstancePerRequest();
        }
    }
}