﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class LevelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LevelApplication>().As<ILevelApplication>().InstancePerRequest();
            builder.RegisterType<Level>().As<ILevel>().InstancePerRequest();
            builder.RegisterType<LevelRepository>().As<ILevelRepository>().InstancePerRequest();
        }
    }
}