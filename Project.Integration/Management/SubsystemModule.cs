﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class SubsystemModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SubsystemApplication>().As<ISubsystemApplication>().InstancePerRequest();
            builder.RegisterType<Subsystem>().As<ISubsystem>().InstancePerRequest();
            builder.RegisterType<SubsystemRepository>().As<ISubsystemRepository>().InstancePerRequest();
        }
    }
}