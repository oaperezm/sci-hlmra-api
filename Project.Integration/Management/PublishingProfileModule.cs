﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class PublishingProfileModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PublishingProfileApplication>().As<IPublishingProfileApplication>().InstancePerRequest();
            builder.RegisterType<PublishingProfile>().As<IPublishingProfile>().InstancePerRequest();
            builder.RegisterType<PublishingProfileRepository>().As<IPublishingProfileRepository>().InstancePerRequest();
        }
    }
}