﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class NodeContentModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NodeContentApplication>().As<INodeContentApplication>().InstancePerRequest();
            builder.RegisterType<NodeContent>().As<INodeContent>().InstancePerRequest();
            builder.RegisterType<NodeContentRepository>().As<INodeContentRepository>().InstancePerRequest();
        }
    }
}