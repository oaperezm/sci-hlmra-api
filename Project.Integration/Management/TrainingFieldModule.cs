﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class TrainingFieldModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TrainingFieldApplication>().As<ITrainingFieldApplication>().InstancePerRequest();
            builder.RegisterType<TrainingField>().As<ITrainingField>().InstancePerRequest();
            builder.RegisterType<TrainingFieldRepository>().As<ITrainingFieldRepository>().InstancePerRequest();
        }
    }
}