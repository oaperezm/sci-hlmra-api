﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class RoleModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RoleApplication>().As<IRoleApplication>().InstancePerRequest();
            builder.RegisterType<Role>().As<IRole>().InstancePerRequest();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerRequest();
        }
    }
}