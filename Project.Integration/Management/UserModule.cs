﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Infraestructure.Services;
using Project.Repositories.Management;
using Project.Repositories.Services;

namespace Project.Integration.Management
{
    public class UserModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {            
            builder.RegisterType<UserApplication>().As<IUserApplication>().InstancePerRequest();
            builder.RegisterType<User>().As<IUser>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();            
            builder.RegisterType<BookRepository>().As<IBookRepository>().InstancePerRequest();

            builder.RegisterType<UserDeviceRepository>().As<IUserDeviceRepository>().InstancePerRequest();
            builder.RegisterType<UserCCTRepository>().As<IUserCCTRepository>().InstancePerRequest();
            builder.RegisterType<UserAccessRepository>().As<IUserAccessRepository>().InstancePerRequest();

            


        }
    }
}
