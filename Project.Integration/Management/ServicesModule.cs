﻿using Autofac;
using Project.Infraestructure.Repositories.Management;
using Project.Infraestructure.Services;
using Project.Repositories.Management;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class ServicesModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {   
            builder.RegisterType<MailerService>().As<IMailer>().InstancePerRequest();
            builder.RegisterType<AESEncryption>().As<IAESEncryption>().InstancePerRequest();
            builder.RegisterType<RequestService>().As<IRequestService>().InstancePerRequest();
            builder.RegisterType<PushNotificationService>().As<IPushNotificationService>().InstancePerRequest();

            builder.RegisterType<NotificationMessagesRepository>().As<INotificationMessagesRepository>().InstancePerRequest();
            builder.RegisterType<UserNotificationRepository>().As<IUserNotificationRepository>().InstancePerRequest();


        }
    }
}
