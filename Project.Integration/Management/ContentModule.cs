﻿using Autofac;
using Project.Application.Core;
using Project.Application.Management;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Infraestructure.Services;
using Project.Repositories.Management;
using Project.Repositories.Services;

namespace Project.Integration.Management
{
    public class ContentModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContentApplication>().As<IContentApplication>().InstancePerRequest();
            builder.RegisterType<Content>().As<IContent>().InstancePerRequest();

            builder.RegisterType<TagApplication>().As<ITagApplication>().InstancePerRequest();          
            builder.RegisterType<Tag>().As<ITag>().InstancePerRequest();

            builder.RegisterType<ContentRepository>().As<IContentRepository>().InstancePerRequest();
            builder.RegisterType<NodeContentRepository>().As<INodeContentRepository>().InstancePerRequest();
            builder.RegisterType<TagRepository>().As<ITagRepository>().InstancePerRequest();

            builder.RegisterType<FileUploaderService>().As<IFileUploader>().InstancePerRequest();
           
        }
    }
}