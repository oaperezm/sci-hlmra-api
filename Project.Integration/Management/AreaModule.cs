﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class AreaModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AreaApplication>().As<IAreaApplication>().InstancePerRequest();
            builder.RegisterType<Area>().As<IArea>().InstancePerRequest();
            builder.RegisterType<AreaRepository>().As<IAreaRepository>().InstancePerRequest();
        }
    }
}