﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class FormativeFieldModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FormativeFieldApplication>().As<IFormativeFieldApplication>().InstancePerRequest();
            builder.RegisterType<FormativeField>().As<IFormativeField>().InstancePerRequest();
            builder.RegisterType<FormativeFieldRepository>().As<IFormativeFieldRepository>().InstancePerRequest();
        }
    }
}