﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class NodeTypeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NodeTypeApplication>().As<INodeTypeApplication>().InstancePerRequest();
            builder.RegisterType<NodeType>().As<INodeType>().InstancePerRequest();
            builder.RegisterType<NodeTypeRepository>().As<INodeTypeRepository>().InstancePerRequest();
        }
    }
}