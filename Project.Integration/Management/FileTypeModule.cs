﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class FileTypeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileTypeApplication>().As<IFileTypeApplication>().InstancePerRequest();
            builder.RegisterType<FileType>().As<IFileType>().InstancePerRequest();
            builder.RegisterType<FileTypeRepository>().As<IFileTypeRepository>().InstancePerRequest();
        }
    }
}