﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class PublishingProfileUserModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PublishingProfileUserApplication>().As<IPublishingProfileUserApplication>().InstancePerRequest();
            builder.RegisterType<PublishingProfileUser>().As<IPublishingProfileUser>().InstancePerRequest();
            builder.RegisterType<PublishingProfileUserRepository>().As<IPublishingProfileUserRepository>().InstancePerRequest();
        }
    }
}