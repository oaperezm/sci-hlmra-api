﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Infraestructure.Repositories.Views;
using Project.Repositories.Management;
using Project.Repositories.Views;

namespace Project.Integration.Management
{
    public class NodeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NodeApplication>().As<INodeApplication>().InstancePerRequest();
            builder.RegisterType<Node>().As<INode>().InstancePerRequest();
            builder.RegisterType<NodeRepository>().As<INodeRepository>().InstancePerRequest();
            builder.RegisterType<VNodesTotalFileTypesRepository>().As<IVNodesTotalFileTypesRepository>().InstancePerRequest();
            builder.RegisterType<VNodesTotalFileTypesRARepository>().As<IVNodesTotalFileTypesRARepository>().InstancePerRequest();
        }
    }
}