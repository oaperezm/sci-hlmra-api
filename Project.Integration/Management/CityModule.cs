﻿using Autofac;
using Project.Application.Core;
using Project.Application.Management;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class CityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CityApplication>().As<ICityApplication>().InstancePerRequest();
            builder.RegisterType<City>().As<ICity>().InstancePerRequest();
            builder.RegisterType<CityRepository>().As<ICityRepository>().InstancePerRequest();
            builder.RegisterType<StateRepository>().As<IStateRepository>().InstancePerRequest();
        }
    }
}
