﻿using Autofac;
using Project.Infraestructure.Data;
using Project.Repositories.Core;

namespace Project.Integration.Management
{
    public class DataModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();            
        }
    }
}
