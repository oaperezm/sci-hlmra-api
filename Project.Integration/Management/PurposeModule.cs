﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class PurposeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PurposeApplication>().As<IPurposeApplication>().InstancePerRequest();
            builder.RegisterType<Purpose>().As<IPurpose>().InstancePerRequest();
            builder.RegisterType<PurposeRepository>().As<IPurposeRepository>().InstancePerRequest();
        }
    }
}