﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class BlockModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BlockApplication>().As<IBlockApplication>().InstancePerRequest();
            builder.RegisterType<Block>().As<IBlock>().InstancePerRequest();
            builder.RegisterType<BlockRepository>().As<IBlockRepository>().InstancePerRequest();
        }
    }
}