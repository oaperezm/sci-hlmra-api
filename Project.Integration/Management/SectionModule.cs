﻿using Autofac;
using Project.Application.Core;
using Project.Application.Management;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class SectionModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<SectionAppnlication>().As<ISectionApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<Section>().As<ISection>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<SectionRepository>().As<ISectionsRepository>().InstancePerRequest();
        }
    }
}
