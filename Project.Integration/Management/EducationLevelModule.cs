﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Management
{
    public class EducationLevelModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<EducationLevelApplication>().As<IEducationLevelApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<EducationLevel>().As<IEducationLevel>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<EducationLevelRepository>().As<IEducationLevelRepository>().InstancePerRequest();
        }
    }
}
