﻿using Autofac;
using Project.Domain.Management;
using Project.Application.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class AreaPersonalSocialDevelopmentModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<AreaPersonalSocialDevelopmentApplication>().As<IAreaPersonalSocialDevelopmentApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<AreaPersonalSocialDevelopment>().As<IAreaPersonalSocialDevelopment>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<AreaPersonalSocialDevelopmentRepository>().As<IAreaPersonalSocialDevelopmentRepository>().InstancePerRequest();
        }
    }
}
