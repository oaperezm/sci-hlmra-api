﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class BookModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BookApplication>().As<IBookApplication>().InstancePerRequest();
            builder.RegisterType<Book>().As<IBook>().InstancePerRequest();
            builder.RegisterType<BookRepository>().As<IBookRepository>().InstancePerRequest();
        }
    }
}