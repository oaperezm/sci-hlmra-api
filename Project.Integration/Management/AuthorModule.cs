﻿using Autofac;
using Project.Application.Management;
using Project.Domain.Management;
using Project.Infraestructure.Management;
using Project.Repositories.Management;

namespace Project.Integration.Management
{
    public class AuthorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorApplication>().As<IAuthorApplication>().InstancePerRequest();
            builder.RegisterType<Author>().As<IAuthor>().InstancePerRequest();
            builder.RegisterType<AuthorRepository>().As<IAuthorRepository>().InstancePerRequest();
        }
    }
}