﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class CourseClassModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CourseClassApplication>().As<ICourseClassApplication>().InstancePerRequest();
            builder.RegisterType<CourseClass>().As<ICourseClass>().InstancePerRequest();
            builder.RegisterType<CourseClassRepository>().As<ICourseClassRepository>().InstancePerRequest();
        }
    }
}
