﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class AnswerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AnswerApplication>().As<IAnswerApplication>().InstancePerRequest();
            builder.RegisterType<Answer>().As<IAnswer>().InstancePerRequest();
            builder.RegisterType<AnswerRepository>().As<IAnswerRepository>().InstancePerRequest();
        }
    }
}