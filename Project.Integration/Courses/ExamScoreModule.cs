﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class ExamScoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExamScoreApplication>().As<IExamScoreApplication>().InstancePerRequest();
            builder.RegisterType<ExamScore>().As<IExamScore>().InstancePerRequest();
            builder.RegisterType<ExamScoreRepository>().As<IExamScoreRepository>().InstancePerRequest();
        }
    }
}
