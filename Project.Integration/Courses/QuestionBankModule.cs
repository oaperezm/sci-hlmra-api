﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class QuestionBankModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QuestionBankApplication>().As<IQuestionBankApplication>().InstancePerRequest();
            builder.RegisterType<QuestionBank>().As<IQuestionBank>().InstancePerRequest();
            builder.RegisterType<QuestionBankRepository>().As<IQuestionBankRepository>().InstancePerRequest();
        }
    }
}