﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class AnnouncementModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AnnouncementApplication>().As<IAnnouncementApplication>().InstancePerRequest();
            builder.RegisterType<Announcement>().As<IAnnouncement>().InstancePerRequest();
            builder.RegisterType<AnnouncementRepository>().As<IAnnouncementRepository>().InstancePerRequest();               
        }
    }
}
