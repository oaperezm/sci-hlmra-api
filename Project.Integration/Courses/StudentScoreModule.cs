﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class StudentScoreModule:  Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StudentScoreApplication>().As<IStudentScoreApplication>().InstancePerRequest();
            builder.RegisterType<StudentScore>().As<IStudentScore>().InstancePerRequest();
            builder.RegisterType<StudentScoreRepository>().As<IStudentScoreRepository>().InstancePerRequest();
        }
    }
}
