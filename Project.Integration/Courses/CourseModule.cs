﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Management;
using Project.Infraestructure.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Repositories.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Management;

namespace Project.Integration.Courses
{
    public class CourseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // PROGRAMACION DE  TAREAS
            builder.RegisterType<HomeWorkApplication>().As<IHomeWorkApplication>().InstancePerRequest();
            builder.RegisterType<HomeWork>().As<IHomeWork>().InstancePerRequest();
            builder.RegisterType<HomeWorkRepository>().As<IHomeWorkRepository>().InstancePerRequest();
            builder.RegisterType<HomeWorkAnswerFileRepository>().As<IHomeWorkAnswerFileRepository>().InstancePerRequest();
            builder.RegisterType<HomeWorkAnswerRepository>().As<IHomeWorkAnswerRepository>().InstancePerRequest();

            // CURSOS
            builder.RegisterType<CourseApplication>().As<ICourseApplication>().InstancePerRequest();
            builder.RegisterType<Course>().As<ICourse>().InstancePerRequest();
            builder.RegisterType<CourseRepository>().As<ICourseRepository>().InstancePerRequest();

            // PROGRAMACION DE EVENTOS
            builder.RegisterType<ScheduledEventApplication>().As<IScheduledEventApplication>().InstancePerRequest();
            builder.RegisterType<ScheduledEvent>().As<IScheduledEvent>().InstancePerRequest();
            builder.RegisterType<ScheduleEventRepository>().As<IScheduledEventRepository>().InstancePerRequest();

            // ASISTENCIAS
            builder.RegisterType<AttendanceApplication>().As<IAttendanceApplication>().InstancePerRequest();
            builder.RegisterType<Attendance>().As<IAttendance>().InstancePerRequest();
            builder.RegisterType<AttendanceRepository>().As<IAttendanceRepository>().InstancePerRequest();

            // LINKINTEREST
            builder.RegisterType<LinkInterestRepository>().As<ILinkInterestRepository>().InstancePerRequest();
            builder.RegisterType<LinkInterestApplication>().As<ILinkInterestApplication>().InstancePerRequest();
            builder.RegisterType<LinkInterest>().As<ILinkInterest>().InstancePerRequest();

        }
    }
}
