﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;
using Project.Infraestructure.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class CourseSectionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CourseSectionApplication>().As<ICourseSectionApplication>().InstancePerRequest();
            builder.RegisterType<CourseSection>().As<ICourseSection>().InstancePerRequest();
            builder.RegisterType<CourseSectionRepository>().As<ICourseSectionRepository>().InstancePerRequest();
        }
    }
}
