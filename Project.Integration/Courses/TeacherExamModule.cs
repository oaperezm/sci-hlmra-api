﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class TeacherExamModule: Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TeacherExamApplication>().As<ITeacherExamApplication>().InstancePerRequest();
            builder.RegisterType<TeacherExam>().As<ITeacherExam>().InstancePerRequest();
            builder.RegisterType<TeacherExamRepository>().As<ITeacherExamRepository>().InstancePerRequest();
            builder.RegisterType<TeacherExamQuestionRepository>().As<ITeacherExamQuestionRepository>().InstancePerRequest();
        }
        
    }
}
