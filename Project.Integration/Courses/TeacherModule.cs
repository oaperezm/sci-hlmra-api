﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class TeacherModule : Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TeacherResourceApplication>().As<ITeacherResourceApplication>().InstancePerRequest();
            builder.RegisterType<TeacherResource>().As<ITeacherResource>().InstancePerRequest();
            builder.RegisterType<TeacherResourceRepository>().As<ITeacherResourceRepository>().InstancePerRequest();

            builder.RegisterType<TeacherExamApplication>().As<ITeacherExamApplication>().InstancePerRequest();
            builder.RegisterType<TeacherExam>().As<ITeacherExam>().InstancePerRequest();
            builder.RegisterType<TeacherExamRepository>().As<ITeacherExamRepository>().InstancePerRequest();
            builder.RegisterType<TeacherExamQuestionRepository>().As<ITeacherExamQuestionRepository>().InstancePerRequest();
            builder.RegisterType<WeightingRepository>().As<IWeightingRepository>().InstancePerRequest();
            builder.RegisterType<ExamScheduleRepository>().As<IExamScheduleRepository>().InstancePerRequest();
            builder.RegisterType<TestRepository>().As<ITestRepository>().InstancePerRequest();

            // RESOURCES TYPE
            builder.RegisterType<ResourceTypeApplication>().As<IResourceTypeApplication>().InstancePerRequest();
            builder.RegisterType<ResourceType>().As<IResourceType>().InstancePerRequest();
            builder.RegisterType<ResourceTypeRepository>().As<IResourceTypeRepository>().InstancePerRequest();
        }

    }
}
