﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class StudentGroupModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StudentGroupApplication>().As<IStudentGroupApplication>().InstancePerRequest();
            builder.RegisterType<StudentGroup>().As<IStudentGroup>().InstancePerRequest();
            builder.RegisterType<StudentGroupRepository>().As<IStudentGroupRepository>().InstancePerRequest();
        }
    }
}