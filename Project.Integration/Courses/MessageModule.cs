﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class MessageModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<MessagesApplication>().As<IMessagesApplication>().InstancePerRequest();
            builder.RegisterType<Messages>().As<IMessages>().InstancePerRequest();

            builder.RegisterType<ChatMessageRepository>().As<IChatMessageRepository>().InstancePerRequest();
            builder.RegisterType<ChatGroupUserRepository>().As<IChatGroupUserRepository>().InstancePerRequest();
            builder.RegisterType<ChatGroupRepository>().As<IChatGroupRepository>().InstancePerRequest();
        }
    }
}
