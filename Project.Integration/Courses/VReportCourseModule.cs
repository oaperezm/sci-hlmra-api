﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class VReportCourseModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<VReportCourseApplication>().As<IVReportCourseAppliction>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<VReportCourse>().As<IVReportCourse>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<VReportCourseRepository>().As<IVReportCourseRepository>().InstancePerRequest();
        }
    }
}
