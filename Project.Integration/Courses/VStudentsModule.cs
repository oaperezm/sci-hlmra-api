﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Infraestructure.Services;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Integration.Courses
{
    public class VStudentsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VStudentsApplication>().As<IVStudentsApplication>().InstancePerRequest();
            builder.RegisterType<VStudents>().As<IVStudents>().InstancePerRequest();
            builder.RegisterType<VStudentsRepository>().As<IVStudentsRepository>().InstancePerRequest();
        }
    }
}
