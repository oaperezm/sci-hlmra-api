﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Views;
using Project.Repositories.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class VCoordinatorReportModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<VCoordinatorReportApplication>().As<IVCoordinatorReportApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<VCoordinatorReport>().As<IVCoordinatorReport>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<VCoordinatorReportRepository>().As<IVCoordinatorReportRepository>().InstancePerRequest();
        }
    }
}
