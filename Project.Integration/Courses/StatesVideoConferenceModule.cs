﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using System.Text;
using System.Threading.Tasks;
using Project.Application.Courses;
using Project.Application.Core;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class StatesVideoConferenceModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StatesVideoConferenceApplication>().As<IStatesVideoConferenceApplication>().InstancePerRequest();
            builder.RegisterType<StatesVideoConference>().As<IStatesVideoConference>().InstancePerRequest();
            builder.RegisterType<StatesVideoConfereceRepository>().As<IStatesVideoConferenceRepository>().InstancePerRequest();
        }
    }
}
