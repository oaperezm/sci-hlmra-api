﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class VCoursesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VCoursesApplication>().As<IVCoursesApplication>().InstancePerRequest();
            builder.RegisterType<VCourses>().As<IVCourses>().InstancePerRequest();
            builder.RegisterType<VCoursesRepository>().As<IVCoursesRepository>().InstancePerRequest();
        }
    }
}
