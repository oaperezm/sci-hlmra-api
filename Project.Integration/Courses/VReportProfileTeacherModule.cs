﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class VReportProfileTeacherModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<VReportProfileTeacherApplication>().As<IVReportProfileTeacherApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<VReportProfileTeacher>().As<IVReportProfileTeacher>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<VReportProfileTeacherRepository>().As<IVReportProfileTeacherReposytory>().InstancePerRequest();
        }
    }
}
