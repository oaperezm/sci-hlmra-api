﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class QuestionTypeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QuestionTypeApplication>().As<IQuestionTypeApplication>().InstancePerRequest();
            builder.RegisterType<QuestionType>().As<IQuestionType>().InstancePerRequest();
            builder.RegisterType<QuestionTypeRepository>().As<IQuestionTypeRepository>().InstancePerRequest();
        }
    }
}