﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class GlossaryModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GlossaryApplication>().As<IGlossaryApplication>().InstancePerRequest();
            builder.RegisterType<Glossary>().As<IGlossary>().InstancePerRequest();
            builder.RegisterType<GlossaryRepository>().As<IGlossaryRepository>().InstancePerRequest();
            builder.RegisterType<WordRepository>().As<IWordRepository>().InstancePerRequest();
        }
    }
}
