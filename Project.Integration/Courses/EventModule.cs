﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
   public class EventModule :Module
    {
        protected override void Load(ContainerBuilder builder)
        {   //Capa de Aplicación
            builder.RegisterType<EventApplication>().As<IEventApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<Event>().As<IEvent>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<EventRepository>().As<IEventRepository>().InstancePerRequest();
        }
    }
}
