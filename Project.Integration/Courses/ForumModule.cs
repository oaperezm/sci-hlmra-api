﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class ForumModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ForumApplication>().As<IForumApplication>().InstancePerRequest();
            builder.RegisterType<Forum>().As<IForum>().InstancePerRequest();
            builder.RegisterType<ForumPostRepository>().As<IForumPostRepository>().InstancePerRequest();
            builder.RegisterType<ForumThreadRepository>().As<IForumThreadRepository>().InstancePerRequest();
            builder.RegisterType<ForumPostVoteRepository>().As<IForumPostVoteRepository>().InstancePerRequest();
        }
    }
}
