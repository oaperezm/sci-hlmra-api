﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class ActivityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ActivityApplication>().As<IActivityApplication>().InstancePerRequest();
            builder.RegisterType<Activity>().As<IActivity>().InstancePerRequest();
            builder.RegisterType<ActivityRepository>().As<IActivityRepository>().InstancePerRequest();
            builder.RegisterType<ActivityFileRepository>().As<IActivityFileRepository>().InstancePerRequest();
            builder.RegisterType<ActivityAnswerRepository>().As<IActivityAnswerRepository>().InstancePerRequest();
        }
    }
}
