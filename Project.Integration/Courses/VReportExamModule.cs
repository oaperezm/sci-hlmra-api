﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class VReportExamModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Capa de Aplicación
            builder.RegisterType<VReportExamApplication>().As<IVReportExamApplication>().InstancePerRequest();
            //Capa de Dominio
            builder.RegisterType<VReportExam>().As<IVReportExam>().InstancePerRequest();
            //Capa de Repositorio e infraestructura
            builder.RegisterType<VReportExamRepository>().As<IVReportExamRepository>().InstancePerRequest();
        }
           
    }
}
