﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class EventReportModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventReportApplication>().As<IEventReportApplication>().InstancePerRequest();
            builder.RegisterType<EventsReport>().As<IEventsReport>().InstancePerRequest();
            builder.RegisterType<EventsReportRepository>().As<IEventsReportRepository>().InstancePerRequest();
        }

    }
}
