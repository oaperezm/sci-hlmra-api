﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class ActivityStudentFileModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ActivityStudentFileApplication>().As<IActivityStudentFileApplication>().InstancePerRequest();
            builder.RegisterType<ActivityStudentFile>().As<IActivityStudentFile>().InstancePerRequest();
            builder.RegisterType<ActivityStudentFileRepository>().As<IActivityStudentFileRepository>().InstancePerRequest();
        }
    }
}
