﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class QuestionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QuestionApplication>().As<IQuestionApplication>().InstancePerRequest();
            builder.RegisterType<Question>().As<IQuestion>().InstancePerRequest();
            builder.RegisterType<QuestionRepository>().As<IQuestionRepository>().InstancePerRequest();
        }
    }
}