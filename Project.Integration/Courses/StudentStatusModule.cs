﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class StudentStatusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StudentStatusApplication>().As<IStudentStatusApplication>().InstancePerRequest();
            builder.RegisterType<StudentStatus>().As<IStudentStatus>().InstancePerRequest();
            builder.RegisterType<StudentStatusRepository>().As<IStudentStatusRepository>().InstancePerRequest();
        }
    }
}