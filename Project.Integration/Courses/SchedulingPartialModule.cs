﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Integration.Courses
{
    public class SchedulingPartialModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SchedulingPartialApplication>().As<ISchedulingPartialApplication>().InstancePerRequest();
            builder.RegisterType<CoursePlanningRepository>().As<ICoursePlanningRepository>().InstancePerRequest();
            builder.RegisterType<SchedulingPartialRepository>().As<ISchedulingPartialRepository>().InstancePerRequest();
            builder.RegisterType<SchedulingPartial>().As<ISchedulingPartial>().InstancePerRequest();
            builder.RegisterType<ExamScheduleRepository>().As<IExamScheduleRepository>().InstancePerRequest();
        }
    }
}