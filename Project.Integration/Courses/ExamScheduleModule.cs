﻿using Autofac;
using Project.Application.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Courses;

namespace Project.Integration.Courses
{
    public class ExamScheduleModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // PROGRAMACIÓN DE EXÁMEN
            builder.RegisterType<ExamScheduleApplication>().As<IExamScheduleApplication>().InstancePerRequest();
            builder.RegisterType<ExamSchedule>().As<IExamSchedule>().InstancePerRequest();
            builder.RegisterType<ExamScheduleRepository>().As<IExamScheduleRepository>().InstancePerRequest();
            builder.RegisterType<SchedulingPartialRepository>().As<ISchedulingPartialRepository>().InstancePerRequest();            

            // APLiCACION DE EXAMEN
            builder.RegisterType<TestApplication>().As<ITestApplication>().InstancePerRequest();
            builder.RegisterType<Test>().As<ITest>().InstancePerRequest();            
            builder.RegisterType<TestRepository>().As<ITestRepository>().InstancePerRequest();
            builder.RegisterType<TestAnswerRepository>().As<ITestAnswerRepository>().InstancePerRequest();
        }
    }
}
