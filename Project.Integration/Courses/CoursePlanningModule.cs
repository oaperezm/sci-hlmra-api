﻿using Autofac;
using Project.Application.Core;
using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Management;
using Project.Infraestructure.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Repositories.Courses;
using Project.Domain.Courses;
using Project.Infraestructure.Repositories.Management;

namespace Project.Integration.Courses
{
    public class CoursePlanningModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CoursePlanningApplication>().As<ICoursePlanningApplication>().InstancePerRequest();
            builder.RegisterType<CoursePlanning>().As<ICoursePlanning>().InstancePerRequest();
            builder.RegisterType<CoursePlanningRepository>().As<ICoursePlanningRepository>().InstancePerRequest();
        }
    }
}
