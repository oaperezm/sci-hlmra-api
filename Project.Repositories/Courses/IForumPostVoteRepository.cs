﻿using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Courses
{
    public interface IForumPostVoteRepository: IRepository<ForumPostVote>
    {

    }
}
