﻿using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Courses
{
    public interface IHomeWorkRepository: IRepository<HomeWork>
    {
    }
}
