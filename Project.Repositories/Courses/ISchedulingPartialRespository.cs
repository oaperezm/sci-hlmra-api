﻿using Project.Entities;
using Project.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Courses
{
    public interface ISchedulingPartialRespository : IRepository<SchedulingPartial>
    {
    }
}
