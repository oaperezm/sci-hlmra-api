﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Courses
{
  public  interface IVideoConferenceRepository : IRepository<VideoConference>
    {
    }
}
