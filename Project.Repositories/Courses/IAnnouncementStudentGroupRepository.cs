﻿using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Courses
{
    public interface IAnnouncementStudentGroupRepository : IRepository<AnnouncementStudentGroup>
    {
    }
}
