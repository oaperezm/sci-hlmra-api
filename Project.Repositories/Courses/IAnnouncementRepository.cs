﻿using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Courses
{
    public interface IAnnouncementRepository: IRepository<Announcement>
    {
    }
}
