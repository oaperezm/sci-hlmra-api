﻿using Project.Entities;
using Project.Repositories.Core;
using Project.Entities.Request;

namespace Project.Repositories.Courses
{
    public interface IQuestionBankRepository: IRepository<QuestionBank>
    {
        
    }
}
