﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Core
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        bool Commit();
        Task<int> SaveChangesAsync();
    }
}
