﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Home
{
    public interface ITipRepository : IRepository<Tip>
    {
    }
}
