﻿using Project.Entities;
using Project.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Home
{
    public interface INewsRepository : IRepository<News>
    {
    }
}
