﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Services
{
    public interface IAESEncryption
    {
        string EncryptMessage(string message);
        string DecryptMessage(string encryptedMessage);
        string GeneratedPassword();


    }
}
