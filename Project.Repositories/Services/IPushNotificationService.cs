﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Services
{
    public interface IPushNotificationService
    {
        Task<bool> CreateNotification(string topic, int notificationMessageId, object data);
        Task<bool> CreateNotification(List<int> userIds, int notificationMessageId, object data);
        Task<bool> RegisterTopic(string deviceId, string topic);
    }
}
