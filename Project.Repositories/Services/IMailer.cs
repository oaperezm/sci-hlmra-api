﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities.Utils;

namespace Project.Repositories.Services
{
    public interface IMailer
    {
        Task SendMail(Message message);
        Task SendPwdResetMail(PwdResetMessage message);
        Task SendPwdChangeMail(PwdChangeMessage message);
        Task SendNewUserMail(WelcomeMessage message);
        Task SendGroupInvitation(InvitationMessage message);
        Task SendContactMail(ContactMessage message);
    }
}
