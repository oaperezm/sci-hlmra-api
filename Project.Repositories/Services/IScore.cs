﻿using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Services
{
    public interface IScore
    {
        Task<Response> GetStudentsScoresByGroup(int studentGroupId, int userId);
    }
}
