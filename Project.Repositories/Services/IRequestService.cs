﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Services
{
    public interface IRequestService
    {
        Task<TU> Get<TU>(string method, string endPoint, string parameters);        
    }
}
