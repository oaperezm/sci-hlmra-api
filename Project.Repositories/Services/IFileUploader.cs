﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities.Utils;
using System.IO;

namespace Project.Repositories.Services
{
    public interface IFileUploader
    {
        Task<string> FileUpload(byte[] fileArray, string fileName, string containerName);
        Task<bool> FileRemove(string filename, string containerName);
        Task<MemoryStream> FileDownload(string filename, string containerName);
    }
}
