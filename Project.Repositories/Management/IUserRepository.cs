﻿using Project.Entities;
using Project.Repositories.Core;
using Project.Entities.Request;

namespace Project.Repositories.Management
{
    public interface IUserRepository: IRepository<User>
    {
        
    }
}
