﻿using Project.Entities;
using Project.Repositories.Core;

namespace Project.Repositories.Management
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
