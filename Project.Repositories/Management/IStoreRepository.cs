﻿using Entities;
using System.Collections.Generic;

namespace Repositories.Management
{
    public interface IStoreRepository
    {
        Product Add(Product product);
        Product Update(Product product);
        Product Remove(Product product);
        List<Product> List(Product product);
    }
}
