﻿using Project.Entities;
using Project.Repositories.Core;
using Project.Entities.Request;

namespace Project.Repositories.Management
{
    public interface IPublishingProfileRepository: IRepository<PublishingProfile>
    {
        
    }
}
