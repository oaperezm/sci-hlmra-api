﻿using Project.Entities.Views;
using Project.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories.Views
{
    public interface IVNodesTotalFileTypesRepository: IRepository<VNodesTotalFileTypes>
    {

    }
}
