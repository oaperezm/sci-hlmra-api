﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class UserAccessConfig: EntityBaseConfiguration<UserAccess>
    {
        public UserAccessConfig() {

            Property(u => u.UserId)
               .IsRequired();

            Property(u => u.AccessDate)
               .IsRequired();

            Property(u => u.CCTs)
             .IsRequired();

            Property(u => u.Roles)
                .IsRequired();


            ToTable("UserAccess");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.AccessDate).HasColumnName("AccessDate");
            Property(u => u.CCTs).HasColumnName("CCTs");
            Property(u => u.Roles).HasColumnName("Roles");


        }
    }
}
