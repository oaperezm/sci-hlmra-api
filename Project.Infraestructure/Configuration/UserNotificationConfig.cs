﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class UserNotificationConfig: EntityBaseConfiguration<UserNotification>
    {
        public UserNotificationConfig() {

            Property(n => n.UserId)
                .IsRequired();

            Property(n => n.Message)
                .IsRequired()
                .HasMaxLength(500);

            Property(n => n.NotificationTypeId)
                  .IsRequired();

            Property(n => n.Read)
                .IsRequired();

            Property(n => n.RegisterDate)
                .IsRequired();

            Property(n => n.Title)
                .IsRequired()
                .HasMaxLength(300);

            Property(n => n.ReadDate)
                .IsOptional();

            ToTable("UserNotofocations");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Message).HasColumnName("Message");
            Property(u => u.NotificationTypeId).HasColumnName("NotificationTypeId");
            Property(u => u.Read).HasColumnName("Read");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.ReadDate).HasColumnName("ReadDate");
            Property(u => u.Title).HasColumnName("Title");

        }
    }
}
