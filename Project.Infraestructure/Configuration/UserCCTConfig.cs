﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class UserCCTConfig: EntityBaseConfiguration<UserCCT>
    {
        public UserCCTConfig() {

            Property(u => u.CCT)
                .IsRequired()
                .HasMaxLength(10);

            Property(u => u.Season)
                 .IsRequired()
                 .HasMaxLength(10);

            Property(u => u.SubSeason)
                .IsRequired()
                .HasMaxLength(10);

            Property(u => u.StartDate)
               .IsRequired();

            Property(u => u.EndDate)
               .IsRequired();

            Property(u => u.UserId)
                .IsRequired();

            Property(u => u.NodeId)
                .IsRequired();

            Property(u => u.RolId)
             .IsRequired();

            Property(u => u.ClasificationSchool)
            .IsOptional();

            Property(u => u.State)
           .IsOptional();

            Property(u => u.Zone)
           .IsOptional();

            Property(u => u.Control)
           .IsOptional();

            Property(u => u.Promoter)
          .IsOptional();


            ToTable("UserCCT");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.CCT).HasColumnName("CCT");
            Property(c => c.Season).HasColumnName("Season");
            Property(c => c.SubSeason).HasColumnName("SubSeason");
            Property(c => c.StartDate).HasColumnName("StartDate");
            Property(c => c.EndDate).HasColumnName("EndDate");
            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.NodeId).HasColumnName("NodeId");
            Property(c => c.RolId).HasColumnName("RolId");


            ///Campos  nuevos
            Property(c => c.ClasificationSchool).HasColumnName("ClasificationSchool");
            Property(c => c.State).HasColumnName("State");
            Property(c => c.Zone).HasColumnName("Zone");
            Property(c => c.Control).HasColumnName("Control");
            Property(c => c.Promoter).HasColumnName("Promoter");

        }
    }
}
