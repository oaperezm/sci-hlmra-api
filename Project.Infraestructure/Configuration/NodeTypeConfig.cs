﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class NodeTypeConfig : EntityBaseConfiguration<NodeType>
    {
        public NodeTypeConfig()
        {
            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Order)
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();

            ToTable("NodeTypes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Order).HasColumnName("Order");
            Property(u => u.Active).HasColumnName("Active");
        }

    }
}
