﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class QuestionConfig : EntityBaseConfiguration<Question>
    {
        public QuestionConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.Content)
                .IsRequired()
                .HasMaxLength(800);

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.Order)
                .IsRequired();

            Property(u => u.QuestionBankId)
                .IsRequired();

            Property(u => u.QuestionTypeId)
                .IsRequired();
            
            Property(u => u.UrlImage)
                .IsOptional();

            Property(u => u.Explanation)
                .HasMaxLength(650)
                .IsOptional();

            Property(u => u.ImageWidth)             
               .IsOptional();

            Property(u => u.ImageHeight)               
               .IsOptional();

            ToTable("Questions");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Content).HasColumnName("Content");
            Property(u => u.Explanation).HasColumnName("Explanation");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.Order).HasColumnName("Order");
            Property(u => u.QuestionBankId).HasColumnName("QuestionBankId");
            Property(u => u.QuestionTypeId).HasColumnName("QuestionTypeId");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.ImageWidth).HasColumnName("ImageWidth");
            Property(u => u.ImageHeight).HasColumnName("ImageHeight");
        }

    }
}
