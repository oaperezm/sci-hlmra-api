﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class QuestionBankConfig : EntityBaseConfiguration<QuestionBank>
    {
        public QuestionBankConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(350);

            Property(u => u.Description)
                .IsOptional()
                .HasMaxLength(600);

            Property(u => u.NodeId)
                .IsRequired();

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UpdateDate)
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.UserId)
                .IsOptional();

            Property(u => u.SystemId)
                .IsOptional();

            ToTable("QuestionBanks");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.NodeId).HasColumnName("NodeId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.SystemId).HasColumnName("SystemId");
        }

    }
}
