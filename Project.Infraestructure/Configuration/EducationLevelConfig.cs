﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class EducationLevelConfig: EntityBaseConfiguration<EducationLevel>
    {
         public EducationLevelConfig()
    {
        Property(u => u.Description)
            .IsRequired();
        Property(u => u.Active)
            .IsOptional();

        ToTable("EducationLevel");

        Property(u => u.Id).HasColumnName("Id");
        Property(u => u.Description).HasColumnName("Description");
        Property(u => u.Active).HasColumnName("Active");

    }
}
}
