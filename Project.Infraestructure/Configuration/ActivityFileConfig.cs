﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ActivityFileConfig : EntityBaseConfiguration<ActivityFile>
    {
        public ActivityFileConfig()
        {

            Property(x => x.ActivityAnswerId)
              .IsRequired();

            Property(x => x.Url)
                .IsRequired();

            Property(x => x.Comment)
                .IsOptional()
                .HasMaxLength(500);

            Property(x => x.RegisterDate)
                .IsOptional();

            ToTable("ActivityFile");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.ActivityAnswerId).HasColumnName("ActivityAnswerId");
            Property(u => u.Url).HasColumnName("Url");
            Property(u => u.Comment).HasColumnName("Comment");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");

        }
    }
}
