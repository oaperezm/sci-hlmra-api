﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class PurposeConfig : EntityBaseConfiguration<Purpose>
    {
        public PurposeConfig()
        {

            Property(u => u.Description)
                .IsRequired();
               
            ToTable("Purposes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
        }

    }
}
