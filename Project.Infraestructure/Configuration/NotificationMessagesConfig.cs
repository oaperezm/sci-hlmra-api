﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class NotificationMessagesConfig: EntityBaseConfiguration<NotificationMessages>
    {
        public NotificationMessagesConfig() {

            Property(n => n.Module)
                .HasMaxLength(350)
                .IsRequired();

            Property(n => n.Action)
                .HasMaxLength(250)
                .IsRequired();

            Property(n => n.Generate)
                .HasMaxLength(250)
                .IsRequired();

            Property(n => n.Receiver)
                .HasMaxLength(250)
                .IsRequired();

            Property(n => n.Title)
                .HasMaxLength(300)
                .IsRequired();

            Property(n => n.Message)
              .HasMaxLength(500)
              .IsRequired();
            
            ToTable("NotificationMessages");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Module).HasColumnName("Module");
            Property(u => u.Action).HasColumnName("Action");
            Property(u => u.Generate).HasColumnName("Generate");
            Property(u => u.Receiver).HasColumnName("Receiver");
            Property(u => u.Title).HasColumnName("Title");
            Property(u => u.Message).HasColumnName("Message");

        }
    }
}
