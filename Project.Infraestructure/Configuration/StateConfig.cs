﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class StateConfig:EntityBaseConfiguration<State>
    {
        public StateConfig() {

            Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(350);

            Property(s => s.CityId)
                .IsRequired();

            ToTable("States");

            Property(s => s.Id).HasColumnName("Id");
            Property(s => s.Name).HasColumnName("Name");
            Property(s => s.CityId).HasColumnName("CityId");
        }
    }
}
