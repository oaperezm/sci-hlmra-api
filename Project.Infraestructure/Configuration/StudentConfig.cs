﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class StudentConfig : EntityBaseConfiguration<Student>
    {
        public StudentConfig()
        {
            Property(u => u.Id)
                .IsRequired();
            Property(u => u.Codigo)
                .IsOptional()
                .HasMaxLength(250);
            Property(u => u.InvitationDate)
                .IsRequired();
            Property(u => u.CheckDate)
                .IsOptional();
            Property(u => u.StudentGroupId)
                .IsRequired();
            Property(u => u.UserId)
                .IsRequired();
            Property(u => u.StudentStatusId)
                .IsRequired();

            ToTable("Students");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Codigo).HasColumnName("Codigo");
            Property(u => u.InvitationDate).HasColumnName("InvitationDate");
            Property(u => u.CheckDate).HasColumnName("CheckDate");
            Property(u => u.StudentGroupId).HasColumnName("StudentGroupId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.StudentStatusId).HasColumnName("StudentStatusId");
        }

    }
}
