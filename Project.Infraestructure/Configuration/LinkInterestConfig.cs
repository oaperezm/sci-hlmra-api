﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class LinkInterestConfig: EntityBaseConfiguration<LinkInterest>
    {
        public LinkInterestConfig() {
            
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(250);

            Property(x => x.Description)
                .IsOptional()
                .HasMaxLength(500);

            Property(x => x.Uri)             
                .IsRequired();
            
            Property(x => x.CourseId)             
                .IsRequired();

            ToTable("LinkInterests");

            Property(x => x.Id).HasColumnName("Id");
            Property(x => x.Title).HasColumnName("Title");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.Uri).HasColumnName("Uri");
            Property(x => x.CourseId).HasColumnName("CourseId");          
        }
    }
}
