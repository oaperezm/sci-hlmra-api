﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class BookConfig : EntityBaseConfiguration<Book>
    {
        public BookConfig()
        {
            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(250);

            Property(u => u.Active)
                .IsOptional();

            Property(u => u.ISBN)
                 .IsRequired()
                 .HasMaxLength(15);

            Property(u => u.Edition)
                .IsOptional()
                .HasMaxLength(100);
                        
            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UpdateDate)
                .IsOptional();

            Property(u => u.Codebar)
                .IsRequired()
                .HasMaxLength(20);
            
            ToTable("Books");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.ISBN).HasColumnName("ISBN");
            Property(u => u.Codebar).HasColumnName("Codebar");
            Property(u => u.Edition).HasColumnName("Edition");            
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");

            HasMany<Author>(r => r.Authors)
                .WithMany(p => p.Books)
                .Map(rp => {
                    rp.MapLeftKey("BookId");
                    rp.MapRightKey("AuthorId");
                });
        }
    }
}
