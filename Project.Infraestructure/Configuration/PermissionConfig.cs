﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class PermissionConfig : EntityBaseConfiguration<Permission>
    {

        public PermissionConfig()
        {

            Property(u => u.Module)
                .IsRequired()
                .HasMaxLength(80);

            Property(u => u.Description)
                .IsOptional()
                .HasMaxLength(250);

            Property(u => u.Url)
                .IsOptional()
                .HasMaxLength(350);

            Property(u => u.Icon)
                .IsOptional()
                .HasMaxLength(50);

            Property(u => u.ParentId)
              .IsOptional();              

            Property(u => u.Active)
                .IsOptional();
            
            Property(u => u.ShowInMenu)
                .IsRequired();

            Property(u => u.SystemId)
              .IsOptional();

            Property(u => u.Order)
                .IsOptional();

            ToTable("Permissions");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Module).HasColumnName("Module");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Url).HasColumnName("Url");
            Property(u => u.Icon).HasColumnName("Icon");
            Property(u => u.ParentId).HasColumnName("ParentId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.ShowInMenu).HasColumnName("ShowInMenu");
            Property(u => u.SystemId).HasColumnName("SystemId");
            Property(u => u.Order).HasColumnName("Order");

        }
    }
}
