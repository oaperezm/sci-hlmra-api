﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class PublishingProfileUserConfig : EntityBaseConfiguration<PublishingProfileUser>
    {
        public PublishingProfileUserConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.PublishingProfileId)
                .IsOptional();

            Property(u => u.UserId)
                .IsOptional();

            Property(u => u.RegisterDate)
                .IsOptional();

            ToTable("PublishingProfileUsers");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.PublishingProfileId).HasColumnName("PublishingProfileId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");         
        }

    }
}
