﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ChatMessageConfig: EntityBaseConfiguration<ChatMessage>
    {
        public ChatMessageConfig() {

            Property(c => c.Message)
                .IsRequired();

            Property(c => c.Url)
                .IsOptional();

            Property(c => c.SenderUserId)
                .IsRequired();

            Property(c => c.ReceiverUserId)
                .IsRequired();

            Property(c => c.ChatGroupId)
               .IsRequired();

            Property(c => c.Read)
                .IsRequired();

            Property(c => c.MessageSequence)
                .IsRequired();

            Property(c => c.RegisterDate)
                .IsRequired();

            ToTable("ChatMessages");

            Property(c => c.Message).HasColumnName("Message");
            Property(c => c.Url).HasColumnName("Url");
            Property(c => c.SenderUserId).HasColumnName("SenderUserId");
            Property(c => c.ReceiverUserId).HasColumnName("ReceiverUserId");
            Property(c => c.ChatGroupId).HasColumnName("ChatGroupId");
            Property(c => c.Read).HasColumnName("Read");
            Property(c => c.MessageSequence).HasColumnName("MessageSequence");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");

            HasRequired(x => x.SenderUser)
                .WithMany()
                .WillCascadeOnDelete(false);

            HasRequired(x => x.ReceiverUser)
                .WithMany()
                .WillCascadeOnDelete(false);

        }
    }
}
