﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class StudentScoreConfig : EntityBaseConfiguration<StudentScore>
    {
        public StudentScoreConfig()
        {
            Property(u => u.Id)
               .IsRequired();

            Property(u => u.Homework)
             .IsOptional();

            Property(c => c.Exam)
             .IsRequired();

            Property(c => c.Activity)
               .IsRequired();

            Property(c => c.Score)
               .IsRequired();

            Property(c => c.Assistance)
            .IsOptional();

            Property(c => c.SchedulingPartialId)
             .IsOptional();

            Property(c => c.Points)
             .IsRequired();

            Property(c => c.Status)
             .IsRequired();          

            Property(c => c.RegisterDate)
             .IsOptional();

            Property(c => c.Comment)
                .IsOptional()
                .HasMaxLength(500);

            Property(c => c.StudentGroupId)
                .IsRequired();

            Property(c => c.StudentId)
                .IsRequired();

            Property(c => c.UserId)
                .IsRequired();

            ToTable("StudentScore");

            Property(c => c.Id).HasColumnName("Id");

            Property(c => c.Homework).HasColumnName("Homework");

            Property(c => c.Exam).HasColumnName("Exam");

            Property(c => c.Assistance).HasColumnName("Assistance");

            Property(c => c.Activity).HasColumnName("Activity");

            Property(c => c.Score).HasColumnName("Score");

            Property(c => c.SchedulingPartialId).HasColumnName("SchedulingPartialId");

            Property(c => c.Points).HasColumnName("Points");

            Property(c => c.Status).HasColumnName("Status");            

            Property(c => c.RegisterDate).HasColumnName("RegisterDate");

            Property(c => c.Comment).HasColumnName("Comment");

            Property(c => c.StudentGroupId).HasColumnName("StudentGroupId");

            Property(c => c.StudentId).HasColumnName("StudentId");

            Property(c => c.UserId).HasColumnName("UserId");

            HasRequired(s => s.StudentGroup)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
