﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ChatGroupConfig: EntityBaseConfiguration<ChatGroup>
    {
        public ChatGroupConfig() {

            Property(c => c.CreateUserId)
                    .IsRequired();

            Property(c => c.CreateDate)
                .IsRequired();

            Property(c => c.UpdateDate)
                .IsOptional();

            ToTable("ChatGroups");

            Property(c => c.CreateUserId).HasColumnName("CreateUserId");
            Property(c => c.CreateDate).HasColumnName("CreateDate");
            Property(c => c.UpdateDate).HasColumnName("UpdateDate");

            HasRequired(x => x.CreateUser)
                .WithMany()
                .WillCascadeOnDelete(false);

        }
    }
}
