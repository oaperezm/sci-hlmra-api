﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class VideoConferenceConfig : EntityBaseConfiguration<VideoConference>

    {
        public VideoConferenceConfig()
        {
            Property(v => v.Title)
                .IsRequired()
                .HasMaxLength(100);
            Property(v => v.Information)
                .IsRequired()
                .HasMaxLength(800);
            Property(v => v.DateTimeVideoConference)
                .IsOptional();
            Property(v => v.Duration)
                .IsRequired();
            //Property(v => v.InvitedId)
            //   .IsOptional();
            Property(v => v.StatusId)
                         .IsRequired();
            Property(v => v.Commentary)
              .IsRequired()
              .HasMaxLength(800);
            Property(v => v.RegisterDate)
                             .IsOptional();

            Property(v => v.UpdateDate)
                .IsOptional();
            Property(v => v.RoomVideoConference)
                .IsRequired();
            Property(v => v.GuestMail)
              .IsRequired();
            Property(v => v.StudentGroupId)
              .IsOptional();
            Property(v => v.CourseId)
               .IsOptional();
            Property(v => v.emailsExternal)
               .IsOptional();
            Property(v => v.UserId)
               .IsOptional();

            ToTable("VideoConference");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Title).HasColumnName("Title");
            Property(c => c.Information).HasColumnName("Information");
            Property(c => c.DateTimeVideoConference).HasColumnName("DateTimeVideoConference");
            Property(c => c.Duration).HasColumnName("Duration");

            Property(c => c.StatusId).HasColumnName("StatusId");
            Property(c => c.Commentary).HasColumnName("Commentary");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.UpdateDate).HasColumnName("UpdateDate");
            Property(c => c.RoomVideoConference).HasColumnName("RoomVideoConference");
            Property(c => c.GuestMail).HasColumnName("GuestMail");
            Property(c => c.StudentGroupId).HasColumnName("StudentGroupId");
            Property(c => c.CourseId).HasColumnName("CourseId");
            Property(c => c.emailsExternal).HasColumnName("emailsExternal");
            Property(c => c.UserId).HasColumnName("UserId");

            //HasRequired(s => s.StudentGroup)
            //  .WithMany()
            //  .WillCascadeOnDelete(false);

            //HasRequired(s => s.Course)
            //  .WithMany()
            //  .WillCascadeOnDelete(false);

        }





    }



}