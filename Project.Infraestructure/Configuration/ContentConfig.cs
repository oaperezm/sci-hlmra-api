﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class ContentConfig : EntityBaseConfiguration<Content>
    {
        public ContentConfig()
        {
            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.UrlContent)
                .IsRequired()
                .HasMaxLength(500);

            Property(u => u.IsPublic)
                .IsOptional();

            Property(u => u.Active)
                .IsOptional();

            Property(u => u.ContentTypeId)
                .IsRequired();

            Property(u => u.FileTypeId)
                .IsRequired();

            Property(u => u.TrainingFieldId)
                 .IsOptional();

            Property(u => u.FormativeFieldId)
                .IsOptional();

            Property(u => u.AreaId)
                .IsOptional();

            Property(u => u.PurposeId)
                .IsOptional();
            
            Property(u => u.ContentResourceTypeId)
              .IsOptional();

            Property(u => u.Thumbnails)
                .IsOptional();
            
            Property(u => u.SystemId)
                .IsOptional();

            Property(u => u.RegisterDate)
                .IsOptional();

            Property(u => u.Visits)
                .IsRequired();

            Property(u => u.IsRecommended)
                .IsRequired();

            Property(u => u.BlockId)
                .IsOptional();
            Property(u => u.EducationLevelId)
               .IsOptional();
            Property(u => u.AreaPersonalSocialDevelopmentId)
                .IsOptional();
            Property(u => u.KeylearningId)
                .IsOptional();
            Property(u => u.AreasCurriculumAutonomyId)
                .IsOptional();
            Property(u => u.LearningexpectedId)
                .IsOptional();
            Property(u => u.AxisId)
                .IsOptional();

            ToTable("Contents");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.UrlContent).HasColumnName("UrlContent");
            Property(u => u.IsPublic).HasColumnName("IsPublic");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.ContentTypeId).HasColumnName("ContentTypeId");
            Property(u => u.FileTypeId).HasColumnName("FileTypeId");
            Property(u => u.TrainingFieldId).HasColumnName("TrainingFieldId");
            Property(u => u.FormativeFieldId).HasColumnName("FormativeFieldId");
            Property(u => u.AreaId).HasColumnName("AreaId");
            Property(u => u.PurposeId).HasColumnName("PurposeId");
            Property(u => u.ContentResourceTypeId).HasColumnName("ContentResourceTypeId");
            Property(u => u.Thumbnails).HasColumnName("Thumbnails");
            Property(u => u.SystemId).HasColumnName("SystemId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.Visits).HasColumnName("Visits");
            Property(u => u.BlockId).HasColumnName("BlockId");
            Property(u => u.BlockId).HasColumnName("BlockId");
            Property(u => u.AxisId).HasColumnName("AxisId");
            Property(u => u.AreasCurriculumAutonomyId).HasColumnName("AreasCurriculumAutonomyId");
            Property(u => u.AreaPersonalSocialDevelopmentId).HasColumnName("AreaPersonalSocialDevelopmentId");
            Property(u => u.KeylearningId).HasColumnName("KeylearningId");
            Property(u => u.LearningexpectedId).HasColumnName("LearningexpectedId");
            Property(u => u.EducationLevelId).HasColumnName("EducationLevelId");
            HasMany(u => u.NodeContents).WithRequired(c => c.Content);

            HasMany(u => u.Tags)
                .WithMany( t => t.Contents )
                .Map(nt =>
            {
                nt.MapLeftKey("ContentId");
                nt.MapRightKey("TagId");
            });
            
        }
    }
}
