﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ScheduledEventConfig: EntityBaseConfiguration<ScheduledEvent>
    {
        public ScheduledEventConfig() {

            Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(350);

            Property(s => s.Description)
                .IsRequired();

            Property(s => s.DateEvent)
                .IsRequired();

            Property(s => s.InitTime)
                .IsRequired()
                .HasMaxLength(5);

            Property(s => s.Duration)
                .IsRequired();

            Property(s => s.EndEvent)
                .IsRequired();

            Property(s => s.UserId)
                .IsRequired();

            Property(s => s.CourseId)
                .IsRequired();

            Property(s => s.Active)
                .IsRequired();
            
            Property(s => s.RegisterDate)
               .IsRequired();

            ToTable("ScheduledEvents");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.DateEvent).HasColumnName("DateEvent");
            Property(c => c.EndEvent).HasColumnName("EndEvent");
            Property(c => c.InitTime).HasColumnName("InitTime");
            Property(c => c.Duration).HasColumnName("Duration");
            Property(c => c.CourseId).HasColumnName("CourseId");
            Property(c => c.Active).HasColumnName("Active");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");            
            Property(c => c.UserId).HasColumnName("UserId");

            HasMany( s => s.Groups)
                .WithMany( s=> s.Events)                
                .Map(rp => {
                    rp.MapLeftKey("ScheduledEventId");
                    rp.MapRightKey("StudentGroupId");
                });

            HasRequired(s => s.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            HasRequired(s => s.Course)
               .WithMany()
               .WillCascadeOnDelete(false);
        }
    }
}
