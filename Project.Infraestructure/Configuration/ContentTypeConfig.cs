﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class ContentTypeConfig : EntityBaseConfiguration<ContentType>
    {
        public ContentTypeConfig()
        {

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            ToTable("ContentTypes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
        }

    }
}
