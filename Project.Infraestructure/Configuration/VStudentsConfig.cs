﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Infraestructure.Configuration
{
    public class VStudentsConfig : EntityBaseConfiguration<VStudents>
    {
        public VStudentsConfig()
        {
            Property(u => u.Teacher);
            Property(u => u.StudentId);
            Property(u => u.Student);
            Property(u => u.StudentEmail);
            Property(u => u.StudentUrlImage);
            Property(u => u.StudentSystemId);
            Property(u => u.StudentSystem);
            Property(u => u.StudentUserId);
            Property(u => u.StudentStatusId);
            Property(u => u.StudentStatusDescription);
            Property(u => u.StudentGroupId);
            Property(u => u.StudentGroupDescription);
            Property(u => u.CourseId);
            Property(u => u.CourseName);
            Property(u => u.CourseDescription);
            Property(u => u.CourseSubject);

            ToTable("V_Students");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Teacher).HasColumnName("Teacher");
            Property(u => u.StudentId).HasColumnName("StudentId");
            Property(u => u.Student).HasColumnName("Student");
            Property(u => u.StudentEmail).HasColumnName("StudentEmail");
            Property(u => u.StudentUrlImage).HasColumnName("StudentUrlImage");
            Property(u => u.StudentSystemId).HasColumnName("StudentSystemId");
            Property(u => u.StudentSystem).HasColumnName("StudentSystem");
            Property(u => u.StudentUserId).HasColumnName("StudentUserId");
            Property(u => u.StudentStatusId).HasColumnName("StudentStatusId");
            Property(u => u.StudentStatusDescription).HasColumnName("StudentStatusDescription");
            Property(u => u.StudentGroupId).HasColumnName("StudentGroupId");
            Property(u => u.StudentGroupDescription).HasColumnName("StudentGroupDescription");
            Property(u => u.CourseId).HasColumnName("CourseId");
            Property(u => u.CourseName).HasColumnName("CourseName");
            Property(u => u.CourseDescription).HasColumnName("CourseDescription");
            Property(u => u.CourseSubject).HasColumnName("CourseSubject");
        }
    }
}
