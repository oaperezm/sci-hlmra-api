﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ExamScheduleTypeConfig : EntityBaseConfiguration<ExamScheduleType>
    {
        public ExamScheduleTypeConfig() {
                        

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(350);

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UpdateDate)
                .IsOptional();

            ToTable("ExamScheduleTypes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");
        }
    }
}
