﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class StudentStatusConfig : EntityBaseConfiguration<StudentStatus>
    {
        public StudentStatusConfig()
        {
            Property(u => u.Id)
                .IsRequired();
            Property(u => u.Description)
                .IsRequired()     
                .HasMaxLength(250);

            ToTable("StudentStatuses");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");            
        }

    }
}
