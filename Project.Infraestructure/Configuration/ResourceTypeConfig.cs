﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Project.Infraestructure.Configuration
{
    public class ResourceTypeConfig: EntityBaseConfiguration<ResourceType>
    {
        public ResourceTypeConfig() {
                       

            Property(c => c.Description)
               .IsRequired()
               .HasMaxLength(300);

            Property(c => c.RegisterDate)
               .IsRequired();               

            Property(u => u.Order)
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();
            
            ToTable("ResourceType");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.Order).HasColumnName("Order");
            Property(c => c.Active).HasColumnName("Active");
        }
    }
}
