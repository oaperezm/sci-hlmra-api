﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ActivityStudentFileConfig : EntityBaseConfiguration<ActivityStudentFile>
    {
        public ActivityStudentFileConfig()
        {   
            Property(x => x.ActivitiesId)
              .IsRequired();

            Property(x => x.Url)
                .IsRequired();

            Property(x => x.Comment)
                .IsOptional()
                .HasMaxLength(500);

            Property(x => x.RegisterDate)
                .IsOptional();

            Property(x => x.UserId)
              .IsRequired();

            ToTable("ActivityStudentFile");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.ActivitiesId).HasColumnName("ActivitiesId");
            Property(u => u.Url).HasColumnName("Url");
            Property(u => u.Comment).HasColumnName("Comment");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UserId).HasColumnName("UserId");
        }
         
    }
}
