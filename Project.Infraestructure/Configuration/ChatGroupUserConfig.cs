﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ChatGroupUserConfig:EntityBaseConfiguration<ChatGroupUser>
    {
        public ChatGroupUserConfig() {

            Property(c => c.UserId)
                .IsRequired();

            Property(c => c.ChatGroupId)
                .IsRequired();

            Property(c => c.RegisterDate)
                .IsRequired();

            Property(c => c.ArchiveDate)
                .IsOptional();

            Property(c => c.DeleteDate)
               .IsOptional();

            ToTable(" ChatGroupUsers");

            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.ChatGroupId).HasColumnName("ChatGroupId");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.ArchiveDate).HasColumnName("ArchiveDate");
            Property(c => c.DeleteDate).HasColumnName("DeleteDate");

            HasRequired(c => c.User)
                .WithMany()
                .WillCascadeOnDelete(false);

        }

    }
}
