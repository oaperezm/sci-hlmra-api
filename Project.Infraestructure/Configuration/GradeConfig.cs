﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class GradeConfig : EntityBaseConfiguration<Grade>
    {
        public GradeConfig()
        {

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            ToTable("Grades");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
        }

    }
}
