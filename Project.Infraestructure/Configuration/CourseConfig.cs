﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class CourseConfig : EntityBaseConfiguration<Course>
    {
        public CourseConfig() {

            Property(c => c.Name)
               .IsRequired()
               .HasMaxLength(500);

            Property(c => c.Description)
               .IsRequired()
               .HasMaxLength(800);

            Property(c => c.Gol)
               .IsRequired()
               .HasMaxLength(800);

            Property(c => c.Subject)
               .IsRequired()
               .HasMaxLength(350);

            Property(c => c.StartDate)
               .IsRequired();

            Property(c => c.EndDate)
               .IsRequired();

            Property(c => c.Active)
               .IsRequired();

            Property(c => c.NodeId)
               .IsRequired();

            Property(c => c.UserId)
             .IsRequired();

            Property(c => c.RegisterDate)
             .IsOptional();

            Property(c => c.UpdateDate)
             .IsOptional();

            Property(c => c.Monday)
               .IsRequired();

            Property(c => c.Tuesday)
               .IsRequired();

            Property(c => c.Wednesday)
               .IsRequired();

            Property(c => c.Thursday)
               .IsRequired();

            Property(c => c.Friday)
               .IsRequired();

            Property(c => c.Saturday)
               .IsRequired();

            Property(c => c.Sunday)
               .IsRequired();

            Property(c => c.Institution)
               .IsOptional()
               .HasMaxLength(300);



            ToTable("Courses");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.Gol).HasColumnName("Gol");
            Property(c => c.StartDate).HasColumnName("StartDate");
            Property(c => c.EndDate).HasColumnName("EndDate");
            Property(c => c.Institution).HasColumnName("Institution");
            Property(c => c.Active).HasColumnName("Active");
            Property(c => c.NodeId).HasColumnName("NodeId");
            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.UpdateDate).HasColumnName("UpdateDate");
            Property(c => c.Subject).HasColumnName("Subject");
            Property(c => c.Monday).HasColumnName("Monday");
            Property(c => c.Tuesday).HasColumnName("Tuesday");
            Property(c => c.Wednesday).HasColumnName("Wednesday");
            Property(c => c.Thursday).HasColumnName("Thursday");
            Property(c => c.Friday).HasColumnName("Friday");
            Property(c => c.Saturday).HasColumnName("Saturday");
            Property(c => c.Sunday).HasColumnName("Sunday");

        }
    }
}
