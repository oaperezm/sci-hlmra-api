﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class EventsReportConfig :EntityBaseConfiguration<EventsReport>
    {
        public EventsReportConfig()
        {
            Property(x => x.Id)
                .IsRequired();
            Property(x => x.CCT)
                .IsRequired();
            Property(x => x.Username)
                .IsRequired();
            Property(x => x.Level)
            .IsRequired();
            Property(x => x.InstitutionName)
            .IsRequired();
            Property(x => x.State)
              .IsRequired();
            Property(x => x.ClasificationSchool)
             .IsRequired();
            Property(x => x.Zone)
            .IsRequired();
            Property(x => x.Control)
            .IsRequired();
            Property(x => x.Prometer)
            .IsRequired();
            Property(x => x.EntryDate)
            .IsRequired();
            Property(x => x.TimeNavegation)
            .IsRequired();
            Property(x => x.SectionId)
            .IsRequired();
            Property(x => x.Grade)
            .IsRequired();
            Property(x => x.EducationField)
            .IsRequired();
            Property(x => x.Subject)
            .IsRequired();
            Property(x => x.FileTypeId)
            .IsRequired();
            Property(x => x.ResourcenName)
           .IsRequired();
            Property(x => x.UserId)
            .IsRequired();
            Property(x => x.UserId)
           .IsRequired();
            Property(x => x.EventId)
           .IsRequired();
            Property(x => x.Element)
           .IsRequired();
            Property(x => x.Comment)
           .IsRequired();
            Property(x => x.CommentDetail)
          .IsRequired();
            Property(x => x.EducationLevelId)
            .IsRequired();

          
            Property(x => x.ContentTypeId)
            .IsRequired();

            ToTable("EventsReport");

            Property(x => x.Id).HasColumnName("Id");
            Property(x => x.CCT).HasColumnName("CCT");
            Property(x => x.Username).HasColumnName("Username");
            Property(x => x.Level).HasColumnName("Level");
            Property(x => x.InstitutionName).HasColumnName("InstitutionName");
            Property(x => x.State).HasColumnName("State");
            Property(x => x.ClasificationSchool).HasColumnName("ClasificationSchool");
            Property(x => x.Control).HasColumnName("Control");
            Property(x => x.Prometer).HasColumnName("Prometer");
            Property(x => x.EntryDate).HasColumnName("EntryDate");
            Property(x => x.TimeNavegation).HasColumnName("TimeNavegation");
            Property(x => x.SectionId).HasColumnName("SectionId");
            Property(x => x.Grade).HasColumnName("Grade");
            Property(x => x.EducationField).HasColumnName("EducationField");
            Property(x => x.Subject).HasColumnName("Subject");
            Property(x => x.FileTypeId).HasColumnName("FileTypeId");
            Property(x => x.ResourcenName).HasColumnName("ResourcenName");
            Property(x => x.UserId).HasColumnName("UserId");
            Property(x => x.EducationField).HasColumnName("EducationField");
            Property(x => x.EventId).HasColumnName("EventId");
            Property(x => x.Element).HasColumnName("Element");
            Property(x => x.Comment).HasColumnName("Comment");
            Property(x => x.CommentDetail).HasColumnName("CommentDetail");
            Property(x => x.ContentTypeId).HasColumnName("ContentTypeId");

            HasRequired(x => x.Events)
             .WithMany()
             .WillCascadeOnDelete(false);

            HasRequired(u => u.EducationLevel)
             .WithMany()
             .WillCascadeOnDelete(false);

            HasRequired(u => u.FileType)
            .WithMany()
            .WillCascadeOnDelete(false);

            HasRequired(u => u.Sections)
            .WithMany()
            .WillCascadeOnDelete(false);
            HasRequired(u => u.ContentType)
            .WithMany()
            .WillCascadeOnDelete(false);
        }
    }
}
