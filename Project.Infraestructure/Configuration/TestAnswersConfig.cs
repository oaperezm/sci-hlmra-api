﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TestAnswersConfig: EntityBaseConfiguration<TestAnswer>
    {
        public TestAnswersConfig() {

            Property(t => t.TestId)
              .IsRequired();

            Property(t => t.AnswerId)               
               .IsOptional();

            Property(t => t.Explanation)
               .IsRequired();

            Property(t => t.TeacherExamQuestionId)
               .IsOptional(); 

            ToTable("TestAnswers");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.TestId).HasColumnName("TestId");
            Property(u => u.AnswerId).HasColumnName("AnswerId");
            Property(u => u.Explanation).HasColumnName("Explanation");
            Property(u => u.TeacherExamQuestionId).HasColumnName("TeacherExamQuestionId");
        }
    }
}
