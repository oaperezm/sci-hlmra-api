﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class CourseSectionConfig : EntityBaseConfiguration<CourseSection>
    {
        public CourseSectionConfig() {

            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.Name)
               .IsRequired()
               .HasMaxLength(300);

            Property(c => c.Content)
               .IsRequired()
               .HasMaxLength(800);

            Property(u => u.Order)
                .IsOptional();

            Property(u => u.CourseId)
                .IsRequired();


            ToTable("CourseSections");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Content).HasColumnName("Content");
            Property(c => c.Order).HasColumnName("Order");
            Property(c => c.CourseId).HasColumnName("CourseId");

        }
    }
}
