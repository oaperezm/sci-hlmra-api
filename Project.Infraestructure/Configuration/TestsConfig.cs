﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TestsConfig: EntityBaseConfiguration<Test>
    {
        public TestsConfig() {
            Property(t => t.RegisterDate)
                       .IsRequired();

            Property(t => t.Score)
               .HasPrecision(6, 2)
               .IsOptional();

            Property(t => t.IsCompleted)
               .IsRequired();

            Property(t => t.ClientId)
               .IsRequired();

            Property(t => t.UserId)
             .IsRequired();

            Property(t => t.ExamScheduleId)
             .IsRequired();

            Property(t => t.CorrectAnswers)
              .IsOptional();

            Property(t => t.Comments)
              .IsOptional()
              .HasMaxLength(450);

            Property(t => t.StartDate)
              .IsOptional();

            Property(t => t.LastResponseDate)
              .IsOptional();

            Property(t => t.IsEvaluated)
              .IsOptional();

            Property(t => t.EvaluationDate)
              .IsOptional();

            ToTable("Tests");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.Score).HasColumnName("Score");
            Property(u => u.IsCompleted).HasColumnName("IsCompleted");
            Property(u => u.ClientId).HasColumnName("ClientId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.ExamScheduleId).HasColumnName("ExamScheduleId");
            Property(u => u.CorrectAnswers).HasColumnName("CorrectAnswers");
            Property(u => u.Comments).HasColumnName("Comments");
            Property(u => u.StartDate).HasColumnName("StartDate");
            Property(u => u.LastResponseDate).HasColumnName("LastResponseDate");
            Property(u => u.IsEvaluated).HasColumnName("IsEvaluated");
            Property(u => u.EvaluationDate).HasColumnName("EvaluationDate");
        }

    }
}
