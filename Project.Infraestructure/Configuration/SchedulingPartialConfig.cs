﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class SchedulingPartialConfig : EntityBaseConfiguration<SchedulingPartial>
    {
        public SchedulingPartialConfig()
        {
            Property(s => s.Id).IsRequired();
            Property(s => s.StartDate).IsRequired();
            Property(s => s.EndDate).IsRequired();
            Property(s => s.Description).IsRequired();
            Property(s => s.FinalPartial).IsRequired();
            Property(s => s.CoursePlanningId).IsRequired();
            Property(s => s.RegisterDate).IsRequired();
            Property(s => s.UpdateDate).IsRequired();

            ToTable(" SchedulingPartial");
            Property(s => s.Id).HasColumnName("Id");
            Property(s => s.StartDate).HasColumnName("StartDate");
            Property(s => s.EndDate).HasColumnName("EndDate");
            Property(s => s.Description).HasColumnName("Description");
            Property(s => s.FinalPartial).HasColumnName("FinalPartial");
            Property(s => s.CoursePlanningId).HasColumnName("CoursePlanningId");
            Property(s => s.RegisterDate).HasColumnName("RegisterDate");
            Property(s => s.UpdateDate).HasColumnName("UpdateDate");


        }
    }
}
