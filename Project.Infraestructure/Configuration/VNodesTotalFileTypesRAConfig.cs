﻿using Project.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VNodesTotalFileTypesRAConfig: EntityBaseConfiguration<VNodesTotalFileTypesRA>
    {
        public VNodesTotalFileTypesRAConfig()
        {            
            ToTable("V_NodesTotalFileTypesRA");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.ParentId).HasColumnName("ParentId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.NodeTypeId).HasColumnName("NodeTypeId");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.Videos).HasColumnName("Videos");
            Property(u => u.Audios).HasColumnName("Audios");
            Property(u => u.Documents).HasColumnName("Documents");
            Property(u => u.Images).HasColumnName("Images");

        }
    }
}
