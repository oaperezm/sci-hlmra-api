﻿using Project.Entities;
using Project.Infraestructure.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure
{
    public class SectionConfig: EntityBaseConfiguration<Sections>
    {

        public SectionConfig()
        {
            Property(u => u.Description)
                .IsRequired();
            Property(u => u.Active)
                .IsOptional();
            
            ToTable("Sections");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
           

        }
    }
}
