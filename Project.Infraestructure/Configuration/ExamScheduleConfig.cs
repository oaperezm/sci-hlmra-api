﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ExamScheduleConfig: EntityBaseConfiguration<ExamSchedule>
    {
        public ExamScheduleConfig() {

            Property(u => u.SchedulingPartialId)
                .IsOptional();

            Property(u => u.Description)
                .HasMaxLength(350)
                .IsRequired();

            Property(u => u.BeginApplicationDate)
                .IsRequired();

            Property(u => u.EndApplicationDate)
                .IsRequired();         

            Property(u => u.StudentGroupId)
                .IsRequired();

            Property(u => u.UserId)
                .IsRequired();

            Property(u => u.ExamScheduleTypeId)
                .IsRequired();

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UpdateDate)
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();

            Property(t => t.TeacherExamId)
                .IsOptional();

            ToTable("ExamSchedules");
            
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.SchedulingPartialId ).HasColumnName("SchedulingPartialId");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.BeginApplicationDate).HasColumnName("BeginApplicationDate");
            Property(u => u.EndApplicationDate).HasColumnName("EndApplicationDate");
            Property(u => u.BeginApplicationTime).HasColumnName("BeginApplicationTime");
            Property(u => u.EndApplicationTime).HasColumnName("EndApplicationTime");
            Property(u => u.MinutesExam).HasColumnName("MinutesExam");
            Property(u => u.StudentGroupId).HasColumnName("StudentGroupId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.TeacherExamId).HasColumnName("TeacherExamId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");
            Property(u => u.ExamScheduleTypeId).HasColumnName("ExamScheduleTypeId");
            Property(u => u.Active).HasColumnName("Active");


            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);

        }
    }
}
