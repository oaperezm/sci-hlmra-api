﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Infraestructure.Configuration
{
    public class VCoursesConfig : EntityBaseConfiguration<VCourses>
    {
        public VCoursesConfig()
        {
            Property(u => u.Subject);
            Property(u => u.NodeId);
            Property(u => u.StartDate);
            Property(u => u.EndDate);
            Property(u => u.StudentCount);
            Property(u => u.Groups);
            Property(u => u.TeachId);

            ToTable("V_Courses");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.NodeId).HasColumnName("NodeId");
            Property(u => u.StartDate).HasColumnName("StartDate");
            Property(u => u.EndDate).HasColumnName("EndDate");
            Property(u => u.StudentCount).HasColumnName("StudentCount");
            Property(u => u.Groups).HasColumnName("Groups");
            Property(u => u.TeachId).HasColumnName("TeachId");

        }
    }
}
