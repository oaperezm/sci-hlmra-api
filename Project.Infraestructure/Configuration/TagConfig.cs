﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TagConfig: EntityBaseConfiguration<Tag>
    {
        public TagConfig() {

            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(300);

            ToTable("Tags");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            
        }
    }
}
