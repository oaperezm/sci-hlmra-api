﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class StudentGroupConfig : EntityBaseConfiguration<StudentGroup>
    {
        public StudentGroupConfig()
        {
            Property(u => u.Id)
                .IsRequired();
            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(250);
            Property(u => u.Status)
                .IsRequired();
            Property(u => u.CourseId)
                .IsRequired();
            Property(u => u.Code)
                .IsRequired();

            ToTable("StudentGroups");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Status).HasColumnName("Status");
            Property(u => u.CourseId).HasColumnName("CourseId");
            Property(u => u.Code).HasColumnName("Code");
        }

    }
}
