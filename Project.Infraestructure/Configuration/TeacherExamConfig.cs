﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TeacherExamConfig: EntityBaseConfiguration<TeacherExam>
    {
        public TeacherExamConfig() {

            Property(t => t.TeacherExamTypeId)
                .IsRequired();

            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(450);

            Property(t => t.UserId)
                .IsRequired();
            
            Property(t => t.Active)
                .IsRequired();

            Property(t => t.RegisterDate)
                .IsRequired();

            Property(t => t.UpdateDate)
                .IsOptional();

            Property(t => t.WeightingId)
               .IsRequired();

            Property(t => t.MaximumExamScore)
                .IsRequired();

            Property(t => t.IsAutomaticValue)
                .IsRequired();

            ToTable("TeacherExams");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.TeacherExamTypeId).HasColumnName("TeacherExamTypeId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");
            Property(u => u.WeightingId).HasColumnName("WeightingId");
            Property(u => u.MaximumExamScore).HasColumnName("MaximumExamScore");
            Property(u => u.IsAutomaticValue).HasColumnName("IsAutomaticValue");

            HasMany(x => x.TeacherExamQuestion)
                .WithRequired(x => x.TeacherExam)
                .WillCascadeOnDelete(true);
                
        }
    }
}
