﻿using Project.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VCoordinatorReportConfig : EntityBaseConfiguration<VCoordinatorReport>
    {
        public VCoordinatorReportConfig() 
        {

            ToTable("V_CoordinatorReport");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.cct).HasColumnName("cct");
            Property(u => u.email).HasColumnName("email");
            Property(u => u.userName).HasColumnName("userName");
            Property(u => u.nameSchool).HasColumnName("nameSchool");
            Property(u => u.level).HasColumnName("level");
            Property(u => u.grade).HasColumnName("grade");
            //Property(u => u.formativeField).HasColumnName("formativeField");
            Property(u => u.Subjet).HasColumnName("Subject");
            Property(u => u.course).HasColumnName("course");
            Property(u => u.group).HasColumnName("group");
            Property(u => u.totalStudents).HasColumnName("totalStudents");
            Property(u => u.totalActivities).HasColumnName("totalActivities");
            Property(u => u.totalScheduledExams).HasColumnName("totalScheduledExams");
            Property(u => u.totalQuestionsBanks).HasColumnName("totalQuestionsBanks");
            Property(u => u.totalQuestionTeacher).HasColumnName("totalQuestionTeacher");
            Property(u => u.totalQuestions).HasColumnName("totalQuestions");
            Property(u => u.startDate).HasColumnName("startDate");
            Property(u => u.endDate).HasColumnName("endDate");
            
    }
    }
}
