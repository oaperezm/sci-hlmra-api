﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class WeightingConfig : EntityBaseConfiguration<Weighting>
    {

        public WeightingConfig() {
            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Unit)
               .IsRequired()
               .HasMaxLength(10);

            Property(t => t.UserId)
             .IsRequired();

            Property(u => u.RegisterDate)
              .IsRequired();

            Property(u => u.UpdateDate)
             .IsOptional();

            ToTable("Weightings");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Unit).HasColumnName("Unit");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");

        }

    }
}
