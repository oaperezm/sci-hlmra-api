﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TeacherResourcesConfig: EntityBaseConfiguration<TeacherResources>
    {
        public TeacherResourcesConfig() {
            
            Property(c => c.Name)
               .IsRequired()
               .HasMaxLength(100);

            Property(c => c.Description)
             .IsOptional()
             .HasMaxLength(350);

            Property(c => c.Url)
           .IsRequired();

            Property(c => c.ResourceTypeId)
               .IsRequired();

            Property(c => c.UserId)
             .IsRequired();

            Property(c => c.RegisterDate)
           .IsRequired();
                            
            ToTable("TeacherResources");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.ResourceTypeId).HasColumnName("ResourceTypeId");
            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
        }
    }
}
