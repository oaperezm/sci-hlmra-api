﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class NodeContentConfig : EntityBaseConfiguration<NodeContent>
    {
        public NodeContentConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.ContentId)
                .IsRequired();

            Property(u => u.NodeId)
                .IsRequired();
            
            ToTable("NodeContents");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.ContentId).HasColumnName("ContentId");
            Property(u => u.NodeId).HasColumnName("NodeId");
            
        }

    }
}
