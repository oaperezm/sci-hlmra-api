﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VReportTeacherConfig : EntityBaseConfiguration<VReportTeacher>
    {
        public VReportTeacherConfig()
        {
            Property(u => u.Id);
            Property(u => u.TeacherId);
            Property(u => u.Teacher);
            Property(u => u.Student);
            Property(u => u.StudentEmail);
            Property(u => u.Institution);
            Property(u => u.StudentUserId);
            Property(u => u.StudentGroupDescription);
            Property(u => u.CourseId);
            Property(u => u.Subject);
            Property(u => u.Level);
            Property(u => u.DaysPassed);
            Property(u => u.totalAttendances);
            Property(u => u.PHomework);
            Property(u => u.PExam);
            Property(u => u.PAssistance);
            Property(u => u.PActivity);
            Property(u => u.CheckDate);
            Property(u => u.PHomework);
            Property(u => u.Grade);
            Property(u => u.CourseName);

            ToTable("V_ReportTeacher");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.TeacherId).HasColumnName("TeacherId");
            Property(u => u.Teacher).HasColumnName("Teacher");
            Property(u => u.Student).HasColumnName("Student");
            Property(u => u.StudentEmail).HasColumnName("StudentEmail");
            Property(u => u.Institution).HasColumnName("Institution");
            Property(u => u.StudentUserId).HasColumnName("StudentUserId");
            Property(u => u.StudentGroupDescription).HasColumnName("StudentGroupDescription");
            Property(u => u.CourseId).HasColumnName("CourseId");
            Property(u => u.Subject).HasColumnName("Subject");
            Property(u => u.Level).HasColumnName("Level");

            Property(u => u.DaysPassed).HasColumnName("DaysPassed");
            Property(u => u.totalAttendances).HasColumnName("totalAttendances");
            Property(u => u.PExam).HasColumnName("PExam");
            Property(u => u.PAssistance).HasColumnName("PAssistance");
            Property(u => u.PActivity).HasColumnName("PActivity");
            Property(u => u.CheckDate).HasColumnName("CheckDate");
            Property(u => u.PHomework).HasColumnName("PHomework");
            Property(u => u.Grade).HasColumnName("Grade");
            Property(u => u.CourseName).HasColumnName("CourseName");



        }
    }
}
