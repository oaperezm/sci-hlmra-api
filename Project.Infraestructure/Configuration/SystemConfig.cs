﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class SystemConfig: EntityBaseConfiguration<Entities.System>
    {
        public SystemConfig() {

            Property(u => u.Description)
               .IsRequired()
               .HasMaxLength(200);

            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Uri)
               .IsOptional();

            Property(u => u.RegisterDate)           
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();
            
            ToTable("Systems");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Uri).HasColumnName("Uri");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.Active).HasColumnName("Active");

        }
    }
}
