﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class HomeWorkConfig: EntityBaseConfiguration<HomeWork>
    {
        public HomeWorkConfig() {
          
            Property(u => u.ScheduledDate)
               .IsRequired();

            Property(u => u.Name)
             .IsRequired()
             .HasMaxLength(250);

            Property(c => c.Description)
             .IsRequired()
             .HasMaxLength(400);

            Property(c => c.RegisterDate)
               .IsRequired();

            Property(c => c.UserId)
              .IsRequired();

            Property(c => c.StudentGroupId)
                .IsRequired();

            Property(c => c.Qualified)
               .IsRequired();

            Property(c => c.OpeningDate)
               .IsOptional();

            Property(c => c.TypeOfQualification)
               .IsOptional();

            Property(c => c.AssignedQualification)
               .IsOptional();

            Property(c => c.SchedulingPartialId)
               .IsOptional();
            Property(c => c.TypeScore)
             .IsOptional();

            ToTable("HomeWorks");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.ScheduledDate).HasColumnName("ScheduledDate");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.StudentGroupId).HasColumnName("StudentGroupId");
            Property(c => c.Qualified).HasColumnName("Qualified");

            Property(c => c.OpeningDate).HasColumnName("OpeningDate");
            Property(c => c.TypeOfQualification).HasColumnName("TypeOfQualification");
            Property(c => c.AssignedQualification).HasColumnName("AssignedQualification");
            Property(c => c.SchedulingPartialId).HasColumnName("SchedulingPartialId");
            Property(c => c.TypeScore).HasColumnName("TypeScore");

            HasMany<TeacherResources>(r => r.Files)
             .WithMany(t => t.HomeWorks)
             .Map(rp => {
                 rp.MapLeftKey("HomeWorkId");
                 rp.MapRightKey("TeacherResourceId");
             });
                       
             HasRequired(s => s.User)
                  .WithMany()
                  .WillCascadeOnDelete(false);

            HasRequired(s => s.StudentGroup)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
