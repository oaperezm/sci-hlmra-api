﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class KeylearningConfig: EntityBaseConfiguration<Keylearning>
    {
        public KeylearningConfig()
        {
            Property(u => u.Description)
                .IsRequired();
            Property(u => u.Active)
                .IsOptional();
            Property(u => u.EducationLevelId)
                .IsOptional();

            ToTable("Keylearning");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.EducationLevelId).HasColumnName("EducationLevelId");

            HasRequired(u => u.EducationLevel)
           .WithMany()
           .WillCascadeOnDelete(false);

        }
    }
}
