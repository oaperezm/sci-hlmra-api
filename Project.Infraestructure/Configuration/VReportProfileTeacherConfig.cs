﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VReportProfileTeacherConfig : EntityBaseConfiguration<VReportProfileTeacher>
    {
        public VReportProfileTeacherConfig()
        {
            Property(u => u.Id);
            Property(u => u.FullName);
            Property(u => u.Email);
            Property(u => u.NameSchool);
            Property(u => u.Level);
            Property(u => u.Subject);
            Property(u => u.Groups);
            Property(u => u.ElapsedDays);
            Property(u => u.Period);

            Property(u => u.vpActivity);
            Property(u => u.totalSchedulesExam);
            Property(u => u.BaseQuestion);
            Property(u => u.GeneratedQuestion);

            Property(u => u.totalQuestion);
            Property(u => u.ActivityScheduled);

            Property(u => u.StartDate);
            Property(u => u.EndDate);
            Property(u => u.Grade);
            Property(u => u.PHomework);
            Property(u => u.PHomework);
            Property(u => u.PExam);
            Property(u => u.PAssistance);
            Property(u => u.PActivity);
            Property(u => u.userid);

            ToTable("V_ReportProfileTeacher");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.FullName).HasColumnName("FullName");
            Property(u => u.Email).HasColumnName("Email");
            Property(u => u.NameSchool).HasColumnName("NameSchool");
            Property(u => u.Level).HasColumnName("Level");
            Property(u => u.Subject).HasColumnName("Subject");
            Property(u => u.Groups).HasColumnName("Groups");
            Property(u => u.ElapsedDays).HasColumnName("ElapsedDays");
            Property(u => u.Period).HasColumnName("Period");

            Property(u => u.vpActivity).HasColumnName("vpActivity");
            Property(u => u.totalSchedulesExam).HasColumnName("totalSchedulesExam");
            Property(u => u.BaseQuestion).HasColumnName("BaseQuestion");
            Property(u => u.GeneratedQuestion).HasColumnName("GeneratedQuestion");

            Property(u => u.totalQuestion).HasColumnName("totalQuestion");
            Property(u => u.ActivityScheduled).HasColumnName("ActivityScheduled");
            Property(u => u.StartDate).HasColumnName("StartDate");
            Property(u => u.EndDate).HasColumnName("EndDate");
            Property(u => u.Grade).HasColumnName("Grade");
            Property(u => u.PExam).HasColumnName("PExam");
            Property(u => u.PAssistance).HasColumnName("PAssistance");
            Property(u => u.PActivity).HasColumnName("PActivity");
            Property(u => u.PHomework).HasColumnName("PHomework");
            Property(u => u.userid).HasColumnName("userid");

        }
    }
}
