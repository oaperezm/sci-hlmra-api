﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class SubsystemConfig : EntityBaseConfiguration<Subsystem>
    {
        public SubsystemConfig()
        {

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            ToTable("Subsystems");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
        }

    }
}
