﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class EventConfig : EntityBaseConfiguration<Events>
    {
        public EventConfig()
        {
            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(80);

            Property(u => u.Description)
                .IsOptional()
                .HasMaxLength(250);
            
            Property(u => u.Active)
                .IsOptional();
            ToTable("Events");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
         

          
        }
    }
}
