﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class NodeConfig : EntityBaseConfiguration<Node>
    {
        public NodeConfig()
        {
            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Description)
                .IsOptional()
                .HasMaxLength(2000);

            Property(u => u.UrlImage)
                .IsOptional()
                .HasMaxLength(500);

            Property(u => u.ParentId)
                .IsRequired();

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.NodeTypeId)
                .IsRequired();

            Property(u => u.BookId)
                .IsOptional();

            Property(u => u.Pathindex)
               .IsOptional();

            Property(u => u.Depth)
               .IsOptional();

            Property(u => u.Numericalmapping)
               .IsOptional();

            Property(u => u.SystemId)
            .IsOptional();

            Property(u => u.Public)
                .IsOptional();
            Property(u => u.Order)
                .IsOptional();

            ToTable("Nodes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.ParentId).HasColumnName("ParentId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.NodeTypeId).HasColumnName("NodeTypeId");
            Property(u => u.Pathindex).HasColumnName("Pathindex");
            Property(u => u.Depth).HasColumnName("Depth");
            Property(u => u.Numericalmapping).HasColumnName("Numericalmapping");
            Property(u => u.SystemId).HasColumnName("SystemId");
            Property(u => u.Public).HasColumnName("Public");
            Property(u => u.Order).HasColumnName("Sort");
        }

    }
}
