﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class TipConfig : EntityBaseConfiguration<Tip>
    {
        public TipConfig()
        {
            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(800);

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UrlImage)
                .IsOptional()
                .HasMaxLength(500);

            Property(u => u.Published)
                .IsRequired();

            Property(u => u.SystemId)
                .IsOptional();

            ToTable("Tips");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.Published).HasColumnName("Published");
            Property(u => u.SystemId).HasColumnName("SystemId");

        }
    }
}
