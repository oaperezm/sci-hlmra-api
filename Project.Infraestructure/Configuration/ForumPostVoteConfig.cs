﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
   public class ForumPostVoteConfig: EntityBaseConfiguration<ForumPostVote>
    {
        public ForumPostVoteConfig() {

            Property(x => x.ForumPostId)
                .IsRequired();

            Property(x => x.UserId)
                .IsRequired();

            Property(x => x.Like)
                .IsRequired();

            Property(x => x.RegisterDate)
                .IsOptional();

            ToTable("ForumPostVotes");

            Property(x => x.Id).HasColumnName("Id");
            Property(x => x.ForumPostId).HasColumnName("ForumPostId");
            Property(x => x.UserId).HasColumnName("UserId");
            Property(x => x.Like).HasColumnName("Like");
            Property(x => x.RegisterDate).HasColumnName("RegisterDate");
        }
    }
}
