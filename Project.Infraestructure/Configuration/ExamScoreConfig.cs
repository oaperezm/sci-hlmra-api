﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Project.Infraestructure.Configuration
{
    public class ExamScoreConfig: EntityBaseConfiguration<ExamScore>
    {
        public ExamScoreConfig() {

            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.ExamScheduleId)
                .IsRequired();

            Property(u => u.UserId)
                .IsRequired();

            Property(u => u.Score)
                .HasMaxLength(10)
                .IsOptional();

            Property(u => u.CorrectAnswers)
                .IsOptional();

            Property(u => u.Comments)
                .HasMaxLength(500)
                .IsOptional();

            Property(u => u.RegisterDate)
                .IsOptional();

            Property(u => u.UpdateDate)
                .IsOptional();

            Property(u => u.TestId)
                .IsOptional();
            
            ToTable("ExamScores");
            
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.ExamScheduleId).HasColumnName("ExamScheduleId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Score).HasColumnName("Score");
            Property(u => u.CorrectAnswers).HasColumnName("CorrectAnswers");
            Property(u => u.Comments).HasColumnName("Comments");
            Property(u => u.TestId).HasColumnName("TestId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");
            
            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);

        }
    }
}
