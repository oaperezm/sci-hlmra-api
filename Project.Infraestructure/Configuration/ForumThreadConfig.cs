﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ForumThreadConfig: EntityBaseConfiguration<ForumThread>
    {
        public ForumThreadConfig() {

            Property(x => x.UserId)
                .IsRequired();

            Property(x => x.Title)
                .IsRequired();

            Property(x => x.Description)
                .IsRequired();

            Property(x => x.InitDate)
                .IsOptional();

            Property(x => x.EndDate)
                .IsOptional();

            Property(x => x.CourseId)
                .IsRequired();

            Property(x => x.Active)
                .IsRequired();

            Property(x => x.Views)
             .IsOptional();

            ToTable("ForumThreads");

            Property(x => x.UserId).HasColumnName("UserId");
            Property(x => x.Title).HasColumnName("Title");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.InitDate).HasColumnName("InitDate");
            Property(x => x.EndDate).HasColumnName("EndDate");
            Property(x => x.CourseId).HasColumnName("CourseId");
            Property(x => x.Active).HasColumnName("Active");
            Property(x => x.Views).HasColumnName("Views");

            HasMany(g => g.Groups)
                .WithMany(s => s.Threads)                                
                .Map(gs =>
                {
                    gs.MapLeftKey("ForumThreadId");
                    gs.MapRightKey("StudentGroupId");
                });
                        
            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Course)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
