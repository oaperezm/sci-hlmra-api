﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class UserConfig: EntityBaseConfiguration<User>
    {
        public UserConfig()
        {

            Property(u => u.FullName)
                .IsRequired()
                .HasMaxLength(500);

            Property(u => u.Birthday)
                .IsRequired();

            Property(u => u.Gender)
                   .IsRequired();

            Property(u => u.StateId)
                   .IsOptional();                  

            Property(u => u.Email)
                   .IsRequired()
                   .HasMaxLength(250);

            Property(u => u.Password)
                .HasMaxLength(250)
                .IsRequired();

            Property(u => u.Key)
              .HasMaxLength(60)
              .IsOptional();

            Property(u => u.Institution)
                .HasMaxLength(200)
                .IsOptional();

            Property(u => u.RegisterDate)
                .IsOptional();

            Property(u => u.UpdateDate)
                .IsOptional();

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.RoleId)
                .IsRequired();

            Property(u => u.LevelId)
                .IsOptional();

            Property(u => u.AreaId)
                .IsOptional();

            Property(u => u.SubsystemId)
                .IsOptional();

            Property(u => u.Semester)
                .IsOptional();

            Property(u => u.UrlImage)
                .HasMaxLength(500)
                .IsOptional();

            Property(u => u.RefreshToken)
                .HasMaxLength(400)
                .IsOptional();

            Property(u => u.ClientId)
              .HasMaxLength(150)
              .IsOptional();

            Property(u => u.ProtectedTicket)                
                .IsOptional();

            Property(u => u.SystemId)
               .IsOptional();

            ToTable("Users");
            
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.FullName).HasColumnName("FullName");
            Property(u => u.Birthday).HasColumnName("Birthday");
            Property(u => u.StateId).HasColumnName("StateId");
            Property(u => u.Gender).HasColumnName("Gender");
            Property(u => u.Email).HasColumnName("Email");
            Property(u => u.Password).HasColumnName("Password");
            Property(u => u.Key).HasColumnName("Key");
            Property(u => u.Institution).HasColumnName("Institution");
            Property(u => u.RoleId).HasColumnName("RoleId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");            
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.LevelId).HasColumnName("LevelId");
            Property(u => u.AreaId).HasColumnName("AreaId");
            Property(u => u.SubsystemId).HasColumnName("SubsystemId");
            Property(u => u.Semester).HasColumnName("Semester");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.RefreshToken).HasColumnName("RefreshToken");
            Property(u => u.ClientId).HasColumnName("ClientId");
            Property(u => u.ProtectedTicket).HasColumnName("ProtectedTicket");
            Property(u => u.SystemId).HasColumnName("SystemId");
        }
    }
}
