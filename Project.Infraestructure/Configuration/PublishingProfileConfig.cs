﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class PublishingProfileConfig : EntityBaseConfiguration<PublishingProfile>
    {
        public PublishingProfileConfig()
        {
            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Active)
                .IsOptional();

            Property(u => u.SystemId)
             .IsOptional();

            ToTable("PublishingProfiles");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.SystemId).HasColumnName("SystemId");

            HasMany<Node>(r => r.Nodes)
                .WithMany(p => p.PublishingProfiles)
                .Map(rp => {
                    rp.MapLeftKey("PublishingProfileId");
                    rp.MapRightKey("NodeId");
                });
        }
    }
}
