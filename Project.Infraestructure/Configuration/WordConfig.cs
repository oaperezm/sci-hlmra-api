﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class WordConfig: EntityBaseConfiguration<Word>
    {

        public WordConfig() {

            Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(300);
            
            Property(u => u.Description)
                .IsRequired();

            Property(u => u.GlossaryId)
                .IsRequired();

            Property(u => u.RegisterDate)
              .IsRequired();

            Property(u => u.UpdateDate)
             .IsOptional();

            Property(u => u.Active)
              .IsRequired();

            ToTable("GlossaryWords");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.GlossaryId).HasColumnName("GlossaryId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");

        }

    }
}
