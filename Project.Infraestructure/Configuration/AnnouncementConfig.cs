﻿using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class AnnouncementConfig : EntityBaseConfiguration<Announcement>
    {
        public AnnouncementConfig()
        {

            Property(u => u.Title)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(350);

            Property(u => u.Active)
                .IsRequired();

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UpdateDate)
                .IsOptional();

            ToTable("Announcements");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Title).HasColumnName("Title");
            Property(u => u.Description).HasColumnName("Description");                    
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");            

            HasRequired(s => s.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            HasMany(s => s.StudentGroup)
                .WithMany(s => s.Announcements)
                .Map(rp => {
                    rp.MapLeftKey("AnnouncementId");
                    rp.MapRightKey("StudentGroupId");
                });
        }
    }
}

