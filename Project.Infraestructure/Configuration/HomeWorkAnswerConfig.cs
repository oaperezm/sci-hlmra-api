﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class HomeWorkAnswerConfig: EntityBaseConfiguration<HomeWorkAnswer>
    {
        public HomeWorkAnswerConfig() {

            Property(x => x.HomeWorkId)
                .IsRequired();

            Property(x => x.UserId)
                .IsRequired();

            Property(x => x.Score)
                .IsRequired()
                .HasMaxLength(5);

            Property(x => x.Comment)
                .IsOptional()
                .HasMaxLength(500);

            Property(x => x.RegisterDate)
                .IsOptional();

            Property(x => x.Delivered)
                .IsOptional();

            Property(x => x.AddNewAnswers)
                .IsOptional();

            Property(x => x.PartialEvaluation)
                .IsRequired();

            Property(x => x.StatusHomework)
                .IsRequired();

            ToTable("HomeWorkAnswers");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.HomeWorkId).HasColumnName("HomeWorkId");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Score).HasColumnName("Score");
            Property(u => u.Comment).HasColumnName("Comment");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");

            Property(u => u.Delivered).HasColumnName("Delivered");
            Property(u => u.AddNewAnswers).HasColumnName("AddNewAnswers");

            Property(u => u.PartialEvaluation).HasColumnName("PartialEvaluation");
            Property(u => u.StatusHomework).HasColumnName("StatusHomework");
            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);

        }
    }
}
