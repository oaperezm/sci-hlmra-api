﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class GlossaryConfig: EntityBaseConfiguration<Glossary>
    {
        public GlossaryConfig() {

            Property(u => u.Name)
              .IsRequired()
              .HasMaxLength(300);
            
            Property(u => u.Description)
              .IsRequired()
              .HasMaxLength(500);
            
            Property(u => u.Active)
              .IsRequired();

            Property(u => u.RegisterDate)
               .IsRequired();

            Property(u => u.UpdateDate)
               .IsOptional();

            Property(u => u.UserId)
                .IsRequired();

            ToTable("Glossaries");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Name).HasColumnName("Name");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UpdateDate).HasColumnName("UpdateDate");

            HasMany(x => x.Nodes)
                .WithMany(n => n.Glossaries)
                .Map(r =>
                {
                    r.ToTable("GlossaryNodes");
                    r.MapLeftKey("GlossaryId");
                    r.MapRightKey("NodeId");
                });
        }

    }
}
