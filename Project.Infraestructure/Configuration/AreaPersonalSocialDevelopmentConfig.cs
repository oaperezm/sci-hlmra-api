﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
   public  class AreaPersonalSocialDevelopmentConfig: EntityBaseConfiguration<AreaPersonalSocialDevelopment>
    {
        public AreaPersonalSocialDevelopmentConfig()
        {
            Property(u => u.Description)
                .IsRequired();
            Property(u => u.Active)
                .IsOptional();

            Property(u => u.EducationLevelId)
           .IsRequired();
            Property(u => u.FileTypeTypeId)
             .IsRequired();

            ToTable("AreaPersonalSocialDevelopment");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
            Property(u => u.EducationLevelId).HasColumnName("EducationLevelId");
            Property(u => u.FileTypeTypeId).HasColumnName("FileTypeTypeId");

            HasRequired(u => u.EducationLevel)
            .WithMany()
            .WillCascadeOnDelete(false);

            HasRequired(u => u.FileType)
            .WithMany()
            .WillCascadeOnDelete(false);

        }
    }
}
