﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class AttendanceConfig: EntityBaseConfiguration<Attendance>
    {
        public AttendanceConfig()
        {
            Property(a => a.StudentGroupId)
                .IsRequired();

            Property(a => a.UserId)
                .IsRequired();

            Property(a => a.AttendanceDate)
                .IsRequired();        

            Property(a => a.AttendanceClass)
             .IsRequired();

            Property(a => a.RegisterDate)
             .IsRequired();

            ToTable("Attendances");

            Property(a => a.StudentGroupId).HasColumnName("StudentGroupId");
            Property(a => a.UserId).HasColumnName("UserId");
            Property(a => a.AttendanceDate).HasColumnName("AttendanceDate");
            Property(a => a.AttendanceClass).HasColumnName("AttendanceClass");
            Property(a => a.RegisterDate).HasColumnName("RegisterDate");

            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            HasRequired(c => c.StudentGroup)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
