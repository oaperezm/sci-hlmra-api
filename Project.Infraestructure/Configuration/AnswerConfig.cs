﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class AnswerConfig : EntityBaseConfiguration<Answer>
    {
        public AnswerConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(500);

            Property(u => u.IsCorrect)
                .IsRequired();

            Property(u => u.Order)
                .IsRequired();

            Property(u => u.QuestionId)
                .IsRequired();
            
            Property(u => u.RelationDescription)
                .IsOptional()
                .HasMaxLength(500);

            Property(u => u.ImageWidth)
              .IsOptional();

            Property(u => u.ImageHeight)
              .IsOptional();

            ToTable("Answers");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.RelationDescription).HasColumnName("RelationDescription");
            Property(u => u.IsCorrect).HasColumnName("IsCorrect");
            Property(u => u.Order).HasColumnName("Order");
            Property(u => u.QuestionId).HasColumnName("QuestionId");
            Property(u => u.ImageWidth).HasColumnName("ImageWidth");
            Property(u => u.ImageHeight).HasColumnName("ImageHeight");
        }

    }
}
