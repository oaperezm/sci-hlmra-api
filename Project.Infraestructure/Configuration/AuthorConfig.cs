﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class AuthorConfig : EntityBaseConfiguration<Author>
    {
        public AuthorConfig()
        {

            Property(u => u.FullName)
                .IsRequired()
                .HasMaxLength(200);

            Property(u => u.LastName)
                .IsRequired()
                .HasMaxLength(200);

            Property(u => u.Comment)
             .IsOptional();

            ToTable("Authors");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.FullName).HasColumnName("FullName");
            Property(u => u.LastName).HasColumnName("LastName");
            Property(u => u.Comment).HasColumnName("Comment");
        }

    }
}
