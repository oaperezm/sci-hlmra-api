﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VReportCourseConfig: EntityBaseConfiguration<VReportCourse>
    {
        public VReportCourseConfig()
        {
            Property(u => u.Id);
            Property(u => u.cct);
            Property(u => u.email);
            Property(u => u.userName);
            Property(u => u.nameSchool);
            Property(u => u.level);    
            Property(u => u.courseRegisterDate);
            Property(u => u.courseId);
            Property(u => u.courseName);
            Property(u => u.group);
            Property(u => u.startDate);
            Property(u => u.endDate);

            ToTable("V_ReportCourse");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.cct).HasColumnName("cct");
            Property(u => u.email).HasColumnName("email");
            Property(u => u.userName).HasColumnName("userName");
            Property(u => u.nameSchool).HasColumnName("nameSchool");
            Property(u => u.level).HasColumnName("level");
            Property(u => u.courseRegisterDate).HasColumnName("courseRegisterDate");
            Property(u => u.courseId).HasColumnName("courseId");
            Property(u => u.courseName).HasColumnName("courseName");
            Property(u => u.group).HasColumnName("group");
            Property(u => u.startDate).HasColumnName("startDate");
            Property(u => u.endDate).HasColumnName("endDate");
        }
    }
}
