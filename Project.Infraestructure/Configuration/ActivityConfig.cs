﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ActivityConfig : EntityBaseConfiguration<Activity>
    {
        public ActivityConfig()
        { 
            Property(c => c.SchedulingPartialId)
                   .IsRequired();

            Property(u => u.Name)
                 .IsRequired()
                 .HasMaxLength(250);

            Property(c => c.Description)
                 .IsRequired()
                 .HasMaxLength(400);

            Property(c => c.OpeningDate)
                   .IsRequired();

            Property(u => u.ScheduledDate)
               .IsRequired();

            Property(c => c.RegisterDate)
                   .IsRequired();

            Property(c => c.UserId)
                  .IsRequired();

            Property(c => c.StudentGroupId)
                .IsRequired();

            Property(c => c.Qualified)
               .IsRequired();

            Property(c => c.TypeOfQualification)
               .IsOptional();

            Property(c => c.AssignedQualification)
               .IsOptional();
            Property(c => c.TypeScore)
            .IsOptional();

            ToTable("Activities");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.SchedulingPartialId).HasColumnName("SchedulingPartialId");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.OpeningDate).HasColumnName("OpeningDate");
            Property(c => c.ScheduledDate).HasColumnName("ScheduledDate");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.UserId).HasColumnName("UserId");
            Property(c => c.StudentGroupId).HasColumnName("StudentGroupId");

            Property(c => c.Qualified).HasColumnName("Qualified"); 
            Property(c => c.TypeOfQualification).HasColumnName("TypeOfQualification");
            Property(c => c.AssignedQualification).HasColumnName("AssignedQualification");
            Property(c => c.TypeScore).HasColumnName("TypeScore");

            HasMany<TeacherResources>(r => r.Files)
             .WithMany(t => t.Activities)
             .Map(rp => {
                 rp.MapLeftKey("ActivityId");
                 rp.MapRightKey("TeacherResourceId");
             });

            HasRequired(s => s.User)
                 .WithMany()
                 .WillCascadeOnDelete(false);
        }
    }
}
