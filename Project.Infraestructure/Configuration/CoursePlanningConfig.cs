﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Project.Infraestructure.Configuration
{
   public class CoursePlanningConfig : EntityBaseConfiguration<CoursePlanning>
    {
        public CoursePlanningConfig()
        {
            Property(u => u.Id)
               .IsRequired();

            Property(u => u.Homework)
             .IsOptional();

            Property(c => c.Exam)
             .IsRequired();

            Property(c => c.Activity)
               .IsOptional();

            Property(c => c.Assistance)
            .IsOptional();
            Property(c => c.NumberPartial)
             .IsOptional();
            Property(c => c.StudentGroupId)
                .IsRequired();

            ToTable("CoursePlanning");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Homework).HasColumnName("Homework");
            Property(c => c.Exam).HasColumnName("Exam");
            Property(c => c.Assistance).HasColumnName("Assistance");
            Property(c => c.Activity).HasColumnName("Activity");
            Property(c => c.NumberPartial).HasColumnName("NumberPartial");
            Property(c => c.StudentGroupId).HasColumnName("StudentGroupId");
            HasRequired(s => s.StudentGroup)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
