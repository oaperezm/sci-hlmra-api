﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class StatesVideoConfereceConfig : EntityBaseConfiguration<StatesVideoConference>
    {
        public StatesVideoConfereceConfig()
        {
            Property(v => v.Id)
             .IsRequired();

            Property(v => v.Description)
             .IsRequired();

            ToTable("StatesVideoConferece");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Description).HasColumnName("Description");
        
        }
    }
}
