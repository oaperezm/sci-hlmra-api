﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class FileTypeConfig : EntityBaseConfiguration<FileType>
    {
        public FileTypeConfig()
        {

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.SystemId)
                .IsOptional();


            ToTable("FileTypes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.SystemId).HasColumnName("SystemId");
        }

    }
}
