﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class GeneralEventConfig: EntityBaseConfiguration<GeneralEvent>
    {
        public GeneralEventConfig() {

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(250);

            Property(c => c.Description)
                 .IsRequired()
                 .HasMaxLength(500);
          
            Property(c => c.Place)
                .IsRequired()
                .HasMaxLength(300);

            Property(c => c.EventDate)
                .IsRequired();

            Property(c => c.Hours)
               .IsRequired()
               .HasMaxLength(5);

            Property(c => c.Published)
              .IsRequired();

            Property(c => c.RegisterDate)
              .IsRequired();

            Property(u => u.SystemId)
               .IsOptional();

            ToTable("GeneralEvents");

            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Name).HasColumnName("Name");
            Property(c => c.Description).HasColumnName("Description");
            Property(c => c.Place).HasColumnName("Place");
            Property(c => c.EventDate).HasColumnName("EventDate");
            Property(c => c.Hours).HasColumnName("Hours");
            Property(c => c.Published).HasColumnName("Published");
            Property(c => c.RegisterDate).HasColumnName("RegisterDate");
            Property(c => c.SystemId).HasColumnName("SystemId");

        }
    }
}
