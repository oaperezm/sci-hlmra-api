﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class VReportExamConfig: EntityBaseConfiguration<VReportExam>
    {
        public VReportExamConfig()
        {
            Property(u => u.Id);
            Property(u => u.cct);
            Property(u => u.level);
            Property(u => u.userName);
            Property(u => u.email);
            Property(u => u.nameSchool);
            Property(u => u.totalquestion);
            Property(u => u.questionbank);
            Property(u => u.signature);
            Property(u => u.starDate);
            //Property(u => u.endDate);
            

            ToTable("V_ReportExam");
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.cct).HasColumnName("cct");
            
            Property(u => u.userName).HasColumnName("userName");
            Property(u => u.email).HasColumnName("email");
            Property(u => u.nameSchool).HasColumnName("nameSchool");
            Property(u => u.level).HasColumnName("level");
            Property(u => u.totalquestion).HasColumnName("totalquestion");
            Property(u => u.questionbank).HasColumnName("questionbank");
            Property(u => u.signature).HasColumnName("signature");
            Property(u => u.starDate).HasColumnName("starDate");
            //Property(u => u.endDate).HasColumnName("endDate");
            }
    }
}
