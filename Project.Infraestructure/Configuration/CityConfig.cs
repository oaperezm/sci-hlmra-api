﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class CityConfig: EntityBaseConfiguration<City>
    {
        public CityConfig() {


            Property(s => s.Description)
               .IsRequired()
               .HasMaxLength(350);

           
            ToTable("Cities");

            Property(s => s.Id).HasColumnName("Id");
            Property(s => s.Description).HasColumnName("Description");
          

        }
    }
}
