﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class NewsConfig : EntityBaseConfiguration<News>
    {
        public NewsConfig()
        {
            Property(u => u.Title)
                    .IsRequired()
                    .HasMaxLength(100);

            Property(u => u.Description)
                .IsRequired();                

            Property(u => u.RegisterDate)
                .IsRequired();

            Property(u => u.UrlImage)
                .IsOptional()
                .HasMaxLength(500);
            
            Property(u => u.Published)
                .IsOptional();

            Property(u => u.SystemId)
             .IsOptional();

            ToTable("News");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Title).HasColumnName("Title");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
            Property(u => u.UrlImage).HasColumnName("UrlImage");
            Property(u => u.Published).HasColumnName("Published");
            Property(u => u.SystemId).HasColumnName("SystemId");

        }
    }
}
