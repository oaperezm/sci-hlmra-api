﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class ForumPostConfig:EntityBaseConfiguration<ForumPost>
    {
        public ForumPostConfig() {

            Property(x => x.ForumThreadId)
               .IsRequired();

            Property(x => x.UserId)
               .IsRequired();

            Property(x => x.RegisterDate)
               .IsRequired();

            Property(x => x.Title)
               .IsRequired()
               .HasMaxLength(350);

            Property(x => x.Description)
               .IsRequired()
               .HasMaxLength(1000); 
            
            Property(x => x.Replay)
               .IsOptional()
               .HasMaxLength(1000);

            Property(x => x.BestAnswer)
               .IsOptional();

            Property(x => x.Votes)                
               .IsOptional();

            ToTable("ForumPosts");

            Property(x => x.ForumThreadId).HasColumnName("ForumThreadId");
            Property(x => x.UserId).HasColumnName("UserId");
            Property(x => x.RegisterDate).HasColumnName("RegisterDate");
            Property(x => x.Title).HasColumnName("Title");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.Replay).HasColumnName("Replay");
            Property(x => x.BestAnswer).HasColumnName("BestAnswer");
            Property(x => x.Votes).HasColumnName("Votes");

            HasRequired(x => x.User)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
