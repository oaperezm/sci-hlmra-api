﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Infraestructure.Configuration
{
    public class QuestionTypeConfig : EntityBaseConfiguration<QuestionType>
    {
        public QuestionTypeConfig()
        {
            Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(350);

            Property(u => u.Active)
                .IsRequired();

            ToTable("QuestionTypes");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
            Property(u => u.Active).HasColumnName("Active");
        }

    }
}
