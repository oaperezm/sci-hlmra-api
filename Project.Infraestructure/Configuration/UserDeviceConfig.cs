﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class UserDeviceConfig: EntityBaseConfiguration<UserDevice>
    {
        public UserDeviceConfig() {

            Property(u => u.ClientId)
                    .IsRequired();

            Property(u => u.DeviceId)
                    .IsOptional();

            Property(u => u.TokenDevice)
                    .IsRequired()
                    .HasMaxLength(500);

            Property(u => u.UserId)
                    .IsRequired();

            Property(u => u.RegisterDate)
                    .IsRequired();

            ToTable("UserDevices");
            
            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.ClientId).HasColumnName("ClientId");
            Property(u => u.TokenDevice).HasColumnName("TokenDevice");
            Property(u => u.UserId).HasColumnName("UserId");
            Property(u => u.RegisterDate).HasColumnName("RegisterDate");
        }
    }
}
