﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;

namespace Project.Infraestructure.Configuration
{
    public class TrainingFieldConfig : EntityBaseConfiguration<TrainingField>
    {
        public TrainingFieldConfig()
        {

            Property(u => u.Description)
                .IsRequired()
                .HasMaxLength(100);

            ToTable("TrainingFields");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.Description).HasColumnName("Description");
        }

    }
}
