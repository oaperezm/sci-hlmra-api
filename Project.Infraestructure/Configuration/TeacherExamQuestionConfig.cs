﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Configuration
{
    public class TeacherExamQuestionConfig: EntityBaseConfiguration<TeacherExamQuestion>
    {
        public TeacherExamQuestionConfig() {

            Property(t => t.TeacherExamId)
                .IsRequired();

            Property(t => t.QuestionId)
                .IsRequired();

            Property(t => t.Value)
                .HasPrecision(5,2)
                .IsRequired();

            ToTable("TeacherExamQuestions");

            Property(u => u.Id).HasColumnName("Id");
            Property(u => u.TeacherExamId).HasColumnName("TeacherExamId");
            Property(u => u.QuestionId).HasColumnName("QuestionId");
            Property(u => u.Value).HasColumnName("Value");

        }
    }
}
