﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities.Utils;
using System.Web;
using System.Configuration;
using System.IO;
using System.Drawing;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Project.Repositories.Services;

namespace Project.Infraestructure.Services
{
    public class FileUploaderService : IFileUploader
    {
        /// <summary>
        /// Loads an image to azure's blobstorage service
        /// </summary>
        /// <param name="fileArray"></param>
        /// <param name="fileName"></param>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public async Task<string> FileUpload(byte[] fileArray, string fileName, string containerName)
        {
            String url = "";
            var downloadUrl = "";
            try
            {
                System.Text.StringBuilder errores = new System.Text.StringBuilder();
                
                //int AllowedFileSize = 52428800;
                if (fileArray != null)
                {
                    //var fileFotoC = fileFoto.FirstOrDefault();

                    //if (fileArray.Length > AllowedFileSize)
                    //    errores.Append("El archivo " + fileName + " excede el tamaño permitido de " + AllowedFileSize.ToString("##,###,###") + ".");
                    
                    if (errores.Length == 0)
                    {
                        //Se carga la foto en el contenedor de azure.
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                        CloudBlobClient blobClient         = storageAccount.CreateCloudBlobClient();
                        CloudBlobContainer container       = blobClient.GetContainerReference(containerName);

                        container.CreateIfNotExists();
                        url = $"{ Guid.NewGuid().ToString() }.{ fileName.Split('.').Last() }";

                        if (fileArray != null && fileArray.Length > 0)
                        {
                            String nombre            = url;
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(nombre);
                            
                            await blockBlob.UploadFromByteArrayAsync(fileArray, 0, fileArray.Length);

                            // Allow file download access for 25 years
                            var sasToken = blockBlob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
                            {
                                Permissions            = SharedAccessBlobPermissions.Read,
                                SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddYears(25),
                            }, new SharedAccessBlobHeaders()
                            {
                                ContentDisposition = "attachment; filename=\"" + url + "\"",
                            });

                            downloadUrl = string.Format("{0}{1}", blockBlob.Uri.AbsoluteUri, sasToken);


                            //TODO Realizar validación por tipo de archivo para realizar la miniatura de la foto
                            /*

                            System.Drawing.Image miniatura;
                            CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(nombre + "_miniatura");
                            using (var nmemoryStream = new MemoryStream())
                            {
                                nmemoryStream.Write(fileArray, 0, fileArray.Length);
                                miniatura = GetThumbnailImage(new Bitmap(nmemoryStream));
                            }
                            using (var nmemoryStream = new MemoryStream())
                            {
                                miniatura.Save(nmemoryStream, GetImageFormat(fileName.Split('.').Last()));
                                blockBlob1.UploadFromByteArray(nmemoryStream.ToArray(), 0, Convert.ToInt32(nmemoryStream.Length));
                            }
                            */
                        }
                    }
                }
                else
                    return "";


                
            }
            catch (Exception ex)
            {
                return "";               
            }

            return downloadUrl;
        }

        /// <summary>
        /// Eliminates file from blobstorage
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public async Task<bool> FileRemove(string filename, string containerName)
        {
            bool Eliminado = false;

            try
            {
                string urlSimple = HttpUtility.UrlDecode(filename).Split('?').First();
                string fname = urlSimple.Split('/').Last();
                //string fname = HttpUtility.ParseQueryString(queryString).Get("filename");

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                //Se borra el archivo de la foto original.
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fname);
                await blockBlob.DeleteAsync();

                //Se borra el archivo de la foto miniatura.
                CloudBlockBlob blockBlob2 = container.GetBlockBlobReference(filename + "_miniatura");
                await blockBlob2.DeleteIfExistsAsync();
                Eliminado = true;
            }
            catch
            {
                return Eliminado;
            }
            return Eliminado;
        }


        public async Task<MemoryStream> FileDownload(string filename, string containerName)
        {
            MemoryStream Meme = new MemoryStream();

            try
            {
                //string urlSimple = HttpUtility.UrlDecode(filename).Split('?').First();
                //string fname = urlSimple.Split('/').Last();

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Download resourse from azure to memoryStream
                
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                await blockBlob.DownloadToStreamAsync(Meme);
                Meme.Seek(0, 0);
            }
            catch(Exception e)
            {
                return new MemoryStream();
            }
            return Meme;
        }

        private Image GetThumbnailImage(Image image)
        {
            return image.GetThumbnailImage(120, 120, null, new IntPtr());
        }

        private System.Drawing.Imaging.ImageFormat GetImageFormat(string extensionArchivo)
        {
            switch (extensionArchivo)
            {
                case "jpeg": return System.Drawing.Imaging.ImageFormat.Jpeg;
                case "jpg": return System.Drawing.Imaging.ImageFormat.Jpeg;
                case "png": return System.Drawing.Imaging.ImageFormat.Png;
            }
            return null;
        }
    }
}
