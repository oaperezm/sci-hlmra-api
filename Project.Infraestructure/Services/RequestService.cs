﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Project.Repositories.Services;

namespace Project.Infraestructure.Services
{
    public class RequestService : IRequestService
    {
        /// <summary>
        /// Método para realizar peticiones GET a servicios REST
        /// </summary>
        /// <typeparam name="TU"></typeparam>
        /// <param name="method"></param>
        /// <param name="endPoint"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<TU> Get<TU>(string method, string endPoint, string parameters)
        {
            try
            {
                using (var client = new HttpClient { MaxResponseContentBufferSize = 2147483647, Timeout = TimeSpan.FromSeconds(60) })
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("x-api-key", System.Configuration.ConfigurationManager.AppSettings["Api-key"]);

                    var response = await client.GetAsync($"{endPoint}/{method}/{parameters}");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<TU>(content);
                    }
                    else
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
                            throw new HttpRequestException($"Problemas al conectarse con el servidor de servicios: {(int)response.StatusCode} - {response.StatusCode}");
                        else
                            throw new Exception(response.ReasonPhrase);
                    }
                }

            }
            catch (System.Net.Http.HttpRequestException e)
            {
                throw new System.Net.Http.HttpRequestException(e.Message);
            }
            catch (Exception ex)
            {
                return default(TU);
            }
        }
    }
}
