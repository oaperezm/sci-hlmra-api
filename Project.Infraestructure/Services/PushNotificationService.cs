﻿using Newtonsoft.Json;
using Project.Entities.Enums;
using Project.Entities.Push;
using Project.Infraestructure.Repositories.Management;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Services
{
    public class PushNotificationService : IPushNotificationService
    {
        private TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        private readonly Uri FireBasePushNotificationsURL;
        private readonly string FireBaseURLTipics;
        private readonly string ServerKey;
        private readonly INotificationMessagesRepository _notificationRepository;
        private readonly IUserDeviceRepository _userDeviceRepository;
        private readonly IUserNotificationRepository _userNotificationRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PushNotificationService(INotificationMessagesRepository notificationRepository,
                                       IUserNotificationRepository userNotificationRepository,
                                       IUserDeviceRepository userDeviceRepository,
                                       IUnitOfWork unitOfWor)
        {
            FireBasePushNotificationsURL = new Uri(ConfigurationManager.AppSettings["Firebase.URL"]);
            ServerKey                    = ConfigurationManager.AppSettings["Firebase.ServerKey"];
            FireBaseURLTipics        = ConfigurationManager.AppSettings["Firebase.URL.TOPICS"];
            _notificationRepository      = notificationRepository;
            _userDeviceRepository        = userDeviceRepository;
            _userNotificationRepository  = userNotificationRepository;
            _unitOfWork                  = unitOfWor;
        }


        /// <summary>
        /// Crear una notificación para un topico
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="pushConfigId"></param>
        /// <returns></returns>
        public async Task<bool> CreateNotification(string topic, int notificationMessageId, object data) {
            
            var message = await this.GetMessageConfig(notificationMessageId, data);
            var body    = await this.CreateBody(topic, message);

            return await this.SendNotification(body, new List<int>(), notificationMessageId);
        }

        /// <summary>
        /// Crear una notificación para usuarios
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="pushConfigId"></param>
        /// <returns></returns>
        public async Task<bool> CreateNotification(List<int> userIds, int notificationMessageId, object data)
        {
            var message = await this.GetMessageConfig(notificationMessageId, data);
            var body    = await this.CreateBody(userIds, message);

            return await this.SendNotification(body, userIds, notificationMessageId);            
        }

        public async Task<bool> RegisterTopic(string deviceId, string topic)
        {

            var response = false;

            try
            {
                var uri = string.Format(FireBaseURLTipics, deviceId, topic);

                var request = new HttpRequestMessage(HttpMethod.Post, uri);

                request.Headers.TryAddWithoutValidation("Authorization", "key=" + ServerKey);
                request.Content = new StringContent("", Encoding.UTF8, "application/json");

                HttpResponseMessage result;
                using (var client = new HttpClient())
                {
                    result = await client.SendAsync(request);
                    var content = await result.Content.ReadAsStringAsync();
                    response = result.IsSuccessStatusCode;
                }
            }
            catch (Exception)
            {
            }

            return response;
        }


        #region PRIVATE METHODS

        /// <summary>
        /// Enviar notificación push con los datos proporcionados
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private async Task<bool> SendNotification(Message message, List<int> userIds, int notificationMessageId) {

            bool sent = false;

            try
            {                
                string jsonMessage = JsonConvert.SerializeObject(message);
                var request        = new HttpRequestMessage(HttpMethod.Post, FireBasePushNotificationsURL);

                request.Headers.TryAddWithoutValidation("Authorization", "key=" + ServerKey);
                request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

                HttpResponseMessage result;
                using (var client = new HttpClient())
                {
                    result      = await client.SendAsync(request);
                    var content = await result.Content.ReadAsStringAsync();
                    sent        = result.IsSuccessStatusCode;

                    if (sent) {

                        try
                        {
                            userIds.ForEach(id =>
                            {
                                var notification = new Entities.UserNotification
                                {
                                    Message            = message.Notification.Text,
                                    Read               = false,
                                    RegisterDate       = DateTime.UtcNow.AddHours(offset.Hours),
                                    UserId             = id,
                                    Title              = message.Notification.Title,
                                    NotificationTypeId = notificationMessageId
                                };
                                _userNotificationRepository.Add(notification);
                            });
                           
                            await _unitOfWork.SaveChangesAsync();
                        }
                        catch (Exception)
                        {                            
                        }                        
                    }
                }
            }
            catch (Exception)
            {
                
            }

            return sent;


        }
        
        /// <summary>
        /// Obtener la configuración de la notificación
        /// </summary>
        /// <param name="notificationMessageId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private async Task<NotificationBody> GetMessageConfig(int notificationMessageId, object data)
        {

            var config = await _notificationRepository.FindAsync(x => x.Id == notificationMessageId);
            var message = "";

            switch (notificationMessageId)
            {
                case (int)EnumPush.NewActivity:
                case (int)EnumPush.NewScheduledCourse:
                case (int)EnumPush.NewFinalScore:
                // CONFIGURACION NUEVA PROGRAMACIÓN DE EXAMEN
                // CONFIGURACION NUEVA TAREA
                case (int)EnumPush.NewScheduleExamn:              
                case (int)EnumPush.NewScheduleHomeWork:
                    System.Reflection.PropertyInfo name = data.GetType().GetProperty("groupName");
                    System.Reflection.PropertyInfo date = data.GetType().GetProperty("date");
                    System.Reflection.PropertyInfo descripcion = data.GetType().GetProperty("homeWork");

                    message = string.Format(config.Message,
                                                   (string)(name.GetValue(data, null)), 
                                                   (string)(descripcion.GetValue(data, null)), 
                                                   (string)(date.GetValue(data, null)));
                    break;
                // CONFIGURACION PARA UN NUEVO EVENTO
                case (int)EnumPush.NewScheduleEvent:
                    System.Reflection.PropertyInfo courseName = data.GetType().GetProperty("courseName");
                    System.Reflection.PropertyInfo dateEvent  = data.GetType().GetProperty("dateEvent");
                    System.Reflection.PropertyInfo eventName  = data.GetType().GetProperty("eventName");

                    message = string.Format(config.Message,
                                                  (string)(courseName.GetValue(data, null)),
                                                  (string)(eventName.GetValue(data, null)),
                                                  (string)(dateEvent.GetValue(data, null)));

                    break;

                // CONFIGURACION PARA UN NUEVO EVENTO
                case (int)EnumPush.NewGlosarry:
                    System.Reflection.PropertyInfo courseGlossary = data.GetType().GetProperty("courseName");
                    System.Reflection.PropertyInfo glossaryName   = data.GetType().GetProperty("glossaryName");

                    message = string.Format(config.Message,
                                                  (string)(courseGlossary.GetValue(data, null)),
                                                  (string)(glossaryName.GetValue(data, null)));

                    break;

                case (int)EnumPush.NewAnnouncement:
                    System.Reflection.PropertyInfo announcementName = data.GetType().GetProperty("announcementName");
                    System.Reflection.PropertyInfo announcementDate = data.GetType().GetProperty("announcementDate");

                    message = string.Format(config.Message,
                                                  (string)(announcementName.GetValue(data, null)),
                                                  (string)(announcementDate.GetValue(data, null)));

                    break;

                // CONFIGURACION PARA NUEVO MENSAJE DIRECTO
                case (int)EnumPush.NewMessage:
                    System.Reflection.PropertyInfo sender = data.GetType().GetProperty("sender");
                    message = string.Format(config.Message, (string)(sender.GetValue(data, null)));
                    break;

            }

            return new NotificationBody
            {
                Title = config.Title,
                Body  = message,
                Data  = data,
            };
        }

        /// <summary>
        /// Crear el cuerpo de la notificación push directos
        /// </summary>
        /// <param name="deviceTokens"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private async Task<Message> CreateBody(List<int> userIds, NotificationBody message)
        {

            var userDevices   = await _userDeviceRepository.FindAllAsync(x => userIds.Contains(x.UserId));
            var deviceTokens = userDevices.Select(x => x.TokenDevice).ToArray();

            return new MessageDirect()
            {
                Notification = new Notification()
                {
                    Title = message.Title,
                    Text = message.Body,
                    Sound = "default"
                },
                Data = message.Data,
                RegistrationIds = deviceTokens
            };
        }

        /// <summary>
        /// Crear el cuerpo de la notificación para un topico
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private async Task<Message> CreateBody(string topic, NotificationBody message)
        {
            return new MessageTopic()
            {
                Notification = new Notification()
                {
                    Title = message.Title,
                    Text  = message.Body,
                    Sound = "default"
                },
                Data = message.Data,
                To   = $"/topics/{topic}"
            };
        }
        
        #endregion

    }
}
