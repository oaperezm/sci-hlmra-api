﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Configuration;
using Project.Repositories.Services;
using Project.Entities.Utils;
using System.Configuration;
using Project.Entities.Enums;
using System.Web;

namespace Project.Infraestructure.Services
{
    public class MailerService : IMailer
    {
        public MailerService()
        {

        }

        public async Task SendMail(Message message)
        {
            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            //Body = Body.Replace("#Mensaje#", Mensaje);

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.To, message.Subject, altView, message.SystemName);
            
        }

        /// <summary>
        /// Enviar correo cuando se registra un nuevo usuario
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendNewUserMail(WelcomeMessage message)
        {
            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            //string uriConfirmation = domainName + "/api/users/confirmSuscripcion?email=" + message.UserId;
            string uriConfirmation = "https://sistema.hlmra.com/#/login/" + message.UserId;
       
            Body = Body.Replace("#UserName#", message.UserName);
            Body = Body.Replace("#urlSistema#", message.SystemUrl);
            Body = Body.Replace("#Email#", message.Email);
            Body = Body.Replace("#Password#", message.Password);
            Body = Body.Replace("#linkSuscripcion#", uriConfirmation);

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.To, message.Subject, altView, message.SystemName);
        }

        /// <summary>
        /// Enviar correo para inicializar password
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendPwdResetMail(PwdResetMessage message)
        {
            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            Body = Body.Replace("#UserName#", message.UserName);
            Body = Body.Replace("#NewPassword#", message.NewPassword);
            Body = Body.Replace("#urlSistema#", message.SystemUrl);

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.To, message.Subject, altView, message.SystemName);

        }

        /// <summary>
        /// Enviar invitación para unirse al grupo
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendGroupInvitation(InvitationMessage message)
        {
            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            Body = Body.Replace("#Message#", message.Body);
            Body = Body.Replace("#UserName#", message.UserName);
            Body = Body.Replace("#CourseName#", message.CourseName);
            Body = Body.Replace("#CourseLink#", message.CourseLink);
            Body = Body.Replace("#urlSistema#", message.SystemUrl);

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.To, message.Subject, altView, message.SystemName);
        }

        /// <summary>
        /// Enviar correos de contacto 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendContactMail(ContactMessage message)
        {

            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            Body = Body.Replace("#fullName#", message.FullName);
            Body = Body.Replace("#email#", message.To);
            Body = Body.Replace("#subject#", message.Subject);
            Body = Body.Replace("#message#", message.Message);

            if (message.SystemId == (int)EnumSystems.RA) {
                Body = Body.Replace("#school#", message.School);
                Body = Body.Replace("#workplace#", message.WorkPlace);
            }            

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.From, message.Subject, altView, message.SystemName);

        }

        /// <summary>
        /// Enviar correo al cambiar el password
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendPwdChangeMail(PwdChangeMessage message)
        {
            string ServerPath = ConfigurationManager.AppSettings["ServerPath"].ToString();

            //Se obtiene el html de la plantilla de correo y se insertan los datos del usuario.
            string Body = System.IO.File.ReadAllText(ServerPath + "/content/mailtemplates/" + message.FullTemplate);

            Body = Body.Replace("#UserName#", message.UserName);
            Body = Body.Replace("#urlSistema#", message.SystemUrl);

            //Se insertan las imagenes al correo.
            AlternateView altView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);

            await SMTPSend(message.To, message.Subject, altView, message.SystemName);

        }

        /// <summary>
        /// Function que realiza el envio del correo
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="mailBody"></param>
        /// <returns></returns>
        public async Task SMTPSend(string to, string subject, AlternateView mailBody , string systemName)
        {


            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            var email = new System.Net.Mail.MailMessage(new MailAddress(smtpSection.Network.UserName, systemName), new MailAddress(to, systemName));

            try
            {
                email.AlternateViews.Add(mailBody);
                // email.To.Add(new MailAddress(to));

                email.From     = new MailAddress(smtpSection.Network.UserName, systemName);
                email.Subject  = subject;
                email.Priority = MailPriority.Normal;
                                

                if (smtpSection != null)
                {
                    int port    = smtpSection.Network.Port;
                    string host = smtpSection.Network.Host;
                    string pwd  = smtpSection.Network.Password;
                    string uid  = smtpSection.Network.UserName;

                    SmtpClient smtp = new SmtpClient
                    {
                        Host        = host,
                        Port        = port,
                        Credentials = new NetworkCredential(uid, pwd),
                        EnableSsl   = smtpSection.Network.EnableSsl
                    };

                    await smtp.SendMailAsync(email);
                    email.Dispose();
                }
            }
            catch (Exception ex) {
                email.Dispose();
            }

        }
    }
}
