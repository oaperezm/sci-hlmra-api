﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Data
{
    public interface IDbFactory: IDisposable
    {
        SailContext Init();
    }
}
