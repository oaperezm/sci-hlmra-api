﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Data
{
    public class DbFactory : Disposable, IDbFactory
    {
        SailContext _dbContext;

        public SailContext Init()
        {
            return _dbContext ?? (_dbContext = new SailContext());
        }

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }
}
