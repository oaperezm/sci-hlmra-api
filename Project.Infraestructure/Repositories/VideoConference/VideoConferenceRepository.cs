﻿
using Project.Infraestructure.Data;
using Project.Repositories.Courses;

using Project.Entities;

namespace Project.Infraestructure.Repositories.VideoConference
{
   public class VideoConferenceRepository : RepositoryBase<Entities.VideoConference>, IVideoConferenceRepository
    {

        public VideoConferenceRepository(IDbFactory dbFactory)
   :        base(dbFactory) { }
    }
}
