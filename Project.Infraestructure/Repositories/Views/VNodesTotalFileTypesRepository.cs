﻿using Project.Entities.Views;
using Project.Infraestructure.Data;
using Project.Repositories.Views;

namespace Project.Infraestructure.Repositories.Views
{
    public class VNodesTotalFileTypesRepository: RepositoryBase<VNodesTotalFileTypes>, IVNodesTotalFileTypesRepository
    {
        public VNodesTotalFileTypesRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
