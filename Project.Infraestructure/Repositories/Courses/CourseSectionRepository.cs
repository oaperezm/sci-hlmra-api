﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Repositories.Courses
{
    public class CourseSectionRepository : RepositoryBase<CourseSection>, ICourseSectionRepository
    {
        public CourseSectionRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }
}
