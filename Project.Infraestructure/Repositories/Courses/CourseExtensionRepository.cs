﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Courses;

namespace Project.Infraestructure.Repositories.Courses
{
    public class CourseExtensionRepository : RepositoryBase<CourseExtension>, ICourseExtensionRepository
    {
        public CourseExtensionRepository(IDbFactory dbFactory)
          : base(dbFactory) { }
    }
}
