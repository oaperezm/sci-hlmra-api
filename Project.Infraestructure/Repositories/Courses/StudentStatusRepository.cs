﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Courses
{
    public class StudentStatusRepository : RepositoryBase<StudentStatus>, IStudentStatusRepository
    {
        public StudentStatusRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }
}
