﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Courses;

namespace Project.Infraestructure.Repositories.Courses
{
    public class AnnouncementRepository : RepositoryBase<Announcement>, IAnnouncementRepository
    {
        public AnnouncementRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }

}
