﻿using Project.Entities.Views;
using Project.Infraestructure.Data;
using Project.Repositories.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Repositories.Views
{
    public class VCoordinatorReportRepository : RepositoryBase<VCoordinatorReport>, IVCoordinatorReportRepository
    {
        public VCoordinatorReportRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }
}
