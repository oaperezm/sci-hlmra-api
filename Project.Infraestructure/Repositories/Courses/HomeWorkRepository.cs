﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Repositories.Courses
{
    public class HomeWorkRepository: RepositoryBase<HomeWork> , IHomeWorkRepository
    {
        public HomeWorkRepository(IDbFactory dbFactory) 
            : base(dbFactory) { }
    }
}
