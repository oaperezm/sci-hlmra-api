﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Management
{
    public class LocationRepository : RepositoryBase<Location>, ILocationRepository
    {
        public LocationRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }
}
