﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Management
{
    public class FileTypeRepository : RepositoryBase<FileType>, IFileTypeRepository
    {
        public FileTypeRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }
}
