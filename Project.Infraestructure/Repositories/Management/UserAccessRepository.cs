﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Repositories.Management
{
    public class UserAccessRepository : RepositoryBase<UserAccess>, IUserAccessRepository
    {
        public UserAccessRepository(IDbFactory dbFactory)
          : base(dbFactory) { }
    }
}
