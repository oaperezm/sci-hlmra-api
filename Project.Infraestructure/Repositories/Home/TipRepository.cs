﻿using Project.Entities;
using Project.Infraestructure.Data;
using Project.Repositories.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure.Repositories.Home
{
    public class TipRepository : RepositoryBase<Tip>, ITipRepository
    {
        public TipRepository(IDbFactory dbFactory)
           : base(dbFactory) { }
    }

}
