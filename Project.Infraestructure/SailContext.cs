﻿using Project.Entities;
using Project.Entities.Views;
using Project.Infraestructure.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Infraestructure
{
    public class SailContext: DbContext
    {

        public SailContext() : base("SAILConecction")
        {
            Database.SetInitializer<SailContext>(null);

            Configuration.LazyLoadingEnabled   = true;
            Configuration.ProxyCreationEnabled = true;
        }

        public IDbSet<User> User                                     { get; set; }
        public IDbSet<Role> Role                                     { get; set; }
        public IDbSet<Permission> Permission                         { get; set; }

        public IDbSet<Area> Area                                     { get; set; }
        public IDbSet<Author> Author                                 { get; set; }
        public IDbSet<Block> Block                                   { get; set; }
        public IDbSet<Book> Book                                     { get; set; }              
        public IDbSet<Content> Content                               { get; set; }
        public IDbSet<ContentType> ContentType                       { get; set; }
        public IDbSet<FileType> FileType                             { get; set; }
        public IDbSet<FormativeField> FormativeField                 { get; set; }
        public IDbSet<Grade> Grade                                   { get; set; }
        public IDbSet<Level> Level                                   { get; set; }
        public IDbSet<Location> Location                             { get; set; }
        public IDbSet<Node> Node                                     { get; set; }
        public IDbSet<NodeContent> NodeContent                       { get; set; }
        public IDbSet<NodeType> NodeType                             { get; set; }
        public IDbSet<Purpose> Purpose                               { get; set; }
        public IDbSet<Subsystem> Subsystem                           { get; set; }
        public IDbSet<TrainingField> TrainingField                   { get; set; }

        public IDbSet<Course> Course                                 { get; set; }        
        public IDbSet<PublishingProfile> PublishingProfile           { get; set; }

        public IDbSet<QuestionBank> QuestionBank                     { get; set; }
        public IDbSet<Question> Question                             { get; set; }
        public IDbSet<QuestionType> QuestionType                     { get; set; }
        public IDbSet<Answer> Answer                                 { get; set; }

        public IDbSet<StudentGroup> StudentGroup                     { get; set; }
        public IDbSet<Student> Student                               { get; set; }
        public IDbSet<StudentStatus> StudentStatus                   { get; set; }
        public IDbSet<PublishingProfileUser> PublishingProfileUser   { get; set; }

        public IDbSet<ExamSchedule> ExamSchedule                     { get; set; }
        public IDbSet<ExamScheduleType> ExamScheduleType             { get; set; }
        public IDbSet<TeacherExamType> TeacherExamType               { get; set; }
        public IDbSet<TeacherExam> TeacherExam                       { get; set; }
        public IDbSet<TeacherExamQuestion> TeacherExamQuestion       { get; set; }
        public IDbSet<ExamScore> ExamScore                           { get; set; }

        public IDbSet<CourseSection> CourseSection                   { get; set; }
        public IDbSet<CourseClass> CourseClass                       { get; set; }
        public IDbSet<ContentResourceType> ContentResource           { get; set; }

        public IDbSet<Glossary> Glossary                             { get; set; }
        public IDbSet<Word> Word                                     { get; set; }

        public IDbSet<Test> Test                                     { get; set; }
        public IDbSet<TestAnswer> TestAnswer                         { get; set; }

        public IDbSet<TeacherResources> TeacherResources             { get; set; }
        public IDbSet<ResourceType> ResourceType                     { get; set; }

        public IDbSet<HomeWork> HomeWork                             { get; set; }
        public IDbSet<ScheduledEvent> ScheduledEvent                 { get; set; }

        public IDbSet<HomeWorkAnswer> HomeWorkAnswer                 { get; set; }
        public IDbSet<HomeWorkAnswerFile> HomeWorkAnswerFile         { get; set; }

        public IDbSet<UserDevice> UserDevice                         { get; set; }

        public IDbSet<NotificationMessages> NotificationMessages     { get; set; }

        public IDbSet<UserNotification> UserNotification             { get; set; }

        public IDbSet<ChatMessage> ChatMessage                       { get; set; }
        public IDbSet<ChatGroup> ChatGroup                           { get; set; }
        public IDbSet<ChatGroupUser> ChatGroupUser                   { get; set; }

        public IDbSet<Attendance> Attendance                         { get; set; }

        public IDbSet<ForumThread> ForumThread                       { get; set; }

        public IDbSet<ForumPost> ForumPost                           { get; set; }

        public IDbSet<ForumPostVote> ForumPostVote                   { get; set; }

        public IDbSet<LinkInterest> LinkInterest                     { get; set; }

        public IDbSet<Tip> Tip                                       { get; set; }

        public IDbSet<News> News                                     { get; set; }

        public IDbSet<VStudents> VStudents                           { get; set; }

        public IDbSet<VNodesTotalFileTypes> VNodesTotalFileTypes     { get; set; }

        public IDbSet<VNodesTotalFileTypesRA> VNodesTotalFileTypesRA { get; set; }

        public IDbSet<Entities.System> Systems                       { get; set; }

        public IDbSet<VCourses> VCourses                             { get; set; }
        
        public IDbSet<City> City                                     { get; set; }

        public IDbSet<State> State                                   { get; set; }

        public IDbSet<UserCCT> UserCCT                               { get; set; }

        public IDbSet<GeneralEvent> GeneralEvent                     { get; set; }

        public IDbSet<UserAccess> UserAccess                         { get; set; }

        public IDbSet<VideoConference> VideoConferences              { get; set; }

        public IDbSet<Weighting> Weighting                           { get; set; }

        public IDbSet<CoursePlanning> CoursePlanning                 { get; set; }

        
        public IDbSet<StatesVideoConference> StatesVideoConferences { get; set; }

        public IDbSet<SchedulingPartial> SchedulingPartial { get; set; }
		
		public IDbSet<Announcement> Announcement { get; set; }

        public IDbSet<Activity> Activity { get; set; }       

        public IDbSet<ActivityStudentFile> ActivityStudent { get; set; }

        public IDbSet<StudentScore> StudentScore { get; set; }

        public IDbSet<ActivityAnswer> ActivityAnswer { get; set; }

        public IDbSet<ActivityFile> ActivityFile { get; set; }

        public IDbSet<EventsReport> EventsReport { get; set; }

        public IDbSet<Events> Events { get; set; }
        public IDbSet<AreaPersonalSocialDevelopment> AreaPersonalSocialDevelopment { get; set; }

        public IDbSet<AreasCurriculumAutonomy> AreasCurriculumAutonomy { get; set; }
        public IDbSet<EducationLevel> EducationLevel { get; set; }
        public IDbSet<Axis> Axis { get; set; }
        public IDbSet<Keylearning> Keylearning { get; set; }
        public IDbSet<Learningexpected> Learningexpected { get; set; }
        //public IDbSet<SchoolGrade> SchoolGrade { get; set; }

        public IDbSet<Reports> Reports { get; set; }
        public IDbSet<VCoordinatorReport> VCoordinatorReport { get; set; }
        public IDbSet<VReportCourse> VReportCourse { get; set; }
        public IDbSet<VReportExam> VReportExam { get; set; }

        public IDbSet<Sections> Sections { get; set; }

        public IDbSet<VReportTeacher> VReportTeacher { get; set; }

        public IDbSet<VReportProfileTeacher> VReportProfileTeacher { get; set; }


        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Test>()
             .HasRequired(t => t.User)
             .WithMany()
             .HasForeignKey(t => t.UserId)
             .WillCascadeOnDelete(false);
            
            modelBuilder.Configurations.Add(new UserConfig());
            modelBuilder.Configurations.Add(new RoleConfig());
            modelBuilder.Configurations.Add(new PermissionConfig());

            modelBuilder.Configurations.Add(new AreaConfig());
            modelBuilder.Configurations.Add(new AuthorConfig());
            modelBuilder.Configurations.Add(new BlockConfig());
            modelBuilder.Configurations.Add(new BookConfig());
            modelBuilder.Configurations.Add(new ContentConfig());
            modelBuilder.Configurations.Add(new ContentTypeConfig());
            modelBuilder.Configurations.Add(new FileTypeConfig());

            modelBuilder.Configurations.Add(new FormativeFieldConfig());
            modelBuilder.Configurations.Add(new GradeConfig());
            modelBuilder.Configurations.Add(new LevelConfig());
            modelBuilder.Configurations.Add(new LocationConfig());
            modelBuilder.Configurations.Add(new NodeConfig());
            modelBuilder.Configurations.Add(new NodeContentConfig());
            modelBuilder.Configurations.Add(new NodeTypeConfig());
            modelBuilder.Configurations.Add(new PublishingProfileConfig());
            modelBuilder.Configurations.Add(new PurposeConfig());
            modelBuilder.Configurations.Add(new SubsystemConfig());
            modelBuilder.Configurations.Add(new TrainingFieldConfig());
            modelBuilder.Configurations.Add(new CourseConfig());

            modelBuilder.Configurations.Add(new QuestionBankConfig());
            modelBuilder.Configurations.Add(new QuestionConfig());
            modelBuilder.Configurations.Add(new QuestionTypeConfig());
            modelBuilder.Configurations.Add(new AnswerConfig());

            modelBuilder.Configurations.Add(new StudentGroupConfig());
            modelBuilder.Configurations.Add(new StudentConfig());
            modelBuilder.Configurations.Add(new StudentStatusConfig());

            modelBuilder.Configurations.Add(new PublishingProfileUserConfig());

            modelBuilder.Configurations.Add(new ExamScheduleConfig());
            modelBuilder.Configurations.Add(new ExamScheduleTypeConfig());
            modelBuilder.Configurations.Add(new ExamScoreConfig());

            modelBuilder.Configurations.Add(new CourseSectionConfig());
            modelBuilder.Configurations.Add(new CourseClassConfig());

            modelBuilder.Configurations.Add(new TeacherExamTypeConfig());
            modelBuilder.Configurations.Add(new TeacherExamConfig());
            modelBuilder.Configurations.Add(new TeacherExamQuestionConfig());

            modelBuilder.Configurations.Add(new ContentResourceTypeConfig());

            modelBuilder.Configurations.Add(new GlossaryConfig());
            modelBuilder.Configurations.Add(new WordConfig());

            modelBuilder.Configurations.Add(new TestsConfig());
            modelBuilder.Configurations.Add(new TestAnswersConfig());

            modelBuilder.Configurations.Add(new TeacherResourcesConfig());
            modelBuilder.Configurations.Add(new ResourceTypeConfig());

            modelBuilder.Configurations.Add(new HomeWorkConfig());

            modelBuilder.Configurations.Add(new ScheduledEventConfig());

            modelBuilder.Configurations.Add(new HomeWorkAnswerConfig());
            modelBuilder.Configurations.Add(new HomeWorkAnswerFileConfig());

            modelBuilder.Configurations.Add(new UserDeviceConfig());

            modelBuilder.Configurations.Add(new NotificationMessagesConfig());

            modelBuilder.Configurations.Add(new UserNotificationConfig());

            modelBuilder.Configurations.Add(new ChatMessageConfig());
            modelBuilder.Configurations.Add(new ChatGroupUserConfig());
            modelBuilder.Configurations.Add(new ChatGroupConfig());

            modelBuilder.Configurations.Add(new AttendanceConfig());

            modelBuilder.Configurations.Add(new ForumPostConfig());
            modelBuilder.Configurations.Add(new ForumThreadConfig());

            modelBuilder.Configurations.Add(new ForumPostVoteConfig());

            modelBuilder.Configurations.Add(new LinkInterestConfig());
            modelBuilder.Configurations.Add(new TipConfig());
            modelBuilder.Configurations.Add(new NewsConfig());
            modelBuilder.Configurations.Add(new VStudentsConfig());
            modelBuilder.Configurations.Add(new VNodesTotalFileTypesConfig());
            modelBuilder.Configurations.Add(new VNodesTotalFileTypesRAConfig());
            modelBuilder.Configurations.Add(new SystemConfig());
            modelBuilder.Configurations.Add(new VCoursesConfig());

            modelBuilder.Configurations.Add(new GeneralEventConfig());

            modelBuilder.Configurations.Add(new CityConfig());
            modelBuilder.Configurations.Add(new StateConfig());
            modelBuilder.Configurations.Add(new UserCCTConfig());

            modelBuilder.Configurations.Add(new UserAccessConfig());
            modelBuilder.Configurations.Add(new VideoConferenceConfig());
            modelBuilder.Configurations.Add(new CoursePlanningConfig());

            modelBuilder.Configurations.Add(new StatesVideoConfereceConfig());

            modelBuilder.Configurations.Add(new WeightingConfig());
			
			modelBuilder.Configurations.Add(new AnnouncementConfig());

            modelBuilder.Configurations.Add(new ActivityConfig());
            modelBuilder.Configurations.Add(new ActivityStudentFileConfig());           

            modelBuilder.Configurations.Add(new StudentScoreConfig());

            modelBuilder.Configurations.Add(new ActivityAnswerConfig());

            modelBuilder.Configurations.Add(new ActivityFileConfig());


            modelBuilder.Configurations.Add(new EventsReportConfig());

            modelBuilder.Configurations.Add(new EventConfig());

            modelBuilder.Configurations.Add(new AreaPersonalSocialDevelopmentConfig());
            modelBuilder.Configurations.Add(new AreasCurriculumAutonomyConfig());
            modelBuilder.Configurations.Add(new EducationLevelConfig());
            modelBuilder.Configurations.Add(new AxisConfig());
            modelBuilder.Configurations.Add(new KeylearningConfig());
            modelBuilder.Configurations.Add(new LearningexpectedConfig());
            //modelBuilder.Configurations.Add(new SchoolGradeConfig());

            modelBuilder.Configurations.Add(new ReportConfig());

            modelBuilder.Configurations.Add(new VCoordinatorReportConfig());
            modelBuilder.Configurations.Add(new VReportCourseConfig());
            modelBuilder.Configurations.Add(new VReportExamConfig());


            modelBuilder.Configurations.Add(new SectionConfig());

            modelBuilder.Configurations.Add(new VReportTeacherConfig());

            modelBuilder.Configurations.Add(new VReportProfileTeacherConfig());


        }
    }
}
