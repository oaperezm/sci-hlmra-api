namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1250_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "TypeScore", c => c.Boolean());
            Sql("UPDATE dbo.Activities SET TypeScore=0  where isnull(TypeScore,0)=0");
            Sql("ALTER TABLE dbo.Activities ADD DEFAULT (0) for TypeScore");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "TypeScore");
        }
    }
}
