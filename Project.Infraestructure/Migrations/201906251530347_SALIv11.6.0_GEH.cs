namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1160_GEH : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityStudentFile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(nullable: false),
                        Comment = c.String(maxLength: 500),
                        RegisterDate = c.DateTime(),
                        UserId = c.Int(nullable: false),
                        ActivitiesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivitiesId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ActivitiesId);

            AddColumn("dbo.HomeWorkAnswers", "PartialEvaluation", c => c.Int());
            AddColumn("dbo.HomeWorkAnswers", "StatusHomework", c => c.Int());
            Sql("UPDATE dbo.HomeWorkAnswers SET StatusHomework = 1 where StatusHomework is null");
            Sql("UPDATE dbo.HomeWorkAnswers SET PartialEvaluation = 0 where PartialEvaluation is null");
            Sql("UPDATE dbo.HomeWorks SET OpeningDate = RegisterDate where OpeningDate is null");
            Sql("UPDATE dbo.HomeWorks SET TypeOfQualification = 1 where TypeOfQualification is null");
            Sql("UPDATE dbo.HomeWorks SET AssignedQualification = 1 where AssignedQualification is null");
            Sql("UPDATE dbo.HomeWorks SET PartialEvaluation = 0 where PartialEvaluation is null");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivityStudentFile", "UserId", "dbo.Users");
            DropForeignKey("dbo.ActivityStudentFile", "ActivitiesId", "dbo.Activities");
            DropIndex("dbo.ActivityStudentFile", new[] { "ActivitiesId" });
            DropIndex("dbo.ActivityStudentFile", new[] { "UserId" });
            DropTable("dbo.ActivityStudentFile");
            DropColumn("dbo.HomeWorkAnswers", "StatusHomework");
            DropColumn("dbo.HomeWorkAnswers", "PartialEvaluation");
            Sql("UPDATE dbo.HomeWorkAnswers SET StatusHomework = 1 where StatusHomework is null");
            Sql("UPDATE dbo.HomeWorkAnswers SET PartialEvaluation = 0 where PartialEvaluation is null");
            Sql("UPDATE dbo.HomeWorks SET OpeningDate = RegisterDate where OpeningDate is null");
            Sql("UPDATE dbo.HomeWorks SET TypeOfQualification = 1 where TypeOfQualification is null");
            Sql("UPDATE dbo.HomeWorks SET AssignedQualification = 1 where AssignedQualification is null");
            Sql("UPDATE dbo.HomeWorks SET PartialEvaluation = 0 where PartialEvaluation is null");
        }
    }
}
