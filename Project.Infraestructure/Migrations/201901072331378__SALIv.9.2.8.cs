namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _SALIv928 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "LastName", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.Authors", "Comment", c => c.String());
            AlterColumn("dbo.Authors", "FullName", c => c.String(nullable: false, maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Authors", "FullName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Authors", "Comment");
            DropColumn("dbo.Authors", "LastName");
        }
    }
}
