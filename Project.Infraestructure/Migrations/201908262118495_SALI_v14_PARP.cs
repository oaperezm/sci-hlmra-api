namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v14_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "EducationLevelId", c => c.Int(nullable: true));
            AddColumn("dbo.Contents", "AreaPersonalSocialDevelopmentId", c => c.Int(nullable: true));
            AddColumn("dbo.Contents", "AreasCurriculumAutonomyId", c => c.Int(nullable: true));
            AddColumn("dbo.Contents", "AxisId", c => c.Int(nullable: true));
            AddColumn("dbo.Contents", "KeylearningId", c => c.Int(nullable: true));
            AddColumn("dbo.Contents", "LearningexpectedId", c => c.Int());
            CreateIndex("dbo.Contents", "EducationLevelId");
            CreateIndex("dbo.Contents", "AreaPersonalSocialDevelopmentId");
            CreateIndex("dbo.Contents", "AreasCurriculumAutonomyId");
            CreateIndex("dbo.Contents", "AxisId");
            CreateIndex("dbo.Contents", "KeylearningId");
            CreateIndex("dbo.Contents", "LearningexpectedId");
            AddForeignKey("dbo.Contents", "AreaPersonalSocialDevelopmentId", "dbo.AreaPersonalSocialDevelopment", "Id");
            AddForeignKey("dbo.Contents", "AreasCurriculumAutonomyId", "dbo.AreasCurriculumAutonomy", "Id");
            AddForeignKey("dbo.Contents", "AxisId", "dbo.Axis", "Id");
            AddForeignKey("dbo.Contents", "EducationLevelId", "dbo.EducationLevel", "Id");
            AddForeignKey("dbo.Contents", "KeylearningId", "dbo.Keylearning", "Id");
            AddForeignKey("dbo.Contents", "LearningexpectedId", "dbo.Learningexpected", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "LearningexpectedId", "dbo.Learningexpected");
            DropForeignKey("dbo.Contents", "KeylearningId", "dbo.Keylearning");
            DropForeignKey("dbo.Contents", "EducationLevelId", "dbo.EducationLevel");
            DropForeignKey("dbo.Contents", "AxisId", "dbo.Axis");
            DropForeignKey("dbo.Contents", "AreasCurriculumAutonomyId", "dbo.AreasCurriculumAutonomy");
            DropForeignKey("dbo.Contents", "AreaPersonalSocialDevelopmentId", "dbo.AreaPersonalSocialDevelopment");
            DropIndex("dbo.Contents", new[] { "LearningexpectedId" });
            DropIndex("dbo.Contents", new[] { "KeylearningId" });
            DropIndex("dbo.Contents", new[] { "AxisId" });
            DropIndex("dbo.Contents", new[] { "AreasCurriculumAutonomyId" });
            DropIndex("dbo.Contents", new[] { "AreaPersonalSocialDevelopmentId" });
            DropIndex("dbo.Contents", new[] { "EducationLevelId" });
            DropColumn("dbo.Contents", "LearningexpectedId");
            DropColumn("dbo.Contents", "KeylearningId");
            DropColumn("dbo.Contents", "AxisId");
            DropColumn("dbo.Contents", "AreasCurriculumAutonomyId");
            DropColumn("dbo.Contents", "AreaPersonalSocialDevelopmentId");
            DropColumn("dbo.Contents", "EducationLevelId");
        }
    }
}
