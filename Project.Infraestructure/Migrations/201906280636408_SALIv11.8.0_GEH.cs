namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1180_GEH : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "SchedulingPartialId", c => c.Int());
            AddColumn("dbo.HomeWorks", "SchedulingPartialId", c => c.Int());
            CreateIndex("dbo.Activities", "SchedulingPartialId");
            CreateIndex("dbo.HomeWorks", "SchedulingPartialId");
            AddForeignKey("dbo.HomeWorks", "SchedulingPartialId", "dbo.SchedulingPartial", "Id");
            AddForeignKey("dbo.Activities", "SchedulingPartialId", "dbo.SchedulingPartial", "Id");
            DropColumn("dbo.Activities", "PartialEvaluation");
            DropColumn("dbo.HomeWorks", "PartialEvaluation");

            CreateTable(
                "dbo.ActivityTeacherResources",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ActivityId = c.Int(nullable: false),
                    TeacherResourceId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.TeacherResources", t => t.TeacherResourceId)
                .Index(t => t.ActivityId)
                .Index(t => t.TeacherResourceId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivityTeacherResources", "TeacherResourceId", "dbo.TeacherResources");
            DropForeignKey("dbo.ActivityTeacherResources", "ActivityId", "dbo.Activities");
            DropIndex("dbo.ActivityTeacherResources", new[] { "TeacherResourceId" });
            DropIndex("dbo.ActivityTeacherResources", new[] { "ActivityId" });
            DropTable("dbo.ActivityTeacherResources");

            AddColumn("dbo.HomeWorks", "PartialEvaluation", c => c.Int());
            AddColumn("dbo.Activities", "PartialEvaluation", c => c.Int(nullable: false));
            DropForeignKey("dbo.Activities", "SchedulingPartialId", "dbo.SchedulingPartial");
            DropForeignKey("dbo.HomeWorks", "SchedulingPartialId", "dbo.SchedulingPartial");
            DropIndex("dbo.HomeWorks", new[] { "SchedulingPartialId" });
            DropIndex("dbo.Activities", new[] { "SchedulingPartialId" });
            DropColumn("dbo.HomeWorks", "SchedulingPartialId");
            DropColumn("dbo.Activities", "SchedulingPartialId");
        }
    }
}
