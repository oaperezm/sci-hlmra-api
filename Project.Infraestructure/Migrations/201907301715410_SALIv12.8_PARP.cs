namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv128_PARP : DbMigration
    {
        public override void Up()
        {
            Sql(
                "INSERT INTO [dbo].[NotificationMessages]([Module] ,[Action] ,[Generate] ,[Receiver] ,[Title] ,[Message])  VALUES ('Programación de actividades', 'Actualización de programación de actividad', 'Profesor', 'Alumno: los alumnos que estén asociados al grupo de la programación', 'Programación de actividades', 'Tu profesor del curso {0}  ha programado una actividad {1} para el {2}')"+
                "INSERT INTO [dbo].[NotificationMessages]([Module] ,[Action] ,[Generate] ,[Receiver] ,[Title] ,[Message])  VALUES ('Programación de planeación del curso', 'Actualización de programación de la planeación del curso', 'Profesor', 'Alumno: los alumnos que estén asociados al grupo de la programación', 'Programación de planeación del curso', 'Tu profesor del curso {0}  ha programado planeación del curso {1} para el {2}')"+
                "INSERT INTO [dbo].[NotificationMessages]([Module] ,[Action] ,[Generate] ,[Receiver] ,[Title] ,[Message])  VALUES ('Programación de la calificación final', 'Actualización de programación de la calificación final', 'Profesor', 'Alumno: los alumnos que estén asociados al grupo de la calificación final', 'Programación de la calificación final', 'Tu profesor del curso {0}  ha programado la calificación final {1} para el {2}')"
               );
        }
        
        public override void Down()
        {
            Sql("delete from dbo.NotificationMessages where Module='Programación de actividades'"+
                "delete from dbo.NotificationMessages where Module='Programación de planeación del curso'" +
                "delete from dbo.NotificationMessages where Module='Programación de la calificación final'"
                );
        }
    }
}
