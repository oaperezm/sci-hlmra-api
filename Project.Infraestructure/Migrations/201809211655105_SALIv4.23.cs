namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv423 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Answer", newName: "Answers");
            RenameTable(name: "dbo.Question", newName: "Questions");
            RenameTable(name: "dbo.QuestionBank", newName: "QuestionBanks");
            RenameTable(name: "dbo.QuestionType", newName: "QuestionTypes");
            CreateTable(
                "dbo.StudentGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvitationDate = c.DateTime(nullable: false),
                        CheckDate = c.DateTime(),
                        StudentGroupId = c.Int(nullable: false),
                        UserId = c.Int(),
                        StudentStatusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId, cascadeDelete: true)
                .ForeignKey("dbo.StudentStatuses", t => t.StudentStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.StudentGroupId)
                .Index(t => t.UserId)
                .Index(t => t.StudentStatusId);
            
            CreateTable(
                "dbo.StudentStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.Answers", "Description", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Questions", "Content", c => c.String(nullable: false, maxLength: 800));
            AlterColumn("dbo.QuestionBanks", "Name", c => c.String(nullable: false, maxLength: 350));
            AlterColumn("dbo.QuestionBanks", "Description", c => c.String(maxLength: 600));
            AlterColumn("dbo.QuestionBanks", "UpdateDate", c => c.DateTime());
            AlterColumn("dbo.QuestionTypes", "Description", c => c.String(nullable: false, maxLength: 350));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "UserId", "dbo.Users");
            DropForeignKey("dbo.Students", "StudentStatusId", "dbo.StudentStatuses");
            DropForeignKey("dbo.Students", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.StudentGroups", "CourseId", "dbo.Courses");
            DropIndex("dbo.Students", new[] { "StudentStatusId" });
            DropIndex("dbo.Students", new[] { "UserId" });
            DropIndex("dbo.Students", new[] { "StudentGroupId" });
            DropIndex("dbo.StudentGroups", new[] { "CourseId" });
            AlterColumn("dbo.QuestionTypes", "Description", c => c.String());
            AlterColumn("dbo.QuestionBanks", "UpdateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.QuestionBanks", "Description", c => c.String());
            AlterColumn("dbo.QuestionBanks", "Name", c => c.String());
            AlterColumn("dbo.Questions", "Content", c => c.String());
            AlterColumn("dbo.Answers", "Description", c => c.String());
            DropTable("dbo.StudentStatuses");
            DropTable("dbo.Students");
            DropTable("dbo.StudentGroups");
            RenameTable(name: "dbo.QuestionTypes", newName: "QuestionType");
            RenameTable(name: "dbo.QuestionBanks", newName: "QuestionBank");
            RenameTable(name: "dbo.Questions", newName: "Question");
            RenameTable(name: "dbo.Answers", newName: "Answer");
        }
    }
}
