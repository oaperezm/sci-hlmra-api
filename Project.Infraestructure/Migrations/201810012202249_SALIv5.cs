namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CourseSections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Content = c.String(nullable: false, maxLength: 800),
                        Order = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.CourseClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Content = c.String(nullable: false, maxLength: 800),
                        Order = c.Int(nullable: false),
                        CourseSectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseSections", t => t.CourseSectionId, cascadeDelete: true)
                .Index(t => t.CourseSectionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CourseClasses", "CourseSectionId", "dbo.CourseSections");
            DropForeignKey("dbo.CourseSections", "CourseId", "dbo.Courses");
            DropIndex("dbo.CourseClasses", new[] { "CourseSectionId" });
            DropIndex("dbo.CourseSections", new[] { "CourseId" });
            DropTable("dbo.CourseClasses");
            DropTable("dbo.CourseSections");
        }
    }
}
