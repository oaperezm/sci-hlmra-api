namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5112 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "NodeId", "dbo.Nodes");
            DropIndex("dbo.Books", new[] { "NodeId" });
            AddColumn("dbo.Nodes", "BookId", c => c.Int());
            AddColumn("dbo.Books", "ISBN", c => c.String(nullable: false, maxLength: 15));
            AddColumn("dbo.Books", "Codebar", c => c.String(nullable: false, maxLength: 20));
            CreateIndex("dbo.Nodes", "BookId");
            AddForeignKey("dbo.Nodes", "BookId", "dbo.Books", "Id");
            DropColumn("dbo.Books", "NodeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "NodeId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Nodes", "BookId", "dbo.Books");
            DropIndex("dbo.Nodes", new[] { "BookId" });
            DropColumn("dbo.Books", "Codebar");
            DropColumn("dbo.Books", "ISBN");
            DropColumn("dbo.Nodes", "BookId");
            CreateIndex("dbo.Books", "NodeId");
            AddForeignKey("dbo.Books", "NodeId", "dbo.Nodes", "Id", cascadeDelete: true);
        }
    }
}
