namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv10 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.Nodes SET Sort=0");
        }
        
        public override void Down()
        {
           
        }
    }
}
