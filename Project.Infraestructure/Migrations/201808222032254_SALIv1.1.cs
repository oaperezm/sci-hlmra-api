namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PublishingProfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Node", "PublishingProfile_Id", c => c.Int());
            CreateIndex("dbo.Node", "PublishingProfile_Id");
            AddForeignKey("dbo.Node", "PublishingProfile_Id", "dbo.PublishingProfile", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Node", "PublishingProfile_Id", "dbo.PublishingProfile");
            DropIndex("dbo.Node", new[] { "PublishingProfile_Id" });
            DropColumn("dbo.Node", "PublishingProfile_Id");
            DropTable("dbo.PublishingProfile");
        }
    }
}
