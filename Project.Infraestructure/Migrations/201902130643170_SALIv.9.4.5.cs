namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv945 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GeneralEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(nullable: false, maxLength: 500),
                        Place = c.String(nullable: false, maxLength: 300),
                        EventDate = c.DateTime(nullable: false),
                        Hours = c.String(nullable: false, maxLength: 5),
                        Published = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GeneralEvents");
        }
    }
}
