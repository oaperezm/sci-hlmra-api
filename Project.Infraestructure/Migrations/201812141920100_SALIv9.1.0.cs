namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv910 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDevices", "DeviceId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDevices", "DeviceId");
        }
    }
}
