namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv11110_GEH : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityAnswers",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ActivityId = c.Int(nullable: false),
                    UserId = c.Int(nullable: false),
                    Score = c.String(nullable: false, maxLength: 5),
                    RegisterDate = c.DateTime(),
                    Comment = c.String(maxLength: 500),
                    Delivered = c.Boolean(),
                    PartialEvaluation = c.Int(nullable: false),
                    StatusActivity = c.Int(nullable: false),
                    AddNewAnswers = c.Boolean(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ActivityId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.ActivityFile",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ActivityId = c.Int(nullable: false),
                    Url = c.String(),
                    Comment = c.String(),
                    RegisterDate = c.DateTime(nullable: false),
                    ActivityAnswer_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.ActivityAnswers", t => t.ActivityAnswer_Id)
                .Index(t => t.ActivityId)
                .Index(t => t.ActivityAnswer_Id);

            AddColumn("dbo.Activities", "Qualified", c => c.Boolean(nullable: false));
            AddColumn("dbo.Activities", "TypeOfQualification", c => c.Boolean());
            AddColumn("dbo.Activities", "AssignedQualification", c => c.Int());

            Sql("update dbo.HomeWorkAnswers set AddNewAnswers=0 where AddNewAnswers is null");
            //Sql("ALTER TABLE dbo.HomeWorkAnswers ADD DEFAULT (0) for AddNewAnswers");
            //Sql("ALTER TABLE dbo.ActivityAnswers ADD DEFAULT (0) for AddNewAnswers");
        }

        public override void Down()
        {
            DropForeignKey("dbo.ActivityAnswers", "UserId", "dbo.Users");
            DropForeignKey("dbo.ActivityFile", "ActivityAnswer_Id", "dbo.ActivityAnswers");
            DropForeignKey("dbo.ActivityFile", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.ActivityAnswers", "ActivityId", "dbo.Activities");
            DropIndex("dbo.ActivityFile", new[] { "ActivityAnswer_Id" });
            DropIndex("dbo.ActivityFile", new[] { "ActivityId" });
            DropIndex("dbo.ActivityAnswers", new[] { "UserId" });
            DropIndex("dbo.ActivityAnswers", new[] { "ActivityId" });
            DropColumn("dbo.Activities", "AssignedQualification");
            DropColumn("dbo.Activities", "TypeOfQualification");
            DropColumn("dbo.Activities", "Qualified");
            DropTable("dbo.ActivityFile");
            DropTable("dbo.ActivityAnswers");
        }
    }
}