namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv960 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoConference", "RoomVideoConference", c => c.String(nullable: false));
            AddColumn("dbo.VideoConference", "GuestMail", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VideoConference", "GuestMail");
            DropColumn("dbo.VideoConference", "RoomVideoConference");
        }
    }
}
