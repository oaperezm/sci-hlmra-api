namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v142_PARP : DbMigration
    {
        public override void Up()
        {
            //    CreateTable(
            //        "dbo.V_CoordinatorReport",
            //        c => new
            //            {
            //                Id = c.Int(nullable: false, identity: true),
            //                cct = c.String(),
            //                email = c.String(),
            //                userName = c.String(),
            //                nameSchool = c.String(),
            //                level = c.String(),
            //                grade = c.String(),
            //                formativeField = c.String(),
            //                signature = c.String(),
            //                course = c.String(),
            //                group = c.String(),
            //                totalStudents = c.Int(nullable: false),
            //                totalActivities = c.Int(nullable: false),
            //                totalScheduledExams = c.Int(nullable: false),
            //                totalQuestionsBanks = c.Int(nullable: false),
            //                totalQuestionTeacher = c.Int(nullable: false),
            //                totalQuestions = c.Int(nullable: false),
            //            })
            //        .PrimaryKey(t => t.Id);
            Sql("CREATE VIEW dbo.V_CoordinatorReport AS select Description from dbo.Nodes where ParentId=148");
        }
        
        public override void Down()
        {
            Sql(" drop VIEW dbo.V_CoordinatorReport");
        }
    }
}
