namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv907 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HomeWorkAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HomeWorkId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Score = c.String(nullable: false, maxLength: 5),
                        RegisterDate = c.DateTime(),
                        Comment = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HomeWorks", t => t.HomeWorkId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.HomeWorkId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.HomeWorkAnswerFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HomeWorkAnswerId = c.Int(nullable: false),
                        Url = c.String(nullable: false),
                        Comment = c.String(maxLength: 500),
                        RegisterDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HomeWorkAnswers", t => t.HomeWorkAnswerId)
                .Index(t => t.HomeWorkAnswerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HomeWorkAnswers", "UserId", "dbo.Users");
            DropForeignKey("dbo.HomeWorkAnswerFiles", "HomeWorkAnswerId", "dbo.HomeWorkAnswers");
            DropForeignKey("dbo.HomeWorkAnswers", "HomeWorkId", "dbo.HomeWorks");
            DropIndex("dbo.HomeWorkAnswerFiles", new[] { "HomeWorkAnswerId" });
            DropIndex("dbo.HomeWorkAnswers", new[] { "UserId" });
            DropIndex("dbo.HomeWorkAnswers", new[] { "HomeWorkId" });
            DropTable("dbo.HomeWorkAnswerFiles");
            DropTable("dbo.HomeWorkAnswers");
        }
    }
}
