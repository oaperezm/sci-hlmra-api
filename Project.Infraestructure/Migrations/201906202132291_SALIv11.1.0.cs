namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1110 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HomeWorks", "OpeningDate", c => c.DateTime(nullable: true));
            AlterColumn("dbo.HomeWorks", "TypeOfQualification", c => c.Int(nullable: true));
            AlterColumn("dbo.HomeWorks", "AssignedQualification", c => c.Int(nullable: true));
            AlterColumn("dbo.HomeWorks", "PartialEvaluation", c => c.Int(nullable: true));      
        }
        
        public override void Down()
        {
            Sql("UPDATE dbo.HomeWorks SET PartialEvaluation = 0");
            AlterColumn("dbo.HomeWorks", "PartialEvaluation", c => c.Int(nullable: false));
            Sql("UPDATE dbo.HomeWorks SET AssignedQualification = 0");
            AlterColumn("dbo.HomeWorks", "AssignedQualification", c => c.Int(nullable: false));
            Sql("UPDATE dbo.HomeWorks SET TypeOfQualification = 0");
            AlterColumn("dbo.HomeWorks", "TypeOfQualification", c => c.Int(nullable: false));
            Sql("UPDATE dbo.HomeWorks SET OpeningDate = GETDATE()");
            AlterColumn("dbo.HomeWorks", "OpeningDate", c => c.DateTime(nullable: false));
        }
    }
}
