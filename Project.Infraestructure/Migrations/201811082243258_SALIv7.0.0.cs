namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv700 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ResourceType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 300),
                        RegisterDate = c.DateTime(nullable: false),
                        Order = c.Int(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeacherResources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 350),
                        Url = c.String(nullable: false),
                        ResourceTypeId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ResourceType", t => t.ResourceTypeId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.ResourceTypeId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherResources", "UserId", "dbo.Users");
            DropForeignKey("dbo.TeacherResources", "ResourceTypeId", "dbo.ResourceType");
            DropIndex("dbo.TeacherResources", new[] { "UserId" });
            DropIndex("dbo.TeacherResources", new[] { "ResourceTypeId" });
            DropTable("dbo.TeacherResources");
            DropTable("dbo.ResourceType");
        }
    }
}
