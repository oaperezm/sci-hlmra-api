namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv914 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserNotofocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Message = c.String(nullable: false, maxLength: 500),
                        NotificationTypeId = c.Int(nullable: false),
                        Read = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        ReadDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserNotofocations", "UserId", "dbo.Users");
            DropIndex("dbo.UserNotofocations", new[] { "UserId" });
            DropTable("dbo.UserNotofocations");
        }
    }
}
