namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv136_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AreaPersonalSocialDevelopment", "EducationLevelId", c => c.Int(nullable: true));
            CreateIndex("dbo.AreaPersonalSocialDevelopment", "EducationLevelId");
            AddForeignKey("dbo.AreaPersonalSocialDevelopment", "EducationLevelId", "dbo.EducationLevel", "Id");
            Sql("UPDATE dbo.AreaPersonalSocialDevelopment SET EducationLevelId=3  where isnull(EducationLevelId,0)=0");

            DropTable("dbo.SchoolGrade");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SchoolGrade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.AreaPersonalSocialDevelopment", "EducationLevelId", "dbo.EducationLevel");
            DropIndex("dbo.AreaPersonalSocialDevelopment", new[] { "EducationLevelId" });
            DropColumn("dbo.AreaPersonalSocialDevelopment", "EducationLevelId");
        }
    }
}
