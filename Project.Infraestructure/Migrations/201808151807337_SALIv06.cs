namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv06 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Permits", newName: "Permission");
            AlterColumn("dbo.Permission", "Module", c => c.String());
            AlterColumn("dbo.Permission", "Description", c => c.String());
            AlterColumn("dbo.Permission", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Permission", "Active", c => c.Boolean());
            AlterColumn("dbo.Permission", "Description", c => c.String(maxLength: 250));
            AlterColumn("dbo.Permission", "Module", c => c.String(nullable: false, maxLength: 80));
            RenameTable(name: "dbo.Permission", newName: "Permits");
        }
    }
}
