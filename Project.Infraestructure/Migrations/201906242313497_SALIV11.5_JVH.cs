namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIV115_JVH : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExamSchedules", "SchedulingPartialId", c => c.Int());
            AddColumn("dbo.TeacherExamQuestions", "UserEdited", c => c.Boolean());
            CreateIndex("dbo.ExamSchedules", "SchedulingPartialId");
            AddForeignKey("dbo.ExamSchedules", "SchedulingPartialId", "dbo.SchedulingPartial", "Id");
            DropColumn("dbo.ExamSchedules", "PartialId");
        }

        public override void Down()
        {
            AddColumn("dbo.ExamSchedules", "PartialId", c => c.Int());
            Sql("UPDATE dbo.ExamSchedules SET PartialId = 0");
            AlterColumn("dbo.ExamSchedules", "PartialId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ExamSchedules", "SchedulingPartialId", "dbo.SchedulingPartial");
            DropIndex("dbo.ExamSchedules", new[] { "SchedulingPartialId" });
            DropColumn("dbo.TeacherExamQuestions", "UserEdited");
            DropColumn("dbo.ExamSchedules", "SchedulingPartialId");
        }
    }
}
