namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv501 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CourseSections", "Order", c => c.Int());
            AlterColumn("dbo.CourseClasses", "Order", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CourseClasses", "Order", c => c.Int(nullable: false));
            AlterColumn("dbo.CourseSections", "Order", c => c.Int(nullable: false));
        }
    }
}
