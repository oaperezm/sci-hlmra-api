namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv518 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.NodeContents", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.NodeContents", "BlockId", "dbo.Blocks");
            DropForeignKey("dbo.NodeContents", "FormativeFieldId", "dbo.FormativeFields");
            DropForeignKey("dbo.NodeContents", "GradeId", "dbo.Grades");
            DropForeignKey("dbo.NodeContents", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.NodeContents", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.NodeContents", "PurposeId", "dbo.Purposes");
            DropForeignKey("dbo.NodeContents", "SubsystemId", "dbo.Subsystems");
            DropForeignKey("dbo.NodeContents", "TrainingFieldId", "dbo.TrainingFields");
            DropIndex("dbo.NodeContents", new[] { "LevelId" });
            DropIndex("dbo.NodeContents", new[] { "SubsystemId" });
            DropIndex("dbo.NodeContents", new[] { "TrainingFieldId" });
            DropIndex("dbo.NodeContents", new[] { "FormativeFieldId" });
            DropIndex("dbo.NodeContents", new[] { "GradeId" });
            DropIndex("dbo.NodeContents", new[] { "BlockId" });
            DropIndex("dbo.NodeContents", new[] { "AreaId" });
            DropIndex("dbo.NodeContents", new[] { "PurposeId" });
            DropIndex("dbo.NodeContents", new[] { "LocationId" });
            AddColumn("dbo.Contents", "FormativeFieldId", c => c.Int());
            AddColumn("dbo.Contents", "AreaId", c => c.Int());
            AddColumn("dbo.Contents", "PurposeId", c => c.Int());
            AddColumn("dbo.Contents", "TrainingFieldId", c => c.Int());
            CreateIndex("dbo.Contents", "FormativeFieldId");
            CreateIndex("dbo.Contents", "AreaId");
            CreateIndex("dbo.Contents", "PurposeId");
            CreateIndex("dbo.Contents", "TrainingFieldId");
            AddForeignKey("dbo.Contents", "AreaId", "dbo.Areas", "Id");
            AddForeignKey("dbo.Contents", "FormativeFieldId", "dbo.FormativeFields", "Id");
            AddForeignKey("dbo.Contents", "PurposeId", "dbo.Purposes", "Id");
            AddForeignKey("dbo.Contents", "TrainingFieldId", "dbo.TrainingFields", "Id");
            DropColumn("dbo.NodeContents", "LevelId");
            DropColumn("dbo.NodeContents", "SubsystemId");
            DropColumn("dbo.NodeContents", "TrainingFieldId");
            DropColumn("dbo.NodeContents", "FormativeFieldId");
            DropColumn("dbo.NodeContents", "GradeId");
            DropColumn("dbo.NodeContents", "BlockId");
            DropColumn("dbo.NodeContents", "AreaId");
            DropColumn("dbo.NodeContents", "PurposeId");
            DropColumn("dbo.NodeContents", "LocationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NodeContents", "LocationId", c => c.Int());
            AddColumn("dbo.NodeContents", "PurposeId", c => c.Int());
            AddColumn("dbo.NodeContents", "AreaId", c => c.Int());
            AddColumn("dbo.NodeContents", "BlockId", c => c.Int());
            AddColumn("dbo.NodeContents", "GradeId", c => c.Int());
            AddColumn("dbo.NodeContents", "FormativeFieldId", c => c.Int());
            AddColumn("dbo.NodeContents", "TrainingFieldId", c => c.Int());
            AddColumn("dbo.NodeContents", "SubsystemId", c => c.Int());
            AddColumn("dbo.NodeContents", "LevelId", c => c.Int());
            DropForeignKey("dbo.Contents", "TrainingFieldId", "dbo.TrainingFields");
            DropForeignKey("dbo.Contents", "PurposeId", "dbo.Purposes");
            DropForeignKey("dbo.Contents", "FormativeFieldId", "dbo.FormativeFields");
            DropForeignKey("dbo.Contents", "AreaId", "dbo.Areas");
            DropIndex("dbo.Contents", new[] { "TrainingFieldId" });
            DropIndex("dbo.Contents", new[] { "PurposeId" });
            DropIndex("dbo.Contents", new[] { "AreaId" });
            DropIndex("dbo.Contents", new[] { "FormativeFieldId" });
            DropColumn("dbo.Contents", "TrainingFieldId");
            DropColumn("dbo.Contents", "PurposeId");
            DropColumn("dbo.Contents", "AreaId");
            DropColumn("dbo.Contents", "FormativeFieldId");
            CreateIndex("dbo.NodeContents", "LocationId");
            CreateIndex("dbo.NodeContents", "PurposeId");
            CreateIndex("dbo.NodeContents", "AreaId");
            CreateIndex("dbo.NodeContents", "BlockId");
            CreateIndex("dbo.NodeContents", "GradeId");
            CreateIndex("dbo.NodeContents", "FormativeFieldId");
            CreateIndex("dbo.NodeContents", "TrainingFieldId");
            CreateIndex("dbo.NodeContents", "SubsystemId");
            CreateIndex("dbo.NodeContents", "LevelId");
            AddForeignKey("dbo.NodeContents", "TrainingFieldId", "dbo.TrainingFields", "Id");
            AddForeignKey("dbo.NodeContents", "SubsystemId", "dbo.Subsystems", "Id");
            AddForeignKey("dbo.NodeContents", "PurposeId", "dbo.Purposes", "Id");
            AddForeignKey("dbo.NodeContents", "LocationId", "dbo.Locations", "Id");
            AddForeignKey("dbo.NodeContents", "LevelId", "dbo.Levels", "Id");
            AddForeignKey("dbo.NodeContents", "GradeId", "dbo.Grades", "Id");
            AddForeignKey("dbo.NodeContents", "FormativeFieldId", "dbo.FormativeFields", "Id");
            AddForeignKey("dbo.NodeContents", "BlockId", "dbo.Blocks", "Id");
            AddForeignKey("dbo.NodeContents", "AreaId", "dbo.Areas", "Id");
        }
    }
}
