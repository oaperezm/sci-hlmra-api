namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv926 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ForumPosts", "Votes", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ForumPosts", "Votes", c => c.Boolean());
        }
    }
}
