// <auto-generated />
namespace Project.Infraestructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALIv17 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALIv17));
        
        string IMigrationMetadata.Id
        {
            get { return "201808281613405_SALIv1.7"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
