namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv131_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventsReport", "EventId", c => c.Int(nullable: true));
            AddColumn("dbo.EventsReport", "Element", c => c.String(nullable: true));
            AddColumn("dbo.EventsReport", "Comment", c => c.String(nullable: true));
            AddColumn("dbo.EventsReport", "CommentDetail", c => c.String(nullable: true));
            CreateIndex("dbo.EventsReport", "EventId");
            AddForeignKey("dbo.EventsReport", "EventId", "dbo.Events", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventsReport", "EventId", "dbo.Events");
            DropIndex("dbo.EventsReport", new[] { "EventId" });
            DropColumn("dbo.EventsReport", "CommentDetail");
            DropColumn("dbo.EventsReport", "Comment");
            DropColumn("dbo.EventsReport", "Element");
            DropColumn("dbo.EventsReport", "EventId");
        }
    }
}
