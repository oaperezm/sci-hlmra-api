namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv958 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoConference", "InvitedId", c => c.Int(nullable:true));
            AddColumn("dbo.VideoConference", "StatusId", c => c.Boolean(nullable: true));
            DropColumn("dbo.VideoConference", "Type");
            DropColumn("dbo.VideoConference", "Invited");
            DropColumn("dbo.VideoConference", "BookId");
            DropColumn("dbo.VideoConference", "Accepted");
            DropColumn("dbo.VideoConference", "rejected");
            DropColumn("dbo.VideoConference", "EditorId");
            DropColumn("dbo.VideoConference", "Editor");

            Sql("ALTER TABLE dbo.VideoConference ADD DEFAULT (0) for InvitedId");
            Sql("ALTER TABLE dbo.VideoConference ADD DEFAULT (0) for StatusId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VideoConference", "Editor", c => c.String(nullable: false, maxLength: 300));
            AddColumn("dbo.VideoConference", "EditorId", c => c.Int(nullable: false));
            AddColumn("dbo.VideoConference", "rejected", c => c.Boolean(nullable: false));
            AddColumn("dbo.VideoConference", "Accepted", c => c.Boolean(nullable: false));
            AddColumn("dbo.VideoConference", "BookId", c => c.Int(nullable: false));
            AddColumn("dbo.VideoConference", "Invited", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.VideoConference", "Type", c => c.String(nullable: false, maxLength: 200));
            DropColumn("dbo.VideoConference", "StatusId");
            DropColumn("dbo.VideoConference", "InvitedId");
        }
    }
}
