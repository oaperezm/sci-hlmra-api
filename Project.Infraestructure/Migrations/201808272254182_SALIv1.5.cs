namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Node", "Name", c => c.String());
            AddColumn("dbo.Node", "UrlImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Node", "UrlImage");
            DropColumn("dbo.Node", "Name");
        }
    }
}
