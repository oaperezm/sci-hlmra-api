namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv962 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.VideoConference", "CourseId", "dbo.Courses");
            DropIndex("dbo.VideoConference", new[] { "CourseId" });
            AlterColumn("dbo.VideoConference", "CourseId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VideoConference", "CourseId", c => c.Int(nullable: false));
            CreateIndex("dbo.VideoConference", "CourseId");
            AddForeignKey("dbo.VideoConference", "CourseId", "dbo.Courses", "Id");
        }
    }
}
