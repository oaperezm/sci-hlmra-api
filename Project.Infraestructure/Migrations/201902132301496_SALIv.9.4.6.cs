namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv946 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GeneralEvents", "UrlImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GeneralEvents", "UrlImage");
        }
    }
}
