namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv131 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Permissions", "Url", c => c.String(maxLength: 350));
            AddColumn("dbo.Permissions", "Icon", c => c.String(maxLength: 50));
            AddColumn("dbo.Permissions", "ParentId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Permissions", "ParentId");
            DropColumn("dbo.Permissions", "Icon");
            DropColumn("dbo.Permissions", "Url");
        }
    }
}
