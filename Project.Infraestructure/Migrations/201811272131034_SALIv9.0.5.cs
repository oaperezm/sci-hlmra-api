namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv905 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduledEvents", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.ScheduledEvents", "CourseId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScheduledEvents", "UserId");
            CreateIndex("dbo.ScheduledEvents", "CourseId");
            AddForeignKey("dbo.ScheduledEvents", "CourseId", "dbo.Courses", "Id");
            AddForeignKey("dbo.ScheduledEvents", "UserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduledEvents", "UserId", "dbo.Users");
            DropForeignKey("dbo.ScheduledEvents", "CourseId", "dbo.Courses");
            DropIndex("dbo.ScheduledEvents", new[] { "CourseId" });
            DropIndex("dbo.ScheduledEvents", new[] { "UserId" });
            DropColumn("dbo.ScheduledEvents", "CourseId");
            DropColumn("dbo.ScheduledEvents", "UserId");
        }
    }
}
