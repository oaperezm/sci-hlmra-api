// <auto-generated />
namespace Project.Infraestructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALIv127_PARP : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALIv127_PARP));
        
        string IMigrationMetadata.Id
        {
            get { return "201907191613336_SALIv12.7_PARP"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
