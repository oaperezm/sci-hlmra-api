namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv101 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Weightings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        Unit = c.String(nullable: false, maxLength: 10),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.ExamSchedules", "PartialId", c => c.Int(nullable: false));
            AddColumn("dbo.ExamSchedules", "BeginApplicationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExamSchedules", "EndApplicationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExamSchedules", "BeginApplicationTime", c => c.DateTime());
            AddColumn("dbo.ExamSchedules", "EndApplicationTime", c => c.DateTime());
            AddColumn("dbo.ExamSchedules", "MinutesExam", c => c.Int());
            AddColumn("dbo.TeacherExams", "WeightingId", c => c.Int(nullable: false));
            AddColumn("dbo.TeacherExams", "MaximumExamScore", c => c.Int(nullable: false));
            AddColumn("dbo.TeacherExams", "IsAutomaticValue", c => c.Boolean(nullable: false));
            AddColumn("dbo.TeacherExamQuestions", "Value", c => c.Decimal(nullable: false, precision: 5, scale: 2));
            AddColumn("dbo.Tests", "CorrectAnswers", c => c.Int());
            AddColumn("dbo.Tests", "Comments", c => c.String(maxLength: 450));
            AddColumn("dbo.Tests", "StartDate", c => c.DateTime());
            AddColumn("dbo.Tests", "LastResponseDate", c => c.DateTime());
            AddColumn("dbo.Tests", "IsEvaluated", c => c.Boolean());
            AddColumn("dbo.Tests", "EvaluationDate", c => c.DateTime());
            AddColumn("dbo.TestAnswers", "ResponseDate", c => c.DateTime());
            AddColumn("dbo.TestAnswers", "TeacherExamQuestionId", c => c.Int(nullable: false));

            Sql(@"
                SET IDENTITY_INSERT [dbo].[Weightings] ON
                GO
                DECLARE @UserId AS INT
                SELECT @UserId = MIN(Id) FROM [dbo].[Users]
                IF NOT EXISTS (SELECT 1 FROM [dbo].[Weightings] WHERE id = 1)
	                INSERT INTO [dbo].[Weightings] ([Id], [Description], [Unit], [UserId], [RegisterDate], [UpdateDate])
                        VALUES 
                            (1,'Porcentual','%',  @UserId, GETDATE(), GETDATE()),
                            (2,'Puntuación','pts.',  @UserId, GETDATE(), GETDATE())   

                SET IDENTITY_INSERT [dbo].[Weightings] OFF
                GO

                UPDATE [dbo].[TeacherExams] SET [WeightingId] = 1
                GO

                DECLARE @TeacherExamQuestionId AS INT
                SELECT @TeacherExamQuestionId = MIN(Id) FROM dbo.TeacherExamQuestions
                UPDATE [dbo].[TestAnswers] SET [TeacherExamQuestionId] = @TeacherExamQuestionId
                GO");
            CreateIndex("dbo.TeacherExams", "WeightingId");
            CreateIndex("dbo.TestAnswers", "TeacherExamQuestionId");
            AddForeignKey("dbo.TeacherExams", "WeightingId", "dbo.Weightings", "Id");
            AddForeignKey("dbo.TestAnswers", "TeacherExamQuestionId", "dbo.TeacherExamQuestions", "Id");
            DropColumn("dbo.ExamSchedules", "ApplicationDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExamSchedules", "ApplicationDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.TestAnswers", "TeacherExamQuestionId", "dbo.TeacherExamQuestions");
            DropForeignKey("dbo.TeacherExams", "WeightingId", "dbo.Weightings");
            DropForeignKey("dbo.Weightings", "UserId", "dbo.Users");
            DropIndex("dbo.TestAnswers", new[] { "TeacherExamQuestionId" });
            DropIndex("dbo.Weightings", new[] { "UserId" });
            DropIndex("dbo.TeacherExams", new[] { "WeightingId" });
            DropColumn("dbo.TestAnswers", "TeacherExamQuestionId");
            DropColumn("dbo.TestAnswers", "ResponseDate");
            DropColumn("dbo.Tests", "EvaluationDate");
            DropColumn("dbo.Tests", "IsEvaluated");
            DropColumn("dbo.Tests", "LastResponseDate");
            DropColumn("dbo.Tests", "StartDate");
            DropColumn("dbo.Tests", "Comments");
            DropColumn("dbo.Tests", "CorrectAnswers");
            DropColumn("dbo.TeacherExamQuestions", "Value");
            DropColumn("dbo.TeacherExams", "IsAutomaticValue");
            DropColumn("dbo.TeacherExams", "MaximumExamScore");
            DropColumn("dbo.TeacherExams", "WeightingId");
            DropColumn("dbo.ExamSchedules", "MinutesExam");
            DropColumn("dbo.ExamSchedules", "EndApplicationTime");
            DropColumn("dbo.ExamSchedules", "BeginApplicationTime");
            DropColumn("dbo.ExamSchedules", "EndApplicationDate");
            DropColumn("dbo.ExamSchedules", "BeginApplicationDate");
            DropColumn("dbo.ExamSchedules", "PartialId");
            DropTable("dbo.Weightings");
        }
    }
}
