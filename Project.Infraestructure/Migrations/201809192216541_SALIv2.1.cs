namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv21 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Courses", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.Courses", "UserId", "dbo.Users");
            DropIndex("dbo.Courses", new[] { "NodeId" });
            DropIndex("dbo.Courses", new[] { "UserId" });
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IsCorrect = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Question", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        QuestionBankId = c.Int(nullable: false),
                        QuestionTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionBank", t => t.QuestionBankId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionType", t => t.QuestionTypeId, cascadeDelete: true)
                .Index(t => t.QuestionBankId)
                .Index(t => t.QuestionTypeId);
            
            CreateTable(
                "dbo.QuestionBank",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.QuestionType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Courses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 500),
                        Description = c.String(nullable: false, maxLength: 800),
                        Gol = c.String(nullable: false, maxLength: 800),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        NodeId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(),
                        UpdateDate = c.DateTime(),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Fridays = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        Sunday = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Question", "QuestionTypeId", "dbo.QuestionType");
            DropForeignKey("dbo.Question", "QuestionBankId", "dbo.QuestionBank");
            DropForeignKey("dbo.QuestionBank", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.Answer", "QuestionId", "dbo.Question");
            DropIndex("dbo.QuestionBank", new[] { "NodeId" });
            DropIndex("dbo.Question", new[] { "QuestionTypeId" });
            DropIndex("dbo.Question", new[] { "QuestionBankId" });
            DropIndex("dbo.Answer", new[] { "QuestionId" });
            DropTable("dbo.QuestionType");
            DropTable("dbo.QuestionBank");
            DropTable("dbo.Question");
            DropTable("dbo.Answer");
            CreateIndex("dbo.Courses", "UserId");
            CreateIndex("dbo.Courses", "NodeId");
            AddForeignKey("dbo.Courses", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Courses", "NodeId", "dbo.Nodes", "Id", cascadeDelete: true);
        }
    }
}
