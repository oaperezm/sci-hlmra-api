namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv513 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams");
            AddColumn("dbo.ExamSchedules", "TeacherExamId", c => c.Int());
            CreateIndex("dbo.ExamSchedules", "TeacherExamId");
            AddForeignKey("dbo.ExamSchedules", "TeacherExamId", "dbo.TeacherExams", "Id");
            AddForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams");
            DropForeignKey("dbo.ExamSchedules", "TeacherExamId", "dbo.TeacherExams");
            DropIndex("dbo.ExamSchedules", new[] { "TeacherExamId" });
            DropColumn("dbo.ExamSchedules", "TeacherExamId");
            AddForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams", "Id", cascadeDelete: true);
        }
    }
}
