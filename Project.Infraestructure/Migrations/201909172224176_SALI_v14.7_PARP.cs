namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v147_PARP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.EventsReport", "FileTypeId", c => c.Int(nullable: true));
            AddColumn("dbo.EventsReport", "SectionId", c => c.Int(nullable: true));
            AddColumn("dbo.EventsReport", "ContentTypeId", c => c.Int(nullable: true));
            CreateIndex("dbo.EventsReport", "FileTypeId");
            CreateIndex("dbo.EventsReport", "SectionId");
            CreateIndex("dbo.EventsReport", "ContentTypeId");
            AddForeignKey("dbo.EventsReport", "ContentTypeId", "dbo.ContentTypes", "Id");
            AddForeignKey("dbo.EventsReport", "FileTypeId", "dbo.FileTypes", "Id");
            AddForeignKey("dbo.EventsReport", "SectionId", "dbo.Sections", "Id");
            DropColumn("dbo.EventsReport", "Sections");
            DropColumn("dbo.EventsReport", "Format");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventsReport", "Format", c => c.String(nullable: false));
            AddColumn("dbo.EventsReport", "Sections", c => c.String(nullable: false));
            DropForeignKey("dbo.EventsReport", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.EventsReport", "FileTypeId", "dbo.FileTypes");
            DropForeignKey("dbo.EventsReport", "ContentTypeId", "dbo.ContentTypes");
            DropIndex("dbo.EventsReport", new[] { "ContentTypeId" });
            DropIndex("dbo.EventsReport", new[] { "SectionId" });
            DropIndex("dbo.EventsReport", new[] { "FileTypeId" });
            DropColumn("dbo.EventsReport", "ContentTypeId");
            DropColumn("dbo.EventsReport", "SectionId");
            DropColumn("dbo.EventsReport", "FileTypeId");
            DropTable("dbo.Sections");
        }
    }
}
