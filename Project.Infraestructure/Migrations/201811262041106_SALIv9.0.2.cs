namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv902 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HomeWorks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScheduledDate = c.DateTime(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(nullable: false, maxLength: 400),
                        RegisterDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.StudentGroupId);
            
            CreateTable(
                "dbo.HomeWorkTeacherResources",
                c => new
                    {
                        TeacherResourceId = c.Int(nullable: false),
                        HomeWorkId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TeacherResourceId, t.HomeWorkId })
                .ForeignKey("dbo.HomeWorks", t => t.TeacherResourceId, cascadeDelete: true)
                .ForeignKey("dbo.TeacherResources", t => t.HomeWorkId, cascadeDelete: true)
                .Index(t => t.TeacherResourceId)
                .Index(t => t.HomeWorkId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HomeWorks", "UserId", "dbo.Users");
            DropForeignKey("dbo.HomeWorks", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.HomeWorkTeacherResources", "HomeWorkId", "dbo.TeacherResources");
            DropForeignKey("dbo.HomeWorkTeacherResources", "TeacherResourceId", "dbo.HomeWorks");
            DropIndex("dbo.HomeWorkTeacherResources", new[] { "HomeWorkId" });
            DropIndex("dbo.HomeWorkTeacherResources", new[] { "TeacherResourceId" });
            DropIndex("dbo.HomeWorks", new[] { "StudentGroupId" });
            DropIndex("dbo.HomeWorks", new[] { "UserId" });
            DropTable("dbo.HomeWorkTeacherResources");
            DropTable("dbo.HomeWorks");
        }
    }
}
