namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv130_PARP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        Description = c.String(maxLength: 250),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);

            Sql("INSERT INTO [dbo].[Events]([Name],[Description],[Active]) VALUES('Consult�' ,'Consult� un evento' ,1), ('Guard�' ,'Guard� un evento' ,1), ('Actualiz�','Actualiz� un evento',1),('Elimin�' ,'Elimin� un evento' ,1)");
        }
        
        public override void Down()
        {
            DropTable("dbo.Events");
        }
    }
}
