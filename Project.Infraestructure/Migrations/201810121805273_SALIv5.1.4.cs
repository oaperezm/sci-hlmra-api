namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv514 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams");
            CreateTable(
                "dbo.ExamScores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExamScheduleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Score = c.String(),
                        CorrectAnswers = c.Int(),
                        Comments = c.String(maxLength: 500),
                        RegisterDate = c.DateTime(),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExamSchedules", t => t.ExamScheduleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ExamScheduleId)
                .Index(t => t.UserId);
            
            AddForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams");
            DropForeignKey("dbo.ExamScores", "UserId", "dbo.Users");
            DropForeignKey("dbo.ExamScores", "ExamScheduleId", "dbo.ExamSchedules");
            DropIndex("dbo.ExamScores", new[] { "UserId" });
            DropIndex("dbo.ExamScores", new[] { "ExamScheduleId" });
            DropTable("dbo.ExamScores");
            AddForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams", "Id");
        }
    }
}
