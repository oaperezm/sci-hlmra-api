namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv953 : DbMigration
    {
        public override void Up()
        {            
            CreateTable(
                "dbo.UserAccess",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AccessDate = c.DateTime(nullable: false),
                        CCTs = c.String(nullable: false),
                        Roles = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
        }
        
        public override void Down()
        {           
            DropForeignKey("dbo.UserAccess", "UserId", "dbo.Users");
            DropIndex("dbo.UserAccess", new[] { "UserId" });           
            DropTable("dbo.UserAccess");            
        }
    }
}
