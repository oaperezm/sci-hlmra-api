namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv940 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileTypes", "SystemId", c => c.Int());
            CreateIndex("dbo.FileTypes", "SystemId");
            AddForeignKey("dbo.FileTypes", "SystemId", "dbo.Systems", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FileTypes", "SystemId", "dbo.Systems");
            DropIndex("dbo.FileTypes", new[] { "SystemId" });
            DropColumn("dbo.FileTypes", "SystemId");
        }
    }
}
