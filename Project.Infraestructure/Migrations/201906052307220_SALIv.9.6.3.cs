namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv963 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.VideoConference", "StudentGroupId", "dbo.StudentGroups");
            DropIndex("dbo.VideoConference", new[] { "StudentGroupId" });
            AlterColumn("dbo.VideoConference", "StudentGroupId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VideoConference", "StudentGroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.VideoConference", "StudentGroupId");
            AddForeignKey("dbo.VideoConference", "StudentGroupId", "dbo.StudentGroups", "Id");
        }
    }
}
