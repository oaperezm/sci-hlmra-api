namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv932 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tips", "RegisterDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tips", "RegisterDate");
        }
    }
}
