namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv904 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScheduledEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 350),
                        Description = c.String(nullable: false),
                        DateEvent = c.DateTime(nullable: false),
                        InitTime = c.String(nullable: false, maxLength: 5),
                        Duration = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ScheduledEventStudentGroup",
                c => new
                    {
                        ScheduledEventId = c.Int(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ScheduledEventId, t.StudentGroupId })
                .ForeignKey("dbo.ScheduledEvents", t => t.ScheduledEventId)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .Index(t => t.ScheduledEventId)
                .Index(t => t.StudentGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduledEventStudentGroup", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.ScheduledEventStudentGroup", "ScheduledEventId", "dbo.ScheduledEvents");
            DropIndex("dbo.ScheduledEventStudentGroup", new[] { "StudentGroupId" });
            DropIndex("dbo.ScheduledEventStudentGroup", new[] { "ScheduledEventId" });
            DropTable("dbo.ScheduledEventStudentGroup");
            DropTable("dbo.ScheduledEvents");
        }
    }
}
