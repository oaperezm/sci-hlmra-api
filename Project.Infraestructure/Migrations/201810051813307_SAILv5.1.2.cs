namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SAILv512 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ExamScheduleType", newName: "ExamScheduleTypes");
            CreateTable(
                "dbo.TeacherExams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeacherExamTypeId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 450),
                        UserId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TeacherExamType", t => t.TeacherExamTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.TeacherExamTypeId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.TeacherExamQuestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeacherExamId = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.TeacherExams", t => t.TeacherExamId, cascadeDelete: true)
                .Index(t => t.TeacherExamId)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.TeacherExamType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 350),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherExams", "UserId", "dbo.Users");
            DropForeignKey("dbo.TeacherExams", "TeacherExamTypeId", "dbo.TeacherExamType");
            DropForeignKey("dbo.TeacherExamQuestions", "TeacherExamId", "dbo.TeacherExams");
            DropForeignKey("dbo.TeacherExamQuestions", "QuestionId", "dbo.Questions");
            DropIndex("dbo.TeacherExamQuestions", new[] { "QuestionId" });
            DropIndex("dbo.TeacherExamQuestions", new[] { "TeacherExamId" });
            DropIndex("dbo.TeacherExams", new[] { "UserId" });
            DropIndex("dbo.TeacherExams", new[] { "TeacherExamTypeId" });
            DropTable("dbo.TeacherExamType");
            DropTable("dbo.TeacherExamQuestions");
            DropTable("dbo.TeacherExams");
            RenameTable(name: "dbo.ExamScheduleTypes", newName: "ExamScheduleType");
        }
    }
}
