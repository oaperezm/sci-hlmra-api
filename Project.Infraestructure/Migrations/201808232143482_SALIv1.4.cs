namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv14 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Node", "PublishingProfile_Id", "dbo.PublishingProfile");
            DropForeignKey("dbo.Node", "User_Id", "dbo.Users");
            DropIndex("dbo.Node", new[] { "PublishingProfile_Id" });
            DropIndex("dbo.Node", new[] { "User_Id" });
            CreateTable(
                "dbo.PublishingProfileNode",
                c => new
                    {
                        PublishingProfile_Id = c.Int(nullable: false),
                        Node_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PublishingProfile_Id, t.Node_Id })
                .ForeignKey("dbo.PublishingProfile", t => t.PublishingProfile_Id, cascadeDelete: true)
                .ForeignKey("dbo.Node", t => t.Node_Id, cascadeDelete: true)
                .Index(t => t.PublishingProfile_Id)
                .Index(t => t.Node_Id);
            
            DropColumn("dbo.Node", "PublishingProfile_Id");
            DropColumn("dbo.Node", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Node", "User_Id", c => c.Int());
            AddColumn("dbo.Node", "PublishingProfile_Id", c => c.Int());
            DropForeignKey("dbo.PublishingProfileNode", "Node_Id", "dbo.Node");
            DropForeignKey("dbo.PublishingProfileNode", "PublishingProfile_Id", "dbo.PublishingProfile");
            DropIndex("dbo.PublishingProfileNode", new[] { "Node_Id" });
            DropIndex("dbo.PublishingProfileNode", new[] { "PublishingProfile_Id" });
            DropTable("dbo.PublishingProfileNode");
            CreateIndex("dbo.Node", "User_Id");
            CreateIndex("dbo.Node", "PublishingProfile_Id");
            AddForeignKey("dbo.Node", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Node", "PublishingProfile_Id", "dbo.PublishingProfile", "Id");
        }
    }
}
