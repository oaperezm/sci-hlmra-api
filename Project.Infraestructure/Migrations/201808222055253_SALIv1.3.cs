namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv13 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NodeContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContentId = c.Int(nullable: false),
                        NodeId = c.Int(nullable: false),
                        LevelId = c.Int(nullable: false),
                        SubsystemId = c.Int(nullable: false),
                        TrainingFieldId = c.Int(nullable: false),
                        FormativeFieldId = c.Int(nullable: false),
                        GradeId = c.Int(nullable: false),
                        BlockId = c.Int(nullable: false),
                        AreaId = c.Int(nullable: false),
                        PurposeId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.AreaId, cascadeDelete: true)
                .ForeignKey("dbo.Blocks", t => t.BlockId, cascadeDelete: true)
                .ForeignKey("dbo.Contents", t => t.ContentId, cascadeDelete: true)
                .ForeignKey("dbo.FormativeField", t => t.FormativeFieldId, cascadeDelete: true)
                .ForeignKey("dbo.Grade", t => t.GradeId, cascadeDelete: true)
                .ForeignKey("dbo.Level", t => t.LevelId, cascadeDelete: true)
                .ForeignKey("dbo.Location", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Node", t => t.NodeId, cascadeDelete: true)
                .ForeignKey("dbo.Purpose", t => t.PurposeId, cascadeDelete: true)
                .ForeignKey("dbo.Subsystem", t => t.SubsystemId, cascadeDelete: true)
                .ForeignKey("dbo.TrainingField", t => t.TrainingFieldId, cascadeDelete: true)
                .Index(t => t.ContentId)
                .Index(t => t.NodeId)
                .Index(t => t.LevelId)
                .Index(t => t.SubsystemId)
                .Index(t => t.TrainingFieldId)
                .Index(t => t.FormativeFieldId)
                .Index(t => t.GradeId)
                .Index(t => t.BlockId)
                .Index(t => t.AreaId)
                .Index(t => t.PurposeId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Blocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        UrlContent = c.String(nullable: false, maxLength: 500),
                        IsPublic = c.Boolean(),
                        Active = c.Boolean(),
                        ContentTypeId = c.Int(nullable: false),
                        FileTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentTypes", t => t.ContentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.FileTypes", t => t.FileTypeId, cascadeDelete: true)
                .Index(t => t.ContentTypeId)
                .Index(t => t.FileTypeId);
            
            CreateTable(
                "dbo.ContentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FileTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormativeField",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Grade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Level",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Node",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        ParentId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        NodeTypeId = c.Int(nullable: false),
                        PublishingProfile_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeType", t => t.NodeTypeId, cascadeDelete: true)
                .ForeignKey("dbo.PublishingProfile", t => t.PublishingProfile_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.NodeTypeId)
                .Index(t => t.PublishingProfile_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.NodeType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Purpose",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subsystem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TrainingField",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 250),
                        Active = c.Boolean(),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Node", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.PublishingProfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BookAuthor",
                c => new
                    {
                        BookId = c.Int(nullable: false),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BookId, t.AuthorId })
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Authors", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.BookId)
                .Index(t => t.AuthorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Node", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Node", "PublishingProfile_Id", "dbo.PublishingProfile");
            DropForeignKey("dbo.Books", "NodeId", "dbo.Node");
            DropForeignKey("dbo.BookAuthor", "AuthorId", "dbo.Authors");
            DropForeignKey("dbo.BookAuthor", "BookId", "dbo.Books");
            DropForeignKey("dbo.NodeContent", "TrainingFieldId", "dbo.TrainingField");
            DropForeignKey("dbo.NodeContent", "SubsystemId", "dbo.Subsystem");
            DropForeignKey("dbo.NodeContent", "PurposeId", "dbo.Purpose");
            DropForeignKey("dbo.NodeContent", "NodeId", "dbo.Node");
            DropForeignKey("dbo.Node", "NodeTypeId", "dbo.NodeType");
            DropForeignKey("dbo.NodeContent", "LocationId", "dbo.Location");
            DropForeignKey("dbo.NodeContent", "LevelId", "dbo.Level");
            DropForeignKey("dbo.NodeContent", "GradeId", "dbo.Grade");
            DropForeignKey("dbo.NodeContent", "FormativeFieldId", "dbo.FormativeField");
            DropForeignKey("dbo.NodeContent", "ContentId", "dbo.Contents");
            DropForeignKey("dbo.Contents", "FileTypeId", "dbo.FileTypes");
            DropForeignKey("dbo.Contents", "ContentTypeId", "dbo.ContentTypes");
            DropForeignKey("dbo.NodeContent", "BlockId", "dbo.Blocks");
            DropForeignKey("dbo.NodeContent", "AreaId", "dbo.Areas");
            DropIndex("dbo.BookAuthor", new[] { "AuthorId" });
            DropIndex("dbo.BookAuthor", new[] { "BookId" });
            DropIndex("dbo.Books", new[] { "NodeId" });
            DropIndex("dbo.Node", new[] { "User_Id" });
            DropIndex("dbo.Node", new[] { "PublishingProfile_Id" });
            DropIndex("dbo.Node", new[] { "NodeTypeId" });
            DropIndex("dbo.Contents", new[] { "FileTypeId" });
            DropIndex("dbo.Contents", new[] { "ContentTypeId" });
            DropIndex("dbo.NodeContent", new[] { "LocationId" });
            DropIndex("dbo.NodeContent", new[] { "PurposeId" });
            DropIndex("dbo.NodeContent", new[] { "AreaId" });
            DropIndex("dbo.NodeContent", new[] { "BlockId" });
            DropIndex("dbo.NodeContent", new[] { "GradeId" });
            DropIndex("dbo.NodeContent", new[] { "FormativeFieldId" });
            DropIndex("dbo.NodeContent", new[] { "TrainingFieldId" });
            DropIndex("dbo.NodeContent", new[] { "SubsystemId" });
            DropIndex("dbo.NodeContent", new[] { "LevelId" });
            DropIndex("dbo.NodeContent", new[] { "NodeId" });
            DropIndex("dbo.NodeContent", new[] { "ContentId" });
            DropTable("dbo.BookAuthor");
            DropTable("dbo.PublishingProfile");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
            DropTable("dbo.TrainingField");
            DropTable("dbo.Subsystem");
            DropTable("dbo.Purpose");
            DropTable("dbo.NodeType");
            DropTable("dbo.Node");
            DropTable("dbo.Location");
            DropTable("dbo.Level");
            DropTable("dbo.Grade");
            DropTable("dbo.FormativeField");
            DropTable("dbo.FileTypes");
            DropTable("dbo.ContentTypes");
            DropTable("dbo.Contents");
            DropTable("dbo.Blocks");
            DropTable("dbo.NodeContent");
            DropTable("dbo.Areas");
        }
    }
}
