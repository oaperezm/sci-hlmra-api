namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv961 : DbMigration
    {
        public override void Up()
        {
         
            AddColumn("dbo.VideoConference", "StudentGroupId", c => c.Int(nullable: true));
            AddColumn("dbo.VideoConference", "CourseId", c => c.Int(nullable: true));
            AlterColumn("dbo.VideoConference", "StatusId", c => c.Int(nullable: false));
            CreateIndex("dbo.VideoConference", "StudentGroupId");
            CreateIndex("dbo.VideoConference", "CourseId");
            AddForeignKey("dbo.VideoConference", "CourseId", "dbo.Courses", "Id");
            AddForeignKey("dbo.VideoConference", "StudentGroupId", "dbo.StudentGroups", "Id");
            DropColumn("dbo.VideoConference", "InvitedId");

            Sql("ALTER TABLE dbo.VideoConference ADD DEFAULT (0) for CourseId");
            Sql("ALTER TABLE dbo.VideoConference ADD DEFAULT (0) for StudentGroupId");


        }
        
        public override void Down()
        {
            AddColumn("dbo.VideoConference", "InvitedId", c => c.Int());
            
            DropForeignKey("dbo.VideoConference", "StudentGroupId", "dbo.StudentGroups");
          
            DropForeignKey("dbo.VideoConference", "CourseId", "dbo.Courses");
           
            DropIndex("dbo.VideoConference", new[] { "CourseId" });
            DropIndex("dbo.VideoConference", new[] { "StudentGroupId" });
            AlterColumn("dbo.VideoConference", "StatusId", c => c.Boolean(nullable: false));
            
            DropColumn("dbo.VideoConference", "CourseId");
            DropColumn("dbo.VideoConference", "StudentGroupId");
            
        }
    }
}
