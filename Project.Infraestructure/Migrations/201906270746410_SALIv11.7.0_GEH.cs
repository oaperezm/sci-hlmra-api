namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1170_GEH : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentScore",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Homework = c.Single(),
                        Exam = c.Single(nullable: false),
                        Assistance = c.Single(),
                        Activity = c.Single(nullable: false),
                        Score = c.Single(nullable: false),
                        NumberPartial = c.Int(),
                        Points = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        RegisterDate = c.DateTime(),
                        Comment = c.String(maxLength: 500),
                        StudentGroupId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        ExamScheduleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExamSchedules", t => t.ExamScheduleId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.StudentGroupId)
                .Index(t => t.StudentId)
                .Index(t => t.UserId)
                .Index(t => t.ExamScheduleId);
            
            //AddColumn("dbo.HomeWorkAnswers", "PartialEvaluation", c => c.Int(nullable: false));
            //AddColumn("dbo.HomeWorkAnswers", "StatusHomework", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentScore", "UserId", "dbo.Users");
            DropForeignKey("dbo.StudentScore", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.StudentScore", "StudentId", "dbo.Students");
            DropForeignKey("dbo.StudentScore", "ExamScheduleId", "dbo.ExamSchedules");
            DropIndex("dbo.StudentScore", new[] { "ExamScheduleId" });
            DropIndex("dbo.StudentScore", new[] { "UserId" });
            DropIndex("dbo.StudentScore", new[] { "StudentId" });
            DropIndex("dbo.StudentScore", new[] { "StudentGroupId" });
            //DropColumn("dbo.HomeWorkAnswers", "StatusHomework");
            //DropColumn("dbo.HomeWorkAnswers", "PartialEvaluation");
            DropTable("dbo.StudentScore");
        }
    }
}
