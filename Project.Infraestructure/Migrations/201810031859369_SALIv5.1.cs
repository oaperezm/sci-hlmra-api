namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv51 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExamScheduleType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 350),
                        Active = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ExamSchedules", "ExamScheduleTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.ExamSchedules", "ExamScheduleTypeId");
            AddForeignKey("dbo.ExamSchedules", "ExamScheduleTypeId", "dbo.ExamScheduleType", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExamSchedules", "ExamScheduleTypeId", "dbo.ExamScheduleType");
            DropIndex("dbo.ExamSchedules", new[] { "ExamScheduleTypeId" });
            DropColumn("dbo.ExamSchedules", "ExamScheduleTypeId");
            DropTable("dbo.ExamScheduleType");
        }
    }
}
