namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv937 : DbMigration
    {
        public override void Up()
        {          
        
            
            AddColumn("dbo.Nodes", "Depth", c => c.Int());
            AddColumn("dbo.Nodes", "Pathindex", c => c.Int());
            AddColumn("dbo.Nodes", "Numericalmapping", c => c.String());
            AddColumn("dbo.Nodes", "SystemId", c => c.Int());           
            CreateIndex("dbo.Nodes", "SystemId");
            AddForeignKey("dbo.Nodes", "SystemId", "dbo.Systems", "Id");           
        }
        
        public override void Down()
        {                       
      
            DropForeignKey("dbo.Nodes", "SystemId", "dbo.Systems");
            DropIndex("dbo.Nodes", new[] { "SystemId" });          
            DropColumn("dbo.Nodes", "SystemId");
            DropColumn("dbo.Nodes", "Numericalmapping");
            DropColumn("dbo.Nodes", "Pathindex");
            DropColumn("dbo.Nodes", "Depth");         
           
           
        }
    }
}
