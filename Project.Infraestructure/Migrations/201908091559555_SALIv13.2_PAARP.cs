namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv132_PAARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventsReport", "ResourcenName", c => c.String(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventsReport", "ResourcenName");
        }
    }
}
