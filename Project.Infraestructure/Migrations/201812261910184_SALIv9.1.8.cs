namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv918 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "Thumbnails", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contents", "Thumbnails");
        }
    }
}
