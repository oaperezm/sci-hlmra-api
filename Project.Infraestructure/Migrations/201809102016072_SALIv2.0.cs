namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv20 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "LevelId", c => c.Int());
            AddColumn("dbo.Users", "AreaId", c => c.Int());
            AddColumn("dbo.Users", "SubsystemId", c => c.Int());
            AddColumn("dbo.Users", "Semester", c => c.Int());
            AddColumn("dbo.Users", "UrlImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "UrlImage");
            DropColumn("dbo.Users", "Semester");
            DropColumn("dbo.Users", "SubsystemId");
            DropColumn("dbo.Users", "AreaId");
            DropColumn("dbo.Users", "LevelId");
        }
    }
}
