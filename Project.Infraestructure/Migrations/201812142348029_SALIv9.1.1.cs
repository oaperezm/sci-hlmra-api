namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv911 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "Institution", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "Institution");
        }
    }
}
