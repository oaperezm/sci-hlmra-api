namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv929 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LinkInterests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 250),
                        Description = c.String(maxLength: 500),
                        Uri = c.String(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .Index(t => t.CourseId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LinkInterests", "CourseId", "dbo.Courses");
            DropIndex("dbo.LinkInterests", new[] { "CourseId" });
            DropTable("dbo.LinkInterests");
        }
    }
}
