namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5116 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegisterDate = c.DateTime(nullable: false),
                        Score = c.Decimal(precision: 3, scale: 2),
                        IsCompleted = c.Boolean(nullable: false),
                        ClientId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        ExamScheduleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExamSchedules", t => t.ExamScheduleId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ExamScheduleId);
            
            CreateTable(
                "dbo.TestAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TestId = c.Int(nullable: false),
                        AnswerId = c.Int(),
                        Explanation = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Answers", t => t.AnswerId)
                .ForeignKey("dbo.Tests", t => t.TestId)
                .Index(t => t.TestId)
                .Index(t => t.AnswerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tests", "UserId", "dbo.Users");
            DropForeignKey("dbo.Tests", "ExamScheduleId", "dbo.ExamSchedules");
            DropForeignKey("dbo.TestAnswers", "TestId", "dbo.Tests");
            DropForeignKey("dbo.TestAnswers", "AnswerId", "dbo.Answers");
            DropIndex("dbo.TestAnswers", new[] { "AnswerId" });
            DropIndex("dbo.TestAnswers", new[] { "TestId" });
            DropIndex("dbo.Tests", new[] { "ExamScheduleId" });
            DropIndex("dbo.Tests", new[] { "UserId" });
            DropTable("dbo.TestAnswers");
            DropTable("dbo.Tests");
        }
    }
}
