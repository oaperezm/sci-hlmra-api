namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv126_JVH : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentScore", "ExamScheduleId", "dbo.ExamSchedules");
            DropIndex("dbo.StudentScore", new[] { "ExamScheduleId" });
            AddColumn("dbo.StudentScore", "SchedulingPartialId", c => c.Int());
            CreateIndex("dbo.StudentScore", "SchedulingPartialId");
            AddForeignKey("dbo.StudentScore", "SchedulingPartialId", "dbo.SchedulingPartial", "Id");
            DropColumn("dbo.StudentScore", "NumberPartial");
            DropColumn("dbo.StudentScore", "ExamScheduleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StudentScore", "ExamScheduleId", c => c.Int(nullable: false));
            AddColumn("dbo.StudentScore", "NumberPartial", c => c.Int());
            DropForeignKey("dbo.StudentScore", "SchedulingPartialId", "dbo.SchedulingPartial");
            DropIndex("dbo.StudentScore", new[] { "SchedulingPartialId" });
            DropColumn("dbo.StudentScore", "SchedulingPartialId");
            CreateIndex("dbo.StudentScore", "ExamScheduleId");
            AddForeignKey("dbo.StudentScore", "ExamScheduleId", "dbo.ExamSchedules", "Id", cascadeDelete: true);
        }
    }
}
