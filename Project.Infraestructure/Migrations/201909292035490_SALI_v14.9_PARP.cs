namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v149_PARP : DbMigration
    {
        public override void Up()
        {
            //DropColumn("dbo.V_ReportTeacher", "CheckDate");
            //RenameColumn(table: "dbo.V_ReportTeacher", name: "CheckDate1", newName: "CheckDate");
            //CreateTable(
            //    "dbo.V_ReportProfileTeacher",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Email = c.String(),
            //            FullName = c.String(),
            //            NameSchool = c.String(),
            //            Level = c.String(),
            //            Subject = c.String(),
            //            Groups = c.String(),
            //            ElapsedDays = c.Int(nullable: false),
            //            Period = c.String(),
            //            vpActivity = c.Single(nullable: false),
            //            totalSchedulesExam = c.Int(nullable: false),
            //            BaseQuestion = c.Int(nullable: false),
            //            GeneratedQuestion = c.Int(nullable: false),
            //            totalQuestion = c.Int(nullable: false),
            //            ActivityScheduled = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);

            //AddColumn("dbo.V_ReportTeacher", "PHomework", c => c.Single(nullable: false));
            //AddColumn("dbo.V_ReportTeacher", "PExam", c => c.Single(nullable: false));
            //AddColumn("dbo.V_ReportTeacher", "PAssistance", c => c.Single(nullable: false));
            //AddColumn("dbo.V_ReportTeacher", "PActivity", c => c.Single(nullable: false));
            //AlterColumn("dbo.V_ReportTeacher", "CheckDate", c => c.DateTime(nullable: false));
            //DropColumn("dbo.V_ReportTeacher", "Exam");
            //DropColumn("dbo.V_ReportTeacher", "Assistance");
            //DropColumn("dbo.V_ReportTeacher", "Activity");
           
            Sql("CREATE VIEW dbo.V_ReportProfileTeacher AS select " +
                "Description from dbo.Nodes where ParentId=148");

        }
        
        public override void Down()
        {

            Sql(" drop VIEW dbo.V_ReportProfileTeacher");
            //AddColumn("dbo.V_ReportTeacher", "Activity", c => c.Int(nullable: false));
            //AddColumn("dbo.V_ReportTeacher", "Assistance", c => c.Int(nullable: false));
            //AddColumn("dbo.V_ReportTeacher", "Exam", c => c.Int(nullable: false));
            //AlterColumn("dbo.V_ReportTeacher", "CheckDate", c => c.Int(nullable: false));
            //DropColumn("dbo.V_ReportTeacher", "PActivity");
            //DropColumn("dbo.V_ReportTeacher", "PAssistance");
            //DropColumn("dbo.V_ReportTeacher", "PExam");
            //DropColumn("dbo.V_ReportTeacher", "PHomework");
            //DropTable("dbo.V_ReportProfileTeacher");
            //RenameColumn(table: "dbo.V_ReportTeacher", name: "CheckDate", newName: "CheckDate1");
            //AddColumn("dbo.V_ReportTeacher", "CheckDate", c => c.Int(nullable: false));
        }
    }
}
