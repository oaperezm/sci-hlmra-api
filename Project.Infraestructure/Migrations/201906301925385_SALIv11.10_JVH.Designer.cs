// <auto-generated />
namespace Project.Infraestructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALIv1110_JVH : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALIv1110_JVH));
        
        string IMigrationMetadata.Id
        {
            get { return "201906301925385_SALIv11.10_JVH"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
