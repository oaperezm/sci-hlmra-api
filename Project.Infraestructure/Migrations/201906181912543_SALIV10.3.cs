namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIV103 : DbMigration
    {
        public override void Up()
        {
            // adding new columns to Homeworks table
            AddColumn("dbo.HomeWorks", "OpeningDate", c => c.DateTime());
            AddColumn("dbo.HomeWorks", "TypeOfQualification", c => c.Int());
            AddColumn("dbo.HomeWorks", "AssignedQualification", c => c.Int());

            //  adding new columns to HomeworkAnswers table
            AddColumn("dbo.HomeWorkAnswers", "Delivered", c => c.Int());
            AddColumn("dbo.HomeWorkAnswers", "AddNewAnswers", c => c.Int());
        }

        public override void Down()
        {
            DropColumn("dbo.HomeWorkAnswers", "AddNewAnswers");
            DropColumn("dbo.HomeWorkAnswers", "Delivered");
            DropColumn("dbo.HomeWorks", "AssignedQualification");
            DropColumn("dbo.HomeWorks", "TypeOfQualification");
            DropColumn("dbo.HomeWorks", "OpeningDate");
        }
    }
}
