namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv913 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "RegisterDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Books", "UpdateDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "UpdateDate");
            DropColumn("dbo.Books", "RegisterDate");
        }
    }
}
