namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv137_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AreasCurriculumAutonomy", "EducationLevelId", c => c.Int(nullable: false));
            AddColumn("dbo.Keylearning", "EducationLevelId", c => c.Int(nullable: false));
            AddColumn("dbo.Learningexpected", "EducationLevelId", c => c.Int(nullable: false));
            CreateIndex("dbo.AreasCurriculumAutonomy", "EducationLevelId");
            CreateIndex("dbo.Keylearning", "EducationLevelId");
            CreateIndex("dbo.Learningexpected", "EducationLevelId");
            AddForeignKey("dbo.AreasCurriculumAutonomy", "EducationLevelId", "dbo.EducationLevel", "Id");
            AddForeignKey("dbo.Keylearning", "EducationLevelId", "dbo.EducationLevel", "Id");
            AddForeignKey("dbo.Learningexpected", "EducationLevelId", "dbo.EducationLevel", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Learningexpected", "EducationLevelId", "dbo.EducationLevel");
            DropForeignKey("dbo.Keylearning", "EducationLevelId", "dbo.EducationLevel");
            DropForeignKey("dbo.AreasCurriculumAutonomy", "EducationLevelId", "dbo.EducationLevel");
            DropIndex("dbo.Learningexpected", new[] { "EducationLevelId" });
            DropIndex("dbo.Keylearning", new[] { "EducationLevelId" });
            DropIndex("dbo.AreasCurriculumAutonomy", new[] { "EducationLevelId" });
            DropColumn("dbo.Learningexpected", "EducationLevelId");
            DropColumn("dbo.Keylearning", "EducationLevelId");
            DropColumn("dbo.AreasCurriculumAutonomy", "EducationLevelId");
        }
    }
}
