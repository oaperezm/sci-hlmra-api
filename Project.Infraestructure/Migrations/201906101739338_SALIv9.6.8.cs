namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv968 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoConference", "UserId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VideoConference", "UserId");
        }
    }
}
