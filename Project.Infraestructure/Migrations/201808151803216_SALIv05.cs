namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv05 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Permission", newName: "Permits");
            AlterColumn("dbo.Permits", "Module", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Permits", "Description", c => c.String(maxLength: 250));
            AlterColumn("dbo.Permits", "Active", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Permits", "Active", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Permits", "Description", c => c.String());
            AlterColumn("dbo.Permits", "Module", c => c.String());
            RenameTable(name: "dbo.Permits", newName: "Permission");
        }
    }
}
