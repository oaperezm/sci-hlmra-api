namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv138_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", c => c.Int(nullable: true));
            AddColumn("dbo.Axis", "EducationLevelId", c => c.Int(nullable: false));
            CreateIndex("dbo.AreaPersonalSocialDevelopment", "ContentTypeId");
            CreateIndex("dbo.Axis", "EducationLevelId");
            AddForeignKey("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", "dbo.ContentTypes", "Id");
            AddForeignKey("dbo.Axis", "EducationLevelId", "dbo.EducationLevel", "Id");
            Sql(" UPDATE dbo.AreaPersonalSocialDevelopment SET ContentTypeId = 4  where isnull(ContentTypeId,0)= 0");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Axis", "EducationLevelId", "dbo.EducationLevel");
            DropForeignKey("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", "dbo.ContentTypes");
            DropIndex("dbo.Axis", new[] { "EducationLevelId" });
            DropIndex("dbo.AreaPersonalSocialDevelopment", new[] { "ContentTypeId" });
            DropColumn("dbo.Axis", "EducationLevelId");
            DropColumn("dbo.AreaPersonalSocialDevelopment", "ContentTypeId");
        }
    }
}
