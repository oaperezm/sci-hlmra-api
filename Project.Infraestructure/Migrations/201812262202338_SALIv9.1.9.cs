namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv919 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeContents", "IsPublic", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeContents", "IsPublic");
        }
    }
}
