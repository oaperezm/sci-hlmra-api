namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SAILv09 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Key", c => c.String(maxLength: 60));
            AlterColumn("dbo.Users", "Institution", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Institution", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Users", "Key", c => c.String(nullable: false, maxLength: 60));
        }
    }
}
