namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v145_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventsReport", "EducationLevelId", c => c.Int(nullable: true));
            CreateIndex("dbo.EventsReport", "EducationLevelId");
            AddForeignKey("dbo.EventsReport", "EducationLevelId", "dbo.EducationLevel", "Id");
            //DropColumn("dbo.V_ReportExam", "endDate");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.V_ReportExam", "endDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.EventsReport", "EducationLevelId", "dbo.EducationLevel");
            DropIndex("dbo.EventsReport", new[] { "EducationLevelId" });
            DropColumn("dbo.EventsReport", "EducationLevelId");
        }
    }
}
