namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv909 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotificationMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Module = c.String(nullable: false, maxLength: 350),
                        Action = c.String(nullable: false, maxLength: 250),
                        Generate = c.String(nullable: false, maxLength: 250),
                        Receiver = c.String(nullable: false, maxLength: 250),
                        Title = c.String(nullable: false, maxLength: 300),
                        Message = c.String(nullable: false, maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NotificationMessages");
        }
    }
}
