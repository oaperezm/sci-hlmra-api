namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Areas", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Blocks", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Contents", "ContentType_Id", "dbo.ContentTypes");
            DropForeignKey("dbo.ContentTypes", "Content_Id", "dbo.Contents");
            DropForeignKey("dbo.Contents", "FileType_Id", "dbo.FileTypes");
            DropForeignKey("dbo.FileTypes", "Content_Id", "dbo.Contents");
            DropForeignKey("dbo.Contents", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.FormativeField", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Grade", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Level", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Location", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.NodeType", "Node_Id", "dbo.Node");
            DropForeignKey("dbo.Node", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Purpose", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Subsystem", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.TrainingField", "NodeContent_Id", "dbo.NodeContent");
            DropForeignKey("dbo.Node", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.NodeContent", "Area_Id", "dbo.Areas");
            DropIndex("dbo.Areas", new[] { "NodeContent_Id" });
            DropIndex("dbo.NodeContent", new[] { "AreaId" });
            DropIndex("dbo.NodeContent", new[] { "Area_Id" });
            DropIndex("dbo.Blocks", new[] { "NodeContent_Id" });
            DropIndex("dbo.Contents", new[] { "ContentType_Id" });
            DropIndex("dbo.Contents", new[] { "FileType_Id" });
            DropIndex("dbo.Contents", new[] { "NodeContent_Id" });
            DropIndex("dbo.ContentTypes", new[] { "Content_Id" });
            DropIndex("dbo.FileTypes", new[] { "Content_Id" });
            DropIndex("dbo.FormativeField", new[] { "NodeContent_Id" });
            DropIndex("dbo.Grade", new[] { "NodeContent_Id" });
            DropIndex("dbo.Level", new[] { "NodeContent_Id" });
            DropIndex("dbo.Location", new[] { "NodeContent_Id" });
            DropIndex("dbo.Node", new[] { "NodeContent_Id" });
            DropIndex("dbo.Node", new[] { "Book_Id" });
            DropIndex("dbo.NodeType", new[] { "Node_Id" });
            DropIndex("dbo.Purpose", new[] { "NodeContent_Id" });
            DropIndex("dbo.Subsystem", new[] { "NodeContent_Id" });
            DropIndex("dbo.TrainingField", new[] { "NodeContent_Id" });
            DropColumn("dbo.NodeContent", "AreaId");
            RenameColumn(table: "dbo.NodeContent", name: "Area_Id", newName: "AreaId");
            AlterColumn("dbo.NodeContent", "AreaId", c => c.Int(nullable: false));
            CreateIndex("dbo.NodeContent", "AreaId");
            AddForeignKey("dbo.NodeContent", "AreaId", "dbo.Areas", "Id", cascadeDelete: true);
            DropColumn("dbo.Areas", "NodeContent_Id");
            DropColumn("dbo.Blocks", "NodeContent_Id");
            DropColumn("dbo.Contents", "ContentType_Id");
            DropColumn("dbo.Contents", "FileType_Id");
            DropColumn("dbo.Contents", "NodeContent_Id");
            DropColumn("dbo.ContentTypes", "Content_Id");
            DropColumn("dbo.FileTypes", "Content_Id");
            DropColumn("dbo.FormativeField", "NodeContent_Id");
            DropColumn("dbo.Grade", "NodeContent_Id");
            DropColumn("dbo.Level", "NodeContent_Id");
            DropColumn("dbo.Location", "NodeContent_Id");
            DropColumn("dbo.Node", "NodeContent_Id");
            DropColumn("dbo.Node", "Book_Id");
            DropColumn("dbo.NodeType", "Node_Id");
            DropColumn("dbo.Purpose", "NodeContent_Id");
            DropColumn("dbo.Subsystem", "NodeContent_Id");
            DropColumn("dbo.TrainingField", "NodeContent_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TrainingField", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Subsystem", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Purpose", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.NodeType", "Node_Id", c => c.Int());
            AddColumn("dbo.Node", "Book_Id", c => c.Int());
            AddColumn("dbo.Node", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Location", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Level", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Grade", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.FormativeField", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.FileTypes", "Content_Id", c => c.Int());
            AddColumn("dbo.ContentTypes", "Content_Id", c => c.Int());
            AddColumn("dbo.Contents", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Contents", "FileType_Id", c => c.Int());
            AddColumn("dbo.Contents", "ContentType_Id", c => c.Int());
            AddColumn("dbo.Blocks", "NodeContent_Id", c => c.Int());
            AddColumn("dbo.Areas", "NodeContent_Id", c => c.Int());
            DropForeignKey("dbo.NodeContent", "AreaId", "dbo.Areas");
            DropIndex("dbo.NodeContent", new[] { "AreaId" });
            AlterColumn("dbo.NodeContent", "AreaId", c => c.Int());
            RenameColumn(table: "dbo.NodeContent", name: "AreaId", newName: "Area_Id");
            AddColumn("dbo.NodeContent", "AreaId", c => c.Int(nullable: false));
            CreateIndex("dbo.TrainingField", "NodeContent_Id");
            CreateIndex("dbo.Subsystem", "NodeContent_Id");
            CreateIndex("dbo.Purpose", "NodeContent_Id");
            CreateIndex("dbo.NodeType", "Node_Id");
            CreateIndex("dbo.Node", "Book_Id");
            CreateIndex("dbo.Node", "NodeContent_Id");
            CreateIndex("dbo.Location", "NodeContent_Id");
            CreateIndex("dbo.Level", "NodeContent_Id");
            CreateIndex("dbo.Grade", "NodeContent_Id");
            CreateIndex("dbo.FormativeField", "NodeContent_Id");
            CreateIndex("dbo.FileTypes", "Content_Id");
            CreateIndex("dbo.ContentTypes", "Content_Id");
            CreateIndex("dbo.Contents", "NodeContent_Id");
            CreateIndex("dbo.Contents", "FileType_Id");
            CreateIndex("dbo.Contents", "ContentType_Id");
            CreateIndex("dbo.Blocks", "NodeContent_Id");
            CreateIndex("dbo.NodeContent", "Area_Id");
            CreateIndex("dbo.NodeContent", "AreaId");
            CreateIndex("dbo.Areas", "NodeContent_Id");
            AddForeignKey("dbo.NodeContent", "Area_Id", "dbo.Areas", "Id");
            AddForeignKey("dbo.Node", "Book_Id", "dbo.Books", "Id");
            AddForeignKey("dbo.TrainingField", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Subsystem", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Purpose", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Node", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.NodeType", "Node_Id", "dbo.Node", "Id");
            AddForeignKey("dbo.Location", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Level", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Grade", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.FormativeField", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Contents", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.FileTypes", "Content_Id", "dbo.Contents", "Id");
            AddForeignKey("dbo.Contents", "FileType_Id", "dbo.FileTypes", "Id");
            AddForeignKey("dbo.ContentTypes", "Content_Id", "dbo.Contents", "Id");
            AddForeignKey("dbo.Contents", "ContentType_Id", "dbo.ContentTypes", "Id");
            AddForeignKey("dbo.Blocks", "NodeContent_Id", "dbo.NodeContent", "Id");
            AddForeignKey("dbo.Areas", "NodeContent_Id", "dbo.NodeContent", "Id");
        }
    }
}
