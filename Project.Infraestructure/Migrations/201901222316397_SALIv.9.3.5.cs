namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv935 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionBanks", "SystemId", c => c.Int());
            AddColumn("dbo.Contents", "SystemId", c => c.Int());
            AddColumn("dbo.PublishingProfiles", "SystemId", c => c.Int());
            AddColumn("dbo.Permissions", "SystemId", c => c.Int());
            CreateIndex("dbo.QuestionBanks", "SystemId");
            CreateIndex("dbo.Contents", "SystemId");
            CreateIndex("dbo.PublishingProfiles", "SystemId");
            CreateIndex("dbo.Permissions", "SystemId");
            AddForeignKey("dbo.Contents", "SystemId", "dbo.Systems", "Id");
            AddForeignKey("dbo.PublishingProfiles", "SystemId", "dbo.Systems", "Id");
            AddForeignKey("dbo.Permissions", "SystemId", "dbo.Systems", "Id");
            AddForeignKey("dbo.QuestionBanks", "SystemId", "dbo.Systems", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionBanks", "SystemId", "dbo.Systems");
            DropForeignKey("dbo.Permissions", "SystemId", "dbo.Systems");
            DropForeignKey("dbo.PublishingProfiles", "SystemId", "dbo.Systems");
            DropForeignKey("dbo.Contents", "SystemId", "dbo.Systems");
            DropIndex("dbo.Permissions", new[] { "SystemId" });
            DropIndex("dbo.PublishingProfiles", new[] { "SystemId" });
            DropIndex("dbo.Contents", new[] { "SystemId" });
            DropIndex("dbo.QuestionBanks", new[] { "SystemId" });
            DropColumn("dbo.Permissions", "SystemId");
            DropColumn("dbo.PublishingProfiles", "SystemId");
            DropColumn("dbo.Contents", "SystemId");
            DropColumn("dbo.QuestionBanks", "SystemId");
        }
    }
}
