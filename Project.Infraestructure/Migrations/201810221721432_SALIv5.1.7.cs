namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv517 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Permissions", "ShowInMenu", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Permissions", "ShowInMenu");
        }
    }
}
