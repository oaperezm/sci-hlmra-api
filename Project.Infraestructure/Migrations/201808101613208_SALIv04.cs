namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv04 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.RolPermission", newName: "RolePermission");
            RenameColumn(table: "dbo.RolePermission", name: "RolId", newName: "RoleId");
            RenameColumn(table: "dbo.Users", name: "RolId", newName: "RoleId");
            RenameIndex(table: "dbo.Users", name: "IX_RolId", newName: "IX_RoleId");
            RenameIndex(table: "dbo.RolePermission", name: "IX_RolId", newName: "IX_RoleId");
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Users", "LastName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Users", "MiddleName", c => c.String(maxLength: 150));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "MiddleName", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false));
            RenameIndex(table: "dbo.RolePermission", name: "IX_RoleId", newName: "IX_RolId");
            RenameIndex(table: "dbo.Users", name: "IX_RoleId", newName: "IX_RolId");
            RenameColumn(table: "dbo.Users", name: "RoleId", newName: "RolId");
            RenameColumn(table: "dbo.RolePermission", name: "RoleId", newName: "RolId");
            RenameTable(name: "dbo.RolePermission", newName: "RolPermission");
        }
    }
}
