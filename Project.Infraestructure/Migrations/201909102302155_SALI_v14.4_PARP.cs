namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v144_PARP : DbMigration
    {
        public override void Up()
        {
            //RenameColumn(table: "dbo.V_ReportCourse", name: "endDate", newName: "__mig_tmp__0");
            //RenameColumn(table: "dbo.V_ReportCourse", name: "endDate1", newName: "endDate");
            //RenameColumn(table: "dbo.V_ReportCourse", name: "__mig_tmp__0", newName: "nameSchool");
            //CreateTable(
            //    "dbo.V_ReportExam",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            cct = c.String(),
            //            userName = c.String(),
            //            email = c.String(),
            //            nameSchool = c.String(),
            //            level = c.String(),
            //            totalquestion = c.Int(nullable: false),
            //            questionbank = c.Int(nullable: false),
            //            signature = c.String(),
            //            starDate = c.DateTime(nullable: false),
            //            endDate = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);

            //AlterColumn("dbo.V_ReportCourse", "endDate", c => c.DateTime(nullable: false));
            //AlterColumn("dbo.V_ReportCourse", "group", c => c.String());

            
            Sql("CREATE VIEW dbo.V_ReportExam AS select Description from dbo.Nodes where ParentId=148");
        }
        
        public override void Down()
        {
            //AlterColumn("dbo.V_ReportCourse", "group", c => c.Int(nullable: false));
            //AlterColumn("dbo.V_ReportCourse", "endDate", c => c.String());
            //DropTable("dbo.V_ReportExam");
            //RenameColumn(table: "dbo.V_ReportCourse", name: "nameSchool", newName: "__mig_tmp__0");
            //RenameColumn(table: "dbo.V_ReportCourse", name: "endDate", newName: "endDate1");
            //RenameColumn(table: "dbo.V_ReportCourse", name: "__mig_tmp__0", newName: "endDate");
            Sql(" drop VIEW dbo.V_ReportExam");
        }
    }
}
