namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ALIv906 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduledEvents", "EndEvent", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduledEvents", "EndEvent");
        }
    }
}
