namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv942 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "Published", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "Published");
        }
    }
}
