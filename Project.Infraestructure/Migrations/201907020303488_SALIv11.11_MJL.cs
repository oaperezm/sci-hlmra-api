namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1111_MJL : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Announcements", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.NoticeStudentGroup", "NoticeId", "dbo.Announcements");
            DropForeignKey("dbo.NoticeStudentGroup", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.Announcements", "UserId", "dbo.Users");
            DropIndex("dbo.Announcements", new[] { "CourseId" });
            DropIndex("dbo.NoticeStudentGroup", new[] { "NoticeId" });
            DropIndex("dbo.NoticeStudentGroup", new[] { "StudentGroupId" });
            CreateTable(
                "dbo.AnnouncementsStudentGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnnouncementId = c.Int(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Announcements", t => t.AnnouncementId, cascadeDelete: true)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId, cascadeDelete: true)
                .Index(t => t.AnnouncementId)
                .Index(t => t.StudentGroupId);

            //DropTable("dbo.AnnouncementsCourse");
            DropTable("dbo.NoticeStudentGroup");
            DropColumn("dbo.Announcements", "CourseId");
            //DropTable("dbo.AnnouncementStudentGroup");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.NoticeStudentGroup",
                c => new
                    {
                        NoticeId = c.Int(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NoticeId, t.StudentGroupId });
            
            CreateTable(
                "dbo.Announcements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 350),
                        CourseId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.AnnouncementsStudentGroups", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.Announcements", "UserId", "dbo.Users");
            DropForeignKey("dbo.AnnouncementsStudentGroups", "AnnouncementId", "dbo.Announcements");
            DropIndex("dbo.AnnouncementsStudentGroups", new[] { "StudentGroupId" });
            DropIndex("dbo.AnnouncementsStudentGroups", new[] { "AnnouncementId" });
            DropTable("dbo.Announcements");
            DropTable("dbo.AnnouncementsStudentGroups");
            CreateIndex("dbo.NoticeStudentGroup", "StudentGroupId");
            CreateIndex("dbo.NoticeStudentGroup", "NoticeId");
            CreateIndex("dbo.Announcements", "CourseId");
            AddForeignKey("dbo.Announcements", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.NoticeStudentGroup", "StudentGroupId", "dbo.StudentGroups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NoticeStudentGroup", "NoticeId", "dbo.Announcements", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Announcements", "CourseId", "dbo.Courses", "Id");
        }
    }
}
