namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv925 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ForumPosts", "BestAnswer", c => c.Boolean());
            AddColumn("dbo.ForumPosts", "Votes", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ForumPosts", "Votes");
            DropColumn("dbo.ForumPosts", "BestAnswer");
        }
    }
}
