namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv425 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExamSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 350),
                        ApplicationDate = c.DateTime(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        Active = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.StudentGroupId)
                .Index(t => t.UserId);
            
            AlterColumn("dbo.NodeTypes", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExamSchedules", "UserId", "dbo.Users");
            DropForeignKey("dbo.ExamSchedules", "StudentGroupId", "dbo.StudentGroups");
            DropIndex("dbo.ExamSchedules", new[] { "UserId" });
            DropIndex("dbo.ExamSchedules", new[] { "StudentGroupId" });
            AlterColumn("dbo.NodeTypes", "Active", c => c.Int(nullable: false));
            DropTable("dbo.ExamSchedules");
        }
    }
}
