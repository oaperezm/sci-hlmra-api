namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv951 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 350),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 350),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Users", "StateId", c => c.Int());
            CreateIndex("dbo.Users", "StateId");
            AddForeignKey("dbo.Users", "StateId", "dbo.States", "Id");
            DropColumn("dbo.Users", "State");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "State", c => c.String(nullable: false, maxLength: 150));
            DropForeignKey("dbo.Users", "StateId", "dbo.States");
            DropForeignKey("dbo.States", "CityId", "dbo.Cities");
            DropIndex("dbo.States", new[] { "CityId" });
            DropIndex("dbo.Users", new[] { "StateId" });
            DropColumn("dbo.Users", "StateId");
            DropTable("dbo.Cities");
            DropTable("dbo.States");
        }
    }
}
