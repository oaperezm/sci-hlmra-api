namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v143_PARP : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.V_ReportCourse",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            cct = c.String(),
            //            level = c.String(),
            //            userName = c.String(),
            //            email = c.String(),
            //            endDate = c.String(),
            //            courseRegisterDate = c.DateTime(nullable: false),
            //            courseId = c.Int(nullable: false),
            //            courseName = c.String(),
            //            group = c.Int(nullable: false),
            //            startDate = c.DateTime(nullable: false),
            //            endDate1 = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);

            //AddColumn("dbo.V_CoordinatorReport", "startDate", c => c.DateTime(nullable: false));
            //AddColumn("dbo.V_CoordinatorReport", "endDate", c => c.DateTime(nullable: false));

            Sql("CREATE VIEW dbo.V_ReportCourse AS select 0 as Id,CCT.CCT as [cct], u.Email as [email],u.FullName as [userName],u.Institution as [nameSchool],n.Description as [level],c.RegisterDate as [courseRegisterDate],c.Id as [courseId],c.Name  as [courseName],sp.Description as [group],c.StartDate as [startDate],c.EndDate as [endDate]" +
                "from dbo.Users u "+
                "inner join [dbo].[UserCCT] cct on  u.Id=cct.UserId " +
                "inner  join dbo.Roles r  on  r.Id=u.RoleId " +
                "left join dbo.Courses c on c.UserId=u.Id " +
                "left join [dbo].[Nodes] n  on c.NodeId=n.Id  " +
                "left join dbo.StudentGroups sp on sp.CourseId= c.Id " +
                "left join dbo.Students s on sp.Id= s.StudentGroupId and s.StudentStatusId=2 ");

        }
        
        public override void Down()
        {
            Sql("drop VIEW dbo.dbo.V_ReportCourse");
        }
    }
}
