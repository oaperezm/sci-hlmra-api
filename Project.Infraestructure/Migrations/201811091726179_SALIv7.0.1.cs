namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv701 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "UrlImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "UrlImage");
        }
    }
}
