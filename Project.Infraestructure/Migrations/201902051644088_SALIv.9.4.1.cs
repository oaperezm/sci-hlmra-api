namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv941 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "Public", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "Public");
        }
    }
}
