namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv959 : DbMigration
    {
        public override void Up()
        {
                      
            AlterColumn("dbo.VideoConference", "Duration", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
           
            AlterColumn("dbo.VideoConference", "Duration", c => c.Time(nullable: false, precision: 7));
          
        }
    }
}
