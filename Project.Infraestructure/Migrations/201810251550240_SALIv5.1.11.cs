namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5111 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionBanks", "UserId", c => c.Int());
            CreateIndex("dbo.QuestionBanks", "UserId");
            AddForeignKey("dbo.QuestionBanks", "UserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionBanks", "UserId", "dbo.Users");
            DropIndex("dbo.QuestionBanks", new[] { "UserId" });
            DropColumn("dbo.QuestionBanks", "UserId");
        }
    }
}
