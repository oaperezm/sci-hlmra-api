namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv950 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Permissions", "Order", c => c.Int( defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Permissions", "Order");
        }
    }
}
