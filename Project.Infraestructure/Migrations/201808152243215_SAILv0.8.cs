namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SAILv08 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FullName", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.Users", "Birthday", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "Gender", c => c.String(nullable: false));
            AddColumn("dbo.Users", "State", c => c.String(nullable: false, maxLength: 150));
            AddColumn("dbo.Users", "Key", c => c.String(nullable: false, maxLength: 60));
            AddColumn("dbo.Users", "Institution", c => c.String(nullable: false, maxLength: 200));
            DropColumn("dbo.Users", "Name");
            DropColumn("dbo.Users", "LastName");
            DropColumn("dbo.Users", "MiddleName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "MiddleName", c => c.String(maxLength: 150));
            AddColumn("dbo.Users", "LastName", c => c.String(nullable: false, maxLength: 150));
            AddColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 150));
            DropColumn("dbo.Users", "Institution");
            DropColumn("dbo.Users", "Key");
            DropColumn("dbo.Users", "State");
            DropColumn("dbo.Users", "Gender");
            DropColumn("dbo.Users", "Birthday");
            DropColumn("dbo.Users", "FullName");
        }
    }
}
