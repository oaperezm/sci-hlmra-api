namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv172 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeContents", "NodeRoute", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeContents", "NodeRoute");
        }
    }
}
