// <auto-generated />
namespace Project.Infraestructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALIv5 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALIv5));
        
        string IMigrationMetadata.Id
        {
            get { return "201810012202249_SALIv5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
