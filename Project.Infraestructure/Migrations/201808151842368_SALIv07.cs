namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv07 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Permission", "Module", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Permission", "Description", c => c.String(maxLength: 250));
            AlterColumn("dbo.Permission", "Active", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Permission", "Active", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Permission", "Description", c => c.String());
            AlterColumn("dbo.Permission", "Module", c => c.String());
        }
    }
}
