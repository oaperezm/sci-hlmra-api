namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv901 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CourseClassTeacherResources",
                c => new
                    {
                        TeacherResourceId = c.Int(nullable: false),
                        CourseClasseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TeacherResourceId, t.CourseClasseId })
                .ForeignKey("dbo.CourseClasses", t => t.TeacherResourceId, cascadeDelete: true)
                .ForeignKey("dbo.TeacherResources", t => t.CourseClasseId, cascadeDelete: true)
                .Index(t => t.TeacherResourceId)
                .Index(t => t.CourseClasseId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CourseClassTeacherResources", "CourseClasseId", "dbo.TeacherResources");
            DropForeignKey("dbo.CourseClassTeacherResources", "TeacherResourceId", "dbo.CourseClasses");
            DropIndex("dbo.CourseClassTeacherResources", new[] { "CourseClasseId" });
            DropIndex("dbo.CourseClassTeacherResources", new[] { "TeacherResourceId" });
            DropTable("dbo.CourseClassTeacherResources");
        }
    }
}
