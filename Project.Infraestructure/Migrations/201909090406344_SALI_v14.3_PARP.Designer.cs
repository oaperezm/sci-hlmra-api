// <auto-generated />
namespace Project.Infraestructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALI_v143_PARP : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALI_v143_PARP));
        
        string IMigrationMetadata.Id
        {
            get { return "201909090406344_SALI_v14.3_PARP"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
