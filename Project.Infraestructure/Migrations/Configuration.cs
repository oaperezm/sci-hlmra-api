namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    internal sealed class Configuration : DbMigrationsConfiguration<Project.Infraestructure.SailContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Project.Infraestructure.SailContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.       

            
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var baseDirStudents = Path.GetDirectoryName(path) + "\\Migrations\\V_Students.sql";
            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDirStudents));
            var baseDirCourses = Path.GetDirectoryName(path) + "\\Migrations\\V_Courses.sql";
            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDirCourses));
          
            var baseDirReportCoordinator = Path.GetDirectoryName(path) + "\\Migrations\\V_CoordinatorReport.sql";
            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDirReportCoordinator));


        }
    }
}
