namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv424 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PublishingProfileUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublishingProfileId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PublishingProfiles", t => t.PublishingProfileId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.PublishingProfileId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PublishingProfileUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.PublishingProfileUsers", "PublishingProfileId", "dbo.PublishingProfiles");
            DropIndex("dbo.PublishingProfileUsers", new[] { "UserId" });
            DropIndex("dbo.PublishingProfileUsers", new[] { "PublishingProfileId" });
            DropTable("dbo.PublishingProfileUsers");
        }
    }
}
