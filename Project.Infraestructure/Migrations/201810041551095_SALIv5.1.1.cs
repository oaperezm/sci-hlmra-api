namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv511 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "Explanation", c => c.String(maxLength: 650));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "Explanation");
        }
    }
}
