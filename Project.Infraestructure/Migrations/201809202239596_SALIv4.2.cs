namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv42 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 500),
                        IsCorrect = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 800),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        QuestionBankId = c.Int(nullable: false),
                        QuestionTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionBanks", t => t.QuestionBankId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionTypes", t => t.QuestionTypeId, cascadeDelete: true)
                .Index(t => t.QuestionBankId)
                .Index(t => t.QuestionTypeId);
            
            CreateTable(
                "dbo.QuestionBanks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 350),
                        Description = c.String(maxLength: 600),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 2000),
                        UrlImage = c.String(maxLength: 500),
                        ParentId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        NodeTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeTypes", t => t.NodeTypeId, cascadeDelete: true)
                .Index(t => t.NodeTypeId);
            
            CreateTable(
                "dbo.NodeTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PublishingProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false, maxLength: 500),
                        Birthday = c.DateTime(nullable: false),
                        Gender = c.String(nullable: false),
                        State = c.String(nullable: false, maxLength: 150),
                        Email = c.String(nullable: false, maxLength: 250),
                        Password = c.String(nullable: false, maxLength: 250),
                        Key = c.String(maxLength: 60),
                        Institution = c.String(maxLength: 200),
                        RegisterDate = c.DateTime(),
                        UpdateDate = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                        LevelId = c.Int(),
                        AreaId = c.Int(),
                        SubsystemId = c.Int(),
                        Semester = c.Int(),
                        UrlImage = c.String(maxLength: 500),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 500),
                        Description = c.String(nullable: false, maxLength: 800),
                        Gol = c.String(nullable: false, maxLength: 800),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        NodeId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(),
                        UpdateDate = c.DateTime(),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Fridays = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        Sunday = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.NodeId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.StudentGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvitationDate = c.DateTime(nullable: false),
                        CheckDate = c.DateTime(),
                        StudentGroupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        StudentStatusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId, cascadeDelete: true)
                .ForeignKey("dbo.StudentStatuses", t => t.StudentStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.StudentGroupId)
                .Index(t => t.UserId)
                .Index(t => t.StudentStatusId);
            
            CreateTable(
                "dbo.StudentStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        Description = c.String(maxLength: 250),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Module = c.String(nullable: false, maxLength: 80),
                        Description = c.String(maxLength: 250),
                        Url = c.String(maxLength: 350),
                        Icon = c.String(maxLength: 50),
                        Active = c.Boolean(),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 350),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 250),
                        Active = c.Boolean(),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.Blocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        UrlContent = c.String(nullable: false, maxLength: 500),
                        IsPublic = c.Boolean(),
                        Active = c.Boolean(),
                        ContentTypeId = c.Int(nullable: false),
                        FileTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentTypes", t => t.ContentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.FileTypes", t => t.FileTypeId, cascadeDelete: true)
                .Index(t => t.ContentTypeId)
                .Index(t => t.FileTypeId);
            
            CreateTable(
                "dbo.ContentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FileTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NodeContents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContentId = c.Int(nullable: false),
                        NodeId = c.Int(nullable: false),
                        LevelId = c.Int(),
                        SubsystemId = c.Int(),
                        TrainingFieldId = c.Int(),
                        FormativeFieldId = c.Int(),
                        GradeId = c.Int(),
                        BlockId = c.Int(),
                        AreaId = c.Int(),
                        PurposeId = c.Int(),
                        LocationId = c.Int(),
                        NodeRoute = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .ForeignKey("dbo.Blocks", t => t.BlockId)
                .ForeignKey("dbo.FormativeFields", t => t.FormativeFieldId)
                .ForeignKey("dbo.Grades", t => t.GradeId)
                .ForeignKey("dbo.Levels", t => t.LevelId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .ForeignKey("dbo.Purposes", t => t.PurposeId)
                .ForeignKey("dbo.Subsystems", t => t.SubsystemId)
                .ForeignKey("dbo.TrainingFields", t => t.TrainingFieldId)
                .ForeignKey("dbo.Contents", t => t.ContentId, cascadeDelete: true)
                .Index(t => t.ContentId)
                .Index(t => t.NodeId)
                .Index(t => t.LevelId)
                .Index(t => t.SubsystemId)
                .Index(t => t.TrainingFieldId)
                .Index(t => t.FormativeFieldId)
                .Index(t => t.GradeId)
                .Index(t => t.BlockId)
                .Index(t => t.AreaId)
                .Index(t => t.PurposeId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.FormativeFields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Purposes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subsystems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TrainingFields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PublishingProfileNode",
                c => new
                    {
                        PublishingProfileId = c.Int(nullable: false),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PublishingProfileId, t.NodeId })
                .ForeignKey("dbo.PublishingProfiles", t => t.PublishingProfileId, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.PublishingProfileId)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.UserNode",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Node_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Node_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.Node_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Node_Id);
            
            CreateTable(
                "dbo.RolePermission",
                c => new
                    {
                        RoleId = c.Int(nullable: false),
                        PermissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.PermissionId })
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Permissions", t => t.PermissionId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.BookAuthor",
                c => new
                    {
                        BookId = c.Int(nullable: false),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BookId, t.AuthorId })
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Authors", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.BookId)
                .Index(t => t.AuthorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NodeContents", "ContentId", "dbo.Contents");
            DropForeignKey("dbo.NodeContents", "TrainingFieldId", "dbo.TrainingFields");
            DropForeignKey("dbo.NodeContents", "SubsystemId", "dbo.Subsystems");
            DropForeignKey("dbo.NodeContents", "PurposeId", "dbo.Purposes");
            DropForeignKey("dbo.NodeContents", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.NodeContents", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.NodeContents", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.NodeContents", "GradeId", "dbo.Grades");
            DropForeignKey("dbo.NodeContents", "FormativeFieldId", "dbo.FormativeFields");
            DropForeignKey("dbo.NodeContents", "BlockId", "dbo.Blocks");
            DropForeignKey("dbo.NodeContents", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.Contents", "FileTypeId", "dbo.FileTypes");
            DropForeignKey("dbo.Contents", "ContentTypeId", "dbo.ContentTypes");
            DropForeignKey("dbo.Books", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.BookAuthor", "AuthorId", "dbo.Authors");
            DropForeignKey("dbo.BookAuthor", "BookId", "dbo.Books");
            DropForeignKey("dbo.Questions", "QuestionTypeId", "dbo.QuestionTypes");
            DropForeignKey("dbo.Questions", "QuestionBankId", "dbo.QuestionBanks");
            DropForeignKey("dbo.QuestionBanks", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RolePermission", "PermissionId", "dbo.Permissions");
            DropForeignKey("dbo.RolePermission", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserNode", "Node_Id", "dbo.Nodes");
            DropForeignKey("dbo.UserNode", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Courses", "UserId", "dbo.Users");
            DropForeignKey("dbo.Students", "UserId", "dbo.Users");
            DropForeignKey("dbo.Students", "StudentStatusId", "dbo.StudentStatuses");
            DropForeignKey("dbo.Students", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.StudentGroups", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.PublishingProfileNode", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.PublishingProfileNode", "PublishingProfileId", "dbo.PublishingProfiles");
            DropForeignKey("dbo.Nodes", "NodeTypeId", "dbo.NodeTypes");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.BookAuthor", new[] { "AuthorId" });
            DropIndex("dbo.BookAuthor", new[] { "BookId" });
            DropIndex("dbo.RolePermission", new[] { "PermissionId" });
            DropIndex("dbo.RolePermission", new[] { "RoleId" });
            DropIndex("dbo.UserNode", new[] { "Node_Id" });
            DropIndex("dbo.UserNode", new[] { "User_Id" });
            DropIndex("dbo.PublishingProfileNode", new[] { "NodeId" });
            DropIndex("dbo.PublishingProfileNode", new[] { "PublishingProfileId" });
            DropIndex("dbo.NodeContents", new[] { "LocationId" });
            DropIndex("dbo.NodeContents", new[] { "PurposeId" });
            DropIndex("dbo.NodeContents", new[] { "AreaId" });
            DropIndex("dbo.NodeContents", new[] { "BlockId" });
            DropIndex("dbo.NodeContents", new[] { "GradeId" });
            DropIndex("dbo.NodeContents", new[] { "FormativeFieldId" });
            DropIndex("dbo.NodeContents", new[] { "TrainingFieldId" });
            DropIndex("dbo.NodeContents", new[] { "SubsystemId" });
            DropIndex("dbo.NodeContents", new[] { "LevelId" });
            DropIndex("dbo.NodeContents", new[] { "NodeId" });
            DropIndex("dbo.NodeContents", new[] { "ContentId" });
            DropIndex("dbo.Contents", new[] { "FileTypeId" });
            DropIndex("dbo.Contents", new[] { "ContentTypeId" });
            DropIndex("dbo.Books", new[] { "NodeId" });
            DropIndex("dbo.Students", new[] { "StudentStatusId" });
            DropIndex("dbo.Students", new[] { "UserId" });
            DropIndex("dbo.Students", new[] { "StudentGroupId" });
            DropIndex("dbo.StudentGroups", new[] { "CourseId" });
            DropIndex("dbo.Courses", new[] { "UserId" });
            DropIndex("dbo.Courses", new[] { "NodeId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Nodes", new[] { "NodeTypeId" });
            DropIndex("dbo.QuestionBanks", new[] { "NodeId" });
            DropIndex("dbo.Questions", new[] { "QuestionTypeId" });
            DropIndex("dbo.Questions", new[] { "QuestionBankId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.BookAuthor");
            DropTable("dbo.RolePermission");
            DropTable("dbo.UserNode");
            DropTable("dbo.PublishingProfileNode");
            DropTable("dbo.TrainingFields");
            DropTable("dbo.Subsystems");
            DropTable("dbo.Purposes");
            DropTable("dbo.Locations");
            DropTable("dbo.Levels");
            DropTable("dbo.Grades");
            DropTable("dbo.FormativeFields");
            DropTable("dbo.NodeContents");
            DropTable("dbo.FileTypes");
            DropTable("dbo.ContentTypes");
            DropTable("dbo.Contents");
            DropTable("dbo.Blocks");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
            DropTable("dbo.Areas");
            DropTable("dbo.QuestionTypes");
            DropTable("dbo.Permissions");
            DropTable("dbo.Roles");
            DropTable("dbo.StudentStatuses");
            DropTable("dbo.Students");
            DropTable("dbo.StudentGroups");
            DropTable("dbo.Courses");
            DropTable("dbo.Users");
            DropTable("dbo.PublishingProfiles");
            DropTable("dbo.NodeTypes");
            DropTable("dbo.Nodes");
            DropTable("dbo.QuestionBanks");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
        }
    }
}
