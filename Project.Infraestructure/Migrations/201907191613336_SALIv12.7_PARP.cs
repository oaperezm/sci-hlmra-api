namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv127_PARP : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [dbo].[Permissions] SET [Order]=15  where Id = 115" +
                "UPDATE [dbo].[Permissions] SET [Order]=16  where Id = 88" +
                "UPDATE [dbo].[Permissions] SET [Order]=17  where Id = 79" +
                "UPDATE [dbo].[Permissions] SET [Order]=18  where Id = 78"
                );
        }
        
        public override void Down()
        {

            Sql("UPDATE [dbo].[Permissions] SET [Order]=0  where Id = 116" +
            "UPDATE [dbo].[Permissions] SET [Order]=15  where Id = 88" +
            "UPDATE [dbo].[Permissions] SET [Order]=16  where Id = 79" +
            "UPDATE [dbo].[Permissions] SET [Order]=17  where Id = 78"
            );
        }
    }
}
