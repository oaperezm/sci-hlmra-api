namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v139_PARP : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", "dbo.ContentTypes");
            DropIndex("dbo.AreaPersonalSocialDevelopment", new[] { "ContentTypeId" });
            AddColumn("dbo.AreaPersonalSocialDevelopment", "FileTypeTypeId", c => c.Int(nullable: true));
            CreateIndex("dbo.AreaPersonalSocialDevelopment", "FileTypeTypeId");
            AddForeignKey("dbo.AreaPersonalSocialDevelopment", "FileTypeTypeId", "dbo.FileTypes", "Id");
            DropColumn("dbo.AreaPersonalSocialDevelopment", "ContentTypeId");
            Sql(" UPDATE dbo.AreaPersonalSocialDevelopment SET FileTypeTypeId = 11  where isnull(FileTypeTypeId,0)= 0");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AreaPersonalSocialDevelopment", "FileTypeTypeId", "dbo.FileTypes");
            DropIndex("dbo.AreaPersonalSocialDevelopment", new[] { "FileTypeTypeId" });
            DropColumn("dbo.AreaPersonalSocialDevelopment", "FileTypeTypeId");
            CreateIndex("dbo.AreaPersonalSocialDevelopment", "ContentTypeId");
            AddForeignKey("dbo.AreaPersonalSocialDevelopment", "ContentTypeId", "dbo.ContentTypes", "Id");
        }
    }
}
