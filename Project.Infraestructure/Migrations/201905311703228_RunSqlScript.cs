namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RunSqlScript : DbMigration
    {

        public override void Up()
        {
            CreateTable(
                "dbo.VideoConference",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(nullable: false, maxLength: 100),
                    Information = c.String(nullable: false, maxLength: 800),
                    DateTimeVideoConference = c.DateTime(),
                    Duration = c.Time(nullable: false, precision: 7),
                    Type = c.String(nullable: false, maxLength: 200),
                    Invited = c.String(nullable: false, maxLength: 200),
                    BookId = c.Int(nullable: false),
                    Accepted = c.Boolean(nullable: false),
                    rejected = c.Boolean(nullable: false),
                    EditorId = c.Int(nullable: true),
                    Editor = c.String(nullable: true, maxLength: 300),
                    Commentary = c.String(nullable: true, maxLength: 800),
                    RegisterDate = c.DateTime(),
                    UpdateDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Nodes", "Sort", c => c.Int(nullable: true, defaultValue: 0));

        }

        public override void Down()
        {
            DropColumn("dbo.Nodes", "Sort");
            DropTable("dbo.VideoConference");
        }

    }
}
