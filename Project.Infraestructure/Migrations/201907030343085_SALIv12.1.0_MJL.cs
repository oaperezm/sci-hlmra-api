namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1210_MJL : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AnnouncementsStudentGroups", newName: "AnnouncementStudentGroup");
            DropPrimaryKey("dbo.AnnouncementStudentGroup");
            AddPrimaryKey("dbo.AnnouncementStudentGroup", new[] { "AnnouncementId", "StudentGroupId" });
            DropColumn("dbo.AnnouncementStudentGroup", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AnnouncementStudentGroup", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.AnnouncementStudentGroup");
            AddPrimaryKey("dbo.AnnouncementStudentGroup", "Id");
            RenameTable(name: "dbo.AnnouncementStudentGroup", newName: "AnnouncementsStudentGroups");
        }
    }
}
