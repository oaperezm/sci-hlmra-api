namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv966 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.VideoconferenceStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.VideoconferenceStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
