namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv920 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateUserId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreateUserId)
                .Index(t => t.CreateUserId);
            
            CreateTable(
                "dbo.ChatGroupUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ChatGroupId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        ArchiveDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChatGroups", t => t.ChatGroupId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ChatGroupId);
            
            CreateTable(
                "dbo.ChatMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(nullable: false),
                        Url = c.String(),
                        SenderUserId = c.Int(nullable: false),
                        ReceiverUserId = c.Int(nullable: false),
                        Read = c.Boolean(nullable: false),
                        MessageSequence = c.Long(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ReceiverUserId)
                .ForeignKey("dbo.Users", t => t.SenderUserId)
                .Index(t => t.SenderUserId)
                .Index(t => t.ReceiverUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatMessages", "SenderUserId", "dbo.Users");
            DropForeignKey("dbo.ChatMessages", "ReceiverUserId", "dbo.Users");
            DropForeignKey("dbo.ChatGroupUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.ChatGroupUsers", "ChatGroupId", "dbo.ChatGroups");
            DropForeignKey("dbo.ChatGroups", "CreateUserId", "dbo.Users");
            DropIndex("dbo.ChatMessages", new[] { "ReceiverUserId" });
            DropIndex("dbo.ChatMessages", new[] { "SenderUserId" });
            DropIndex("dbo.ChatGroupUsers", new[] { "ChatGroupId" });
            DropIndex("dbo.ChatGroupUsers", new[] { "UserId" });
            DropIndex("dbo.ChatGroups", new[] { "CreateUserId" });
            DropTable("dbo.ChatMessages");
            DropTable("dbo.ChatGroupUsers");
            DropTable("dbo.ChatGroups");
        }
    }
}
