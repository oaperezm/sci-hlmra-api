namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1240_PARP : DbMigration
    {
        public override void Up()
        {

            DropForeignKey("dbo.ActivityAnswers", "UserId", "dbo.Users");
            DropIndex("dbo.ActivityAnswers", new[] { "UserId" });
            AddColumn("dbo.ActivityAnswers", "StudentId", c => c.Int(nullable: false));
            DropColumn("dbo.ActivityAnswers", "UserId");


            AddColumn("dbo.HomeWorks", "TypeScore", c => c.Boolean());
            Sql("UPDATE dbo.HomeWorks SET TypeScore=0  where isnull(TypeScore,0)=0");
            Sql("ALTER TABLE dbo.HomeWorks ADD DEFAULT (0) for TypeScore");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ActivityAnswers", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.ActivityAnswers", "StudentId");
            CreateIndex("dbo.ActivityAnswers", "UserId");
            AddForeignKey("dbo.ActivityAnswers", "UserId", "dbo.Users", "Id");


            DropColumn("dbo.HomeWorks", "TypeScore");
        }
    }
}
