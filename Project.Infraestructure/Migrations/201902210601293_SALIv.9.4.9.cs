namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv949 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GeneralEvents", "SystemId", c => c.Int());
            AddColumn("dbo.News", "SystemId", c => c.Int());
            AddColumn("dbo.Tips", "SystemId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tips", "SystemId");
            DropColumn("dbo.News", "SystemId");
            DropColumn("dbo.GeneralEvents", "SystemId");
        }
    }
}
