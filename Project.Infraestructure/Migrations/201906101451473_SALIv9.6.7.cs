namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv967 : DbMigration
    {
        public override void Up()
        {
  
            AddColumn("dbo.VideoConference", "emailsExternal", c => c.String());
         
        }
        
        public override void Down()
        {
               DropColumn("dbo.VideoConference", "emailsExternal");
        
        }
    }
}
