namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv924 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ForumThreads", "Views", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ForumThreads", "Views");
        }
    }
}
