namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv915 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserNotofocations", "Title", c => c.String(nullable: false, maxLength: 300));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserNotofocations", "Title");
        }
    }
}
