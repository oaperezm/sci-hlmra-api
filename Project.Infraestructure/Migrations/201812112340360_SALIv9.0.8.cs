namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv908 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        TokenDevice = c.String(nullable: false, maxLength: 500),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDevices", "UserId", "dbo.Users");
            DropIndex("dbo.UserDevices", new[] { "UserId" });
            DropTable("dbo.UserDevices");
        }
    }
}
