namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIV107 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Announcements",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(nullable: false, maxLength: 100),
                    Description = c.String(nullable: false, maxLength: 350),
                    CourseId = c.Int(nullable: false),
                    RegisterDate = c.DateTime(nullable: false),
                    UserId = c.Int(nullable: false),
                    Active = c.Boolean(nullable: false),
                    UpdateDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.CourseId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.NoticeStudentGroup",
                c => new
                {
                    NoticeId = c.Int(nullable: false),
                    StudentGroupId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.NoticeId, t.StudentGroupId })
                .ForeignKey("dbo.Announcements", t => t.NoticeId)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .Index(t => t.NoticeId)
                .Index(t => t.StudentGroupId);

            //Guillermo
            AddColumn("dbo.HomeWorks", "PartialEvaluation1", c => c.Int());


            //adding new columns to Homeworks table
            AddColumn("dbo.HomeWorks", "partial", c => c.Int());

            //prepare previous rows to fill OpeningDate with the RegisterDate, just for the previous rows

            Sql("UPDATE dbo.HomeWorks SET OpeningDate=RegisterDate");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Announcements", "UserId", "dbo.Users");
            DropForeignKey("dbo.NoticeStudentGroup", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.NoticeStudentGroup", "NoticeId", "dbo.Announcements");
            DropForeignKey("dbo.Announcements", "CourseId", "dbo.Courses");
            DropIndex("dbo.NoticeStudentGroup", new[] { "StudentGroupId" });
            DropIndex("dbo.NoticeStudentGroup", new[] { "NoticeId" });
            DropIndex("dbo.Announcements", new[] { "UserId" });
            DropIndex("dbo.Announcements", new[] { "CourseId" });
            DropColumn("dbo.HomeWorks", "PartialEvaluation1");
            DropColumn("dbo.HomeWorks", "partial");
            DropTable("dbo.NoticeStudentGroup");
            DropTable("dbo.Announcements");
        }
    }
}
