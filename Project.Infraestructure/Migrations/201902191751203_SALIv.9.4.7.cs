namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv947 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "Visits", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.Contents", "RegisterDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contents", "RegisterDate");
            DropColumn("dbo.Contents", "Visits");
        }
    }
}
