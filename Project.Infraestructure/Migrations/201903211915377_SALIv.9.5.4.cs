namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv954 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "ImageWidth", c => c.Int());
            AddColumn("dbo.Questions", "ImageHeight", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "ImageHeight");
            DropColumn("dbo.Questions", "ImageWidth");
        }
    }
}
