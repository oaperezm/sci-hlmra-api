namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SAILv03 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "RegisterDate", c => c.DateTime());
            AlterColumn("dbo.Users", "UpdateDate", c => c.DateTime());
            AlterColumn("dbo.Users", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Active", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "UpdateDate", c => c.String());
            AlterColumn("dbo.Users", "RegisterDate", c => c.String());
        }
    }
}
