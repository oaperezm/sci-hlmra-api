namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv129_PARP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventsReport",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CCT = c.String(nullable: false),
                        Email = c.String(),
                        Username = c.String(nullable: false),
                        InstitutionName = c.String(nullable: false),
                        Level = c.String(nullable: false),
                        State = c.String(nullable: false),
                        ClasificationSchool = c.String(nullable: false),
                        Zone = c.String(nullable: false),
                        Control = c.String(nullable: false),
                        Prometer = c.String(nullable: false),
                        EntryDate = c.DateTime(nullable: false),
                        TimeNavegation = c.String(nullable: false),
                        Sections = c.String(nullable: false),
                        Grade = c.String(nullable: false),
                        EducationField = c.String(nullable: false),
                        Subject = c.String(nullable: false),
                        Format = c.String(nullable: false),
                        RegisterDate = c.DateTime(),
                        UpdateDate = c.DateTime(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventsReport", "UserId", "dbo.Users");
            DropIndex("dbo.EventsReport", new[] { "UserId" });
            DropTable("dbo.EventsReport");
        }
    }
}
