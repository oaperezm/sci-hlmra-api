namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv969 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.VideoConference", "UserId");
            AddForeignKey("dbo.VideoConference", "UserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VideoConference", "UserId", "dbo.Users");
            DropIndex("dbo.VideoConference", new[] { "UserId" });
        }
    }
}
