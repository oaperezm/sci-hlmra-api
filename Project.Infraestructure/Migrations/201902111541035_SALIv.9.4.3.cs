namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv943 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tips", "Published", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tips", "Published");
        }
    }
}
