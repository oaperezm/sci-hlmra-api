namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv515 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "RelationDescription", c => c.String(maxLength: 500));
            AddColumn("dbo.Users", "RefreshToken", c => c.String(maxLength: 400));
            AddColumn("dbo.Users", "ClientId", c => c.String(maxLength: 150));
            AlterColumn("dbo.ExamScores", "Score", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExamScores", "Score", c => c.String());
            DropColumn("dbo.Users", "ClientId");
            DropColumn("dbo.Users", "RefreshToken");
            DropColumn("dbo.Answers", "RelationDescription");
        }
    }
}
