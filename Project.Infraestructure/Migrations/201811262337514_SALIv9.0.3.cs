namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv903 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "TeacherResourceId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "CourseClasseId", newName: "TeacherResourceId");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "TeacherResourceId", newName: "__mig_tmp__1");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "HomeWorkId", newName: "TeacherResourceId");
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "__mig_tmp__0", newName: "CourseClasseId");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "__mig_tmp__1", newName: "HomeWorkId");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "IX_TeacherResourceId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "IX_HomeWorkId", newName: "IX_TeacherResourceId");
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "IX_TeacherResourceId", newName: "__mig_tmp__1");
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "IX_CourseClasseId", newName: "IX_TeacherResourceId");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "__mig_tmp__0", newName: "IX_HomeWorkId");
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "__mig_tmp__1", newName: "IX_CourseClasseId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "IX_CourseClasseId", newName: "__mig_tmp__1");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "IX_HomeWorkId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "IX_TeacherResourceId", newName: "IX_CourseClasseId");
            RenameIndex(table: "dbo.CourseClassTeacherResources", name: "__mig_tmp__1", newName: "IX_TeacherResourceId");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "IX_TeacherResourceId", newName: "IX_HomeWorkId");
            RenameIndex(table: "dbo.HomeWorkTeacherResources", name: "__mig_tmp__0", newName: "IX_TeacherResourceId");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "HomeWorkId", newName: "__mig_tmp__1");
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "CourseClasseId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "TeacherResourceId", newName: "HomeWorkId");
            RenameColumn(table: "dbo.HomeWorkTeacherResources", name: "__mig_tmp__1", newName: "TeacherResourceId");
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "TeacherResourceId", newName: "CourseClasseId");
            RenameColumn(table: "dbo.CourseClassTeacherResources", name: "__mig_tmp__0", newName: "TeacherResourceId");
        }
    }
}
