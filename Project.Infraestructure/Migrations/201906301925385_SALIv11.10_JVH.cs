namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1110_JVH : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestAnswers", "TeacherExamQuestionId", "dbo.TeacherExamQuestions");
            DropIndex("dbo.TestAnswers", new[] { "TeacherExamQuestionId" });                        
            AlterColumn("dbo.TestAnswers", "TeacherExamQuestionId", c => c.Int());
            CreateIndex("dbo.TestAnswers", "TeacherExamQuestionId");
        }
        
        public override void Down()
        {            
            DropForeignKey("dbo.TestAnswers", "TeacherExamQuestionId", "dbo.TeacherExamQuestions");         
            DropIndex("dbo.TestAnswers", new[] { "TeacherExamQuestionId" });
            AlterColumn("dbo.TestAnswers", "TeacherExamQuestionId", c => c.Int(nullable: false));            
            CreateIndex("dbo.TestAnswers", "TeacherExamQuestionId");
            AddForeignKey("dbo.TestAnswers", "TeacherExamQuestionId", "dbo.TeacherExamQuestions", "Id");
        }
    }
}
