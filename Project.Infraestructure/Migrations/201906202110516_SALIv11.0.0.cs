namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1100 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.HomeWorks", "partial");
            DropColumn("dbo.HomeWorks", "PartialEvaluation1");            
            AddColumn("dbo.HomeWorks", "PartialEvaluation", c => c.Int());
        }

        public override void Down()
        {
            DropColumn("dbo.HomeWorks", "PartialEvaluation");
            AddColumn("dbo.HomeWorks", "PartialEvaluation1", c => c.Int());
            AddColumn("dbo.HomeWorks", "partial", c => c.Int());
        }
    }
}
