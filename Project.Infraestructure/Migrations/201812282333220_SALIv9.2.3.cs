namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv923 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForumThreads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        InitDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        CourseId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.ForumPosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ForumThreadId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        Title = c.String(nullable: false, maxLength: 350),
                        Description = c.String(nullable: false, maxLength: 1000),
                        Replay = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ForumThreads", t => t.ForumThreadId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ForumThreadId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ForumThreadStudentGroup",
                c => new
                    {
                        ForumThreadId = c.Int(nullable: false),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ForumThreadId, t.StudentGroupId })
                .ForeignKey("dbo.ForumThreads", t => t.ForumThreadId)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .Index(t => t.ForumThreadId)
                .Index(t => t.StudentGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumThreads", "UserId", "dbo.Users");
            DropForeignKey("dbo.ForumPosts", "UserId", "dbo.Users");
            DropForeignKey("dbo.ForumPosts", "ForumThreadId", "dbo.ForumThreads");
            DropForeignKey("dbo.ForumThreadStudentGroup", "StudentGroupId", "dbo.StudentGroups");
            DropForeignKey("dbo.ForumThreadStudentGroup", "ForumThreadId", "dbo.ForumThreads");
            DropForeignKey("dbo.ForumThreads", "CourseId", "dbo.Courses");
            DropIndex("dbo.ForumThreadStudentGroup", new[] { "StudentGroupId" });
            DropIndex("dbo.ForumThreadStudentGroup", new[] { "ForumThreadId" });
            DropIndex("dbo.ForumPosts", new[] { "UserId" });
            DropIndex("dbo.ForumPosts", new[] { "ForumThreadId" });
            DropIndex("dbo.ForumThreads", new[] { "CourseId" });
            DropIndex("dbo.ForumThreads", new[] { "UserId" });
            DropTable("dbo.ForumThreadStudentGroup");
            DropTable("dbo.ForumPosts");
            DropTable("dbo.ForumThreads");
        }
    }
}
