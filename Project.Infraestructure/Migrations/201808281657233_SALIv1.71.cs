namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv171 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Node", newName: "Nodes");
            RenameTable(name: "dbo.NodeType", newName: "NodeTypes");
            RenameTable(name: "dbo.PublishingProfile", newName: "PublishingProfiles");
            RenameTable(name: "dbo.FormativeField", newName: "FormativeFields");
            RenameTable(name: "dbo.Grade", newName: "Grades");
            RenameTable(name: "dbo.Level", newName: "Levels");
            RenameTable(name: "dbo.Location", newName: "Locations");
            RenameTable(name: "dbo.NodeContent", newName: "NodeContents");
            RenameTable(name: "dbo.Purpose", newName: "Purposes");
            RenameTable(name: "dbo.Subsystem", newName: "Subsystems");
            RenameTable(name: "dbo.TrainingField", newName: "TrainingFields");
            RenameTable(name: "dbo.Permission", newName: "Permissions");
            DropForeignKey("dbo.NodeContent", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.NodeContent", "BlockId", "dbo.Blocks");
            DropForeignKey("dbo.NodeContent", "FormativeFieldId", "dbo.FormativeField");
            DropForeignKey("dbo.NodeContent", "GradeId", "dbo.Grade");
            DropForeignKey("dbo.NodeContent", "LevelId", "dbo.Level");
            DropForeignKey("dbo.NodeContent", "LocationId", "dbo.Location");
            DropForeignKey("dbo.NodeContent", "PurposeId", "dbo.Purpose");
            DropForeignKey("dbo.NodeContent", "SubsystemId", "dbo.Subsystem");
            DropForeignKey("dbo.NodeContent", "TrainingFieldId", "dbo.TrainingField");
            DropIndex("dbo.NodeContents", new[] { "LevelId" });
            DropIndex("dbo.NodeContents", new[] { "SubsystemId" });
            DropIndex("dbo.NodeContents", new[] { "TrainingFieldId" });
            DropIndex("dbo.NodeContents", new[] { "FormativeFieldId" });
            DropIndex("dbo.NodeContents", new[] { "GradeId" });
            DropIndex("dbo.NodeContents", new[] { "BlockId" });
            DropIndex("dbo.NodeContents", new[] { "AreaId" });
            DropIndex("dbo.NodeContents", new[] { "PurposeId" });
            DropIndex("dbo.NodeContents", new[] { "LocationId" });
            RenameColumn(table: "dbo.PublishingProfileNode", name: "PublishingProfile_Id", newName: "PublishingProfileId");
            RenameColumn(table: "dbo.PublishingProfileNode", name: "Node_Id", newName: "NodeId");
            RenameIndex(table: "dbo.PublishingProfileNode", name: "IX_PublishingProfile_Id", newName: "IX_PublishingProfileId");
            RenameIndex(table: "dbo.PublishingProfileNode", name: "IX_Node_Id", newName: "IX_NodeId");
            CreateTable(
                "dbo.UserNode",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Node_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Node_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.Node_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Node_Id);
            
            AlterColumn("dbo.Nodes", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Nodes", "Description", c => c.String(maxLength: 2000));
            AlterColumn("dbo.Nodes", "UrlImage", c => c.String(maxLength: 500));
            AlterColumn("dbo.NodeTypes", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.PublishingProfiles", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.PublishingProfiles", "Active", c => c.Boolean());
            AlterColumn("dbo.FormativeFields", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Grades", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Levels", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Locations", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.NodeContents", "LevelId", c => c.Int());
            AlterColumn("dbo.NodeContents", "SubsystemId", c => c.Int());
            AlterColumn("dbo.NodeContents", "TrainingFieldId", c => c.Int());
            AlterColumn("dbo.NodeContents", "FormativeFieldId", c => c.Int());
            AlterColumn("dbo.NodeContents", "GradeId", c => c.Int());
            AlterColumn("dbo.NodeContents", "BlockId", c => c.Int());
            AlterColumn("dbo.NodeContents", "AreaId", c => c.Int());
            AlterColumn("dbo.NodeContents", "PurposeId", c => c.Int());
            AlterColumn("dbo.NodeContents", "LocationId", c => c.Int());
            AlterColumn("dbo.Purposes", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Subsystems", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.TrainingFields", "Description", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.NodeContents", "LevelId");
            CreateIndex("dbo.NodeContents", "SubsystemId");
            CreateIndex("dbo.NodeContents", "TrainingFieldId");
            CreateIndex("dbo.NodeContents", "FormativeFieldId");
            CreateIndex("dbo.NodeContents", "GradeId");
            CreateIndex("dbo.NodeContents", "BlockId");
            CreateIndex("dbo.NodeContents", "AreaId");
            CreateIndex("dbo.NodeContents", "PurposeId");
            CreateIndex("dbo.NodeContents", "LocationId");
            AddForeignKey("dbo.NodeContents", "AreaId", "dbo.Areas", "Id");
            AddForeignKey("dbo.NodeContents", "BlockId", "dbo.Blocks", "Id");
            AddForeignKey("dbo.NodeContents", "FormativeFieldId", "dbo.FormativeFields", "Id");
            AddForeignKey("dbo.NodeContents", "GradeId", "dbo.Grades", "Id");
            AddForeignKey("dbo.NodeContents", "LevelId", "dbo.Levels", "Id");
            AddForeignKey("dbo.NodeContents", "LocationId", "dbo.Locations", "Id");
            AddForeignKey("dbo.NodeContents", "PurposeId", "dbo.Purposes", "Id");
            AddForeignKey("dbo.NodeContents", "SubsystemId", "dbo.Subsystems", "Id");
            AddForeignKey("dbo.NodeContents", "TrainingFieldId", "dbo.TrainingFields", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NodeContents", "TrainingFieldId", "dbo.TrainingFields");
            DropForeignKey("dbo.NodeContents", "SubsystemId", "dbo.Subsystems");
            DropForeignKey("dbo.NodeContents", "PurposeId", "dbo.Purposes");
            DropForeignKey("dbo.NodeContents", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.NodeContents", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.NodeContents", "GradeId", "dbo.Grades");
            DropForeignKey("dbo.NodeContents", "FormativeFieldId", "dbo.FormativeFields");
            DropForeignKey("dbo.NodeContents", "BlockId", "dbo.Blocks");
            DropForeignKey("dbo.NodeContents", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.UserNode", "Node_Id", "dbo.Nodes");
            DropForeignKey("dbo.UserNode", "User_Id", "dbo.Users");
            DropIndex("dbo.UserNode", new[] { "Node_Id" });
            DropIndex("dbo.UserNode", new[] { "User_Id" });
            DropIndex("dbo.NodeContents", new[] { "LocationId" });
            DropIndex("dbo.NodeContents", new[] { "PurposeId" });
            DropIndex("dbo.NodeContents", new[] { "AreaId" });
            DropIndex("dbo.NodeContents", new[] { "BlockId" });
            DropIndex("dbo.NodeContents", new[] { "GradeId" });
            DropIndex("dbo.NodeContents", new[] { "FormativeFieldId" });
            DropIndex("dbo.NodeContents", new[] { "TrainingFieldId" });
            DropIndex("dbo.NodeContents", new[] { "SubsystemId" });
            DropIndex("dbo.NodeContents", new[] { "LevelId" });
            AlterColumn("dbo.TrainingFields", "Description", c => c.String());
            AlterColumn("dbo.Subsystems", "Description", c => c.String());
            AlterColumn("dbo.Purposes", "Description", c => c.String());
            AlterColumn("dbo.NodeContents", "LocationId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "PurposeId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "AreaId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "BlockId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "GradeId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "FormativeFieldId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "TrainingFieldId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "SubsystemId", c => c.Int(nullable: false));
            AlterColumn("dbo.NodeContents", "LevelId", c => c.Int(nullable: false));
            AlterColumn("dbo.Locations", "Description", c => c.String());
            AlterColumn("dbo.Levels", "Description", c => c.String());
            AlterColumn("dbo.Grades", "Description", c => c.String());
            AlterColumn("dbo.FormativeFields", "Description", c => c.String());
            AlterColumn("dbo.PublishingProfiles", "Active", c => c.Boolean(nullable: false));
            AlterColumn("dbo.PublishingProfiles", "Description", c => c.String());
            AlterColumn("dbo.NodeTypes", "Description", c => c.String());
            AlterColumn("dbo.Nodes", "UrlImage", c => c.String());
            AlterColumn("dbo.Nodes", "Description", c => c.String());
            AlterColumn("dbo.Nodes", "Name", c => c.String());
            DropTable("dbo.UserNode");
            RenameIndex(table: "dbo.PublishingProfileNode", name: "IX_NodeId", newName: "IX_Node_Id");
            RenameIndex(table: "dbo.PublishingProfileNode", name: "IX_PublishingProfileId", newName: "IX_PublishingProfile_Id");
            RenameColumn(table: "dbo.PublishingProfileNode", name: "NodeId", newName: "Node_Id");
            RenameColumn(table: "dbo.PublishingProfileNode", name: "PublishingProfileId", newName: "PublishingProfile_Id");
            CreateIndex("dbo.NodeContents", "LocationId");
            CreateIndex("dbo.NodeContents", "PurposeId");
            CreateIndex("dbo.NodeContents", "AreaId");
            CreateIndex("dbo.NodeContents", "BlockId");
            CreateIndex("dbo.NodeContents", "GradeId");
            CreateIndex("dbo.NodeContents", "FormativeFieldId");
            CreateIndex("dbo.NodeContents", "TrainingFieldId");
            CreateIndex("dbo.NodeContents", "SubsystemId");
            CreateIndex("dbo.NodeContents", "LevelId");
            AddForeignKey("dbo.NodeContent", "TrainingFieldId", "dbo.TrainingField", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "SubsystemId", "dbo.Subsystem", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "PurposeId", "dbo.Purpose", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "LocationId", "dbo.Location", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "LevelId", "dbo.Level", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "GradeId", "dbo.Grade", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "FormativeFieldId", "dbo.FormativeField", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "BlockId", "dbo.Blocks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.NodeContent", "AreaId", "dbo.Areas", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.Permissions", newName: "Permission");
            RenameTable(name: "dbo.TrainingFields", newName: "TrainingField");
            RenameTable(name: "dbo.Subsystems", newName: "Subsystem");
            RenameTable(name: "dbo.Purposes", newName: "Purpose");
            RenameTable(name: "dbo.NodeContents", newName: "NodeContent");
            RenameTable(name: "dbo.Locations", newName: "Location");
            RenameTable(name: "dbo.Levels", newName: "Level");
            RenameTable(name: "dbo.Grades", newName: "Grade");
            RenameTable(name: "dbo.FormativeFields", newName: "FormativeField");
            RenameTable(name: "dbo.PublishingProfiles", newName: "PublishingProfile");
            RenameTable(name: "dbo.NodeTypes", newName: "NodeType");
            RenameTable(name: "dbo.Nodes", newName: "Node");
        }
    }
}
