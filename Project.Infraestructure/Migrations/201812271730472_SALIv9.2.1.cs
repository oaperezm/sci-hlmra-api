namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv921 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChatMessages", "ChatGroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.ChatMessages", "ChatGroupId");
            AddForeignKey("dbo.ChatMessages", "ChatGroupId", "dbo.ChatGroups", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatMessages", "ChatGroupId", "dbo.ChatGroups");
            DropIndex("dbo.ChatMessages", new[] { "ChatGroupId" });
            DropColumn("dbo.ChatMessages", "ChatGroupId");
        }
    }
}
