namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5114 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.GlossaryWords", name: "GlossatyId", newName: "GlossaryId");
            RenameIndex(table: "dbo.GlossaryWords", name: "IX_GlossatyId", newName: "IX_GlossaryId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.GlossaryWords", name: "IX_GlossaryId", newName: "IX_GlossatyId");
            RenameColumn(table: "dbo.GlossaryWords", name: "GlossaryId", newName: "GlossatyId");
        }
    }
}
