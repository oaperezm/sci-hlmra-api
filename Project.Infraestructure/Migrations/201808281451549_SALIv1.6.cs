namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv16 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserNode",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Node_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Node_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Node", t => t.Node_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Node_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserNode", "Node_Id", "dbo.Node");
            DropForeignKey("dbo.UserNode", "User_Id", "dbo.Users");
            DropIndex("dbo.UserNode", new[] { "Node_Id" });
            DropIndex("dbo.UserNode", new[] { "User_Id" });
            DropTable("dbo.UserNode");
        }
    }
}
