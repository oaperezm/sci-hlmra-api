namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5117 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExamScores", "TestId", c => c.Int());
            CreateIndex("dbo.ExamScores", "TestId");
            AddForeignKey("dbo.ExamScores", "TestId", "dbo.Tests", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExamScores", "TestId", "dbo.Tests");
            DropIndex("dbo.ExamScores", new[] { "TestId" });
            DropColumn("dbo.ExamScores", "TestId");
        }
    }
}
