namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv917 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HomeWorks", "Qualified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HomeWorks", "Qualified");
        }
    }
}
