namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1190_GEH : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HomeWorkAnswers", "Delivered", c => c.Boolean());
            AlterColumn("dbo.HomeWorks", "TypeOfQualification", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HomeWorks", "TypeOfQualification", c => c.Int());
            AlterColumn("dbo.HomeWorkAnswers", "Delivered", c => c.Int());
        }
    }
}
