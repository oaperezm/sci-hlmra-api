namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1200_GEH : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ActivityFile", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.ActivityFile", "ActivityAnswer_Id", "dbo.ActivityAnswers");
            DropIndex("dbo.ActivityFile", new[] { "ActivityId" });
            DropIndex("dbo.ActivityFile", new[] { "ActivityAnswer_Id" });
            RenameColumn(table: "dbo.ActivityFile", name: "ActivityAnswer_Id", newName: "ActivityAnswerId");
            AlterColumn("dbo.ActivityFile", "Url", c => c.String(nullable: false));
            AlterColumn("dbo.ActivityFile", "RegisterDate", c => c.DateTime());
            AlterColumn("dbo.ActivityFile", "ActivityAnswerId", c => c.Int(nullable: false));
            CreateIndex("dbo.ActivityFile", "ActivityAnswerId");
            AddForeignKey("dbo.ActivityFile", "ActivityAnswerId", "dbo.ActivityAnswers", "Id");
            DropColumn("dbo.ActivityFile", "ActivityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ActivityFile", "ActivityId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ActivityFile", "ActivityAnswerId", "dbo.ActivityAnswers");
            DropIndex("dbo.ActivityFile", new[] { "ActivityAnswerId" });
            AlterColumn("dbo.ActivityFile", "ActivityAnswerId", c => c.Int());
            AlterColumn("dbo.ActivityFile", "RegisterDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ActivityFile", "Url", c => c.String());
            RenameColumn(table: "dbo.ActivityFile", name: "ActivityAnswerId", newName: "ActivityAnswer_Id");
            CreateIndex("dbo.ActivityFile", "ActivityAnswer_Id");
            CreateIndex("dbo.ActivityFile", "ActivityId");
            AddForeignKey("dbo.ActivityFile", "ActivityAnswer_Id", "dbo.ActivityAnswers", "Id");
            AddForeignKey("dbo.ActivityFile", "ActivityId", "dbo.Activities", "Id");
        }
    }
}
