namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v146_PARP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserCCT", "ClasificationSchool", c => c.String(nullable:true));
            AddColumn("dbo.UserCCT", "State", c => c.String(nullable: true));
            AddColumn("dbo.UserCCT", "Zone", c => c.String(nullable: true));
            AddColumn("dbo.UserCCT", "Control", c => c.String(nullable: true));
            AddColumn("dbo.UserCCT", "Promoter", c => c.String(nullable: true));
            //AddColumn("dbo.V_ReportExam", "endDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            //DropColumn("dbo.V_ReportExam", "endDate");
            DropColumn("dbo.UserCCT", "Promoter");
            DropColumn("dbo.UserCCT", "Control");
            DropColumn("dbo.UserCCT", "Zone");
            DropColumn("dbo.UserCCT", "State");
            DropColumn("dbo.UserCCT", "ClasificationSchool");
        }
    }
}
