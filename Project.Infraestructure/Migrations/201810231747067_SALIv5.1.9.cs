namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv519 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentResourceTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        Active = c.Boolean(nullable: false),
                        RegisterDate = c.Boolean(nullable: false),
                        UpdateDate = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ContentResourceTypes");
        }
    }
}
