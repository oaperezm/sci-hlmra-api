namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1220_PARP : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HomeWorkAnswers", "AddNewAnswers", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HomeWorkAnswers", "AddNewAnswers", c => c.Int());
        }
    }
}
