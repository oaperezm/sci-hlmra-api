namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv931 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tips", "UrlImage", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tips", "UrlImage");
        }
    }
}
