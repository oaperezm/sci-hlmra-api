namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv939 : DbMigration
    {
        public override void Up()
        {           
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContentTag",
                c => new
                    {
                        ContentId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContentId, t.TagId })
                .ForeignKey("dbo.Contents", t => t.ContentId, cascadeDelete: true)
                .ForeignKey("dbo.Tag", t => t.TagId, cascadeDelete: true)
                .Index(t => t.ContentId)
                .Index(t => t.TagId);           
        
        }
        
        public override void Down()
        {
           
            DropForeignKey("dbo.ContentTag", "TagId", "dbo.Tag");
            DropForeignKey("dbo.ContentTag", "ContentId", "dbo.Contents");
            DropIndex("dbo.ContentTag", new[] { "TagId" });
            DropIndex("dbo.ContentTag", new[] { "ContentId" });           
            DropTable("dbo.ContentTag");
            DropTable("dbo.Tag");
        }
    }
}
