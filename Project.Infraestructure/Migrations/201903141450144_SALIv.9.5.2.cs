namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv952 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCCT",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Season = c.String(nullable: false, maxLength: 10),
                        SubSeason = c.String(nullable: false, maxLength: 10),
                        NodeId = c.Int(nullable: false),
                        RolId = c.Int(nullable: false),
                        CCT = c.String(nullable: false, maxLength: 10),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: false)
                .ForeignKey("dbo.Roles", t => t.RolId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.NodeId)
                .Index(t => t.RolId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCCT", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserCCT", "RolId", "dbo.Roles");
            DropForeignKey("dbo.UserCCT", "NodeId", "dbo.Nodes");
            DropIndex("dbo.UserCCT", new[] { "RolId" });
            DropIndex("dbo.UserCCT", new[] { "NodeId" });
            DropIndex("dbo.UserCCT", new[] { "UserId" });
            DropTable("dbo.UserCCT");
        }
    }
}
