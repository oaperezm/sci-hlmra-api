namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SAILv01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Module = c.String(),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        Description = c.String(maxLength: 250),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        MiddleName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        RegisterDate = c.String(),
                        UpdateDate = c.String(),
                        Active = c.String(nullable: false),
                        RolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);
            
            CreateTable(
                "dbo.RolPermission",
                c => new
                    {
                        RolId = c.Int(nullable: false),
                        PermissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RolId, t.PermissionId })
                .ForeignKey("dbo.Roles", t => t.RolId, cascadeDelete: true)
                .ForeignKey("dbo.Permission", t => t.PermissionId, cascadeDelete: true)
                .Index(t => t.RolId)
                .Index(t => t.PermissionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RolId", "dbo.Roles");
            DropForeignKey("dbo.RolPermission", "PermissionId", "dbo.Permission");
            DropForeignKey("dbo.RolPermission", "RolId", "dbo.Roles");
            DropIndex("dbo.RolPermission", new[] { "PermissionId" });
            DropIndex("dbo.RolPermission", new[] { "RolId" });
            DropIndex("dbo.Users", new[] { "RolId" });
            DropTable("dbo.RolPermission");
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
            DropTable("dbo.Permission");
        }
    }
}
