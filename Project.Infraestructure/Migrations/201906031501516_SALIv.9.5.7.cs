namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv957 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CoursePlanning",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Homework = c.Single(),
                        Exam = c.Single(nullable: true),
                        Assistance = c.Single(nullable: true),
                        Activity = c.Single(nullable: true),
                        NumberPartial = c.Int(nullable: true),
                        StudentGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentGroups", t => t.StudentGroupId)
                .Index(t => t.StudentGroupId);

            Sql("ALTER TABLE dbo.CoursePlanning ADD DEFAULT (0) for Homework");
            Sql("ALTER TABLE dbo.CoursePlanning ADD DEFAULT (0) for Exam");
            Sql("ALTER TABLE dbo.CoursePlanning ADD DEFAULT (0) for Assistance");
            Sql("ALTER TABLE dbo.CoursePlanning ADD DEFAULT (0) for Activity");
            Sql("ALTER TABLE dbo.CoursePlanning ADD DEFAULT (0) for NumberPartial");


        }
        
        public override void Down()
        {
            
            DropForeignKey("dbo.CoursePlanning", "StudentGroupId", "dbo.StudentGroups");
            DropIndex("dbo.CoursePlanning", new[] { "StudentGroupId" });
            DropTable("dbo.CoursePlanning");
        }
    }
}
