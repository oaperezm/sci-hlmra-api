namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v148_PARP : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.V_ReportTeacher",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            TeacherId = c.Int(nullable: false),
            //            Teacher = c.String(),
            //            Student = c.String(),
            //            StudentEmail = c.String(),
            //            Institution = c.String(),
            //            StudentUserId = c.Int(nullable: false),
            //            StudentGroupId = c.Int(nullable: false),
            //            StudentGroupDescription = c.String(),
            //            CourseId = c.Int(nullable: false),
            //            Subject = c.String(),
            //            Level = c.String(),
            //            DaysPassed = c.Int(nullable: false),
            //            totalAttendances = c.Int(nullable: false),
            //            CheckDate = c.Int(nullable: false),
            //            Exam = c.Int(nullable: false),
            //            Assistance = c.Int(nullable: false),
            //            Activity = c.Int(nullable: false),
            //            CheckDate1 = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);

            Sql("CREATE VIEW dbo.V_ReportTeacher AS select " +
                "Description from dbo.Nodes where ParentId=148");

        }
        
        public override void Down()
        {
            Sql(" drop VIEW dbo.V_ReportTeacher");
        }
    }
}
