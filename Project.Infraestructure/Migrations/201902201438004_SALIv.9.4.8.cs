namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv948 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "BlockId", c => c.Int());
            AddColumn("dbo.Contents", "IsRecommended", c => c.Boolean(nullable: false, defaultValue: false));
            CreateIndex("dbo.Contents", "BlockId");
            AddForeignKey("dbo.Contents", "BlockId", "dbo.Blocks", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "BlockId", "dbo.Blocks");
            DropIndex("dbo.Contents", new[] { "BlockId" });
            DropColumn("dbo.Contents", "IsRecommended");
            DropColumn("dbo.Contents", "BlockId");
        }
    }
}
