namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv516 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ProtectedTicket", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ProtectedTicket");
        }
    }
}
