namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv955 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "ImageWidth", c => c.Int());
            AddColumn("dbo.Answers", "ImageHeight", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "ImageHeight");
            DropColumn("dbo.Answers", "ImageWidth");
        }
    }
}
