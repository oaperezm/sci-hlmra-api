namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv956TestingF : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.VideoConference SET Commentary=''");
            Sql("UPDATE dbo.VideoConference SET Editor=''");
            Sql("UPDATE dbo.VideoConference SET EditorId=0");
        }
        
        public override void Down()
        {
        }
    }
}
