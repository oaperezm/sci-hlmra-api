namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5110 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "ContentResourceTypeId", c => c.Int());
            AlterColumn("dbo.ContentResourceTypes", "RegisterDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ContentResourceTypes", "UpdateDate", c => c.DateTime());
            CreateIndex("dbo.Contents", "ContentResourceTypeId");
            AddForeignKey("dbo.Contents", "ContentResourceTypeId", "dbo.ContentResourceTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "ContentResourceTypeId", "dbo.ContentResourceTypes");
            DropIndex("dbo.Contents", new[] { "ContentResourceTypeId" });
            AlterColumn("dbo.ContentResourceTypes", "UpdateDate", c => c.Boolean());
            AlterColumn("dbo.ContentResourceTypes", "RegisterDate", c => c.Boolean(nullable: false));
            DropColumn("dbo.Contents", "ContentResourceTypeId");
        }
    }
}
