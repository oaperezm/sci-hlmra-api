namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5113 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Glossaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Description = c.String(nullable: false, maxLength: 500),
                        Active = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GlossaryWords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Description = c.String(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                        GlossatyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glossaries", t => t.GlossatyId, cascadeDelete: true)
                .Index(t => t.GlossatyId);
            
            AddColumn("dbo.Nodes", "Glossary_Id", c => c.Int());
            CreateIndex("dbo.Nodes", "Glossary_Id");
            AddForeignKey("dbo.Nodes", "Glossary_Id", "dbo.Glossaries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GlossaryWords", "GlossatyId", "dbo.Glossaries");
            DropForeignKey("dbo.Glossaries", "UserId", "dbo.Users");
            DropForeignKey("dbo.Nodes", "Glossary_Id", "dbo.Glossaries");
            DropIndex("dbo.GlossaryWords", new[] { "GlossatyId" });
            DropIndex("dbo.Glossaries", new[] { "UserId" });
            DropIndex("dbo.Nodes", new[] { "Glossary_Id" });
            DropColumn("dbo.Nodes", "Glossary_Id");
            DropTable("dbo.GlossaryWords");
            DropTable("dbo.Glossaries");
        }
    }
}
