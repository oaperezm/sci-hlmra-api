namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv944 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "Description", c => c.String());
            AlterColumn("dbo.Purposes", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.News", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Tips", "Description", c => c.String(nullable: false, maxLength: 800));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tips", "Description", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.News", "Description", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Purposes", "Description", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Contents", "Description");
        }
    }
}
