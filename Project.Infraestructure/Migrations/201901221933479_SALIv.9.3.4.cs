namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv934 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Systems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 100),
                        Uri = c.String(),
                        RegisterDate = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Users", "SystemId", c => c.Int());
            AddColumn("dbo.Roles", "SystemId", c => c.Int());
            CreateIndex("dbo.Users", "SystemId");
            CreateIndex("dbo.Roles", "SystemId");
            AddForeignKey("dbo.Roles", "SystemId", "dbo.Systems", "Id");
            AddForeignKey("dbo.Users", "SystemId", "dbo.Systems", "Id");
            DropTable("dbo.News");
            DropTable("dbo.Tips");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Tips",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 255),
                        RegisterDate = c.DateTime(nullable: false),
                        UrlImage = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 255),
                        RegisterDate = c.DateTime(nullable: false),
                        UrlImage = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Users", "SystemId", "dbo.Systems");
            DropForeignKey("dbo.Roles", "SystemId", "dbo.Systems");
            DropIndex("dbo.Roles", new[] { "SystemId" });
            DropIndex("dbo.Users", new[] { "SystemId" });
            DropColumn("dbo.Roles", "SystemId");
            DropColumn("dbo.Users", "SystemId");
            DropTable("dbo.Systems");
        }
    }
}
