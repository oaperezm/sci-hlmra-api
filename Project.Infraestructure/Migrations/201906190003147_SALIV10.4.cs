namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIV104 : DbMigration
    {
        public override void Up()
        {            
            CreateTable(
              "dbo.SchedulingPartial",
              c => new
              {
                  Id = c.Int(nullable: false, identity: true),
                  StartDate = c.DateTime(nullable: false),
                  EndDate = c.DateTime(nullable: false),
                  Description = c.String(),
                  FinalPartial = c.Int(nullable: false),
                  RegisterDate = c.DateTime(),
                  UpdateDate = c.DateTime(),
                  CoursePlanningId = c.Int(nullable: false),
              })
              .PrimaryKey(t => t.Id)
              .ForeignKey("dbo.CoursePlanning", t => t.CoursePlanningId)
              .Index(t => t.CoursePlanningId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchedulingPartial", "CoursePlanningId", "dbo.CoursePlanning");
            DropIndex("dbo.SchedulingPartial", new[] { "CoursePlanningId" });
            DropTable("dbo.SchedulingPartial");
        }
    }
}
