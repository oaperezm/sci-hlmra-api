namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv5115 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Nodes", "Glossary_Id", "dbo.Glossaries");
            DropIndex("dbo.Nodes", new[] { "Glossary_Id" });
            CreateTable(
                "dbo.GlossaryNodes",
                c => new
                    {
                        GlossaryId = c.Int(nullable: false),
                        NodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GlossaryId, t.NodeId })
                .ForeignKey("dbo.Glossaries", t => t.GlossaryId, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .Index(t => t.GlossaryId)
                .Index(t => t.NodeId);
            
            DropColumn("dbo.Nodes", "Glossary_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Nodes", "Glossary_Id", c => c.Int());
            DropForeignKey("dbo.GlossaryNodes", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.GlossaryNodes", "GlossaryId", "dbo.Glossaries");
            DropIndex("dbo.GlossaryNodes", new[] { "NodeId" });
            DropIndex("dbo.GlossaryNodes", new[] { "GlossaryId" });
            DropTable("dbo.GlossaryNodes");
            CreateIndex("dbo.Nodes", "Glossary_Id");
            AddForeignKey("dbo.Nodes", "Glossary_Id", "dbo.Glossaries", "Id");
        }
    }
}
