namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv1140 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    PartialEvaluation = c.Int(nullable: false),
                    Name = c.String(nullable: true),
                    Description = c.String(nullable: true),
                    OpeningDate = c.DateTime(nullable: true),
                    ScheduledDate = c.DateTime(nullable: true),
                    RegisterDate = c.DateTime(),
                    UserId = c.Int(nullable: true),
                    UpdateDate = c.DateTime(),
                    StudentGroupId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
               "dbo.ActivitiesFiles",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   ActivityId = c.Int(nullable: false),
                   Url = c.String(nullable: false),
                   Comment = c.String(maxLength: 500),
                   RegisterDate = c.DateTime(),
               })
               .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
        
            DropTable("dbo.Activities");
            DropTable("dbo.ActivitiesFiles");
            
        }
    }
}
