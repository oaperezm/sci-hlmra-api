namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALI_v150_JVH : DbMigration
    {
        public override void Up()
        {
           
            AlterColumn("dbo.Tests", "Score", c => c.Decimal(precision: 6, scale: 2));
          
        }
        
        public override void Down()
        {
            
            AlterColumn("dbo.Tests", "Score", c => c.Decimal(precision: 3, scale: 2));
           
        }
    }
}
