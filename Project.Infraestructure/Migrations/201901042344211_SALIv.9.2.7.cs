namespace Project.Infraestructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALIv927 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForumPostVotes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ForumPostId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Like = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ForumPosts", t => t.ForumPostId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ForumPostId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumPostVotes", "UserId", "dbo.Users");
            DropForeignKey("dbo.ForumPostVotes", "ForumPostId", "dbo.ForumPosts");
            DropIndex("dbo.ForumPostVotes", new[] { "UserId" });
            DropIndex("dbo.ForumPostVotes", new[] { "ForumPostId" });
            DropTable("dbo.ForumPostVotes");
        }
    }
}
