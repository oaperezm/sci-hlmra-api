﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using Repositories.Management;
using Entities;
using Domain.Management;

namespace Tests.Management
{
    [TestClass]
    public class StoreTest
    {
        private IStore _sut;
        private Mock<IStoreRepository> _mock;
        private List<Product> _products;

        [TestInitialize]
        public void Setup()
        {
            
            _products = new List<Product>
            {
                new Product{Id=1, Name="P001"},
                new Product{Id=2, Name="P002"},
                new Product{Id=3, Name="P003"},
                new Product{Id=4, Name="P004"},
                new Product{Id=5, Name="P005"}
            };

            _mock = new Mock<IStoreRepository>();
            _mock.Setup(x => x.Add(It.IsAny<Product>())).Returns((Product p) =>
            {
                _products.Add(p);
                return p;
            });
            _mock.Setup(x => x.Remove(It.IsAny<Product>())).Returns((Product p) =>
            {
                _products.Remove(p);
                return p;
            });

            _mock.Setup(x => x.List(It.IsAny<Product>())).Returns((Product p) =>
            {
                return _products;
            });

            _sut = new Store(_mock.Object);


        }

        [TestMethod]
        public void Should_Insert_Ok_When_Parameters_Complete()
        {
            var result = _sut.Add(new Product { Id = 6, Name = "P006" });
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Product));
            Assert.AreEqual("P006", result.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Should_Insert_Fail_When_Parameter_Name_Ignored()
        {
            _sut.Add(new Product { Id = 6, Name="" });
        }

        [TestMethod]
        public void Should_Delete_When_IdParameter_Complete()
        {
            var result = _sut.Remove(new Product { Id = 1, Name = "P001" });
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Product));
            Assert.AreEqual("P001", result.Name);
        }

        [TestMethod]
        public void Should_List()
        {
            var result = _sut.List(new Product { Id = 1, Name = "P001" });
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Product>));
            Assert.AreEqual(5, result.Count);
        }
    }
}
