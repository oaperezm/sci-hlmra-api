﻿using Project.Entities;
using Project.Entities.Responses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Helpers
{
    public static class PaginationHelper
    {
        public static void SetData(this Pagination pagination, int totalElements , int currentPage, int sizePage) {


            if (sizePage > 0)
                pagination.PageSize = sizePage;

            pagination.Total            = totalElements;
            pagination.TotalPage        = (int)Math.Ceiling(pagination.Total / (double)pagination.PageSize);
            pagination.ShowPreviousPage = currentPage > 1 ? true : false;
            pagination.ShowNextPage     = currentPage < pagination.TotalPage ? true : false;
            
        }
    }
}
