﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Helpers
{
    public static class NodeUtility
    {
        public static List<Entities.Node> GetFullStructureByList(List<Entities.Node> nodes, List<Entities.Node> fullStrcuture)
        {

            var structure = new List<Entities.Node>();

            foreach (var p in nodes)
            {

                var parentId = p.ParentId;

                while (parentId > 0)
                {
                    var parent = fullStrcuture.FirstOrDefault(x => x.Id == parentId);

                    if (parent != null)
                    {


                        if (!structure.Any(s => s.Id == parent.Id))
                        {
                            structure.Add(new Entities.Node
                            {
                                Active = parent.Active,
                                Description = parent.Description,
                                Id = parent.Id,
                                Name = parent.Name,
                                NodeTypeId = parent.NodeTypeId,
                                ParentId = parent.ParentId,
                                UrlImage = parent.UrlImage
                            });
                        }


                        parentId = parent.ParentId;
                    }
                    else
                    {
                        parentId = 0;
                    }
                }
            }

            return structure;
        }

    }
}
