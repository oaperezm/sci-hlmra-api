﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IAuthor
    {
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter);
        Task<Response> GetById(int id);
        Task<Response> AddAuthor(AuthorSaveRequest request);
        Task<Response> UpdateAuthor(AuthorUpdateRequest request);
        Task<Response> DeleteAuthor(int AuthorId);
    }
}
