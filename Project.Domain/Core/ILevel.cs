﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface ILevel
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddLevel(LevelSaveRequest request);
        Task<Response> UpdateLevel(LevelUpdateRequest request);
        Task<Response> DeleteLevel(int LevelId);
    }
}
