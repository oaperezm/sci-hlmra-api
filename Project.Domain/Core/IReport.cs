﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public  interface IReport
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(ReportsRequest request);
        Task<Response> Update(ReportsUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
