﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ICoursePlanning
    {
        Task<Response> Add(CoursePlanningSaveRequest request);
        Task<Response> Update(CoursePlanningUpdateRequest request);
        Task<Response> GetById(int id);
        Task<Response> GetAll();
        Task<Response> Delete(int id);
        Task<Response> GetByGroup(int id);
        Task<Response> GetByStudentId(int StudentId, int StudentGroupId);
    }
}
