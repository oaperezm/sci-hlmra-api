﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface IVStudents
    {
        Task<Response> GetAllByIdTeacher(int teacherId);
        Task<ResponsePagination> GetAll(PaginationRelationRequest request);
    }
}
