﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ISection
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(SectionsRequest request);
        Task<Response> Update(SectionsUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
