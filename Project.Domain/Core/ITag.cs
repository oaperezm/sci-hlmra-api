﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ITag
    {
        Task<Response> Add(TagSaveRequest request);
        Task<Response> Update(TagUpdateRequest request);
        Task<Response> Delete(int tagId);
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter);
    }
}
