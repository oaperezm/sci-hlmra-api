﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
   public interface IVideoconference
    {
        Task<Response> GetAll(int systemId, int UserId);
        Task<Response> GetNodo();
        Task<Response> GetById(int id);
        Task<Response> Add(VideoConferenceSaveRequest request,int systemId, int userId);
        Task<Response> Update(VideoConferenceUpdateRequest request, int systemId);
        Task<Response> Delete(int id, int systemId);
        Task<Response> Add(VideoConferenceSaveRequest request,InvitedVideoConferenceSaveRequest invited, InvitedVideoConferenceSaveRequest externalguest);
    }
}
