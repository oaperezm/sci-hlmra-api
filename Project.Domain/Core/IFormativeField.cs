﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IFormativeField
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddFormativeField(FormativeFieldSaveRequest request);
        Task<Response> UpdateFormativeField(FormativeFieldUpdateRequest request);
        Task<Response> DeleteFormativeField(int FormativeFieldId);
    }
}
