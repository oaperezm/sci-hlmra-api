﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IArea
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddArea(AreaSaveRequest request);
        Task<Response> UpdateArea(AreaUpdateRequest request);
        Task<Response> DeleteArea(int AreaId);
    }
}
