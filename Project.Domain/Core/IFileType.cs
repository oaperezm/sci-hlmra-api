﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IFileType
    {
        Task<Response> GetAll(int systemId);
        Task<Response> GetById(int id);
        Task<Response> AddFileType(FileTypeSaveRequest request);
        Task<Response> UpdateFileType(FileTypeUpdateRequest request);
        Task<Response> DeleteFileType(int FileTypeId);
    }
}
