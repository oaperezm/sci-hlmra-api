﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IAxis
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(AxisRequest request);
        Task<Response> Update(AxisUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
