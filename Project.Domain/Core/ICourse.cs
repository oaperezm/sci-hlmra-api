﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ICourse
    {
        Task<Response> GetAll();
        Task<Response> GetAllByIdUser(int userId);
        Task<Response> GetById(int id);
        Task<Response> Add(CourseSaveRequest request);
        Task<Response> Update(CourseUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetAllByIdUser(int userId, int syatemId);
        Task<Response> GetAllCourseGroup();
    }
}
