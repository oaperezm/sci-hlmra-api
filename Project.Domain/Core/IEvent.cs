﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
   public interface IEvent
    {
        Task<Response> Add(EventRequest request);
        Task<Response> Delete(int id);
    }
}
