﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IPublishingProfileUser
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPublishingProfileUser(PublishingProfileUserSaveRequest request);
        Task<Response> UpdatePublishingProfileUser(PublishingProfileUserUpdateRequest request);
        Task<Response> DeletePublishingProfileUser(int PublishingProfileUserId);
        Task<Response> GetByUserId(int userId);
        

    }
}
