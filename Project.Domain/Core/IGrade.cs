﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IGrade
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddGrade(GradeSaveRequest request);
        Task<Response> UpdateGrade(GradeUpdateRequest request);
        Task<Response> DeleteGrade(int GradeId);
    }
}
