﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface INodeType
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNodeType(NodeTypeSaveRequest request);
        Task<Response> UpdateNodeType(NodeTypeUpdateRequest request);
        Task<Response> DeleteNodeType(int NodeTypeId);
    }
}
