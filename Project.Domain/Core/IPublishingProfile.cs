﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IPublishingProfile
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request);
        Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request);
        Task<Response> DeletePublishingProfile(int PublishingProfileId);
        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
        Task<Response> GetLinkedUsers(int profileId);
    }
}
