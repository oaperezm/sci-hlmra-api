﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ITeacherExam
    {
        Task<Response> Add(TeacherExamSaveRequest request);
        Task<Response> Update(TeacherExamUpdateRequest request);
        Task<Response> GetByUserId(int userId);
        Task<Response> GetById(int id, bool converImageBase64 = false);
        Task<Response> GetExamSchedule(int id, int examScheduleId, int userId, bool converImageBase64 = false);
        Task<Response> StartExamSchedule(int id, int examScheduleId, int userId, bool converImageBase64 = false);
        Task<Response> Delete(int id);
        Task<ResponsePagination> GetAllByUserPagination(int currentPage, int sizePage, string filter, int type, int userId);
        Task<Response> GetByUserIdShortInfo(int userId);
        Task<Response> UpdateWeighting(TeacherExamUpdateRequest request);

    }
}
