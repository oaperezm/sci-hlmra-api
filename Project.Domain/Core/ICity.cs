﻿using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface ICity
    {
        Task<Response> GetAll();
        Task<Response> GetStateByCity(int id);
    }
}
