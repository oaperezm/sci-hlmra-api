﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IPermission
    {
        Task<Response> GetAll(int systemId);
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId);
        Task<Response> GetById(int id);
        Task<Response> AddPermission(PermissionSaveRequest request);
        Task<Response> UpdatePermission(PermissionSaveRequest request);
        Task<Response> DeletePermission(int roleId);        
    }
}
