﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface ISubsystem
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddSubsystem(SubsystemSaveRequest request);
        Task<Response> UpdateSubsystem(SubsystemUpdateRequest request);
        Task<Response> DeleteSubsystem(int SubsystemId);
    }
}
