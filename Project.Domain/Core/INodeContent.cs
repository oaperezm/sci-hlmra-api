﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface INodeContent
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNodeContent(NodeContentSaveRequest request);
        Task<Response> UpdateNodeContent(NodeContentUpdateRequest request);
        Task<Response> DeleteNodeContent(int nodeContentId);
    }
}
