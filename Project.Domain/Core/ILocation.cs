﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface ILocation
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddLocation(LocationSaveRequest request);
        Task<Response> UpdateLocation(LocationUpdateRequest request);
        Task<Response> DeleteLocation(int LocationId);
    }
}
