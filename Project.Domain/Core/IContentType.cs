﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IContentType
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddContentType(ContentTypeSaveRequest request);
        Task<Response> UpdateContentType(ContentTypeUpdateRequest request);
        Task<Response> DeleteContentType(int ContentTypeId);
    }
}
