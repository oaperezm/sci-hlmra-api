﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface ITrainingField
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddTrainingField(TrainingFieldSaveRequest request);
        Task<Response> UpdateTrainingField(TrainingFieldUpdateRequest request);
        Task<Response> DeleteTrainingField(int TrainingFieldId);
    }
}
