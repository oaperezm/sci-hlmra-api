﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Core
{
    public interface INews
    {
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId);
        Task<Response> AddNews(NewsSaveRequest request);
        Task<Response> UpdateNews(NewsUpdateRequest request);
        Task<Response> DeleteNews(int id);
        Task<Response> GetById(int id);
    }
}
