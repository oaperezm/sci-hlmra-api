﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IBlock
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddBlock(BlockSaveRequest request);
        Task<Response> UpdateBlock(BlockUpdateRequest request);
        Task<Response> DeleteBlock(int BlockId);
    }
}
