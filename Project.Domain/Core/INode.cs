﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface INode
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNode(NodeSaveRequest request);
        Task<Response> UpdateNode(NodeUpdateRequest request);
        Task<Response> DeleteNode(int NodeId);
        Task<Response> GetSubnodes(int id, int systemId);
        Task<Response> GetNodeContents(int id, int rolId);
        Task<Response> RemoveImage(int id);
        Task<Response> GetNodeStructureByParentId(int id, bool fullData = false);
        Task<ResponsePagination> GetBooksByNodeId(int nodeId, int curentPage, int sizePage, int userId);
        Task<Response> GetInventoryFiles(int id, int systemId);
        Task<ResponsePagination> GetInventoryFileTypes(int id, int fileType, int currentPage, int sizePage, int systemId);
        
        #region RA
        Task<ResponsePagination> GetContentByTypeSearch(int nodeId, int currentPage, int sizePage, int searchId, int systemId);
        Task<ResponsePagination> GetNodeContentRecursive(int nodeId, List<int> fileTypeId, int purposeId, int blockId, int formativeFieldId, int currentPage, int sizePage, int systemId, string filter, int rolId
                                                          , int EducationLevelId, int AreaPersonalSocialDevelopmentId, int AreasCurriculumAutonomyId, int AxisId, int KeylearningId, int LearningexpectedId);        
        #endregion

    }
}
