﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IBook
    {
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter);
        Task<Response> GetById(int id);
        Task<Response> AddBook(BookSaveRequest request);
        Task<Response> UpdateBook(BookUpdateRequest request);
        Task<Response> DeleteBook(int BookId);
    }
}
