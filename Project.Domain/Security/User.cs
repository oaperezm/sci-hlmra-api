﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Management;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using Project.Entities.Response;
using Project.Entities.Responses;
using System.Configuration;
using Project.Entities.Enums;
using Project.Entities.Converters;
using Project.Domain.Helpers;

namespace Project.Domain.Management
{
    public class User : IUser
    {
        private readonly IUserRepository _userRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IPermissionRepository _permissionRepository;
        private readonly IPublishingProfileRepository _publishingProfileRepository;
        private readonly IPublishingProfileUserRepository _publishingProfileUserRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMailer _imailer;
        private readonly IAESEncryption _iencrypter;
        private readonly IFileUploader _fileUploader;
        private readonly IRequestService _request;
        private readonly IUserNotificationRepository _userNotificationRepository;
        private readonly IUserDeviceRepository _userDeviceRepository;
        private readonly IUserCCTRepository _userCCTRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IUserAccessRepository _userAccessRepository;
        private readonly IRoleRepository _roleRepository;

        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        public User(
            IUserRepository userRepositor,
            INodeRepository nodeRepository,
            IPublishingProfileRepository publishingProfilerepository,
            IPublishingProfileUserRepository publishingProfileUserrepository,
            IPermissionRepository permissionRepository,
            IStudentGroupRepository studentGroupRepository,
            IStudentRepository studentRepository,
            IBookRepository bookRepository,
            IUnitOfWork unitOfWor,
            IMailer imailer,
            IAESEncryption iencrypter,
            IFileUploader fileUploader,
            IRequestService request,
            IUserDeviceRepository userDeviceRepository,
            IUserNotificationRepository userNotificationRepository,
            IUserCCTRepository userCCTRepository,
            IUserAccessRepository userAccessRepository,
            IPushNotificationService srvPushNotification,
            IContentRepository contentRepository,
            IRoleRepository roleRepository) {

            this._userRepository                  = userRepositor;
            this._unitOfWork                      = unitOfWor;
            this._imailer                         = imailer;
            this._iencrypter                      = iencrypter;
            this._nodeRepository                  = nodeRepository;
            this._permissionRepository            = permissionRepository;
            this._publishingProfileRepository     = publishingProfilerepository;
            this._publishingProfileUserRepository = publishingProfileUserrepository;
            this._studentGroupRepository          = studentGroupRepository;
            this._studentRepository               = studentRepository;
            this._fileUploader                    = fileUploader;
            this._contentRepository               = contentRepository;
            this._request                         = request;
            this._bookRepository                  = bookRepository;
            this._userDeviceRepository            = userDeviceRepository;
            this._userNotificationRepository      = userNotificationRepository;
            this._userCCTRepository               = userCCTRepository;
            this._userAccessRepository            = userAccessRepository;
            this._srvPushNotification             = srvPushNotification;
            this._roleRepository                  = roleRepository;
        }

        #region METHODS

        public async Task<Response> ConfirmSuscripcion(int email)
        {

            var result = new Response();
            string sitio = "https://sistema.hlmra.com/";

                try
                {
                    //var user = await _userRepository.FindAsync(x => x.Email.Equals(email));
                    var user = await _userRepository.FindAsync(x => x.Id.Equals(email));

                    if (user == null) throw new Exception("El usuario no existe");
                    if (user.Active)
                    {
                        result.Data = sitio;
                        result.Success = true;
                        result.Message = "Usuario ya activo";

                    }
                    else
                    {

                        user.Active = true;
                        this._userRepository.Update(user);
                        this._unitOfWork.SaveChanges();

                        result.Data = sitio;
                        result.Success = true;
                        result.Message = "Usuario activado exitosamente";
                    }



                }
                catch (Exception ex)
                {
                    result.Data = sitio;
                    result.Success = false;
                    result.Message = ex.Message;
                }
           

            return result;
        }


        /// <summary>
        /// Obtiene los permisos, perfiles a los cuales tiene acceso el usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUserProfile(int userId, int systemId, int origin) {

            var result = new Response();
            var fullPermision = new List<Project.Entities.Node>();
            var profileNodes = new List<Entities.Node>();
            var publishingNodes = new List<Entities.Node>();
            var topics = new List<string>();

            try
            {
                var user = _userRepository.GetById(userId);

                if (user == null)
                    throw new Exception("Usuario no encontrado");

                var nodes    = await _nodeRepository.FindAllAsyncInclude(x => x.SystemId == systemId, "NodeType");
                profileNodes = user.PublishingProfileUsers.SelectMany(x => x.PublishingProfile.Nodes)
                                                          .Union(user.Nodes)
                                                          .Select(n => new Project.Entities.Node {
                                                              Id = n.Id,
                                                              Name = n.Name,
                                                              Description = n.Description,
                                                              NodeTypeId = n.NodeTypeId,
                                                              UrlImage = n.UrlImage,
                                                              ParentId = n.ParentId,
                                                              Active = n.Active,
                                                              NodeType = new Entities.NodeType { Id = n.NodeType.Id, Description = n.NodeType.Description },
                                                              NodeAccessTypeId = (int)EnumNodeAccessType.Owner,
                                                              Book = new Entities.Book
                                                              {
                                                                  Id =  n.BookId.HasValue ? n.Book.Id : 0,
                                                                  Name = n.Book?.Name,
                                                                  ISBN = n.Book?.ISBN,
                                                                  Authors = n.Book != null ? n.Book.Authors.Select(x => new Entities.Author
                                                                  {
                                                                      FullName = x.FullName,
                                                                      LastName = x.LastName,
                                                                      Comment = x.Comment,
                                                                  }).ToList() : new List<Entities.Author>()
                                                              }
                                                          }).Distinct().ToList();

                 var nodesContentPublic = await this.GetPublicContent(publishingNodes, systemId, nodes.ToList());
                 publishingNodes = profileNodes.Concat(nodesContentPublic).Distinct().ToList();

                foreach (var p in publishingNodes)
                {
                    var parentId = p.ParentId;

                    while (parentId > 0)
                    {
                        var parent = nodes.FirstOrDefault(x => x.Id == parentId);

                        if (parent != null)
                        {
                            if (!publishingNodes.Any(x => x.Id == parent.Id) && !fullPermision.Any(x => x.Id == parent.Id))
                            {

                                if (!fullPermision.Contains(parent))
                                {
                                    fullPermision.Add(new Entities.Node
                                    {
                                        Active = parent.Active,
                                        Description = parent.Description,
                                        Id = parent.Id,
                                        Name = parent.Name,
                                        NodeTypeId = parent.NodeTypeId,
                                        ParentId = parent.ParentId,
                                        UrlImage = parent.UrlImage,
                                        NodeType = new Entities.NodeType {
                                            Id = parent.NodeType.Id,
                                            Description = parent.NodeType.Description
                                        },
                                        Book = new Entities.Book
                                        {
                                            Id = parent.BookId.HasValue ? parent.Book.Id : 0,
                                            Name = parent.Book?.Name,
                                            ISBN = parent.Book?.ISBN,
                                            Authors = parent.Book != null ? parent.Book.Authors.Select(x => new Entities.Author
                                            {
                                                FullName = x.FullName,
                                                LastName = x.LastName,
                                                Comment = x.Comment,
                                            }).ToList() : new List<Entities.Author>()
                                        }
                                    });
                                }
                            }

                            parentId = parent.ParentId;
                        }
                        else
                        {
                            parentId = 0;
                        }
                    }
                }

                fullPermision.Distinct().ToList().ForEach(f => {
                    if (!publishingNodes.Contains(f))
                        publishingNodes.Add(f);
                });

                publishingNodes = publishingNodes.Select(x => x).Distinct().ToList();

                var publishing = publishingNodes.Where(x => x.Id == 522).ToList();

                // Obtener los permisos del usuario                
                var userPermisions = new List<Entities.Permission>();
                userPermisions     = user.Role.Permissions.ToList();

                // Validar que el sistema es RA y se obtienen los permisos asigandos por cada CCT registrado
                if (user.SystemId == (int)EnumSystems.RA && origin == (int)EnumSystems.RA ) {
                    var cctPermissions = user.UserCCTs.SelectMany(r => r.Role.Permissions)
                                             .Distinct()
                                             .ToList();

                    if (cctPermissions.Count > 0)
                        userPermisions = cctPermissions;
                }
                
                var permissions    = userPermisions.Where(x => x.ParentId == null && x.Active)
                                                   .OrderBy( x=> x.Order)
                                                   .Select(p => new {
                                                            Title = p.Module,
                                                            Description = p.Description,
                                                            Icon = p.Icon,
                                                            Id = p.Id,
                                                            ParentId = 0,
                                                            Url = p.Url,
                                                            ShowInMenu = p.ShowInMenu,
                                                            Children = userPermisions.Where(s => s.ParentId == p.Id)
                                                                                     .OrderBy( s => s.Order )
                                                                                     .Select(s => new {
                                                                                                     Title = s.Module,
                                                                                                     Description = s.Description,
                                                                                                     Icon = s.Icon,
                                                                                                     Id = s.Id,
                                                                                                     Url = s.Url,
                                                                                                     ParentId = s.ParentId.HasValue ? s.ParentId : 0
                                                                                                 })
                                                                                      .OrderBy(s => s.Title)
                                                                                      .ToList()})
                                                        .ToList();

                if (user.RoleId == 3)
                {
                    topics = user.Students.Select(x => $"group_{x.StudentGroupId}").ToList();
                }
                else if (user.RoleId == 2) {
                    var groups = await _studentGroupRepository.FindAllAsync(x => x.Course.UserId == user.Id);
                    topics = groups.Select(g => $"group_{g.Id}").ToList();
                }

                // Obtener todas las notificaciones qu no ha leido el usuario
                var notifications = user.Notifications.Where(n => n.Read == false)
                                        .OrderByDescending(n => n.RegisterDate)
                                        .Select(n => n.New())
                                        .ToList();

                var nodeData = new List<Entities.Node>();

                publishingNodes.ForEach(n =>
                {
                    if (!nodeData.Any(d => d.Id == n.Id))
                        nodeData.Add(n);

                });
                
                result.Data = new {
                    permissions,
                    topics,
                    publishingNodes = nodeData.OrderBy(p => p.Id),
                    notifications,
                    user = new Project.Entities.User {
                        UrlImage = user.UrlImage,
                        FullName = user.FullName,
                        Email    = user.Email,
                        Gender   = user.Gender,
                        State    = new Entities.State {
                            Id   = user.State != null ? user.State.Id : 0,
                            Name = user.State?.Name,
                            City = new Entities.City {
                                Description = user.State?.City.Description
                            }
                        },
                        Role = new Project.Entities.Role {
                            Description = user.Role?.Description,
                            Id          = user.RoleId
                        }
                    },
                    systemId
                };

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener el perfil: {ex.Message}";

            }

            return result;

        }

        /// <summary>
        /// Agregar un nuevo usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RegisterUser(UserSaveRequest request)
        {
            var result = new Response();
            var newUser = new Project.Entities.User();
            string container = "users";
            string fileUrl = string.Empty;

            try
            {

                if (_userRepository.Get(x => x.Email.Equals(request.Email) && x.SystemId == request.SystemId) != null)
                    throw new Exception($"Ya existe un usuario con el correo: { request.Email }");

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileContentBase64 != null && request.FileContentBase64.Length > 0)
                {
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                    newUser.UrlImage = fileUrl;
                }

                newUser.Active          = false;
                newUser.Birthday        = request.Birthday;
                newUser.Email           = request.Email;
                newUser.FullName        = request.FullName;
                newUser.Gender          = request.Gender;
                newUser.Institution     = request.Institution;
                newUser.Password        = this._iencrypter.EncryptMessage(request.Password);
                newUser.RegisterDate    = DateTime.Now;
                newUser.RoleId          = request.RoleId;                
                newUser.LevelId         = request.LevelId;
                newUser.AreaId          = request.AreaId;
                newUser.SubsystemId     = request.SubsystemId;
                newUser.Semester        = request.Semester;
                newUser.Key             = request.Key;
                newUser.SystemId        = request.SystemId;
                newUser.Institution     = request.Institution;

                if(request.StateId > 0)
                    newUser.StateId = request.StateId;
                
                if (request.SystemId == (int)EnumSystems.RA)
                {                    
                   var data = await  this.ValidateUserSIDE(request.Email);

                    if (data.Success) {
                        var seasons = (List<Season>)data.Data;

                        seasons.Distinct().ToList().ForEach(s =>
                        {
                            var userCCT = new UserCCT
                            {
                                CCT       = s.Cct,
                                Season    = s.SeasonName,
                                SubSeason = s.SubSeason,
                                StartDate = DateTime.Parse(s.StartDate),
                                EndDate   = DateTime.Parse(s.EndDate),
                                UserId    = newUser.Id,
                                RolId     = (int)EnumRol.StudentRA
                            };
                                                                                 
                                switch (s.Level.ToLower())
                                {
                                    case "preescolar":
                                        if((int)EnumRol.TeacherRA == request.RoleId)
                                            userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_PRESCOLAR"]);
                                            

                                        userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.PRESCOLAR"]);
                                        break;
                                    case "primaria":
                                        if ((int)EnumRol.TeacherRA == request.RoleId)
                                            userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_PRIMARIA"]);

                                        userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.PRIMARIA"]);
                                        break;
                                    case "secundaria":
                                        if ((int)EnumRol.TeacherRA == request.RoleId)
                                            userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_SECUNDARIA"]);

                                        userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.SECUNDARIA"]);
                                        break;
                                };

                            if(userCCT.NodeId > 0)
                                newUser.UserCCTs.Add(userCCT);
                        });
                    }                   
                }

                
                _userRepository.Add(newUser);
                _unitOfWork.SaveChanges();

                string systemName = "";

                switch (request.SystemId)
                {
                    case (int)EnumSystems.SALI:
                        systemName = ConfigurationManager.AppSettings["SALI.APP.NAME"];
                        break;
                    case (int)EnumSystems.RA:
                        systemName = ConfigurationManager.AppSettings["RA.APP.NAME"];
                        break;
                };

                await this._imailer.SendNewUserMail(new Entities.Utils.WelcomeMessage(request.Email, "secundaria@recursosacademicos.com", $"Bienvenido a { systemName }", "NewUser.html", request.FullName, request.SystemId, request.Password, request.Email, newUser.Id));

                if (request.SystemId == (int)EnumSystems.SALI)
                    await this.AddPBookFromSIDE(newUser.Id);
              
                result.Data = newUser.Email;
                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de un usuario de manera permanente
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteUser(int userId)
        {
            var result = new Response();

            try
            {
                var user = _userRepository.GetById(userId);
                if (user == null)
                    throw new Exception("Usuario no registrado.");

                if (user.Active == false)
                {
                    if (user.Courses.Count == 0
                     && user.Students.Count == 0
                     && user.Notifications.Count == 0
                     && user.UserCCTs.Count == 0
                     && user.Devices.Count == 0
                     && user.Nodes.Count == 0
                     && user.PublishingProfileUsers.Count == 0)
                    {
                        await _userRepository.DeleteAsync(user);
                        result.Message = "Usuario ha sido eliminado";
                    }
                    else
                        throw new Exception("El usuario no puede ser eliminado, existe información asociada a la cuenta.");
                }
                else
                {
                    user.Active = false;
                    _userRepository.Update(user);
                    result.Message = "Usuario desactivado correctamente";
                }
                await _unitOfWork.SaveChangesAsync();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: {(ex.InnerException != null ? ex.InnerException.Message : ex.Message) }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los usuarios registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                var data = await _userRepository.GetAllAsync();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = data.Select(u => u.New()).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        
        /// <summary>
        /// Obtener todos los usuarios por paginación y filtrado
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.User>);
                var data     = await _userRepository.FindAllAsync( x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data   = data.Where(x => x.FullName.ToLower().Contains(filter) || x.Email.ToLower().Contains(filter)).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.FullName)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data    = pageData.Select(u => u.New()).ToList();               
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los usuarios por paginación y filtrado y verificarlos con el servicio siide
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAllwithSIIDE(int currentPage, int sizePage, int systemId)
        {
            var result = new ResponsePagination();
            try
            {
                var pageData = default(List<Entities.User>);
                var data = await _userRepository.FindAllAsync(x => x.SystemId == systemId);
                var resultado = default(List<Entities.User>);

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.FullName)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.New()).ToList();

                foreach (var d in result.Data)
                {
                    var aux = await this.GetBooksFromSIDE(d.Email);
                    if (aux!=null && aux.Count>0)
                       {
                        d.Status = true;
                    }
                    resultado.Add(d);

                }

                result.Data = resultado;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
 
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message} ";
               
            }
            return result;
        }
        /// <summary>
        /// Obtener los datos de un usuario por ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(x => x.Id.Equals(id));

                if (user == null)
                    throw new Exception("El usuario no existe");

                result.Data = user;
                result.Success = true;
                result.Message = "Usuario valido";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        /// <summary>
        /// Recobrar la contraseña del usuario
        /// </summary>
        /// <param name="email"></param>
        /// <param name="bodyHTML"></param>
        /// <returns></returns>
        public Task<Response> RecoveryPassword(string email, string bodyHTML)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Actualizar los datos de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateUser(UserUpdateRequest request)
        {
            var result = new Response();
            string container = "users";
            string fileUrl = string.Empty;

            try
            {
                var user = _userRepository.GetById(request.Id);

                if (_userRepository.Get(x => x.Email.Equals(request.Email) && x.Id != request.Id && x.SystemId == request.SystemId) != null)
                    throw new Exception($"Ya existe un usuario con el correo: { request.Email }");

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileContentBase64 != null && request.FileContentBase64.Length > 0)
                {
                    fileUrl = await this._fileUploader.FileUpload(request.FileContentBase64, request.FileName, container);
                    user.UrlImage = fileUrl;
                }

                user.Birthday    = request.Birthday;
                user.Email       = request.Email;
                user.FullName    = request.FullName;
                user.Gender      = request.Gender;
                user.Institution = request.Institution;
                user.UpdateDate  = DateTime.Now;
                user.Key         = request.Key;

                if (request.StateId > 0)                
                   user.StateId = request.StateId;

                if (request.RoleId > 0)
                    user.RoleId = request.RoleId;

                _userRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Valida que se encuentre registrado el usuario
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Password de usuario</param>
        /// <returns></returns>
        public async Task<Response> ValidateUser(string username, string password, int systemId)
        {
            var result = new Response();

            try
            {
                password = this._iencrypter.EncryptMessage(password);
                var user = await _userRepository.FindAsync(x => x.Email.Equals(username) && x.Password.Equals(password) && x.SystemId == systemId);

                if (user == null)
                    throw new Exception("Usuario y/o contraseña no son válidos");

                if (!user.Active)
                    throw new Exception("El usuario esta temporalmente suspendido, favor de contactar al administrador");

                if (user.RoleId != (int)EnumRol.Administrator)
                {
                    if (systemId == (int)EnumSystems.RA)
                    {
                        await this.AddSeasonsFromSIDE(user.Id);

                        if (user.UserCCTs != null && user.UserCCTs.Count > 0)
                        {
                            string sroles = "";
                            var ccts = string.Join(",", user.UserCCTs.Select(x => x.CCT).ToList());
                            //var roles = string.Join(",", user.UserCCTs.Select(x => x.Role.Name).ToList());
                            foreach (Entities.UserCCT cct in user.UserCCTs)
                            {
                                //sroles= cct.Role.Name. + ",";
                                var usercctseason =  _userCCTRepository.GetMany(x => x.CCT == cct.CCT);
                                //var rolecct = _roleRepository.GetMany(x => x.Id == cct.RolId);
                                var rolecct = await _roleRepository.FindAsync(x => x.Id == cct.RolId);

                                
                                if (rolecct == null)
                                {
                                    throw new Exception("No se encontraron los roles del CCT");
                                }
                                else 
                                {
                                    sroles += rolecct.Name;
                                    //sroles = string.Join(",", usercctseason.Select(x => x.Role.Name).ToList());
                                }

                                //if (gis != null)
                                //{

                                //    Entities.DTO.InvitedVideoConferenceDTO gs = new Entities.DTO.InvitedVideoConferenceDTO();
                                //    gs.Name = gis.Name;
                                //    gs.Email = gis.Email;
                                //    emails += gis.Email + ",";
                                //    //emailsinvited+= gis.Email + ",";
                                //    //Se agrega al lista para el envio de correo electronico 
                                //    strList.Add(gis);
                                //}
                            }


                            //var roles = string.Join(",", user.UserCCTs.Select(x => x.Role.Name).ToList());
                            sroles = sroles.TrimEnd(',');
                            _userAccessRepository.Add(new UserAccess
                            {
                                CCTs = ccts,
                                Roles = sroles,
                                UserId = user.Id,
                                AccessDate = DateTime.UtcNow.AddHours(offset.Hours)
                            });

                            await _unitOfWork.SaveChangesAsync();

                            result.Data = user;
                            result.Success = true;
                            result.Message = "Usuario valido";

                        }
                        else
                        {

                            if (bool.Parse(ConfigurationManager.AppSettings["API.SIDE.VALIDATE"]))
                            {
                                result.Message = "Usuario no encontrado en SIDE, favor de contactar al administrador";
                                result.Data = null;
                                result.Success = false;

                            }
                            else
                            {

                                result.Data = user;
                                result.Success = true;
                                result.Message = "Usuario valido";
                            }
                        }

                    }
                    else
                    {
                        await this.AddPBookFromSIDE(user.Id);
                        result.Data = user;
                        result.Success = true;
                        result.Message = "Usuario valido";
                    }
                }
                else {
                    result.Data    = user;
                    result.Success = true;
                    result.Message = "Usuario valido";
                }

                        

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }
            
            return result;
        }

        /// <summary>
        /// Asigna una nueva contraseña al usuario y la envía por correo
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public async Task<Response> ResetPassword(UserSavePasswordRequest Request)
        {
            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(x => x.Email == Request.Email && x.SystemId == Request.SystemId);

                if (user == null)
                    throw new Exception("El usuario no existe");
                
                string NewPassword = this._iencrypter.GeneratedPassword();
                user.Password      = this._iencrypter.EncryptMessage(NewPassword);

                this._userRepository.Update(user);
                this._unitOfWork.SaveChanges();

                await this._imailer.SendPwdResetMail(new Entities.Utils.PwdResetMessage(user.Email, "servidor@sali.com", "Recuperar contraseña", "recoverpasswordtemplate.html", user.FullName, NewPassword, user.SystemId));

                result.Success = true;
                result.Message = "Una nueva contraseña ha sido enviada al correo del usuario.";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        /// <summary>
        /// GetNodes: Get user's associated nodes
        /// </summary>
        /// <param name="id">User's Id</param>
        /// <returns>A list of node entities</returns>
        public async Task<Response> GetNodes(int id)
        {
            var result = new Response();

            try
            {
                var user = _userRepository.GetById(id);
                if (user != null)
                {
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";
                    result.Data = user.Nodes.Select(n => new
                    {
                        Id = n.Id,
                        Name = n.Name,
                        Description = n.Description,
                        NodeTypeId = n.NodeTypeId,
                        UrlImage = n.UrlImage,
                        NodeType = n.NodeType.Description
                    }).ToList();
                } else
                {
                    result.Success = false;
                    result.Message = "No se encontró referencia al usuario solicitado";
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// AddNodes: Rewrite user's associated node list
        /// </summary>
        /// <param name="request">User's Id and list of nodes</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> AddNodes(int id, List<int> nodes)
        {
            var result = new Response();

            try
            {

                var user = _userRepository.GetById(id);
                user.Nodes.Clear();

                var lstNodes = _nodeRepository.GetAll().Where(x => nodes.Contains(x.Id));

                foreach (var p in lstNodes)
                    user.Nodes.Add(p);

                _userRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Nodos agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// CopyProfile: Duplicate node set from a publishingprofile to any user & associate userid with userprofileid
        /// </summary>
        /// <param name="profileId">Profile's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> CopyProfile(int profileId, int[] userIds)
        {
            var result = new Response();

            try
            {
                foreach (int userId in userIds)
                {
                    // Delete previously added user


                    var user = _userRepository.GetById(userId);
                    if (user != null)
                    {
                        var profile = _publishingProfileRepository.GetById(profileId);
                        if (profile != null && profile.Nodes.Count() > 0)
                        {
                            user.Nodes.Clear();
                            foreach (var p in profile.Nodes)
                                user.Nodes.Add(p);

                            _userRepository.Update(user);
                            _unitOfWork.SaveChanges();

                            result.Success = true;
                            result.Message = userIds.Count() == 1 ? "Se asignó el perfil correctamente" : "Se asignaron los perfiles correctamente";
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "No se encontró referencia al perfil solicitado";
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No se encontró referencia al usuario solicitado";
                    }
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener usuario por correo electronico
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns></returns>
        public async Task<Response> GetByMail(string eMail)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(eMail) && IsValidEmail(eMail))
                try
                {
                    var user = await _userRepository.FindAsync(x => x.Email.Equals(eMail));

                    if (user == null) throw new Exception("El usuario no existe");

                    result.Data = //new Project.Entities.User
                        new {
                            UrlImage = user.UrlImage,
                            FullName = user.FullName,
                            Email = user.Email,
                            Gender = user.Gender,
                            RoleId = user.RoleId,
                            RegisterDate = user.RegisterDate,
                            RoleDescription = user.Role.Description
                        };
                    result.Success = true;
                    result.Message = "Usuario valido";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "El correo no es válido";
            }


            return result;
        }

        /// <summary>
        /// Validar correo electrónico
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns></returns>
        private bool IsValidEmail(string eMail)
        {
            return true;
        }

        /// <summary>
        /// Obtener datos de usuario por contenido en el correo
        /// </summary>
        /// <param name="partialMail"></param>
        /// <returns></returns>
        public async Task<Response> GetByPartialMail(string partialMail)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var users = _userRepository.
                        GetMany(x => x.Email.Contains(partialMail)).
                        Select(s => new
                        {
                            Id = s.Id,
                            UrlImage = s.UrlImage,
                            FullName = s.FullName,
                            Email = s.Email,
                            Gender = s.Gender,
                            RoleId = s.RoleId,
                            RegisterDate = s.RegisterDate,
                            RoleDescription = s.Role.Description
                        }

                        ).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }

        /// <summary>
        /// Obtener datos de usuario por contenido en el correo y rol
        /// </summary>
        /// <param name="partialMail"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<Response> GetByPartialMailRole(string partialMail, int roleId)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var users = _userRepository.
                        GetMany(x => x.Email.Contains(partialMail) && x.RoleId == roleId).
                        Select(s => new
                        {
                            Id = s.Id,
                            UrlImage = s.UrlImage,
                            FullName = s.FullName,
                            Email = s.Email,
                            Gender = s.Gender,
                            RoleId = s.RoleId,
                            RegisterDate = s.RegisterDate,
                            RoleDescription = s.Role.Description
                        }

                        ).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }

        /// <summary>
        /// Obtener datos de usuario por contenido en el correo, rol y identificador del curso
        /// </summary>
        /// <param name="partialMail"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId, int systemId)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var groups   = (await _studentGroupRepository.FindAllAsync(sg => sg.CourseId.Equals(courseId))).Select(i => i.Id).ToList();
                    var students = (await _studentRepository.FindAllAsync(s => groups.Contains(s.StudentGroupId))).Select(s => s.UserId).ToList();
                    var users    = (await _userRepository.FindAllAsync(x => x.SystemId == systemId
                                                                         && x.Email.Contains(partialMail) 
                                                                         && x.RoleId == roleId && !students.Contains(x.Id)))                        
                                        .Select(s => new
                                        {
                                            Id = s.Id,
                                            UrlImage = s.UrlImage,
                                            FullName = s.FullName,
                                            Email = s.Email,
                                            Gender = s.Gender,
                                            RoleId = s.RoleId,
                                            RegisterDate = s.RegisterDate,
                                            RoleDescription = s.Role.Description
                                        }).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }

        /// <summary>
        /// LinkProfile: Associate userid with userprofileid
        /// </summary>
        /// <param name="profileId">Profile's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> LinkProfile(int profileId, int[] userIds)
        {
            var result = new Response();

            try
            {
                // Delete previouly created records for this profile
                _publishingProfileUserRepository.Delete(pu => pu.PublishingProfileId == profileId);
                await _unitOfWork.SaveChangesAsync();

                foreach (int userId in userIds)
                {
                    // Delete previously added user
                    var user = _userRepository.GetById(userId);
                    if (user != null)
                    {
                        var profile = _publishingProfileRepository.GetById(profileId);
                        if (profile != null)
                        {
                            // Delete previouly created records for this user
                            _publishingProfileUserRepository.Delete(pu => pu.UserId == user.Id);
                            _unitOfWork.SaveChanges();

                            Entities.PublishingProfileUser ppu = new Entities.PublishingProfileUser();

                            ppu.PublishingProfileId = profileId;
                            ppu.UserId = user.Id;
                            ppu.RegisterDate = DateTime.UtcNow;

                            _publishingProfileUserRepository.Add(ppu);
                            await _unitOfWork.SaveChangesAsync();

                            result.Success = true;
                            result.Message = userIds.Count() == 1 ? "Se asoció el usuario correctamente" : "Se asociaron los usuarios correctamente";
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "No se encontró referencia al perfil solicitado";
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No se encontró referencia al usuario solicitado";
                    }
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Guardar el refrestoken en la base de datos
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="refrhesToken"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<bool> SaveRefreshToken(int userId, string refreshToken, string client, string protectedTicket) {

            try
            {
                var user = await _userRepository.FindAsync(x => x.Id == userId);

                if (user == null)
                    return false;

                user.RefreshToken = refreshToken;
                user.ClientId = client;
                user.ProtectedTicket = protectedTicket;

                _userRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Obtener usuario por refreshtoken
        /// </summary>
        /// <param name="refreshToken">Refreshtoken </param>        
        /// <returns></returns>
        public async Task<Entities.User> GetByRegreshToken(string refreshToken)
        {
            var user = new Entities.User();

            try
            {
                user = await _userRepository.FindAsync(x => x.RefreshToken == refreshToken);

                if (user == null)
                    throw new Exception("Usuario y/o contraseña no son validos");

                if (!user.Active)
                    throw new Exception("El usuario esta temporalmente suspendido, favor de contactar al administrador");

                return user;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get users profile data information
        /// </summary>
        /// <param name="UserId">User Id</param>
        /// <returns>Success value, message and user profile data information</returns>
        public async Task<Response> GetUserProfileData(int userId)
        {
            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(u => u.Id == userId);
                
                if (user == null)
                    throw new Exception("Los datos del usuario no fueron encontrados");

                result.Data = new Entities.User
                {
                    Id          = user.Id,
                    Active      = user.Active,
                    Birthday    = user.Birthday,
                    Email       = user.Email,
                    FullName    = user.FullName,
                    Gender      = user.Gender,
                    Institution = user.Institution,
                    Semester    = user.Semester,
                    LevelId     = user.LevelId,
                    SubsystemId = user.SubsystemId,
                    Key         = user.Key,
                    Password    = user.Password,
                    Role        = new Project.Entities.Role { Id = user.Role.Id, Name = user.Role.Name },
                    State    = new State {
                        Id   = user.State.Id,
                        Name = user.State.Name,
                        City = new Entities.City {
                             Id          = user.State.City.Id,
                             Description = user.State.City.Description
                         }
                    },
                    UserCCTs = user.UserCCTs?.Select( x=> new UserCCT {
                        Id        = x.Id,
                        CCT       = x.CCT,
                        EndDate   = x.EndDate,
                        NodeId    = x.NodeId,
                        RolId     = x.RolId,
                        Season    = x.Season,
                        StartDate = x.StartDate,
                        SubSeason = x.SubSeason                        
                    }).ToList(),
                    UrlImage = user.UrlImage
                };
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        /// <summary>
        /// Change user password by user validating old and new passwords
        /// </summary>
        /// <param name="Request"></param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> ChangePassword(UserChangePasswordRequest Request)
        {
            var result = new Response();

            try
            {
                var password = this._iencrypter.EncryptMessage(Request.OldPassword);
                var user = await _userRepository.FindAsync(x => x.Email.Equals(Request.Email) && x.Password.Equals(password));

                if (user == null)
                    throw new Exception("La contraseña actual no es valida");

                string newPassword = this._iencrypter.EncryptMessage(Request.NewPassword);
                user.Password = newPassword;

                _userRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();

                // await _imailer.SendPwdChangeMail(new Entities.Utils.PwdChangeMessage(user.Email, "servidor@sali.com", "Cambiar contraseña", "mailtemplate.html", user.FullName));

                result.Success = true;
                result.Message = "Una nueva contraseña ha sido creada por el usuario.";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Change user profile image by user 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns>new user image profile, success value, message and 200 code</returns>
        public async Task<Response> UploadImgProfile(UserUploadImgProfileRequest request)
        {
            var result = new Response();
            string container = "users";
            string fileUrl = string.Empty;
            try
            {
                var user = _userRepository.GetById(request.Id);
                if (user == null)
                    throw new Exception("Usuario no válido");

                if (!user.Active)
                    throw new Exception("El usuario esta temporalmente suspendido, favor de contactar al administrador");

                if (request.FileContentBase64 != null && request.FileContentBase64.Length > 0)
                {
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                    user.UrlImage = fileUrl;
                }

                _userRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Data = user.UrlImage;
                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Cerrar sesión del usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> LogOut(int userId)
        {

            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(x => x.Id == userId);

                if (user == null)
                    throw new Exception("Datos de usuario no encontrado");

                user.ProtectedTicket = null;
                user.RefreshToken = null;

                _userRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al cerrar la sesión { ex.Message}";
            }

            return result;

        }


        public async Task<Response> GetRolesByUserEmail(string email) {

            var result = new Response();

            try
            {
                var users = await _userRepository.FindAllAsync(x => x.Email.ToLower().Equals(email));

                result.Data = users.Select(u => new
                {
                    rol        = u.Role.Name,
                    systemName = u.System.Name,
                    systemId   = u.SystemId,
                    rolId      = u.RoleId
                });

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Validar usuario en SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Response> ValidateUserSIDE(string email) {

            var response = new Response();

            try
            {
                var data = await this.GetSeasonsFromSIDE(email);
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";
                response.Data = data;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos desde SIDE: { ex.Message}";               
            }

            return response;
        }

        /// <summary>
        /// Validar usuario en SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Response> ValidateCCTUserSIDE(string email)
        {

            var response = new Response();

            try
            {
                var data = await this.GetCCTFromSIDE(email);
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";
                response.Data = data;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos desde SIDE: { ex.Message}";
            }

            return response;
        }


        #endregion

        #region NOTIFICATIONS

        /// <summary>
        /// Registrar un dispositivo al usuario
        /// </summary>
        /// <returns></returns>
        public async Task<Response> RegisterDevice(UserDeviceSaveRequest request, int userId)
        {

            var result = new Response();
            var device = default(UserDevice);

            try
            {
                if (string.IsNullOrWhiteSpace(request.DeviceId) && request.ClientId != 1)
                {
                    device = new UserDevice
                    {
                        ClientId = request.ClientId,
                        TokenDevice = request.Token,
                        DeviceId = Guid.NewGuid(),
                        UserId = userId,
                        RegisterDate = DateTime.Now
                    };
                    _userDeviceRepository.Add(device);
                }
                else
                {
                    if (request.ClientId == 1)
                        device = await _userDeviceRepository.FindAsync(x => x.UserId == userId && x.ClientId == request.ClientId);
                    else
                    {
                        var guidId = new Guid(request.DeviceId);
                        device = await _userDeviceRepository.FindAsync(x => x.DeviceId == guidId);
                    }

                    if (device != null)
                    {
                        device.TokenDevice = request.Token;
                        _userDeviceRepository.Update(device);
                    }
                    else
                    {
                        device = new UserDevice
                        {
                            ClientId = request.ClientId,
                            TokenDevice = request.Token,
                            DeviceId = Guid.NewGuid(),
                            UserId = userId,
                            RegisterDate = DateTime.Now
                        };
                        _userDeviceRepository.Add(device);
                    }
                }

                await _unitOfWork.SaveChangesAsync();

                //TODO Registrar equipo
                var topic = request.SystemId == (int)EnumSystems.SALI ? ConfigurationManager.AppSettings["SALI.FIREBASE.GENERAL_TOPIC"] :
                                                                        ConfigurationManager.AppSettings["RA.FIREBASE.GENERAL_TOPIC"];

                await _srvPushNotification.RegisterTopic(request.Token, topic);

                result.Data = device.DeviceId;
                result.Success = true;
                result.Message = "Dispositivo registrado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al registrar el dispositivo { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las notificaciones no leidas
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUnReadNotifications(int userId) {

            var result = new Response();

            try
            {
                var data = await _userNotificationRepository.FindAllAsync(n => n.UserId == userId && n.Read == false);
                var notifications = data.Select(n => n.New())
                                        .OrderByDescending(n => n.RegisterDate)
                                        .ToList();

                result.Data = notifications;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener las notificaciones { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las notificaciones
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllNotifications(int userId) {

            var result = new Response();

            try
            {

                var data = await _userNotificationRepository.FindAllAsync(n => n.UserId == userId);
                var notifications = data.OrderBy(n => n.RegisterDate)
                                        .Select(n => n.New())                                        
                                        .ToList();

                result.Data = notifications;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener las notificaciones { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las notificaciones con paginación
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAllNotifications(int userId, int currentPage, int sizePage) {

            var result = new ResponsePagination();

            try
            {
                var data = await _userNotificationRepository.FindAllAsync(n => n.UserId == userId && n.Read == false);
                result.Pagination.SetData(data.Count , currentPage, sizePage);

                var pageData  = data.OrderByDescending( x=> x.RegisterDate)
                                    .Skip((currentPage - 1) * result.Pagination.PageSize)
                                    .Take(result.Pagination.PageSize)
                                    .ToList();

                result.Data    = pageData.Select(n => n.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Message = $"Problemas al obtener las notificaciones: {ex.Message}";
                result.Success = false;
            }


            return result;
        }

        /// <summary>
        /// Actualizar el estatus de leido de la notificación.
        /// </summary>
        /// <param name="notificationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> CheckReadNotification(int notificationId, int userId) {

            var result = new Response();

            try
            {
                var notification = await _userNotificationRepository.FindAsync(x => x.Id == notificationId && x.UserId == userId);

                if (notification == null)
                    throw new Exception("Datos no encontrados");

                notification.Read     = true;
                notification.ReadDate = DateTime.Now;
                _userNotificationRepository.Update(notification);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos actualizados correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al marcar como leída la notificacion: { ex.Message}";
            }


            return result;
        }

        #endregion
        
        #region PRIVATE METHODS

        private async Task<bool> AddPBookFromSIDE( int userId ) {

            try
            {
                var user     = await _userRepository.FindAsync(x => x.Id == userId);
                var books    = await this.GetBooksFromSIDE(user.Email);
                var listISBN = books.Select(a => a.ISBN).ToList();
                var nodes    = (await _nodeRepository.FindAllAsync(x => x.Book != null && listISBN.Contains(x.Book.ISBN))).Select(x => x).ToList();

                var publisingBook = user.Nodes.Select(x => x.Id).ToList();
                var listAddNodes  = nodes.Where(x => !publisingBook.Contains(x.Id)).ToList();

                if (listAddNodes.Count > 0)
                {
                    foreach (var n in listAddNodes)
                    {
                        if (!user.Nodes.Contains(n))
                            user.Nodes.Add(n);
                    }                                         

                    _userRepository.Update(user);
                    await _unitOfWork.SaveChangesAsync();
                }
               
                else // Se  agrega  la  opción  guardar  perfil  para  cuando  no  encuentre  libros
                {
                    var publishingUser = await _publishingProfileUserRepository.FindAsync(x => x.UserId == userId);
                    var listprofileuser = _publishingProfileUserRepository.FindAsync(r => r.UserId.Equals(user.Id)); //_publishingProfileUserRepository.FindAsync(x => x.UserId == user.Id);
                    int idprofile = Int32.Parse(ConfigurationManager.AppSettings["SALI.APP.PROFILE"]);
                    int idperfil = 0;
                    if (publishingUser!=null)
                    {
                        idperfil = publishingUser.Id;

                    }                

                    if (idperfil!=0)
                    {
                        Entities.PublishingProfileUser profile = _publishingProfileUserRepository.GetById(idperfil);

                        profile.PublishingProfileId = idprofile;
                     
                        profile.RegisterDate = DateTime.UtcNow;

                        _publishingProfileUserRepository.Update(profile);
                        await this._unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                       
                        Entities.PublishingProfileUser ppu = new Entities.PublishingProfileUser();
                        ppu.PublishingProfileId = idprofile;
                        ppu.UserId = user.Id;
                        ppu.RegisterDate = DateTime.UtcNow;
                        _publishingProfileUserRepository.Add(ppu);
                        await _unitOfWork.SaveChangesAsync();

                    }

                   
                }

                return true;


            }
            catch (Exception)
            {
                return false;
            }           
            
        }

        /// <summary>
        /// Obtener los nodos del contenido publicp
        /// </summary>
        /// <returns></returns>
        private async Task<List<Entities.Node>> GetPublicContent( List<Entities.Node> nodesAccess, int systemId, List<Entities.Node> nodes) {

            var lstContent = new List<Entities.Node>();

            try
            {            

                var content      = await _nodeRepository.FindAllAsync(x => x.Public == true && x.SystemId == systemId );
                var nodesBook    = new List<Entities.Node>();


                content.ToList().ForEach(n =>
                {
                    if (n.NodeTypeId == (int)EnumNodeType.Book)
                        nodesBook.Add(n);
                    else
                    {
                        var parent = nodes.FirstOrDefault(x => x.Id == n.ParentId);
                        if (parent != null) {
                            if (parent.NodeTypeId == (int)EnumNodeType.Book)
                                nodesBook.Add(parent);
                        }
                    }
                        

                });

                var nodesContent = nodesBook.Where(x => x.NodeTypeId == (int)EnumNodeType.Book)
                                          .Select(x => new Entities.Node {
                                              Id                = x.Id,
                                              Description       = x.Description,
                                              Name              = x.Name,
                                              NodeTypeId        = x.NodeTypeId,
                                              UrlImage          = x.UrlImage,
                                              ParentId          = x.ParentId,
                                              NodeAccessTypeId  = (int)EnumNodeAccessType.Public,
                                              Active            = x.Active,
                                              NodeType          = new Entities.NodeType
                                              {
                                                  Id          = x.NodeType.Id,
                                                  Description = x.NodeType.Description
                                              },
                                              Book = new Entities.Book
                                              {
                                                  Id      = x.BookId.HasValue ? x.Book.Id : 0,
                                                  Name    = x.Book?.Name,
                                                  ISBN    = x.Book?.ISBN,
                                                  Authors = x.Book != null ? x.Book.Authors.Select(n => new Entities.Author
                                                  {
                                                      FullName = n.FullName,
                                                      LastName = n.LastName,
                                                      Comment  = n.Comment,
                                                  }).ToList() : new List<Entities.Author>()
                                              }
                                          }).ToList();

                return nodesContent;
                
            }
            catch (Exception)
            {
              lstContent = new List<Entities.Node>();
            }

            return lstContent;

        }

        /// <summary>
        /// Obtener todas las publicaciones que el usuario tiene acceso en el SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private async Task<List<Adopcion>> GetBooksFromSIDE(string email)
        {

            try
            {
                var result = await this._request.Get<List<Adopcion>>(ConfigurationManager.AppSettings["Api-method-side"],
                                                                                          ConfigurationManager.AppSettings["Api-endpoint"],
                                                                                          "?email="+email);                //alfonso.ramirez@imt.maristas.edu.mx
                return result;
            }
            catch (Exception)
            {

                return null;
            }
        }

        /// <summary>
        /// Obtener todas las temporadas que el usuario tiene acceso en el SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private async Task<List<Season>> GetSeasonsFromSIDE(string email)
        {
            try
            {
                var result = await this._request.Get<List<Season>>(ConfigurationManager.AppSettings["Api-method-side-temporada"],
                                                                                          ConfigurationManager.AppSettings["Api-endpoint"],
                                                                                          "?email=" + email);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Agregar perfiles de publicación al usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<bool> AddSeasonsFromSIDE(int userId)
        {

            try
            {
                var user     = await _userRepository.FindAsync(x => x.Id == userId);
                //var seasons  = await this.GetSeasonsFromSIDE(user.Email);
                var seasons = await this.GetCCTFromSIDE(user.Email);
                var profiles = await _publishingProfileRepository.FindAllAsync(x => x.SystemId == (int)EnumSystems.RA);
                               
                //Si el sistema RA tiene perfiles
                if (seasons.Count > 0)
                {                   
                    var userPublishing = user.PublishingProfileUsers.Select(x => x.PublishingProfile).ToList();
                    foreach (var p in userPublishing) {

                        var seasson = seasons.FirstOrDefault(x => x.Level.ToLower() == p.Description.ToLower());

                        if (seasson == null) {
                            var publishingUser = await _publishingProfileUserRepository.FindAsync(x => x.UserId == userId && x.PublishingProfileId == p.Id);

                            if (publishingUser != null) {
                                _publishingProfileUserRepository.Delete(publishingUser);
                            }
                        }
                    }
                    
                                       
                    //Comparar cada perfil de sistema con los niveles que regreso el servicio de SIIDE
                    foreach (var season in seasons.Distinct())
                    {
                        var profile = profiles.FirstOrDefault(x => x.Description.ToLower() == season.Level.ToLower());

                        //Si el nivel es igual a la descripcion que tiene los perfiles de sistema 
                        if (profile != null && !user.PublishingProfileUsers.Any( x=> x.PublishingProfileId == profile.Id))
                        {
                            //Registro el perfil en el usuario y salto el ciclo
                            _publishingProfileUserRepository.Add(new Entities.PublishingProfileUser
                            {
                                PublishingProfileId = profile.Id,
                                UserId              = user.Id,
                                RegisterDate        = DateTime.Now
                            });                           
                        }
                    }
                   
                    await _unitOfWork.SaveChangesAsync();
                }

                await this.AddCctFromSIDE(user, seasons);

                return true;


            }
            catch (Exception ex)
            {
                return false;
            }

        }
        
        /// <summary>
        /// Asignar CCTS a un usuario
        /// </summary>
        /// <param name="user"></param>
        /// <param name="seassons"></param>
        /// <returns></returns>
        private async  Task<bool> AddCctFromSIDE(Entities.User user, List<CCT> seasons)
        {

            try
            {
                var removeCCT = new List<UserCCT>();

             
                foreach (var ucct in user.UserCCTs.ToList())
                {
                   
                    var seasson = seasons.FirstOrDefault(x => x.Cct == ucct.CCT);
                    
                    if (seasson == null)
                    {
                        removeCCT.Add(ucct);
                    }
                    else if (!string.IsNullOrEmpty(seasson.clasificationschool ) || !string.IsNullOrEmpty(seasson.EntityName) || !string.IsNullOrEmpty(seasson.ZoneName) || !string.IsNullOrEmpty(seasson.ControlName) || !string.IsNullOrEmpty(seasson.PromotorName))
                    {
                        // var usercctseason = await _userCCTRepository.FindAsync(x => x.CCT == ucct.CCT);
                        var usercctseason = await _userCCTRepository.FindAsync(x => x.CCT == ucct.CCT && x.UserId == user.Id);
                        if (usercctseason == null)
                        { throw new Exception("No se encontraron los datos del CCT");
                        }
                        else if (string.IsNullOrEmpty(usercctseason.ClasificationSchool) || 
                            string.IsNullOrEmpty(usercctseason.State) || 
                            string.IsNullOrEmpty(usercctseason.Zone) || 
                            string.IsNullOrEmpty(usercctseason.Control) 
                            || string.IsNullOrEmpty(usercctseason.Promoter))
                        {
                            removeCCT.Add(ucct);
                        }

                                              
                    }
                }

                if (removeCCT.Count > 0)
                    removeCCT.ForEach(r => _userCCTRepository.Delete(r));

                seasons.Distinct().ToList().ForEach(s =>
                {
                    if (!user.UserCCTs.Any(x => x.CCT == s.Cct))
                    {

                        var userCCT = new UserCCT
                        {
                            CCT = s.Cct,
                            Season = s.SeasonName,
                            SubSeason = s.SubSeason,
                            StartDate = DateTime.Parse(s.StartDate),
                            EndDate = DateTime.Parse(s.EndDate),
                            UserId = user.Id,
                            RolId = (int)EnumRol.StudentRA,
                            ClasificationSchool= s.clasificationschool,
                            State=s.EntityName,
                            Zone=s.ZoneName,
                            Control=s.ControlName,
                            Promoter=s.PromotorName

                        };

                        switch (s.Level.ToLower())
                        {
                            case "preescolar":
                                if ((int)EnumRol.TeacherRA == user.RoleId)
                                    userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_PRESCOLAR"]);
                                  

                                userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.PRESCOLAR"]);
                                break;
                            case "primaria":
                                if ((int)EnumRol.TeacherRA == user.RoleId)
                                    userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_PRIMARIA"]);

                                userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.PRIMARIA"]);
                                break;
                            case "secundaria":
                                if ((int)EnumRol.TeacherRA == user.RoleId)
                                    userCCT.RolId = int.Parse(ConfigurationManager.AppSettings["RA.ROL.PROFESOR_SECUNDARIA"]);

                                userCCT.NodeId = int.Parse(ConfigurationManager.AppSettings["RA.NODE.SECUNDARIA"]);
                                break;
                        };

                        user.UserCCTs.Add(userCCT);

                    }

                });

                await _unitOfWork.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }





        /// <summary>
        /// Obtine los datos del cct por email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private async Task<List<CCT>> GetCCTFromSIDE(string email)
        {
            try
            {
                var result = await this._request.Get<List<CCT>>(ConfigurationManager.AppSettings["Api-method-side-cct"],
                                                                                          ConfigurationManager.AppSettings["Api-endpoint"],
                                                                                          "?email=" + email);


                var result2 = await this._request.Get<List<Season>>(ConfigurationManager.AppSettings["Api-method-side-temporada"],
                                                                                          ConfigurationManager.AppSettings["Api-endpoint"],
                                                                                         "?email=" + email);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region cct
        /// <summary>
        /// Obtener todos los cct
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetCCT()
        {
            var result = new Response();

            try
            {
                //var data = await _userCCTRepository.GetAllAsync();

                //var usercc/*t = new List<Entities.UserCCT>();*/
                //var data = (_userCCTRepository.GetAll()).ToList();


                var usercct = _userCCTRepository.GetAll().Select(x=> new Entities.DTO.UserCCTDTO { Id=x.Id, CCT= x.CCT}).Distinct().ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = usercct;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        #endregion
    }
}
