﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public interface IUser
    {
        Task<Response> ConfirmSuscripcion(int email);
        Task<Response> ValidateUser(string username, string password, int systemId);        
        Task<Response> RecoveryPassword(string email, string bodyHTML);
        Task<Response> ResetPassword(UserSavePasswordRequest Request);
        Task<Entities.User> GetByRegreshToken(string refreshToken);

        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
        Task<Response> CopyProfile(int profileId, int[] userIds);
                
        Task<Response> LinkProfile(int profileId, int[] userIds);
        Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId, int systemId);
        Task<bool> SaveRefreshToken(int userId, string refreshToken, string client, string protectedTicket);        
        Task<Response> UploadImgProfile(UserUploadImgProfileRequest request);
        Task<Response> ChangePassword(UserChangePasswordRequest Request);
        Task<Response> GetUserProfileData(int userId);
        Task<Response> LogOut(int userId);
        Task<Response> GetRolesByUserEmail(string email);

        Task<Response> ValidateUserSIDE(string email);
        Task<Response> ValidateCCTUserSIDE(string email);
        

        #region SEARCH

        Task<Response> GetUserProfile(int userId, int systemId, int origin);
        Task<Response> GetByMail(string eMail);
        Task<Response> GetByPartialMail(string partialMail);
        Task<Response> GetByPartialMailRole(string partialMail, int roleId);

        #endregion

        #region ADMINISTRATOR

        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId);
        Task<ResponsePagination> GetAllwithSIIDE(int currentPage, int sizePage, int systemId);
        Task<Response> GetById(int id);
        Task<Response> RegisterUser(UserSaveRequest request);
        Task<Response> UpdateUser(UserUpdateRequest request);
        Task<Response> DeleteUser(int userId);

        #endregion
        
        #region NOTIFICATIONS

        Task<Response> RegisterDevice(UserDeviceSaveRequest request, int userId);
        Task<Response> GetUnReadNotifications(int userId);
        Task<Response> GetAllNotifications(int userId);
        Task<ResponsePagination> GetAllNotifications(int userId, int currentPage, int sizePage);
        Task<Response> CheckReadNotification(int notificationId, int userId);

        #endregion

        #region cct

        Task<Response> GetCCT();
        #endregion

    }
}
