﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Permission : IPermission
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Permission(IPermissionRepository permissionRepository, IUnitOfWork unitOfWor)
        {

            this._permissionRepository = permissionRepository;
            this._unitOfWork = unitOfWor;
        }

        /// <summary>
        /// Método que realizar el registro de un nuevo elemento del menú
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPermission(PermissionSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Permission permission = new Entities.Permission();

                permission.Id           = request.Id;
                permission.Module       = request.Module;
                permission.Description  = request.Description;
                permission.Active       = request.Active;
                permission.Icon         = request.Icon;
                permission.Url          = request.Url;
                permission.ParentId     = request.ParentId;
                permission.ShowInMenu   = request.ShowInMenu;
                permission.SystemId     = request.SystemId;
                permission.Order        = 0;

                _permissionRepository.Add(permission);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = permission;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un permiso existente que no este asignado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeletePermission(int id)
        {
            var result = new Response();

            try
            {
                var permission = await _permissionRepository.FindAsync(x => x.Id == id);

                if (permission == null)
                    throw new Exception("No se encontraron los datos");

                if (permission.Roles.Count > 0)
                    throw new Exception("El permiso esta asignado a un rol, no se puede eliminar");

                _permissionRepository.Delete(permission);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al eliminar los datos: {ex.Message}";                
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los permisos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll(int systemId)
        {
            var result = new Response();

            try
            {
                var data = await  _permissionRepository.FindAllAsync( x=> x.SystemId == systemId);

                result.Data    = data.Select(p => p.New()).OrderBy(x=> x.Module).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";                
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los permisos registrados paginados y filtrado por descripción
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Permission>);
                var data = await _permissionRepository.FindAllAsync( x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Description.ToLower().Contains(filter)).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Module)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los datos de un permiso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _permissionRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para actualizar los datos del permiso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePermission(PermissionSaveRequest request)
        {
            var result = new Response();

            try
            {
                var permission = await _permissionRepository.FindAsync( x=> x.Id == request.Id);

                if (permission == null) {
                    throw new Exception(" Datos de permiso no encontrado ");
                }

                permission.Id           = request.Id;
                permission.Module       = request.Module;
                permission.Description  = request.Description;
                permission.Active       = request.Active;
                permission.Icon         = request.Icon;
                permission.Url          = request.Url;               
                permission.ShowInMenu   = request.ShowInMenu;
                // permission.Order        = 0;

                if (request.ParentId > 0)
                    permission.ParentId = request.ParentId;
                else
                    permission.ParentId = null;

                _permissionRepository.Update(permission);
                await this._unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos guardados correctamente";
                result.Data =  new Entities.Permission {
                   Id          = permission.Id,
                   Module      = permission.Module,
                   Description = permission.Description,
                   Active      = permission.Active,
                   Icon        = permission.Icon,
                   Url         = permission.Url,
                   ParentId    = permission.ParentId,
                   ShowInMenu  = permission.ShowInMenu,
                   SystemId    = permission.SystemId
                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        
    }
}