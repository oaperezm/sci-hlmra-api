﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Role : IRole
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IPermissionRepository _permissionRepository;       
        private readonly IUnitOfWork _unitOfWork;


        public Role(IRoleRepository roleRepository, 
                    IPermissionRepository permissionRepository,
                    INodeRepository nodeRepository,
                    IUnitOfWork unitOfWor)
        {

            this._roleRepository       = roleRepository;
            this._permissionRepository = permissionRepository;
          
            this._unitOfWork           = unitOfWor;
        }

        /// <summary>
        /// Agregar un nuevo rol
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddRole(RoleSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Role role = new Entities.Role
                {
                    Id          = request.Id,
                    Name        = request.Name,
                    Description = request.Description,
                    Active      = request.Active,
                    SystemId    = request.SystemId                    
                };

                _roleRepository.Add(role);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = role;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un rol existente
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteRole(int id)
        {
            var result = new Response();

            try
            {
                var role = await _roleRepository.FindAsync(x => x.Id == id);

                if (role.Permissions.Count > 0)
                    throw new Exception("No se puede eliminar el rol, tiene permisos asignados");

                _roleRepository.Delete(role);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el role: ${ ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Obtener todos los roles registrados sin paginación
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll( int systemId)
        {
            var result = new Response();

            try
            {
                var data = await _roleRepository.FindAllAsync( x=> x.SystemId == systemId);

                result.Data    = data.OrderBy( u=> u.Name)
                                     .Select(u => u.New())
                                     .ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los roles registrados por paginación y filtrado
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Role>);
                var data = await _roleRepository.FindAllAsync( x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Name.ToLower().Contains(filter) || x.Description.ToLower().Contains(filter)).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener un rol por idetificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _roleRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un role
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateRole(RoleSaveRequest request)
        {
            var result = new Response();

            try
            {
                var role = await _roleRepository.FindAsync(x => x.Id == request.Id);

                if (role == null)
                    throw new Exception("No se encontraron los datos del rol");

                role.Name        = request.Name;
                role.Description = request.Description;
                role.Active      = request.Active;

                _roleRepository.Update(role);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = role.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
        /// <summary>
        /// Obtener los roles de un permiso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetRolePermissions(int id, int systemId)
        {
            var result = new Response();

            try
            {
                var rol   = await _roleRepository.FindAsync( x=> x.Id ==  id);                         
                var perms = (await _permissionRepository.FindAllAsync( x=> x.SystemId == systemId))
                           .OrderBy( u => u.Module)
                           .Select(u => new PermissionDTO
                                {
                                    Id          = u.Id,
                                    Module      = u.Module,
                                    Description = u.Description,
                                    Active      = u.Active,
                                    Assigned    = false,
                                    ParentId    = u.ParentId.HasValue ? u.ParentId : 0
                                })
                            .ToList();

                if (rol.Permissions != null && rol.Permissions.Count() > 0)
                {
                    foreach (var p in perms)
                    {
                        var rp = rol.Permissions.Where(rperm => rperm.Id == p.Id).FirstOrDefault();
                        if (rp != null)
                        {
                            p.Assigned = true;

                        }
                    }
                }

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = perms;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los permisos de un rol
        /// </summary>
        /// <param name="id"></param>
        /// <param name="permissions"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePermissions(int id, List<int> permissions)
        {
            var result = new Response();

            try
            {

                var rol = await _roleRepository.FindAsync( x=> x.Id == id);

                if (rol == null)
                    throw new Exception("Datos no encontrados");

                rol.Permissions.Clear();
                var lstPermissions = await _permissionRepository.FindAllAsync(x => permissions.Contains(x.Id));

                foreach(var p in lstPermissions)
                    rol.Permissions.Add(p);
                
                _roleRepository.Update(rol);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Permisos asignados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar permisos al rol: { ex.Message }";
            }

            return result;
        }

        
        
    }
}