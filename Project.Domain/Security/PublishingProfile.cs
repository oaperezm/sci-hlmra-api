﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class PublishingProfile : IPublishingProfile
    {
        private readonly IPublishingProfileRepository _PublishingProfileRepository;
        private readonly IPublishingProfileUserRepository _PublishingProfileUserRepository;
        private readonly IUserRepository _UserRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PublishingProfile(IPublishingProfileRepository PublishingProfileRepository, 
            INodeRepository nodeRepository, 
            IPublishingProfileUserRepository PublishingProfileUserRepository,
            IUserRepository UserRepository,
            IUnitOfWork unitOfWor)
        {

            this._PublishingProfileRepository      = PublishingProfileRepository;
            this._PublishingProfileUserRepository  = PublishingProfileUserRepository;
            this._UserRepository                   = UserRepository;
            this._nodeRepository                   = nodeRepository;
            this._unitOfWork                       = unitOfWor;
            
        }

        /// <summary>
        /// Agregar un nuevo perfil de publicación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfile PublishingProfile = new Entities.PublishingProfile();

                PublishingProfile.Description = request.Description;
                PublishingProfile.Active      = request.Active;
                PublishingProfile.SystemId    = request.SystemId;

                // Save nodes
                if (request.Nodes.Count() > 0)
                {
                    var lstNodes = _nodeRepository.GetAll().Where(x => request.Nodes.Contains(x.Id));

                    foreach (var p in lstNodes)
                        PublishingProfile.Nodes.Add(p);
                }

                _PublishingProfileRepository.Add(PublishingProfile);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = request;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un perfil de publicación
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeletePublishingProfile(int id)
        {
            var result = new Response();
            

            try
            {
                result.Success = true;
                result.Message = "Registro eliminado correctamente";
                
                Entities.PublishingProfile profile = _PublishingProfileRepository.GetById(id);

                if (profile != null)
                {
                    await _PublishingProfileRepository.DeleteAsync(profile);
                    await this._unitOfWork.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los perfiles de publicación registrados
        /// </summary>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<Response> GetAll(int systemId)
        {
            var result = new Response();

            try
            {
                var data = await _PublishingProfileRepository.FindAllAsync(x => x.SystemId == systemId);

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = data.OrderBy( x=> x.Description )
                                     .Select(u => u.New())
                                     .ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los perfiles de publicación por paginación
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, bool? active, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.PublishingProfile>);
                var data     = await _PublishingProfileRepository.FindAllAsync( x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data   = data.Where(x => x.Description.ToLower().Contains(filter)).ToList();
                }

                if (active != null)
                    data = data.Where(x => x.Active == active.Value).ToList();
                                
                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Description)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener perfil de publicación por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _PublishingProfileRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar un perfil de publicación registrado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfile profile = _PublishingProfileRepository.GetById(request.Id);

                //Entities.PublishingProfile PublishingProfile = new Entities.PublishingProfile();

                //PublishingProfile.Id = request.Id;
                profile.Description = request.Description;
                profile.Active = request.Active;           
                
                // Save nodes
                if(request.Nodes.Count() > 0)
                {
                    profile.Nodes.Clear();

                    var lstNodes = _nodeRepository.GetAll().Where(x => request.Nodes.Contains(x.Id));

                    foreach (var p in lstNodes)
                        profile.Nodes.Add(p);
                }

                _PublishingProfileRepository.Update(profile);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = profile;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los nodos de un perfil de publicación
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetNodes(int id)
        {
            var result = new Response();

            try
            {
                var profile = _PublishingProfileRepository.GetById(id);

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = profile.Nodes.Select(n => new
                {
                    Id = n.Id,
                    Name = n.Name,
                    Description = n.Description,
                    NodeTypeId = n.NodeTypeId,
                    UrlImage = n.UrlImage,
                    NodeType = n.NodeType.Description
                }).ToList();

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Agregar nodo al perfil de publicación
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public async Task<Response> AddNodes(int id, List<int> nodes)
        {
            var result = new Response();

            try
            {

                var profile = _PublishingProfileRepository.GetById(id);
                profile.Nodes.Clear();

                var lstNodes = _nodeRepository.GetAll().Where(x => nodes.Contains(x.Id));

                foreach (var p in lstNodes)
                    profile.Nodes.Add(p);

                _PublishingProfileRepository.Update(profile);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Nodos agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los usuarios que tienen asignado un perfil d epublicación
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<Response> GetLinkedUsers(int profileId, int systemId)
        {
            var result = new Response();

            try
            {
                
                var data = await _UserRepository.FindAllAsync(x => x.SystemId == systemId);
                var users = data.Select(u => new
                {
                    Id = u.Id,
                    FullName = u.FullName,
                    Active = u.Active,
                    Selected = false
                }).ToList();

                var perfiles = _PublishingProfileUserRepository.GetAll()
                                                               .Where(ppu => ppu.PublishingProfileId == profileId)
                                                               .Select(v => v.UserId)
                                                               .ToList();
                result.Data = new
                {
                    u = users,
                    p = perfiles
                };

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}