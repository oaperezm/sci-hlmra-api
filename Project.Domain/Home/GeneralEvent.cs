﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities.Converters;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Home;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Home
{
    public class GeneralEvent: IGeneralEvent
    {
        private readonly IGeneralEventRepository _generalEventRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;
        private readonly string _container;

        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
        //DateTime.UtcNow.AddHours(offset.Hours);

        public GeneralEvent( IGeneralEventRepository generalEventRepository,
                             IFileUploader fileUploader,
                             IUnitOfWork unitOfWork) {

            _generalEventRepository = generalEventRepository;
            _unitOfWork             = unitOfWork;
            _fileUploader           = fileUploader;
            _container              = "events";
        }

        /// <summary>
        /// Agregar un nuevo evento general
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(GeneralEventSaveRequest request) {

            var result = new Response();
            string fileUrl = string.Empty;

            try
            {
                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, _container);

                var eventGeneral = new Entities.GeneralEvent
                {
                    Description  = request.Description,
                    EventDate    = request.EventDate,
                    Hours        = request.Hours,
                    Name         = request.Name,
                    Place        = request.Place,
                    Published    = request.Published,
                    RegisterDate = DateTime.UtcNow.AddHours(offset.Hours),
                    SystemId     = request.SystemId
                };

                if (!string.IsNullOrEmpty(fileUrl))
                    eventGeneral.UrlImage = fileUrl;

                _generalEventRepository.Add(eventGeneral);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = eventGeneral.New();
                result.Success = true;
                result.Message = "Evento registrado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = true;
                result.Message = $"Problemas al agregar un evento ${ ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un evento existe
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(GeneralEventUpdateRequest request) {

            var result     = new Response();
            string fileUrl = string.Empty;
            
            try
            {
                var eventGeneral = await _generalEventRepository.FindAsync(x => x.Id == request.Id);

                if (eventGeneral == null)
                    throw new Exception("Datos no encontraods");

                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    await _fileUploader.FileRemove(eventGeneral.UrlImage, _container);
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, _container);
                }

                if (!string.IsNullOrEmpty(fileUrl))
                    eventGeneral.UrlImage = fileUrl;
               
                eventGeneral.Description = request.Description;
                eventGeneral.EventDate   = request.EventDate;
                eventGeneral.Hours       = request.Hours;
                eventGeneral.Name        = request.Name;
                eventGeneral.Place       = request.Place;
                eventGeneral.Published   = request.Published;

                _generalEventRepository.Update(eventGeneral);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = eventGeneral.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = true;
                result.Message = $"Problemas al actualizar los datos del evento: ${ ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los tips registrados paginados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.GeneralEvent>);
                var data = await _generalEventRepository.FindAllAsync( x=> x.SystemId == systemId );

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    filter = filter.ToLower();

                    data = data.Where(x => compareInfo.IndexOf(x.Name.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1
                                         || compareInfo.IndexOf(x.Description.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.RegisterDate)
                               .ThenBy(x => x.Name)
                               .Skip((curentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data    = pageData.Select(x => x.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener evento por identificador
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int Id)
        {
            var result = new Response();

            try
            {
                var eventGeneral = await _generalEventRepository.FindAsync(x => x.Id == Id);

                if (eventGeneral == null)
                    throw new Exception("Datos no encontraods");

                result.Data    = eventGeneral.New();
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        
        }

        /// <summary>
        /// Eliminar un evento general existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var eventGeneral = await _generalEventRepository.FindAsync(x => x.Id == id);

                if (eventGeneral == null)
                    throw new Exception("Datos no encontraods");

                _generalEventRepository.Delete(eventGeneral);
                await _unitOfWork.SaveChangesAsync();
              
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
