﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Home;
using Project.Repositories.Core;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using System.Globalization;

namespace Project.Domain.Home
{
    public class News : INews
    {
        private readonly INewsRepository _newsRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        private TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        public News(INewsRepository newsRepository, IUnitOfWork unitOfWork, IFileUploader fileUploader)
        {
            _newsRepository = newsRepository;
            _unitOfWork     = unitOfWork;
            _fileUploader   = fileUploader;
        }

        /// <summary>
        /// Agregar una nueva noticia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddNews(NewsSaveRequest request)
        {

            var result = new Response();
            string fileUrl = string.Empty;

            try
            {
                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, "news");

                var News          = new Entities.News();
                News.Title        = request.Title;
                News.Description  = request.Description;
                News.RegisterDate = DateTime.UtcNow.AddHours(offset.Hours);
                News.Published    = request.Published;
                News.SystemId     = request.SystemId;

                if (!string.IsNullOrEmpty(fileUrl))
                    News.UrlImage = fileUrl;

                _newsRepository.Add(News);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = new Entities.News
                {
                    Id           = News.Id,
                    Title        = News.Title,
                    Description  = News.Description,
                    RegisterDate = News.RegisterDate,
                    UrlImage     = News.UrlImage,
                    Published    = News.Published                   
                };

                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Obtener todas las noticias registradas
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {

            var result = new Response();

            try
            {
                var data = await _newsRepository.GetAllAsync();
                result.Data = data.OrderBy(x => x.RegisterDate)
                                  .Select(x => new Entities.News()
                                  {
                                      Id          = x.Id,
                                      Title       = x.Title,
                                      Description = x.Description,
                                      UrlImage    = x.UrlImage
                                  }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Problemas al obtener las noticias";
            }

            return result;

        }

        /// <summary>
        /// Obtener todos los tips registrados paginados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.News>);
                var data = await _newsRepository.FindAllAsync(x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    filter          = filter.ToLower();                   

                    data = data.Where(x => compareInfo.IndexOf(x.Title.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1
                                         || compareInfo.IndexOf(x.Description.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x=> x.RegisterDate)
                               .ThenBy(x => x.Title)
                               .Skip((curentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(x => new Entities.News {
                                              Id           = x.Id,
                                              Title        = x.Title,
                                              Description  = x.Description,
                                              UrlImage     = x.UrlImage,
                                              Published    = x.Published,
                                              RegisterDate = x.RegisterDate
                                          }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de una noticia existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateNews(NewsUpdateRequest request)
        {
            var result       = new Response();
            string fileUrl   = string.Empty;
            string container = "news";

            try
            {
                var News = await _newsRepository.FindAsync(x => x.Id == request.Id);

                if (News == null)
                    throw new Exception("News no encontrado");

                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    await _fileUploader.FileRemove(News.UrlImage, container);
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                }

                if (!string.IsNullOrEmpty(fileUrl))
                    News.UrlImage = fileUrl;

                News.Title       = request.Title;
                News.Description = request.Description;
                News.Published   = request.Published;

                _newsRepository.Update(News);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = News.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un News registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteNews(int id)
        {
            var result = new Response();
            string container = "news";

            try
            {
                var News = await _newsRepository.FindAsync(x => x.Id == id);

                if (News == null) {
                    throw new Exception("Datos no encontrados");
                }

                await _fileUploader.FileRemove(News.UrlImage, container);

                _newsRepository.Delete(News);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Noticia eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar la noticia: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los datos de un News por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = await _newsRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
