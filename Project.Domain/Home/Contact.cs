﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Home
{
    public class Contact: IContact
    {
        private readonly IMailer _mail;


        public Contact(IMailer mail) {
            _mail = mail;
        }

        public async Task<Response> SendContactMail(MailRequest request) {

            var result = new Response();

            try
            {
                var data = new Entities.Utils.ContactMessage(request.Email,request.ReceptionEmail, request.Subject, request.Message, 
                                                             request.FullName, "ContactTamplate.html", request.SystemId, request.School, request.WorkPlace);               
                await _mail.SendContactMail(data);

                result.Success = true;
                result.Message = "Mensaje enviado corretamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error al intentar de mandar el correo, intentar más tarde<";
            }

            return result;
        }
    }
}
