﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Home;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Home
{
    public class Tip : ITip
    {
        private readonly ITipRepository _tipRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        private TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        public Tip(ITipRepository tipRepository, IUnitOfWork unitOfWork, IFileUploader fileUploader)
        {
            _tipRepository = tipRepository;
            _unitOfWork = unitOfWork;
            _fileUploader = fileUploader;
        }

        /// <summary>
        /// Agregar un nuevo tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddTip(TipSaveRequest request)
        {

            var result     = new Response();
            string fileUrl = string.Empty;

            try
            {

                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, "tips");

                var tip          = new Entities.Tip();
                tip.Name         = request.Name;
                tip.Description  = request.Description;
                tip.RegisterDate = DateTime.UtcNow.AddHours(offset.Hours);
                tip.Published    = request.Published;
                tip.SystemId     = request.SystemId;

                if (!string.IsNullOrEmpty(fileUrl))
                    tip.UrlImage = fileUrl;

                _tipRepository.Add(tip);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = new Entities.Tip
                {
                    Id           = tip.Id,
                    Name         = tip.Name,
                    Description  = tip.Description,
                    RegisterDate = tip.RegisterDate,
                    UrlImage     = tip.UrlImage,
                    Published    = tip.Published
                };

                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Obtener todos los tips registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {

            var result = new Response();

            try
            {
                var data = await _tipRepository.GetAllAsync();
                result.Data = data.OrderBy(x => x.Name)
                                  .Select(x => new Entities.Tip()
                                  {
                                      Id           = x.Id,
                                      Name         = x.Name,
                                      Description  = x.Description,
                                      UrlImage     = x.UrlImage,
                                      RegisterDate = x.RegisterDate,
                                      Published    = x.Published
                                  }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Problemas al obtener los tips";
            }

            return result;

        }

        /// <summary>
        /// Obtener todos los tips registrados paginados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Tip>);
                var data = await _tipRepository.FindAllAsync( x=> x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    filter = filter.ToLower();                    
                    data = data.Where(x => compareInfo.IndexOf(x.Name.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1
                                          || compareInfo.IndexOf(x.Description.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.RegisterDate)
                                .ThenBy( x=> x.Name)
                                .Skip((curentPage - 1) * result.Pagination.PageSize)
                                .Take(result.Pagination.PageSize)
                                .ToList();

                result.Data = pageData.Select(x => new Entities.Tip {
                    Id           = x.Id,
                    Name         = x.Name,
                    Description  = x.Description,
                    UrlImage     = x.UrlImage,
                    Published    = x.Published,
                    RegisterDate = x.RegisterDate
                }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un tip existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateTip(TipUpdateRequest request)
        {
            var result       = new Response();
            string fileUrl   = string.Empty;
            string container = "tips";

            try
            {
                var tip = await _tipRepository.FindAsync(x => x.Id == request.Id);

                if (tip == null)
                    throw new Exception("Tip no encontrado");

                // En caso contener datos el array de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    await _fileUploader.FileRemove(tip.UrlImage, container);
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                }

                if (!string.IsNullOrEmpty(fileUrl))
                    tip.UrlImage = fileUrl;

                tip.Name        = request.Name;
                tip.Description = request.Description;
                tip.Published   = request.Published;

                _tipRepository.Update(tip);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = tip.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un tip registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteTip(int id)
        {
            var result = new Response();
            string container = "tips";

            try
            {
                var tip = await _tipRepository.FindAsync(x => x.Id == id);

                if (tip == null)
                {
                    throw new Exception("Datos no encontrados");
                }

                await _fileUploader.FileRemove(tip.UrlImage, container);

                _tipRepository.Delete(tip);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tip eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el tip: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los datos de un tip por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _tipRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

    }
}
