﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Role : IRole
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IPermissionRepository _permissionRepository;       
        private readonly IUnitOfWork _unitOfWork;


        public Role(IRoleRepository roleRepository, 
                    IPermissionRepository permissionRepository,
                    INodeRepository nodeRepository,
                    IUnitOfWork unitOfWor)
        {

            this._roleRepository       = roleRepository;
            this._permissionRepository = permissionRepository;
          
            this._unitOfWork           = unitOfWor;
        }

        public async Task<Response> AddRole(RoleSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Role role = new Entities.Role();

                role.Id = request.Id;
                role.Name = request.Name;
                role.Description = request.Description;
                role.Active = request.Active;

                _roleRepository.Add(role);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = role;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteRole(int RoleId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _roleRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Name = u.Name,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _roleRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateRole(RoleSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Role role = new Entities.Role();

                role.Id = request.Id;
                role.Name = request.Name;
                role.Description = request.Description;
                role.Active = request.Active;

                _roleRepository.Update(role);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = role;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        public class permiso
        {
            public int Id               { get; set; }
            public string Module        { get; set; }
            public string Description   { get; set; }
            public bool Active          { get; set; }
            public bool Assigned        { get; set; }
            public int? ParentId        { get; set; }
        }
        
        public async Task<Response> GetRolePermissions(int id)
        {
            var result = new Response();

            try
            {
                var rol = _roleRepository.GetById(id);             

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var perms = _permissionRepository.GetAll().
                    Select(u => new permiso {
                        Id          = u.Id,
                        Module      = u.Module,
                        Description = u.Description,
                        Active      = u.Active,
                        Assigned    = false,
                        ParentId    = u.ParentId.HasValue ? u.ParentId : 0
                    }).OrderBy(u => u.Module).ToList();

                if (rol.Permissions != null && rol.Permissions.Count() > 0)
                {
                    foreach (var p in perms)
                    {
                        var rp = rol.Permissions.Where(rperm => rperm.Id == p.Id).FirstOrDefault();
                        if (rp != null)
                        {
                            p.Assigned = true;

                        }
                    }
                }

                result.Data = perms;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdatePermissions(int id, List<int> permissions)
        {
            var result = new Response();

            try
            {

                var rol = _roleRepository.GetById(id);
                rol.Permissions.Clear();

                var lstPermissions = _permissionRepository.GetAll().Where(x => permissions.Contains(x.Id));

                foreach(var p in lstPermissions)
                    rol.Permissions.Add(p);
                
                _roleRepository.Update(rol);
                _unitOfWork.SaveChanges();

                result.Success = true;
                result.Message = "Roles agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar permisos al rol: { ex.Message }";
            }

            return result;
        }

    }
}