﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Learningexpected:ILearningexpected
    {
        private readonly ILearningexpectedRepository _learningexpectedRepository;
        private readonly IEducationLevelRepository _educationLevelRepository;
        private readonly IUnitOfWork _unitOfWork;
        public Learningexpected(ILearningexpectedRepository learningexpectedRepository, IUnitOfWork unitOfWor, IEducationLevelRepository educationLevelRepository)
        {   this._learningexpectedRepository = learningexpectedRepository;
            this._unitOfWork = unitOfWor;
            this._educationLevelRepository = educationLevelRepository;
        }
        public async Task<Response> Add(LearningexpectedRequest request)
        {
            var result = new Response();

            try
            {   Entities.Learningexpected learningexpected = new Entities.Learningexpected();
                learningexpected.Description = request.Description;
                learningexpected.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    learningexpected.EducationLevelId = request.EducationLevelId;
                }
                _learningexpectedRepository.Add(learningexpected);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = learningexpected;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public Task<Response> Delete(int Id)
        {
            throw new NotImplementedException();
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _learningexpectedRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active,
                        EducationLevelId=u.EducationLevelId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _learningexpectedRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> Update(LearningexpectedUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Learningexpected Learningexpected = new Entities.Learningexpected();

                Learningexpected.Id = request.Id;
                Learningexpected.Description = request.Description;
                Learningexpected.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    Learningexpected.EducationLevelId = request.EducationLevelId;
                }
                _learningexpectedRepository.Update(Learningexpected);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Learningexpected;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
    }
}
