﻿using Entities;
using Repositories.Management;
using System;
using System.Collections.Generic;


namespace Domain.Management
{
    public class Store : IStore
    {
        private readonly IStoreRepository _store;

        public Store(IStoreRepository store)
        {
            this._store = store;
        }

        public Product Add(Product product)
        {
            if (product.Name.Equals(String.Empty))
            {
                throw new Exception("Falta el argumento de Name");
            }
            return _store.Add(product);
        }

        public List<Product> List(Product product)
        {
            return _store.List(product);
        }

        public Product Remove(Product product)
        {
            return _store.Remove(product);
        }

        public Product Update(Product product)
        {
            return _store.Update(product);
        }
    }
}
