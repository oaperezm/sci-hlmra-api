﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class AreaPersonalSocialDevelopment : IAreaPersonalSocialDevelopment
    {
        private readonly IAreaPersonalSocialDevelopmentRepository _areaPersonalSocialDevelopmentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEducationLevelRepository _educationLevelRepository;
        private readonly IFileTypeRepository _fileTypeRepository;
        public AreaPersonalSocialDevelopment(IAreaPersonalSocialDevelopmentRepository areaPersonalSocialDevelopmentRepository, IUnitOfWork unitOfWor,
                                             IEducationLevelRepository educationLevelRepository,
                                             IFileTypeRepository fileTypeRepository)
        {
            this._areaPersonalSocialDevelopmentRepository = areaPersonalSocialDevelopmentRepository;
            this._unitOfWork = unitOfWor;
            this._educationLevelRepository = educationLevelRepository;
            this._fileTypeRepository = fileTypeRepository;
        }
        public async Task<Response> Add(AreaPersonalSocialDevelopmentRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.AreaPersonalSocialDevelopment apsd = new Entities.AreaPersonalSocialDevelopment();

                apsd.Description = request.Description;
                apsd.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                 educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel ==null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    apsd.EducationLevelId = request.EducationLevelId;
                }
                Entities.FileType FileType = new Entities.FileType();
                FileType = await _fileTypeRepository.FindAsync(r => r.Id.Equals(request.FileTypeId));
                if (educationLevel == null)
                {
                    throw new Exception("No se encontro el recurso en nustro catalogo");
                }
                else
                {
                    apsd.FileTypeTypeId = request.FileTypeId;
                }



                _areaPersonalSocialDevelopmentRepository.Add(apsd);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = apsd;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> Delete(int AreaPSDId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _areaPersonalSocialDevelopmentRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        EducationLevelId = u.EducationLevelId,
                        FileTypeId= u.FileTypeTypeId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _areaPersonalSocialDevelopmentRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> Update(AreaPersonalSocialDevelopmentUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.AreaPersonalSocialDevelopment APSD = new Entities.AreaPersonalSocialDevelopment();

                APSD.Id = request.Id;
                APSD.Description = request.Description;
                APSD.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    APSD.EducationLevelId = request.EducationLevelId;
                }
                Entities.FileType FileType = new Entities.FileType();
                FileType = await _fileTypeRepository.FindAsync(r => r.Id.Equals(request.FileTypeId));
                if (educationLevel == null)
                {
                    throw new Exception("No se encontro el recurso en nustro catalogo");
                }
                else
                {
                    APSD.FileTypeTypeId = request.FileTypeId;
                }


                _areaPersonalSocialDevelopmentRepository.Update(APSD);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = APSD;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
    }
}
