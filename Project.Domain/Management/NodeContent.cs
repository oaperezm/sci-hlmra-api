﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using Project.Repositories.Services;

namespace Project.Domain.Management
{
    public class NodeContent : INodeContent
    {
        private readonly INodeContentRepository _nodeContentRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IUnitOfWork _unitOfWork;
       

        public NodeContent(INodeContentRepository nodeContentRepository,
                           IContentRepository contentRepository,
                           IPushNotificationService srvPushNotification,
                           IUnitOfWork unitOfWor)
        {            
            _nodeContentRepository  = nodeContentRepository;
            _contentRepository      = contentRepository;
            _srvPushNotification    = srvPushNotification;
            _unitOfWork             = unitOfWor;
        }

        /// <summary>
        /// Permite relacionar un contenido a un elemento de la publicación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddNodeContent(NodeContentSaveRequest request)
        {
            var result = new Response();

            try
            {                
                Entities.NodeContent nodeContent = new Entities.NodeContent();

                nodeContent.ContentId = request.ContentId;
                nodeContent.NodeId    = request.NodeId;
                nodeContent.NodeRoute = request.NodeRoute;

                var node = await _contentRepository.FindAsync(x => x.Id == request.ContentId);

                if (node != null)
                {
                    nodeContent.IsPublic = node.IsPublic;
                }
                
                _nodeContentRepository.Add(nodeContent);
                await this._unitOfWork.SaveChangesAsync();


                #region SEND PUSH NOTIFICATION

                var content = await _contentRepository.FindAsync(x => x.Id == nodeContent.ContentId);
                var data = new
                {
                    nodeRoute = nodeContent.NodeRoute,
                    name = content.Name,
                };

                var topic = request.SystemId == (int)EnumSystems.SALI ? "general_sali" : "general_ra";
                // await _srvPushNotification.CreateNotification(topic, (int)EnumPush.NewContentAdmin, data);

                #endregion

                result.Data   = new Entities.NodeContent {
                    Id        = nodeContent.Id,
                    ContentId = nodeContent.ContentId,
                    IsPublic  = nodeContent.IsPublic,
                    NodeId    = nodeContent.NodeId,
                    NodeRoute = nodeContent.NodeRoute,
                };
                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos del contenido: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        ///  Eliminar un registro de la relación del contenido y la estructura
        /// </summary>
        /// <param name="nodeContentId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteNodeContent(int nodeContentId)
        {
            var result = new Response();

            try
            {
                var deleteElement = _nodeContentRepository.GetById(nodeContentId);

                if(deleteElement != null) {
                    _nodeContentRepository.Delete(deleteElement);
                    await _unitOfWork.SaveChangesAsync();
                }

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos del contenido: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener toda la relación entre el nodo y el contenido
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _nodeContentRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,                        
                        NodeId = u.NodeId                        
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener la información de un contenido 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _nodeContentRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de una asignación de contenido y node
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateNodeContent(NodeContentUpdateRequest request)
        {
            var result = new Response();

            try
            {
                Entities.NodeContent nodeContent = new Entities.NodeContent();

                nodeContent.Id        = request.Id;
                nodeContent.ContentId = request.ContentId;
                nodeContent.NodeId    = request.NodeId;
                
                var node = await _contentRepository.FindAsync(x => x.Id == request.ContentId);

                if (node != null)
                    nodeContent.IsPublic = node.IsPublic;

                _nodeContentRepository.Update(nodeContent);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = nodeContent;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}