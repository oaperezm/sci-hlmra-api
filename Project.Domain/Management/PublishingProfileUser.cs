﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class PublishingProfileUser : IPublishingProfileUser
    {
        private readonly IPublishingProfileUserRepository _PublishingProfileUserRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PublishingProfileUser(IPublishingProfileUserRepository PublishingProfileUserRepository, INodeRepository nodeRepository, IUnitOfWork unitOfWor)
        {

            this._PublishingProfileUserRepository = PublishingProfileUserRepository;
            this._nodeRepository = nodeRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddPublishingProfileUser(PublishingProfileUserSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfileUser PublishingProfileUser = new Entities.PublishingProfileUser();

                PublishingProfileUser.PublishingProfileId = request.PublishingProfileId;
                PublishingProfileUser.UserId = request.UserId;
                PublishingProfileUser.RegisterDate = request.RegisterDate;
                
                _PublishingProfileUserRepository.Add(PublishingProfileUser);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = request;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> DeletePublishingProfileUser(int id)
        {
            var result = new Response();
            

            try
            {
                result.Success = true;
                result.Message = "Registro eliminado correctamente";
                
                Entities.PublishingProfileUser profile = _PublishingProfileUserRepository.GetById(id);

                if (profile != null)
                {
                    await _PublishingProfileUserRepository.DeleteAsync(profile);
                    await this._unitOfWork.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _PublishingProfileUserRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        PublishingProfileId = u.PublishingProfileId,
                        PublishingProfileDescription = u.PublishingProfile.Description,
                        UserId = u.UserId,
                        UserName = u.User.FullName,
                        RegisterDate = u.RegisterDate
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _PublishingProfileUserRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdatePublishingProfileUser(PublishingProfileUserUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfileUser profile = _PublishingProfileUserRepository.GetById(request.Id);

                profile.PublishingProfileId = request.PublishingProfileId;
                profile.UserId = request.UserId;
                profile.RegisterDate = request.RegisterDate;

                _PublishingProfileUserRepository.Update(profile);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = profile;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetByUserId(int userId)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _PublishingProfileUserRepository.FindAsync(r => r.UserId.Equals(userId));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

    }
}