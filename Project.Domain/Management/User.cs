﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Management;
using Project.Repositories.Courses;
using Project.Repositories.Services;

namespace Project.Domain.Management
{
    public class User : IUser
    {
        private readonly IUserRepository _userRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IPermissionRepository _permissionRepository;
        private readonly IPublishingProfileRepository _publishingProfileRepository;
        private readonly IPublishingProfileUserRepository _publishingProfileUserRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMailer _imailer;
        private readonly IAESEncryption _iencrypter;
        private readonly IFileUploader _fileUploader;

        public User(
            IUserRepository userRepositor, 
            INodeRepository nodeRepository, 
            IPublishingProfileRepository publishingProfilerepository,
            IPublishingProfileUserRepository publishingProfileUserrepository,
            IPermissionRepository permissionRepository,
            IStudentGroupRepository studentGroupRepository,
            IStudentRepository studentRepository,
            IUnitOfWork unitOfWor, 
            IMailer imailer, 
            IAESEncryption iencrypter,
            IFileUploader fileUploader) {

            this._userRepository = userRepositor;
            this._unitOfWork     = unitOfWor;
            this._imailer = imailer;
            this._iencrypter = iencrypter;
            this._nodeRepository = nodeRepository;
            this._permissionRepository = permissionRepository;
            this._publishingProfileRepository = publishingProfilerepository;
            this._publishingProfileUserRepository = publishingProfileUserrepository;
            this._studentGroupRepository = studentGroupRepository;
            this._studentRepository = studentRepository;
            this._fileUploader = fileUploader;
        }

        /// <summary>
        /// Obtiene los permisos, perfiles a los cuales tiene acceso el usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUserProfile(int userId) {

            var result          = new Response();
            int profileId       = 0;
            var fullPermision   = new List<Project.Entities.Node>();            
            var profileNodes    = new List<Entities.Node>();
            var userNodes       = new List<Entities.Node>();
            var publishingNodes = new List<Entities.Node>();

            try
            {
                // Get if there is a related profile 
                var profile = _publishingProfileUserRepository.Get(ppu => ppu.UserId == userId);

                if (profile != null)                
                    profileId = profile.PublishingProfileId.Value;
                
                var user = _userRepository.GetById(userId);

                if (user == null)
                    throw new Exception("Usuario no encontrado");

                // Get all nodes from the related profile 
                if (profileId != 0)
                {
                    profileNodes = _publishingProfileRepository.GetById(profileId).Nodes.Select(n => new Project.Entities.Node
                    {
                        Id          = n.Id,
                        Name        = n.Name,
                        Description = n.Description,
                        NodeTypeId  = n.NodeTypeId,
                        UrlImage    = n.UrlImage,
                        ParentId    = n.ParentId,
                        Active      = n.Active,
                    }).ToList();
                }

                userNodes = user.Nodes                                          
                                .Select(n => new Project.Entities.Node
                                          {
                                              Id            = n.Id,
                                              Name          = n.Name,
                                              Description   = n.Description,
                                              NodeTypeId    = n.NodeTypeId,
                                              UrlImage      = n.UrlImage,    
                                              ParentId      = n.ParentId,
                                              Active        = n.Active
                                          })
                                .ToList();

                if(profileNodes.Count() != 0)
                {
                    publishingNodes = profileNodes.Concat(userNodes).ToList();
                } else
                {
                    publishingNodes = profileNodes.ToList();
                }

                var nodes           = _nodeRepository.GetAll();
                var userPermisions  = user.Role.Permissions.ToList();
                var permissions     = userPermisions.Where(x => x.ParentId == null && x.Active)
                                                    .Select(p => new
                                                           {
                                                               Title         = p.Module,
                                                               Description   = p.Description,
                                                               Icon          = p.Icon,
                                                               Id            = p.Id,
                                                               ParentId      = 0,
                                                               Url           = p.Url,
                                                               Children      = userPermisions.Where(s => s.ParentId == p.Id)
                                                                                             .Select( s => new {
                                                                                                 Title       = s.Module,
                                                                                                 Description = s.Description,                                                                                                 
                                                                                                 Icon        = s.Icon,
                                                                                                 Id          = s.Id,                                                               
                                                                                                 Url         = s.Url,
                                                                                                 ParentId    = s.ParentId.HasValue ? s.ParentId : 0
                                                                                              })
                                                                                             .OrderBy( s=> s.Title)
                                                                                             .ToList()
                                                            })
                                                    .ToList();

               
                foreach (var p in publishingNodes) {

                    var parentId = p.ParentId;

                    while (parentId > 0) {
                        var parent = nodes.FirstOrDefault(x => x.Id == parentId);

                        if (parent != null)
                        {
                            if (!publishingNodes.Any(x => x.Id == parent.Id) && !fullPermision.Any(x => x.Id == parent.Id))
                            {

                                if (!fullPermision.Contains(parent))
                                {
                                    fullPermision.Add(new Entities.Node
                                    {
                                        Active = parent.Active,
                                        Description = parent.Description,
                                        Id = parent.Id,
                                        Name = parent.Name,
                                        NodeTypeId = parent.NodeTypeId,
                                        ParentId = parent.ParentId,
                                        UrlImage = parent.UrlImage
                                    });
                                }
                            }

                            parentId = parent.ParentId;
                        }
                        else {
                            parentId = 0;
                        }                        
                    }                    
                }

                fullPermision = fullPermision.Select(d => d).Distinct().ToList();
                fullPermision.ForEach(f => {                
                    if(!publishingNodes.Contains(f))
                        publishingNodes.Add(f);
                });
                 
                result.Data = new {
                   permissions,
                   publishingNodes = publishingNodes.OrderBy( p=> p.Id ),
                    user = new  Project.Entities.User {
                        UrlImage = user.UrlImage,
                        FullName = user.FullName,
                        Email    = user.Email,
                        Gender   = user.Gender,
                        Role     = new Project.Entities.Role {
                            Description = user.Role?.Description,
                            Id          = user.RoleId
                        }                        
                    }
                };

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";                

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener el perfil: {ex.Message}";
                
            }

            return result;

        }
        
        public async Task<Response> RegisterUser(UserSaveRequest request)
        {
            var result       = new Response();
            var newUser      = new Project.Entities.User();
            string container = "users";
            string fileUrl   = string.Empty;

            try
            {

                if (_userRepository.Get(x => x.Email.Equals(request.Email)) != null)
                    throw new Exception($"Ya existe un usuario con el correo: { request.Email }");

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileContentBase64 != null && request.FileContentBase64.Length > 0)
                {
                    fileUrl          = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                    newUser.UrlImage = fileUrl;
                }

                result.Success = true;

                newUser.Active       = true;
                newUser.Birthday     = request.Birthday;
                newUser.Email        = request.Email;
                newUser.FullName     = request.FullName;
                newUser.Gender       = request.Gender;
                newUser.Institution  = request.Institution;
                newUser.Password     = this._iencrypter.EncryptMessage(request.Password);
                newUser.RegisterDate = DateTime.Now;
                newUser.RoleId       = request.RoleId;
                newUser.State        = request.State;
                newUser.LevelId      = request.LevelId;
                newUser.AreaId       = request.AreaId;
                newUser.SubsystemId  = request.SubsystemId;
                newUser.Semester     = request.Semester;

                _userRepository.Add(newUser);
                _unitOfWork.SaveChanges();
                
                await this._imailer.SendNewUserMail(new Entities.Utils.WelcomeMessage(request.Email, "servidor@sali.com", "Bienvenido a SALI", "NewUser.html", request.FullName));

                result.Data    = newUser;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            
            return result;
        }

        public async Task<Response> DeleteUser(int userId)
        {
            var result = new Response();

            try
            {
                var user    = _userRepository.GetById(userId);
                user.Active = false;

                _userRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Success = true;
                result.Message = "Datos actiulizados correctamente";
                
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {                
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = _userRepository.GetAll().Select(u => new  Project.Entities.User {
                       Active      = u.Active,
                       Birthday    = u.Birthday,
                       Email       = u.Email,
                       FullName    = u.FullName,
                       Gender      = u.Gender,
                       Id          = u.Id,
                       Institution = u.Institution,
                       Key         = u.Key,
                       Password    = u.Password,
                       Role        = new Project.Entities.Role { Id = u.Role.Id , Name = u.Role.Name },
                       State       = u.State
                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";                
            }
            
            return result;            
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(x => x.Id.Equals(id));

                if (user == null)
                    throw new Exception("El usuario no existe");

                result.Data = user;
                result.Success = true;
                result.Message = "Usuario valido";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        public Task<Response> RecoveryPassword(string email, string bodyHTML)
        {
            throw new NotImplementedException();
        }
        
        public async Task<Response> UpdateUser(UserUpdateRequest request)
        {
            var result = new Response();
            string container = "users";
            string fileUrl = string.Empty;

            try
            {
                var user = _userRepository.GetById(request.Id);

                if (_userRepository.Get(x => x.Email.Equals(request.Email) && x.Id != request.Id ) != null)
                    throw new Exception($"Ya existe un usuario con el correo: { request.Email }");

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileContentBase64 != null && request.FileContentBase64.Length > 0)
                {
                    fileUrl = await this._fileUploader.FileUpload(request.FileContentBase64, request.FileName, container);
                    user.UrlImage = fileUrl;
                }

                user.Birthday       = request.Birthday;
                user.Email          = request.Email;
                user.FullName       = request.FullName;
                user.Gender         = request.Gender;
                user.Institution    = request.Institution;
                user.UpdateDate     = DateTime.Now;
                user.RoleId         = request.RoleId;
                user.State          = request.State;

                _userRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Success = true;                
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Valida que se encuentre registrado el usuario
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Password de usuario</param>
        /// <returns></returns>
        public async Task<Response> ValidateUser(string username, string password)
        {
            var result = new Response();

            try
            {
                password = this._iencrypter.EncryptMessage(password);
                var user = await _userRepository.FindAsync(x => x.Email.Equals(username) && x.Password.Equals(password));

                if(user == null)
                    throw new Exception("Usuario y/o contraseña no son validos");

                if(!user.Active)
                    throw new Exception("El usuario esta temporalmente suspendido, favor de contactar al administrador");

                result.Data    = user;
                result.Success = true;
                result.Message = "Usuario valido";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        /// <summary>
        /// Asigna una nueva contraseña al usuario y la envía por correo
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public async Task<Response> ResetPassword(UserSavePasswordRequest Request)
        {
            var result = new Response();

            try
            {
                var user = await _userRepository.FindAsync(x => x.Email.Equals(Request.Email));

                if (user == null)
                    throw new Exception("El usuario no existe");

                string NewPassword = this._iencrypter.GeneratedPassword();
                user.Password = this._iencrypter.EncryptMessage(NewPassword);
                this._unitOfWork.SaveChanges();

                await this._imailer.SendPwdResetMail(new Entities.Utils.PwdResetMessage(user.Email, "servidor@sali.com", "Recuperar contraseña", "recoverpasswordtemplate.html", user.FullName, NewPassword));

                result.Success = true;
                result.Message = "Una nueva contraseña ha sido enviada al correo del usuario.";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        /// <summary>
        /// GetNodes: Get user's associated nodes
        /// </summary>
        /// <param name="id">User's Id</param>
        /// <returns>A list of node entities</returns>
        public async Task<Response> GetNodes(int id)
        {
            var result = new Response();

            try
            {
                var user = _userRepository.GetById(id);
                if (user != null)
                {
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";
                    result.Data = user.Nodes.Select(n => new 
                    {
                        Id = n.Id,
                        Name = n.Name,
                        Description = n.Description,
                        NodeTypeId = n.NodeTypeId,
                        UrlImage = n.UrlImage,
                        NodeType = n.NodeType.Description
                    }).ToList();
                } else
                {
                    result.Success = false;
                    result.Message = "No se encontró referencia al usuario solicitado";
                }
                

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// AddNodes: Rewrite user's associated node list
        /// </summary>
        /// <param name="request">User's Id and list of nodes</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> AddNodes(int id, List<int> nodes)
        {
            var result = new Response();

            try
            {

                var user = _userRepository.GetById(id);
                user.Nodes.Clear();

                var lstNodes = _nodeRepository.GetAll().Where(x => nodes.Contains(x.Id));

                foreach (var p in lstNodes)
                    user.Nodes.Add(p);

                _userRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Success = true;
                result.Message = "Nodos agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// CopyProfile: Duplicate node set from a publishingprofile to any user & associate userid with userprofileid
        /// </summary>
        /// <param name="profileId">Profile's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> CopyProfile(int profileId, int[] userIds)
        {
            var result = new Response();

            try
            {
                foreach (int userId in userIds)
                {
                    // Delete previously added user
                    

                    var user = _userRepository.GetById(userId);
                    if (user != null)
                    {
                        var profile = _publishingProfileRepository.GetById(profileId);
                        if (profile != null && profile.Nodes.Count() > 0)
                        {
                            user.Nodes.Clear();
                            foreach (var p in profile.Nodes)
                                user.Nodes.Add(p);

                            _userRepository.Update(user);
                            _unitOfWork.SaveChanges();

                            result.Success = true;
                            result.Message = userIds.Count() == 1 ? "Se asignó el perfil correctamente" : "Se asignaron los perfiles correctamente";
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "No se encontró referencia al perfil solicitado";
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No se encontró referencia al usuario solicitado";
                    }
                }
                

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetByMail(string eMail)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(eMail) && IsValidEmail(eMail)) 
            try
            {
                var user = await _userRepository.FindAsync(x => x.Email.Equals(eMail));

                if (user == null) throw new Exception("El usuario no existe");

                result.Data = //new Project.Entities.User
                    new {
                        UrlImage = user.UrlImage,
                        FullName = user.FullName,
                        Email = user.Email,
                        Gender = user.Gender,
                        RoleId = user.RoleId,
                        RegisterDate = user.RegisterDate,
                        RoleDescription = user.Role.Description
                    };
                result.Success = true;
                result.Message = "Usuario valido";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = ex.Message;
            }
            else
            {
                result.Success = false;
                result.Message = "El correo no es válido";
            }


            return result;
        }

        private bool IsValidEmail(string eMail)
        {
            return true;
        }

        public async Task<Response> GetByPartialMail(string partialMail)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var users = _userRepository.
                        GetMany(x => x.Email.Contains(partialMail)).
                        Select(s => new
                        {
                            Id = s.Id,
                            UrlImage = s.UrlImage,
                            FullName = s.FullName,
                            Email = s.Email,
                            Gender = s.Gender,
                            RoleId = s.RoleId,
                            RegisterDate = s.RegisterDate,
                            RoleDescription = s.Role.Description
                        }

                        ).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }

        public async Task<Response> GetByPartialMailRole(string partialMail, int roleId)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var users = _userRepository.
                        GetMany(x => x.Email.Contains(partialMail) && x.RoleId == roleId).
                        Select(s => new
                        {
                            Id = s.Id,
                            UrlImage = s.UrlImage,
                            FullName = s.FullName,
                            Email = s.Email,
                            Gender = s.Gender,
                            RoleId = s.RoleId,
                            RegisterDate = s.RegisterDate,
                            RoleDescription = s.Role.Description
                        }

                        ).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }

        public async Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId)
        {
            var result = new Response();

            if (!string.IsNullOrEmpty(partialMail))
                try
                {
                    var groups = _studentGroupRepository.GetAll().
                        Where(sg => sg.CourseId.Equals(courseId)).
                        Select(i => i.Id).ToList();

                    var students = _studentRepository.GetAll().
                        Where(s => groups.Contains(s.StudentGroupId)).                        
                        Select(s => s.UserId).ToList();

                    var users = _userRepository.
                        GetAll().Where(x => x.Email.Contains(partialMail) && x.RoleId == roleId && !students.Contains(x.Id)).
                        Select(s => new 
                        {
                            Id = s.Id,
                            UrlImage = s.UrlImage,
                            FullName = s.FullName,
                            Email = s.Email,
                            Gender = s.Gender,
                            RoleId = s.RoleId,
                            RegisterDate = s.RegisterDate,
                            RoleDescription = s.Role.Description
                        }).ToList();

                    if (users == null || users.Count() == 0) throw new Exception("No se encontro información.");

                    result.Data = users;
                    result.Success = true;
                    result.Message = "Información recuperada correctamente.";

                }
                catch (Exception ex)
                {

                    result.Success = false;
                    result.Message = ex.Message;
                }
            else
            {
                result.Success = false;
                result.Message = "La búsqueda no es válida";
            }


            return result;
        }
        /// <summary>
        /// LinkProfile: Associate userid with userprofileid
        /// </summary>
        /// <param name="profileId">Profile's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns>Success value, message and 200 code</returns>
        public async Task<Response> LinkProfile(int profileId, int[] userIds)
        {
            var result = new Response();

            try
            {
                // Delete previouly created records for this profile
                _publishingProfileUserRepository.Delete(pu => pu.PublishingProfileId == profileId);
                _unitOfWork.SaveChanges();

                foreach (int userId in userIds)
                {
                    // Delete previously added user
                    var user = _userRepository.GetById(userId);
                    if (user != null)
                    {
                        var profile = _publishingProfileRepository.GetById(profileId);
                        if (profile != null)
                        {
                            // Delete previouly created records for this user
                            _publishingProfileUserRepository.Delete(pu => pu.UserId == user.Id);
                            _unitOfWork.SaveChanges();

                            Entities.PublishingProfileUser ppu = new Entities.PublishingProfileUser();

                            ppu.PublishingProfileId = profileId;
                            ppu.UserId = user.Id;
                            ppu.RegisterDate = DateTime.UtcNow;

                            _publishingProfileUserRepository.Add(ppu);                            
                            _unitOfWork.SaveChanges();

                            result.Success = true;
                            result.Message = userIds.Count() == 1 ? "Se asoció el usuario correctamente" : "Se asociaron los usuarios correctamente";
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "No se encontró referencia al perfil solicitado";
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No se encontró referencia al usuario solicitado";
                    }
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        
    }
}
