﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class EducationLevel: IEducationLevel
    {
        private readonly IEducationLevelRepository _educationLevelRepository;
        private readonly IUnitOfWork _unitOfWork;
        public EducationLevel(IEducationLevelRepository educationLevel, IUnitOfWork unitOfWor)
        {

            this._educationLevelRepository = educationLevel;
            this._unitOfWork = unitOfWor;
        }
        public async Task<Response> Add(EducationLevelRequest request)
        {
            var result = new Response();

            try
            {

                Entities.EducationLevel EducationLevel = new Entities.EducationLevel();

                EducationLevel.Description = request.Description;
                EducationLevel.Active = request.Active;
                _educationLevelRepository.Add(EducationLevel);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = EducationLevel;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public Task<Response> Delete(int Id)
        {
            throw new NotImplementedException();
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _educationLevelRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _educationLevelRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> Update(EducationLevelUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.EducationLevel EducationLevel = new Entities.EducationLevel();

                EducationLevel.Id = request.Id;
                EducationLevel.Description = request.Description;
                EducationLevel.Active = request.Active;

                _educationLevelRepository.Update(EducationLevel);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = EducationLevel;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

    }
}
