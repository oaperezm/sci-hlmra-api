﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Block : IBlock
    {
        private readonly IBlockRepository _BlockRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Block(IBlockRepository BlockRepository, IUnitOfWork unitOfWor)
        {

            this._BlockRepository = BlockRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddBlock(BlockSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Block Block = new Entities.Block();

                Block.Description = request.Description;

                _BlockRepository.Add(Block);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Block;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteBlock(int BlockId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _BlockRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _BlockRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateBlock(BlockUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Block Block = new Entities.Block();

                Block.Id = request.Id;
                Block.Description = request.Description;

                _BlockRepository.Update(Block);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Block;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}