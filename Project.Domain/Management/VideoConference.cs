﻿using System;

using System.Threading.Tasks;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Domain.Core;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using Project.Repositories.Services;
using Project.Entities.Enums;

namespace Project.Domain.Management
{
    public class VideoConference : IVideoconference
    {
        private readonly IVideoConferenceRepository _videoConferenceRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IBook _bookRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly INodeRepository _nodeRepository;
        private readonly IMailer _mailer;
        private readonly IStudentGroupRepository _StudentGroupRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStatesVideoConferenceRepository _statesVideoConferenceRepository;
        
        private readonly IUserRepository _userRepository;

        public VideoConference(IVideoConferenceRepository videoConferenceRepository, IAuthorRepository authorRepository, IBook bookRepository, IUnitOfWork unitOfWork, INodeRepository nodeRepository
                , IMailer mailer, IStudentGroupRepository StudentGroupRepository, ICourseRepository CourseRepository, IStatesVideoConferenceRepository statesVideoConferenceRepository,
            IUserRepository userRepository)
        {
            this._videoConferenceRepository = videoConferenceRepository;
            this._authorRepository = authorRepository;
            this._bookRepository = bookRepository;
            this._unitOfWork = unitOfWork;
            this._nodeRepository = nodeRepository;
            this._mailer = mailer;
            this._StudentGroupRepository = StudentGroupRepository;
            this._courseRepository = CourseRepository;
            this._statesVideoConferenceRepository = statesVideoConferenceRepository;
            this._userRepository = userRepository;
        }



        /// <summary>
        /// Agregar un nuevo registro 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="nodeRepository"></param>
        /// <returns></returns
        public async Task<Response> Add(VideoConferenceSaveRequest request,int systemId, int userId)
        {
            var result = new Response();
            try {
                
                string emails="";
                string emailsExternal = "";
              
                int svc = 0;
              
                if (systemId== (int)EnumSystems.SALI)
                {
                    List<Entities.DTO.InvitedVideoConferenceDTO> strList = new List<Entities.DTO.InvitedVideoConferenceDTO>();

                    Entities.VideoConference videocon = new Entities.VideoConference();
                    videocon.Title = request.Title;
                    videocon.Information = request.Information;
                    videocon.DateTimeVideoConference = request.DateTimeVideoConference;
                    videocon.Duration = request.Duration;
                    videocon.RegisterDate = DateTime.Now;
                    videocon.Commentary = request.Commentary;
                    videocon.UpdateDate = DateTime.Now;
                    videocon.RoomVideoConference = request.RoomVideoConference;
                    videocon.StatusId = request.StatusId;

                    //videocon.emailsExternal = request.ExternalGuest;
                    if (request.StudentGroupId > 0)
                    {
                        videocon.StudentGroupId = request.StudentGroupId;
                    }
                    else
                    {
                        videocon.StudentGroupId = 0;
                    }
                    //else
                    //{
                    //    Entities.StudentGroup student = new Entities.StudentGroup();
                    //    student = _StudentGroupRepository.GetMany(x => x.Description == "General").FirstOrDefault();
                    //    if (student != null && student.Id != 0)
                    //    {
                    //        videocon.StudentGroupId = student.Id;
                    //    }
                    //    else
                    //    {
                    //        throw new Exception("Se necesita asignar  un  grupo");
                    //    }

                    //}
                    if (request.CourseId > 0)
                    {
                        videocon.CourseId = request.CourseId;
                    }
                    else
                    {
                        videocon.CourseId = 0;
                    }
                    //else
                    //{
                    //    Entities.Course course = new Entities.Course();
                    //    course = _courseRepository.GetMany(x => x.Name == "General").FirstOrDefault();
                    //    if (course != null && course.Id != 0)
                    //    {
                    //        videocon.CourseId = course.Id;
                    //    }
                    //    else
                    //    {
                    //        throw new Exception("Se necesita asignar  un  curso");

                    //    }
                    //}
                    if (userId > 0)
                    {
                        videocon.UserId = userId;
                    }
                    else
                    {
                        throw new Exception("Se necesita asignar  un  usuario");
                    }
                    Entities.StatesVideoConference statesVideoConference = new Entities.StatesVideoConference();
                    statesVideoConference = _statesVideoConferenceRepository.GetMany(x => x.Description == "Aceptado").FirstOrDefault();
                    if (statesVideoConference != null)
                    {
                        svc = statesVideoConference.Id;
                    }


                  
                        try
                        {
                        if (request.Guests.Count > 0)
                        {
                            //Se recorre el arreglo de correos de usuarios internos
                            foreach (Entities.DTO.InvitedVideoConferenceDTO gis in request.Guests)
                            {
                                if (gis != null)
                                {

                                    Entities.DTO.InvitedVideoConferenceDTO gs = new Entities.DTO.InvitedVideoConferenceDTO();
                                    gs.Name = gis.Name;
                                    gs.Email = gis.Email;
                                    emails += gis.Email + ",";
                                    //emailsinvited+= gis.Email + ",";
                                    //Se agrega al lista para el envio de correo electronico 
                                    strList.Add(gis);
                                }
                            }
                        }
                            //Se verifica  si existen invitados externos y se recorre el arreglo de correos 
                         if (request.ExternalGuest != null && request.ExternalGuest.Count > 0)
                            {
                                foreach (Entities.DTO.InvitedVideoConferenceDTO ges in request.ExternalGuest)
                                {
                                    if (ges != null)
                                    {

                                        Entities.DTO.InvitedVideoConferenceDTO gse = new Entities.DTO.InvitedVideoConferenceDTO();
                                        gse.Name = ges.Name;
                                        gse.Email = ges.Email;
                                        //emails += ges.Email + ",";
                                        emailsExternal += ges.Email + ",";
                                        strList.Add(gse);
                                    }
                                }
                            }

                            //Verificar  el  estado  Aceptado  y cambiar Status
                            //videocon.StatusId = request.StatusId;
                            //string status = "Accepted";

                           if (svc==videocon.StatusId )
                            {
                                //Se recorre la  lista de correos electronicos  a enviar
                                if (strList != null)
                                {
                                    foreach (Entities.DTO.InvitedVideoConferenceDTO gest in strList)
                                    {
                                        Entities.DTO.InvitedVideoConferenceDTO c = new Entities.DTO.InvitedVideoConferenceDTO();
                                        c.Email = gest.Email;
                                       // emails += c.Email + ",";

                                        //Entities.Utils.InvitationMessage msg = new Entities.Utils.InvitationMessage(c.Email, "admin@sali.com.mx", "Invitación a video conferencia :" + request.Title, "MailCourseInvitationTemplate.html", "", "Video Conferencia de " + request.Title, "", c.Name, systemId);
                                        //msg.CourseLink = request.RoomVideoConference;// + "?codigo=" + sr.Codigo;
                                        //msg.Body = "Has sido invitado a formar parte De la videoconferencia el día  " + request.DateTimeVideoConference; //+ ", para confirmar tu inscripción haz click en el siguiente vínculo:";

                                        Entities.Utils.InvitationMessage msg = new Entities.Utils.InvitationMessage(c.Email, "admin@sali.com.mx", "Invitación a videoconferencia :" + request.Title, "MailCourseInvitationTemplate.html", "", "Videoconferencia de " + request.Title, "", c.Name, systemId);
                                        msg.CourseLink = request.RoomVideoConference;// + "?codigo=" + sr.Codigo;
                                        msg.Body = "Has sido invitado a formar parte de la videoconferencia el día  " + request.DateTimeVideoConference; //+ ", para confirmar tu inscripción haz click en el siguiente vínculo:";





                                    try
                                    {
                                            await _mailer.SendGroupInvitation(msg);
                                        }
                                        catch (Exception e)
                                        {
                                            result.Success = false;
                                            result.Message = $"{e.Message + e.InnerException.InnerException.Message}";
                                        }
                                    }

                                }
                            }
                           

                        }
                        catch (Exception e)
                        {
                            result.Success = false;
                            result.Message = $"{ e.Message }";
                        }

                    


                    if (emails != "")
                    {
                        emails = emails.Remove(emails.Length - 1);
                        videocon.GuestMail = emails;
                    }
                    else
                    {
                        videocon.GuestMail = string.Empty;
                    }
                    if (emailsExternal != "")
                    {
                        emailsExternal = emailsExternal.Remove(emailsExternal.Length - 1);
                        videocon.emailsExternal = emailsExternal;
                    }
                    else
                    {
                        videocon.emailsExternal = string.Empty;
                    }

                    _videoConferenceRepository.Add(videocon);
                    await this._unitOfWork.SaveChangesAsync();

                    result.Data = videocon;
                    result.Success = true;
                    result.Message = "Datos guardados correctamente";
                }
                else
                {
                    result.Message = "Este servicio no esta soportado para este sistema";
                }

              


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }


            return result;
        }
        /// <summary>
        /// Elimina un  registros de video conference
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns
        public async Task<Response> Delete(int id, int systemId)
        {
            var result = new Response();

            try
            {
                if (systemId == (int)EnumSystems.SALI)
                {
                    var videoconference = await _videoConferenceRepository.FindAsync(x => x.Id == id);

                    if (videoconference == null)
                        throw new Exception("No se encontraron datos de la video conferencia");



                    //if (videoconference.BookId > 0)
                    //    throw new Exception("La video conferencia tiene libros registrados");

                    _videoConferenceRepository.Delete(videoconference);
                    await _unitOfWork.SaveChangesAsync();

                    result.Success = true;
                    result.Message = "Registro eliminado correctamente";

                }
                else
                {
                    result.Message = "Este servicio no esta soportado para este sistema";
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: {ex.Message}";
            }

            return result;
        }
        /// <summary>
        /// Obtener todos los registros de video conferencia
        /// </summary>
        /// <returns></returns>
        public  async Task<Response> GetAll(int systemId, int UserId)
        {
            var response = new Response();
            string nameuser = "";
            string email = "";
            int id = 0;
            try
            {
                dynamic list;
               


                if (systemId == (int)EnumSystems.SALI)
                {
                    var video = await _videoConferenceRepository.GetAllAsync();
                    var user = _userRepository.GetById(UserId);

                    if (user == null)
                    {
                        throw new Exception("No se encontró el usuario");
                    }
                    else
                    {
                        id=user.Id;
                        nameuser = user.FullName;
                        email = user.Email;
                    }
                 

                        


                    if (video != null)
                    {
                        //var  videoconference = video.Select(v => new Entities.VideoConference
                        //{
                        //    Id = v.Id,
                        //    Title = v.Title,
                        //    Information = v.Information,
                        //    DateTimeVideoConference = v.DateTimeVideoConference,
                        //    Duration = v.Duration,
                        //    RoomVideoConference= v.RoomVideoConference,
                        //    StatusId = v.StatusId,
                        //    GuestMail=v.GuestMail,
                        //    Commentary = v.Commentary,
                        //    RegisterDate = v.RegisterDate,
                        //    UpdateDate = v.UpdateDate,
                        //    StudentGroupId = v.StudentGroupId,
                        //    CourseId = v.CourseId,
                        //    emailsExternal= v.emailsExternal,
                        //    UserId= v.UserId,



                        //});
                        var videoconference = video.Select(v => new
                        {
                             v.Id,
                             v.Title,
                             v.Information,
                             v.DateTimeVideoConference,
                             v.Duration,
                             v.RoomVideoConference,
                             v.StatusId,
                             v.GuestMail,
                             v.Commentary,
                             v.RegisterDate,
                             v.UpdateDate,
                             v.StudentGroupId,
                             v.CourseId,
                             v.emailsExternal,
                             v.UserId,
                             v.User.FullName
                        });

                        list = new
                        {
                            nameuser = user.FullName,
                            email = user.Email,
                            video = videoconference,
                            id = user.Id
                    };

                        response.Data = list;
                        response.Success = true;
                        response.Message = "Datos obtenidos correctamente";
                        
                    }
                }
                else
                {
                    response.Message = "Este servicio no esta soportado para este sistema";
                }



            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los cursos del profesio: { ex.Message} ";
            }

            return response;


        }


        /// <summary>
        /// Devuelve  los registros por el identificador principal
        /// </summary>
        /// <param name="id">Identificador de la  video conferencia</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var response = new Response();

            try
            {
                var videoConference = _videoConferenceRepository.GetById(id);

                if (videoConference != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = new Project.Entities.VideoConference
                    {
                        Id = videoConference.Id,
                        Title = videoConference.Title,
                        Information = videoConference.Information,
                        DateTimeVideoConference = videoConference.DateTimeVideoConference,
                        Duration = videoConference.Duration,
                        //Type = videoConference.Type,
                        //Invited = videoConference.Invited,
                        //BookId = videoConference.BookId,
                        //Accepted = videoConference.Accepted,
                        //rejected = videoConference.rejected,
                        //Editor = videoConference.Editor,
                        //EditorId = videoConference.EditorId,
                        StatusId = videoConference.StatusId,
                        //InvitedId = videoConference.InvitedId,
                        Commentary = videoConference.Commentary,
                        RegisterDate = videoConference.RegisterDate,
                        UpdateDate = videoConference.UpdateDate
                    };
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos del curso: { ex.Message} ";
            }

            return response;
        }

        /// <summary>
        /// Actuliza  un registro 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns
        public async Task<Response> Update(VideoConferenceUpdateRequest request,int systemId)
        {
            var result = new Response();
            string emails = "";
            string emailsExternal = "";
            int svc = 0;
            try {

                if (systemId == (int)EnumSystems.SALI)
                {
                    List<Entities.DTO.InvitedVideoConferenceDTO> strList = new List<Entities.DTO.InvitedVideoConferenceDTO>();

                    var videoconference = await _videoConferenceRepository.FindAsync(x => x.Id == request.Id);
                    if (videoconference == null) { throw new Exception("No se encontró registro"); }
                    else
                    {
                        videoconference.Title = request.Title;
                        videoconference.Information = request.Information;
                        videoconference.DateTimeVideoConference = request.DateTimeVideoConference;
                        videoconference.Duration = request.Duration;
                        videoconference.StatusId = request.StatusId;
                        videoconference.Commentary = request.Commentary;
                        videoconference.UpdateDate = DateTime.Now;
                        videoconference.RoomVideoConference = request.RoomVideoConference;
                        videoconference.StudentGroupId = request.StudentGroupId;
                        videoconference.CourseId = request.CourseId;
                        videoconference.emailsExternal = request.emailsExternal;


                        try
                        {
                            if (request.Guests.Count > 0)
                            {
                                //Se recorre el arreglo de correos de usuarios internos
                                foreach (Entities.DTO.InvitedVideoConferenceDTO gis in request.Guests)
                                {
                                    if (gis != null)
                                    {

                                        Entities.DTO.InvitedVideoConferenceDTO gs = new Entities.DTO.InvitedVideoConferenceDTO();
                                        gs.Name = gis.Name;
                                        gs.Email = gis.Email;
                                        emails += gis.Email + ",";
                                        //emailsinvited+= gis.Email + ",";
                                        //Se agrega al lista para el envio de correo electronico 
                                        strList.Add(gis);
                                    }
                                }
                            }
                            //Se verifica  si existen invitados externos y se recorre el arreglo de correos 
                            if (request.ExternalGuest != null && request.ExternalGuest.Count > 0)
                            {
                                foreach (Entities.DTO.InvitedVideoConferenceDTO ges in request.ExternalGuest)
                                {
                                    if (ges != null)
                                    {

                                        Entities.DTO.InvitedVideoConferenceDTO gse = new Entities.DTO.InvitedVideoConferenceDTO();
                                        gse.Name = ges.Name;
                                        gse.Email = ges.Email;
                                        //emails += ges.Email + ",";
                                        emailsExternal += ges.Email + ",";
                                        strList.Add(gse);
                                    }
                                }
                            }

                            //Verificar  el  estado  Aceptado  y cambiar Status
                            //videocon.StatusId = request.StatusId;
                            //string status = "Accepted";
                            Entities.StatesVideoConference statesVideoConference = new Entities.StatesVideoConference();
                            statesVideoConference = _statesVideoConferenceRepository.GetMany(x => x.Description == "Aceptado").FirstOrDefault();
                            if (statesVideoConference != null)
                            {
                                svc = statesVideoConference.Id;
                            }

                            if (svc == videoconference.StatusId)
                            {
                                //Se recorre la  lista de correos electronicos  a enviar
                                if (strList != null)
                                {
                                    foreach (Entities.DTO.InvitedVideoConferenceDTO gest in strList)
                                    {
                                        Entities.DTO.InvitedVideoConferenceDTO c = new Entities.DTO.InvitedVideoConferenceDTO();
                                        c.Email = gest.Email;
                                        // emails += c.Email + ",";

                                        Entities.Utils.InvitationMessage msg = new Entities.Utils.InvitationMessage(c.Email, "admin@sali.com.mx", "Invitación a videoconferencia :" + request.Title, "MailCourseInvitationTemplate.html", "", "Videoconferencia de " + request.Title, "", c.Name, systemId);
                                        msg.CourseLink = request.RoomVideoConference;// + "?codigo=" + sr.Codigo;
                                        msg.Body = "Has sido invitado a formar parte de la videoconferencia el día  " + request.DateTimeVideoConference; //+ ", para confirmar tu inscripción haz click en el siguiente vínculo:";

                                        try
                                        {
                                            await _mailer.SendGroupInvitation(msg);
                                        }
                                        catch (Exception e)
                                        {
                                            result.Success = false;
                                            result.Message = $"{e.Message + e.InnerException.InnerException.Message}";
                                        }
                                    }

                                }
                            }


                        }
                        catch (Exception e)
                        {
                            result.Success = false;
                            result.Message = $"{ e.Message }";
                        }




                        if (emails != "")
                        {
                            emails = emails.Remove(emails.Length - 1);
                            videoconference.GuestMail = emails;
                        }
                        else
                        {
                            videoconference.GuestMail = string.Empty;
                        }
                        if (emailsExternal != "")
                        {
                            emailsExternal = emailsExternal.Remove(emailsExternal.Length - 1);
                            videoconference.emailsExternal = emailsExternal;
                        }
                        else
                        {
                            videoconference.emailsExternal = string.Empty;
                        }


                        _videoConferenceRepository.Update(videoconference);
                        await _unitOfWork.SaveChangesAsync();

                        result.Data = videoconference.Id;
                        result.Success = true;
                        result.Message = "Datos actualizado correctamente";
                    }
                }
                else
                {
                    result.Message = "Este servicio no esta soportado para este sistema";
                }


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }


            return result;
        }

        /// <summary>
        /// Obtener todos los nodos registrados de tipo  libro
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetNodo()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _nodeRepository.GetAll()
                                                 .Select(u => new {
                                                     Id = u.Id,
                                                     Name = u.Name,
                                                     Description = u.Description,
                                                     ParentId = u.ParentId,
                                                     NodeTypeId = u.NodeTypeId,
                                                     BookId = u.BookId
                                                 }).Where(x=> x.NodeTypeId==5).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Agregar un nuevo registro 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="nodeRepository"></param>
        /// <returns></returns
        public async Task<Response> Add(VideoConferenceSaveRequest request, InvitedVideoConferenceSaveRequest invited, InvitedVideoConferenceSaveRequest externalguest)
        {
            var result = new Response();
            try
            {

                Entities.VideoConference videocon = new Entities.VideoConference();
                videocon.Title = request.Title;
                videocon.Information = request.Information;
                //videocon.DateTimeVideoConference = DateTime.Parse("2019-05-28 21:22:58.190");
                videocon.DateTimeVideoConference = request.DateTimeVideoConference;
                videocon.Duration = request.Duration;
                //videocon.Type = request.Type;
                //videocon.Invited = request.Invited;
                //videocon.Accepted = request.Accepted;
                //videocon.rejected = request.rejected;
                videocon.StatusId = request.StatusId;
                //videocon.InvitedId = request.InvitedId;
                videocon.RegisterDate = DateTime.Now;
                videocon.Commentary = request.Commentary;
                videocon.UpdateDate = DateTime.Now;


                //if (request.BookId > 0)
                //{
                //    videocon.BookId = request.BookId;
                //}
                //if (request.EditorId > 0)
                //{
                //    videocon.EditorId = request.EditorId;
                //}
                //if (request.Editor != null || request.Editor != "")
                //{
                //    videocon.Editor = request.Editor;
                //}
                _videoConferenceRepository.Add(videocon);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = videocon.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }


            return result;
        }

    }
}
