﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Report:IReport
    {
        private readonly IReportRepository _reportRepository;
        private readonly IUnitOfWork _unitOfWork;
       
        public Report(IReportRepository reportRepository, IUnitOfWork unitOfWor)
        { this._reportRepository = reportRepository;
            this._unitOfWork = unitOfWor;
        }
        /// <summary>
        /// Agrega un reporte
        /// </summary>
        /// <returns></returns>
        public async Task<Response> Add(ReportsRequest request)
        {
            var result = new Response();

            try
            {
                Entities.Reports report = new Entities.Reports();

                report.Description = request.Description;
                report.Active = request.Active;  
                _reportRepository.Add(report);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = report;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        /// <summary>
        /// elimina un registro de reporte
        /// </summary>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            var result = new Response();
            try
            {
                var report = await _reportRepository.FindAsync(x => x.Id == id);

                if (report == null)
                    throw new Exception("No se encontraron datos del reporte");

                _reportRepository.Delete(report);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Curso eliminado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Regresa  el registro de reporte
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _reportRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        /// <summary>
        /// Regresa  el registro de reporte por identificador
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _reportRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> Update(ReportsUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Reports report = new Entities.Reports();
                report.Id = request.Id;
                report.Description = request.Description;
                report.Active = request.Active;  
                _reportRepository.Update(report);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = report;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

       
    }
}
