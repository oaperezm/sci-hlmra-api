﻿using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using Project.Repositories.Courses;
using Project.Entities.Responses;

namespace Project.Domain.Management
{
    public class Course : ICourse
    {
        private readonly ICourseRepository _courseRepository;
        private readonly ICourseSectionRepository _courseSectionRepository;
        private readonly ICourseClassRepository _courseClassRepository;
        private readonly IUnitOfWork _unitOfWork;
        
        public Course(ICourseRepository courseRepository,
            ICourseSectionRepository courseSectionRepository,
            ICourseClassRepository courseClassRepository,
                       IUnitOfWork unitOfWor) {

            this._courseRepository = courseRepository;
            this._courseSectionRepository = courseSectionRepository;
            this._courseClassRepository = courseClassRepository;
            this._unitOfWork       = unitOfWor;            
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(CourseSaveRequest request)
        {
            var response = new Response();

            try
            {
                var courseAdd = new Project.Entities.Course
                {
                    Active       = true,
                    Description  = request.Description,
                    Name         = request.Name,
                    Gol          = request.Gol,
                    UserId       = request.UserId,
                    NodeId       = request.NodeId,
                    StartDate    = request.StartDate,
                    EndDate      = request.EndDate,
                    Monday       = request.Monday,
                    Tuesday      = request.Tuesday,
                    Wednesday    = request.Wednesday,
                    Thursday     = request.Thursday,
                    Friday       = request.Friday,
                    Subject      = request.Subject,
                    Saturday     = request.Saturday,
                    Sunday       = request.Sunday,
                    RegisterDate = DateTime.Now
                };

                _courseRepository.Add(courseAdd);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
                response.Data    = courseAdd.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo curso: { ex.Message }";
            }

            return response;
        }
        
        /// <summary>
        /// Obtiene todos los cursos registrados por un profesor
        /// </summary>
        /// <param name="userId">Identificador principal del profesor</param>
        /// <returns></returns>
        public async Task<Response> GetAllByIdUser(int userId)
        {
            var response = new Response();

            try
            {
                var courses = await _courseRepository.FindByAsyn(x => x.UserId == userId);

                if (courses != null) {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = courses.Select(c => new 
                    {
                        c.Id,
                        c.Description,
                        c.Active,
                        c.EndDate,
                        c.Gol,
                        c.Name,
                        c.NodeId,
                        c.Node.ParentId,
                        c.Subject,
                        c.Monday,
                        c.Tuesday,
                        c.Wednesday,
                        c.Thursday,
                        c.Friday,
                        c.Saturday,
                        c.Sunday,
                        c.StartDate,
                        c.UserId                         
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los cursos del profesio: { ex.Message} ";
            }

            return response;
        }

        /// <summary>
        /// Obtener todos los cursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var response = new Response();

            try
            {
                var courses =  _courseRepository.GetAll();

                if (courses != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = courses.Select(c => new Project.Entities.Course
                    {
                        Id          = c.Id,
                        Description = c.Description,
                        Active      = c.Active,
                        EndDate     = c.EndDate,
                        Gol         = c.Gol,
                        Name        = c.Name,
                        NodeId      = c.NodeId,
                        Subject     = c.Subject,
                        Monday      = c.Monday,
                        Tuesday     = c.Tuesday,
                        Wednesday   = c.Wednesday,
                        Thursday    = c.Thursday,
                        Friday      = c.Friday,
                        Saturday    = c.Saturday,
                        Sunday      = c.Sunday,
                        StartDate   = c.StartDate,
                        UserId      = c.UserId
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los cursos del profesio: { ex.Message} ";
            }

            return response;
        }

        /// <summary>
        /// Obtener los datos de un curso por el identificador principal
        /// </summary>
        /// <param name="id">Identificador del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var response = new Response();

            try
            {
                var course = _courseRepository.GetById(id);

                if (course != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data =  new Project.Entities.Course
                    {
                        Id          = course.Id,
                        Description = course.Description,
                        Active      = course.Active,
                        EndDate     = course.EndDate,
                        Gol         = course.Gol,
                        Name        = course.Name,
                        NodeId      = course.NodeId,
                        Subject     = course.Subject,                        
                        Monday      = course.Monday,
                        Tuesday     = course.Tuesday,
                        Wednesday   = course.Wednesday,
                        Thursday    = course.Thursday,
                        Friday      = course.Friday,
                        Saturday    = course.Saturday,
                        Sunday      = course.Sunday,
                        StartDate   = course.StartDate,
                        UserId      = course.UserId
                    };
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos del curso: { ex.Message} ";
            }

            return response;
        }
        
        /// <summary>
        /// Método para actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseUpdateRequest request)
        {
            var response = new Response();

            try
            {
                var courseUpdate = _courseRepository.GetById(request.Id);

                if (courseUpdate == null)
                    throw new Exception("No se encontraron datos del curso proporcionado");

                courseUpdate.Active      = request.Active;
                courseUpdate.Description = request.Description;
                courseUpdate.Name        = request.Name;
                courseUpdate.Gol         = request.Gol;                
                courseUpdate.NodeId      = request.NodeId;
                courseUpdate.StartDate   = request.StartDate;
                courseUpdate.EndDate     = request.EndDate;
                courseUpdate.Subject     = request.Subject;
                courseUpdate.Monday      = request.Monday;
                courseUpdate.Tuesday     = request.Tuesday;
                courseUpdate.Wednesday   = request.Wednesday;
                courseUpdate.Thursday    = request.Thursday;
                courseUpdate.Friday      = request.Friday;                
                courseUpdate.Saturday    = request.Saturday;
                courseUpdate.Sunday      = request.Sunday;
                courseUpdate.UpdateDate  = DateTime.Now;
                
                _courseRepository.Update(courseUpdate);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo curso: { ex.Message }";
            }

            return response;
        }


        public Task<Response> Delete(int id)
        {
            throw new NotImplementedException();
        }

    }
}
