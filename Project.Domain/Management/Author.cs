﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Author : IAuthor
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Author(IAuthorRepository authorRepository, IUnitOfWork unitOfWor)
        {
            _authorRepository = authorRepository;
            _unitOfWork       = unitOfWor;
        }

        /// <summary>
        /// Agregar un nuevo autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddAuthor(AuthorSaveRequest request)
        {
            var result = new Response();

            try
            {
                var author      = new Entities.Author();
                author.FullName = request.FullName;
                author.FullName = request.FullName;
                author.Comment  = request.Comment;
                author.LastName = request.LastName;

                _authorRepository.Add(author);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = new Entities.Author {
                        Id       = author.Id,
                        FullName = author.FullName,
                        Comment  = author.Comment,
                        LastName = author.LastName
                };
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un autor registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAuthor(int id)
        {
            var result = new Response();

            try
            {
                var author = await _authorRepository.FindAsync(x => x.Id == id);

                if (author.Books.Count > 0)
                    throw new Exception("No se puede eliminar el autor ya que esta relacionado a uno o más libros");

                _authorRepository.Delete(author);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Autor eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el autor: { ex.Message }";
            }

            return result;
        }
        
        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll() {

            var result = new Response();

            try
            {
                var data = await _authorRepository.GetAllAsync();
                result.Data = data.OrderBy( x => x.FullName)
                                  .Select(x => new AuthorDTO {
                                        Id       = x.Id,                                       
                                        Comment  = x.Comment,
                                        LastName = x.LastName,
                                        Name     = x.FullName                                        
                                    }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Problemas al obtener los autores";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter)
        {
            var result = new ResponsePagination();           

            try
            {
                var pageData = default(List<Entities.Author>);
                var data  = await _authorRepository.GetAllAsync();

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data   = data.Where(x => x.FullName.ToLower().Contains(filter) || x.LastName.ToLower().Contains(filter)).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.LastName)
                                      .Skip((curentPage - 1) * result.Pagination.PageSize)
                                      .Take(result.Pagination.PageSize)
                                      .ToList();

                result.Data = pageData.OrderBy( x => x.FullName )
                                          .Select(x => new Entities.Author
                                            {
                                                Id       = x.Id,
                                                FullName = x.FullName,
                                                Comment  = x.Comment,
                                                LastName = x.LastName,
                                                Books = x.Books.Select(a => new Entities.Book
                                                {
                                                    Id = a.Id,
                                                    Description = a.Description,
                                                    Name = a.Name,
                                                    ISBN = a.ISBN,
                                                    Codebar = a.Codebar,
                                                    Edition = a.Edition,
                                                    Active = a.Active
                                                }).ToList()
                                            }).ToList();                

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";                
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los datos de un autor por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = await _authorRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateAuthor(AuthorUpdateRequest request)
        {
            var result = new Response();

            try
            {
                var author = await _authorRepository.FindAsync(x => x.Id == request.Id);

                if (author == null)
                    throw new Exception("Autor no encontrado");
                               
                author.FullName = request.FullName;
                author.LastName = request.LastName;
                author.Comment  = request.Comment;

                _authorRepository.Update(author);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = author.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}