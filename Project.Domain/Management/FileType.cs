﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class FileType : IFileType
    {
        private readonly IFileTypeRepository _FileTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FileType(IFileTypeRepository FileTypeRepository, IUnitOfWork unitOfWor)
        {

            this._FileTypeRepository = FileTypeRepository;
            this._unitOfWork = unitOfWor;
        }

        /// <summary>
        /// Agregar un tipo de archivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddFileType(FileTypeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.FileType FileType = new Entities.FileType();

                FileType.Description = request.Description;

                _FileTypeRepository.Add(FileType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = FileType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un tipo de archivo
        /// </summary>
        /// <param name="FileTypeId"></param>
        /// <returns></returns>
        public Task<Response> DeleteFileType(int FileTypeId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtener todos los tipos de archivos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll(int systemId)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _FileTypeRepository.FindAllAsync( x=> x.SystemId == systemId))
                                .OrderBy( x=> x.Description)
                                .Select(u => new {
                                    Id          = u.Id,
                                    Description = u.Description
                                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener un tipo de archivo por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _FileTypeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un tipo de archivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateFileType(FileTypeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.FileType FileType = new Entities.FileType();

                FileType.Id = request.Id;
                FileType.Description = request.Description;

                _FileTypeRepository.Update(FileType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = FileType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}