﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Section: ISection
    {
        private readonly ISectionsRepository _sectionRepository;
        private readonly IUnitOfWork _unitOfWork;
        public Section(ISectionsRepository sectionsRepository, IUnitOfWork unitOfWor)
        {
            this._sectionRepository = sectionsRepository;
            this._unitOfWork = unitOfWor;
        }
        public async Task<Response> Add(SectionsRequest request)
        {
            var result = new Response();

            try
            {
                Entities.Sections sections = new Entities.Sections();

                sections.Description = request.Description;
                sections.Active = request.Active;

                _sectionRepository.Add(sections);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = sections;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> Delete(int Id)
        {
            var response = new Response();

            try
            {
                var sections = _sectionRepository.GetById(Id);
               

                _sectionRepository.Delete(sections);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al eliminar los datos: { ex.Message } ";
            }

            return response;
        }


        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _sectionRepository.GetAllAsync())
                    .OrderBy(x => x.Id)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _sectionRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }


        public async Task<Response> Update(SectionsUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Sections sections = new Entities.Sections();


                sections.Id = request.Id;
                sections.Description = request.Description;
                sections.Active = request.Active;
               
                _sectionRepository.Update(sections);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = sections;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

    }
}
