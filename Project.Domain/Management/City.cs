﻿using Project.Domain.Core;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class City: ICity
    {
        private readonly IStateRepository _stateRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IUnitOfWork _unitOfWork;

        public City(IStateRepository stateRepository,
                    ICityRepository cityRepository,
                    IUnitOfWork unitOfWork) {

            _cityRepository  = cityRepository;
            _stateRepository = stateRepository;
            _unitOfWork      = unitOfWork;
        }

        /// <summary>
        /// Obtener todos el catálogo de ciudades
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll() {

            var result = new Response();

            try
            {
                var data    = await _cityRepository.GetAllAsync();
                result.Data = data.OrderBy( x=> x.Description )
                                  .Select(x => new Entities.City
                                            {
                                                Id          = x.Id,
                                                Description = x.Description
                                            }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los estados de una ciudad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetStateByCity(int id) {

            var result = new Response();

            try
            {
                var data = await _cityRepository.FindAsync( x=> x.Id == id);
                result.Data = data.States
                                  .OrderBy( x => x.Name)
                                  .Select(x => new Entities.State
                                  {
                                      Id   = x.Id,
                                      Name = x.Name
                                  }).ToList();

                result.Success = true;
                result.Message = "Estados de la ciudad obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message}";
            }

            return result;

        }
    }
}
