﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Area : IArea
    {
        private readonly IAreaRepository _AreaRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Area(IAreaRepository AreaRepository, IUnitOfWork unitOfWor)
        {

            this._AreaRepository = AreaRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddArea(AreaSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Area Area = new Entities.Area();

                Area.Description = request.Description;

                _AreaRepository.Add(Area);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Area;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteArea(int AreaId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _AreaRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _AreaRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateArea(AreaUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Area Area = new Entities.Area();

                Area.Id = request.Id;
                Area.Description = request.Description;

                _AreaRepository.Update(Area);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Area;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}