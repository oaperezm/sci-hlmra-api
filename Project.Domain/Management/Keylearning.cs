﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Keylearning: IKeylearning
    {
        private readonly IKeylearningRepository _keylearningRepository;
        private readonly IEducationLevelRepository _educationLevelRepository;
        private readonly IUnitOfWork _unitOfWork;
        public Keylearning(IKeylearningRepository keylearningRepository, IUnitOfWork unitOfWor, IEducationLevelRepository educationLevelRepository)
        {
            this._keylearningRepository = keylearningRepository;
            this._unitOfWork = unitOfWor;
            this._educationLevelRepository = educationLevelRepository;
        }
        public async Task<Response> Add(KeylearningRequest request)
        {
            var result = new Response();
            try
            {
                Entities.Keylearning keylearning = new Entities.Keylearning();

                keylearning.Description = request.Description;
                keylearning.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    keylearning.EducationLevelId = request.EducationLevelId;
                }
                _keylearningRepository.Add(keylearning);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = keylearning;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public Task<Response> Delete(int Id)
        {
            throw new NotImplementedException();
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _keylearningRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active,
                        EducationLevelId= u.EducationLevelId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _keylearningRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> Update(KeylearningUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Keylearning keylearning = new Entities.Keylearning();

                keylearning.Id = request.Id;
                keylearning.Description = request.Description;
                keylearning.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    keylearning.EducationLevelId = request.EducationLevelId;
                }

                _keylearningRepository.Update(keylearning);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = keylearning;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
    }
}
