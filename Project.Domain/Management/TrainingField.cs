﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class TrainingField : ITrainingField
    {
        private readonly ITrainingFieldRepository _TrainingFieldRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TrainingField(ITrainingFieldRepository TrainingFieldRepository, IUnitOfWork unitOfWor)
        {

            this._TrainingFieldRepository = TrainingFieldRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddTrainingField(TrainingFieldSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.TrainingField TrainingField = new Entities.TrainingField();

                TrainingField.Description = request.Description;

                _TrainingFieldRepository.Add(TrainingField);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = TrainingField;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteTrainingField(int TrainingFieldId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _TrainingFieldRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _TrainingFieldRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateTrainingField(TrainingFieldUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.TrainingField TrainingField = new Entities.TrainingField();

                TrainingField.Id = request.Id;
                TrainingField.Description = request.Description;

                _TrainingFieldRepository.Update(TrainingField);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = TrainingField;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}