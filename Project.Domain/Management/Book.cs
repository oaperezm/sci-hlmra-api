﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Book : IBook
    {
        private readonly IBookRepository _BookRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Book(IBookRepository bookRepository,
                    IAuthorRepository authorRepository,
                    INodeRepository nodeRepository,
                    IUnitOfWork unitOfWor)
        {

            _BookRepository   = bookRepository;
            _unitOfWork       = unitOfWor;
            _authorRepository = authorRepository;
            _nodeRepository   = nodeRepository;
        }

        /// <summary>
        /// Agregar un nuebo libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddBook(BookSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Book book = new Entities.Book();

                book.Description  = request.Description;
                book.Codebar      = request.Codebar;
                book.Description  = request.Description;
                book.Edition      = request.Edition;
                book.ISBN         = request.ISBN;
                book.Name         = request.Name;                
                book.RegisterDate = DateTime.Now;
                book.Active       = request.Active;

                if (request.AuthorId.Count > 0) {
                    var authors = await _authorRepository.FindAllAsync(x => request.AuthorId.Contains(x.Id));

                    if (authors != null) {
                        authors.ToList().ForEach(a => { book.Authors.Add(a);});
                    }
                }                
                
                _BookRepository.Add(book);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = book.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un libro existente
        /// </summary>
        /// <param name="BookId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteBook(int bookId)
        {
            var result = new Response();

            try
            {
                var book = await _BookRepository.FindAsync(b => b.Id == bookId);

                if (book == null)
                    throw new Exception("No se encontró el libro");


                var bookAssigne = await _nodeRepository.FindAllAsync(x => x.BookId == bookId);

                if(bookAssigne.Count > 0)
                    throw new Exception("El libro se encuentra asignado a un elemento de la estructura");
                
                _BookRepository.Delete(book);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Libro eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el libro: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los libros activos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll() {
            
            var result = new Response();

            try
            {
                var data = await _BookRepository.FindAllAsync( b => b.Active);
                result.Data = data.OrderBy(b=> b.Name)
                                  .Select(b => new Entities.Book
                                    {
                                        Id          = b.Id,
                                        Edition     = b.Edition,
                                        Name        = b.Name,
                                        ISBN        = b.ISBN,
                                        Codebar     = b.Codebar,
                                        Description = b.Description,
                                        Authors = b.Authors.Select( a => new Entities.Author {
                                            Id = a.Id,
                                           FullName = a.FullName
                                        }).ToList()
                                    });

                result.Success = true;
                result.Message = "Libro obtenidos correctamente";
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los libros";
            }

            return result;

        }


        /// <summary>
        /// Obtener todos los libros registrados.
        /// </summary>
        /// <param name="justActive"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter)
        {
            var result = new ResponsePagination();

            try
            {      
                var pageData = default(List<Entities.Book>);
                var data     = await _BookRepository.GetAllAsync();

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data   = data.Where(x => x.Name.ToLower().Contains(filter)
                                          || x.Authors.Any(a => a.FullName.ToLower().Contains(filter)))
                                 .ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy( x => x.Name) 
                               .Skip((curentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => new Entities.Book
                    {
                        Id          = u.Id,
                        Active      = u.Active,
                        Codebar     = u.Codebar,
                        ISBN        = u.ISBN,
                        Name        = u.Name,
                        Description = u.Description,
                        Edition     = u.Edition,
                        Authors     = u.Authors.Select(a => new Entities.Author
                        {
                            Id       = a.Id,
                            FullName = a.FullName,
                        }).ToList()                        
                    }).ToList();


                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
               
        /// <summary>
        /// Obtener los datos de un libro por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _BookRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un libro existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateBook(BookUpdateRequest request)
        {
            var result = new Response();
            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                var book = await _BookRepository.FindAsync(x => x.Id == request.Id);

                if (book == null)
                    throw new Exception("No se encontró el libro");

                //book.Description = request.Description;
                book.Description = request.Description;
                book.Codebar = request.Codebar;
                book.Description = request.Description;
                book.Edition = request.Edition;
                book.ISBN = request.ISBN;
                book.Name = request.Name;
                book.UpdateDate = DateTime.Now;

                if (request.AuthorId.Count > 0)
                {
                    book.Authors.Clear();
                    var authors = await _authorRepository.FindAllAsync(x => request.AuthorId.Contains(x.Id));

                    if (authors != null)
                        authors.ToList().ForEach(a => { book.Authors.Add(a); });
                }

                _BookRepository.Update(book);

                var nodes = await _nodeRepository.FindAllAsync(p => p.BookId == request.Id);
                foreach (var node in nodes)
                {
                    node.Name = book.Name;
                    node.Description = book.Name;
                    _nodeRepository.Update(node);
                }
                await this._unitOfWork.SaveChangesAsync();

                result.Data = book.Id;
                result.Success = true;
                result.Message = "Datos actualizados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }
            return result;
        }

        
    }
}