﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Entities.Views;
using Project.Repositories.Core;
using Project.Repositories.Management;
using Project.Repositories.Services;
using Project.Repositories.Views;

namespace Project.Domain.Management
{
    public class Node : INode
    {
        private readonly IUserRepository _userRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IVNodesTotalFileTypesRepository _vNodeTotalRepository;
        private readonly IVNodesTotalFileTypesRARepository _vNodeTotalRARepository;
        private readonly INodeContentRepository _nodeContentRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IPublishingProfileRepository _publishingProfileRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="nodeRepository"></param>
        /// <param name="nodeContentRepository"></param>
        /// <param name="vNodeTotalRepository"></param>
        /// <param name="unitOfWor"></param>
        /// <param name="contentRepository"></param>
        /// <param name="publishingProfileRepository"></param>
        /// <param name="tagRepository"></param>
        /// <param name="userRepository"></param>
        /// <param name="fileUploader"></param>
        public Node(INodeRepository nodeRepository,
                    INodeContentRepository nodeContentRepository,
                    IVNodesTotalFileTypesRepository vNodeTotalRepository,
                    IVNodesTotalFileTypesRARepository vNodeTotalRARepository,
                    IUnitOfWork unitOfWor,
                    IContentRepository contentRepository,
                    IPublishingProfileRepository publishingProfileRepository,
                    IUserRepository userRepository,
                    IFileUploader fileUploader) {

            _nodeRepository = nodeRepository;
            _contentRepository = contentRepository;
            _nodeContentRepository = nodeContentRepository;
            _publishingProfileRepository = publishingProfileRepository;
            _userRepository = userRepository;
            _vNodeTotalRepository = vNodeTotalRepository;
            _unitOfWork = unitOfWor;
            _vNodeTotalRARepository = vNodeTotalRARepository;
            _fileUploader = fileUploader;

        }

        #region METHODS

        /// <summary>
        /// ´Método que permite registrar los datos d eun nuevo nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddNode(NodeSaveRequest request)
        {
            var result = new Response();
            string fileUrl = string.Empty;

            try
            {
                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, "node");

                Entities.Node node = new Entities.Node();

                // Agregar valores para optimizar la búsqueda en nodos
                if (request.ParentId > 0)
                {
                    var parentNode = await _nodeRepository.FindAsync(x => x.SystemId == request.SystemId && x.Id == request.ParentId);
                    var childs     = await _nodeRepository.FindAllAsync(x => x.SystemId == request.SystemId &&  x.ParentId == request.ParentId);
                    var lastChild  = childs.OrderBy(x => x.Depth).LastOrDefault();

                    node.Depth = parentNode.Depth + 1;

                    if (lastChild != null)                         
                        node.Pathindex = lastChild.Pathindex + 1;                                            
                    else 
                        node.Pathindex = 1;                    

                    node.Numericalmapping = $"{ parentNode.Numericalmapping }.{ node.Pathindex }";
                }
                
                node.Name        = request.Name;
                node.Description = request.Description;
                node.ParentId    = request.ParentId;
                node.Active      = request.Active;
                node.NodeTypeId  = request.NodeTypeId;
                node.SystemId    = request.SystemId;
                node.Order = request.Order;

                if (request.BookId > 0)
                    node.BookId = request.BookId;

                if (!string.IsNullOrEmpty(fileUrl))
                    node.UrlImage = fileUrl;

                _nodeRepository.Add(node);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = node;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de un nodo en especifico que no tenga hijos ni 
        /// contenido relacionado
        /// </summary>
        /// <param name="NodeId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteNode(int nodoId)
        {
            var response = new Response();

            try
            {
                var node = _nodeRepository.GetById(nodoId);
                var nodeContent = (await _nodeContentRepository.FindAllAsync(x => x.Content != null && x.NodeId == node.Id )).ToList();

                if (nodeContent != null && nodeContent.Where(x=> x.Content != null ).Count() > 0) {
                    throw new Exception(" No se puede eliminar el nodo ya que tiene contenido asignado ");
                }

                _nodeRepository.Delete(node);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al eliminar los datos: { ex.Message } ";
            }

            return response;
        }

        /// <summary>
        /// Obtener todos los nodos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _nodeRepository.GetAll()
                                                 .Select(u => new {
                                                     Id = u.Id,
                                                     Name = u.Name,
                                                     Description = u.Description,
                                                     UrlImage = u.UrlImage,
                                                     Active = u.Active,
                                                     ParentId = u.ParentId,
                                                     NodeTypeId = u.NodeTypeId,
                                                     NodeTypeDesc = u.NodeType.Description,
                                                     BookId = u.BookId,
                                                     Order = u.Order
                                                 }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que obtiene los datos de un nodo en especifico
        /// </summary>
        /// <param name="id">Identificador del nodo</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();    

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _nodeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para actualizar los datos de un nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateNode(NodeUpdateRequest request)
        {
            var result = new Response();
            string fileUrl = string.Empty;

            try
            {
                var node = _nodeRepository.GetById(request.Id);

                if (node == null)
                    throw new Exception("No se encontró el nodo");

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, "node");

                node.Id = request.Id;
                node.Name = request.Name;
                node.Description = request.Description;
                node.ParentId = request.ParentId;
                node.Active = request.Active;
                node.NodeTypeId = request.NodeTypeId;
                node.Order = request.Order;

                if (request.BookId > 0)
                    node.BookId = request.BookId;

                if (!string.IsNullOrEmpty(fileUrl))
                    node.UrlImage = fileUrl;

                _nodeRepository.Update(node);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = new Project.Entities.Node
                {
                    Name = node.Name,
                    Description = node.Description,
                    UrlImage = node.UrlImage,
                    ParentId = node.ParentId,
                    Active = node.Active,
                    NodeTypeId = node.NodeTypeId
                };

                result.Success = true;
                result.Message = "Datos guardados correctamente. <br/>" + "Para visualizar el cambio de orden del nodo, es necesario reingresar a la pantalla de edición de la estructura.";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que obtiene todos los nodos hijos de un nodo padre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetSubnodes(int id, int systemId)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var parent = await _nodeRepository.FindAsync(x => x.Id == id);
                var children = _nodeRepository.GetAll()
                                                .Where(n => n.ParentId == id && n.SystemId == systemId)
                                                .Select(u => new {
                                                    Id = u.Id,
                                                    Name = u.Name,
                                                    Description = u.Description,
                                                    UrlImage = u.UrlImage,
                                                    Active = u.Active,
                                                    ParentId = u.ParentId,
                                                    NodeTypeId = u.NodeTypeId,
                                                    NodeTypeDesc = u.NodeType.Description,
                                                    Order = u.Order
                                                })
                                                 .OrderBy(x => x.Name)
                                                 .ToList();
                result.Data = new
                {
                    children = children,
                    parent = new Project.Entities.Node {
                        Id = (parent == null ? 0 : parent.Id),
                        Description = parent?.Description,
                        Name = parent?.Name,
                        UrlImage = parent?.UrlImage
                    }
                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Get node's associated contents
        /// </summary>
        /// <param name="id">NodeId</param>
        /// <returns>A list of contents associated with the node</returns>
        public async Task<Response> GetNodeContents(int id, int rolId)
        {
            var result = new Response();

            try
            {
                // result.Data = await _NodeContentRepository.FindAllAsync(r => r.NodeId.Equals(id));
                // var contenNode = await _nodeContentRepository.FindAllAsync(r => r.NodeId == id);
                var node = await _nodeRepository.FindAsync(x => x.Id == id);
                var parent = default(Entities.Node);
                var data = default(List<Entities.NodeContent>);

                if (node.ParentId > 0 && rolId != (int)EnumRol.Student)
                {
                    parent = await _nodeRepository.FindAsync(x => x.Id == node.ParentId);
                    data = parent.Contents.Union(node.Contents).ToList();
                }
                else {
                    data = node.Contents.ToList();
                }

                result.Data = data.Distinct()
                                  .Select(u => u.New())
                                  .ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que permite eliminar la imagen de un nodo
        /// </summary>
        /// <param name="id">Identificador del nodo</param>
        /// <returns></returns>
        public async Task<Response> RemoveImage(int id) {

            var result = new Response();

            try
            {
                var node = _nodeRepository.GetById(id);

                if (!String.IsNullOrEmpty(node.UrlImage))
                {
                    var res = await _fileUploader.FileRemove(node.UrlImage, "node");

                    if (res) {

                        node.UrlImage = null;

                        _nodeRepository.Update(node);
                        _unitOfWork.SaveChanges();
                    }

                    result.Success = true;
                    result.Message = "Imagen eliminada correctamente";
                }
                else
                {
                    result.Success = false;
                    result.Message = "El nodo no tiene imagen asignada";
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error remover la imagen: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Método para obtener todos los nodos hijos de un padre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetNodeStructureByParentId(int id, bool fullData = false)
        {
            var response = new Response();

            try
            {
                var nodes = (await _nodeRepository.GetAllAsync()).ToList();
                var parent = nodes.FirstOrDefault(x => x.Id == id);
                var structure = new List<Project.Entities.Node>();

                this.RecursiveChildren(nodes, parent, ref structure);

                if (!structure.Exists(x => x.Id == parent.Id))
                    structure.Add(new Entities.Node
                    {
                        Id = parent.Id,
                        Name = parent.Name,
                        Description = parent.Description,
                        UrlImage = parent.UrlImage,
                        Active = parent.Active,
                        ParentId = parent.ParentId,
                        NodeTypeId = parent.NodeTypeId,
                    });

                structure = structure.OrderBy(s => s.Order).ThenBy(s => s.ParentId).ToList();

                
                    response.Data = structure.Select(u => new Entities.Node
                    {
                        Id          = u.Id,
                        Name        = u.Name,
                        Description = u.Description,
                        UrlImage    = u.UrlImage,
                        Active      = u.Active,
                        ParentId    = u.ParentId,
                        NodeTypeId  = u.NodeTypeId,
                        BookId      = u.BookId,
                        Order       = u.Order,
                        // Contents    = u.Contents.Select( c => new Entities.NodeContent { Id = c.Id }).ToList(),
                        Book = !u.BookId.HasValue ? null : new Entities.Book
                        {
                            Name = u.Book.Name,
                            ISBN = u.Book.ISBN,
                            Codebar = u.Book.Codebar,
                            Description = u.Book.Description,
                            Id = u.Book.Id,
                            Authors = u.Book.Authors.Count == 0 ? new List<Entities.Author>() :
                                                                    u.Book.Authors.Select(a => new Entities.Author
                                                                    {
                                                                        Id = a.Id,
                                                                        FullName = a.FullName
                                                                    }).ToList()
                        }
                    }).ToList();
                
               

                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al intentar de obtener los datos: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Obtener todos los libros en base a un nodo
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetBooksByNodeId(int nodeId, int currentPage, int sizePage, int userId) {

            var resultPagination = new ResponsePagination();

            try
            {
                var nodeParent = await _nodeRepository.FindAsync(x => x.Id == nodeId);
                var books = new List<Entities.Node>();
                var userNodes = default(List<Entities.Node>);

                if (nodeParent.ParentId == 0)
                    books = (await _nodeRepository.FindAllAsync(n => n.NodeTypeId == (int)EnumNodeType.Book))
                            .Select(n => new Entities.Node
                            {
                                Id = n.Id,
                                Name = n.Name.Trim(),
                                Description = n.Description,
                                UrlImage = n.UrlImage,
                                Active = n.Active,
                                ParentId = n.ParentId,
                                NodeTypeId = n.NodeTypeId,
                            }).ToList();
                else {

                    var data = await _nodeRepository.FindAllAsync(x => x.Numericalmapping.StartsWith(nodeParent.Numericalmapping) && x.NodeTypeId == (int)EnumNodeType.Book);
                    books = data.Select(n => new Entities.Node
                    {
                        Id = n.Id,
                        Name = n.Name.Trim(),
                        Description = n.Description,
                        UrlImage = n.UrlImage,
                        Active = n.Active,
                        ParentId = n.ParentId,
                        NodeTypeId = n.NodeTypeId,
                    }).ToList();

                }

                resultPagination.Pagination.SetData(books.Count, currentPage, sizePage);

                var nodesBook = books.OrderBy(x => x.Name)
                                     .Skip((currentPage - 1) * resultPagination.Pagination.PageSize)
                                     .Take(resultPagination.Pagination.PageSize)
                                     .ToList();

                if (userId > 0)
                    userNodes = await this.GetUserNodes(userId);


                if (userNodes != null && userNodes.Count > 0)
                {
                    nodesBook.ForEach(x =>
                    {
                        var node = userNodes.FirstOrDefault(n => n.Id == x.Id);

                        if (node != null)
                            x.NodeAccessTypeId = node.NodeAccessTypeId;
                        else
                            x.NodeAccessTypeId = (int)EnumNodeAccessType.Private;
                    });
                }

                resultPagination.Data = nodesBook;
                resultPagination.Success = true;
                resultPagination.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                resultPagination.Success = false;
                resultPagination.Message = "Error al obtener los datos";
            }

            return resultPagination;

        }
            
        /// <summary>
        /// Obtener el inventario de archivos por nodo
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetInventoryFiles( int id, int systemId ) {

            var response = new Response();

            try
            {
                var nodes = new List<VNodesTotalFileTypes>();

                if(systemId == (int)EnumSystems.SALI) { 
                    var nodesViews  = (await _vNodeTotalRepository.GetAllAsync()).ToList();
                    nodes = nodesViews.Select(x => new VNodesTotalFileTypes
                    {
                        Active       = x.Active,
                         Audios      = x.Audios,
                         Description = x.Description,
                         Documents   = x.Documents,
                         Id          = x.Id,
                         Images      = x.Images,
                         Name        = x.Name,
                         NodeTypeId  = x.NodeTypeId,
                         ParentId    = x.ParentId,
                         UrlImage    = x.UrlImage,
                         Videos      = x.Videos
                    }).ToList();

                } else {

                    var nodesViews = (await _vNodeTotalRARepository.GetAllAsync()).ToList();
                    nodes = nodesViews.Select(x => new VNodesTotalFileTypes
                    {
                        Active = x.Active,
                        Audios = x.Audios,
                        Description = x.Description,
                        Documents = x.Documents,
                        Id = x.Id,
                        Images = x.Images,
                        Name = x.Name,
                        NodeTypeId = x.NodeTypeId,
                        ParentId = x.ParentId,
                        UrlImage = x.UrlImage,
                        Videos = x.Videos
                    }).ToList();
                }

                var childs    = nodes.Where(x => x.ParentId == id);
                var parent    = nodes.FirstOrDefault(x => x.Id == id);
                                
                parent.TotalContent = new CountContentDTO
                {
                    TotalVideo    = parent.Videos,
                    TotalAudio    = parent.Audios,
                    TotalImage    = parent.Images,
                    TotalDocument = parent.Documents
                };

                foreach (var child in childs) {
                    CountContentDTO countFiles = new CountContentDTO(); ;
                    this.RecursiveChildrenSum(nodes, child, ref countFiles);
                    child.TotalContent = countFiles;
                }

                var data = childs.ToList();
                data.Add(parent);
                                
                response.Data = data.OrderBy( x=> x.Id)
                                    .OrderByDescending( x=> x.ParentId)
                                    .Select(u => new NodeInventoryDTO
                {
                  Id           = u.Id,
                  Name         = u.Name,
                  Description  = u.Description,
                  UrlImage     = u.UrlImage,
                  Active       = u.Active,
                  ParentId     = u.ParentId,
                  NodeTypeId   = u.NodeTypeId,
                  TotalContent = u.TotalContent
                }).ToList();
                
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al intentar de obtener los datos: { ex.Message }";
            }

            return response;
        }
        
        /// <summary>
        /// Obtener los contenidos asignado a los nodos hijos , filtrado y paginado
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="systemId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetNodeContentRecursive( int nodeId,
            List<int> fileTypeId, 
            int purposeId,
            int blockId,
            int formativeFieldId,
            int currentPage, 
            int sizePage, 
            int systemId,
            string filter, int rolId
           ,int EducationLevelId
           , int AreaPersonalSocialDevelopmentId,
            int AreasCurriculumAutonomyId,
            int AxisId, int KeylearningId,
            int LearningexpectedId)
        {
            
            var result = new ResponsePagination();

            try
            {
                var contents   = new List<Entities.Content>();
                var filesTypes = new List<Entities.FileType>();
                var files      = new List<Entities.NodeContent>();

                var nodeParent = await _nodeRepository.FindAsync(x => x.Id == nodeId);
                var structure  = await _nodeRepository.FindAllAsync(x => x.Numericalmapping.StartsWith(nodeParent.Numericalmapping) && x.SystemId == systemId);
                var nodeIds    = structure.Select(a => a.Id).ToList();

                // VALIDAR SI EL ROL DEL usUARIO ES MAESTRO PARA FILTRAR LOS 
                // TIPOS DE ARCHIVOS                
                if (rolId == (int)EnumRol.TeacherRA || rolId == (int)EnumRol.AdministratorRA)
                    files = (await _nodeContentRepository.FindAllAsync(x => nodeIds.Contains(x.NodeId.Value))).ToList();
                else if ( rolId == (int)EnumRol.StudentRA )
                    files = (await _nodeContentRepository.FindAllAsync(x => nodeIds.Contains(x.NodeId.Value)
                                                                            && (x.Content.FileTypeId != 11 && x.Content.FileTypeId != 16))).ToList();                
                
                var data = files.Where(x => x.Content != null)
                                .Select(x => x.Content)                                      
                                .ToList();             
                
                // OBTENIDOS TIPOS DE ARCHIVOS
                files.Where(x => x.Content != null).ToList().ForEach(f =>
                {
                    if(!filesTypes.Any( x => x.Id == f.Content.FileType.Id))
                    {
                        filesTypes.Add(new Entities.FileType
                        {
                            Description = f.Content.FileType.Description,
                            Id          = f.Content.FileType.Id
                        });             
                    }
                });
                
                // OBTENER LOS CONTENIDOS 
                data.ToList().ForEach( d => {
                    if (!contents.Any(x => x.Id == d.Id)) {
                        contents.Add(d);
                    }
                });
                
                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    filter = filter.ToLower();
                    contents = contents.Where(x => compareInfo.IndexOf(x.Name.ToLower(), filter, CompareOptions.IgnoreNonSpace) > -1).ToList();
                }

                // FILTRAR LOS TIPOS DE ARCHIVO
                if (fileTypeId.Count() > 0)
                    contents = contents.Where(x => fileTypeId.Contains(x.FileTypeId)).ToList();

                if (blockId > 0)
                    contents = contents.Where(x => x.Bloack != null &&  x.BlockId == blockId).ToList();

                if (purposeId > 0)
                    contents = contents.Where(x => x.PurposeId == purposeId).ToList();

                if (formativeFieldId > 0)
                    contents = contents.Where(x => x.FormativeField != null &&  x.FormativeField?.Id == formativeFieldId).ToList();

                if (EducationLevelId > 0)
                    contents = contents.Where(x => x.EducationLevel != null && x.EducationLevel?.Id == EducationLevelId).ToList();
                if (AreaPersonalSocialDevelopmentId > 0)
                    contents = contents.Where(x => x.AreaPersonalSocialDevelopment != null && x.AreaPersonalSocialDevelopment?.Id == AreaPersonalSocialDevelopmentId).ToList();
                if (AreasCurriculumAutonomyId > 0)
                    contents = contents.Where(x => x.AreasCurriculumAutonomy != null && x.AreasCurriculumAutonomy?.Id == AreasCurriculumAutonomyId).ToList();
                if (AxisId > 0)
                    contents = contents.Where(x => x.Axis != null && x.Axis?.Id == AxisId).ToList();
                if (KeylearningId > 0)
                    contents = contents.Where(x => x.Keylearning != null && x.Keylearning?.Id == KeylearningId).ToList();
                if (LearningexpectedId > 0)
                    contents = contents.Where(x => x.Learningexpected != null && x.Learningexpected?.Id == LearningexpectedId).ToList();





                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(contents.Count(), currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                result.Data = new
                {
                    fileTypes = filesTypes,
                    files     = contents.OrderBy(g => g.Name)
                                        .Skip((currentPage - 1) * result.Pagination.PageSize)
                                        .Take(result.Pagination.PageSize)
                                        .Select( x=> x.NewDTO() )
                                        .ToList()                  
                };
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al intentar de obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los archivos de los hijos de un nodo por tipo de archivo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileType"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetInventoryFileTypes(int id, int fileType, int currentPage, int sizePage, int systemId)
        {

            var response = new ResponsePagination();

            try
            {               
                var structure = new List<Entities.Node>();
                var nodeIds   = new List<int>();
                var files     = new List<Entities.NodeContent>();
                var documentsIdRa = new int[] { 7, 9, 10, 11,13, 14, 15, 16 };
                
                var nodeParent = await _nodeRepository.FindAsync(x => x.Id == id);

                if (nodeParent.ParentId == 0)                
                    structure = (await _nodeRepository.FindAllAsync(x => x.SystemId == systemId )).ToList();                
                else 
                    structure = (await _nodeRepository.FindAllAsync(x => x.Numericalmapping.StartsWith(nodeParent.Numericalmapping))).ToList();
                
                nodeIds = structure.Select(a => a.Id).ToList();

                if (systemId == (int)EnumSystems.RA && fileType == 4 )
                {
                    files = (await _nodeContentRepository.FindAllAsync(x => x.Content != null
                                                                           && nodeIds.Contains(x.NodeId.Value)
                                                                           && documentsIdRa.Contains(x.Content.FileTypeId))).ToList();

                }
                else {

                    files = (await _nodeContentRepository.FindAllAsync(x => x.Content != null
                                                                           && nodeIds.Contains(x.NodeId.Value)
                                                                           && x.Content.FileTypeId == fileType)).ToList();
                }

                

                var data  = files.Select(x => x.Content.NewSimpleDTO()).ToList();

                response.Pagination.SetData(data.Count, currentPage, sizePage);

                var pageData = data.OrderByDescending(x => x.Name)
                                  .Skip((currentPage - 1) * response.Pagination.PageSize)
                                  .Take(response.Pagination.PageSize)
                                  .ToList();

                response.Data    = pageData;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al intentar de obtener los datos: { ex.Message }";
            }

            return response;
        }

        #endregion

        #region RA 

        /// <summary>
        /// Obtener contenido recien enviado
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="fileTypeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetContentByTypeSearch(int nodeId, int currentPage, int sizePage, int searchId, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var contents = new List<ContentDTO>();
                var data     = new List<ContentDTO>();

                var today      = DateTime.Today;                
                var nodeParent = await _nodeRepository.FindAsync(x => x.Id == nodeId);
                var structure  = await _nodeRepository.FindAllAsync(x => x.Numericalmapping.StartsWith(nodeParent.Numericalmapping) && x.SystemId == systemId);
                var nodeIds    = structure.Select(a => a.Id).ToList();
                // var files      = await _nodeContentRepository.FindAllAsync(x => nodeIds.Contains(x.NodeId.Value));

                var files = await _nodeContentRepository.FindAllAsync(x => nodeIds.Contains(x.NodeId.Value) && x.Content.ContentResourceTypeId == 5);

                // DETERMINAR QUE TIPO DE BÚSQUEDA SE VA REALIZAR
                switch (searchId) {

                    case (int)EnumContentSearch.Recent:
                        data = files.OrderBy( x=> x.Content.RegisterDate)
                                    .Where(x => x.Content != null
                                        && x.Content.SystemId == systemId )
                                        // && (x.Content.RegisterDate.Month == today.Month && x.Content.RegisterDate.Year == today.Year))
                                    .Select(x => x.Content.NewSimpleDTO())
                                    .ToList();
                        break;

                    case (int)EnumContentSearch.Visits:
                        data = files.Where(x => x.Content != null
                                           && x.Content.SystemId == systemId )                                     
                                    .OrderByDescending(x => x.Content.Visits )
                                    .Select(x => x.Content.NewSimpleDTO())                                      
                                    .ToList();
                        break;

                    default:
                        data = files.Where(x => x.Content != null
                                              && x.Content.SystemId == systemId
                                              && x.Content.IsRecommended == true)
                                    .OrderByDescending(x => x.Content.Visits)
                                    .Select(x => x.Content.NewSimpleDTO())
                                    .ToList();
                        break;
                }
                
                // OBTENER LOS CONTENIDOS 
                data.ToList().ForEach(d => {
                    if (!contents.Any(x => x.Id == d.Id))                    
                        contents.Add(d);                    
                });

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(contents.Count(), currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                result.Data = contents.OrderBy(g => g.Name)
                                      .Skip((currentPage - 1) * result.Pagination.PageSize)
                                      .Take(result.Pagination.PageSize)
                                      .ToList();
                
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al intentar de obtener el contenido: { ex.Message }";
            }

            return result;
        }
        
        #endregion

        #region METHODS PRIVATES

        /// <summary>
        /// Realiza una búsqueda recursiva de los nodos hijos
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="parent"></param>
        /// <param name="trasnform"></param>
        private void RecursiveChildren(List<Project.Entities.Node> nodes, Project.Entities.Node parent, ref List<Project.Entities.Node> trasnform) {

            var children = nodes.Where(x => x.ParentId == parent.Id);

            trasnform.Add(parent);

            if (children.Count() > 0)
            {
                foreach (var node in children)
                {
                    this.RecursiveChildren(nodes, node, ref trasnform);
                }
            }
            
            
        }

        /// <summary>
        /// Realizar la sumatoria de los elementos
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="parent"></param>
        /// <param name="countFiles"></param>
        private void RecursiveChildrenSum(List<VNodesTotalFileTypes> nodes, VNodesTotalFileTypes parent, ref CountContentDTO countFiles)
        {
            var children = nodes.Where(x => x.ParentId == parent.Id);
            
            countFiles.TotalVideo += parent.Videos;
            countFiles.TotalAudio += parent.Audios;
            countFiles.TotalImage += parent.Images;
            countFiles.TotalDocument += parent.Documents;
           
            if (children.Count() > 0)
            {
                foreach (var node in children)
                {
                    this.RecursiveChildrenSum(nodes, node, ref countFiles);
                }
            }
        }
        
        /// <summary>
        /// Realizar la sumatoria de los elementos
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="parent"></param>
        /// <param name="countFiles"></param>
        private void RecursiveContentFiles(List<Project.Entities.Node> nodes, Project.Entities.Node parent, int fileTypeId, ref List<ContentDTO> countFiles)
        {
            var children = nodes.Where(x => x.ParentId == parent.Id);

            if(parent.Contents.Count() > 0)
                countFiles.AddRange(parent.Contents.Where(x => x.Content?.FileTypeId == fileTypeId).Select( x=> x.Content.NewSimpleDTO()));
            
            if (children.Count() > 0)
            {
                foreach (var node in children)                
                    this.RecursiveContentFiles(nodes, node, fileTypeId, ref countFiles);                
            }
        }

        // Obtener contenido de nodos de manera recursiva
        private void RecursiveContent(List<Project.Entities.Node> nodes, Project.Entities.Node parent, ref List<ContentDTO> countFiles)
        {
            var children = nodes.Where(x => x.ParentId == parent.Id);

            if(parent.Contents.Count() > 0)
                countFiles.AddRange(parent.Contents
                          .Where( x => x.Content != null)
                          .Select(x=> x.Content.NewSimpleDTO()));
            
            if (children.Count() > 0) {
                foreach (var node in children)                
                    this.RecursiveContent(nodes, node,  ref countFiles);
            }
        }
        
        /// <summary>
        /// Obtener los nodos a los cuales tiene acceso el usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<List<Entities.Node>> GetUserNodes(int userId)
        {

            var nodes = default(List<Entities.Node>);

            try
            {
                var user         = await _userRepository.FindAsync(x => x.Id == userId);
                var content      = await _nodeRepository.FindAllAsync(x => x.Public == true && x.SystemId == user.SystemId);
                var structure    = await _nodeRepository.FindAllAsync(x => x.SystemId == user.SystemId );

                var nodesProfile = new List<Entities.Node>();
                var nodesPublic  = new List<Entities.Node>();
                var nodesUser    = new List<Entities.Node>();                
                var nodesBook    = new List<Entities.Node>();
                               
                content.ToList().ForEach(n =>
                {
                    if (n.NodeTypeId == (int)EnumNodeType.Book)
                        nodesBook.Add(n);
                    else
                    {
                        var parent = structure.FirstOrDefault(x => x.Id == n.ParentId);
                        if (parent != null)
                        {
                            if (parent.NodeTypeId == (int)EnumNodeType.Book)
                                nodesBook.Add(parent);
                        }
                    }


                });

                nodesPublic      = nodesBook.Where(x => x.NodeTypeId == (int)EnumNodeType.Book)
                                          .Select(x => new Entities.Node
                                          {
                                              Id               = x.Id,
                                              Active           = x.Active,
                                              Description      = x.Description,
                                              Name             = x.Name,
                                              UrlImage         = x.UrlImage,
                                              NodeAccessTypeId = (int)EnumNodeAccessType.Public
                                          }).ToList();

                if (user.PublishingProfileUsers.Count > 0)
                    nodesProfile = user.PublishingProfileUsers.SelectMany(x => x.PublishingProfile.Nodes)                                                                     
                                                              .Where(x => x.NodeTypeId == (int)EnumNodeType.Book)
                                                              .Select(x => new Entities.Node {
                                                                          Id = x.Id,
                                                                          Active = x.Active,
                                                                          Description = x.Description,
                                                                          Name = x.Name,
                                                                          UrlImage = x.UrlImage,
                                                                          NodeAccessTypeId = (int)EnumNodeAccessType.Owner
                                                                      })
                                                                      .ToList();
                
                nodesUser = user.Nodes?.Select(x => new Entities.Node
                                {
                                    Id = x.Id,
                                    Active = x.Active,
                                    Description = x.Description,
                                    Name = x.Name,
                                    UrlImage = x.UrlImage,
                                    NodeAccessTypeId = (int)EnumNodeAccessType.Owner
                                })
                                .ToList();

                nodesProfile = nodesProfile ?? new List<Entities.Node>();
                nodesPublic  = nodesPublic ?? new List<Entities.Node>();
                nodesUser    = nodesUser ?? new List<Entities.Node>();

                nodesProfile = nodesPublic.Concat(nodesProfile).ToList();
                nodesProfile = nodesProfile.Concat(nodesUser).ToList();
                nodes        = nodesProfile.Distinct().ToList();

            }
            catch (Exception)
            {
                nodes = new List<Entities.Node>();
            }

            return nodes;
        }


        #endregion


    }
}