﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class FormativeField : IFormativeField
    {
        private readonly IFormativeFieldRepository _FormativeFieldRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FormativeField(IFormativeFieldRepository FormativeFieldRepository, IUnitOfWork unitOfWor)
        {

            this._FormativeFieldRepository = FormativeFieldRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddFormativeField(FormativeFieldSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.FormativeField FormativeField = new Entities.FormativeField();

                FormativeField.Description = request.Description;

                _FormativeFieldRepository.Add(FormativeField);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = FormativeField;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteFormativeField(int FormativeFieldId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _FormativeFieldRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _FormativeFieldRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateFormativeField(FormativeFieldUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.FormativeField FormativeField = new Entities.FormativeField();

                FormativeField.Id = request.Id;
                FormativeField.Description = request.Description;

                _FormativeFieldRepository.Update(FormativeField);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = FormativeField;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}