﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Permission : IPermission
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Permission(IPermissionRepository permissionRepository, IUnitOfWork unitOfWor)
        {

            this._permissionRepository = permissionRepository;
            this._unitOfWork = unitOfWor;
        }

        /// <summary>
        /// Método que realizar el registro de un nuevo elemento del menú
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPermission(PermissionSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Permission permission = new Entities.Permission();

                permission.Id           = request.Id;
                permission.Module       = request.Module;
                permission.Description  = request.Description;
                permission.Active       = request.Active;
                permission.Icon         = request.Icon;
                permission.Url          = request.Url;
                permission.ParentId     = request.ParentId;

                _permissionRepository.Add(permission);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = permission;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeletePermission(int PermissionId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _permissionRepository.GetAll().Select(p => new Project.Entities.Permission {
                    Active      = p.Active,
                    Description = p.Description,
                    Id          = p.Id,
                    Module      = p.Module,
                    Icon        = p.Icon,
                    ParentId    = p.ParentId,
                    Url         = p.Url
                }) .ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _permissionRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para actualizar los datos del permiso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePermission(PermissionSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Permission permission = new Entities.Permission();

                permission.Id           = request.Id;
                permission.Module       = request.Module;
                permission.Description  = request.Description;
                permission.Active       = request.Active;
                permission.Icon         = request.Icon;
                permission.Url          = request.Url;
                permission.ParentId     = request.ParentId;

                _permissionRepository.Update(permission);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = permission;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        
    }
}