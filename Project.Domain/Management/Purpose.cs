﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Purpose : IPurpose
    {
        private readonly IPurposeRepository _PurposeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Purpose(IPurposeRepository PurposeRepository, IUnitOfWork unitOfWor)
        {

            this._PurposeRepository = PurposeRepository;
            this._unitOfWork = unitOfWor;
        }

        /// <summary>
        /// Agregar un proposito existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPurpose(PurposeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Purpose Purpose = new Entities.Purpose();

                Purpose.Description = request.Description;

                _PurposeRepository.Add(Purpose);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Purpose;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de un poposito existente
        /// </summary>
        /// <param name="PurposeId"></param>
        /// <returns></returns>
        public Task<Response> DeletePurpose(int PurposeId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtener todos los propositos registrados ordenados por descripción
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _PurposeRepository.GetAllAsync())
                                                       .OrderBy( x=> x.Description)
                                                       .Select(u => new {
                                                            Id = u.Id,
                                                            Description = u.Description})
                                                        .ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener proposito por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _PurposeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar los datos de un proposito existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePurpose(PurposeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Purpose Purpose = new Entities.Purpose();

                Purpose.Id = request.Id;
                Purpose.Description = request.Description;

                _PurposeRepository.Update(Purpose);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Purpose;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}