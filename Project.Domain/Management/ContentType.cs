﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class ContentType : IContentType
    {
        private readonly IContentTypeRepository _ContentTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ContentType(IContentTypeRepository ContentTypeRepository, IUnitOfWork unitOfWor)
        {

            this._ContentTypeRepository = ContentTypeRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddContentType(ContentTypeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.ContentType ContentType = new Entities.ContentType();

                ContentType.Description = request.Description;

                _ContentTypeRepository.Add(ContentType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = ContentType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteContentType(int ContentTypeId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _ContentTypeRepository.GetAllAsync())
                    .OrderBy( x=> x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _ContentTypeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateContentType(ContentTypeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.ContentType ContentType = new Entities.ContentType();

                ContentType.Id = request.Id;
                ContentType.Description = request.Description;

                _ContentTypeRepository.Update(ContentType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = ContentType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}