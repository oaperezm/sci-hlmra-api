﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Management;
using Project.Repositories.Services;
using System.IO;
using System.Web;
using Project.Entities.Responses;
using Project.Domain.Helpers;
using Project.Entities.DTO;
using Project.Entities.Converters;
using System.Globalization;

namespace Project.Domain.Management
{
    public class Content : IContent
    {
        private readonly IContentRepository _contentRepository;
        private readonly INodeContentRepository _nodeContentRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contentRepository"></param>
        /// <param name="_nodeContentRepository"></param>
        /// <param name="tagRepository"></param>
        /// <param name="unitOfWor"></param>
        /// <param name="fileUploader"></param>
        public Content(IContentRepository contentRepository,
                       INodeContentRepository nodeContentRepository,
                       ITagRepository tagRepository,
                       IUnitOfWork unitOfWor,
                       IFileUploader fileUploader)
        {
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWor;
            _fileUploader = fileUploader;
            _nodeContentRepository = nodeContentRepository;
            _tagRepository = tagRepository;
        }

        #region METHODS

        /// <summary>
        /// Obtiene todos los contenidos existentes
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                var data = await _contentRepository.GetAllAsync();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = data.Select(n => n.NewDTO()).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los registros de contenido por paginación
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, PaginationContentRequest request, int systemId)
        {

            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Content>);
                var data = await _contentRepository.FindAllAsync(x => x.SystemId == systemId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(request.Filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    request.Filter = request.Filter.ToLower();
                    data = data.Where(x => compareInfo.IndexOf(x.Name.ToLower(), request.Filter, CompareOptions.IgnoreNonSpace) > -1).ToList();
                }

                // FILTRADO POR TIPO DE RECURSOS
                if (request.ContentResourceTypeId > 0)
                    data = data.Where(x => x.ContentResourceTypeId == request.ContentResourceTypeId).ToList();

                // FILTRADO POR TIPO DE CONTENIDO
                if (request.ContentTypeId > 0)
                    data = data.Where(x => x.ContentTypeId == request.ContentTypeId).ToList();

                // FILTRADO POR TIPO DE ARCHIVO
                if (request.FileTypeId > 0)
                    data = data.Where(x => x.FileTypeId == request.FileTypeId).ToList();

                // FILTRADO POR CAMPO FORMATIVO
                if (request.FormativeFieldId > 0)
                    data = data.Where(x => x.FormativeFieldId == request.FormativeFieldId).ToList();

                // FILTRADO POR PROPOSITO
                if (request.PurposeId > 0)
                    data = data.Where(x => x.PurposeId == request.PurposeId).ToList();



                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.NewDTO()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtiene los datos de un contenido especificado por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                var data = await _contentRepository.FindAsync(r => r.Id.Equals(id));

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = data.NewDTO();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener el contenido relacionado por identificador de contenido
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetRelatedContent(int contentId, int currentPage, int sizePage, int systemId)
        {

            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<ContentDTO>);
                var content = await _contentRepository.FindAsync(x => x.Id == contentId);
                var tagIds = content.Tags.Select(t => t.Id).ToList();
                var tags = await _tagRepository.FindAllAsync(t => tagIds.Contains(t.Id));
                var data = tags.SelectMany(t => t.Contents)
                                   .Where(c => c.SystemId == systemId && c.IsPublic == true && c.Id != contentId)
                                   .Distinct()
                                   .ToList()
                                   .Select(c => c.NewDTO())
                                   .ToList();

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Permite agregar un nuevo registro a la tabla de contenidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddContent(ContentSaveRequest request)
        {
            var result = new Response();

            try
            {
                string fileUrl = string.Empty;
                string container = "books";

                // Se vaida el tipo de contenido para definir el contenedor al cual se van a guardar los datos
                if (request.ContentTypeId == 1)
                    container = "odas";

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);

                    if (string.IsNullOrEmpty(fileUrl))
                        throw new Exception("Problemas al intentar guardar el archivo en el storage");
                }

                Entities.Content content = new Entities.Content();
                content.Name = request.Name;
                content.UrlContent = !String.IsNullOrEmpty(fileUrl) ? fileUrl : request.UrlContent;
                content.Active = request.Active;
                content.IsPublic = request.IsPublic;
                content.ContentTypeId = request.ContentTypeId;
                content.FileTypeId = request.FileTypeId;
                content.SystemId = request.SystemId;
                content.Description = request.Description;
                content.FormativeFieldId = null;
                content.TrainingFieldId = null;
                content.AreaId = null;
                content.RegisterDate = DateTime.UtcNow.AddHours(offset.Hours);
                content.IsRecommended = request.IsRecommended;


                if (content.FileTypeId == 8 || content.FileTypeId == 17) // Interactivo RA
                    content.UrlContent = request.UrlInteractive;

                if (!string.IsNullOrEmpty(request.Thumbnails))
                    content.Thumbnails = request.Thumbnails;

                if (request.BlockId > 0)
                    content.BlockId = request.BlockId;
                else
                    content.BlockId = null;

                if (request.PurposeId > 0)
                    content.PurposeId = request.PurposeId;
                else
                    content.PurposeId = null;

                if (request.ContentResourceTypeId > 0)
                    content.ContentResourceTypeId = request.ContentResourceTypeId;
                else
                    request.ContentResourceTypeId = null;

                if (request.FormativeFieldId > 0)
                    content.FormativeFieldId = request.FormativeFieldId;
                else
                    content.FormativeFieldId = null;

                //Nuevos campos  apertir del  26/082019
                if (request.EducationLevelId > 0)
                    content.EducationLevelId = request.EducationLevelId;
                else
                    content.EducationLevelId = null;

                if (request.AreaPersonalSocialDevelopmentId > 0)
                    content.AreaPersonalSocialDevelopmentId = request.AreaPersonalSocialDevelopmentId;
                else
                    content.AreaPersonalSocialDevelopmentId = null;

                if (request.AreasCurriculumAutonomyId > 0)
                    content.AreasCurriculumAutonomyId = request.AreasCurriculumAutonomyId;
                else
                    content.AreasCurriculumAutonomyId = null;

                if (request.AxisId > 0)
                    content.AxisId = request.AxisId;
                else
                    content.AxisId = null;

                if (request.KeylearningId > 0)
                    content.KeylearningId = request.KeylearningId;
                else
                    content.KeylearningId = null;

                if (request.LearningexpectedId > 0)
                    content.LearningexpectedId = request.LearningexpectedId;
                else
                    content.LearningexpectedId = null;




                // Validar que el nodo sea publico o no
                var contents = await _nodeContentRepository.FindAllAsync(x => x.ContentId == content.Id);

                if (contents.Count > 0)
                {
                    if (content.IsPublic)
                    {
                        contents.ToList().ForEach(c =>
                        {
                            c.Node.Public = true;
                            c.IsPublic = true;
                            _nodeContentRepository.Update(c);
                        });
                    }
                    else
                    {
                        contents.ToList().ForEach(c =>
                        {
                            if (!c.Node.Contents.Any(x => x.Content.IsPublic == true))
                            {
                                c.Node.Public = false;
                                c.IsPublic = false;
                                _nodeContentRepository.Update(c);
                            }
                        });

                    }
                }

                await this.UpdateTags(request.Tags, content);

                _contentRepository.Add(content);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = await this.GetContentById(content.Id);
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Permite actualizar los datos de un contenido 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateContent(ContentUpdateRequest request)
        {
            var result = new Response();

            try
            {
                string fileUrl = string.Empty;
                string container = "books";
                var content = _contentRepository.GetById(request.Id);

                if (content == null)
                    throw new Exception("El contenido a actualizar no se encuentra");

                // Se vaida el tipo de contenido para definir el contenedor al cual se van a guardar los datos
                if (request.ContentTypeId == 1)
                    container = "odas";

                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    // Eliminar el contenido anterior
                    await _fileUploader.FileRemove(content.UrlContent, container);

                    // Agregar el nuevo archivo al blob storage
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                    content.UrlContent = fileUrl;
                }

                if (content.FileTypeId == 1 && content.UrlContent != request.UrlContent)
                    content.UrlContent = request.UrlContent;

                content.Name = request.Name;
                content.IsPublic = request.IsPublic;
                content.Active = request.Active;
                content.ContentTypeId = request.ContentTypeId;
                content.FileTypeId = request.FileTypeId;
                content.Description = request.Description;
                content.IsRecommended = request.IsRecommended;

                // Se valida el tipo de archivo interactivo para guardar la URL del interactivo
                if (content.FileTypeId == 8 || content.FileTypeId == 17) // Interactivo RA  e Interactivo SALI
                    content.UrlContent = request.UrlInteractive;

                if (request.BlockId > 0)
                    content.BlockId = request.BlockId;
                else
                    content.BlockId = null;

                if (request.FormativeFieldId > 0)
                    content.FormativeFieldId = request.FormativeFieldId;
                else
                    content.FormativeFieldId = null;

                if (request.PurposeId > 0)
                    content.PurposeId = request.PurposeId;
                else
                    content.PurposeId = null;

                if (request.ContentTypeId == 3 && request.ContentResourceTypeId > 0)
                    content.ContentResourceTypeId = request.ContentResourceTypeId;
                else
                    request.ContentResourceTypeId = null;

                //Nuevos campos  apertir del  26/082019
                if (request.EducationLevelId > 0)
                    content.EducationLevelId = request.EducationLevelId;
                else
                    content.EducationLevelId = null;

                if (request.AreaPersonalSocialDevelopmentId > 0)
                    content.AreaPersonalSocialDevelopmentId = request.AreaPersonalSocialDevelopmentId;
                else
                    content.AreaPersonalSocialDevelopmentId = null;

                if (request.AreasCurriculumAutonomyId > 0)
                    content.AreasCurriculumAutonomyId = request.AreasCurriculumAutonomyId;
                else
                    content.AreasCurriculumAutonomyId = null;

                if (request.AxisId > 0)
                    content.AxisId = request.AxisId;
                else
                    content.AxisId = null;

                if (request.KeylearningId > 0)
                    content.KeylearningId = request.KeylearningId;
                else
                    content.KeylearningId = null;

                if (request.LearningexpectedId > 0)
                    content.LearningexpectedId = request.LearningexpectedId;
                else
                    content.LearningexpectedId = null;

                if (!string.IsNullOrEmpty(request.Thumbnails))
                    content.Thumbnails = request.Thumbnails;

                // Validar que el nodo sea publico o no
                var contents = await _nodeContentRepository.FindAllAsync(x => x.ContentId == content.Id);

                if (contents.Count > 0)
                {
                    if (content.IsPublic)
                    {
                        contents.ToList().ForEach(c =>
                        {
                            c.Node.Public = true;
                            c.IsPublic = true;
                            _nodeContentRepository.Update(c);
                        });
                    }
                    else
                    {
                        contents.ToList().ForEach(c =>
                        {
                            if (!c.Node.Contents.Any(x => x.Content.IsPublic == true))
                            {
                                c.Node.Public = false;
                                c.IsPublic = false;
                                _nodeContentRepository.Update(c);
                            }
                        });

                    }
                }

                await this.UpdateTags(request.Tags, content);

                _contentRepository.Update(content);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = await this.GetContentById(content.Id);
                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que elimina la información de un contenido de manera permanente
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteContent(int contentId)
        {
            var response = new Response();

            try
            {
                var content = _contentRepository.GetById(contentId);
                string container = "books";


                if (content == null)
                    throw new Exception("Contenido no encontrado");

                var nodeContent = await _nodeContentRepository.FindAllAsync(x => x.ContentId == contentId);

                if (nodeContent.Count > 0)
                    throw new Exception("El contenido esta relacionado a uno o más elementos de la estructura del sistema");

                if (content.ContentTypeId == 1)
                    container = "odas";

                //TODO Eliminar archivos del contenido en el blobstorage
                await _fileUploader.FileRemove(content.UrlContent, container);

                var nodeContet = content.NodeContents.ToList();

                nodeContet.ForEach(n => _nodeContentRepository.Delete(n));
                content.Tags.Clear();

                _contentRepository.Delete(content);
                _unitOfWork.SaveChanges();

                response.Success = true;
                response.Message = "Contenido eliminado correctamente";

            }
            catch (Exception ex)
            {
                response.Message = $"Error al eliminar los datos: { ex.Message} ";
                response.Success = false;
            }



            return response;
        }

        /// <summary>
        /// Método que elimina la información de un contenido de manera permanente
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public async Task<Response> DownloadContent(int contentId)
        {
            var response = new Response();
            MemoryStream Meme = new MemoryStream();
            try
            {
                var content = await _contentRepository.FindAsync(x => x.Id == contentId);
                string container = "books";


                if (content == null)
                    throw new Exception("Contenido no encontrado");

                if (content.ContentTypeId == 1)
                    container = "odas";

                string urlSimple = HttpUtility.UrlDecode(content.UrlContent).Split('?').First();
                string fname = urlSimple.Split('/').Last();

                //Descargar archivos del contenido en el blobstorage
                Meme = await _fileUploader.FileDownload(fname, container);

                response.Data = Meme;
                response.Success = true;
                response.Message = "Contenido descargado correctamente |" + fname;

            }
            catch (Exception ex)
            {
                response.Message = $"Error al descargar los datos: { ex.Message} ";
                response.Success = false;
            }

            return response;
        }

        /// <summary>
        /// Permite actualizar el campo de ACTIVE del contenido
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateActiveStatus(int id, bool status)
        {

            var result = new Response();

            try
            {
                var content = _contentRepository.GetById(id);
                content.Active = status;

                _contentRepository.Update(content);
                await _unitOfWork.SaveChangesAsync();


                result.Message = "Datos actualizados correctamente";
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Message = $"Error al actualizar los datos: { ex.Message }";
                result.Success = false;

            }

            return result;
        }


        /// <summary>
        /// Actualizar las visitas a un contenido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> UpdateVisits(int id)
        {

            var result = new Response();

            try
            {
                var content = await _contentRepository.FindAsync(x => x.Id == id);

                if (content == null)
                {
                    throw new Exception("Datos no encontrados");
                }

                content.Visits = content.Visits + 1;

                _contentRepository.Update(content);
                await _unitOfWork.SaveChangesAsync();

                result.Data = content.Visits;
                result.Success = true;
                result.Message = "Visitas actualizadas correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar el número de visitas: {ex.Message}";
            }

            return result;
        }

        #endregion

        #region TAGS

        /// <summary>
        /// Asignar tags a contenidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AssignatedTags(ContentAssingTagsRequest request)
        {

            var result = new Response();

            try
            {
                var content = await _contentRepository.FindAsync(x => x.Id == request.ContentiId);

                if (content == null)
                    throw new Exception("Datos del contenido no encontrados");

                var tags = await _tagRepository.FindAllAsync(x => request.TagsId.Contains(x.Id));

                if (content.Tags.Count > 0)
                    content.Tags.Clear();

                tags.ToList().ForEach(t => { content.Tags.Add(t); });


                _contentRepository.Update(content);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tags asignados correctamente al contenido";
                result.Data = null;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al asignar los tags al contneido  { ex.Message}";

            }

            return result;
        }

        /// <summary>
        /// Actualizar los tags de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateTags(ContentTagsRequest request)
        {

            var result = new Response();

            try
            {
                var content = await _contentRepository.FindAsync(x => x.Id == request.Id);

                if (content == null)
                    throw new Exception("No se encontraron datos del contenido");

                await this.UpdateTags(request.Tags, content);

                _contentRepository.Update(content);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tags del contenido actualizados correctamente al contenido";
                result.Data = null;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al actualizar los tags al contneido  { ex.Message}";
            }

            return result;
        }


        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Obtener los datos de un contenido por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<ContentDTO> GetContentById(int id)
        {

            var content = await _contentRepository.FindAsync(c => c.Id == id);
            var dto = default(ContentDTO);

            try
            {
                if (content != null)
                    dto = content.NewDTO();

                return dto;
            }
            catch (Exception)
            {
                return new ContentDTO();
            }

        }

        /// <summary>
        /// Actualizar los tags de un contenido
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        private async Task<bool> UpdateTags(List<TagRequest> tags, Entities.Content content)
        {

            var result = false;

            try
            {

                // Agregar tags asignados al contenido
                if (tags.Count > 0)
                {
                    content.Tags.Clear();
                    var tagIds = tags.Select(x => x.Id).ToList();
                    var lstTags = await _tagRepository.FindAllAsync(x => tagIds.Contains(x.Id));

                    lstTags.ToList().ForEach(t =>
                    {
                        content.Tags.Add(t);
                    });

                    // Agregar nuevos tags
                    var newTags = tags.Where(x => x.Id == 0).ToList();

                    if (newTags.Count > 0)
                    {
                        foreach (var n in newTags)
                        {
                            var tagName = await _tagRepository.FindAsync(x => x.Name.ToLower() == n.Name.ToLower());

                            if (tagName == null)
                                content.Tags.Add(new Entities.Tag { Name = n.Name });
                            else
                                content.Tags.Add(tagName);
                        }
                    }
                }
                else
                {
                    content.Tags.Clear();
                }

                result = true;

            }
            catch (Exception)
            {
                result = false;
            }

            return result;

        }


        #endregion
    }
}