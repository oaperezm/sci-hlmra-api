﻿using Project.Domain.Core;
using Project.Entities.Responses;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class ContentResourceType: IContentResourceType
    {
        private readonly IContentResourceTypeRepository  _contentResourceTypeRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contentResourceTypeRepository"></param>
        public ContentResourceType(IContentResourceTypeRepository contentResourceTypeRepository) {
            _contentResourceTypeRepository = contentResourceTypeRepository;
        }

        /// <summary>
        /// Obtener todos los tipos de contenido de recursos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Entities.Responses.Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _contentResourceTypeRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
