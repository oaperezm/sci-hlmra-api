﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class NodeType : INodeType
    {
        private readonly INodeTypeRepository _NodeTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public NodeType(INodeTypeRepository NodeTypeRepository, IUnitOfWork unitOfWor)
        {

            this._NodeTypeRepository = NodeTypeRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddNodeType(NodeTypeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.NodeType NodeType = new Entities.NodeType();

                NodeType.Description = request.Description;

                _NodeTypeRepository.Add(NodeType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = NodeType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteNodeType(int NodeTypeId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = _NodeTypeRepository.GetAll()
                                                    .Where( x=> x.Active == true)
                                                    .OrderBy( x=> x.Order)
                                                    .Select(u => new {
                                                                Id = u.Id,
                                                                Description = u.Description
                                                             }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _NodeTypeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateNodeType(NodeTypeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.NodeType NodeType = new Entities.NodeType();

                NodeType.Id = request.Id;
                NodeType.Description = request.Description;

                _NodeTypeRepository.Update(NodeType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = NodeType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}