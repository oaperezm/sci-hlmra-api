﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Tag: ITag
    {
        private readonly ITagRepository _tagRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Tag(ITagRepository tagRepository, IUnitOfWork unitOfWork) {
            _tagRepository = tagRepository;
            _unitOfWork    = unitOfWork;
        }

        /// <summary>
        /// Agregar un nuevo tag al listado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TagSaveRequest request) {

            var result = new Response();

            try
            {
                var tag  = new Entities.Tag();
                tag.Name = request.Name;

                await _tagRepository.AddAsync(tag);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = tag.Id;
                result.Success = true;
                result.Message = "Tag agregado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al agregar el tag { ex.Message }";
            }

            return result;

        }
        
        /// <summary>
        /// Actualizar un tag existentex
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(TagUpdateRequest request) {

            var result = new Response();

            try
            {
                var tag  = await _tagRepository.FindAsync(x => x.Id == request.Id);
                tag.Name = request.Name;

                _tagRepository.Update(tag);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = tag.Id;
                result.Success = true;
                result.Message = "Datos del tag agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al agregar el tag { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliominar un tag existente que no este relacionado
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int tagId) {

            var result = new Response();

            try
            {
                var tag = await _tagRepository.FindAsync(x => x.Id == tagId);

                if (tag == null)
                    throw new Exception("Datos del tag no econtrados");

                if (tag.Contents.Count > 0)
                    throw new Exception("El tag no puede ser eliminado ya que tiene contenido relacionado");                

                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas la eliminar el tag; { ex.Message }";
              
            }

            return result;

        }

        /// <summary>
        /// Obtener todos los tag registrados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter) {

            var result = new ResponsePagination();

            try
            {
                var data     = await _tagRepository.GetAllAsync();
                var pageData = default(List<Entities.Tag>);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
                    filter          = filter.ToLower();
                    data            = data.Where(x => compareInfo.IndexOf( x.Name.ToLower() , filter, CompareOptions.IgnoreNonSpace) > -1 ).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data   = pageData.Select(u => new Entities.Tag {
                                                Id   = u.Id,
                                                Name = u.Name
                                               }).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas la obtener los tags registrados: { ex.Message }";
            }

            return result;

        }
    }
}
