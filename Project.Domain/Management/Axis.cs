﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class Axis: IAxis
    {
        private readonly IAxisRepository _axisRepository;
        private readonly IEducationLevelRepository _educationLevelRepository;
        private readonly IUnitOfWork _unitOfWork;
        public Axis(IAxisRepository axisRepository, IUnitOfWork unitOfWor, IEducationLevelRepository educationLevelRepository)
        {
            this._educationLevelRepository = educationLevelRepository;
            this._axisRepository = axisRepository;
            this._unitOfWork = unitOfWor;
        }
        public async Task<Response> Add(AxisRequest request)
        {
            var result = new Response();

            try
            {
                Entities.Axis axis = new Entities.Axis();

                axis.Description = request.Description;
                axis.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    axis.EducationLevelId = request.EducationLevelId;
                }
                _axisRepository.Add(axis);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = axis;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public Task<Response> Delete(int Id)
        {
            throw new NotImplementedException();
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _axisRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active,
                        EducationLevelId= u.EducationLevelId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _axisRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> Update(AxisUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Axis axis = new Entities.Axis();

                axis.Id = request.Id;
                axis.Description = request.Description;
                axis.Active = request.Active;
                Entities.EducationLevel educationLevel = new Entities.EducationLevel();
                educationLevel = await _educationLevelRepository.FindAsync(r => r.Id.Equals(request.EducationLevelId));
                if (educationLevel == null)
                {
                    throw new Exception("Se necesita asignar  un  nivel educativo");
                }
                else
                {
                    axis.EducationLevelId = request.EducationLevelId;
                }
                _axisRepository.Update(axis);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = axis;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }


    }
}
