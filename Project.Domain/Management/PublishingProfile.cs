﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class PublishingProfile : IPublishingProfile
    {
        private readonly IPublishingProfileRepository _PublishingProfileRepository;
        private readonly IPublishingProfileUserRepository _PublishingProfileUserRepository;
        private readonly IUserRepository _UserRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PublishingProfile(IPublishingProfileRepository PublishingProfileRepository, 
            INodeRepository nodeRepository, 
            IPublishingProfileUserRepository PublishingProfileUserRepository,
            IUserRepository UserRepository,
            IUnitOfWork unitOfWor)
        {

            this._PublishingProfileRepository = PublishingProfileRepository;
            this._PublishingProfileUserRepository = PublishingProfileUserRepository;
            this._UserRepository = UserRepository;
            this._nodeRepository = nodeRepository;
            this._unitOfWork = unitOfWor;
            
        }

        public async Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfile PublishingProfile = new Entities.PublishingProfile();

                PublishingProfile.Description = request.Description;
                PublishingProfile.Active = request.Active;

                // Save nodes
                if (request.Nodes.Count() > 0)
                {
                    var lstNodes = _nodeRepository.GetAll().Where(x => request.Nodes.Contains(x.Id));

                    foreach (var p in lstNodes)
                        PublishingProfile.Nodes.Add(p);
                }

                _PublishingProfileRepository.Add(PublishingProfile);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = request;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> DeletePublishingProfile(int id)
        {
            var result = new Response();
            

            try
            {
                result.Success = true;
                result.Message = "Registro eliminado correctamente";
                
                Entities.PublishingProfile profile = _PublishingProfileRepository.GetById(id);

                if (profile != null)
                {
                    await _PublishingProfileRepository.DeleteAsync(profile);
                    await this._unitOfWork.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _PublishingProfileRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active,
                        Nodes = u.Nodes.Select(n => new
                        {
                            Id = n.Id,
                            Name = n.Name,
                            Descrption = n.Description
                        })
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _PublishingProfileRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.PublishingProfile profile = _PublishingProfileRepository.GetById(request.Id);

                //Entities.PublishingProfile PublishingProfile = new Entities.PublishingProfile();

                //PublishingProfile.Id = request.Id;
                profile.Description = request.Description;
                profile.Active = request.Active;           
                
                // Save nodes
                if(request.Nodes.Count() > 0)
                {
                    profile.Nodes.Clear();

                    var lstNodes = _nodeRepository.GetAll().Where(x => request.Nodes.Contains(x.Id));

                    foreach (var p in lstNodes)
                        profile.Nodes.Add(p);
                }

                _PublishingProfileRepository.Update(profile);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = profile;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetNodes(int id)
        {
            var result = new Response();

            try
            {
                var profile = _PublishingProfileRepository.GetById(id);

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = profile.Nodes.Select(n => new
                {
                    Id = n.Id,
                    Name = n.Name,
                    Description = n.Description,
                    NodeTypeId = n.NodeTypeId,
                    UrlImage = n.UrlImage,
                    NodeType = n.NodeType.Description
                }).ToList();

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> AddNodes(int id, List<int> nodes)
        {
            var result = new Response();

            try
            {

                var profile = _PublishingProfileRepository.GetById(id);
                profile.Nodes.Clear();

                var lstNodes = _nodeRepository.GetAll().Where(x => nodes.Contains(x.Id));

                foreach (var p in lstNodes)
                    profile.Nodes.Add(p);

                _PublishingProfileRepository.Update(profile);
                _unitOfWork.SaveChanges();

                result.Success = true;
                result.Message = "Nodos agregados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al asignar nodes al perfil: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetLinkedUsers(int profileId)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var usuarios = _UserRepository.GetAll().Select(u => new 
                {
                    Id = u.Id,
                    FullName = u.FullName,
                    Active = u.Active,
                    Selected = false
                }).ToList();

                var perfiles = _PublishingProfileUserRepository.GetAll().Where(ppu => ppu.PublishingProfileId == profileId).Select(v => v.UserId).ToList();

                result.Data = new
                {
                    u = usuarios,
                    p = perfiles
                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}