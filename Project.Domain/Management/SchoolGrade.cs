﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Management
{
    public class SchoolGrade:ISchoolGrade
    {
        private readonly ISchoolGradeRepository _schoolGradeRepository;
        private readonly IUnitOfWork _unitOfWork;
        public SchoolGrade(ISchoolGradeRepository schoolGradeRepository, IUnitOfWork unitOfWor)
        {   this._schoolGradeRepository = schoolGradeRepository;
            this._unitOfWork = unitOfWor;
        }
        public async Task<Response> Add(SchoolGradeRequest request)
        {
            var result = new Response();

            try
            {   Entities.SchoolGrade schoolGrade = new Entities.SchoolGrade();
                schoolGrade.Description = request.Description;
                schoolGrade.Active = request.Active;

                _schoolGradeRepository.Add(schoolGrade);
                await this._unitOfWork.SaveChangesAsync();
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = schoolGrade;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        public Task<Response> Delete(int Id)
        {
            throw new NotImplementedException();
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();
            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = (await _schoolGradeRepository.GetAllAsync())
                    .OrderBy(x => x.Description)
                    .Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _schoolGradeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        public async Task<Response> Update(SchoolGradeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.SchoolGrade schoolGrade = new Entities.SchoolGrade();

                schoolGrade.Id = request.Id;
                schoolGrade.Description = request.Description;
                schoolGrade.Active = request.Active;

                _schoolGradeRepository.Update(schoolGrade);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = schoolGrade;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
    }
}
