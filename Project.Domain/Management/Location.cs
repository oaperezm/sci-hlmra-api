﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Location : ILocation
    {
        private readonly ILocationRepository _LocationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Location(ILocationRepository LocationRepository, IUnitOfWork unitOfWor)
        {

            this._LocationRepository = LocationRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddLocation(LocationSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Location Location = new Entities.Location();

                Location.Description = request.Description;

                _LocationRepository.Add(Location);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Location;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteLocation(int LocationId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _LocationRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _LocationRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateLocation(LocationUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Location Location = new Entities.Location();

                Location.Id = request.Id;
                Location.Description = request.Description;

                _LocationRepository.Update(Location);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Location;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}