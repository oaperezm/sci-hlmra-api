﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Grade : IGrade
    {
        private readonly IGradeRepository _GradeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Grade(IGradeRepository GradeRepository, IUnitOfWork unitOfWor)
        {

            this._GradeRepository = GradeRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddGrade(GradeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Grade Grade = new Entities.Grade();

                Grade.Description = request.Description;

                _GradeRepository.Add(Grade);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Grade;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteGrade(int GradeId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _GradeRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _GradeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateGrade(GradeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Grade Grade = new Entities.Grade();

                Grade.Id = request.Id;
                Grade.Description = request.Description;

                _GradeRepository.Update(Grade);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Grade;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}