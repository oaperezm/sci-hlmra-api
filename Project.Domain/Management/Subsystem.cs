﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Management;

namespace Project.Domain.Management
{
    public class Subsystem : ISubsystem
    {
        private readonly ISubsystemRepository _SubsystemRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Subsystem(ISubsystemRepository SubsystemRepository, IUnitOfWork unitOfWor)
        {

            this._SubsystemRepository = SubsystemRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddSubsystem(SubsystemSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Subsystem Subsystem = new Entities.Subsystem();

                Subsystem.Description = request.Description;

                _SubsystemRepository.Add(Subsystem);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Subsystem;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteSubsystem(int SubsystemId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _SubsystemRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _SubsystemRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateSubsystem(SubsystemUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Subsystem Subsystem = new Entities.Subsystem();

                Subsystem.Id = request.Id;
                Subsystem.Description = request.Description;

                _SubsystemRepository.Update(Subsystem);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Subsystem;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}