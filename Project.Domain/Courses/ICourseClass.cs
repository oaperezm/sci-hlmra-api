﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface ICourseClass
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(CourseClassSaveRequest request);
        Task<Response> Update(CourseClassUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetBySectionId(int courseSectionId);
        Task<Response> AddFiles(CourseClassFilesRequest request);
        Task<Response> RemoveFiles(CourseClassFilesRequest request);
    }
}
