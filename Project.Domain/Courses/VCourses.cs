﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VCourses : IVCourses
    {
        private readonly IVCoursesRepository _vCoursesRepository;

        public VCourses(IVCoursesRepository vCoursesRepository)
        {
            _vCoursesRepository = vCoursesRepository;
        }

        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(PaginationRequestById request)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData     = default(List<Entities.VCourses>);
                var data         = await _vCoursesRepository.FindAllAsync(x => x.TeachId == request.Id);
                var publications = data.Select(x => new { x.NodeId,
                                                          x.Publication })
                                       .Distinct()
                                       .OrderBy(x => x.Publication).ToList();

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(request.Filter))
                {
                    request.Filter = request.Filter.ToLower();
                    data = data.Where(x => x.Subject.ToLower().Contains(request.Filter)).ToList();
                }

                if (request.Type > 0) {
                    data = data.Where(x => x.NodeId == request.Type).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, request.CurrentPage, request.PageSize);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Subject)
                                      .Skip((request.CurrentPage - 1) * result.Pagination.PageSize)
                                      .Take(result.Pagination.PageSize)
                                      .ToList();

                var resources = pageData.OrderBy(x => x.Subject)
                                          .Select(c => new Entities.VCourses
                                          {
                                              Id = c.Id,
                                              Institution = c.Institution,
                                              CourseName = c.CourseName,
                                              Publication = c.Publication,
                                              Subject = c.Subject,
                                              NodeId = c.NodeId,
                                              StartDate = c.StartDate,
                                              EndDate = c.EndDate,
                                              StudentCount = c.StudentCount,
                                              Groups = c.Groups,
                                              TeachId = c.TeachId
                                          }).ToList();

                result.Data = new
                {
                    resources,
                    publications
                };

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

    }
}
