﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using Project.Repositories.Management;
using Project.Entities.Responses;
using Project.Entities.DTO;
using Project.Entities.Enums;

//Ricardo - Agregado - Inicio
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
//Ricardo - Agregado - Fin

//Armand  - Agregado - Fin
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
//Armand  - Agregado - Inicio

namespace Project.Domain.Courses
{
    public class StudentScore : IStudentScore
    {
        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        private readonly IStudentScoreRepository _StudentScoreRepository;
        private readonly IStudentRepository __StudentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _UserRepository;
        private readonly IStudentGroupRepository _StudentGroupRepository;
        private readonly IExamScoreRepository _examScoreRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly ITestRepository _testRepository;
        private readonly ICoursePlanningRepository _CoursePlanningRepository;
        private readonly IHomeWorkRepository _homeWorkRepository;
        private readonly IHomeWorkAnswerRepository _homeWorkAnswerRepository;
        private readonly IActivityRepository _activityRepository;
        private readonly IActivityAnswerRepository _activityAnswerRepository;
        private readonly IAttendanceRepository _attendanceRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IPushNotificationService _srvPushNotification;
        //Ricardo - Agregado - Inicio
        private readonly IUserCCTRepository _userCCTRepository;
        private readonly ICourseExtensionRepository _courseExtensionRepository;
        private readonly INodeRepository _nodeRepository;
        //Ricardo - Agregado - Fin

        private readonly IMailer _mailer;//Armand - Agregado
        public StudentScore(IStudentScoreRepository StudentScoreRepository,
                            IStudentRepository StudentRepository,
                            IUnitOfWork unitOfWork,
                            IUserRepository UserRepository,
                            IStudentGroupRepository StudentGroupRepository,
                            IExamScoreRepository examScoreRepository,
                            IExamScheduleRepository examScheduleRepository,
                            ITestRepository testRepository,
                            ICoursePlanningRepository CoursePlanningRepository,
                            IHomeWorkRepository homeWorkRepository,
                            IHomeWorkAnswerRepository homeWorkAnswerRepository,
                            IActivityRepository activityRepository,
                            IAttendanceRepository attendanceRepository,
                            ICourseRepository courseRepository,
                            IActivityAnswerRepository activityAnswerRepository,
                            ISchedulingPartialRepository schedulingPartialRepository,
                            IStudentGroupRepository studentGroupRepository, 
                            IPushNotificationService srvPushNotification

                            //Ricardo - Agregado - Inicio
                            ,
                            ICourseExtensionRepository courseExtensionRepository,
                            INodeRepository nodeRepository,
                            IUserCCTRepository userCCTRepository
                            //Ricardo - Agregado - Fin
                            , IMailer mailer//Armand - Agregado)
        {
            this._StudentScoreRepository = StudentScoreRepository;
            this.__StudentRepository = StudentRepository;
            this._unitOfWork = unitOfWork;
            this._UserRepository = UserRepository;
            this._StudentGroupRepository = StudentGroupRepository;
            this._examScoreRepository = examScoreRepository;
            this._examScheduleRepository = examScheduleRepository;
            this._testRepository = testRepository;
            this._CoursePlanningRepository = CoursePlanningRepository;
            this._homeWorkRepository = homeWorkRepository;
            this._homeWorkAnswerRepository = homeWorkAnswerRepository;
            this._activityRepository = activityRepository;
            this._attendanceRepository = attendanceRepository;
            this._courseRepository = courseRepository;
            this._activityAnswerRepository = activityAnswerRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
            this._studentGroupRepository = studentGroupRepository;
            this._srvPushNotification = srvPushNotification;
        }

        /// <summary>
        /// Obtener Todos los parciales de un alumno
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="StudentId"></param> 
        /// <returns></returns>
        public async Task<Response> GetAllPartials(int studentGroupId, int userId)
        {
            var result = new Response();

            try
            {
                Entities.StudentScore StudentScore = new Entities.StudentScore();
                Entities.SchedulingPartial SchedulingPartial = new Entities.SchedulingPartial();

                var ssd = (await _StudentScoreRepository
                    .FindAllAsync(s => s.StudentGroupId == studentGroupId & s.UserId == userId))
                    .Select(ss => new Entities.StudentScore()
                    {
                        UserId = ss.UserId,
                        Id = ss.Id,
                        Homework = ss.Homework,
                        Exam = ss.Exam,
                        Assistance = ss.Assistance,
                        Activity = ss.Activity,
                        Score = ss.Score,
                        SchedulingPartialId = ss.SchedulingPartialId,
                        Points = ss.Points,
                        Status = ss.Status,
                        RegisterDate = ss.RegisterDate,
                        Comment = ss.Comment,
                    });

                result.Data = ssd;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener las calificaciones de un parcial
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="StudentId"></param> 
        /// <param name="Partialid"></param>
        /// <returns></returns>
        public async Task<Response> GetByPartialId(int studentGroupId, int Partialid, int userId)
        {
            var result = new Response();

            try
            {
                var datos = (await _StudentScoreRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId
                && p.UserId == userId
                && p.SchedulingPartialId == Partialid)).Select(u => new
                {
                    u.Id,
                    u.StudentGroupId,
                    StudentGroupDescription = u.StudentGroup.Description,
                    u.Status,
                    u.StudentId,
                    studentName = u.Student.User.FullName,
                    u.Homework,
                    u.Exam,
                    u.Assistance,
                    u.Activity,
                    u.SchedulingPartialId,
                    u.Comment,
                    u.Score,
                    u.Points
                }).ToList();

                result.Data = datos;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> SaveStudentScore(StudentScoreSaveRequest request)
        {
            var result = new Response();

            try
            {

                Entities.StudentScore studentScore = await _StudentScoreRepository.FindAsync(p =>
                p.StudentId == request.StudentId && p.UserId == request.UserId && p.StudentGroupId == request.StudentGroupId
                && p.SchedulingPartialId == request.SchedulingPartialId);

                if (studentScore != null)
                {
                    //throw new Exception("Petición invalidad");
                    studentScore.Points = request.Points;
                    studentScore.Status = request.Status;
                    studentScore.Comment = request.Comment;
                    studentScore.RegisterDate = DateTime.Now;
                    _StudentScoreRepository.Update(studentScore);
                    await this._unitOfWork.SaveChangesAsync();
                }
                else
                {
                    studentScore = new Entities.StudentScore
                    {   
                        StudentId = request.StudentId,
                        UserId = request.UserId,
                        StudentGroupId = request.StudentGroupId,
                        SchedulingPartialId = request.SchedulingPartialId,
                        Points = request.Points,
                        Status = request.Status,
                        Comment = request.Comment,
                        RegisterDate = DateTime.Now,

                        Homework= request.Homework,
                        Exam=request.Exam,
                        Assistance= request.Assistance,
                        Activity=request.Activity,
                        Score=request.Score

                    };

                    _StudentScoreRepository.Add(studentScore);
                    await this._unitOfWork.SaveChangesAsync();

                    //Si  se cierra la calificación  se envia la notificación
                    if (studentScore.Status == 1)
                    {
                        #region PUSHNOTIFICATION

                        var group = await _studentGroupRepository.FindAsync(x => x.Id == studentScore.StudentGroupId);
                        var data = new
                        {
                            groupName = group.Description,
                            homeWork = "Programación de calificación final ",
                            date = DateTime.Now.ToShortDateString(),
                        };
                        var userIds = group.Students.Select(x => x.UserId).ToList();
                        // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewFinalScore, data);

                        #endregion

                    }


                }
                result.Data = studentScore.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al gaurdar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar datos de grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStudentScore(StudentScoreUpdateRequest request)
        {
            var result = new Response();

            try
            {
                Entities.StudentScore studentScore = await _StudentScoreRepository
                    .FindAsync(p => p.StudentId == request.StudentId && p.UserId == request.UserId && p.StudentGroupId == request.StudentGroupId
                    && p.SchedulingPartialId == request.SchedulingPartialId);

                if (studentScore == null)
                    throw new Exception("Petición invalidad");
                if (studentScore.Id != request.Id)
                    throw new Exception("Petición invalidad");
                studentScore.Points = request.Points;
                studentScore.Status = request.Status;
                studentScore.Comment = request.Comment;
                studentScore.RegisterDate = DateTime.Now;
                _StudentScoreRepository.Update(studentScore);
                await this._unitOfWork.SaveChangesAsync();

                //Si  se cierra la calificación  se envia la notificación
                if (studentScore.Status == 1)
                {
                    #region PUSHNOTIFICATION

                    var group = await _studentGroupRepository.FindAsync(x => x.Id == studentScore.StudentGroupId);
                    var data = new
                    {
                        groupName = group.Description,
                        homeWork = " ",
                        date = DateTime.Now.ToShortDateString(),
                    };
                    var userIds = group.Students.Select(x => x.UserId).ToList();
                    // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewFinalScore, data);

                    #endregion

                }
                result.Data = studentScore.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }


        /// <summary>
        /// Obtener todos los estudiantes 
        /// </summary>
        /// <param name="studentGroupId"></param> 
        /// <returns></returns>
        public async Task<Response> GetAllByGroup(int studentGroupId)
        {
            return await GetScoresGroup(studentGroupId);
        }

        /// <summary>
        /// Obtener las calificaciones propuestas para estudiantes de un grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsScoresByGroup(int studentGroupId, int userId)
        {
            return await GetScoresGroup(studentGroupId, userId);
        }
        /// <summary>
        /// Obtener todos los estudiantes con detalle
        /// </summary>
        /// <param name="studentGroupId"></param> 
        /// <returns></returns>
        public async Task<Response> GetAllByGroupDetailsExcel(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO)
        {
            return await GetScoresGroupDetailsExcel(finalScoreFileRequirementsDTO);
        }
        private async Task<Response> GetScoresGroup(int studentGroupId, int? userId = null)
        {
            var result = new Response();

            try
            {
                result.Data = await GetScores(studentGroupId, userId);
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        private async Task<List<StudentFinalScoreDTO>> GetScores(int studentGroupId, int? userId = null)
        {
            List<StudentFinalScoreDTO> finalScores = new List<StudentFinalScoreDTO>();
            Dictionary<string, string> listError = new Dictionary<string, string>();

            List<Entities.StudentScore> finalscoresStudents;
            if (userId.HasValue)
                finalscoresStudents = (await _StudentScoreRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId && p.UserId == userId.Value)).ToList();
            else
                finalscoresStudents = (await _StudentScoreRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId)).ToList();

            var cp = await _CoursePlanningRepository.FindAsync(r => r.StudentGroupId == studentGroupId);
            if (cp != null)
            {
                var isOkPonderation = (cp.Activity + cp.Homework + cp.Assistance + cp.Exam) == 100;
                cp = new Entities.CoursePlanning
                {
                    Id = cp.Id,
                    Activity = isOkPonderation ? cp.Activity : 25,
                    Homework = isOkPonderation ? cp.Homework : 25,
                    Assistance = isOkPonderation ? cp.Assistance : 25,
                    Exam = isOkPonderation ? cp.Exam : 25,
                    StudentGroupId = studentGroupId,
                };
            }
            else
            {
                cp = new Entities.CoursePlanning();
                cp.Activity = cp.Homework = cp.Assistance = cp.Exam = 25;
                cp.StudentGroupId = studentGroupId;
            }
            var studentGroup = await _StudentGroupRepository.FindAsync(p => p.Id == studentGroupId);

            var students = await GetStudentAsync(studentGroupId, userId);

            // Parciales del curso                
            var schedulePartials = (await GetSchedulePartiaslAsync(cp.Id)).OrderBy(p => p.StartDate).ThenBy(p => p.Description).ToList();
            cp.NumberPartial = schedulePartials.Count;

            // se obtienen las tareas para el grupo si tiene asignado un porcentaje en la calificación final
            List<Entities.HomeWork> homeWorks = new List<Entities.HomeWork>();
            if (cp.Homework > 0)
            {
                try
                {
                    homeWorks = await GetHomeWorkAsync(studentGroupId, userId);
                }
                catch (Exception ex)
                {
                    listError.Add("Homework", ex.Message);
                }
            }

            // se obtienen las actividades para el grupo si tiene asignado un porcentaje en la calificación final
            List<Entities.Activity> activities = new List<Entities.Activity>();
            if (cp.Activity > 0)
            {
                try
                {
                    activities = await GetActivityAsync(studentGroupId, userId);
                }
                catch (Exception ex)
                {
                    listError.Add("Activity", ex.Message);
                }
            }

            //se obtienen los examenes para el grupo  si tiene asignado un porcentaje en la calificación final
            List<Entities.Attendance> attendances = new List<Entities.Attendance>();
            if (cp.Assistance > 0)
            {
                try
                {
                    attendances = await GetAttendanceAsync(studentGroupId);
                }
                catch (Exception ex)
                {
                    listError.Add("Assistance", ex.Message);
                }
            }

            //se obtienen los examenes para el grupo  si tiene asignado un porcentaje en la calificación final
            List<Entities.ExamSchedule> scheduleExams = new List<Entities.ExamSchedule>();
            if (cp.Exam > 0)
            {
                try
                {
                    scheduleExams = await GetScheduleExamAsync(studentGroupId, userId);
                }
                catch (Exception ex)
                {
                    listError.Add("Exam", ex.Message);
                }
            }

            foreach (var student in students)
            {
                StudentScoreDTO ss;

                if (schedulePartials.Count > 0)
                {
                    int numeroPartial = 0;
                    StudentFinalScoreDTO finalScoreStudent = new StudentFinalScoreDTO
                    {
                        UserId = student.UserId,
                        StudentId = student.Id,
                        StudentGroupId = studentGroupId,
                        StudenName = student.User.FullName,
                        StudentGroupDescription = student.StudentGroup.Description,
                        StudentScores = new List<StudentScoreDTO>(),
                        Errors = listError,
                    };
                    finalScores.Add(finalScoreStudent);

                    StudentScoreDTO fss = new StudentScoreDTO
                    {
                        UserId = student.UserId,
                        StudentId = student.Id,
                        SchedulingPartialId = null,
                        StudentGroupId = studentGroupId,
                        NumberPartial = 100,
                        PartialDecription = "100",
                        PartialPeriod = studentGroup.Course.StartDate.ToString("dd/MM/yyyy") + " - " + studentGroup.Course.EndDate.ToString("dd/MM/yyyy"),
                    };

                    StudentScoreDTO tempSS = null;
                    foreach (var scp in schedulePartials)
                    {
                        numeroPartial++;
                        ss = new StudentScoreDTO();
                        ss.UserId = student.UserId;
                        ss.StudentId = student.Id;
                        ss.SchedulingPartialId = scp.Id;
                        ss.StudentGroupId = studentGroupId;
                        ss.NumberPartial = numeroPartial;
                        ss.PartialDecription = scp.Description;
                        ss.PartialPeriod = scp.StartDate.ToString("dd/MM/yyyy") + " - " + scp.EndDate.ToString("dd/MM/yyyy");

                        ss.Comment = "";
                        if (cp.Homework > 0)
                        {
                            try
                            {
                                ss.Homework = GetScoreHomeWork(homeWorks, student.UserId, cp.Homework, scp.Id);
                                if (ss.Homework == -1)
                                {
                                    ss.Homework = 0;
                                    ss.StatusHomework = "Tareas Sin Cerrar";
                                }
                                else if (ss.Homework == -2)
                                {
                                    ss.Homework = cp.Homework;
                                    ss.StatusHomework = "Se asigno un porcentaje a tareas, pero no se programaron tareas.";
                                }
                            }
                            catch (Exception e)
                            {
                                ss.StatusHomework = "Error al calcular " + e.Message;
                            }
                        }

                        if (cp.Activity > 0)
                        {
                            try
                            {
                                ss.Activity = GetScoreActivities(activities, student.UserId, cp.Activity, scp.Id);
                                if (ss.Activity == -1)
                                {
                                    ss.Activity = 0;
                                    ss.StatusActivity = "Actividades Sin Cerrar";
                                }
                                else if (ss.Activity == -2)
                                {
                                    ss.Activity = cp.Activity;
                                    ss.StatusActivity = "Se asigno un porcentaje a actividades, pero no se programaron actividades.";
                                }
                            }
                            catch (Exception e)
                            {
                                ss.StatusActivity = "Error al calcular " + e.Message;
                            }

                        }

                        if (cp.Assistance > 0)
                        {
                            if (scp.FinalPartial == 1)
                                tempSS = ss;

                            DateTime startdate = scp.StartDate;
                            int totalDays = 0;
                            while (startdate <= scp.EndDate)
                            {
                                totalDays +=
                                    (studentGroup.Course.Monday && startdate.DayOfWeek == DayOfWeek.Monday) ? 1 :
                                    (studentGroup.Course.Tuesday && startdate.DayOfWeek == DayOfWeek.Tuesday) ? 1 :
                                    (studentGroup.Course.Wednesday && startdate.DayOfWeek == DayOfWeek.Wednesday) ? 1 :
                                    (studentGroup.Course.Thursday && startdate.DayOfWeek == DayOfWeek.Thursday) ? 1 :
                                    (studentGroup.Course.Friday && startdate.DayOfWeek == DayOfWeek.Friday) ? 1 :
                                    (studentGroup.Course.Saturday && startdate.DayOfWeek == DayOfWeek.Saturday) ? 1 :
                                    (studentGroup.Course.Sunday && startdate.DayOfWeek == DayOfWeek.Sunday) ? 1 : 0;
                                startdate = startdate.AddDays(1);
                            }
                            try
                            {
                                ss.Assistance = GetScoreAttendance(attendances, student.UserId, cp.Assistance, totalDays, scp);
                            }
                            catch (Exception e)
                            {
                                ss.StatusAssistance = "Error al calcular " + e.Message;
                            }
                        }
                        if (cp.Exam > 0)
                        {
                            try
                            {
                                ss.Exam = GetScoreExam(scheduleExams, student.UserId, cp.Exam, scp.Id);
                                if (ss.Exam == -2)
                                {
                                    ss.Exam = cp.Exam;
                                    ss.StatusExam = "Se asigno un porcentaje a examen, pero no se programaron exámenes.";
                                }
                            }
                            catch (Exception e)
                            {
                                ss.StatusExam = "Error al calcular " + e.Message;
                            }
                        }
                        Entities.StudentScore studentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && p.SchedulingPartialId == scp.Id);
                        if (studentScore != null)
                        {
                            ss.Id = studentScore.Id;
                            ss.Points = studentScore.Points > 100 ? 100 : studentScore.Points < -100 ? -100 : studentScore.Points;
                            ss.Status = studentScore.Status;
                            ss.Comment = studentScore.Comment;
                        }
                        else
                            ss.Points = 0;
                        ss.Score = ss.Homework + ss.Activity + ss.Assistance + ss.Exam + ss.Points;
                        ss.Score = ss.Score > 100 ? 100 : ss.Score < 0 ? 0 : ss.Score;
                        fss.Homework += ss.Homework;
                        fss.Activity += ss.Activity;
                        fss.Assistance += ss.Assistance;
                        fss.Exam += ss.Exam;
                        fss.Score += ss.Score;
                        finalScoreStudent.StudentScores.Add(ss);
                    }
                    Entities.StudentScore fstudentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && !p.SchedulingPartialId.HasValue);
                    if (fstudentScore != null)
                    {
                        fss.Id = fstudentScore.Id;
                        fss.Points = fstudentScore.Points > 100 ? 100 : fstudentScore.Points < -100 ? -100 : fstudentScore.Points;
                        fss.Status = fstudentScore.Status;
                        fss.Comment = fstudentScore.Comment;
                    }
                    else
                        fss.Points = 0;
                    fss.Homework = fss.Homework / schedulePartials.Count;
                    fss.Activity = fss.Activity / schedulePartials.Count;
                    if (tempSS == null)
                        fss.Assistance = fss.Assistance / schedulePartials.Count;
                    else
                    {

                        fss.Assistance = fss.Assistance / (schedulePartials.Count > 1 ? schedulePartials.Count - 1 : schedulePartials.Count);
                        tempSS.Assistance = fss.Assistance;
                    }
                    fss.Exam = fss.Exam / schedulePartials.Count;
                    fss.Score = (fss.Score / schedulePartials.Count) + fss.Points;
                    fss.Score = fss.Score > 100 ? 100 : fss.Score < 0 ? 0 : fss.Score;
                    finalScoreStudent.StudentScores.Add(fss);
                }
                else
                {
                    ss = new StudentScoreDTO();
                    StudentFinalScoreDTO finalScore = new StudentFinalScoreDTO
                    {
                        UserId = student.UserId,
                        StudentId = student.Id,
                        StudentGroupId = studentGroupId,
                        StudenName = student.User.FullName,
                        StudentGroupDescription = student.StudentGroup.Description,
                        StudentScores = new List<StudentScoreDTO>()
                    };
                    finalScores.Add(finalScore);
                    ss.UserId = student.UserId;
                    ss.StudentId = student.Id;
                    ss.SchedulingPartialId = null;
                    ss.NumberPartial = 100;
                    ss.PartialDecription = "100";
                    ss.StudentGroupId = studentGroupId;
                    ss.UserId = student.UserId;
                    ss.StudentId = student.Id;

                    ss.PartialPeriod = studentGroup.Course.StartDate.ToString("dd/MM/yyyy") + " - " + studentGroup.Course.EndDate.ToString("dd/MM/yyyy");
                    ss.Comment = "";

                    if (cp.Homework > 0)
                    {
                        try
                        {
                            ss.Homework = GetScoreHomeWork(homeWorks, student.UserId, cp.Homework);
                            if (ss.Homework == -1)
                            {
                                ss.Homework = 0;
                                ss.StatusHomework = "Tareas Sin Cerrar";
                            }
                            else if (ss.Homework == -2)
                            {
                                ss.Homework = cp.Homework;
                                ss.StatusHomework = "Se asigno un porcentaje a tareas, pero no se programaron tareas.";
                            }
                        }
                        catch (Exception e)
                        {
                            ss.StatusHomework = "Error al calcular " + e.Message;
                        }
                    }

                    if (cp.Activity > 0)
                    {
                        try
                        {
                            ss.Activity = GetScoreActivities(activities, student.UserId, cp.Activity);
                            if (ss.Activity == -1)
                            {
                                ss.Activity = 0;
                                ss.StatusActivity = "Actividades Sin Cerrar";
                            }
                            else if (ss.Activity == -2)
                            {
                                ss.Activity = cp.Activity;
                                ss.StatusActivity = "Se asigno un porcentaje a actividades, pero no se programaron actividades.";
                            }
                        }
                        catch (Exception e)
                        {
                            ss.StatusActivity = "Error al calcular " + e.Message;
                        }
                    }

                    if (cp.Assistance > 0)
                    {
                        DateTime startdate = studentGroup.Course.StartDate;
                        int totalDays = 0;
                        while (startdate <= studentGroup.Course.EndDate)
                        {
                            totalDays +=
                                (studentGroup.Course.Monday && startdate.DayOfWeek == DayOfWeek.Monday) ? 1 :
                                (studentGroup.Course.Tuesday && startdate.DayOfWeek == DayOfWeek.Tuesday) ? 1 :
                                (studentGroup.Course.Wednesday && startdate.DayOfWeek == DayOfWeek.Wednesday) ? 1 :
                                (studentGroup.Course.Thursday && startdate.DayOfWeek == DayOfWeek.Thursday) ? 1 :
                                (studentGroup.Course.Friday && startdate.DayOfWeek == DayOfWeek.Friday) ? 1 :
                                (studentGroup.Course.Saturday && startdate.DayOfWeek == DayOfWeek.Saturday) ? 1 :
                                (studentGroup.Course.Sunday && startdate.DayOfWeek == DayOfWeek.Sunday) ? 1 : 0;
                            startdate = startdate.AddDays(1);
                        }
                        try
                        {
                            ss.Assistance = GetScoreAttendance(attendances, student.UserId, cp.Assistance, totalDays);
                        }
                        catch (Exception e)
                        {
                            ss.StatusAssistance = "Error al calcular " + e.Message;
                        }
                    }
                    if (cp.Exam > 0)
                    {
                        try
                        {
                            ss.Exam = GetScoreExam(scheduleExams, student.UserId, cp.Exam);
                            if (ss.Exam == -2)
                            {
                                ss.Exam = cp.Exam;
                                ss.StatusExam = "Se asigno un porcentaje a examen, pero no se programaron exámenes.";
                            }
                        }
                        catch (Exception e)
                        {
                            ss.StatusExam = "Error al calcular " + e.Message;
                        }
                    }
                    Entities.StudentScore studentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && !p.SchedulingPartialId.HasValue);
                    if (studentScore != null)
                    {
                        ss.Id = studentScore.Id;
                        ss.Points = studentScore.Points > 100 ? 100 : studentScore.Points < -100 ? -100 : studentScore.Points;
                        ss.Status = studentScore.Status;
                        ss.Comment = studentScore.Comment;
                    }
                    else
                        ss.Points = 0;
                    ss.Score = ss.Homework + ss.Activity + ss.Assistance + ss.Exam + ss.Points;

                    finalScore.StudentScores.Add(ss);
                }
            }

            return finalScores;
        }


        private async Task<List<Entities.Student>> GetStudentAsync(int studentGroupId, int? userId = null)
        {
            List<Entities.Student> list;
            list = (await __StudentRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId && (userId.HasValue ? p.UserId == userId.Value : true)))
                .Select(u => new Entities.Student
                {
                    Id = u.Id,
                    StudentGroupId = u.StudentGroupId,
                    StudentGroup = new Entities.StudentGroup
                    {
                        Id = u.StudentGroup.Id,
                        Description = u.StudentGroup.Description,
                    },
                    StudentStatusId = u.StudentStatusId,
                    StudentStatus = new Entities.StudentStatus
                    {
                        Id = u.StudentStatus.Id,
                        Description = u.StudentStatus.Description
                    },
                    InvitationDate = u.InvitationDate,
                    CheckDate = u.CheckDate,
                    UserId = u.UserId,
                    User = new User
                    {
                        Id = u.User.Id,
                        FullName = u.User.FullName,
                        Email = u.User.Email,
                    }
                }).ToList();
            return list;
        }

        private async Task<List<Entities.SchedulingPartial>> GetSchedulePartiaslAsync(int cpId)
        {
            List<Entities.SchedulingPartial> list;
            list = (await _schedulingPartialRepository.FindAllAsync(p => p.CoursePlanningId == cpId))
                .Select(p => new Entities.SchedulingPartial
                {
                    Id = p.Id,
                    CoursePlanningId = p.CoursePlanningId,
                    Description = p.Description,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate
                }).ToList();
            return list;
        }

        private async Task<List<Entities.HomeWork>> GetHomeWorkAsync(int studentGroupId, int? userId = null)
        {
            List<Entities.HomeWork> list;
            list = (await _homeWorkRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId))
                .Select(p => new Entities.HomeWork
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    UserId = p.UserId,
                    StudentGroupId = p.StudentGroupId,
                    Qualified = p.Qualified,
                    AssignedQualification = p.AssignedQualification,
                    SchedulingPartialId = p.SchedulingPartialId,
                    TypeOfQualification = p.TypeOfQualification,
                    TypeScore = p.TypeScore,
                    Answers = !userId.HasValue ? p.Answers.Select(a => new HomeWorkAnswer
                    {
                        Id = a.Id,
                        HomeWorkId = a.HomeWorkId,
                        Score = a.Score,
                        UserId = a.UserId,
                        Delivered = a.Delivered
                    }).ToList()
                    : p.Answers.Where(a => a.UserId == userId.Value).Select(a => new HomeWorkAnswer
                    {
                        Id = a.Id,
                        HomeWorkId = a.HomeWorkId,
                        Score = a.Score,
                        UserId = a.UserId,
                        Delivered = a.Delivered
                    }).ToList()
                }).ToList();
            return list;
        }

        private async Task<List<Entities.Activity>> GetActivityAsync(int studentGroupId, int? userId = null)
        {
            List<Entities.Activity> list;
            list = (await _activityRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId))
                .Select(p => new Entities.Activity
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    UserId = p.UserId,
                    StudentGroupId = p.StudentGroupId,
                    Qualified = p.Qualified,
                    AssignedQualification = p.AssignedQualification,
                    SchedulingPartialId = p.SchedulingPartialId,
                    TypeOfQualification = p.TypeOfQualification,
                    TypeScore = p.TypeScore,
                    ActivityAnswers = !userId.HasValue ? p.ActivityAnswers.Select(a => new ActivityAnswer
                    {
                        Id = a.Id,
                        ActivityId = a.ActivityId,
                        Score = a.Score,
                        StudentId = a.StudentId,
                        Delivered = a.Delivered,
                    }).ToList()
                    : p.ActivityAnswers.Where(a => a.StudentId == userId.Value).Select(a => new ActivityAnswer
                    {
                        Id = a.Id,
                        ActivityId = a.ActivityId,
                        Score = a.Score,
                        StudentId = a.StudentId,
                        Delivered = a.Delivered,
                    }).ToList()
                }).ToList();
            return list;
        }

        private async Task<List<Entities.Attendance>> GetAttendanceAsync(int studentGroupId)
        {
            List<Entities.Attendance> list;
            list = (await _attendanceRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId))
                .Select(p => new Entities.Attendance
                {
                    Id = p.Id,
                    AttendanceClass = p.AttendanceClass,
                    UserId = p.UserId,
                    StudentGroupId = p.StudentGroupId,
                    AttendanceDate = p.AttendanceDate,
                }).ToList();
            return list;
        }

        private async Task<List<Entities.ExamSchedule>> GetScheduleExamAsync(int studentGroupId, int? userId = null)
        {
            List<Entities.ExamSchedule> list;
            list = (await _examScheduleRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId))
                .Select(p => new Entities.ExamSchedule
                {
                    Id = p.Id,
                    SchedulingPartialId = p.SchedulingPartialId,
                    Description = p.Description,
                    ApplicationDate = p.BeginApplicationDate,
                    BeginApplicationDate = p.BeginApplicationDate,
                    EndApplicationDate = p.EndApplicationDate,
                    BeginApplicationTime = p.BeginApplicationTime,
                    EndApplicationTime = p.EndApplicationTime,
                    MinutesExam = p.MinutesExam,
                    Active = p.Active,
                    RegisterDate = p.RegisterDate,
                    UserId = p.UserId,
                    StudentGroupId = p.StudentGroupId,
                    TeacherExamId = p.TeacherExamId,
                    TeacherExam = p.TeacherExam == null ? new Entities.TeacherExam { Weighting = new Weighting() } : new Entities.TeacherExam
                    {
                        Id = p.TeacherExam.Id,
                        TeacherExamTypeId = p.TeacherExam.TeacherExamTypeId,
                        Description = p.TeacherExam.Description,
                        MaximumExamScore = p.TeacherExam.MaximumExamScore,
                        TeacherExamTypes = new Entities.TeacherExamType
                        {
                            Id = p.TeacherExam.TeacherExamTypes.Id,
                            Description = p.TeacherExam.TeacherExamTypes.Description
                        },
                        Weighting = p.TeacherExam.Weighting == null ? new Weighting() : new Weighting
                        {
                            Id = p.TeacherExam.WeightingId,
                            Description = p.TeacherExam.Weighting.Description,
                            Unit = p.TeacherExam.Weighting.Unit,
                        }
                    },
                    ExamScheduleType = new ExamScheduleType
                    {
                        Id = p.ExamScheduleType.Id,
                        Description = p.ExamScheduleType.Description
                    },
                    Group = new Entities.StudentGroup
                    {
                        Id = p.Group.Id,
                        Description = p.Group.Description,
                    },
                    SchedulingPartial = new Entities.SchedulingPartial
                    {
                        Id = p.SchedulingPartialId ?? 0,
                        Description = p.SchedulingPartial != null ? p.SchedulingPartial.Description : "",
                    },
                    Tests = !userId.HasValue ? p.Tests.Select(a => new Entities.Test
                    {
                        Id = a.Id,
                        Score = a.Score,
                        UserId = a.UserId,
                        IsCompleted = a.IsCompleted,
                    }).ToList()
                    :
                    p.Tests.Where(a => a.UserId == userId).Select(a => new Entities.Test
                    {
                        Id = a.Id,
                        Score = a.Score,
                        UserId = a.UserId,
                        IsCompleted = a.IsCompleted,
                    }).ToList()

                }).ToList();
            return list;
        }

        private float GetScoreHomeWork(List<Entities.HomeWork> homeWorks, int studentId, float percentage, int? partialId = null)
        {
            List<Entities.HomeWork> hwp;
            float scoreStudent = 0;
            float scoreResult = 0;

            if (partialId.HasValue)
                hwp = homeWorks.Where(p => p.SchedulingPartialId == partialId).ToList();
            else
                hwp = homeWorks;

            //Verifica que todas las tareas esten cerradas
            if (hwp.Where(p => p.Qualified).Count() < hwp.Count)
                return -1;

            if (hwp.Count > 0)
            {
                Entities.HomeWork cfg = hwp[0];
                foreach (var hw in hwp)
                {
                    float maxScore = hw.AssignedQualification;
                    var hwas = hw.Answers.Where(p => p.UserId == studentId).FirstOrDefault();
                    if (hwas != null)
                    {
                        //Por Cumplimiento
                        if (cfg.TypeScore == false)
                        {
                            if (hwas.Delivered)
                                scoreStudent++;
                        }
                        //Por Porcentaje o Por puntos
                        else
                        {
                            float score = 0;
                            try
                            {
                                score = Convert.ToSingle(hwas.Score);
                            }
                            catch { }
                            if (score > maxScore)
                                score = maxScore;
                            if (maxScore > 0)
                                scoreStudent += (score / maxScore);
                        }
                    }
                }
                scoreResult = (scoreStudent / hwp.Count) * percentage;
            }
            else
                return -2;
            return
                scoreResult;
        }

        private float GetScoreActivities(List<Entities.Activity> activities, int studentId, float percentage, int? partialId = null)
        {
            List<Entities.Activity> actp;
            float scoreStudent = 0;
            float scoreResult = 0;

            if (partialId.HasValue)
                actp = activities.Where(p => p.SchedulingPartialId == partialId).ToList();
            else
                actp = activities;

            //Verifica que todas las Actividades esten cerradas
            if (actp.Where(p => p.Qualified).Count() < actp.Count)
                return -1;

            if (actp.Count > 0)
            {
                Entities.Activity cfg = actp[0];
                foreach (var act in actp)
                {
                    float maxScore = act.AssignedQualification ?? 0;
                    var acta = act.ActivityAnswers.Where(p => p.StudentId == studentId).FirstOrDefault();
                    if (acta != null)
                    {
                        //Por Cumplimiento
                        if ((cfg.TypeScore ?? false) == false)
                        {
                            if (acta.Delivered)
                                scoreStudent++;
                        }
                        //Por Porcentaje o Por puntos mismo cálculo
                        else
                        {
                            float score = 0;
                            try
                            {
                                score = Convert.ToSingle(acta.Score);
                            }
                            catch { }
                            if (score > maxScore)
                                score = maxScore;
                            if (maxScore > 0)
                                scoreStudent += (score / maxScore);
                        }
                    }
                }
                scoreResult = (scoreStudent / actp.Count) * percentage;
            }
            else
                return -2;
            return scoreResult;
        }

        private float GetScoreAttendance(List<Entities.Attendance> attendances, int studentId, float percentage, int totalAttendance, Entities.SchedulingPartial partial = null)
        {
            float scoreResult = 0;

            if (totalAttendance > 0)
            {
                float attendanceStudent = 0;
                List<Entities.Attendance> ap;
                if (partial != null)
                    ap = attendances.Where(p => p.AttendanceDate >= partial.StartDate && p.AttendanceDate <= partial.EndDate).ToList();
                else
                    ap = attendances;
                attendanceStudent = ap.Where(p => p.UserId == studentId).Count();
                scoreResult = (attendanceStudent / totalAttendance) * percentage;
            }
            return scoreResult;
        }

        private float GetScoreExam(List<Entities.ExamSchedule> scheduleExams, int studentId, float percentage, int? partialId = null)
        {
            float scoreStudent = 0;
            float scoreResult = 0;
            List<Entities.ExamSchedule> sep;
            if (partialId.HasValue)
                sep = scheduleExams.Where(p => p.SchedulingPartialId == partialId).ToList();
            else
                sep = scheduleExams.ToList();
            if (sep.Count > 0)
            {
                foreach (var se in sep)
                {
                    var MaxScore = se.TeacherExam.MaximumExamScore;
                    var tests = se.Tests.Where(p => p.UserId == studentId).ToList();
                    float scoreStudentTests = 0;
                    foreach (var test in tests)
                    {
                        try
                        {
                            scoreStudentTests += Convert.ToSingle(test.Score);
                        }
                        catch { }
                    }
                    if (tests.Count > 0 && scoreStudentTests > 0 && MaxScore > 0)
                        scoreStudent = ((scoreStudentTests / tests.Count) / MaxScore) * percentage;
                }
                scoreResult = scoreStudent / sep.Count;
            }
            else
                return -2;
            return scoreResult;
        }


        public List<StudentFinalScoreDTO> GetScoreGeneral(int studentGroupId, int userId)
        {
            List<StudentFinalScoreDTO> finalScores = new List<StudentFinalScoreDTO>();
            finalScores = Task.Run(() => GetScores(studentGroupId, userId)).Result;
            return finalScores;
        }
    }
    //Ricardo - Agregado - Inicio
    private async Task<List<DetailsStudentFinalScoreDTO>> GetScoresDetails(int studentGroupId, int? userId = null)
    {
        List<DetailsStudentFinalScoreDTO> finalScores = new List<DetailsStudentFinalScoreDTO>();
        Dictionary<string, string> listError = new Dictionary<string, string>();

        List<Entities.StudentScore> finalscoresStudents;
        if (userId.HasValue)
            finalscoresStudents = (await _StudentScoreRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId && p.UserId == userId.Value)).ToList();
        else
            finalscoresStudents = (await _StudentScoreRepository.FindAllAsync(p => p.StudentGroupId == studentGroupId)).ToList();

        var cp = await _CoursePlanningRepository.FindAsync(r => r.StudentGroupId == studentGroupId);
        if (cp != null)
        {
            var isOkPonderation = (cp.Activity + cp.Homework + cp.Assistance + cp.Exam) == 100;
            cp = new Entities.CoursePlanning
            {
                Id = cp.Id,
                Activity = isOkPonderation ? cp.Activity : 25,
                Homework = isOkPonderation ? cp.Homework : 25,
                Assistance = isOkPonderation ? cp.Assistance : 25,
                Exam = isOkPonderation ? cp.Exam : 25,
                StudentGroupId = studentGroupId,
            };
        }
        else
        {
            cp = new Entities.CoursePlanning();
            cp.Activity = cp.Homework = cp.Assistance = cp.Exam = 25;
            cp.StudentGroupId = studentGroupId;
        }
        var studentGroup = await _StudentGroupRepository.FindAsync(p => p.Id == studentGroupId);

        var students = await GetStudentAsync(studentGroupId, userId);

        // Parciales del curso                
        var schedulePartials = (await GetSchedulePartiaslAsync(cp.Id)).OrderBy(p => p.StartDate).ThenBy(p => p.Description).ToList();
        cp.NumberPartial = schedulePartials.Count;

        // se obtienen las tareas para el grupo si tiene asignado un porcentaje en la calificación final
        List<Entities.HomeWork> homeWorks = new List<Entities.HomeWork>();
        if (cp.Homework > 0)
        {
            try
            {
                homeWorks = await GetHomeWorkAsync(studentGroupId, userId);
            }
            catch (Exception ex)
            {
                listError.Add("Homework", ex.Message);
            }
        }

        // se obtienen las actividades para el grupo si tiene asignado un porcentaje en la calificación final
        List<Entities.Activity> activities = new List<Entities.Activity>();
        if (cp.Activity > 0)
        {
            try
            {
                activities = await GetActivityAsync(studentGroupId, userId);
            }
            catch (Exception ex)
            {
                listError.Add("Activity", ex.Message);
            }
        }

        //se obtienen los examenes para el grupo  si tiene asignado un porcentaje en la calificación final
        List<Entities.Attendance> attendances = new List<Entities.Attendance>();
        if (cp.Assistance > 0)
        {
            try
            {
                attendances = await GetAttendanceAsync(studentGroupId);
            }
            catch (Exception ex)
            {
                listError.Add("Assistance", ex.Message);
            }
        }

        //se obtienen los examenes para el grupo  si tiene asignado un porcentaje en la calificación final
        List<Entities.ExamSchedule> scheduleExams = new List<Entities.ExamSchedule>();
        if (cp.Exam > 0)
        {
            try
            {
                scheduleExams = await GetScheduleExamAsync(studentGroupId, userId);
            }
            catch (Exception ex)
            {
                listError.Add("Exam", ex.Message);
            }
        }

        foreach (var student in students)
        {
            DetailsStudentScoreDTO ss;

            if (schedulePartials.Count > 0)
            {
                int numeroPartial = 0;
                DetailsStudentFinalScoreDTO finalScoreStudent = new DetailsStudentFinalScoreDTO
                {
                    UserId = student.UserId,
                    StudentId = student.Id,
                    StudentGroupId = studentGroupId,
                    StudenName = student.User.FullName,
                    StudentGroupDescription = student.StudentGroup.Description,
                    StudentScores = new List<DetailsStudentScoreDTO>(),
                    Errors = listError,
                    PercentageActivity = Convert.ToInt32(cp.Activity),
                    PercentageAssitance = Convert.ToInt32(cp.Assistance),
                    PercentageExam = Convert.ToInt32(cp.Exam),
                    PercentageHomework = Convert.ToInt32(cp.Homework),
                    //Armand
                    EmailUser = student.User.Email.ToString()
                };
                finalScores.Add(finalScoreStudent);

                DetailsStudentScoreDTO fss = new DetailsStudentScoreDTO
                {
                    UserId = student.UserId,
                    StudentId = student.Id,
                    SchedulingPartialId = null,
                    StudentGroupId = studentGroupId,
                    NumberPartial = 100,
                    PartialDecription = "100",
                    PartialPeriod = studentGroup.Course.StartDate.ToString("dd/MM/yyyy") + " - " + studentGroup.Course.EndDate.ToString("dd/MM/yyyy"),
                };
                DetailsStudentScoreDTO tempSS = null;
                foreach (var scp in schedulePartials)
                {
                    numeroPartial++;
                    ss = new DetailsStudentScoreDTO();
                    ss.UserId = student.UserId;
                    ss.StudentId = student.Id;
                    ss.SchedulingPartialId = scp.Id;
                    ss.StudentGroupId = studentGroupId;
                    ss.NumberPartial = numeroPartial;
                    ss.PartialDecription = scp.Description;
                    ss.PartialPeriod = scp.StartDate.ToString("dd/MM/yyyy") + " - " + scp.EndDate.ToString("dd/MM/yyyy");

                    ss.Comment = "";
                    ss.HomemorksList = new List<float>();
                    if (cp.Homework > 0)
                    {
                        try
                        {

                            ss.Homework = GetScoreHomeWork(homeWorks, student.UserId, 10, scp.Id); //10 para no tener ponderación
                            if (ss.Homework == -1)
                            {
                                ss.Homework = 0;
                                ss.StatusHomework = "Tareas Sin Cerrar";
                            }
                            else if (ss.Homework == -2)
                            {
                                ss.Homework = 10; //10 para no tener ponderación
                                ss.StatusHomework = "Se asigno un porcentaje a tareas, pero no se programaron tareas.";
                            }
                            else
                                ss.HomemorksList = GetListHomeWork(homeWorks, student.UserId, scp.Id);

                        }
                        catch (Exception e)
                        {
                            ss.StatusHomework = "Error al calcular " + e.Message;
                        }
                    }
                    ss.ActivityList = new List<float>();
                    if (cp.Activity > 0)
                    {
                        try
                        {

                            ss.Activity = GetScoreActivities(activities, student.UserId, 10, scp.Id);//10 para no tener ponderación
                            if (ss.Activity == -1)
                            {
                                ss.Activity = 0;
                                ss.StatusActivity = "Actividades Sin Cerrar";
                            }
                            else if (ss.Activity == -2)
                            {
                                ss.Activity = 10;//10 para no tener ponderación
                                ss.StatusActivity = "Se asigno un porcentaje a actividades, pero no se programaron actividades.";
                            }
                            else
                                ss.ActivityList = GetListActivities(activities, student.UserId, scp.Id);
                        }
                        catch (Exception e)
                        {
                            ss.StatusActivity = "Error al calcular " + e.Message;
                        }

                    }
                    ss.AssistanceList = new List<float>();
                    if (cp.Assistance > 0)
                    {
                        if (scp.FinalPartial == 1)
                            tempSS = ss;

                        DateTime startdate = scp.StartDate;
                        int totalDays = 0;
                        while (startdate <= scp.EndDate)
                        {
                            totalDays +=
                                (studentGroup.Course.Monday && startdate.DayOfWeek == DayOfWeek.Monday) ? 1 :
                                (studentGroup.Course.Tuesday && startdate.DayOfWeek == DayOfWeek.Tuesday) ? 1 :
                                (studentGroup.Course.Wednesday && startdate.DayOfWeek == DayOfWeek.Wednesday) ? 1 :
                                (studentGroup.Course.Thursday && startdate.DayOfWeek == DayOfWeek.Thursday) ? 1 :
                                (studentGroup.Course.Friday && startdate.DayOfWeek == DayOfWeek.Friday) ? 1 :
                                (studentGroup.Course.Saturday && startdate.DayOfWeek == DayOfWeek.Saturday) ? 1 :
                                (studentGroup.Course.Sunday && startdate.DayOfWeek == DayOfWeek.Sunday) ? 1 : 0;
                            startdate = startdate.AddDays(1);
                        }
                        try
                        {

                            ss.Assistance = GetScoreAttendance(attendances, student.UserId, 10, totalDays, scp); //10 para no tener ponderación
                            ss.AssistanceList = GetListAttendance(attendances, student.UserId, totalDays, scp);
                        }
                        catch (Exception e)
                        {
                            ss.StatusAssistance = "Error al calcular " + e.Message;
                        }
                    }
                    ss.ExamList = new List<float>();
                    if (cp.Exam > 0)
                    {
                        try
                        {

                            ss.Exam = GetScoreExam(scheduleExams, student.UserId, 10, scp.Id);//10 para no tener ponderación
                            if (ss.Exam == -2)
                            {
                                ss.Exam = 10; //100 para no tener ponderación
                                ss.StatusExam = "Se asigno un porcentaje a examen, pero no se programaron exámenes.";
                            }
                            else
                                ss.ExamList = GetListExam(scheduleExams, student.UserId, scp.Id);
                        }
                        catch (Exception e)
                        {
                            ss.StatusExam = "Error al calcular " + e.Message;
                        }
                    }
                    Entities.StudentScore studentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && p.SchedulingPartialId == scp.Id);
                    if (studentScore != null)
                    {
                        ss.Id = studentScore.Id;
                        ss.Points = studentScore.Points > 100 ? 100 : studentScore.Points < -100 ? -100 : studentScore.Points;
                        ss.Status = studentScore.Status;
                        ss.Comment = studentScore.Comment;
                    }
                    else
                        ss.Points = 0;
                    //Ricardo - Truncar calificación a 2 digitos.
                    ss.Homework = (float)(Math.Truncate((double)ss.Homework * 100) / 100);
                    ss.Activity = (float)(Math.Truncate((double)ss.Activity * 100) / 100);
                    ss.Assistance = (float)(Math.Truncate((double)ss.Assistance * 10) / 10);
                    ss.Exam = (float)(Math.Truncate((double)ss.Exam * 100) / 100);

                    ss.Score = (ss.Homework * cp.Homework / 100) + (ss.Activity * cp.Activity / 100) + (ss.Assistance * cp.Assistance / 100) + (ss.Exam * cp.Exam / 100) + ss.Points;
                    //Ricardo - Truncar calificación a 1 digitos.
                    ss.Score = (float)(Math.Round((double)ss.Score * 10) / 10);



                    ss.Score = ss.Score > 10 ? 10 : ss.Score < 0 ? 0 : ss.Score;
                    fss.Homework += ss.Homework;
                    fss.Activity += ss.Activity;
                    fss.Assistance += ss.Assistance;
                    fss.Exam += ss.Exam;
                    fss.Score += ss.Score;
                    finalScoreStudent.StudentScores.Add(ss);
                }
                Entities.StudentScore fstudentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && !p.SchedulingPartialId.HasValue);
                if (fstudentScore != null)
                {
                    fss.Id = fstudentScore.Id;
                    fss.Points = fstudentScore.Points > 10 ? 10 : fstudentScore.Points < -10 ? -10 : fstudentScore.Points;
                    fss.Status = fstudentScore.Status;
                    fss.Comment = fstudentScore.Comment;
                }
                else
                    fss.Points = 0;
                fss.Homework = fss.Homework / schedulePartials.Count;
                fss.Activity = fss.Activity / schedulePartials.Count;
                if (tempSS == null)
                    fss.Assistance = fss.Assistance / schedulePartials.Count;
                else
                {

                    fss.Assistance = fss.Assistance / (schedulePartials.Count > 1 ? schedulePartials.Count - 1 : schedulePartials.Count);
                    tempSS.Assistance = fss.Assistance;
                }
                fss.Exam = fss.Exam / schedulePartials.Count;
                fss.Score = (fss.Score / schedulePartials.Count) + fss.Points;
                fss.Score = fss.Score > 10 ? 10 : fss.Score < 0 ? 0 : fss.Score;

                //Ricardo - Truncar calificación a 2 digitos.
                fss.Homework = (float)(Math.Round((double)fss.Homework * 100) / 100);
                fss.Activity = (float)(Math.Round((double)fss.Activity * 100) / 100);
                fss.Assistance = (float)(Math.Round((double)fss.Assistance * 100) / 100);
                fss.Exam = (float)(Math.Round((double)fss.Exam * 100) / 100);
                //Ricardo - Truncar calificación a 0 digitos.
                fss.Score = (float)Math.Round((double)fss.Score);

                finalScoreStudent.StudentScores.Add(fss);
            }
            else
            {
                ss = new DetailsStudentScoreDTO();
                DetailsStudentFinalScoreDTO finalScore = new DetailsStudentFinalScoreDTO
                {
                    UserId = student.UserId,
                    StudentId = student.Id,
                    StudentGroupId = studentGroupId,
                    StudenName = student.User.FullName,
                    StudentGroupDescription = student.StudentGroup.Description,
                    StudentScores = new List<DetailsStudentScoreDTO>(),
                    PercentageActivity = Convert.ToInt32(cp.Activity),
                    PercentageAssitance = Convert.ToInt32(cp.Assistance),
                    PercentageExam = Convert.ToInt32(cp.Exam),
                    PercentageHomework = Convert.ToInt32(cp.Homework),
                    //Armand
                    EmailUser = student.User.Email.ToString()

                };
                finalScores.Add(finalScore);
                ss.UserId = student.UserId;
                ss.StudentId = student.Id;
                ss.SchedulingPartialId = null;
                ss.NumberPartial = 100;
                ss.PartialDecription = "100";
                ss.StudentGroupId = studentGroupId;
                ss.UserId = student.UserId;
                ss.StudentId = student.Id;

                ss.PartialPeriod = studentGroup.Course.StartDate.ToString("dd/MM/yyyy") + " - " + studentGroup.Course.EndDate.ToString("dd/MM/yyyy");
                ss.Comment = "";

                if (cp.Homework > 0)
                {
                    try
                    {
                        ss.Homework = GetScoreHomeWork(homeWorks, student.UserId, 10);  //10 para no tener ponderación
                        if (ss.Homework == -1)
                        {
                            ss.Homework = 0;
                            ss.StatusHomework = "Tareas Sin Cerrar";
                        }
                        else if (ss.Homework == -2)
                        {
                            ss.Homework = 10; //10 para no tener ponderación
                            ss.StatusHomework = "Se asigno un porcentaje a tareas, pero no se programaron tareas.";
                        }
                        else
                        {
                            ss.HomemorksList = GetListHomeWork(homeWorks, student.UserId);
                        }
                    }
                    catch (Exception e)
                    {
                        ss.StatusHomework = "Error al calcular " + e.Message;
                    }
                }

                if (cp.Activity > 0)
                {
                    try
                    {
                        ss.Activity = GetScoreActivities(activities, student.UserId, 10); //10 para no tener ponderación
                        if (ss.Activity == -1)
                        {
                            ss.Activity = 0;
                            ss.StatusActivity = "Actividades Sin Cerrar";
                        }
                        else if (ss.Activity == -2)
                        {
                            ss.Activity = 10; //100 para no tener ponderación
                            ss.StatusActivity = "Se asigno un porcentaje a actividades, pero no se programaron actividades.";
                        }
                        else
                            ss.ActivityList = GetListActivities(activities, student.UserId);
                    }
                    catch (Exception e)
                    {
                        ss.StatusActivity = "Error al calcular " + e.Message;
                    }
                }

                if (cp.Assistance > 0)
                {
                    DateTime startdate = studentGroup.Course.StartDate;
                    int totalDays = 0;
                    while (startdate <= studentGroup.Course.EndDate)
                    {
                        totalDays +=
                            (studentGroup.Course.Monday && startdate.DayOfWeek == DayOfWeek.Monday) ? 1 :
                            (studentGroup.Course.Tuesday && startdate.DayOfWeek == DayOfWeek.Tuesday) ? 1 :
                            (studentGroup.Course.Wednesday && startdate.DayOfWeek == DayOfWeek.Wednesday) ? 1 :
                            (studentGroup.Course.Thursday && startdate.DayOfWeek == DayOfWeek.Thursday) ? 1 :
                            (studentGroup.Course.Friday && startdate.DayOfWeek == DayOfWeek.Friday) ? 1 :
                            (studentGroup.Course.Saturday && startdate.DayOfWeek == DayOfWeek.Saturday) ? 1 :
                            (studentGroup.Course.Sunday && startdate.DayOfWeek == DayOfWeek.Sunday) ? 1 : 0;
                        startdate = startdate.AddDays(1);
                    }
                    try
                    {
                        ss.AssistanceList = new List<float>();
                        ss.Assistance = GetScoreAttendance(attendances, student.UserId, 10, totalDays); //10 para no tener ponderación
                        ss.AssistanceList = GetListAttendance(attendances, student.UserId, totalDays);
                    }
                    catch (Exception e)
                    {
                        ss.StatusAssistance = "Error al calcular " + e.Message;
                    }
                }
                if (cp.Exam > 0)
                {
                    try
                    {
                        ss.Exam = GetScoreExam(scheduleExams, student.UserId, 10); //10 para no tener ponderación
                        if (ss.Exam == -2)
                        {
                            ss.Exam = 10; //10 para no tener ponderación
                            ss.StatusExam = "Se asigno un porcentaje a examen, pero no se programaron exámenes.";
                        }
                        else
                            ss.ExamList = GetListExam(scheduleExams, student.UserId);
                    }
                    catch (Exception e)
                    {
                        ss.StatusExam = "Error al calcular " + e.Message;
                    }
                }
                Entities.StudentScore studentScore = finalscoresStudents.FirstOrDefault(p => p.UserId == student.UserId && !p.SchedulingPartialId.HasValue);
                if (studentScore != null)
                {
                    ss.Id = studentScore.Id;
                    ss.Points = studentScore.Points > 10 ? 10 : studentScore.Points < -10 ? -10 : studentScore.Points;
                    ss.Status = studentScore.Status;
                    ss.Comment = studentScore.Comment;
                }
                else
                    ss.Points = 0;
                ss.Score = (ss.Homework * cp.Homework / 100) + (ss.Activity * cp.Activity / 100) + (ss.Assistance * cp.Assistance / 100) + (ss.Exam * cp.Exam / 100) + ss.Points;
                ss.Score = ss.Score > 100 ? 100 : ss.Score < 0 ? 0 : ss.Score;

                finalScore.StudentScores.Add(ss);
            }
        }

        return finalScores;
    }

    /// <summary>
    /// Obtener las calificaciones propuestas para estudiantes de un grupo con detalle
    /// </summary>
    /// <param name="studentGroupId"></param>
    /// <returns></returns>
    private async Task<Response> GetScoresGroupDetailsExcel(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO, int? userId = null)
    {
        using (ExcelPackage excel = new ExcelPackage())

        {

            var result = new Response();

            excel.Workbook.Worksheets.Add("Calificaciones");



            // Target a worksheet

            var worksheet = excel.Workbook.Worksheets["Calificaciones"];

            worksheet.Cells.Style.Font.Size = 10;

            worksheet.Cells.Style.Font.Name = "Calibri";



            List<DetailsStudentFinalScoreDTO> FinalScore = await GetScoresDetails(finalScoreFileRequirementsDTO.studentGroupId, userId);

            List<PartialRecordsDTO> partialRecordsDTOList = await GetPartialRecords(finalScoreFileRequirementsDTO.studentGroupId);

            FinalScoreHeadersDTO finalScoreHeadersDTO = await GetTitles(finalScoreFileRequirementsDTO);



            int rowInitialHeaders = 2;
            int columnInitialHeaders = 2;


            //Información del grupo
            worksheet.Cells[rowInitialHeaders, columnInitialHeaders].Value = "Evaluación continua"; // Encabezado

            worksheet.Cells[rowInitialHeaders + 2, columnInitialHeaders].Value = "CCT"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 3, columnInitialHeaders].Value = "Escuela"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 4, columnInitialHeaders].Value = "Nivel"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 5, columnInitialHeaders].Value = "Materia"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 6, columnInitialHeaders].Value = "Grado"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 7, columnInitialHeaders].Value = "Grupo"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 8, columnInitialHeaders].Value = "Turno"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 9, columnInitialHeaders].Value = "Ciclo Escolar"; // Encabezado
            worksheet.Cells[rowInitialHeaders + 10, columnInitialHeaders].Value = "Profesor"; // Encabezado



            worksheet.Cells[rowInitialHeaders + 2, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.CCT; // Encabezado
            worksheet.Cells[rowInitialHeaders + 3, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.School; // Encabezado
            worksheet.Cells[rowInitialHeaders + 4, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Level; // Encabezado
            worksheet.Cells[rowInitialHeaders + 5, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Subject; // Encabezado
            worksheet.Cells[rowInitialHeaders + 6, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Grade; // Encabezado
            worksheet.Cells[rowInitialHeaders + 7, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Group; // Encabezado
            worksheet.Cells[rowInitialHeaders + 8, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Turn; // Encabezado
            worksheet.Cells[rowInitialHeaders + 9, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.SchoolCycle; // Encabezado
            worksheet.Cells[rowInitialHeaders + 10, columnInitialHeaders + 1].Value = finalScoreHeadersDTO.Professor; // Encabezado

            worksheet.Cells[rowInitialHeaders, columnInitialHeaders, rowInitialHeaders + 10, columnInitialHeaders + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            int rowInitial = rowInitialHeaders + 12;
            int columnInitial = 2;

            int column = columnInitial + 2;

            int row = rowInitial;

            int columnend = columnInitial;

            //Inicia titulos

            worksheet.Cells[row + 2, column - 1].Value = "Porc"; // Titulo que no se repite

            worksheet.Cells[row + 2, column - 1].Style.Border.Top.Style = ExcelBorderStyle.Medium;

            worksheet.Cells[row + 2, column - 1].Style.Border.Right.Style = ExcelBorderStyle.Thick;

            worksheet.Cells[row + 2, column - 1].Style.Border.Left.Style = ExcelBorderStyle.Thick;



            foreach (PartialRecordsDTO PR in partialRecordsDTOList)

            {

                int newcolumn = column + 2 + (PR.MaximumElementsCount > 0 ? PR.MaximumElementsCount : 1); // 2 por los campos Promedio y calificación

                //fila de nombre del bloque

                worksheet.Cells[row, column, row, newcolumn].Merge = true;

                worksheet.Cells[row, column].Value = PR.PartialDescription;

                worksheet.Cells[row, column, row, newcolumn].Style.Border.Top.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row, newcolumn].Style.Border.Right.Style = ExcelBorderStyle.Thick;

                worksheet.Cells[row, column, row, newcolumn].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row, newcolumn].Style.Border.Left.Style = ExcelBorderStyle.Thick;

                worksheet.Cells[row, column, row, newcolumn].Style.Fill.PatternType = ExcelFillStyle.Solid;

                worksheet.Cells[row, column, row, newcolumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));

                worksheet.Cells[row, column, row, newcolumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //fila de periodo



                worksheet.Cells[row + 1, column, row + 1, newcolumn].Merge = true;

                worksheet.Cells[row + 1, column].Value = PR.PartialPeriod;

                worksheet.Cells[row + 1, column].Style.Border.Right.Style = ExcelBorderStyle.Thick;

                worksheet.Cells[row + 1, column].Style.Border.Left.Style = ExcelBorderStyle.Thick;

                worksheet.Cells[row + 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                //fila titulos

                worksheet.Cells[row + 2, column].Value = "No.";

                worksheet.Cells[row + 2, column + 1, row + 2, newcolumn - 2].Merge = true;

                worksheet.Cells[row + 2, column + 1].Value = "Calificaciones parciales";

                worksheet.Cells[row + 2, newcolumn - 1].Value = "Promedio";

                worksheet.Cells[row + 2, newcolumn].Value = "Calificación";

                worksheet.Cells[row + 2, column].Style.Border.Left.Style = ExcelBorderStyle.Thick;

                worksheet.Cells[row + 2, column, row + 2, newcolumn].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                column = newcolumn + 1; //recorro las columnas

            }



            worksheet.Cells[row + 1, column].Value = "Calificación";

            worksheet.Cells[row + 2, column].Value = "Ciclo";



            worksheet.Cells[row + 1, column, row + 2, column].Style.Border.Top.Style = ExcelBorderStyle.Medium;

            worksheet.Cells[row + 1, column, row + 2, column].Style.Border.Right.Style = ExcelBorderStyle.Medium;

            worksheet.Cells[row + 1, column, row + 2, column].Style.Border.Left.Style = ExcelBorderStyle.Medium;
            //Fin titulos

            //Inicia alumnos

            columnend = column;

            row += 3;

            foreach (DetailsStudentFinalScoreDTO detailsStudentFinalScore in FinalScore)

            {
                //Inicia fila del nombre de alumno
                column = columnInitial;

                worksheet.Cells[row, column, row, columnend].Merge = true;

                worksheet.Cells[row, column].Style.Font.Bold = true;

                worksheet.Cells[row, column, row, columnend].Style.Fill.PatternType = ExcelFillStyle.Solid;

                worksheet.Cells[row, column, row, columnend].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#C0C0C0"));

                worksheet.Cells[row, column].Value = detailsStudentFinalScore.StudenName;



                worksheet.Cells[row, column, row, columnend].Style.Border.Top.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row, columnend].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row, columnend].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row, columnend].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                //Fin fila del nombre de alumno

                //Inicio sección a evaluar y porcentajes

                worksheet.Cells[row + 1, column].Value = "Tareas";

                worksheet.Cells[row + 2, column].Value = "Actividades";

                worksheet.Cells[row + 3, column].Value = "Evaluación";

                worksheet.Cells[row + 4, column].Value = "Asistencia";

                worksheet.Cells[row + 5, column].Value = "Otros";

                worksheet.Cells[row + 6, column].Value = "Final";

                worksheet.Cells[row + 1, column, row + 6, column].Style.Border.Top.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row + 1, column, row + 6, column].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row + 1, column, row + 6, column].Style.Border.Left.Style = ExcelBorderStyle.Medium;

                column++;

                row++;

                worksheet.Cells[row, column].Value = detailsStudentFinalScore.PercentageHomework + " %";

                worksheet.Cells[row + 1, column].Value = detailsStudentFinalScore.PercentageActivity + " %";

                worksheet.Cells[row + 2, column].Value = detailsStudentFinalScore.PercentageExam + " %";

                worksheet.Cells[row + 3, column].Value = detailsStudentFinalScore.PercentageAssitance + " %";

                worksheet.Cells[row + 4, column].Value = "5%";

                worksheet.Cells[row + 5, column].Value = "105%";

                worksheet.Cells[row + 5, column].Style.Font.Bold = true;

                worksheet.Cells[row, column, row + 5, column].Style.Border.Top.Style = ExcelBorderStyle.Medium;

                worksheet.Cells[row, column, row + 5, column].Style.Border.Right.Style = ExcelBorderStyle.Medium;


                worksheet.Cells[row, column, row + 5, column].Style.Border.Left.Style = ExcelBorderStyle.Medium;

                //Fin sección a evaluar y porcentajes

                column++;

                int partial = 0;

                //Inicia escribir calificaciones del alumno por parcial

                foreach (DetailsStudentScoreDTO detailsStudentScoreDTO in detailsStudentFinalScore.StudentScores)

                {

                    if (detailsStudentFinalScore.StudentScores.Count() - 1 == partial) // Si es el ultimo parcial

                    {

                        //Inicia información de calificaciones finales
                        worksheet.Cells[row, column].Value = ((Math.Round((double)(detailsStudentScoreDTO.Homework * detailsStudentFinalScore.PercentageHomework / 10))) / 10).ToString("N1");

                        worksheet.Cells[row + 1, column].Value = ((Math.Round((double)(detailsStudentScoreDTO.Activity * detailsStudentFinalScore.PercentageActivity / 10))) / 10).ToString("N1");

                        worksheet.Cells[row + 2, column].Value = ((Math.Round((double)(detailsStudentScoreDTO.Exam * detailsStudentFinalScore.PercentageExam / 10))) / 10).ToString("N1");

                        worksheet.Cells[row + 3, column].Value = ((Math.Round((double)(detailsStudentScoreDTO.Assistance * detailsStudentFinalScore.PercentageAssitance / 10))) / 10).ToString("N1");

                        worksheet.Cells[row + 4, column].Value = ((Math.Round((double)(detailsStudentScoreDTO.Points * 10))) / 10).ToString("N1");

                        worksheet.Cells[row + 5, column].Value = (detailsStudentScoreDTO.Score).ToString("N0");

                        worksheet.Cells[row, column, row + 5, column].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                        worksheet.Cells[row, column, row + 5, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row + 5, column].Style.Font.Bold = true;

                        //Fin información de calificaciones finales

                    }

                    else

                    {
                        //Inicia información de calificacion

                        //Inicia porcentaje por aspecto a evaluar
                        worksheet.Cells[row, column].Value = detailsStudentScoreDTO.HomemorksList.Count();

                        worksheet.Cells[row + 1, column].Value = detailsStudentScoreDTO.ActivityList.Count();

                        worksheet.Cells[row + 2, column].Value = detailsStudentScoreDTO.ExamList.Count();

                        worksheet.Cells[row + 3, column].Value = detailsStudentScoreDTO.AssistanceList.Count();

                        worksheet.Cells[row, column].Style.Font.Bold = true;

                        worksheet.Cells[row + 1, column].Style.Font.Bold = true;

                        worksheet.Cells[row + 2, column].Style.Font.Bold = true;

                        worksheet.Cells[row + 3, column].Style.Font.Bold = true;

                        //Fin porcentaje por aspecto a evaluar


                        column++;

                        int incremental = 0;

                        //Inicia tareas
                        foreach (float value in detailsStudentScoreDTO.HomemorksList)

                        {

                            worksheet.Cells[row, column + incremental].Value = (Math.Truncate((double)value * 10) / 10).ToString("N1");
                            worksheet.Cells[row, column + incremental].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            incremental++;

                        }
                        //Fin tareas
                        incremental = 0;
                        //Inicia actividades
                        foreach (float value in detailsStudentScoreDTO.ActivityList)

                        {

                            worksheet.Cells[row + 1, column + incremental].Value = (Math.Truncate((double)value * 10) / 10).ToString("N1");
                            worksheet.Cells[row + 1, column + incremental].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            incremental++;

                        }
                        //Fin actividades
                        incremental = 0;
                        //Inicia examenes
                        foreach (float value in detailsStudentScoreDTO.ExamList)

                        {

                            worksheet.Cells[row + 2, column + incremental].Value = (Math.Truncate((double)value * 10) / 10).ToString("N1");
                            worksheet.Cells[row + 2, column + incremental].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            incremental++;

                        }
                        //Fin examenes
                        incremental = 0;
                        //Inicia asistencias
                        foreach (float value in detailsStudentScoreDTO.AssistanceList)

                        {

                            worksheet.Cells[row + 3, column + incremental].Value = (Math.Truncate((double)value * 10) / 10).ToString("N1");
                            worksheet.Cells[row + 3, column + incremental].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            incremental++;

                        }
                        //Fin asistencias

                        //Inicia porcentajes y calificación por parcial
                        column = column + (partialRecordsDTOList[partial].MaximumElementsCount > 0 ? partialRecordsDTOList[partial].MaximumElementsCount : 1);

                        worksheet.Cells[row, column].Value = detailsStudentScoreDTO.Homework.ToString("N2");

                        worksheet.Cells[row + 1, column].Value = detailsStudentScoreDTO.Activity.ToString("N2");

                        worksheet.Cells[row + 2, column].Value = detailsStudentScoreDTO.Exam.ToString("N2");

                        worksheet.Cells[row + 3, column].Value = detailsStudentScoreDTO.Assistance.ToString("N2");

                        worksheet.Cells[row, column, row + 5, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        column++;

                        worksheet.Cells[row, column].Value = ((Math.Truncate((double)(detailsStudentScoreDTO.Homework * detailsStudentFinalScore.PercentageHomework))) / 100).ToString("N2");

                        worksheet.Cells[row + 1, column].Value = ((Math.Truncate((double)(detailsStudentScoreDTO.Activity * detailsStudentFinalScore.PercentageActivity))) / 100).ToString("N2");

                        worksheet.Cells[row + 2, column].Value = ((Math.Truncate((double)(detailsStudentScoreDTO.Exam * detailsStudentFinalScore.PercentageExam))) / 100).ToString("N2");

                        worksheet.Cells[row + 3, column].Value = ((Math.Truncate((double)(detailsStudentScoreDTO.Assistance * detailsStudentFinalScore.PercentageAssitance))) / 100).ToString("N2");

                        worksheet.Cells[row + 4, column].Value = (detailsStudentScoreDTO.Points).ToString("N2");

                        worksheet.Cells[row + 5, column].Value = (detailsStudentScoreDTO.Score).ToString("N1");

                        worksheet.Cells[row, column, row + 5, column].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                        worksheet.Cells[row, column, row + 5, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row + 5, column].Style.Font.Bold = true;

                        //Fin porcentajes y calificación por parcial
                        column++;

                        partial++;
                    }

                }
                //Termina escribir calificaciones del alumno por parcial
                worksheet.Cells[row + 5, columnInitial, row + 5, column].Style.Border.Bottom.Style = ExcelBorderStyle.Medium; //Borde final de las calificaciones
                row = row + 6; // Siguiente alumno

            }
            //Fin de alumnos
            worksheet.Cells[rowInitialHeaders, columnInitialHeaders, row, column].AutoFitColumns();
            byte[] bExcel = excel.GetAsByteArray();

            string base64String = System.Convert.ToBase64String(bExcel);
            string filename = "Calificacion_Final_Grado_" + finalScoreHeadersDTO.Grade + "_Grupo_" + finalScoreHeadersDTO.Group;

            result.Data = filename + '|' + base64String;
            result.Success = true;
            result.Message = "Contenido descargado correctamente";


            return result;

        }
    }
    private async Task<FinalScoreHeadersDTO> GetTitles(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO)
    {
        FinalScoreHeadersDTO finalScoreHeadersDTO = new FinalScoreHeadersDTO();
        try
        {

            finalScoreHeadersDTO.SchoolCycle = finalScoreFileRequirementsDTO.schoolCycle;
            finalScoreHeadersDTO.Turn = finalScoreFileRequirementsDTO.turn;
            int? courseId = (await _StudentGroupRepository.FindAsync(q => q.Id == finalScoreFileRequirementsDTO.studentGroupId))?.CourseId;
            int? NodeId = (await _courseRepository.FindAsync(p => p.Id == courseId))?.NodeId;
            int? UserId = (await _courseRepository.FindAsync(p => p.Id == courseId))?.UserId;

            finalScoreHeadersDTO.School = (await _courseRepository.FindAsync(p => p.Id == courseId))?.Institution;
            finalScoreHeadersDTO.Subject = (await _courseRepository.FindAsync(p => p.Id == courseId))?.Name;


            finalScoreHeadersDTO.Grade = (await _courseExtensionRepository.FindAsync(p => p.CourseId == courseId))?.Grade;
            finalScoreHeadersDTO.Group = (await _studentGroupRepository.FindAsync(p => p.Id == finalScoreFileRequirementsDTO.studentGroupId))?.Description;
            finalScoreHeadersDTO.Level = (await _nodeRepository.FindAsync(p => p.Id == NodeId))?.Description;
            finalScoreHeadersDTO.Professor = (await _UserRepository.FindAsync(p => p.Id == UserId))?.FullName;
            List<UserCCT> userCCTs = (await _userCCTRepository.FindAllAsync(p => p.UserId == UserId && p.NodeId == NodeId)).ToList();
            foreach (UserCCT userCCT in userCCTs)
            {
                finalScoreHeadersDTO.CCT += userCCT.CCT;
                if (userCCT.Id != userCCTs.Last().Id)
                    finalScoreHeadersDTO.CCT += ",";
            }

        }
        catch (Exception e)
        {
        }
        return finalScoreHeadersDTO;
    }

}
