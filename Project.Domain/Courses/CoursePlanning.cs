﻿using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using Project.Repositories.Courses;
using Project.Entities.Responses;
using Project.Repositories.Services;
using Project.Entities.Enums;

namespace Project.Domain.Courses
{
    public class CoursePlanning : ICoursePlanning
    {
        private readonly ICoursePlanningRepository _CoursePlanningRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;
        private readonly IPushNotificationService _srvPushNotification;


        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="CoursePlanningRepository"></param>
        /// <param name="studentGroupRepository"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>

        public CoursePlanning(ICoursePlanningRepository CoursePlanningRepository, IStudentGroupRepository studentGroupRepository, IUnitOfWork unitOfWork, IStudentRepository StudentRepository, ISchedulingPartialRepository schedulingPartialRepository, IPushNotificationService srvPushNotification)
        {
            this._CoursePlanningRepository = CoursePlanningRepository;
            this._studentGroupRepository = studentGroupRepository;
            this._unitOfWork = unitOfWork;
            this._studentRepository = StudentRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
            this._srvPushNotification = srvPushNotification;
        }



        // <summary>
        /// Agregar una  nueva planeación de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(CoursePlanningSaveRequest request)
        {

            var result = new Response();

            try
            {
                Entities.CoursePlanning courseplanning = new Entities.CoursePlanning();
                courseplanning.Homework = request.Homework;
                courseplanning.Exam = request.Exam;
                courseplanning.Activity = request.Activity;
                courseplanning.Assistance = request.Assistance;
                courseplanning.NumberPartial = request.NumberPartial;

                var group = await _studentGroupRepository.FindAsync(x => x.Id == request.StudentGroupId);

                if (group != null)
                {
                    courseplanning.StudentGroupId = request.StudentGroupId;
                }
                else
                {
                    throw new Exception("Se necesita crear  un grupo de curso");
                }


                _CoursePlanningRepository.Add(courseplanning);
                await _unitOfWork.SaveChangesAsync();



                result.Data = courseplanning.Id;
                result.Success = true;
                result.Message = "Registro agregado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar el nuevo plan de curso: { ex.Message}";
            }

            return result;
        }


        // <summary>
        /// Actualiza una  nueva planeación de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CoursePlanningUpdateRequest request)
        {
            var response = new Response();
            try
            {

                Entities.CoursePlanning cp = new Entities.CoursePlanning();
                cp = await _CoursePlanningRepository.FindAsync(x => x.StudentGroupId == request.StudentGroupId);
                if (cp == null)
                {
                    throw new Exception("No se encontraron datos del curso proporcionado");
                }
                else
                {
                    cp.Homework = request.Homework;
                    cp.Exam = request.Exam;
                    cp.Assistance = request.Assistance;
                    cp.Activity = request.Activity;
                    cp.NumberPartial = request.NumberPartial;

                }
                _CoursePlanningRepository.Update(cp);
                await _unitOfWork.SaveChangesAsync();




                #region PUSHNOTIFICATION

                var group = await _studentGroupRepository.FindAsync(x => x.Id == cp.StudentGroupId);
                var data = new
                {
                    groupName = group.Description,
                    homeWork = "",
                    date = DateTime.Now.ToShortDateString(),
                };
                var userIds = group.Students.Select(x => x.UserId).ToList();
                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewScheduledCourse, data);

                #endregion




                response.Data = cp.Id;
                response.Success = true;
                response.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo curso: { ex.Message }";
            }

            return response;
        }

        // <summary>
        /// Elimina una  nueva planeación de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int Id)
        {
            var result = new Response();

            try
            {
                var course = await _CoursePlanningRepository.FindAsync(x => x.Id == Id);

                if (course == null)
                    throw new Exception("No se encontró datos del curso");

                if (course.StudentGroupId > 0)
                    throw new Exception("No se puede eliminar el curso, hay grupos asignados");

                _CoursePlanningRepository.Delete(course);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos del grupo eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        // <summary>
        /// Regresa todos los registros de planeacióno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _CoursePlanningRepository.GetAll().
                    Select(u => new {
                        u.Id,
                        u.NumberPartial,
                        u.Exam,
                        u.Activity,
                        u.Homework,
                        u.StudentGroupId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }


        // <summary>
        /// Regresa todos los registros de planeacióno por Id 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> GetByGroup(int id)
        {
            var result = new Response();

            try
            {

                var courses = await _CoursePlanningRepository.FindByAsyn(r => r.StudentGroupId==id);


                if (courses != null)
                {


                    result.Data = courses.Select(c => new
                    {
                        c.Id,
                        c.Homework,
                        c.Exam,
                        c.Assistance,
                        c.Activity,
                        c.NumberPartial,
                        c.StudentGroupId
                        
                    });
                }

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

        }
        // <summary>
        /// Regresa todoslos cursos planeados por id
        /// </summary>
        /// <param name="

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _CoursePlanningRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        // <summary>
        /// Regresa todoslos cursos planeados por el identificador del studiante
        /// </summary>
        /// <param name="

        public async Task<Response> GetByStudentId(int StudentId,int StudentGroupId)
        {
            var result = new Response();

            try
            { var student = await _studentRepository.FindAsync(r => r.UserId == StudentId && r.StudentGroupId == StudentGroupId);

                if (student != null)
                {
                    var  CoursePlanning = await _CoursePlanningRepository.FindByAsyn(r => r.StudentGroupId == student.StudentGroupId);

                if (CoursePlanning != null)
                 {                                     
                        result.Data = CoursePlanning.Select(c => new
                        {
                            c.Id,
                            c.Homework,
                            c.Exam,
                            c.Assistance,
                            c.Activity,
                            c.NumberPartial,
                            c.StudentGroupId,
                            schedulingPartial = c.SchedulingPartials.Select(p => new Entities.SchedulingPartial
                            {
                                Id = p.Id,
                                Description = p.Description,
                                StartDate = p.StartDate,
                                EndDate = p.EndDate,
                                FinalPartial = p.FinalPartial,
                                CoursePlanningId = p.CoursePlanningId,
                                RegisterDate=p.RegisterDate,
                                UpdateDate=p.UpdateDate,

                            })
                        });
                    }
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

    }

}
