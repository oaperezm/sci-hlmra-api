﻿using Project.Domain.Core;
using Project.Entities.Responses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VReportExam :IVReportExam
    {

        private readonly IVReportExamRepository _vreportExamRepository;

        public VReportExam(IVReportExamRepository vreportExamRepository)
        {
            _vreportExamRepository = vreportExamRepository;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var reportCoordinator = new List<VCoordinatorReport>();
                result.Data = _vreportExamRepository.GetAll().Select(x => new
                {
                    x.Id,
                    cct = x.cct,
                    email = x.email,
                    userName = x.userName,
                    nameSchool = x.nameSchool,
                    level = x.level,
                    questionbank = x.questionbank,
                    totalquestion = x.totalquestion,
                    signature = x.signature,
                    starDate = x.starDate,
                    //endDate = x.endDate
                    
                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
