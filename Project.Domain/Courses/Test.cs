﻿using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Test : ITest
    {
        private readonly ITestRepository _testRepository;
        private readonly ITestAnswerRepository _testAnswerRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IUnitOfWork _unitOfWork;

        public Test(ITestRepository testRepository,
                     ITestAnswerRepository testAnswerRepository,
                     IExamScheduleRepository examScheduleRepository,
                     IPushNotificationService srvPushNotification,
                     IUnitOfWork unitOfWork)
        {

            this._testAnswerRepository = testAnswerRepository;
            this._testRepository = testRepository;
            this._unitOfWork = unitOfWork;
            this._examScheduleRepository = examScheduleRepository;
            this._srvPushNotification = srvPushNotification;
        }

        /// <summary>
        /// Agregar una nueva aplicación de examen
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TestSaveRequest request, EnumRol rol)
        {
            var result = new Response();
            try
            {
                if (rol == EnumRol.TeacherRA || rol == EnumRol.StudentRA)
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                    var fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);

                    var scheduleExam = await _examScheduleRepository.FindAsync(x => x.Id == request.ExamScheduleId);
                    if (scheduleExam == null)
                        throw new Exception("No se encontró registro de la programación del examen");

                    if (fecha >= scheduleExam.BeginApplicationDate && fecha <= scheduleExam.EndApplicationDate.AddDays(1).AddSeconds(-1)
                        && ((!scheduleExam.BeginApplicationTime.HasValue && !scheduleExam.EndApplicationTime.HasValue)
                        || (fecha.TimeOfDay >= scheduleExam.BeginApplicationTime.Value.TimeOfDay && fecha.TimeOfDay <= scheduleExam.EndApplicationTime.Value.TimeOfDay)))
                        scheduleExam.IsExamAvailable = true;

                    Entities.Test test = await _testRepository.FindAsync(x => x.UserId == request.UserId && x.ExamScheduleId == request.ExamScheduleId);

                    if (scheduleExam.MinutesExam.HasValue)
                    {
                        if (test != null)
                        {
                            if (!test.IsCompleted)
                            {

                                if (scheduleExam.ExamScheduleTypeId != (int)EnumExamScheduleType.Applied)
                                {
                                    scheduleExam.ExamScheduleTypeId = (int)EnumExamScheduleType.Applied;
                                    _examScheduleRepository.Update(scheduleExam);
                                }
                                decimal examScore = 0;
                                int countCorrect = 0;
                                var answers = test.Answers.ToList();

                                foreach (var q in scheduleExam.TeacherExam.TeacherExamQuestion)
                                {
                                    bool isCorrectAnswer = true;
                                    foreach (var qa in q.Question.Answers)
                                    {
                                        if (q.Question.QuestionTypeId != 3)
                                        {
                                            if (answers.Any(p => p.AnswerId == qa.Id))
                                                isCorrectAnswer = qa.IsCorrect;
                                            else if (qa.IsCorrect)
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                        else
                                        {
                                            if (!answers.Any(ta => ta.AnswerId == qa.Id && ta.Explanation == qa.RelationDescription))
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                    }
                                    if (isCorrectAnswer)
                                    {
                                        countCorrect++;
                                        examScore += q.Value;
                                    }
                                }

                                test.IsCompleted = true;
                                test.Score = examScore;
                                test.CorrectAnswers = countCorrect;
                                _testRepository.Update(test);
                                await _unitOfWork.SaveChangesAsync();
                                result.Data = test.Id;
                                result.Success = true;
                                result.Message = "Examen guardado correctamente";
                            }
                            else
                                throw new Exception("El examen ya ha sido completado");
                        }
                        else
                            throw new Exception("No se encontró registro de un examen iniciado");
                    }
                    else
                    {
                        if (test == null)
                        {
                            if (scheduleExam.IsExamAvailable)
                            {
                                if (request.Answers == null || request.Answers.Count == 0)
                                    throw new Exception("No contiene respuestas para registrar");

                                test = new Entities.Test
                                {
                                    ClientId = request.ClientId,
                                    ExamScheduleId = request.ExamScheduleId,
                                    StartDate = fecha,
                                    RegisterDate = fecha,
                                    IsCompleted = true,
                                    UserId = request.UserId
                                };
                                request.Answers.ForEach(a =>
                                {
                                    var answer = new Entities.TestAnswer();

                                    answer.AnswerId = a.AnswerId;
                                    answer.TestId = test.Id;
                                    answer.Explanation = a.Explanation;
                                    answer.ResponseDate = fecha;
                                    answer.TeacherExamQuestionId = a.TeacherExamQuestionId;
                                    test.Answers.Add(answer);
                                });

                                decimal examScore = 0;
                                int countCorrect = 0;
                                foreach (var q in scheduleExam.TeacherExam.TeacherExamQuestion)
                                {
                                    bool isCorrectAnswer = true;
                                    foreach (var qa in q.Question.Answers)
                                    {
                                        if (q.Question.QuestionTypeId != 3)
                                        {
                                            if (request.Answers.Any(p => p.AnswerId == qa.Id))
                                                isCorrectAnswer = qa.IsCorrect;
                                            else if (qa.IsCorrect)
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                        else
                                        {
                                            if (!request.Answers.Any(ta => ta.AnswerId == qa.Id && ta.Explanation == qa.RelationDescription))
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                    }
                                    if (isCorrectAnswer)
                                    {
                                        countCorrect++;
                                        examScore += q.Value;
                                    }
                                }

                                test.Score = examScore;
                                test.CorrectAnswers = countCorrect;
                                _testRepository.Add(test);

                                scheduleExam.ExamScheduleTypeId = (int)EnumExamScheduleType.Applied;
                                _examScheduleRepository.Update(scheduleExam);


                                //Entities.ExamScore eexamScore = new Entities.ExamScore();
                                //eexamScore.ExamScheduleId = request.ExamScheduleId;
                                //eexamScore.UserId = request.UserId;
                                //eexamScore.Score = examScore.ToString();
                                //eexamScore.CorrectAnswers = countCorrect;
                                //eexamScore.Comments = "";
                                //eexamScore.RegisterDate = fecha;
                                //_examScoreRepository.Add(examScore);

                                await this._unitOfWork.SaveChangesAsync();


                                await _unitOfWork.SaveChangesAsync();

                                result.Data = test.Id;
                                result.Success = true;
                                result.Message = "Examen guardado correctamente";
                            }
                            else
                                throw new Exception("Examen no disponible");
                        }
                        else
                            throw new Exception("El examen ya ha sido completado");
                    }
                }
                else
                {

                    if (request.Answers.Count == 0)
                        throw new Exception("No contiene respuestas para registrar");

                    var test = new Entities.Test
                    {
                        ClientId = request.ClientId,
                        ExamScheduleId = request.ExamScheduleId,
                        RegisterDate = DateTime.Today,
                        IsCompleted = false,
                        UserId = request.UserId
                    };

                    if (request.Answers != null && request.Answers.Count > 0)
                    {
                        request.Answers.ForEach(a =>
                        {
                            var answer = new Entities.TestAnswer();

                            answer.AnswerId = a.AnswerId;
                            answer.TestId = test.Id;
                            answer.Explanation = a.Explanation;
                            test.Answers.Add(answer);
                        });
                    }

                    var scheduleExam = await _examScheduleRepository.FindAsync(x => x.Id == request.ExamScheduleId);

                    if (scheduleExam != null)
                    {
                        scheduleExam.ExamScheduleTypeId = (int)EnumExamScheduleType.Applied;
                        _examScheduleRepository.Update(scheduleExam);
                    }

                    _testRepository.Add(test);
                    await _unitOfWork.SaveChangesAsync();

                    result.Data = test.Id;
                    result.Success = true;
                    result.Message = "Examen guardado correctamente";
                }
            }
            catch (System.Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar el examen: {ex.Message}";
            }
            return result;

        }

        /// <summary>
        /// Obtener las respuesta de un alumno y el examen
        /// </summary>
        /// <param name="examScheduleId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserAndExamSchedule(int examScheduleId, int userId, EnumRol rol)
        {

            var result = new Response();
            try
            {
                var testData = await _testRepository.FindAllAsync(t => t.ExamScheduleId == examScheduleId && t.UserId == userId);

                if (testData == null || testData.Count == 0)
                    throw new Exception("El alumno no ha contestado el examen solicitado");

                var data = testData.FirstOrDefault();
                var examSchedule = data.ExamSchedule;
                if (examSchedule.TeacherExam == null)
                    throw new Exception("La programación de examen no tiene un examen asignado");
                if (examSchedule.TeacherExam.TeacherExamQuestion.Count == 0)
                    throw new Exception("No se encontraron preguntas relacionadas el examen programado");
                if (rol == EnumRol.TeacherRA || rol == EnumRol.StudentRA)
                {
                    var test = new
                    {
                        ExamSchedule = new Entities.ExamSchedule
                        {
                            Id = examSchedule.Id,
                            SchedulingPartialId = examSchedule.SchedulingPartialId,
                            Description = examSchedule.Description,
                            ApplicationDate = examSchedule.BeginApplicationDate,
                            BeginApplicationDate = examSchedule.BeginApplicationDate,
                            EndApplicationDate = examSchedule.EndApplicationDate,
                            BeginApplicationTime = examSchedule.BeginApplicationTime,
                            EndApplicationTime = examSchedule.EndApplicationTime,
                            MinutesExam = examSchedule.MinutesExam,
                        },
                        TeacherExamn = new Entities.TeacherExam
                        {
                            Description = examSchedule.TeacherExam.Description,
                            Weighting = new Entities.Weighting
                            {
                                Id = examSchedule.TeacherExam.Weighting.Id,
                                Description = examSchedule.TeacherExam.Weighting.Description,
                                Unit = examSchedule.TeacherExam.Weighting.Unit,
                            },
                            MaximumExamScore = examSchedule.TeacherExam.MaximumExamScore,
                            IsAutomaticValue = examSchedule.TeacherExam.IsAutomaticValue,
                            TeacherExamTypes = new Entities.TeacherExamType
                            {
                                Id = examSchedule.TeacherExam.TeacherExamTypes.Id,
                                Description = examSchedule.TeacherExam.TeacherExamTypes.Description
                            },
                            TeacherExamQuestion = examSchedule.TeacherExam == null ? null : examSchedule.TeacherExam.TeacherExamQuestion.Select(q => new Entities.TeacherExamQuestion
                            {
                                TeacherExamId = q.TeacherExamId,
                                QuestionId = q.QuestionId,
                                Value = q.Value,
                                UserEdited = q.UserEdited,
                                Question = new Entities.Question
                                {
                                    Id = q.Question.Id,
                                    Content = q.Question.Content,
                                    Explanation = q.Question.Explanation,
                                    UrlImage = q.Question.UrlImage,
                                    ImageHeight = q.Question.ImageHeight,
                                    ImageWidth = q.Question.ImageWidth,
                                    QuestionTypeId = q.Question.QuestionTypeId,
                                    QuestionType = new Entities.QuestionType
                                    {
                                        Id = q.Question.QuestionType.Id,
                                        Description = q.Question.QuestionType.Description
                                    },
                                    Answers = data.Answers.Where(x => x.Answer.QuestionId == q.Question.Id).Select(a => new Entities.Answer
                                    {
                                        Id = a.Answer.Id,
                                        Description = a.Answer.Description,
                                        IsCorrect = a.Answer.IsCorrect,
                                        RelationDescription = a.Explanation,
                                        Active = a.Answer.Active,
                                        Order = a.Answer.Order,
                                        ImageWidth = a.Answer.ImageWidth,
                                        ImageHeight = a.Answer.ImageHeight,
                                    }).ToList(),
                                    QuestionAnswers = q.Question.Answers.Select(a => new Entities.Answer
                                    {
                                        Id = a.Id,
                                        Description = a.Description,
                                        IsCorrect = a.IsCorrect,
                                        RelationDescription = a.RelationDescription,
                                        Active = a.Active,
                                        Order = a.Order,
                                        ImageWidth = a.ImageWidth,
                                        ImageHeight = a.ImageHeight,
                                    }).ToList().OrderBy(p => p.Id).ToList(),
                                }
                            }).ToList()
                        }
                    };

                    decimal examScore = 0;
                    if (rol == EnumRol.StudentRA)
                    {
                        foreach (var q in test.TeacherExamn.TeacherExamQuestion)
                        {
                            bool isCorrectAnswer = true;
                            foreach (var qa in q.Question.QuestionAnswers)
                            {
                                if (q.Question.QuestionTypeId != 3)
                                {
                                    if (q.Question.Answers.Any(p => p.Id == qa.Id))
                                        isCorrectAnswer = qa.IsCorrect;
                                    else if (qa.IsCorrect)
                                        isCorrectAnswer = false;
                                    if (!isCorrectAnswer)
                                        break;
                                }
                                else
                                {
                                    if (!q.Question.Answers.Any(p => p.Id == qa.Id && p.RelationDescription == qa.RelationDescription))
                                        isCorrectAnswer = false;
                                    if (!isCorrectAnswer)
                                        break;
                                }

                            }
                            if (isCorrectAnswer)
                                examScore += q.Value;
                            q.Value = 0;
                            foreach (var a in q.Question.QuestionAnswers)
                            {
                                a.IsCorrect = false;
                                a.RelationDescription = "";
                            }
                            foreach (var a in q.Question.Answers)
                                a.IsCorrect = false;
                        }
                    }
                    result.Data = new
                    {
                        examScore = Math.Round(examScore, 2),
                        isStudent = rol == EnumRol.StudentRA,
                        test.ExamSchedule,
                        test.TeacherExamn,
                    };
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";
                }
                else
                {
                    var test = new Entities.DTO.ResumeTestDTO
                    {
                        ExamSchedule = new Entities.ExamSchedule
                        {
                            Id = examSchedule.Id,
                            Description = examSchedule.Description,
                            ApplicationDate = examSchedule.ApplicationDate
                        },
                        TeacherExamn = new Entities.TeacherExam
                        {
                            Description = examSchedule.TeacherExam.Description,
                            TeacherExamTypes = new Entities.TeacherExamType
                            {
                                Id = examSchedule.TeacherExam.TeacherExamTypes.Id,
                                Description = examSchedule.TeacherExam.TeacherExamTypes.Description
                            },
                            TeacherExamQuestion = examSchedule.TeacherExam.TeacherExamQuestion.Select(
                            q => new Entities.TeacherExamQuestion
                            {
                                Question = new Entities.Question
                                {
                                    Id = q.Question.Id,
                                    Content = q.Question.Content,
                                    Explanation = q.Question.Explanation,
                                    UrlImage = q.Question.UrlImage,
                                    ImageHeight = q.Question.ImageHeight,
                                    ImageWidth = q.Question.ImageWidth,
                                    QuestionType = new Entities.QuestionType
                                    {
                                        Id = q.Question.QuestionType.Id,
                                        Description = q.Question.QuestionType.Description
                                    },
                                    Answers = data.Answers.Where(x => x.Answer.QuestionId == q.Question.Id)
                                                         .Select(a => new Entities.Answer
                                                         {
                                                             Id = a.Answer.Id,
                                                             Description = a.Answer.Description,
                                                             IsCorrect = a.Answer.IsCorrect,
                                                             RelationDescription = a.Answer.RelationDescription,
                                                             Active = a.Answer.Active,
                                                             Order = a.Answer.Order,
                                                             ImageWidth = a.Answer.ImageWidth,
                                                             ImageHeight = a.Answer.ImageHeight
                                                         }).ToList()
                                }
                            }).ToList()
                        }
                    };

                    result.Data = test;
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";

                }

                // Enviar push notification
                // await _srvPushNotification.CreateNotification("global", (int)EnumPush.NewScheduleHomeWork, new { testId = test.ExamSchedule.Id }); ;

            }
            catch (System.Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener el examen y sus respuestas: {ex.Message}";
            }
            return result;

        }

        public async Task<Response> SaveAnswerQuestion(TestAnswerQuestionRequest request, EnumRol rol)
        {
            var result = new Response();
            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);

                if (rol == EnumRol.TeacherRA || rol == EnumRol.StudentRA)
                {
                    Entities.Test test = await _testRepository.FindAsync(x => x.UserId == request.UserId && x.ExamScheduleId == request.ExamScheduleId);

                    if (test != null)
                    {
                        if (test.ExamSchedule.TeacherExam == null)
                            throw new Exception("No se encontro información del examen programado");
                        if (!test.ExamSchedule.MinutesExam.HasValue)
                            throw new Exception("Duración del examen no disponible.");

                        if (!test.IsCompleted && test.StartDate.Value.AddMinutes(test.ExamSchedule.MinutesExam.Value) < fecha)
                        {
                            test.IsCompleted = true;
                            _testRepository.Update(test);
                            await _unitOfWork.SaveChangesAsync();
                        }
                        if (!test.IsCompleted)
                        {

                            Entities.TeacherExamQuestion teacherExamQ = test.ExamSchedule.TeacherExam.TeacherExamQuestion
                                .FirstOrDefault(p => p.Question.Answers.Any(x => x.Id == request.AnswerId));


                            if (teacherExamQ != null)
                            {
                                var totalCorrects = 0;
                                foreach (var a in teacherExamQ.Question.Answers)
                                    totalCorrects += a.IsCorrect ? 1 : 0;

                                teacherExamQ.Question.IsMultiple = totalCorrects > 1;

                                Entities.TestAnswer ta;

                                if (teacherExamQ.Question.QuestionTypeId != 3)
                                {
                                    if (!teacherExamQ.Question.IsMultiple)
                                    {
                                        ta = await _testAnswerRepository.FindAsync(p => p.TestId == test.Id && (p.TeacherExamQuestionId ?? 0) == teacherExamQ.Id);
                                        if (ta != null)
                                        {
                                            ta.AnswerId = request.AnswerId;
                                            ta.ResponseDate = fecha;
                                            ta.Explanation = request.Explanation ?? "";
                                            _testAnswerRepository.Update(ta);
                                        }
                                    }
                                    else
                                    {
                                        ta = await _testAnswerRepository.FindAsync(p => p.TestId == test.Id && (p.TeacherExamQuestionId ?? 0) == teacherExamQ.Id
                                        && p.AnswerId == request.AnswerId);

                                        if (ta != null)
                                            if (request.Explanation == "0")
                                                _testAnswerRepository.Delete(ta);
                                        request.Explanation = "";
                                    }
                                }
                                else
                                {
                                    {
                                        ta = await _testAnswerRepository.FindAsync(p => p.TestId == test.Id && (p.TeacherExamQuestionId ?? 0) == teacherExamQ.Id
                                        && p.AnswerId == request.AnswerId);
                                        if (ta != null)
                                        {
                                            ta.ResponseDate = fecha;
                                            ta.Explanation = request.Explanation ?? "";
                                            _testAnswerRepository.Update(ta);
                                        }
                                    }
                                }
                                if (ta == null)
                                {
                                    ta = new Entities.TestAnswer
                                    {
                                        TestId = test.Id,
                                        TeacherExamQuestionId = teacherExamQ.Id,
                                        AnswerId = request.AnswerId,
                                        Explanation = request.Explanation ?? "",
                                        ResponseDate = fecha,
                                    };
                                    _testAnswerRepository.Add(ta);
                                }
                                test.LastResponseDate = fecha;
                                _testRepository.Update(test);
                                await _unitOfWork.SaveChangesAsync();
                                result.Data = ta.Id;
                                result.Success = true;
                                result.Message = "Respuesta guardada correctamente";
                            }
                        }
                        else
                        {
                            result.Data = null;
                            result.Success = true;
                            result.Message = "-";
                        }
                    }
                    else
                        throw new Exception("No se encontró registro de un examen iniciado");
                }
            }
            catch (System.Exception ex)
            {
                result.Success = false;
                result.Message = "Problemas al intentar de guardar el examen";
            }
            return result;
        }
    }
}
