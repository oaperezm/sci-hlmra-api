﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IStudent
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddStudent(StudentSaveRequest request);
        Task<Response> UpdateStudent(StudentUpdateRequest request);
        Task<Response> DeleteStudent(int StudentId);
        Task<Response> GetStudentsByGroup(int studentGroupId, int examScheduleId);
        Task<Response> SendInvitation(StudentSendInvitationRequest request);
        Task<Response> GetCourseByStudentAndBlock(int usuarioId, int bloqueId);
        Task<Response> ConfirmInvitation(string Codigo);
        Task<Response> AddInvitation(StudentSendInvitationRequest request, int id);
    }
}
