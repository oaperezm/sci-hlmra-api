﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IActivity
    {
        Task<Response> GetAllByGroup(int id);
        Task<Response> Add(ActivitySaveRequest request);
        Task<Response> Update(ActivityUpdateRequest request);
        Task<Response> RemoveFile(ActivityRemoveFileRequest request);
        Task<Response> Remove(int id);
        Task<Response> GetStudentsAnswers(int studenGroupId, int activityId);
        Task<Response> UpdateStatus(int activityId, bool status);

        // ANSWER
        Task<Response> AddAnswer(ActivityAnswerSaveRequest request);
        Task<Response> SaveScore(ActivityScoreRequest request);
        Task<Response> GetStundentFileByActivity(int activityId, int userId);
        Task<Response> DeleteAnswer(int ActivityId, int userId, int answerId);
        Task<Response> GetAllWithScore(int studentGroupId, int userId);
    }
}
