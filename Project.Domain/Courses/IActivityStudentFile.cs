﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IActivityStudentFile
    {
        Task<Response> AddFile(ActivityStudentFileSaveRequest request);
        Task<Response> DeleteFile(int activityStudentFileId, int userId);
        Task<Response> GetStundentFileByActivity(int activityStudentFileId, int userId); 
    }
}
