﻿using Project.Domain.Core;
using Project.Entities.Responses;
using Project.Repositories.Courses;
using Project.Repositories.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VCoordinatorReport : IVCoordinatorReport
    {
        private readonly IVCoordinatorReportRepository _vCoordinatorReportReportRepository;

        public VCoordinatorReport(IVCoordinatorReportRepository vCoordinatorReportRepository)
        {
            _vCoordinatorReportReportRepository = vCoordinatorReportRepository;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var reportCoordinator = new List<VCoordinatorReport>();
                result.Data = _vCoordinatorReportReportRepository.GetAll().Select(x => new 
                {
                    x.Id,
                    cct = x.cct,
                    email = x.email,
                    userName = x.userName,
                    nameSchool = x.nameSchool,
                    level = x.level,
                    //grade = x.grade,
                    //formativeField = x.formativeField,
                    //signature = x.signature,
                    course = x.course,
                    group = x.group,
                    totalStudents = x.totalStudents,
                    totalActivities = x.totalActivities,
                    totalScheduledExams = x.totalScheduledExams,
                    totalQuestionsBanks = x.totalQuestionsBanks,
                    totalQuestionTeacher = x.totalQuestionTeacher,
                    totalQuestions = x.totalQuestions
                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
