﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IAnnouncement
    {
        Task<Response> Add(AnnouncementSaveRequest request);
        Task<Response> Update(AnnouncementUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetById(int userId, int announcementId);
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId);
    }
}
