﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface ICourseSection
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(CourseSectionSaveRequest request);
        Task<Response> Update(CourseSectionUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetByCourseId(int courseId);
    }
}
