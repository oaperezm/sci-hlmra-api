﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Domain.Courses
{
    public class TeacherExam : ITeacherExam
    {
        private readonly ITeacherExamRepository _teacherExamRepository;
        private readonly IFileUploader _fileUploader;
        private readonly ITeacherExamQuestionRepository _teacherExamQuestionRepository;
        private readonly IWeightingRepository _weightingRepositoryRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly ITestRepository _testRepository;


        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="teacherExamRepository"></param>
        /// <param name="unitOfWork"></param>
        public TeacherExam(ITeacherExamRepository teacherExamRepository,
                           ITeacherExamQuestionRepository teacherExamQuestionRepository,
                           IWeightingRepository weightingRepositoryRepository,
                           IFileUploader fileUploader,
                           IExamScheduleRepository examScheduleRepository,
                           ITestRepository testRepository,
                           IUnitOfWork unitOfWork)
        {

            this._teacherExamRepository = teacherExamRepository;
            this._teacherExamQuestionRepository = teacherExamQuestionRepository;
            this._weightingRepositoryRepository = weightingRepositoryRepository;
            this._testRepository = testRepository;
            this._unitOfWork = unitOfWork;
            this._examScheduleRepository = examScheduleRepository;
            this._fileUploader = fileUploader;

        }

        /// <summary>
        /// Guardar un nuevo examen
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TeacherExamSaveRequest request)
        {
            var response = new Response();
            Decimal qValue = 0;
            try
            {
                if (request.Questions.Count == 0)
                    throw new Exception("No puede guardar el examen ya que no ha seleccionado preguntas");

                var exam = new Entities.TeacherExam();

                exam.Active = true;
                exam.Description = request.Description;
                exam.RegisterDate = DateTime.Now;
                exam.TeacherExamTypeId = request.TeacherExamTypeId;
                exam.UserId = request.UserId;
                exam.WeightingId = 2;//request.WeightingId;
                exam.MaximumExamScore = 10;//request.MaximumExamScore;
                exam.IsAutomaticValue = true;// request.IsAutomaticValue;                                

                if (exam.IsAutomaticValue)
                    qValue = Convert.ToDecimal(exam.MaximumExamScore) / request.Questions.Count;
                foreach (var question in request.Questions)
                {
                    exam.TeacherExamQuestion.Add(new TeacherExamQuestion
                    {
                        Value = exam.IsAutomaticValue ? qValue : 1,
                        QuestionId = question.Id,
                        TeacherExamId = exam.Id
                    });
                }

                _teacherExamRepository.Add(exam);
                await _unitOfWork.SaveChangesAsync();

                response.Data = exam.Id;
                response.Success = true;
                response.Message = "Examen registrado correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al registrar un examen: { ex.Message }";

            }

            return response;

        }


        /// <summary>
        /// Guardar un nuevo examen
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(TeacherExamUpdateRequest request)
        {
            var response = new Response();
            Decimal qValue = 0;
            Decimal total = 0;
            int totalEdited = 0;
            try
            {
                if (request.Questions.Count == 0)
                    throw new Exception("No puede guardar el examen ya que no ha seleccionado preguntas");

                var exam = await _teacherExamRepository.FindAsync(x => x.Id == request.Id);


                if (exam == null)
                    throw new Exception("No se encontró datos del examén");

                exam.Active = true;
                exam.Description = request.Description;
                exam.UpdateDate = DateTime.Now;
                exam.TeacherExamTypeId = request.TeacherExamTypeId;
                exam.WeightingId = 2;
                exam.MaximumExamScore = 10;
                exam.IsAutomaticValue = true;
                exam.TeacherExamQuestion.Clear();

                var teacherExamQuestion = await _teacherExamQuestionRepository.FindAllAsync(x => x.TeacherExamId == exam.Id);

                foreach (var t in teacherExamQuestion)
                {
                    _teacherExamQuestionRepository.Delete(t);
                }

                if (exam.IsAutomaticValue)
                    qValue = Convert.ToDecimal(exam.MaximumExamScore) / request.Questions.Count;
                foreach (var question in request.Questions)
                {
                    qValue = Math.Round((Convert.ToDecimal(exam.MaximumExamScore) - total) / (request.Questions.Count - totalEdited) * 100) / 100;
                    total += qValue;
                    totalEdited++;
                    exam.TeacherExamQuestion.Add(new TeacherExamQuestion
                    {
                        QuestionId = question.Id,
                        TeacherExamId = exam.Id,
                        Value = qValue
                    });
                }
                _teacherExamRepository.Update(exam);
                await _unitOfWork.SaveChangesAsync();

                response.Data = exam.Id;
                response.Success = true;
                response.Message = "Datos de examen actualizado correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al registrar un examen: { ex.Message }";

            }

            return response;

        }

        /// <summary>
        /// Eliminar de manera permanente los datos de un examen y sus preguntas
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {

            var response = new Response();

            try
            {
                var teacherExam = await _teacherExamRepository.FindAsync(x => x.Id == id);

                if (teacherExam.ExamSchedules.Count() > 0)
                    throw new Exception("No se puede eliminar el examen ya que tiene programaciones relacionadas");

                _teacherExamQuestionRepository.Delete(x => x.TeacherExamId == id);
                _teacherExamRepository.Delete(teacherExam);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al eliminar el examen: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Obtener el detalle de un examen generado
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id, bool converImageBase64 = false)
        {
            var response = new Response();

            try
            {
                var exam = (await _teacherExamRepository.FindAsync(x => x.Id == id));

                if (exam != null)
                {
                    var teacherExam = new Entities.TeacherExam
                    {

                        Id = exam.Id,
                        Active = exam.Active,
                        Description = exam.Description,
                        UserId = exam.UserId,
                        TeacherExamTypes = new TeacherExamType
                        {
                            Id = exam.TeacherExamTypes.Id,
                            Description = exam.TeacherExamTypes.Description
                        },
                        Weighting = new Weighting
                        {
                            Id = exam.Weighting.Id,
                            Description = exam.Weighting.Description,
                            Unit = exam.Weighting.Unit
                        },
                        IsAutomaticValue = exam.IsAutomaticValue,
                        MaximumExamScore = exam.MaximumExamScore,
                        TeacherExamQuestion = exam.TeacherExamQuestion?.Select(q => new TeacherExamQuestion
                        {
                            QuestionId = q.QuestionId,
                            Value = q.Value,
                            UserEdited = q.UserEdited,
                            Question = new Entities.Question
                            {
                                Id = q.Question.Id,
                                Active = q.Question.Active,
                                Content = q.Question.Content,
                                Explanation = q.Question.Explanation,
                                QuestionTypeId = q.Question.QuestionType.Id,
                                UrlImage = q.Question.UrlImage,
                                ImageWidth = q.Question.ImageWidth,
                                ImageHeight = q.Question.ImageHeight,
                                QuestionBankId = q.Question.QuestionBankId,
                                QuestionType = new Entities.QuestionType
                                {
                                    Id = q.Question.QuestionType.Id,
                                    Active = q.Question.QuestionType.Active,
                                    Description = q.Question.QuestionType.Description
                                },
                                Answers = q.Question.Answers.Select(a => new Entities.Answer
                                {
                                    Id = a.Id,
                                    Active = a.Active,
                                    Description = a.Description,
                                    IsCorrect = a.IsCorrect,
                                    Order = a.Order,
                                    RelationDescription = a.RelationDescription,
                                    ImageWidth = a.ImageWidth,
                                    ImageHeight = a.ImageHeight
                                }).ToList()
                            }
                        }).ToList()

                    };

                    if (converImageBase64)
                    {
                        foreach (var teacherExamQuestion in teacherExam.TeacherExamQuestion)
                        {

                            if (!string.IsNullOrEmpty(teacherExamQuestion.Question.UrlImage))
                            {

                                string urlSimple = HttpUtility.UrlDecode(teacherExamQuestion.Question.UrlImage).Split('?').First();
                                string fname = urlSimple.Split('/').Last();
                                MemoryStream meme = await _fileUploader.FileDownload(fname, "exams");
                                teacherExamQuestion.Question.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                            }

                            if (teacherExamQuestion.Question.QuestionTypeId == (int)EnumQuestionType.ImageMultiple)
                            {
                                foreach (var answers in teacherExamQuestion.Question.Answers)
                                {
                                    string urlSimple = HttpUtility.UrlDecode(answers.Description).Split('?').First();
                                    string fname = urlSimple.Split('/').Last();
                                    MemoryStream meme = await _fileUploader.FileDownload(fname, "teacher");
                                    answers.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                                }
                            }
                        }
                    }

                    response.Data = teacherExam;
                    response.Message = "Datos obtenidos correctamente";
                    response.Success = true;
                }
                else
                {

                    response.Message = "Datos del examen no encontrado";
                    response.Success = false;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al registrar un examen: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Obtener el detalle de un examen programado
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<Response> GetExamSchedule(int id, int examScheduleId, int userId, bool converImageBase64 = false)
        {
            var response = new Response();

            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);

                var examSchedule = await _examScheduleRepository.FindAsync(p => p.Id == examScheduleId && p.TeacherExamId == id);
                if (examSchedule == null)
                    throw new Exception("No se encontro la información del examen programado");
                List<Entities.ExamSchedule> list = new List<Entities.ExamSchedule>();
                Entities.ExamSchedule es = new Entities.ExamSchedule()
                {
                    Id = examSchedule.Id,
                    ApplicationDate = examSchedule.BeginApplicationDate,
                    BeginApplicationDate = examSchedule.BeginApplicationDate,
                    EndApplicationDate = examSchedule.EndApplicationDate,
                    BeginApplicationTime = examSchedule.BeginApplicationTime,
                    EndApplicationTime = examSchedule.EndApplicationTime,
                    Active = examSchedule.Active,
                    Description = examSchedule.Description,
                    MinutesExam = examSchedule.MinutesExam,
                    Tests = new List<Entities.Test>(),
                };

                if (fecha >= es.BeginApplicationDate && fecha <= es.EndApplicationDate.AddDays(1).AddSeconds(-1)
                    && ((!es.BeginApplicationTime.HasValue && !es.EndApplicationTime.HasValue)
                    || (fecha.TimeOfDay >= es.BeginApplicationTime.Value.TimeOfDay && fecha.TimeOfDay <= es.EndApplicationTime.Value.TimeOfDay)))
                    es.IsExamAvailable = true;
                list.Add(es);
                var exam = examSchedule.TeacherExam;
                if (exam != null)
                {
                    List<Entities.Test> listTest = new List<Entities.Test>();
                    Entities.Test currentTest = examSchedule.Tests.FirstOrDefault(p => p.UserId == userId);
                    fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);

                    if (currentTest != null)
                    {
                        if (!currentTest.IsCompleted)
                            if (es.MinutesExam.HasValue && currentTest.StartDate.Value.AddMinutes(es.MinutesExam.Value) < fecha)
                            {
                                currentTest.IsCompleted = true;
                                _testRepository.Update(currentTest);
                                await _unitOfWork.SaveChangesAsync();
                            }
                        currentTest = new Entities.Test
                        {
                            Id = currentTest.Id,
                            RegisterDate = currentTest.RegisterDate,
                            ExamScheduleId = currentTest.ExamScheduleId,
                            IsCompleted = currentTest.IsCompleted,
                            ClientId = currentTest.ClientId,
                            StartDate = currentTest.StartDate,
                            LastResponseDate = currentTest.LastResponseDate,
                            CorrectAnswers = currentTest.CorrectAnswers,
                            IsEvaluated = currentTest.IsEvaluated
                        };
                        es.Tests.Add(currentTest);
                    }
                    var teacherExam = new Entities.TeacherExam
                    {
                        Id = exam.Id,
                        Active = exam.Active,
                        Description = exam.Description,
                        UserId = exam.UserId,
                        ExamSchedules = list,
                        TeacherExamTypes = new TeacherExamType
                        {
                            Id = exam.TeacherExamTypes.Id,
                            Description = exam.TeacherExamTypes.Description
                        },
                        Weighting = new Weighting
                        {
                            Id = exam.Weighting.Id,
                            Description = exam.Weighting.Description,
                            Unit = exam.Weighting.Unit
                        },
                        IsAutomaticValue = exam.IsAutomaticValue,
                        MaximumExamScore = exam.MaximumExamScore,
                        TeacherExamQuestion = (es.IsExamAvailable && currentTest != null)
                        || (es.IsExamAvailable && !es.MinutesExam.HasValue) ?
                        exam.TeacherExamQuestion?.Select(q => new TeacherExamQuestion
                        {
                            QuestionId = q.QuestionId,
                            Value = q.Value,
                            UserEdited = q.UserEdited,
                            Question = new Entities.Question
                            {
                                Id = q.Question.Id,
                                Active = q.Question.Active,
                                Content = q.Question.Content,
                                Explanation = q.Question.Explanation,
                                QuestionTypeId = q.Question.QuestionType.Id,
                                UrlImage = q.Question.UrlImage,
                                ImageWidth = q.Question.ImageWidth,
                                ImageHeight = q.Question.ImageHeight,
                                QuestionBankId = q.Question.QuestionBankId,
                                QuestionType = new Entities.QuestionType
                                {
                                    Id = q.Question.QuestionType.Id,
                                    Active = q.Question.QuestionType.Active,
                                    Description = q.Question.QuestionType.Description
                                },
                                Answers = q.Question.Answers.Select(a => new Entities.Answer
                                {
                                    Id = a.Id,
                                    Active = a.Active,
                                    Description = a.Description,
                                    IsCorrect = a.IsCorrect,
                                    Order = a.Order,
                                    RelationDescription = a.RelationDescription,
                                    ImageWidth = a.ImageWidth,
                                    ImageHeight = a.ImageHeight
                                }).ToList()
                            }
                        }).ToList() : new List<TeacherExamQuestion>()
                    };
                    foreach (var teq in teacherExam.TeacherExamQuestion)
                    {
                        var totalCorrects = 0;
                        foreach (var a in teq.Question.Answers)
                        {
                            totalCorrects += a.IsCorrect ? 1 : 0;
                            a.IsCorrect = false;
                        }
                        teq.Question.IsMultiple = totalCorrects > 1;
                    }


                    response.Data = teacherExam;
                    response.Message = "Datos obtenidos correctamente";
                    response.Success = true;
                }
                else
                {
                    response.Message = "Datos del examen no encontrado";
                    response.Success = false;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al registrar un examen: { ex.Message }";
            }

            return response;
        }


        /// <summary>
        /// Iniciar un examen programado
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<Response> StartExamSchedule(int id, int examScheduleId, int userId, bool converImageBase64 = false)
        {
            var response = new Response();

            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);

                var examSchedule = await _examScheduleRepository.FindAsync(p => p.Id == examScheduleId && p.TeacherExamId == id);
                if (examSchedule == null)
                    throw new Exception("No se encontro la información del examen programado");
                Entities.ExamSchedule es = new Entities.ExamSchedule()
                {
                    Id = examSchedule.Id,
                    ApplicationDate = examSchedule.BeginApplicationDate,
                    BeginApplicationDate = examSchedule.BeginApplicationDate,
                    EndApplicationDate = examSchedule.EndApplicationDate,
                    BeginApplicationTime = examSchedule.BeginApplicationTime,
                    EndApplicationTime = examSchedule.EndApplicationTime,
                    Active = examSchedule.Active,
                    Description = examSchedule.Description,
                    MinutesExam = examSchedule.MinutesExam,
                    //Tests = test,
                };

                if(fecha >= es.BeginApplicationDate && fecha <= es.EndApplicationDate.AddDays(1).AddSeconds(-1)
                    && ((!es.BeginApplicationTime.HasValue && !es.EndApplicationTime.HasValue)
                    || (fecha.TimeOfDay >= es.BeginApplicationTime.Value.TimeOfDay && fecha.TimeOfDay <= es.EndApplicationTime.Value.TimeOfDay)))
                    es.IsExamAvailable = true;
                else
                    throw new Exception("El examen sólo podrá ser contestado en la fecha y horario establecido");
                if ((es.MinutesExam ?? 0) <= 0)
                    throw new Exception("Duración del examen no disponible.");

                var exam = examSchedule.TeacherExam;

                if (exam != null)
                {
                    Entities.TeacherExam teacherExam;

                    List<Entities.ExamSchedule> list = new List<Entities.ExamSchedule>();
                    List<Entities.Test> listTest = new List<Entities.Test>();

                    Entities.Test currentTest = examSchedule.Tests.FirstOrDefault(p => p.UserId == userId);
                    fecha = TimeZoneInfo.ConvertTime(DateTime.UtcNow, cstZone);
                    if (currentTest == null)
                    {
                        currentTest = new Entities.Test
                        {
                            RegisterDate = fecha,
                            ExamScheduleId = examScheduleId,
                            IsCompleted = false,
                            ClientId = 1,
                            StartDate = fecha,
                            LastResponseDate = fecha,
                            CorrectAnswers = 0,
                            UserId = userId,
                            IsEvaluated = false,
                        };
                        _testRepository.Add(currentTest);
                        await _unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        if (!currentTest.IsCompleted && currentTest.StartDate.Value.AddMinutes(es.MinutesExam.Value) < fecha)
                        {
                            currentTest.IsCompleted = true;
                            _testRepository.Update(currentTest);
                            await _unitOfWork.SaveChangesAsync();
                        }
                    }
                    currentTest = new Entities.Test
                    {
                        Id = currentTest.Id,
                        RegisterDate = currentTest.RegisterDate,
                        ExamScheduleId = currentTest.ExamScheduleId,
                        IsCompleted = currentTest.IsCompleted,
                        ClientId = currentTest.ClientId,
                        StartDate = currentTest.StartDate,
                        LastResponseDate = currentTest.LastResponseDate,
                        CorrectAnswers = currentTest.CorrectAnswers,
                        IsEvaluated = currentTest.IsEvaluated
                    };
                    listTest.Add(currentTest);
                    es.Tests = listTest;
                    list.Add(es);
                    teacherExam = new Entities.TeacherExam
                    {
                        Id = exam.Id,
                        Active = exam.Active,
                        Description = exam.Description,
                        UserId = exam.UserId,
                        ExamSchedules = list,
                        TeacherExamTypes = new TeacherExamType
                        {
                            Id = exam.TeacherExamTypes.Id,
                            Description = exam.TeacherExamTypes.Description
                        },
                        Weighting = new Weighting
                        {
                            Id = exam.Weighting.Id,
                            Description = exam.Weighting.Description,
                            Unit = exam.Weighting.Unit
                        },
                        IsAutomaticValue = exam.IsAutomaticValue,
                        MaximumExamScore = exam.MaximumExamScore,
                        TeacherExamQuestion = exam.TeacherExamQuestion?.Select(q => new TeacherExamQuestion
                        {
                            QuestionId = q.QuestionId,
                            Value = q.Value,
                            UserEdited = q.UserEdited,
                            Question = new Entities.Question
                            {
                                Id = q.Question.Id,
                                Active = q.Question.Active,
                                Content = q.Question.Content,
                                Explanation = q.Question.Explanation,
                                QuestionTypeId = q.Question.QuestionType.Id,
                                UrlImage = q.Question.UrlImage,
                                ImageWidth = q.Question.ImageWidth,
                                ImageHeight = q.Question.ImageHeight,
                                QuestionBankId = q.Question.QuestionBankId,
                                QuestionType = new Entities.QuestionType
                                {
                                    Id = q.Question.QuestionType.Id,
                                    Active = q.Question.QuestionType.Active,
                                    Description = q.Question.QuestionType.Description
                                },
                                Answers = q.Question.Answers.Select(a => new Entities.Answer
                                {
                                    Id = a.Id,
                                    Active = a.Active,
                                    Description = a.Description,
                                    IsCorrect = a.IsCorrect,
                                    Order = a.Order,
                                    RelationDescription = a.RelationDescription,
                                    ImageWidth = a.ImageWidth,
                                    ImageHeight = a.ImageHeight
                                }).ToList()
                            }
                        }).ToList()

                    };
                    foreach (var teq in teacherExam.TeacherExamQuestion)
                    {
                        var totalCorrects = 0;
                        foreach (var a in teq.Question.Answers)
                        {
                            totalCorrects += a.IsCorrect ? 1 : 0;
                            a.IsCorrect = false;
                        }
                        teq.Question.IsMultiple = totalCorrects > 1;
                    }
                    if (converImageBase64)
                    {
                        foreach (var teacherExamQuestion in teacherExam.TeacherExamQuestion)
                        {

                            if (!string.IsNullOrEmpty(teacherExamQuestion.Question.UrlImage))
                            {

                                string urlSimple = HttpUtility.UrlDecode(teacherExamQuestion.Question.UrlImage).Split('?').First();
                                string fname = urlSimple.Split('/').Last();
                                MemoryStream meme = await _fileUploader.FileDownload(fname, "exams");
                                teacherExamQuestion.Question.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                            }

                            if (teacherExamQuestion.Question.QuestionTypeId == (int)EnumQuestionType.ImageMultiple)
                            {
                                foreach (var answers in teacherExamQuestion.Question.Answers)
                                {
                                    string urlSimple = HttpUtility.UrlDecode(answers.Description).Split('?').First();
                                    string fname = urlSimple.Split('/').Last();
                                    MemoryStream meme = await _fileUploader.FileDownload(fname, "teacher");
                                    answers.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                                }
                            }
                        }
                    }
                    response.Data = teacherExam;
                    response.Message = "Datos obtenidos correctamente";
                    response.Success = true;
                }
                else
                {
                    response.Message = "Datos del examen no encontrado";
                    response.Success = false;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas para inciar el examen: { ex.Message }";
            }
            return response;
        }

        /// <summary>
        /// Obtener todos los examenes registrados por usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserId(int userId)
        {

            var response = new Response();

            try
            {
                var exams = (await _teacherExamRepository.FindAllAsync(x => x.UserId == userId))
                            .Select(x => new Entities.TeacherExam
                            {
                                Id = x.Id,
                                Active = x.Active,
                                Description = x.Description,
                                UserId = x.UserId,
                                TeacherExamTypes = new TeacherExamType
                                {
                                    Id = x.TeacherExamTypes.Id,
                                    Description = x.TeacherExamTypes.Description
                                },
                                TeacherExamQuestion = x.TeacherExamQuestion?.Select(q => new TeacherExamQuestion
                                {
                                    QuestionId = q.QuestionId,
                                    Question = new Entities.Question
                                    {
                                        Id = q.Question.Id,
                                        Active = q.Question.Active,
                                        Content = q.Question.Content,
                                        Explanation = q.Question.Explanation,
                                        UrlImage = q.Question.UrlImage,
                                        ImageHeight = q.Question.ImageHeight,
                                        ImageWidth = q.Question.ImageWidth,
                                        QuestionTypeId = q.Question.QuestionType.Id,
                                        QuestionBankId = q.Question.QuestionBankId,
                                        QuestionType = new Entities.QuestionType
                                        {
                                            Active = q.Question.QuestionType.Active,
                                            Id = q.Question.QuestionType.Id,
                                            Description = q.Question.QuestionType.Description
                                        },
                                        Answers = q.Question.Answers.Select(a => new Entities.Answer
                                        {
                                            Id = a.Id,
                                            Active = a.Active,
                                            Description = a.Description,
                                            IsCorrect = a.IsCorrect,
                                            Order = a.Order,
                                            RelationDescription = a.RelationDescription,
                                            ImageWidth = a.ImageWidth,
                                            ImageHeight = a.ImageHeight
                                        }).ToList()
                                    }
                                }).ToList()
                            });

                response.Data = exams;
                response.Message = "Datos obtenidos correctamente";
                response.Success = true;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al registrar un examen: { ex.Message }";
            }

            return response;
        }


        /// <summary>
        /// Obtener los examenes registrados por usuario paginados y filtrados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="type"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAllByUserPagination(int currentPage, int sizePage, string filter, int type, int userId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.TeacherExam>);
                var data = await _teacherExamRepository.FindAllAsync(x => x.UserId == userId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Description.ToLower().Contains(filter)).ToList();
                }

                if (type > 0)
                    data = data.Where(x => x.TeacherExamTypeId == type).ToList();


                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Description)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data = pageData.Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los examenes reggistrados por un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserIdShortInfo(int userId)
        {
            var result = new Response();

            try
            {
                var data = await _teacherExamRepository.FindAllAsync(x => x.UserId == userId);

                result.Data = data.OrderByDescending(x => x.RegisterDate).Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }


        /// <summary>
        /// Actualiza la ponderación del examen y preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateWeighting(TeacherExamUpdateRequest request)
        {
            var response = new Response();
            try
            {
                if (request.Questions.Count == 0)
                    throw new Exception("No puede guardar el examen ya que no ha seleccionado preguntas");

                var exam = await _teacherExamRepository.FindAsync(x => x.Id == request.Id);
                if (exam == null)
                    throw new Exception("No se encontró datos del examén");

                var weighting = await _weightingRepositoryRepository.FindAsync(p => p.Unit == request.WeightingUnit);
                if (weighting == null)
                    throw new Exception("No se encontró la ponderación seleccionada para el examén");

                exam.UpdateDate = DateTime.Now;
                exam.WeightingId = weighting.Id;
                exam.MaximumExamScore = request.MaximumExamScore;
                exam.IsAutomaticValue = request.IsAutomaticValue;

                //Verifica que las preguntas sean las mismas antes de actualizar
                foreach (var t in exam.TeacherExamQuestion)
                    if (!request.Questions.Any(p => p.Id == t.QuestionId))
                        throw new Exception("Las preguntas del examén no coinciden");

                foreach (var t in exam.TeacherExamQuestion)
                {
                    var rq = request.Questions.First(p => p.Id == t.QuestionId);
                    t.Value = rq.Value;
                    t.UserEdited = rq.UserEdited;
                }

                _teacherExamRepository.Update(exam);
                await _unitOfWork.SaveChangesAsync();

                response.Data = exam.Id;
                response.Success = true;
                response.Message = "Ponderación de preguntas actualizada correctamente";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al ponderar las preguntas del  examen: { ex.Message }";
            }

            return response;

        }
    }
}
