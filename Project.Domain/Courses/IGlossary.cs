﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IGlossary
    {
        Task<Response> GetAllById(int userId, int glossaryId);

        Task<Response> GetAllByUserId(int userId, bool fullData = false);
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId);

        Task<Response> Add(GlossarySaveRequest request);
        Task<Response> Update(GlossaryUpdateRequest request);
        Task<Response> Delete(int id);

        Task<Response> RemoveToNode(int nodeId, int glosaryId);
        Task<Response> AssignToNode(AssignNodeRequest request);

        Task<Response> AddWord(WordSaveRequest request);
        Task<Response> UpdateWord(WordUpdateRequest request);
        Task<Response> DeleteWord(int id);

        Task<Response> GetByStudent(int nodeId, int userId);         

    }
}
