﻿using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Project.Domain.Courses
{
    public interface IStudentScore
    {
        Task<Response> GetAllByGroup(int studentGroupId);
        Task<Response> GetByPartialId(int studentGroupId, int Partialid, int userId);
        Task<Response> GetAllByGroupDetailsExcel(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO);//Ricardo - Agregado

        Task<Response> GetAllPartials(int studentGroupId, int userId);
        Task<Response> UpdateStudentScore(StudentScoreUpdateRequest request);
        Task<Response> GetStudentsScoresByGroup(int studentGroupId, int userId);
        Task<Response> SaveStudentScore(StudentScoreSaveRequest request);
        List<StudentFinalScoreDTO> GetScoreGeneral(int studentGroupId, int userId);
    }
}
