﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;

namespace Project.Domain.Courses
{
    public class Question : IQuestion
    {
        private readonly IQuestionRepository _QuestionRepository;        
        private readonly IQuestionTypeRepository _questionTypeRepository;
        private readonly ITeacherExamQuestionRepository _teacherExamQuestionRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        public Question(IQuestionRepository QuestionRepository,
                        IQuestionTypeRepository questionTypeRepository,
                        ITeacherExamQuestionRepository teacherExamQuestionRepository,
                        IUnitOfWork unitOfWor,
                        IFileUploader fileUploader)
        {

            _QuestionRepository            = QuestionRepository;
            _questionTypeRepository        = questionTypeRepository;
            _unitOfWork                    = unitOfWor;
            _fileUploader                  = fileUploader;
            _teacherExamQuestionRepository = teacherExamQuestionRepository;
        }

        #region METHODS

        /// <summary>
        /// Método para guardar los datos de una nueva pregunta de un banco de preguntaas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddQuestion(QuestionSaveRequest request)
        {
            var result = new Response();

            try
            {
                string fileUrl   = string.Empty;
                string container = "exams";
                               
                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);

                Entities.Question question = new Entities.Question();

                question.Content        = request.Content;
                question.Active         = request.Active;
                question.Order          = request.Order;
                question.QuestionBankId = request.QuestionBankId;
                question.QuestionTypeId = request.QuestionTypeId;
                question.Explanation    = request.Explanation;

                if (!string.IsNullOrEmpty(fileUrl)) {

                    var width = 0;
                    var height = 0;
                                        
                    try
                    {
                        using (var ms = new MemoryStream(request.FileArray))
                        {
                            var image = System.Drawing.Image.FromStream(ms);

                            width  = image.Width;
                            height = image.Height;
                        }
                    }
                    catch (Exception) { }
                    
                    question.ImageHeight = height;
                    question.ImageWidth  = width;
                    question.UrlImage    = fileUrl;
                }

                if (request.Answers != null && request.Answers.Count > 0) {
                    foreach (var r in request.Answers)
                        question.Answers.Add(r);
                }

                _QuestionRepository.Add(question);
                await this._unitOfWork.SaveChangesAsync();
                
                result.Data    = this.GetNewQuestionObject(question);
                result.Success = true;
                result.Message = "Datos guardados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>|
        /// Método que permite eliminar de manera permanente los datos de la pregunta 
        /// </summary>
        /// <param name="questionId">Identificador unico de la pregunta</param>
        /// <returns></returns>
        public async Task<Response> DeleteQuestion(int questionId)
        {
            var response = new Response();

            try
            {
                //TODO Validar que la pregunta no se encuentre utilizada para generar algún examen
                var question = await  _QuestionRepository.FindAsync(x => x.Id == questionId);

                if (question == null)
                    throw new Exception("No se encontraron datos de la pregunta");

                var questioExamn = await _teacherExamQuestionRepository.FindAllAsync(x => x.QuestionId == questionId);

                if (questioExamn.Count > 0)
                    throw new Exception("La pregunta se encuenta asignada a un examén");

                _QuestionRepository.Delete(question);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al intentar de eliminar la pregunta: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Actualizar los datos de una pregunta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateQuestion(QuestionUpdateRequest request)
        {
            var result = new Response();

            try
            {
                string fileUrl   = string.Empty;
                string container = "exams";

                var question = await _QuestionRepository.FindAsync(x => x.Id == request.Id);

                if (question == null)
                    throw new Exception("No se encontraron los datos de la pregunta");


                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                {
                    await _fileUploader.FileRemove(question.UrlImage, container);                    
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                }


                question.Content        = request.Content;                
                question.Order          = request.Order;
                question.QuestionBankId = request.QuestionBankId;
                question.QuestionTypeId = request.QuestionTypeId;
                question.Explanation    = request.Explanation;

                if (!string.IsNullOrEmpty(fileUrl))
                {
                    question.UrlImage = fileUrl;
                }


                if (question.QuestionTypeId != (int)EnumQuestionType.OpenQuestion) {

                    var answers = question.Answers.ToList();

                    request.Answers.ForEach( a =>
                    {
                        if (a.Id == 0) {
                            question.Answers.Add(a);
                        } else {

                            var answer = answers.FirstOrDefault(x => x.Id == a.Id);

                            if (answer != null) {
                                answer.Description          = a.Description;
                                answer.RelationDescription  = a.RelationDescription;
                                answer.IsCorrect            = a.IsCorrect;
                                answer.ImageHeight          = a.ImageHeight;
                                answer.ImageWidth           = a.ImageWidth;
                                question.Answers.Add(answer);
                            }
                        }
                    });
                }

                _QuestionRepository.Update(question);
                await this._unitOfWork.SaveChangesAsync();
                

                question       = await _QuestionRepository.FindAsync(x => x.Id == request.Id);
                result.Success = true;
                result.Message = "Datos guardados correctamente";
                result.Data    = this.GetNewQuestionObject(question);
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las preguntas registradas
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _QuestionRepository.GetAll().
                    Select(u => new {
                        Id                      = u.Id,
                        Content                 = u.Content,
                        Active                  = u.Active,
                        Order                   = u.Order,
                        QuestionBankId          = u.QuestionBankId,
                        QuestionTypeId          = u.QuestionTypeId,
                        QuestionBankName        = u.QuestionBank.Name,
                        QuestionTypeDescription = u.QuestionType.Description,
                        Explanation             = u.Explanation
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los datos de una pregunta por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _QuestionRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Generar una nueva instancia de un objeto tipo Quesiton
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        private Entities.Question GetNewQuestionObject(Entities.Question question) {

            if (question.QuestionType == null)
                question.QuestionType = _questionTypeRepository.GetById(question.QuestionTypeId);
                

            return new Entities.Question
            {
                Id             = question.Id,
                Content        = question.Content,
                Order          = question.Order,
                QuestionBankId = question.QuestionBankId,
                QuestionTypeId = question.QuestionTypeId,
                Explanation    = question.Explanation,
                UrlImage       = question.UrlImage,
                ImageWidth     = question.ImageWidth,
                ImageHeight    = question.ImageHeight,
                QuestionType   = new Entities.QuestionType { Id = question.QuestionType.Id,
                                                             Description = question.QuestionType?.Description  },
                Answers        = question.Answers?.Distinct()
                                                .Select(a => new Entities.Answer{
                                                            Id                  = a.Id,
                                                            Active              = a.Active,
                                                            Description         = a.Description,                                                             
                                                            IsCorrect           = a.IsCorrect,
                                                            Order               = a.Order,
                                                            QuestionId          = a.QuestionId,
                                                            RelationDescription = a.RelationDescription,
                                                            ImageHeight         = a.ImageHeight,
                                                            ImageWidth          = a.ImageWidth
                                                        }).ToList()
            };
        }

        #endregion

    }
}