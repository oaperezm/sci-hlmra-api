﻿using Project.Domain.Helpers;
using Project.Entities.Converters;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Glossary : IGlossary
    {
        private readonly IGlossaryRepository _glosaryRepository;
        private readonly IWordRepository _wordRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly ICourseRepository _courseRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Glossary(IGlossaryRepository glosaryRepository,
                         IWordRepository wordRepository,
                         INodeRepository nodeRepository,
                         IPushNotificationService srvPushNotification,
                         IStudentGroupRepository studentGroupRepository,
                         ICourseRepository courseRepository,
                         IUnitOfWork unitOfWork) {

            _glosaryRepository      = glosaryRepository;
            _wordRepository         = wordRepository;
            _nodeRepository         = nodeRepository;
            _unitOfWork             = unitOfWork;
            _srvPushNotification    = srvPushNotification;
            _studentGroupRepository = studentGroupRepository;
            _courseRepository       = courseRepository;

        }

        #region GLOSSARY

        /// <summary>
        /// Obtener glosario por identificador y usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="glossaryId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllById(int userId, int glossaryId)
        {
            var result = new Response();
            var gloossaryFound = default(Entities.Glossary);

            try
            {
                var glossary = await _glosaryRepository.FindAsync(x => x.UserId == userId && x.Id == glossaryId);

                if (glossary == null)
                    throw new Exception("Datos de glosario no encontrados, favor de corroborar la información proporcionada");

                gloossaryFound = new Entities.Glossary
                {
                    Id = glossary.Id,
                    Description = glossary.Description,
                    Name = glossary.Name,
                    Active = glossary.Active,
                    Nodes = glossary.Nodes.Select(n => new Entities.Node
                    {
                        Id = n.Id,
                        Description = n.Description,
                        Name = n.Name,
                        Active = n.Active
                    }).ToList(),
                    Words = glossary.Words?.Select(w => new Entities.Word
                    {
                        Id = w.Id,
                        Description = w.Description,
                        Name = w.Name,
                        Active = w.Active,
                        GlossaryId = w.GlossaryId
                    }).ToList()
                };

                result.Success = true;
                result.Data = gloossaryFound;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los glosarios por usuario y paginación
        /// </summary>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Glossary>);
                var data     =  await _glosaryRepository.FindAllAsync(x => x.UserId == userId);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Name.ToLower().Contains(filter)
                                          || x.Description.ToLower().Contains(filter))
                               .ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((curentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();
                                  
                result.Data    = pageData.Select(x => x.New(true)).ToList(); 
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }



        /// <summary>
        /// Obtener todos los glosarios de un profesor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullData"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId, bool fullData = false)
        {
            var result = new Response();
            var lstGlossary = default(List<Entities.Glossary>);

            try
            {
                var glossaries = await _glosaryRepository.FindAllAsync(x => x.UserId == userId);

                if (fullData) 
                    lstGlossary = glossaries.Select(x => x.New(true)).ToList();
                else 
                    lstGlossary = glossaries.Select(x => x.New(false)).ToList();
                
                result.Success = true;
                result.Data = lstGlossary;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que permite guardar un nuevo glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(GlossarySaveRequest request) {

            var result = new Response();

            try
            {
                var glossary = new Entities.Glossary();
                glossary.Description = request.Description;
                glossary.Name = request.Name;
                glossary.Active = true;
                glossary.RegisterDate = DateTime.Today;
                glossary.UserId = request.UserId;

                _glosaryRepository.Add(glossary);
                await _unitOfWork.SaveChangesAsync();
                               
                result.Success = true;
                result.Message = "Datos guardados corretamente";
                result.Data = glossary.Id;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar los datos: { ex.Message }";
            }


            return result;
        }
        
        /// <summary>
        /// Método que permite actualizar los datos de un glosario existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(GlossaryUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var glossary = await _glosaryRepository.FindAsync(x => x.Id == request.Id);

                if (glossary == null)
                    throw new Exception("Datos de glosario no encontrados");

                glossary.Description = request.Description;
                glossary.Name = request.Name;
                glossary.UpdateDate = DateTime.Today;

                _glosaryRepository.Update(glossary);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos guardados corretamente";
                result.Data = glossary.Id;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar los datos: { ex.Message }";
            }


            return result;
        }

        /// <summary>
        /// Eliminar de manera permanente un glosario y sus terminos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var glossary = await  _glosaryRepository.FindAsync(x => x.Id == id);

                if (glossary == null)
                    throw new Exception("Datos del glosario no fueron encontrados");

                _glosaryRepository.Delete(glossary);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados corretamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar los datos: { ex.Message }";
            }

            return result;
        }
    
        /// <summary>
        /// Asignar un glosario a un nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AssignToNode(AssignNodeRequest request) {

            var result = new Response();

            try
            {
                var glossary = await _glosaryRepository.FindAsync(x => x.Id == request.GlossaryId);
                var node     = await _nodeRepository.FindAsync(x => x.Id == request.NodeId);

                if (glossary == null || node == null)
                    throw new Exception("Debe selecionar un glosario y un nodo");

                glossary.Nodes.Add(node);

                _glosaryRepository.Update(glossary);
                await _unitOfWork.SaveChangesAsync();

                #region SEND PUSH NOTIFICATION

                try
                {

                    var course = await _courseRepository.FindAllAsync(x => x.NodeId == request.NodeId && x.UserId == request.UserId);
                    var data   = new
                    {
                        courseName   = node.Name,
                        glossaryName = glossary.Description
                    };
                    var userIds = course.Where( x=> x.UserId == request.UserId)
                                        .SelectMany( x=> x.StudentGroups)                                         
                                        .SelectMany(x => x.Students)
                                        .Select(x => x.UserId)
                                        .ToList();

                    // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewGlosarry, data);
                }
                catch (Exception) { }


                #endregion
                
                result.Success = true;
                result.Message = "Nodo asignado correctamente al glosario"; ;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error al asignar el glosario al nodo";
            }

            return result;
        }

        /// <summary>
        /// Eliminar la asignación de un glosario a un nodo especificado
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="glosaryId"></param>
        /// <returns></returns>
        public async Task<Response> RemoveToNode(int nodeId, int glosaryId)
        {

            var result = new Response();

            try
            {
                var glossary = await _glosaryRepository.FindAsync(x => x.Id == glosaryId);
                var node     = await _nodeRepository.FindAsync(x => x.Id == nodeId);

                if (glossary == null || node == null)
                    throw new Exception("Debe selecionar un glosario y un nodo");

                glossary.Nodes.Remove(node);

                _glosaryRepository.Update(glossary);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Nodo eliminado correctamente al glosario"; ;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Error al eliminar el glosario al nodo";
            }

            return result;
        }

        /// <summary>
        /// Obtener los glosarios de los grupos a los cuales esta inscrito el usuario
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int nodeId, int userId) {

            var result = new Response();

            try
            {
                var studentGroups = await _studentGroupRepository.FindAllAsync(x => x.Students.Any(u => u.UserId == userId && u.CheckDate != null) && x.Course.NodeId == nodeId);
                var groupNodeId   = studentGroups.Select(x => x.Course.NodeId).Distinct().ToList();
                
                if (studentGroups == null)
                    throw new Exception("No se encontraron grupos inscritos del alumno");

                var data = await _glosaryRepository.FindAllAsync(x =>  x.Nodes.Any(n => groupNodeId.Contains(n.Id)));
                var glossaries = data.Select(g => new Entities.Glossary {
                                                                           Id          = g.Id,
                                                                           Description = g.Description,
                                                                           Name        = g.Name,
                                                                           Active      = g.Active,
                                                                           Words       = g.Words.Select( w => new Entities.Word {
                                                                                            Id          = w.Id,
                                                                                            Description = w.Description,
                                                                                            GlossaryId  = w.GlossaryId,
                                                                                            Name        = w.Name,
                                                                                            Active      = w.Active
                                                                                          }).ToList()
                                                                        });
                
                result.Data    = glossaries;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Message = $"Problema al obtener los glosarios: { ex.Message}" ;
                result.Success = false;
                
            }
            
            return result;
        }
        
        #endregion
        
        #region WORDS

        /// <summary>
        /// Agregar un termino a un glosario existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddWord(WordSaveRequest request) {

            var result = new Response();

            try
            {
                var word = new Entities.Word();

                word.Name         = request.Name;
                word.Description  = request.Description;
                word.GlossaryId   = request.GlossaryId;
                word.Active       = true;
                word.RegisterDate = DateTime.Today;

                _wordRepository.Add(word);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = word.Id;
                result.Success = true;
                result.Message = "Definición guardada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar los datos: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Actualizar datos de un termino existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateWord(WordUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var word = await  _wordRepository.FindAsync(x => x.Id == request.Id);

                if (word == null)
                    throw new Exception("No se encontraron los datos del temrino proporcionado");

                word.Name        = request.Name;
                word.Description = request.Description;                                
                word.UpdateDate  = DateTime.Today;

                _wordRepository.Update(word);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = word.Id;
                result.Success = true;
                result.Message = "Datos guardada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar los datos: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de un termino 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteWord(int id) {

            var result = new Response();

            try
            {
                var word = await _wordRepository.FindAsync(x => x.Id == id);

                if (word == null)
                    throw new Exception("No se encontraron los datos del temrino proporcionado");

                _wordRepository.Delete(word);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Termino eliminado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "Problemas al eliminar el termino";
            }

            return result;

        }

        #endregion

    }
}
