﻿using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Attendance: IAttendance
    {
        private readonly IAttendanceRepository _attendanceRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Attendance( IAttendanceRepository attendanceRepository,
                            IUnitOfWork unitOfWork ) {

            _attendanceRepository = attendanceRepository;
            _unitOfWork           = unitOfWork;
        }

        /// <summary>
        /// Registrar las asistencias de un profesor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveAttendances(AttendanceSaveRequest request) {

            var result = new Response();

            try
            {  
                request.Attendances.ForEach( a =>
                {
                     a.StudentAttendances.ForEach( s =>
                    {
                        var attendance =  _attendanceRepository.Get(x => x.StudentGroupId == request.StudentGroupId
                                                                         && x.UserId == s.UserId
                                                                         && (x.AttendanceDate.Year == a.AttendanceDate.Year
                                                                             && x.AttendanceDate.Month == a.AttendanceDate.Month
                                                                             && x.AttendanceDate.Day == a.AttendanceDate.Day));
                        if (attendance == null)
                        {
                            attendance = new Entities.Attendance
                            {
                                AttendanceClass = s.AttendanceClass,
                                AttendanceDate = a.AttendanceDate,
                                RegisterDate = DateTime.Now,
                                StudentGroupId = request.StudentGroupId,
                                UserId = s.UserId
                            };

                            _attendanceRepository.Add(attendance);
                        }
                        else
                        {
                            attendance.AttendanceClass = s.AttendanceClass;
                            attendance.AttendanceDate = a.AttendanceDate;

                            _attendanceRepository.Update(attendance);
                        }
                    });
                });

                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Asistencia registrada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al registrar la sistencia: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Obtener la asistencia por fechay grupo
        /// </summary>
        /// <param name="initDate"></param>
        /// <param name="endDate"></param>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAttendances(DateTime initDate, DateTime endDate, int studentGroupId) {

            var result = new Response();

            try
            {
                var data = await _attendanceRepository.FindAllAsync(x => x.AttendanceDate >= initDate
                                                                 && x.AttendanceDate <= endDate
                                                                 && x.StudentGroupId == studentGroupId);

                var attendance = data.GroupBy(g => g.AttendanceDate)
                                     .Select(a => new AttendanceDTO
                                     {   Day        = a.Key,
                                         Attendance = a.Select(x => new AttendanceStudentDTO
                                         {
                                             Id             = x.Id,
                                             Attendance     = x.AttendanceClass,                                             
                                             StudentGroupId = x.StudentGroupId,
                                             UserId         = x.UserId
                                         }).ToList()
                                     }).ToList();

                result.Data    = attendance;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al registrar la sistencia: { ex.Message}";
            }

            return result;
        }
    }
}
