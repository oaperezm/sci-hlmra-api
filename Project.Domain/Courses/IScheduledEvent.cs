﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IScheduledEvent
    {
        Task<Response> Add(ScheduledEventSaveRequest request);
        Task<Response> Update(ScheduledEventUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetByCourse(int courseId);
        Task<Response> GetByStudent(int userId, DateTime month);
        Task<Response> GetByTeacher(int userId, DateTime month);
    }
}
