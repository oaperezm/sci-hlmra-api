﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class TeacherResource: ITeacherResource
    {
        private readonly ITeacherResourceRepository _teacherResourceRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;
        private readonly string _containerTeacher;

        public TeacherResource(ITeacherResourceRepository teacherResourceRepository,
                               IUnitOfWork unitOfWork,
                               IFileUploader fileUploader) {

            this._teacherResourceRepository = teacherResourceRepository;
            this._unitOfWork = unitOfWork;
            this._fileUploader = fileUploader;
            this._containerTeacher = "teacher";
        }

        /// <summary>
        /// Agregar un nuevo recuerso del profesor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TeacherResourceSaveRequest request) {

            var result  = new Response();
            var fileUrl = string.Empty;

            try
            {
                fileUrl          = string.Empty;
                string container = "teacher";

                if (request.FileArray != null && request.FileArray.Length > 0)
                {                    
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);
                }

                if (fileUrl == string.Empty)
                    throw new Exception("Problemas al cargar el archivo en el blod storage");

                var resource = new Entities.TeacherResources
                {
                    Description    = request.Description,
                    Name           = request.Name,
                    RegisterDate   = DateTime.Today,
                    ResourceTypeId = request.TeacherResourceId,
                    UserId         = request.UserId,
                    Url            = fileUrl
                };

                _teacherResourceRepository.Add(resource);
                await _unitOfWork.SaveChangesAsync();

                result.Data = new {
                    Id  = resource.Id,
                    url = fileUrl,
                };

                result.Success = true;
                result.Message = "Recurso agregado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al guardar el recurso { ex.Message }";
            }
            
            return result;

        }

        /// <summary>
        /// Obtener todos los recursos por usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUser(int userId) {

            var result = new Response();

            try
            {
                var data = await _teacherResourceRepository.FindAllAsync(x => x.UserId == userId);

                result.Data    = data.Select(x=> new Entities.TeacherResources
                {
                    Id              = x.Id,
                    Description     = x.Description,
                    Name            = x.Name,
                    RegisterDate    = x.RegisterDate,
                    Url             = x.Url,
                    ResourceTypeId  = x.ResourceTypeId,
                    ResourceType    = x.ResourceType == null ? new Entities.ResourceType() : new Entities.ResourceType {
                        Id          = x.ResourceType.Id,
                        Description = x.ResourceType.Description }
                    
                }).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al obtener los recursos del profesor {ex.Message}";

            }

            return result;
        }

        /// <summary>
        /// Eliminar un recurso ya registrado en el sistema
        /// </summary>
        /// <param name="resourceId"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int resourceId, int userId) {

            var result = new Response();

            try
            {
                var resource = await _teacherResourceRepository.FindAsync(x => x.Id == resourceId && x.UserId == userId);

                if (resource == null)
                    throw new Exception("Datos no encontrados, no se pueden eliminar los datos");

                _teacherResourceRepository.Delete(resource);
                await _unitOfWork.SaveChangesAsync();

                await _fileUploader.FileRemove(resource.Url, this._containerTeacher);
                               
                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al eliminar el recurso; {ex.Message}";
            }

            return result;

        }
    }
}
