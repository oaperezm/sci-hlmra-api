﻿using Project.Entities;
using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Repositories.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Messages : IMessages
    {
        private readonly IChatGroupRepository _chatGroupRepository;
        private readonly IChatGroupUserRepository _chatGroupUserRepository;
        private readonly IChatMessageRepository _chatMessageRepository;
        private readonly IPushNotificationService _pushNotification;
        private readonly IStudentRepository _studentRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IUnitOfWork _unitOfWork;

        public Messages(IChatGroupRepository chatGroupRepository,
                        IChatGroupUserRepository chatGroupUserRepository,
                        IChatMessageRepository chatMessageRepository,
                        IPushNotificationService pushNotification,
                        IStudentRepository studentRepository,
                        IUserRepository userRepository,
                        IPushNotificationService srvPushNotification,
                        IUnitOfWork unitOfWork) {

            _chatGroupRepository     = chatGroupRepository;
            _chatGroupUserRepository = chatGroupUserRepository;
            _chatMessageRepository   = chatMessageRepository;
            _pushNotification        = pushNotification;
            _studentRepository       = studentRepository;
            _userRepository          = userRepository;
            _srvPushNotification     = srvPushNotification;
            _unitOfWork              = unitOfWork;

        }

        /// <summary>
        /// Enviar un mensaje
        /// </summary>
        /// <param name="senderUserId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SendMessage(int senderUserId, SendMessageRequest request) {

            var result          = new Response();
            var chatGroup       = default(ChatGroup);
            var user            = default(User);
            long messageSequese = 0;
            
            try
            {
                var dateMessage = request.DateMessage.HasValue ? request.DateMessage.Value : DateTime.Now;

                if (request.ChatGroupId == 0)
                {
                    chatGroup = new ChatGroup
                    {
                        CreateDate   = dateMessage,
                        CreateUserId = senderUserId,
                        UpdateDate   = DateTime.Now,
                    };

                    chatGroup.Users.Add(new ChatGroupUser { UserId = senderUserId, RegisterDate = dateMessage });
                    chatGroup.Users.Add(new ChatGroupUser { UserId = request.ReceiverUserId, RegisterDate = dateMessage });

                    _chatGroupRepository.Add(chatGroup);
                }
                else {

                    chatGroup            = await _chatGroupRepository.FindAsync(x => x.Id == request.ChatGroupId);
                    chatGroup.UpdateDate = dateMessage;

                    _chatGroupRepository.Update(chatGroup);
                }

                var lastMessage = await _chatMessageRepository.FindAllAsync(x => x.ChatGroupId == chatGroup.Id);

                if (lastMessage != null && lastMessage.Count > 0)                
                    messageSequese = lastMessage.LastOrDefault().MessageSequence + 1;                
                else 
                    messageSequese++;                

                var message = new ChatMessage {
                    Message         = request.Message,
                    ChatGroupId     = chatGroup.Id,
                    Read            = false,
                    ReceiverUserId  = request.ReceiverUserId,
                    SenderUserId    = senderUserId,
                    RegisterDate    = dateMessage,                     
                    MessageSequence = messageSequese
                };
                
                _chatMessageRepository.Add(message);
                await _unitOfWork.SaveChangesAsync();
                
                if(request.ChatGroupId == 0)
                    chatGroup = await _chatGroupRepository.FindAsync(x => x.Id == chatGroup.Id);

                var userGroup    = chatGroup.Users.FirstOrDefault(u => u.UserId != senderUserId);
                var lastMessages = chatGroup.Messages.LastOrDefault();
                               
                user = await _userRepository.FindAsync(x => x.Id == senderUserId);


                #region PUSHNOTIFICATION

                try
                {
                    var users     = (await _chatGroupRepository.FindAsync(x => x.Id == chatGroup.Id)).Users;
                    var receivers = users.Where(x => x.UserId != senderUserId);
                    var userIds   = receivers.Select(x => x.UserId).ToList();

                    var data = new
                    {
                        sender = user?.FullName,
                        date   = lastMessages?.RegisterDate.ToShortDateString()
                    };
                    
                    // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewMessage, data);

                }
                catch (Exception) { }             

                #endregion

                result.Data = new {
                    ChatGroup = new ConversationDTO {
                        ChatGroupId      = chatGroup.Id,
                        ReceiverUserId   = user.Id,
                        ReceiverUserName = user?.FullName,
                        ReceiverAvatar   = user?.UrlImage,
                        ReceiverEmail    = user?.Email,
                        LastMessage      = lastMessages?.Message,
                        LastMessageDate  = lastMessages?.RegisterDate,
                        IsRead           = lastMessages.Read
                    } ,
                    message = new {
                        message.Id,
                        message.RegisterDate
                    }
                };

                result.Success = true;
                result.Message = "Mensaje enviado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al enviar el mensaje: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los mensajes de un grupo de chat
        /// </summary>
        /// <param name="chatGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetByChatGroup(int chatGroupId, int userId) {

            var result = new Response();

            try
            {
                var data = await _chatMessageRepository.FindAllAsync(x => x.ChatGroupId == chatGroupId);

                data.Where(x => x.ReceiverUserId == userId && x.Read == false)
                    .ToList().ForEach(x => {
                        x.Read = true;
                        _chatMessageRepository.Update(x);
                    });

                await _unitOfWork.SaveChangesAsync();

                result.Data    = data.OrderBy(x => x.MessageSequence).
                    Select( m => new MessageDTO
                    {
                        Id              = m.Id,
                        Message         = m.Message,
                        ChatGroupId     = m.ChatGroupId,
                        MessageSequence = m.MessageSequence,
                        Read            = m.Read,
                        ReceiverUser = new ContactDTO
                        {
                            FullName = m.ReceiverUser.FullName,
                            Avatar = m.ReceiverUser.UrlImage
                        },
                        SenderUser = new ContactDTO
                        {
                            FullName = m.SenderUser.FullName,
                            Avatar   = m.SenderUser.UrlImage
                        },
                        SenderDate = m.RegisterDate,
                        Owner      = m.SenderUserId == userId

                    }).ToList();

                result.Success = true;
                result.Message = "Mensajes obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los mensajes: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las conversaciones actuales
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllConversations(int userId) {

            var result = new Response();

            try
            {
                var data = await _chatGroupRepository.FindAllAsync(x => x.Users.Any(u => u.UserId == userId));
                var conversations = new List<ConversationDTO>();

                data.ToList().ForEach(c =>
                {
                    var user         = c.Users.FirstOrDefault(u => u.UserId != userId).User;
                    var lastMessages = c.Messages.LastOrDefault();
                    var conversation = new ConversationDTO();

                    conversation.ChatGroupId      = c.Id;
                    conversation.ReceiverUserId   = user.Id;
                    conversation.ReceiverUserName = user.FullName;
                    conversation.ReceiverAvatar   = user.UrlImage;
                    conversation.ReceiverEmail    = user.Email;
                    conversation.LastMessage      = lastMessages?.Message;
                    conversation.LastMessageDate  = lastMessages?.RegisterDate;
                    conversation.IsRead           = lastMessages.Read;
                    conversation.Owner            = lastMessages.ReceiverUserId == userId;

                    conversations.Add(conversation);
                });

                result.Data    = conversations.OrderByDescending(x => x.LastMessageDate).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos corretamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener las conversaciones: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Marcar los mensajes como leidos
        /// </summary>
        /// <param name="reciverUserId"></param>
        /// <param name="read"></param>
        /// <returns></returns>
        public async Task<Response> MarkRead(int reciverUserId, bool read) {

            var result = new Response();


            return result;
        }

        /// <summary>
        /// Obtener los contactos a los que puede mandar mensaje un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rolId"></param>
        /// <returns></returns>
        public async Task<Response> GetContact(int userId, int rolId) {

            var result = new Response();

            try
            {
                var contacts = new List<ContactDTO>();
                var users    = new List<User>();

                if (rolId == (int)EnumRol.Student)
                {
                    var groups = await _studentRepository.FindAllAsync(x => x.UserId == userId);
                    users      = groups.Select(x => x.StudentGroup.Course.User)
                                       .Distinct()
                                       .ToList();
                }
                else {
                    var groups = await _studentRepository.FindAllAsync(x => x.StudentGroup.Course.UserId == userId);
                    users      = groups.Select(x => x.User)
                                        .Distinct()
                                        .ToList();
                }

                users.ForEach(t =>
                {
                    contacts.Add(new ContactDTO
                    {
                        Id = t.Id,
                        FullName = t.FullName,
                        Avatar = t.UrlImage,
                        Email = t.Email
                    });

                });

                result.Data    = contacts.OrderBy(c => c.FullName);
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los contactos: {ex.Message}";
            }

            return result;
        }
    }
}
