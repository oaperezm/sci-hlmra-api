﻿using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Entities.Enums;

namespace Project.Domain.Courses
{
    /// <summary>
    /// Dominio para administrar avisos
    /// </summary>
    public class Announcement : IAnnouncement
    {
        private readonly IAnnouncementRepository _announcementRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;        
        private readonly IPushNotificationService _srvPushNotification;        
        private readonly IUnitOfWork _unitOfWork;

        /// Constructor
        public Announcement(IAnnouncementRepository announcementRepository,
                              IStudentGroupRepository studentGroupRepository,                              
                              IPushNotificationService srvPushNotification,                            
                              IUnitOfWork unitOfWork) {

            _announcementRepository = announcementRepository;
            _studentGroupRepository   = studentGroupRepository;            

            _unitOfWork               = unitOfWork;
            _srvPushNotification      = srvPushNotification;           
        }

        #region Métodos Get

        /// <summary>
        /// Obtener aviso por identificador y usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="announcementId"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int userId, int announcementId)
        {
            var result = new Response();
            var announcementFound = default(Entities.Announcement);

            try
            {
                var announcement = await _announcementRepository.FindAsync(x => x.UserId == userId && x.Id == announcementId);

                if (announcement == null)
                    throw new Exception("Datos de aviso no encontrados, favor de corroborar la información proporcionada");

                announcementFound = new Entities.Announcement
                {
                    Id = announcement.Id,
                    Description = announcement.Description,
                    Title = announcement.Title,
                    RegisterDate = announcement.RegisterDate,
                    Active = announcement.Active,
                    StudentGroup = announcement.StudentGroup.Select(
                                                      g => new Entities.StudentGroup
                                                      {
                                                          Id = g.Id,
                                                          Description = g.Description,
                                                          Status = g.Status,
                                                          Course = new Course { Id = g.Course.Id, Name = g.Course.Name }
                                                      }).ToList()

                };


                result.Success = true;
                result.Data = announcementFound;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los avisos por usuario y paginación
        /// </summary>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.Announcement>);

                var data = (await _announcementRepository.FindAllAsync(x => x.UserId == userId))
                    .Select(a => new Entities.Announcement
                    {
                        Id = a.Id,
                        Title = a.Title,
                        Description = a.Description,
                        RegisterDate = a.RegisterDate,
                        Active = a.Active,
                        StudentGroup = a.StudentGroup.Select(
                                                      g => new Entities.StudentGroup
                                                      {
                                                          Id = g.Id,
                                                          Description = g.Description,
                                                          Status = g.Status,
                                                          Course = new Course { Id = g.Course.Id, Name = g.Course.Name }
                                                      }).ToList()
                        })
                        .ToList();


                        // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Title.ToLower().Contains(filter)
                                          || x.Description.ToLower().Contains(filter))
                               .ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, curentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Title)
                               .Skip((curentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)                               
                               .ToList();

                result.Data = pageData.Select(
                       entity => new Entities.Announcement
                       {
                           Id = entity.Id,
                           Description = entity.Description,
                           Title = entity.Title,
                           Active = entity.Active,
                           RegisterDate = entity.RegisterDate,
                           StudentGroup = entity.StudentGroup.Select(
                                                      g => new Entities.StudentGroup
                                                      {
                                                          Id = g.Id,
                                                          Description = g.Description,
                                                          Status = g.Status,
                                                          Course = new Course { Id = g.Course.Id, Name = g.Course.Name  }
                                                      }).ToList()
                       }).ToList();                    


                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        #endregion

        /// <summary>
        /// Agregar un nuevo aviso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(AnnouncementSaveRequest request) {

            var result = new Response();
            var groups = default(List<Entities.StudentGroup>);           

            try
            {
                var announcement = new Entities.Announcement
                {
                    Title = request.Title,
                    Description = request.Description,
                    Active = true,
                    RegisterDate = request.RegisterDate,                                                      
                    UserId = request.UserId
                };


                if (request.Groups.Count > 0)
                {
                    groups = (await _studentGroupRepository.FindAllAsync(s => request.Groups.Contains(s.Id))).ToList();
                    groups.ToList().ForEach(g => announcement.StudentGroup.Add(g));
                }

                _announcementRepository.Add(announcement);
                  await _unitOfWork.SaveChangesAsync();


                #region SEND PUSH NOTIFICATION
              

                var data = new
                {                    
                    announcementName = request.Title,
                    announcementDate = request.RegisterDate.ToShortDateString()
                };
                var userIds = groups.SelectMany(s => s.Students).Select(x => x.UserId).ToList();               

                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewAnnouncement, data);

                #endregion

                result.Data    = announcement.Id;
                result.Success = true;
                result.Message = "Aviso agregado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar el evento: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Actualizar los datos de un aviso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(AnnouncementUpdateRequest request)
        {

            var result = new Response();            

            try
            {
                var announcement = await _announcementRepository.FindAsync(x => x.Id == request.Id);

                announcement.Title        = request.Title;
                announcement.Description = request.Description;
                announcement.RegisterDate = request.RegisterDate;
                announcement.Active      = request.Active;                    
                announcement.UpdateDate = DateTime.Today;

                var groups = await _studentGroupRepository.FindAllAsync(x => request.Groups.Contains(x.Id));
                announcement.StudentGroup.Clear();
                groups.ToList().ForEach(g => announcement.StudentGroup.Add(g));

                _announcementRepository.Update(announcement);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos actualizados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al editar el evento: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar un aviso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var announcement = await _announcementRepository.FindAsync(x => x.Id == id);                                
                
                _announcementRepository.Delete(announcement);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Evento eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el evento: { ex.Message}";
            }

            return result;

        }       

    }
}
