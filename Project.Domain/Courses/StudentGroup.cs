﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Entities.Enums;

namespace Project.Domain.Courses
{
    public class StudentGroup : IStudentGroup
    {
        private readonly IStudentGroupRepository _StudentGroupRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICoursePlanningRepository _coursePlanningRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;

        public StudentGroup(IStudentGroupRepository StudentGroupRepository, IUnitOfWork unitOfWor, ICoursePlanningRepository coursePlanning, ISchedulingPartialRepository schedulingPartialRepository, ICourseRepository courseRepository)
        {

            this._StudentGroupRepository = StudentGroupRepository;
            this._unitOfWork = unitOfWor;
            this._coursePlanningRepository = coursePlanning;
            this._courseRepository = courseRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
        }

        public async Task<Response> AddStudentGroup(StudentGroupSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.StudentGroup StudentGroup = new Entities.StudentGroup();

                StudentGroup.Description = request.Description;
                StudentGroup.Status = request.Status; 
                StudentGroup.CourseId = request.CourseId;
                StudentGroup.Code = request.Code;

                _StudentGroupRepository.Add(StudentGroup);

                await this._unitOfWork.SaveChangesAsync();

                

                result.Data = StudentGroup;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de un grupo 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteStudentGroup(int id)
        {
            var result = new Response();
        
            try
            {
                var group = await _StudentGroupRepository.FindAsync(x => x.Id == id);

                if (group == null)
                    throw new Exception("No se encontró datos del grupo");

                if(group.Students.Count > 0)
                    throw new Exception("No se puede eliminar el grupo, hay alumnos asignados");

                if (group.ExamSchedules.Count > 0)
                    throw new Exception("No se puede eliminar el grupo, hay programación de examenes asignados");

                _StudentGroupRepository.Delete(group);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos del grupo eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;                
            }

            return result;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _StudentGroupRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Status = u.Status,                        
                        CourseId = u.CourseId,
                        CourseDescription = u.Course.Description,
                        Code = u.Code,
                        //ps= u.PlannedCourses.Select(g => new Entities.CoursePlanning
                        //{
                        //    Id = g.Id,
                        //    Exam= g.Exam                           
                        //})
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = await _StudentGroupRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateStudentGroup(StudentGroupUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.StudentGroup StudentGroup = new Entities.StudentGroup();

                StudentGroup.Id = request.Id;
                StudentGroup.Description = request.Description;
                StudentGroup.Status = request.Status;
                StudentGroup.CourseId = request.CourseId;
                StudentGroup.Code = request.Code;

                _StudentGroupRepository.Update(StudentGroup);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = StudentGroup;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetGroupsByCourse(int courseId)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _StudentGroupRepository.GetAll()
                    .Where(sg => sg.CourseId == courseId).
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Status = u.Status,
                        CourseId = u.CourseId,
                        CourseDescription = u.Course.Description,
                        ExamScheduleTotal = u.ExamSchedules.Count()                        
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Agrega los datos del grupo de acuerdo  al sistema 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<Response> AddStudentGroupSystem(StudentGroupSaveRequest request,int systemId)
        {
            var result = new Response();
            try
            {
                dynamic list;

              

                if (systemId == (int)EnumSystems.RA)
                {
                    var course = await _courseRepository.FindAsync(c => c.Id == request.CourseId);
                    if (course == null)
                    {
                        throw new Exception("No es posible guardar la información del parcial para la planeación el curso indicada");
                    }
                    else
                    {


                        Entities.StudentGroup StudentGroup = new Entities.StudentGroup();
                        Entities.CoursePlanning courseplanning = new Entities.CoursePlanning();
                        Entities.SchedulingPartial SchedulingPartial = new Entities.SchedulingPartial();
                        StudentGroup.Description = request.Description;
                        StudentGroup.Status = request.Status;
                        StudentGroup.CourseId = request.CourseId;
                        StudentGroup.Code = request.Code;

                        _StudentGroupRepository.Add(StudentGroup);
                        await this._unitOfWork.SaveChangesAsync();


                        var scheduledcourse = await _coursePlanningRepository.FindAsync(cp => cp.StudentGroupId == StudentGroup.Id);
                        if (scheduledcourse != null)
                        {
                            throw new Exception("Ya existe una planeación para este grupo");
                        }
                        else
                        {
                            courseplanning.StudentGroupId = StudentGroup.Id;
                            _coursePlanningRepository.Add(courseplanning);
                            await this._unitOfWork.SaveChangesAsync();
                        }
                        if (courseplanning == null)
                        {
                            throw new Exception("No es posible guardar la información del parcial para la planeación indicada");
                        }
                        else
                        {
                            SchedulingPartial.Description = "Final";
                            SchedulingPartial.StartDate = course.StartDate;
                            SchedulingPartial.EndDate = course.EndDate;
                            SchedulingPartial.FinalPartial =1;
                            SchedulingPartial.CoursePlanningId = courseplanning.Id;
                            SchedulingPartial.RegisterDate = DateTime.Now;
                            SchedulingPartial.UpdateDate = DateTime.Now;
                            _schedulingPartialRepository.Add(SchedulingPartial);
                            await _unitOfWork.SaveChangesAsync();


                        }

                        result.Data = new Entities.DTO.StudentGroupDTO
                        {
                            Id = StudentGroup.Id,
                            Description = StudentGroup.Description,
                            Code = StudentGroup.Code,
                            Active = StudentGroup.Active,
                            Status = StudentGroup.Status,
                            CourseId = StudentGroup.CourseId,
                            CoursePlanningId = courseplanning.Id,
                            Exam = courseplanning.Exam,
                            Assistance = courseplanning.Assistance,
                            Activity = courseplanning.Activity,
                            CoursePlanningNumberPartial = courseplanning.NumberPartial,
                            SchedulingPartialId = SchedulingPartial.Id,
                            StartDate = SchedulingPartial.StartDate,
                            EndDate = SchedulingPartial.EndDate,
                            SchedulingPartialDescription = SchedulingPartial.Description,
                            FinalPartial = SchedulingPartial.FinalPartial


                        };
                    }
                }
                else if((systemId == (int)EnumSystems.SALI))
                {
                    Entities.StudentGroup StudentGroup = new Entities.StudentGroup();

                    StudentGroup.Description = request.Description;
                    StudentGroup.Status = request.Status;
                    StudentGroup.CourseId = request.CourseId;

                    _StudentGroupRepository.Add(StudentGroup);

                    await this._unitOfWork.SaveChangesAsync();                                       
                    result.Data = StudentGroup;

                }


              
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }


            return result;
        }

        /// <summary>
        /// Eliminar los datos de un grupo 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteStudentGroupSystem(int id, int systemId)
        {
            var result = new Response();

            try
            {
                var group = await _StudentGroupRepository.FindAsync(x => x.Id == id);

                if (group == null)
                    throw new Exception("No se encontró datos del grupo");

                if (group.Students.Count > 0)
                    throw new Exception("No se puede eliminar el grupo, hay alumnos asignada");

                if (group.ExamSchedules.Count > 0)
                    throw new Exception("No se puede eliminar el grupo, hay programación de examenes asignada");
                
               

                if (systemId == (int)EnumSystems.RA)
                {
                    Entities.CoursePlanning cp = new Entities.CoursePlanning();
                    cp = await _coursePlanningRepository.FindAsync(r => r.StudentGroupId.Equals(id));

                    if (cp == null)
                    {
                        throw new Exception("No se puede eliminar el grupo, hay programación de planeación asignada");
                    }
                    else
                    {
                        _coursePlanningRepository.Delete(cp);
                        await _unitOfWork.SaveChangesAsync();
                    }

                }
                _StudentGroupRepository.Delete(group);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos del grupo eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }


    }
}