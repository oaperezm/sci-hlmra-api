﻿using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface ITest
    {
        Task<Response> Add(TestSaveRequest request, EnumRol rol);
        Task<Response> GetByUserAndExamSchedule(int examScheduleId, int userId, EnumRol rol);
        Task<Response> SaveAnswerQuestion(TestAnswerQuestionRequest request, EnumRol rol);
    }
}
