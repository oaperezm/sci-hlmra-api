﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
   public class Event : IEvent
    {
        private readonly IEventRepository _eventRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Event(IEventRepository eventRepository, IUnitOfWork unitOfWor)
        {
            this._eventRepository = eventRepository;
            this._unitOfWork = unitOfWor;
        }

        /// <summary>
        /// Método para agregar un nuevo evento
        /// </summary>
        /// param name="request"
        /// <returns></returns>
        public async Task<Response> Add(EventRequest request)
        {
            var result = new Response();

            try
            {


                Entities.Events Events = new Entities.Events();
                Events.Name = request.Name;
                Events.Description = request.Description;
                Events.Active = request.Active;
                _eventRepository.Add(Events);

                await this._unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = Events;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }
        /// <summary>
        /// Método para eliminar un  evento
        /// </summary>
        /// param name="id"
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            var result = new Response();

            try
            {
                var events = await _eventRepository.FindAsync(x => x.Id == id);

                if (events == null)
                {
                    throw new Exception(" No se econtraron los datos para este evento");
                }

                _eventRepository.Delete(events);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }
    }
}
