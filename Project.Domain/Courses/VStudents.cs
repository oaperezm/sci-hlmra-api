﻿using Project.Domain.Core;
using Project.Domain.Helpers;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VStudents : IVStudents
    {

        private IVStudentsRepository _vStudentsRepository;
        private readonly IUnitOfWork _unitOfWork;


        public VStudents(IVStudentsRepository vStudentsRepository)
        {
            _vStudentsRepository = vStudentsRepository;
        }

        /// <summary>
        /// Obtiene todos los cursos registrados por un profesor
        /// </summary>
        /// <param name="userId">Identificador principal del profesor</param>
        /// <returns></returns>
        public async Task<Response> GetAllByIdTeacher(int teacherId)
        {
            var response = new Response();

            try
            {
                var students = await _vStudentsRepository.FindByAsyn(x => x.Id == teacherId);

                if (students != null)
                {
                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = students.Select(s => new
                    {
                        s.Id,                        
                        s.Teacher,
                        s.StudentId,
                        s.Student,
                        s.StudentEmail,
                        s.StudentUrlImage,
                        s.StudentSystemId,
                        s.StudentSystem,
                        s.StudentUserId,
                        s.StudentStatusId,
                        s.StudentStatusDescription,
                        s.StudentGroupId,
                        s.StudentGroupDescription,
                        s.CourseId,
                        s.CourseName,
                        s.CourseDescription,
                        s.CourseSubject                        
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los estudiantes: { ex.Message} ";
            }

            return response;
        }


        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(PaginationRelationRequest request)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.VStudents>);
                var data = await _vStudentsRepository.FindAllAsync(x=>x.TeacherId == request.Id);

                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(request.Filter))
                {
                    request.Filter = request.Filter.ToLower();
                    data = data.Where(x => x.Student.ToLower().Contains(request.Filter) 
                                           || x.CourseName.ToLower().Contains(request.Filter) 
                                           || x.CourseSubject.ToLower().Contains(request.Filter)).ToList();
                }

                // REALIZAR FILTRO POR CURSO
                if (request.CourseId > 0) {
                    data = data.Where(x => x.CourseId == request.CourseId).ToList();
                }

                // REALIZAR FILTRO POR GRUPO
                if (request.GroupId > 0) {
                    data = data.Where(x => x.StudentGroupId == request.GroupId).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, request.CurrentPage, request.PageSize);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.CourseDescription)
                                      .Skip((request.CurrentPage - 1) * result.Pagination.PageSize)
                                      .Take(result.Pagination.PageSize)
                                      .ToList();

                result.Data = pageData.OrderBy(x => x.CourseDescription)
                                          .Select(s => new Entities.VStudents
                                          {
                                              Id                        =  s.Id,
                                              Teacher                   = s.Teacher,
                                              StudentId                 = s.StudentId,
                                              Student                   = s.Student,
                                              StudentEmail              = s.StudentEmail,
                                              StudentUrlImage           = s.StudentUrlImage,
                                              StudentSystemId           = s.StudentSystemId,
                                              StudentSystem             = s.StudentSystem,
                                              StudentUserId             = s.StudentUserId,
                                              StudentStatusId           = s.StudentStatusId,
                                              StudentStatusDescription  = s.StudentStatusDescription,
                                              StudentGroupId            = s.StudentGroupId,
                                              StudentGroupDescription   = s.StudentGroupDescription,
                                              CourseId                  = s.CourseId,
                                              CourseName                = s.CourseName,
                                              CourseDescription         = s.CourseDescription,
                                              CourseSubject             = s.CourseSubject
                                          }).ToList();

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }



    }
}
