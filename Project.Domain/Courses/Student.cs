﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using Project.Repositories.Management;
using Project.Entities.Responses;
using Project.Entities.DTO;
using Project.Entities.Enums;

namespace Project.Domain.Courses
{
    public class Student : IStudent
    {
        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        private readonly IStudentRepository _StudentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMailer _mailer;
        private readonly IUserRepository _UserRepository;
        private readonly IStudentGroupRepository _StudentGroupRepository;
        private readonly IExamScoreRepository _examScoreRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly ITestRepository _testRepository;

        public Student(IStudentRepository StudentRepository,
            IUnitOfWork unitOfWor,
            IMailer mailer,
            IStudentGroupRepository StudentGroupRepository,
            IExamScoreRepository examScoreRepository,
            IExamScheduleRepository examSchedule,
            ITestRepository testRepository,
            IUserRepository UserRepository)
        {

            this._StudentRepository = StudentRepository;
            this._unitOfWork = unitOfWor;
            this._mailer = mailer;
            this._StudentGroupRepository = StudentGroupRepository;
            this._UserRepository = UserRepository;
            this._examScoreRepository = examScoreRepository;
            this._examScheduleRepository = examSchedule;
            this._testRepository = testRepository;
        }

        /// <summary>
        /// Agregar estudiante
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddStudent(StudentSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Student Student = new Entities.Student();

                Student.StudentGroupId = request.StudentGroupId;
                Student.UserId = request.UserId;
                Student.StudentStatusId = request.StudentStatusId;
                Student.InvitationDate = request.InvitationDate;
                Student.CheckDate = request.CheckDate;

                _StudentRepository.Add(Student);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Student;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar estudiante
        /// </summary>
        /// <param name="StudentId"></param>
        /// <returns></returns>
        public Task<Response> DeleteStudent(int StudentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtener todos los estudiantes 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _StudentRepository.GetAll().
                    Select(u => new
                    {
                        Id = u.Id,
                        StudentGroupId = u.StudentGroupId,
                        StudentGroupDescription = u.StudentGroup.Description,
                        StudentStatusId = u.StudentStatusId,
                        UserId = u.UserId,
                        StudentStatusDescription = u.StudentStatus.Description,
                        InvitationDate = u.InvitationDate,
                        CheckDate = u.CheckDate
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _StudentRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar datos de grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStudent(StudentUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Student Student = new Entities.Student();

                Student.Id = request.Id;
                Student.StudentGroupId = request.StudentGroupId;
                Student.StudentStatusId = request.StudentStatusId;
                Student.UserId = request.UserId;
                Student.InvitationDate = request.InvitationDate;
                Student.CheckDate = request.CheckDate;

                _StudentRepository.Update(Student);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Student;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los estudiantes de un grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="examScheduleId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsByGroup(int studentGroupId, int examScheduleId)
        {
            var result = new Response();

            try
            {
                var group = (await _StudentRepository.GetAllAsync())
                                                .Where(s => s.StudentGroupId == studentGroupId)
                                                .Select(u => new GroupStudentScoreDTO
                                                {
                                                    Id = u.Id,
                                                    StudentGroupId = u.StudentGroupId,
                                                    StudentGroupDescription = u.StudentGroup.Description,
                                                    StudentStatusId = u.StudentStatusId,
                                                    StudentStatusDescription = u.StudentStatus.Description,
                                                    InvitationDate = u.InvitationDate,
                                                    CheckDate = u.CheckDate,
                                                    UserId = u.UserId,
                                                    UserName = u.User.FullName,
                                                    UserMail = u.User.Email,
                                                    Score = string.Empty,
                                                    Comments = string.Empty
                                                }).ToList();

                if (examScheduleId > 0)
                {

                    var scores = await _examScoreRepository.FindAllAsync(x => x.ExamScheduleId == examScheduleId);
                    var scheduleExam = await _examScheduleRepository.FindAsync(x => x.Id == examScheduleId);
                    var totalQuetions = 0;
                    foreach (var student in group)
                    {
                        var test = _testRepository.GetAll().Where(t => t.UserId == student.UserId && t.ExamScheduleId == examScheduleId).FirstOrDefault();
                        var score = scores.FirstOrDefault(s => s.UserId == student.UserId && s.ExamScheduleId == examScheduleId);
                        var correctAnswers = 0;
                        if (scheduleExam != null)
                        {
                            totalQuetions = scheduleExam.TeacherExam.TeacherExamQuestion.Count();
                            student.MaximumExamScore = scheduleExam.TeacherExam.MaximumExamScore;
                            if (scheduleExam.TeacherExam != null)
                            {
                                student.WeightingId = scheduleExam.TeacherExam.WeightingId;
                                student.WeightingDescription = scheduleExam.TeacherExam.Weighting.Description;
                                student.WeightingUnit = scheduleExam.TeacherExam.Weighting.Unit;
                            }
                            student.TotalAnswerScore = 0;
                            if (test != null)
                            {
                                student.IsAnswered = true;
                                foreach (var q in scheduleExam.TeacherExam.TeacherExamQuestion)
                                {
                                    bool isCorrectAnswer = true;
                                    foreach (var qa in q.Question.Answers)
                                    {
                                        if (q.Question.QuestionTypeId != 3)
                                        {
                                            if (test.Answers.Any(p => p.AnswerId == qa.Id))
                                                isCorrectAnswer = qa.IsCorrect;
                                            else if (qa.IsCorrect)
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                        else
                                        {
                                            if (!test.Answers.Any(ta => ta.AnswerId == qa.Id && ta.Explanation == qa.RelationDescription))
                                                isCorrectAnswer = false;
                                            if (!isCorrectAnswer)
                                                break;
                                        }
                                    }
                                    if (isCorrectAnswer)
                                    {
                                        correctAnswers++;
                                        student.TotalAnswerScore += q.Value;
                                    }
                                }
                            }
                        }
                        student.TotalQuestions = totalQuetions;
                        student.TotalCorrect = correctAnswers;
                        student.TestId = test?.Id;
                        if (score != null)
                        {
                            student.Score = score.Score;
                            student.Comments = score.Comments;
                            student.HasScore = true;
                        }
                    }
                }

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = group;


            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para enviar invitaciones a un curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SendInvitation(StudentSendInvitationRequest request)
        {
            var response = new Response();

            try
            {
                if (request.UserIds.Count() > 0)
                {
                    foreach (int id in request.UserIds)
                    {
                        Entities.Student sr = new Entities.Student();
                        sr = _StudentRepository.Get(s => s.UserId == id && s.StudentGroupId == request.StudentGroupId);

                        if (sr == null)
                        {
                            Guid Codigo = Guid.NewGuid();

                            sr = new Entities.Student();
                            sr.UserId = id;
                            sr.StudentGroupId = request.StudentGroupId;
                            sr.InvitationDate = DateTime.UtcNow.AddHours(offset.Hours);
                            sr.Codigo = Codigo.ToString();
                            sr.StudentStatusId = 1;

                            _StudentRepository.Add(sr);
                            await this._unitOfWork.SaveChangesAsync();
                        }

                        Entities.StudentGroup sg = new Entities.StudentGroup();
                        sg = _StudentGroupRepository.GetById(request.StudentGroupId);

                        Entities.User u = new Entities.User();
                        u = _UserRepository.GetById(id);

                        Entities.Utils.InvitationMessage msg = new Entities.Utils.InvitationMessage(u.Email, "admin@sali.com.mx", "Invitación a curso", "MailCourseInvitationTemplate.html", "", sg.Description, "", u.FullName, request.SystemId);
                        msg.CourseLink = request.homeUrl + "?codigo=" + sr.Codigo;
                        msg.Body = "Has sido invitado a formar parte del curso en línea sobre " + sg.Course.Name + ", para confirmar tu inscripción haz click en el siguiente vínculo:";

                        try
                        {
                            await _mailer.SendGroupInvitation(msg);
                        }
                        catch (Exception) { }
                    }
                }

                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo alumno: { ex.Message }";
            }

            return response;
        }


        /// <summary>
        /// Método para que el alumno se inscriba a un curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddInvitation(StudentSendInvitationRequest request, int id)
        {
            var response = new Response();

            try
            {
                //if (request.UserIds.Count() > 0)
                //{
                //foreach (int id in request.UserIds)
                //  {

                Entities.Student sr = new Entities.Student();
                sr = _StudentRepository.Get(s => s.UserId == id && s.StudentGroupId == request.StudentGroupId);

                if (sr == null)
                {
                    Guid Codigo = Guid.NewGuid();

                    sr = new Entities.Student();
                    sr.UserId = id;
                    sr.StudentGroupId = request.StudentGroupId;
                    sr.InvitationDate = DateTime.UtcNow.AddHours(offset.Hours);
                    sr.Codigo = Codigo.ToString();
                    sr.StudentStatusId = 1;

                    _StudentRepository.Add(sr);
                    await this._unitOfWork.SaveChangesAsync();
                }

                // }  

                //}
                response.Data = sr.Codigo;
                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo alumno: { ex.Message }";
            }

            return response;
        }
        /// <summary>
        /// Obtener todos los cursos en los cuales el usuario esta inscrito por bloque
        /// </summary>
        /// <param name="usuarioId"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<Response> GetCourseByStudentAndBlock(int usuarioId, int bloqueId)
        {

            var response = new Response();

            try
            {
                var studentGroups = (await _StudentGroupRepository.FindAllAsync(x => x.Students.Any(u => u.UserId == usuarioId && u.CheckDate != null)));

                if (bloqueId > 0)
                    studentGroups = studentGroups.Where(x => x.Course.NodeId == bloqueId).ToList();

                var studentCourses = studentGroups.Select(x => new Course
                {
                    Id = x.Course.Id,
                    Name = x.Course.Name,
                    Description = x.Course.Description,
                    Gol = x.Course.Gol,
                    StartDate = x.Course.StartDate,
                    EndDate = x.Course.EndDate,
                    NodeId = x.Course.NodeId,
                    User = new User
                    {
                        FullName = x.Course.User.FullName,
                        Email = x.Course.User.Email,
                    },
                    Institution = x.Course.Institution,
                    UserId = x.Course.UserId,
                    Monday = x.Course.Monday,
                    Tuesday = x.Course.Tuesday,
                    Wednesday = x.Course.Wednesday,
                    Thursday = x.Course.Thursday,
                    Saturday = x.Course.Saturday,
                    Sunday = x.Course.Sunday,
                    Subject = x.Course.Subject,
                    Friday = x.Course.Friday,
                    Active = x.Course.Active,
                    StudentGroups = new List<Entities.StudentGroup>() {
                                                                          new Entities.StudentGroup{
                                                                               Id          = x.Id,
                                                                               Description = x.Description,
                                                                               Active      = x.Active,
                                                                                ExamSchedules = x.ExamSchedules?.Select( e => new Entities.ExamSchedule{
                                                                                    Active           = e.Active,
                                                                                    SchedulingPartialId = e.SchedulingPartialId,
                                                                                    Description = e.Description,
                                                                                    BeginApplicationDate = e.BeginApplicationDate,
                                                                                    EndApplicationDate = e.EndApplicationDate,
                                                                                    BeginApplicationTime = e.BeginApplicationTime,
                                                                                    EndApplicationTime = e.EndApplicationTime,
                                                                                    MinutesExam = e.MinutesExam,
                                                                                    ExamScheduleType = new ExamScheduleType{
                                                                                        Id           = e.ExamScheduleType.Id ,
                                                                                        Description  =  e.ExamScheduleType.Description },
                                                                                    Id = e.Id,
                                                                                    TeacherExam = new Entities.TeacherExam{
                                                                                        Id          = e.TeacherExam != null ?  e.TeacherExam.Id :  0 ,
                                                                                        Description = e.TeacherExam?.Description ?? ""
                                                                                    },
                                                                                    User = new Entities.User{
                                                                                        FullName = e.User.FullName
                                                                                    }
                                                                                }).ToList()
                                                                          }
                                                                      },
                    CourseSections = x.Course.CourseSections
                                                                                               .Select(s => new Entities.CourseSection
                                                                                               {
                                                                                                   Content = s.Content,
                                                                                                   Name = s.Name,
                                                                                                   Id = s.Id,
                                                                                                   Order = s.Order,
                                                                                                   CourseClasses = s.CourseClasses.Select(c => new Entities.CourseClass
                                                                                                   {
                                                                                                       Content = c.Content,
                                                                                                       Id = c.Id,
                                                                                                       CourseSectionId = c.CourseSectionId,
                                                                                                       Name = c.Name,
                                                                                                       Files = c.Files?.Select(r => new Entities.TeacherResources
                                                                                                       {
                                                                                                           Id = r.Id,
                                                                                                           Name = r.Name,
                                                                                                           Url = r.Url,
                                                                                                           Description = r.Description
                                                                                                       }).ToList()
                                                                                                   }).ToList(),

                                                                                               }).ToList()
                }).ToList();



                response.Data = studentCourses;
                response.Success = true;
                response.Message = "Cursos del usuario obtenidos correctamente";


            }
            catch (Exception)
            {
                response.Message = "Problemas al obtener los cursos del estudiante";
                response.Success = false;

            }

            return response;
        }

        /// <summary>
        /// Obtener los cursos a los que esta inscrito un usuario
        /// </summary>
        /// <param name="usuarioId"></param>
        /// <returns></returns>
        public async Task<Response> GetCoursesByStudent(int usuarioId)
        {


            var response = new Response();

            try
            {
                var studentCourses = (await _StudentGroupRepository.FindAllAsync(x => x.Students.Any(u => u.UserId == usuarioId && u.CheckDate != null)))
                                      .Select(x => new Course
                                      {
                                          Id = x.Course.Id,
                                          Name = x.Course.Name,
                                          Description = x.Course.Description,
                                          Gol = x.Course.Gol,
                                          StartDate = x.Course.StartDate,
                                          EndDate = x.Course.EndDate,
                                          NodeId = x.Course.NodeId,
                                          UserId = x.Course.UserId,
                                          Monday = x.Course.Monday,
                                          Tuesday = x.Course.Tuesday,
                                          Wednesday = x.Course.Wednesday,
                                          Thursday = x.Course.Thursday,
                                          Saturday = x.Course.Saturday,
                                          Sunday = x.Course.Sunday,
                                          Subject = x.Course.Subject,
                                          Friday = x.Course.Friday,
                                          Active = x.Course.Active,
                                          CourseSections = x.Course.CourseSections
                                                                   .Select(s => new Entities.CourseSection
                                                                   {
                                                                       Content = s.Content,
                                                                       Name = s.Name,
                                                                       Id = s.Id,
                                                                       Order = s.Order,
                                                                       CourseClasses = s.CourseClasses.Select(c => new Entities.CourseClass
                                                                       {
                                                                           Content = c.Content,
                                                                           Id = c.Id,
                                                                           CourseSectionId = c.CourseSectionId,
                                                                           Name = c.Name,
                                                                       }).ToList()
                                                                   }).ToList()
                                      }).ToList();



                response.Data = studentCourses;
                response.Success = true;
                response.Message = "Cursos del usuario obtenidos correctamente";


            }
            catch (Exception)
            {
                response.Message = "Problemas al obtener los cursos del estudiante";
                response.Success = false;

            }

            return response;

        }

        /// <summary>
        /// Método para enviar invitaciones a un curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> ConfirmInvitation(string codigo)
        {
            var response = new Response();

            try
            {
                var sr = await _StudentRepository.FindAsync(s => s.Codigo.Equals(codigo));

                if (sr.StudentStatusId == 1) // Invitado
                {
                    var publisingNodes = sr.User.PublishingProfileUsers
                                                .SelectMany(x => x.PublishingProfile.Nodes);
                    var nodeCoure = sr.StudentGroup.Course.Node;

                    if (!publisingNodes.Any(x => x.Id == nodeCoure.Id))
                    {
                        var user = sr.User;
                        user.Nodes.Add(nodeCoure);
                        _UserRepository.Update(user);
                    }

                    sr.CheckDate = DateTime.UtcNow.AddHours(offset.Hours);
                    sr.StudentStatusId = 2; // Confirmado
                    _StudentRepository.Update(sr);

                    await this._unitOfWork.SaveChangesAsync();

                    response.Data = "";
                    response.Success = true;
                    response.Message = "Datos guardados correctamente";
                }
                else
                {
                    response.Data = "";
                    response.Success = true;
                    response.Message = "La confirmacion ya se";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al confirmar invitación: { ex.Message }";
            }

            return response;
        }

    }
}