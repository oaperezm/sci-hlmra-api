﻿using Project.Entities;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class ExamScore: IExamScore
    {
        private readonly IExamScoreRepository _examScoreRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ExamScore(IExamScoreRepository examScoreRepository, IUnitOfWork unitOfWork)
        {
            this._examScoreRepository = examScoreRepository;
            this._unitOfWork             = unitOfWork;
        }

        /// <summary>
        /// Obtener el listado de cursos por profesor
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId) {

            var response = new Response();

            try
            {
                var data = (await _examScoreRepository.FindAllAsync(x => x.UserId == userId))
                          .Select(x => new Entities.ExamScore
                          {
                              Id               = x.Id,
                              UserId           = x.UserId,
                              ExamScheduleId   = x.ExamScheduleId,
                              Score            = x.Score,
                              CorrectAnswers   = x.CorrectAnswers,
                              Comments         = x.Comments,
                              RegisterDate     = x.RegisterDate,
                              UpdateDate       = x.UpdateDate,

                          });

                response.Data    = data;               
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        /// <summary>
        /// Obtener el listado de cursos por profesor
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetByExamUser(int ExamScheduleId, int UserId)
        {

            var response = new Response();

            try
            {
                var data = (await _examScoreRepository.FindAllAsync(x => x.UserId == UserId && x.ExamScheduleId == ExamScheduleId))
                          .Select(x => new Entities.ExamScore
                          {
                              Id = x.Id,
                              UserId = x.UserId,
                              ExamScheduleId = x.ExamScheduleId,
                              Score = x.Score,
                              CorrectAnswers = x.CorrectAnswers,
                              Comments = x.Comments,
                              RegisterDate = x.RegisterDate,
                              UpdateDate = x.UpdateDate,

                          });

                response.Data = data;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        /// <summary>
        /// Obtener programación de examenes por grupo
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupId(int studentGroupId)
        {

            var response = new Response();

            try
            {
                var data = (await _examScoreRepository.FindAllAsync(x => x.ExamSchedule.StudentGroupId == studentGroupId))
                          .Select(x => new Entities.ExamScore
                          {
                              Id = x.Id,
                              UserId = x.UserId,
                              ExamScheduleId = x.ExamScheduleId,
                              Score = x.Score,
                              CorrectAnswers = x.CorrectAnswers,
                              Comments = x.Comments,
                              RegisterDate = x.RegisterDate,
                              UpdateDate = x.UpdateDate
                          });

                response.Data    = data;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        
        /// <summary>
        /// Método para agregar una nueva programación de examenes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ExamScoreSaveRequest request)
        {
            var result = new Response();

            try
            {               
                Entities.ExamScore examScore = new Entities.ExamScore();

                examScore.ExamScheduleId    = request.ExamScheduleId;
                examScore.UserId            = request.UserId;
                examScore.Score             = request.Score;
                examScore.CorrectAnswers    = request.CorrectAnswers;
                examScore.Comments          = request.Comments;
                examScore.RegisterDate      = DateTime.Now;
                //examScore.UpdateDate        = DateTime.Now;


                _examScoreRepository.Add(examScore);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = examScore.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Guardar las calificaciones de un grupo
        /// </summary>
        /// <param name="scoreList"></param>
        /// <returns></returns>
        public async Task<Response> SaveGroupScore(List<ExamScoreSaveRequest> scoreList)
        {
            var result = new Response();

            try
            {

                foreach (var score in scoreList)
                {
                    Entities.ExamScore examScore = new Entities.ExamScore();

                    examScore = _examScoreRepository.GetMany(x => x.ExamScheduleId == score.ExamScheduleId && x.UserId == score.UserId).FirstOrDefault();

                    if (examScore != null && examScore.Id != 0)
                    {
                        examScore.ExamScheduleId = score.ExamScheduleId;
                        examScore.UserId = score.UserId;
                        examScore.Score = score.Score;
                        examScore.CorrectAnswers = score.CorrectAnswers;
                        examScore.Comments = score.Comments;                        
                        examScore.UpdateDate = DateTime.Now;

                        _examScoreRepository.Update(examScore);
                        await this._unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        examScore = new Entities.ExamScore();

                        examScore.ExamScheduleId = score.ExamScheduleId;
                        examScore.UserId = score.UserId;
                        examScore.Score = score.Score;
                        examScore.CorrectAnswers = score.CorrectAnswers;
                        examScore.Comments = score.Comments;
                        examScore.RegisterDate = DateTime.Now;
                        //examScore.UpdateDate        = DateTime.Now;

                        _examScoreRepository.Add(examScore);
                        await this._unitOfWork.SaveChangesAsync();
                    }                    
                }



                result.Data = null;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de una programación de exámenes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var examScore = await _examScoreRepository.FindAsync(x => x.Id == id);

                if (examScore == null)
                {
                    throw new Exception(" No se econtraron los datos de la calificación del examen");
                }

                _examScoreRepository.Delete(examScore);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para actualizar los datos de una programación de examenes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(ExamScoreUpdateRequest request)
        {
            var result = new Response();

            try
            {
                var examScore = await _examScoreRepository.FindAsync(x => x.Id.Equals(request.Id));

                if (examScore == null) {
                    throw new Exception(" No se econtraron los datos de la calificación del examen");
                }

                examScore.ExamScheduleId    = request.ExamScheduleId;
                examScore.UserId            = request.UserId;
                examScore.Score             = request.Score;
                examScore.CorrectAnswers    = request.CorrectAnswers;
                examScore.Comments          = request.Comments;
                //examScore.RegisterDate = DateTime.Now;
                examScore.UpdateDate        = DateTime.Now;

                _examScoreRepository.Update(examScore);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = examScore.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener el resumen de un examen y la contestación de un usuario
        /// </summary>
        /// <param name="teacherExamId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetScoreExam(int examScheduleId, int userId)
        {

            var result = new Response();

            try
            {
                var score = await _examScoreRepository.FindAsync(s => s.ExamScheduleId == examScheduleId && s.UserId == userId);

                if (score == null)
                    throw new Exception("Datos de la calificación no encontrados");

                var et   = score.ExamSchedule.TeacherExam;
                var test = score.ExamSchedule.Tests;

                var teacherExamn = new Entities.TeacherExam
                {
                    Id = et.Id,
                    Active = et.Active,
                    Description       = et.Description,
                    TeacherExamTypeId = et.TeacherExamTypeId,
                    TeacherExamTypes  = new Entities.TeacherExamType
                    {
                        Id = et.TeacherExamTypes.Id,
                        Description = et.TeacherExamTypes.Description
                    },
                    TeacherExamQuestion = et.TeacherExamQuestion.Select(t => new TeacherExamQuestion
                    {
                        Id = t.Id,
                        Question = new Entities.Question

                        {
                            Id = t.Question.Id,
                            Content = t.Question.Content,
                            Explanation = t.Question.Explanation,
                            QuestionType = new Entities.QuestionType
                            {
                                Id = t.Question.QuestionType.Id,
                                Description = t.Question.QuestionType.Description
                            },
                            UrlImage    = t.Question.UrlImage,
                            ImageWidth  = t.Question.ImageWidth,
                            ImageHeight = t.Question.ImageHeight,
                            Answers     = t.Question.Answers.Select(a => new Entities.Answer
                            {
                                Id                  = a.Id,
                                Description         = a.Description,
                                IsCorrect           = a.IsCorrect,
                                RelationDescription = a.RelationDescription,
                                Active              = a.Active,
                                ImageWidth          = a.ImageWidth,
                                ImageHeight         = a.ImageHeight
                            }).ToList()
                        }
                    }).ToList()
                };



            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al obtener los datos: { ex.Message }";
            }


            return result;

        }
    

}
}
