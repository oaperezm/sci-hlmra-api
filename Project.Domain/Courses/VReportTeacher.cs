﻿using Project.Domain.Core;
using Project.Entities.Responses;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VReportTeacher: IVReportTeacher
    {
        private readonly IVReportTeacherRepository _vReportTeacherRepository;

        public VReportTeacher(IVReportTeacherRepository vreportExamRepository)
        {
            _vReportTeacherRepository = vreportExamRepository;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                var reportCoordinator = new List<VCoordinatorReport>();
                result.Data = _vReportTeacherRepository.GetAll().Select(x => new
                {
                    x.Id,
                    email = x.StudentEmail,
                    userName = x.Student,
                    nameSchool = x.Institution,
                    level = x.Level,
                    x.StudentGroupId,
                    x.StudentUserId,
                    signature = x.Subject,
                    starDate = x.CheckDate,
                    //endDate = x.endDate

                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
