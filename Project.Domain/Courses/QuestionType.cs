﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;

namespace Project.Domain.Courses
{
    public class QuestionType : IQuestionType
    {
        private readonly IQuestionTypeRepository _QuestionTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public QuestionType(IQuestionTypeRepository QuestionTypeRepository, IUnitOfWork unitOfWor)
        {

            this._QuestionTypeRepository = QuestionTypeRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddQuestionType(QuestionTypeSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.QuestionType QuestionType = new Entities.QuestionType();

                QuestionType.Description = request.Description;
                QuestionType.Active = request.Active;

                _QuestionTypeRepository.Add(QuestionType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = QuestionType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteQuestionType(int QuestionTypeId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _QuestionTypeRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        Active = u.Active
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _QuestionTypeRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateQuestionType(QuestionTypeUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.QuestionType QuestionType = new Entities.QuestionType();

                QuestionType.Id = request.Id;
                QuestionType.Description = request.Description;
                QuestionType.Active = request.Active;

                _QuestionTypeRepository.Update(QuestionType);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = QuestionType;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}