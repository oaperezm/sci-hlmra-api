﻿using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class VReportProfileTeacher: IVReportProfileTeacher
    {
        private readonly IVReportProfileTeacherReposytory _vReportProfileTeacherReposytory;
        private readonly IUnitOfWork _unitOfWork;
        public VReportProfileTeacher(IVReportProfileTeacherReposytory vReportProfileTeacherReposytory, IUnitOfWork unitOfWor)
        {
            this._vReportProfileTeacherReposytory = vReportProfileTeacherReposytory;
            this._unitOfWork = unitOfWor;
            
        }
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _vReportProfileTeacherReposytory.GetAll().Select(x => new
                {
                    x.Id,
                    email = x.Email,
                    userName = x.FullName,
                    nameSchool = x.NameSchool,
                    level = x.Level,
                    x.Groups,
                    x.Period,
                    signature = x.Subject,
                    //endDate = x.endDate

                }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
