﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Domain.Courses
{
    public interface IActivityAnswer
    {
        Task<Response> AddAnswer(ActivityAnswerSaveRequest request);
        Task<Response> DeleteAnswer(int ActivityId, int userId, int answerId);
        Task<Response> GetStundentFileByActivity(int homeWorkId, int userId);
        Task<Response> SaveScore(ActivityScoreRequest request);
        Task<Response> GetAllWithScore(int studentGroupId, int userId);
    }
}
