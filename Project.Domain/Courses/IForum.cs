﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IForum
    {
        Task<Response> AddThread(int userId, ThreadSaveRequest request);
        Task<Response> UpdateThread(ThreadUpdateRequest request);
        Task<Response> GeThreadById(int userId, int id, int rolId);
        Task<Response> GetByUser(int userId, int rolId, int systemId);

        Task<Response> AddPost(int userId, PostSaveRequest request);
        Task<Response> UpdatePost(PostUpdateRequest request);
        Task<Response> DeletePost(int userId, int postId);
        Task<Response> UpdateStatus(PostUpdateStatusRequest request);
        Task<Response> UpdateVote(int postId, int userId, int vote);
    }
}
