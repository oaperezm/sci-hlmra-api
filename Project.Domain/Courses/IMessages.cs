﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IMessages
    {
        Task<Response> SendMessage(int senderUserId, SendMessageRequest request);
        Task<Response> GetByChatGroup(int chatGroupId, int userId);
        Task<Response> MarkRead(int reciverUserId, bool read);
        Task<Response> GetAllConversations(int userId);
        Task<Response> GetContact(int userId, int rolId);
    }
}
