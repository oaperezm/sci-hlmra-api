﻿using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Forum: IForum
    {
        private readonly IForumPostRepository _forumPostRepository;
        private readonly IForumThreadRepository _forumThreadRepository;
        private readonly IForumPostVoteRepository _forumPostVoteRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IUnitOfWork _uniOfWork; 

        public Forum(IForumPostRepository forumPostRepository,
                    IForumThreadRepository forumThreadRepository,
                    IStudentRepository studentRepository,
                    IStudentGroupRepository studentGroupRepository,
                    IForumPostVoteRepository forumPostVoteRepository,
                    IUnitOfWork uniOfWork)
        {
            _forumPostRepository    = forumPostRepository;
            _forumThreadRepository  = forumThreadRepository;
            _studentRepository      = studentRepository;
            _studentGroupRepository = studentGroupRepository;
            _forumPostVoteRepository = forumPostVoteRepository;
            _uniOfWork              = uniOfWork;
        }

        #region THREAD

        /// <summary>
        /// Agregar un nuevo hilo
        /// </summary>
        /// <param name="userId">Identificador del maestro que genera el hilo</param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddThread(int userId , ThreadSaveRequest request) {

            var result = new Response();

            try
            {
                var forumThread = new ForumThread
                {
                    Active      = true,
                    CourseId    = request.CourseId,
                    Description = request.Description,
                    InitDate    = request.InitDate,
                    Title       = request.Title,
                    UserId      = userId,
                    Views       = 0,
                };

                if (request.EndDate.HasValue)
                    forumThread.EndDate = request.EndDate;

                if (request.Groups != null && request.Groups.Count > 0) {
                    var groups = await _studentGroupRepository.FindAllAsync(x => request.Groups.Contains(x.Id));

                    if (groups != null) {
                        groups.ToList().ForEach(g => forumThread.Groups.Add(g));
                    }
                }

                _forumThreadRepository.Add(forumThread);
                await _uniOfWork.SaveChangesAsync();

                result.Data    = forumThread.Id;
                result.Success = true;
                result.Message = "Sala agregada correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar una nueva sala { ex.Message }";
            }



            return result;

        }

        /// <summary>
        /// Actualizar los datos de un hilo 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateThread(ThreadUpdateRequest request) {

            var result = new Response();

            try
            {
                var forumThread = await _forumThreadRepository.FindAsync(x => x.Id == request.Id);
                
                forumThread.Description = request.Description;
                forumThread.InitDate    = request.InitDate;
                forumThread.Title       = request.Title;
                
                if (request.EndDate.HasValue)
                    forumThread.EndDate = request.EndDate;

                if (request.Groups != null && request.Groups.Count > 0)
                {
                    forumThread.Groups.Clear();
                    var groups = await _studentGroupRepository.FindAllAsync(x => request.Groups.Contains(x.Id));

                    if (groups != null)                    
                        groups.ToList().ForEach(g => forumThread.Groups.Add(g));  
                }

                _forumThreadRepository.Update(forumThread);
                await _uniOfWork.SaveChangesAsync();

                result.Data = forumThread.Id;
                result.Success = true;
                result.Message = "Datos de la sala actualizados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al actualizar los datos de la sala: { ex.Message }";
            }



            return result;
        }

        /// <summary>
        /// Obtener un hilo por su identidicador principal
        /// </summary>
        /// <param name="id">Identificador principal dle hilo</param>
        /// <returns></returns>
        public async Task<Response> GeThreadById(int userId, int id, int rolId) {

            var result    = new Response();
            var belongs   = false;
            var isTeacher = false;

            try
            {
                var thread = await _forumThreadRepository.FindAsync(x => x.Id == id);
          
                if (thread == null)
                    throw new Exception("Datos no encontrados");

                switch (rolId)
                {
                    case (int)EnumRol.Teacher:
                    case (int)EnumRol.TeacherRA:
                        belongs   = thread.Course.UserId == userId;
                        isTeacher = true;
                        break;

                    case (int)EnumRol.Student:
                    case (int)EnumRol.StudentRA:
                        belongs = thread.Course
                                 .StudentGroups
                                 .SelectMany(x => x.Students)
                                 .Any(u => u.UserId == userId);
                        break;

                    case (int)EnumRol.Administrator:
                    case (int)EnumRol.AdministratorRA:
                        belongs = true;
                        break;
                }             

                if (!belongs)
                    throw new Exception("El usuario no tiene acceso al grupo");                               

                thread.Views = thread.Views.HasValue ?  thread.Views + 1 : 1;
                _forumThreadRepository.Update(thread);
                await _uniOfWork.SaveChangesAsync();
                
                result.Data    = new {
                    thread = thread.NewWithDetail(userId),
                    isTeacher
                };

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al obtener los datos { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener los hilos correspondiente por usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUser(int userId, int rolId, int systemId) {

            var result     = new Response();
            var threads    = new List<ForumThreadDTO>();
            var data       = default(ICollection<ForumThread>);            
            var groupNames = default(List<string>);

            try
            {
                switch (rolId) {
                    // Obtener todos los hilos que genera el profesor
                    case (int)EnumRol.Teacher:
                    case (int)EnumRol.TeacherRA:
                        data       = await _forumThreadRepository.FindAllAsync(x => x.UserId == userId);
                        
                        threads = data.OrderBy(x => x.InitDate)
                                      .Select(x => x.New(userId))
                                      .ToList();

                        groupNames = data.SelectMany(x => x.Groups)
                                         .Distinct()
                                         .Select(x => x.Description)
                                         .OrderBy(x => x)
                                         .ToList();

                        break;
                    // Obtener todos los hilos de los cursos inscritos al grupo
                    case (int)EnumRol.Student:
                    case (int)EnumRol.StudentRA:
                        var groups   = await _studentRepository.FindAllAsync(u => u.UserId == userId);                      
                        var groupIds = groups.Select(x => x.StudentGroupId).ToList();

                        data = await _forumThreadRepository.GetAllAsync();
                        var threadsGroup = data.SelectMany(x => x.Groups)
                                               .Where(x => groupIds.Contains(x.Id))
                                               .SelectMany(x => x.Threads)
                                               .Distinct();

                        threads = threadsGroup.OrderBy(x => x.InitDate)
                                               .Select(x => x.New(userId))
                                               .ToList();

                        groupNames = data.SelectMany(x => x.Groups).Distinct()
                                         .Where( x=> groupIds.Contains( x.Id))
                                         .Select(x => x.Description).OrderBy(x => x).ToList();

                        break;
                    // Obtener todos los hilos registrados
                    case (int)EnumRol.Administrator:
                    case (int)EnumRol.AdministratorRA:
                        data = await _forumThreadRepository.FindAllAsync(x => x.User.SystemId == systemId);

                        threads = data.OrderBy(x => x.InitDate)
                            .Select(x => x.New(userId))
                            .ToList();

                        break;
                    default:
                        threads = new List<ForumThreadDTO>();
                        break;

                }

                

                var filters = new
                 {
                    Courses  = threads?.Select(x => x.CourseName).Distinct(),
                    Teachers = threads?.Select(x => x.Creator.FullName).Distinct(),
                    groups   = groupNames,
                 };                

                result.Data    = new { threads, filters };
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {                
                result.Success = false;
                result.Message = $"Problemas al obtener las salas del foro { ex.Message }";              
            }

            return result;
        }

        #endregion

        #region POST

        /// <summary>
        /// Agregar un nuevo post
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPost(int userId, PostSaveRequest request) {

            var result = new Response();

            try
            {
                var forumThread = await _forumThreadRepository.FindAsync(x => x.Id == request.ForumThreadId);
                
                if (forumThread == null)
                    throw new Exception("No fue encontrado la sala en le cual se realizará la publicación");

                var post = new ForumPost
                {
                    Description   = request.Description,
                    ForumThreadId = request.ForumThreadId,
                    Replay        = request.Replay,
                    Title         = request.Title,
                    UserId        = userId,
                    RegisterDate  = DateTime.Now,
                };

                _forumPostRepository.Add(post);
                await _uniOfWork.SaveChangesAsync();

                result.Data    = post.Id;
                result.Success = true;
                result.Message = "Comentario agregado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar un nuevo comentario { ex.Message }";
            }
                       
            return result;
        }

        /// <summary>
        /// Actualizar los datos d eun post
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePost(PostUpdateRequest request)
        {
            var result = new Response();

            try
            {
                var post = await _forumPostRepository.FindAsync(x => x.Id == request.Id);

                post.Description = request.Description;
                post.Replay      = request.Replay;
                post.Title       = request.Title;
                
                _forumPostRepository.Update(post);
                await _uniOfWork.SaveChangesAsync();

                result.Data    = post.Id;
                result.Success = true;
                result.Message = "Comentario actualizado correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al actualizar los datos del comentario: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un post existente
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns></returns>
        public async Task<Response> DeletePost(int userId, int postId) {

            var result = new Response();

            try
            {
                var post = await _forumPostRepository.FindAsync(x => x.Id == postId && x.UserId == userId);

                if (post == null)
                    throw new Exception("Datos no encontrados");
                                
                post.ForumPostVotes.ToList().ForEach(v => { _forumPostVoteRepository.Delete(v); });
                
                _forumPostRepository.Delete(post);
                await _uniOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al eliminar el post {ex.Message}";
            }

            return result;
        }
        
        /// <summary>
        /// Marcar post como mejor respuesta
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(PostUpdateStatusRequest request) {

            var result = new Response();

            try
            {
                var post = await _forumPostRepository.FindAsync(x => x.Id == request.Id);

                if (post == null)
                    throw new Exception("Datos no encontrados");

                post.BestAnswer = request.Status;

                _forumPostRepository.Update(post);
                await _uniOfWork.SaveChangesAsync();
                                
                result.Success = true;
                result.Message = "Estatus actualizado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al actualizar el estatus del post {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Marcar post como me gusta
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="vote"></param>
        /// <returns></returns>
        public async Task<Response> UpdateVote(int postId, int userId, int vote) {

            var result  = new Response();
            var canVote = true;

            try
            {
                var post = await _forumPostRepository.FindAsync(x => x.Id == postId);

                if (post == null)
                    throw new Exception("Datos no encontrados");

                var postVote = await _forumPostVoteRepository.FindAsync(x => x.ForumPostId == postId && x.UserId == userId);

                if (postVote != null) {       
                    
                    _forumPostVoteRepository.Delete(postVote);
                    canVote = true;

                } else {

                    post.ForumPostVotes.Add(new ForumPostVote
                    {
                        ForumPostId  = postId,
                        UserId       = userId,
                        Like         = true,
                        RegisterDate = DateTime.Now
                    });

                    canVote = false;
                }

                post.Votes = post.ForumPostVotes.Count();
                _forumPostRepository.Update(post);

                await _uniOfWork.SaveChangesAsync();

                result.Data    = canVote;
                result.Success = true;
                result.Message = "Estatus actualizado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al actualizar el estatus del post {ex.Message}";
            }

            return result;

        }


        #endregion

    }
}
