﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IQuestionType
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddQuestionType(QuestionTypeSaveRequest request);
        Task<Response> UpdateQuestionType(QuestionTypeUpdateRequest request);
        Task<Response> DeleteQuestionType(int QuestionTypeId);
    }
}
