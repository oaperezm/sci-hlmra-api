﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IExamSchedule
    {
        Task<Response> GetAllByUserId(int userId);
        Task<Response> GetAllByStudentGroupId(int studentGroupId);
        Task<Response> UpdateStatus(int id, bool active);
        Task<Response> Add(ExamScheduleSaveRequest request, int userId, int systemId);
        Task<Response> Update(ExamScheduleUpdateRequest request, int userId, int systemId);
        Task<Response> Delete(int id);
        Task<Response> GetAllByStudentGroupIdAndUser(int studentGroupId, int userId);
        Task<Response> UpdateExamScheduleType(int examScheduleId, int examScheduleTypeId);
    }
}
