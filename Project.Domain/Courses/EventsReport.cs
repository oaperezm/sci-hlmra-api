﻿using Project.Domain.Core;
using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Entities.Views;
using Project.Infraestructure.Repositories.Courses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Repositories.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class EventsReport : IEventsReport
    {

        private readonly IEventsReportRepository _eventsReportRepository;
        private readonly IEducationLevelRepository _educationLevelRepository;
       
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVCoordinatorReportRepository _vCoordinatorReportRepository;
        private readonly IVReportCourseRepository _vReportCourseRepository;
        private readonly IVReportExamRepository _vreportExamRepository;
        private readonly IStudentScore _StudentScore;
        private readonly IVReportTeacherRepository _vReportTeacherRepository;
        private readonly IUserCCTRepository _userCCTRepository;
        private readonly IVReportProfileTeacherReposytory _vReportProfileTeacherReposytory;
        public EventsReport(IEventsReportRepository eventsReportRepository
            ,IUnitOfWork unitOfWor 
            ,IEducationLevelRepository educationLevelRepository
            , IVCoordinatorReportRepository vCoordinatorReportRepository
            , IVReportCourseRepository vReportCourseRepository
            , IVReportExamRepository vReportExamRepository,
            IVReportTeacherRepository vReportTeacherRepository,
            IStudentScore vstudentScore
            ,IUserCCTRepository userCCTRepository
            , IVReportProfileTeacherReposytory vReportProfileTeacherReposytory
            )
        {
            this._eventsReportRepository = eventsReportRepository;
            this._unitOfWork = unitOfWor;
            this._educationLevelRepository = educationLevelRepository;
            this._vCoordinatorReportRepository = vCoordinatorReportRepository;
            this._vReportCourseRepository = vReportCourseRepository;
            this._vreportExamRepository = vReportExamRepository;
            this._vReportTeacherRepository = vReportTeacherRepository;
            this._StudentScore = vstudentScore;
            this._userCCTRepository = userCCTRepository;
            this._vReportProfileTeacherReposytory = vReportProfileTeacherReposytory;
        }


        /// <summary>
        /// Método para agregar un nuevo evento
        /// </summary>
        /// param name="request"
        /// <returns></returns>
        public async Task<Response> Add(EventsReportUpdateRequest request)
        {

            var response = new Response();

            try
            {
                Entities.EducationLevel level = new Entities.EducationLevel();
                if (request.EducationLevelId==0 )
                {
                   
                    level = _educationLevelRepository.GetMany(x => (x.Description.ToUpper()) == (request.Level.ToUpper())).FirstOrDefault();

                    request.EducationLevelId = level.Id;
                }


                Entities.EventsReport eventsReport = new Entities.EventsReport();
                eventsReport = await _eventsReportRepository.FindAsync(x => x.Id == request.Id);
                if (eventsReport == null)
                {
                    eventsReport = new Project.Entities.EventsReport
                    {



                        CCT = request.CCT,
                        Email = request.Email,
                        Username = request.Username,
                        InstitutionName = request.InstitutionName,
                        UserId = request.UserId.Value,
                        Level = request.Level,
                        State = request.State,
                        ClasificationSchool = request.ClasificationSchool,
                        Zone = request.Zone,
                        Control = request.Control,
                        Prometer = request.Prometer,
                        EntryDate = DateTime.Now,
                        TimeNavegation = request.TimeNavegation,
                        Subject = request.Subject,
                        SectionId = request.SectionId,
                        ResourcenName = request.ResourceName,
                        Grade = request.Grade,
                        EducationField = request.EducationField,
                        FileTypeId = request.FileTypeId,
                        RegisterDate = DateTime.Now,
                        EventId = request.EventId,
                        Element = request.Element,
                        Comment = request.Comment,
                        CommentDetail = request.CommentDetail,
                        EducationLevelId = request.EducationLevelId,
                        ContentTypeId = request.ContentTypeId,
                        UpdateDate = DateTime.Now

                };
                    _eventsReportRepository.Add(eventsReport);
                    
                }
                else
                {
                    eventsReport.UpdateDate = DateTime.Now;
                    eventsReport.TimeNavegation= (eventsReport.UpdateDate.Value - eventsReport.EntryDate).TotalMinutes.ToString();
                    _eventsReportRepository.Update(eventsReport);
                }




                    

              
                await _unitOfWork.SaveChangesAsync();
                response.Data = eventsReport.Id;
                response.Success = true;
                response.Message = "Datos guardados correctamente";
                
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo evento: { ex.Message }";
            }

            return response;




        }
        /// <summary>
        /// Método para obtener todos los datos de un evento
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _eventsReportRepository.GetAll().
                    Select(e => new
                    {
                        e.CCT,
                        e.Email,
                        e.Username,
                        e.InstitutionName,
                        e.UserId,
                        e.Level,
                        e.State,
                        e.ClasificationSchool,
                        e.Zone,
                        e.Control,
                        e.Prometer,
                        e.EntryDate,
                        e.TimeNavegation,
                        e.Subject,
                        e.SectionId,
                        e.Grade,
                        e.EducationField,
                        e.FileTypeId,
                        e.RegisterDate,
                        e.EventId,
                        e.Element,
                        e.Comment,
                        e.CommentDetail,
                        e.EducationLevelId,
                        e.ContentTypeId
                        
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        /// <summary>
        /// Método para obtener todos los datos de un evento por id
        /// </summary>
        /// param name="id"
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {

                var EventsReport = await _eventsReportRepository.FindByAsyn(r => r.Id == id);


                if (EventsReport != null)
                {


                    result.Data = EventsReport.Select(e => new
                    {
                        e.Id,
                        e.CCT,
                        e.Email,
                        e.Username,
                        e.InstitutionName,
                        e.UserId,
                        e.Level,
                        e.State,
                        e.ClasificationSchool,
                        e.Zone,
                        e.Control,
                        e.Prometer,
                        e.EntryDate,
                        e.TimeNavegation,
                        e.Subject,
                        e.SectionId,
                        e.Grade,
                        e.EducationField,
                        e.FileTypeId,
                        e.RegisterDate,
                        e.EventId,
                        e.Element,
                        e.Comment,
                        e.CommentDetail,
                        e.EducationLevelId,
                        e.ContentTypeId

                    });
                }

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
        /// <summary>
        /// Método para actualizar los datos de un evento 
        /// </summary>
        /// param name="request"
        /// <returns></returns>
        public async Task<Response> Update(EventsReportUpdateRequest request)
        {
            var response = new Response();
            try
            {
                Entities.EventsReport eventsReport = new Entities.EventsReport();
                eventsReport = await _eventsReportRepository.FindAsync(x => x.Id == request.Id);
                if (eventsReport == null)
                {
                    throw new Exception("No se encontraron datos del curso proporcionado");
                }
                else
                {
                    eventsReport.CCT = request.CCT;
                    eventsReport.Email = request.Email;
                    eventsReport.Username = request.Username;
                    eventsReport.InstitutionName = request.InstitutionName;
                    eventsReport.UserId = request.UserId.Value;
                    eventsReport.Level = request.Level;
                    eventsReport.State = request.State;
                    eventsReport.ClasificationSchool = request.ClasificationSchool;
                    eventsReport.Zone = request.Zone;
                    eventsReport.Control = request.Control;
                    eventsReport.Prometer = request.Prometer;
                    eventsReport.EntryDate = request.EntryDate;
                    eventsReport.TimeNavegation = request.TimeNavegation;
                    eventsReport.Subject = request.Subject;
                    eventsReport.SectionId = request.SectionId;
                    eventsReport.Grade = request.Grade;
                    eventsReport.EducationField = request.EducationField;
                    eventsReport.FileTypeId = request.FileTypeId;
                    eventsReport.UpdateDate = DateTime.Now;
                    eventsReport.EventId = request.EventId;
                    eventsReport.Element = request.Element;
                    eventsReport.Comment = request.Comment;
                    eventsReport.CommentDetail = request.CommentDetail;
                    eventsReport.EducationLevelId = request.EducationLevelId;
                    eventsReport.ContentTypeId = request.ContentTypeId;

                }
                _eventsReportRepository.Update(eventsReport);
                await _unitOfWork.SaveChangesAsync();

                response.Data = eventsReport.Id;
                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al actualizar un nuevo evento: { ex.Message }";
            }
            return response;
        }

        /// <summary>
        /// Método para obtener todos las consultas de reportes
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetReport(ReportRequest request)
        {
            var result = new Response();

            try
            {   if (request.reportId != 0)
                {
                    switch (request.reportId)
                    {
                        case (int)EnumReport.Functionality:
                            result.Data = (await GetReportFuncionality(request));
                            break;
                        case (int)EnumReport.CoordinatorProfile:
                            result.Data = (await GetReportCoordinator(request));
                            break;
                        case (int)EnumReport.Course:
                            result.Data = (await GetCourseReport(request));
                            break;
                      case (int)EnumReport.Exam:
                            result.Data = (await GetReportExam(request));
                            break;
                     case (int)EnumReport.StudentProfile:
                            result.Data = (ScoreStudent(request));
                            break;
                     case (int)EnumReport.TeacherProfile:
                            result.Data = ( ProfileTeacher(request));
                            break;


                            
                    }

                }
                else
                {
                    throw new Exception("Seleccione un reporte");
                }




            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            result.Success = true;
            return result;
        }

        /// <summary>
        /// Método para obtener consulta de reporte de funcionalidad
        /// </summary>
        /// param name="request"
        /// <returns></returns>
        public async Task<Response> GetReportFuncionality(ReportRequest request)
        {
            var result = new Response();

            try
            {

                if ((request.cct != "") && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();
                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        var report = _eventsReportRepository.GetMany(
                         x => x.CCT == request.cct && x.Level == level.Description && x.EntryDate >= request.startDate.Value && x.EntryDate <= request.endDate.Value).
                         Select(e => new
                         {
                             e.CCT,
                             e.Email,
                             e.Username,
                             e.InstitutionName,
                             e.UserId,
                             e.Level,
                             e.State,
                             e.ClasificationSchool,
                             e.Zone,
                             e.Control,
                             e.Prometer,
                             e.EntryDate,
                             e.TimeNavegation,
                             e.Subject,
                             e.SectionId,
                             e.Grade,
                             e.EducationField,
                             e.FileTypeId,
                             e.RegisterDate,
                             e.EventId,
                             e.Element,
                             e.Comment,
                             e.CommentDetail,
                             e.ResourcenName,
                             e.EducationLevelId,
                             e.ContentTypeId
                         }).ToList();

                        if (request.sectionId.HasValue || request.sectionId > 0)
                        {
                            report = report.Where(x => x.SectionId == request.sectionId).ToList();
                        }
                        if (request.FileTypeId.HasValue || request.FileTypeId > 0)
                        {
                            report = report.Where(x => x.FileTypeId == request.FileTypeId).ToList();
                        }
                        if (request.contentTypeId.HasValue || request.contentTypeId > 0)
                        {
                            report = report.Where(x => x.ContentTypeId == request.contentTypeId).ToList();
                        }
                        if (!string.IsNullOrEmpty(request.username))
                        {
                            report = report.Where(x => x.Username == request.username).ToList();
                        }

                        //if (request.startDate.HasValue && request.endDate.HasValue)
                        //{
                        //    report.Where(x => x.EntryDate >= request.startDate.Value && x.EntryDate <= request.endDate.Value).ToList();

                        //}

                            result.Data = report;

                    }
                    else
                    {
                        throw new Exception("Ingrese una fecha");
                    }


                    //var report = _eventsReportRepository.GetMany(
                    //           x => x.CCT == request.cct && x.Level == level.Description ).
                    //           Select(e => new
                    //           {
                    //               e.CCT,
                    //               e.Email,
                    //               e.Username,
                    //               e.InstitutionName,
                    //               e.UserId,
                    //               e.Level,
                    //               e.State,
                    //               e.ClasificationSchool,
                    //               e.Zone,
                    //               e.Control,
                    //               e.Prometer,
                    //               e.EntryDate,
                    //               e.TimeNavegation,
                    //               e.Subject,
                    //               e.SectionId,
                    //               e.Grade,
                    //               e.EducationField,
                    //               e.FileTypeId,
                    //               e.RegisterDate,
                    //               e.EventId,
                    //               e.Element,
                    //               e.Comment,
                    //               e.CommentDetail,
                    //               e.ResourcenName,
                    //               e.EducationLevelId,
                    //               e.ContentTypeId
                    //           }).ToList();

                    //if (request.sectionId.HasValue || request.sectionId > 0)
                    //{
                    //    report = report.Where(x => x.SectionId == request.sectionId).ToList();
                    //}
                    //if (request.FileTypeId.HasValue || request.FileTypeId > 0)
                    //{
                    //    report = report.Where(x => x.FileTypeId == request.FileTypeId).ToList();
                    //}
                    //if (request.contentTypeId.HasValue || request.contentTypeId > 0)
                    //{
                    //    report = report.Where(x => x.ContentTypeId == request.contentTypeId).ToList();
                    //}
                    //if (!string.IsNullOrEmpty(request.username))
                    //{
                    //    report = report.Where(x => x.Username == request.username).ToList();
                    //}

                    //result.Data = report;
                }      

                

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }
            result.Success = true;
            return result;

        }


        /// <summary>
        /// Método para obtener consulta de reporte de coordinador
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetReportCoordinator(ReportRequest request)
        {
            var result = new Response();

            try
            {

                if ((request.cct != "") && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();

                    var reportCoordinator = new List<Entities.Views.VCoordinatorReport>();
                    var CoordinatorViews =  _vCoordinatorReportRepository.GetMany(x=> x.cct==request.cct && x.level==request.level).ToList();
                    
                    //reportCoordinator = CoordinatorViews.Select(x => new Entities.Views.VCoordinatorReport
                    //{
                    //    Id = x.Id,
                    //    cct = x.cct,
                    //    email = x.email,
                    //    userName = x.userName,
                    //    nameSchool = x.nameSchool,
                    //    level = x.level,
                    //    //grade = x.grade,
                    //    //formativeField = x.formativeField,
                    //    //signature = x.signature,
                    //    course = x.course,
                    //    group = x.group,
                    //    totalStudents = x.totalStudents,
                    //    totalActivities = x.totalActivities,
                    //    totalScheduledExams = x.totalScheduledExams,
                    //    totalQuestionsBanks = x.totalQuestionsBanks,
                    //    totalQuestionTeacher = x.totalQuestionTeacher,
                    //    totalQuestions = x.totalQuestions,
                    //    startDate=x.startDate,
                    //    endDate=x.endDate
                    //}).ToList();


                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        CoordinatorViews = CoordinatorViews.Where(x => x.startDate >= request.startDate.Value ).ToList();
                        
                    }
                    //if (!string.IsNullOrEmpty(request.grade))
                    //{
                    //    reportCoordinator = reportCoordinator.Where(x => x.grade == request.grade).ToList();
                    //}
                    if (!string.IsNullOrEmpty(request.username))
                    {
                        CoordinatorViews = CoordinatorViews.Where(x => x.userName == request.username).ToList();
                    }

                    result.Data = CoordinatorViews;
                }





                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }
            result.Success = true;
            return result;

        }
        /// <summary>
        /// Método para obtener consulta de reporte de curso
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetCourseReport(ReportRequest request)
        {
            var result = new Response();

            try
            {

                if ((request.cct != "") && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();

                    var report= new List<Entities.VReportCourse>();
                    var View = (_vReportCourseRepository.GetMany(x => x.cct == request.cct && x.level == level.Description).ToList());

                    report = View.Select(x => new Entities.VReportCourse
                    {   Id = x.Id,
                        cct = x.cct,
                        email = x.email,
                        userName = x.userName,
                        nameSchool = x.nameSchool,
                        level = x.level,
                        courseRegisterDate = x.courseRegisterDate,
                        courseId = x.courseId,
                        courseName = x.courseName,
                        group = x.group,
                        startDate = x.startDate,
                        endDate = x.endDate
                    }).ToList();



                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        report = report.Where(x => x.startDate >= request.startDate.Value ).ToList();
                    }


                   

                    if (!string.IsNullOrEmpty(request.username))
                    {
                        report = report.Where(x => x.userName == request.username).ToList();
                    }

                    result.Data = report;
                }





                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

        }


        /// <summary>
        /// Método para obtener consulta de reporte de examen
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetReportExam(ReportRequest request)
        {
            var result = new Response();

            try
            {

                if ((request.cct != "") && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();

                    var report = new List<Entities.VReportExam>();
                    var View = (_vreportExamRepository.GetMany(x => x.cct == request.cct && x.level == level.Description).ToList());

                   

                    

                    report = View.Select(x => new Entities.VReportExam
                    {
                        Id = x.Id,
                        cct = x.cct,
                        email = x.email,
                        userName = x.userName,
                        nameSchool = x.nameSchool,
                        level = x.level,
                        totalquestion = x.totalquestion,
                        questionbank = x.questionbank,
                        signature = x.signature,
                        starDate = x.starDate
                        
                        //endDate = Convert.ToDateTime(x.endDate).ToString("dd/MM/yyyy"); 
                    }).ToList();


                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        report = report.Where(x => x.starDate.Date >= request.startDate.Value.Date && x.starDate.Date <=request.endDate.Value).ToList();
                    }


                    if (!string.IsNullOrEmpty(request.username))
                    {
                       report = report.Where(x => x.userName == request.username).ToList();
                    }
                    result.Data = report;
                }





                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

        }

        public List<VReportStudentDTO> ScoreStudent(ReportRequest request)
        {

            var result = new Response();
            List<Entities.DTO.VReportStudentDTO> report = new List<Entities.DTO.VReportStudentDTO>();
            try {
                
                if (!string.IsNullOrEmpty(request.cct) && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();

                    

                    //List<Entities.DTO.UserCCTDTO> ccts = new List<Entities.DTO.UserCCTDTO>();
                    //ccts = _userCCTRepository.GetMany(x => x.CCT == request.cct).Select(x => new Entities.DTO.UserCCTDTO { Id = x.Id, CCT = x.CCT, UserId = x.UserId }).ToList();
                    //var UserTecherid = ccts.Select(utc => utc.UserId).Distinct();
                  

                     Entities.UserCCT ucct = new Entities.UserCCT();
                    ucct = _userCCTRepository.GetMany(x => x.CCT == request.cct).Select(x => new Entities.UserCCT { Id = x.Id, CCT = x.CCT, UserId = x.UserId }).FirstOrDefault() ;

                    var View = _vReportTeacherRepository.GetMany(x => (x.Level == level.Description) && x.TeacherId== ucct.UserId).ToList();



                    report = View.Select(x => new VReportStudentDTO
                    {
                        Id = x.Id,
                        TeacherId = x.TeacherId,
                        Teacher = x.Teacher,
                        Student = x.Student,
                        StudentEmail = x.StudentEmail,
                        StudentGroupDescription = x.StudentGroupDescription,
                        Institution = x.Institution,
                        StudentUserId = x.StudentUserId,
                        StudentGroupId = x.StudentGroupId,
                        CourseId = x.CourseId,
                        Subject = x.Subject,
                        Level = x.Level,
                        DaysPassed = x.DaysPassed,
                        totalAttendances = x.totalAttendances,
                        CheckDate = x.CheckDate,
                        PHomework = x.PHomework,
                        PExam = x.PExam,
                        PAssistance = x.PAssistance,
                        PActivity = x.PActivity,
                        Grade =x.Grade,
                        UserCCT = ucct.CCT ,//ccts.TakeWhile(uc => uc.UserId == x.TeacherId).ToList(),
                        studentscore = _StudentScore.GetScoreGeneral(x.StudentGroupId, x.StudentUserId).FirstOrDefault(),
                        CourseName=x.CourseName
                    }).ToList();

                    

                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        report = report.Where(x => x.CheckDate >= request.startDate.Value && x.CheckDate <= request.endDate.Value).ToList();
                    }

                    result.Data = report;
                    result.Success = true;
                }
                else
                {
                    throw new Exception("Seleccione un cct y un nivel de estudio");
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            //var data= await _StudentScore.GetStudentsScoresByGroup(67, 102);
           
            return report;
            

        }


        public  List<VReportProfileTeacherDTO>  ProfileTeacher(ReportRequest request)
        {

            var result = new Response();
            List<VReportProfileTeacherDTO> report = new List<VReportProfileTeacherDTO>();
            try
            {

                

                // !!CAMBIAR
                //var View = (_vReportTeacherRepository.GetAll().ToList());

                if (!string.IsNullOrEmpty(request.cct) && (request.levelid > 0))
                {
                    Entities.EducationLevel level = new Entities.EducationLevel();
                    level = _educationLevelRepository.GetMany(x => x.Id == request.levelid).FirstOrDefault();

                    


                    Entities.UserCCT ucct = new Entities.UserCCT();
                    ucct = _userCCTRepository.GetMany(x => x.CCT == request.cct).Select(x => new Entities.UserCCT { Id = x.Id, CCT = x.CCT, UserId = x.UserId }).FirstOrDefault();


                    var ccts = new List<Entities.DTO.UserCCTDTO>();
                   

                    ccts = _userCCTRepository.GetMany(x => x.CCT == request.cct).Select(x => new Entities.DTO.UserCCTDTO { Id = x.Id, CCT = x.CCT, UserId = x.UserId }).ToList();

                    var View = _vReportProfileTeacherReposytory.GetMany(x => x.Level == level.Description && x.userid == ucct.UserId).ToList();

                    report = View.Select(x => new VReportProfileTeacherDTO
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                        NameSchool = x.NameSchool,
                        Level = x.Level,
                        Subject = x.Subject,
                        Groups=x.Groups,
                        ElapsedDays= x.ElapsedDays,
                        vpActivity= x.vpActivity,
                        Period= x.Period,
                        totalQuestion=x.totalQuestion,
                        totalSchedulesExam=x.totalSchedulesExam,
                        BaseQuestion= x.BaseQuestion,
                        GeneratedQuestion=x.GeneratedQuestion,
                        ActivityScheduled= x.ActivityScheduled,
                        StartDate= x.StartDate,
                        EndDate= x.EndDate,
                        Grade = x.Grade,
                        PAssistance= x.PAssistance,
                        PExam= x.PExam,
                        PActivity=x.PActivity,
                        PHomework=x.PHomework,
                        CCT = ucct.CCT,
                        userid= x.userid


                    }).Where(x=>x.userid == ucct.UserId).ToList();


                    if (request.startDate.HasValue && request.endDate.HasValue)
                    {
                        report = report.Where(x => x.StartDate >= request.startDate.Value).ToList();
                    }

                    result.Data = report;
                    result.Success = true;
                }
                else
                {
                    throw new Exception("Seleccione un cct y un nivel de estudio");
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            //var data= await _StudentScore.GetStudentsScoresByGroup(67, 102);


            return report;

        }


    }
}


