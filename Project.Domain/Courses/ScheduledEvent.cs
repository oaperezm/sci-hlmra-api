﻿using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    /// <summary>
    /// Dominio para administrar programación de eventos
    /// </summary>
    public class ScheduledEvent : IScheduledEvent
    {
        private readonly IScheduledEventRepository _scheduledEventRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IHomeWorkRepository _homeWorkRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly ICourseRepository _courseRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scheduledEventRepository"></param>
        /// <param name="unitOfWork"></param>
        public ScheduledEvent(IScheduledEventRepository scheduledEventRepository,
                              IStudentGroupRepository studentGroupRepository,
                              IStudentRepository studentRepository,
                              IHomeWorkRepository homeWorkRepository,
                              IPushNotificationService srvPushNotification,
                              ICourseRepository courseRepository,
                              IUnitOfWork unitOfWork) {

            _scheduledEventRepository = scheduledEventRepository;
            _studentGroupRepository   = studentGroupRepository;
            _studentRepository        = studentRepository;
            _homeWorkRepository       = homeWorkRepository;
            _unitOfWork               = unitOfWork;
            _srvPushNotification      = srvPushNotification;
            _courseRepository         = courseRepository;

        }

        /// <summary>
        /// Agregar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ScheduledEventSaveRequest request) {

            var result = new Response();
            var groups = default(List<Entities.StudentGroup>);

            try
            {
                var scheduleEvent = new Entities.ScheduledEvent
                {
                    Name         = request.Name,
                    Description  = request.Description,
                    DateEvent    = request.DateEvent,
                    Duration     = request.Duration,
                    InitTime     = request.InitTime,
                    Active       = request.Active,
                    RegisterDate = DateTime.Now,
                    UserId       = request.UserId,
                    CourseId     = request.CourseId,
                    EndEvent     = request.EndEvent
                };

                if (request.Groups.Count > 0) {
                    groups = (await _studentGroupRepository.FindAllAsync(x => request.Groups.Contains(x.Id))).ToList();
                    groups.ToList().ForEach(g => scheduleEvent.Groups.Add(g));
                }

                _scheduledEventRepository.Add(scheduleEvent);
                await _unitOfWork.SaveChangesAsync();

                
                #region SEND PUSH NOTIFICATION
                                
                var course = await _courseRepository.FindAsync(x => x.Id == scheduleEvent.CourseId);
                var data = new
                {
                    courseName = course.Description,
                    eventName  = request.Description,
                    dateEvent  = request.DateEvent.ToShortDateString()
                };
                var userIds = course.StudentGroups.SelectMany(x=> x.Students)
                                                  .Select(x => x.UserId)
                                                  .ToList();

                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewScheduleEvent, data);

                #endregion

                result.Data    = scheduleEvent.Id;
                result.Success = true;
                result.Message = "Evento agregado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar el evento: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Actualizar los datos de un evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(ScheduledEventUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var scheduleEvent = await _scheduledEventRepository.FindAsync(x => x.Id == request.Id);

                scheduleEvent.Name        = request.Name;
                scheduleEvent.Description = request.Description;
                scheduleEvent.DateEvent   = request.DateEvent;
                scheduleEvent.InitTime    = request.InitTime;
                scheduleEvent.Duration    = request.Duration;
                scheduleEvent.Active      = request.Active;
                scheduleEvent.EndEvent    = request.EndEvent;

                var groups = await _studentGroupRepository.FindAllAsync(x => request.Groups.Contains(x.Id));
                scheduleEvent.Groups.Clear();                                
                groups.ToList().ForEach(g => scheduleEvent.Groups.Add(g));
                

                _scheduledEventRepository.Update(scheduleEvent);
                await _unitOfWork.SaveChangesAsync();
                                
                result.Success = true;
                result.Message = "Evento actualizados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al editar el evento: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar un evento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var scheduleEvent = await _scheduledEventRepository.FindAsync(x => x.Id == id);                
                scheduleEvent.Groups.Clear();
                
                _scheduledEventRepository.Delete(scheduleEvent);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Evento eliminado correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el evento: { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Obtener eventos por curso
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Response> GetByCourse(int courseId) {

            var result = new Response();

            try
            {
                var courses        = (await _scheduledEventRepository.FindAllAsync(x => x.CourseId == courseId))
                                     .Select(e => new Entities.ScheduledEvent
                                            {
                                                  Id           = e.Id,
                                                  Name         = e.Name,
                                                  Description  = e.Description,
                                                  DateEvent    = e.DateEvent,
                                                  EndEvent     = e.EndEvent,
                                                  Duration     = e.Duration,
                                                  InitTime     = e.InitTime,
                                                  Active       = e.Active,
                                                  RegisterDate = e.RegisterDate,
                                                  CourseId     = e.CourseId,
                                                  Groups       = e.Groups?.Select(
                                                      g=> new Entities.StudentGroup {
                                                          Id          = g.Id,
                                                          Description = g.Description,
                                                          Status      = g.Status
                                                      }).ToList()
                                            });

                result.Data    = courses;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los eventos por curso: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Obtener los eventos de un alumno por mes
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int userId, DateTime month) {

            var result = new Response();

            try
            {
                var groups    = await _studentRepository.FindAllAsync(x => x.UserId == userId);
                var data      = new List<Entities.DTO.EventDTO>();

                var courseIds = groups.Select(x => x.StudentGroup.CourseId).ToList();
                var groupIds  = groups.Select(g => g.StudentGroupId).ToList();
                
                var homeWorks = await _homeWorkRepository.FindAllAsync(h => groupIds.Contains(h.StudentGroupId));
                var events    = await _scheduledEventRepository.FindAllAsync(x => courseIds.Contains(x.CourseId)
                                                                         && (x.DateEvent.Month == month.Month && x.DateEvent.Year == month.Year));
                
                // OBTENER EVENTOS DEL USUARIO
                var eventsDTO = events.Select( e => new Entities.DTO.EventDTO {
                                             DateEvent    = e.DateEvent,
                                             Description  = e.Description,
                                             Duration     = e.Duration,
                                             EndEvent     = e.EndEvent,
                                             Id           = e.Id,
                                             InitTime     = e.InitTime,
                                             Name         = e.Name,  
                                             EventType    = (int)EnumEventType.Event,
                                             Teacher      = new {
                                                 e.User.FullName,
                                                 e.User.Email
                                             },                                              
                                             Course = new {
                                                 e.Course.Id,
                                                 e.Course.Name,
                                                 e.Course.Description,
                                                 e.Course.StartDate,
                                                 e.Course.EndDate
                                             }
                                         }).ToList();

                // OBTENER EVENTOS DE PROGRAMACIÓN DE EXAMENES
                var examsDTO = groups.SelectMany(x => x.StudentGroup.ExamSchedules)
                                       .Where( x=> x.BeginApplicationDate.Month == month.Month && x.BeginApplicationDate.Year == month.Year)
                                       .Distinct()
                                       .Select(x => new Entities.DTO.EventDTO
                                     {
                                         Id         = x.Id,
                                         Name       = x.Description,
                                         DateEvent  = x.BeginApplicationDate,
                                         EndEvent   = x.BeginApplicationDate,
                                         InitTime   = string.Empty,
                                         EventType  = (int)EnumEventType.Exam,
                                         Description = string.Empty,
                                         Group = new {
                                             x.Group.Id,
                                             x.Group.Description                                             
                                         },
                                         Teacher = new
                                         {
                                             x.User.FullName,
                                             x.User.Email
                                         },
                                         Course = new
                                         {
                                             x.Group.Course.Id,
                                             x.Group.Course.Name,
                                             x.Group.Course.Description,
                                             x.Group.Course.StartDate,
                                             x.Group.Course.EndDate
                                         }
                                     }).ToList();

                // OBTENER EVENTOS DE TAREAS
                var homeworkDTO = homeWorks.Select(h => new Entities.DTO.EventDTO
                {
                    Id          = h.Id,
                    Name        = h.Name,
                    DateEvent   = h.ScheduledDate,
                    EndEvent    = h.ScheduledDate,
                    InitTime    = string.Empty,
                    EventType = (int)EnumEventType.HomeWork,
                    Description = h.Description,                        
                    Group = new {
                        h.StudentGroup.Id,
                        h.StudentGroup.Description
                    },
                    Course = new {
                        h.StudentGroup.Course.Id,
                        h.StudentGroup.Course.Name,
                        h.StudentGroup.Course.Description,
                        h.StudentGroup.Course.StartDate,
                        h.StudentGroup.Course.EndDate
                    },
                    Teacher = new {
                        h.User.FullName,
                        h.User.Email
                    }, Files = h.Files == null ? new List<Entities.TeacherResources>() : h.Files?.Select( f => new Entities.TeacherResources {
                        Name = f.Name,
                        Url = f.Url                        
                    })
                }).ToList();
                
                if (eventsDTO != null)
                    data = data.Concat(eventsDTO).ToList();

                if (examsDTO != null)
                    data = data.Concat(examsDTO).ToList();

                if (homeworkDTO != null)
                    data = data.Concat(homeworkDTO).ToList();

                result.Data    = data;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas la obtener los eventos: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener los eventos de un maeestro por fecha
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<Response> GetByTeacher(int userId, DateTime month) {

            var result = new Response();

            try {

                var data      = new List<Entities.DTO.EventDTO>();
                var groups    = await _studentRepository.FindAllAsync(x => x.StudentGroup.Course.UserId == userId);
                var events    = await _scheduledEventRepository.FindAllAsync(x => x.UserId == userId && (x.DateEvent.Month == month.Month && x.DateEvent.Year == month.Year));
                var homeWorks = await _homeWorkRepository.FindAllAsync(h => h.UserId == userId && (h.ScheduledDate.Month == month.Month && h.ScheduledDate.Year == month.Year)) ;

                // OBTENER EVENTOS DEL USUARIO
                var eventsDTO = events.Select(e => new Entities.DTO.EventDTO
                {
                    DateEvent = e.DateEvent,
                    Description = e.Description,
                    Duration = e.Duration,
                    EndEvent = e.EndEvent,
                    Id = e.Id,
                    InitTime = e.InitTime,
                    Name = e.Name,
                    EventType = (int)EnumEventType.Event,
                    Teacher = new
                    {
                        e.User.FullName,
                        e.User.Email
                    },
                    Course = new
                    {
                        e.Course.Id,
                        e.Course.Name,
                        e.Course.Description,
                        e.Course.StartDate,
                        e.Course.EndDate
                    }
                }).ToList();

                // OBTENER EVENTOS DE PROGRAMACIÓN DE EXAMENES
                var examsDTO = groups.SelectMany(x => x.StudentGroup.ExamSchedules)
                                       .Where(x => x.BeginApplicationDate.Month == month.Month && x.BeginApplicationDate.Year == month.Year)
                                       .Distinct()
                                       .Select(x => new Entities.DTO.EventDTO
                                       {
                                           Id = x.Id,
                                           Name = x.Description,
                                           DateEvent = x.BeginApplicationDate,
                                           EndEvent = x.EndApplicationDate,
                                           InitTime = string.Empty,
                                           EventType = (int)EnumEventType.Exam,
                                           Description = string.Empty,
                                           Group = new
                                           {
                                               x.Group.Id,
                                               x.Group.Description
                                           },
                                           Teacher = new
                                           {
                                               x.User.FullName,
                                               x.User.Email
                                           },
                                           Course = new
                                           {
                                               x.Group.Course.Id,
                                               x.Group.Course.Name,
                                               x.Group.Course.Description,
                                               x.Group.Course.StartDate,
                                               x.Group.Course.EndDate
                                           }
                                       }).ToList();

                // OBTENER EVENTOS DE TAREAS
                var homeworkDTO = homeWorks.Select(h => new Entities.DTO.EventDTO
                {
                    Id = h.Id,
                    Name = h.Name,
                    DateEvent = h.ScheduledDate,
                    EndEvent = h.ScheduledDate,
                    InitTime = string.Empty,
                    EventType = (int)EnumEventType.HomeWork,
                    Description = h.Description,
                    Group = new
                    {
                        h.StudentGroup.Id,
                        h.StudentGroup.Description
                    },
                    Course = new
                    {
                        h.StudentGroup.Course.Id,
                        h.StudentGroup.Course.Name,
                        h.StudentGroup.Course.Description,
                        h.StudentGroup.Course.StartDate,
                        h.StudentGroup.Course.EndDate
                    },
                    Teacher = new
                    {
                        h.User.FullName,
                        h.User.Email
                    },Files = h.Files == null ? new List<Entities.TeacherResources>() : h.Files?.Select(f => new Entities.TeacherResources {                    
                        Name = f.Name,
                        Url = f.Url
                    })
                }).ToList();



                if (eventsDTO != null)
                    data = data.Concat(eventsDTO).ToList();

                if (examsDTO != null)
                    data = data.Concat(examsDTO).ToList();

                if (homeworkDTO != null)
                    data = data.Concat(homeworkDTO).ToList();

                result.Data = data;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas la obtener los eventos: { ex.Message}";
            }

            return result;

        }
    }
}
