﻿using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class HomeWork : IHomeWork
    {
        private readonly IHomeWorkRepository _homeWorkRepository;
        private readonly IHomeWorkAnswerRepository _homeWorkAnswerRepository;
        private readonly IHomeWorkAnswerFileRepository _homeWorkAnswerFileRepository;
        private readonly ITeacherResourceRepository _teacherResourceRepository;
        private readonly IFileUploader _fileUploader;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly ICoursePlanningRepository _CoursePlanningRepository;
        private readonly ISchedulingPartialRepository _SchedulingPartialRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;

        public HomeWork(IHomeWorkRepository homeWorkRepository,
                        ITeacherResourceRepository teacherResourceRepository,
                        IHomeWorkAnswerRepository homeWorkAnswerRepository,
                        IHomeWorkAnswerFileRepository homeWorkAnswerFileRepository,
                        IStudentGroupRepository studentGroupRepository,
                        IPushNotificationService srvPushNotification,
                        IFileUploader fileUploader,
                        IUnitOfWork unitOfWork,
                        ICoursePlanningRepository coursePlanningRepository,
                        ISchedulingPartialRepository SchedulingPartialRepository, 
                        IExamScheduleRepository examScheduleRepository)
        {
            this._homeWorkRepository = homeWorkRepository;
            this._teacherResourceRepository = teacherResourceRepository;
            this._homeWorkAnswerRepository = homeWorkAnswerRepository;
            this._homeWorkAnswerFileRepository = homeWorkAnswerFileRepository;
            this._unitOfWork = unitOfWork;
            this._fileUploader = fileUploader;
            this._srvPushNotification = srvPushNotification;
            this._studentGroupRepository = studentGroupRepository;
            this._CoursePlanningRepository = coursePlanningRepository;
            this._SchedulingPartialRepository = SchedulingPartialRepository;
            this._examScheduleRepository = examScheduleRepository;
        }


        #region HomeWork

        /// <summary>
        /// Obtener todas las tareas de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByGroup(int id)
        {

            var result = new Response();

            try
            {
                var data = await _homeWorkRepository.FindAllAsync(x => x.StudentGroupId == id);
                var homeWorks = data.Select(h => new Entities.HomeWork
                {
                    Id = h.Id,
                    Description = h.Description,
                    Name = h.Name,
                    ScheduledDate = h.ScheduledDate,
                    StudentGroupId = h.StudentGroupId,
                    Qualified = h.Qualified,
                    OpeningDate = h.OpeningDate,
                    TypeOfQualification = h.TypeOfQualification,
                    AssignedQualification = h.AssignedQualification,
                    SchedulingPartialId = h.SchedulingPartialId,
                    TypeScore=h.TypeScore,
                    Files = h.Files.Select(f => new Entities.TeacherResources
                    {
                        Id = f.Id,
                        Description = f.Description,
                        Name = f.Name,
                        Url = f.Url,
                        ResourceType = f.ResourceType == null ? new Entities.ResourceType() : new Entities.ResourceType
                        {
                            Id = f.ResourceType.Id,
                            Description = f.ResourceType.Description
                        }
                    }).ToList()
                });

                result.Data = homeWorks;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una tarea: { ex.Message}";
            }

            return result;
        }



        /// <summary>
        /// Obtener todas las tareas de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllWithScore(int studentGroupId, int userId)
        {

            var result = new Response();
            int scheduledPartial;

            try
            {
                var answersHomework = await _homeWorkAnswerRepository.FindAllAsync(x => x.HomeWork.StudentGroupId == studentGroupId && x.UserId == userId);
                var data = await _homeWorkRepository.FindAllAsync(x => x.StudentGroupId == studentGroupId);

                var homeWorks = data.Select(h => new HomeWorkDTO
                {
                    TotalAnswers = h.Answers.Count,
                    Id = h.Id,
                    Description = h.Description,
                    Name = h.Name,
                    ScheduledDate = h.ScheduledDate,
                    StudentGroupId = h.StudentGroupId,
                    Score = "",
                    Comment = "",
                    OpeningDate = h.OpeningDate,
                    TypeOfQualification = h.TypeOfQualification,
                    AssignedQualification = h.AssignedQualification,
                    SchedulingPartialId = h.SchedulingPartialId,
                    TypeScore=h.TypeScore,
                    Files = h.Files.Select(f => new Entities.TeacherResources
                    {
                        Id = f.Id,
                        Description = f.Description,
                        Name = f.Name,
                        Url = f.Url,
                        ResourceType = f.ResourceType == null ? new Entities.ResourceType() : new Entities.ResourceType
                        {
                            Id = f.ResourceType.Id,
                            Description = f.ResourceType.Description
                        }
                    

                    }).ToList()
                }).ToList();


                //if (answersHomework.Count > 0)
                //{
                    homeWorks.ForEach(hw =>
                    {
                        var dataACt = _SchedulingPartialRepository.GetAll().Where(xx => xx.Id == hw.SchedulingPartialId);
                        dataACt.ToList().ForEach(xy =>
                        {
                            hw.SchedulingPartialDescription = xy.Description;
                        });

                        var a = answersHomework.FirstOrDefault(x => x.HomeWorkId == hw.Id);
                        if (a != null)
                        {

                            hw.Score = a.Score;
                            hw.Comment = a.Comment;
                            hw.Qualified = a.Score != "" ? true : false;
                            hw.delivered = a.Delivered;
                            hw.addNewAnswers = a.AddNewAnswers;
                            if (a.HomeWorkAnswerFiles.Count > 0) {
                                hw.FileFound = true;
                            }
                            else
                            {
                                hw.FileFound = false;
                            }
                          
                        }
                    });
                //}


                result.Data = homeWorks;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una tarea: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Agregar una nueva tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(HomeWorkSaveRequest request)
        {

            var result = new Response();

            try
            {
                var homeWork = new Entities.HomeWork
                {
                    Description = request.Description,
                    Name = request.Name,
                    RegisterDate = DateTime.Now,
                    ScheduledDate = request.ScheduledDate,
                    StudentGroupId = request.StudentGroupId,
                    UserId = request.UserId,
                    Qualified = false,
                    OpeningDate = request.OpeningDate,
                    TypeOfQualification = request.TypeOfQualification,
                    AssignedQualification = request.AssignedQualification,
                    SchedulingPartialId = request.SchedulingPartialId,
                    TypeScore = request.TypeScore != null ? request.TypeScore.Value : false

                };

                if (request.FilesId != null && request.FilesId.Count > 0)
                {
                    var files = await _teacherResourceRepository.FindAllAsync(x => request.FilesId.Contains(x.Id));
                    files.ToList().ForEach(f => homeWork.Files.Add(f));
                }

                _homeWorkRepository.Add(homeWork);
                await _unitOfWork.SaveChangesAsync();

                #region PUSHNOTIFICATION

                var group = await _studentGroupRepository.FindAsync(x => x.Id == homeWork.StudentGroupId);
                var data = new
                {
                    groupName = group.Description,
                    homeWork = request.Description,
                    date = request.ScheduledDate.ToShortDateString()
                };
                var userIds = group.Students.Select(x => x.UserId).ToList();
                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewScheduleHomeWork, data);


                #endregion

                result.Data = homeWork.Id;
                result.Success = true;
                result.Message = "Tarea agregada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una tarea: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de una tarea
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Remove(int id)
        {

            var result = new Response();

            try
            {
                var homeWork = await _homeWorkRepository.FindAsync(x => x.Id == id);
                homeWork.Files.Clear();

                _homeWorkRepository.Delete(homeWork);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar los datos; { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Actualizar los datos de una tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(HomeWorkUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var homeWork = await _homeWorkRepository.FindAsync(x => x.Id == request.Id);

                homeWork.Description = request.Description;
                homeWork.Name = request.Name;
                homeWork.ScheduledDate = request.ScheduledDate;
                homeWork.OpeningDate = request.OpeningDate;
                homeWork.TypeOfQualification = request.TypeOfQualification;
                homeWork.AssignedQualification = request.AssignedQualification;
                homeWork.SchedulingPartialId = request.SchedulingPartialId;
                homeWork.TypeScore = request.TypeScore != null ? request.TypeScore.Value : false;

                if (request.FilesId != null && request.FilesId.Count > 0)
                {
                    homeWork.Files.Clear();

                    var files = await _teacherResourceRepository.FindAllAsync(x => request.FilesId.Contains(x.Id));
                    files.ToList().ForEach(f => homeWork.Files.Add(f));
                }

                _homeWorkRepository.Update(homeWork);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tarea actualizada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas actualizar los datos : { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar archivo de una tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFile(HomeWorkRemoveFileRequest request)
        {

            var result = new Response();

            try
            {
                var homeWork = await _homeWorkRepository.FindAsync(x => x.Id == request.Id);
                var file = await _teacherResourceRepository.FindAsync(x => x.Id == request.TeacherResourceId);

                homeWork.Files.Remove(file);

                _homeWorkRepository.Update(homeWork);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Archivo agregado correctamente";
                result.Data = new Entities.TeacherResources
                {
                    Id = file.Id,
                    Description = file.Description,
                    Name = file.Name,
                    Url = file.Url
                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar archivo : { ex.Message}";
            }

            return result;


        }

        /// <summary>
        /// Obtener todos las respuesta de los alumnos registrados
        /// </summary>
        /// <param name="studenGroupId"></param>
        /// <param name="homeworkId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsAnswers(int studenGroupId, int homeworkId)
        {

            var result = new Response();

            try
            {
                var group = await _studentGroupRepository.FindAsync(x => x.Id == studenGroupId);
                var homeWork = await _homeWorkRepository.FindAsync(x => x.Id == homeworkId);
                var homeWorkAnswers = homeWork.Answers.ToList();
                var students = group.Students.ToList();
                var answers = new List<HomeWorkScoreDTO>();

                students.ForEach(s =>
                {
                    var studentHomework = new HomeWorkScoreDTO();

                    studentHomework.HomeWorkId = homeworkId;
                    studentHomework.UserId = s.UserId;
                    studentHomework.FullName = s.User.FullName;

                    var homeWorkAnswer = homeWorkAnswers.FirstOrDefault(x => x.UserId == s.UserId);

                    if (homeWorkAnswer != null)
                    {
                        studentHomework.Id = homeWorkAnswer.Id;
                        studentHomework.HasHomwRowk = true;
                        studentHomework.Score = homeWorkAnswer.Score;
                        studentHomework.Comment = homeWorkAnswer.Comment;
                        studentHomework.Delivered = homeWorkAnswer.Delivered;
                        studentHomework.AddNewAnswers = homeWorkAnswer.AddNewAnswers;

                        studentHomework.Files = homeWorkAnswer.HomeWorkAnswerFiles.Select(f => new Entities.HomeWorkAnswerFile
                        {
                            Id = f.Id,
                            Comment = f.Comment,
                            HomeWorkAnswerId = f.HomeWorkAnswerId,
                            Url = f.Url
                        }).ToList();
                    }

                    answers.Add(studentHomework);
                });

                result.Data = new
                {
                    homeWork = new Entities.HomeWork
                    {
                        Id = homeWork.Id,
                        Name = homeWork.Name,
                        Qualified = homeWork.Qualified,
                        Description = homeWork.Description,
                        TypeScore= homeWork.TypeScore
                    },
                    students = answers.OrderBy(a => a.FullName).ToList()
                };
                result.Message = "Datos obtenidos correcamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los alumnos y sus tareas: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Actualizar el estatus de la tarea programada
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int homeworkId, bool status)
        {

            var result = new Response();

            try
            {
                var homeWork = await _homeWorkRepository.FindAsync(x => x.Id == homeworkId);

                homeWork.Qualified = status;
                _homeWorkRepository.Update(homeWork);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tarea cerrada correctamente";

            }
            catch (Exception ex)
            {
                result.Message = $"Problemas al actualizar el estauts: { ex.Message }";
                result.Success = false;
            }

            return result;
        }

        #endregion

        #region ANSWER

        /// <summary>
        /// Agregar una nueva respuesta del alumno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddAnswer(HomeWorkAnswerSaveRequest request)
        {

            var result = new Response();
            string fileUrl = string.Empty;
            string container = "homework";

            try
            {
                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);

                var answerFile = new Entities.HomeWorkAnswerFile
                {
                    RegisterDate = DateTime.UtcNow,
                    Url = fileUrl,
                    Comment = request.Comment
                };

                var homeWorkAnswer = await _homeWorkAnswerRepository.FindAsync(x => x.UserId == request.UserId && x.HomeWorkId == request.HomeWorkId);

                if (homeWorkAnswer == null)
                {

                    homeWorkAnswer = new Entities.HomeWorkAnswer
                    {
                        HomeWorkId = request.HomeWorkId,
                        UserId = request.UserId,
                        RegisterDate = DateTime.UtcNow,
                        Score = string.Empty,
                        Delivered = request.Delivered,
                        AddNewAnswers = request.AddNewAnswers,
                        PartialEvaluation = request.PartialEvaluation,
                        StatusHomework = request.StatusHomework
                    };

                    homeWorkAnswer.HomeWorkAnswerFiles.Add(answerFile);
                    _homeWorkAnswerRepository.Add(homeWorkAnswer);

                }
                else
                {

                    homeWorkAnswer.HomeWorkAnswerFiles.Add(answerFile);
                    _homeWorkAnswerRepository.Update(homeWorkAnswer);
                }

                await _unitOfWork.SaveChangesAsync();

                result.Data = new { Id = answerFile.Id, Url = fileUrl };
                result.Success = true;
                result.Message = "Tarea agregada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al registrar la tarea: {ex.Message}";

                if (string.IsNullOrEmpty(fileUrl))
                {
                    await _fileUploader.FileRemove(fileUrl, container);
                }
            }

            return result;

        }


        /// <summary>
        /// Guardar las calificaciones
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveScore(HomeWorkScoreRequest request)
        {

            var result = new Response();

            try
            {
                var homeWork = await _homeWorkAnswerRepository.FindAllAsync(x => x.HomeWorkId == request.HomeWorkId);

                request.Scores.ForEach(s =>
                {
                    var homeworkAnswer = homeWork.FirstOrDefault(h => h.Id == s.HomeWorkAnswerId);

                    if (homeworkAnswer != null)
                    {
                        homeworkAnswer.Score = s.Score;
                        homeworkAnswer.Comment = s.Comment;
                        homeworkAnswer.Delivered = s.Delivered;
                        homeworkAnswer.AddNewAnswers = s.AddNewAnswers;
                        _homeWorkAnswerRepository.Update(homeworkAnswer);
                    }
                    else
                    {

                        if (s.Score != null)
                        {
                            var answer = new Entities.HomeWorkAnswer
                            {
                                UserId = s.UserId,
                                Comment = s.Comment,
                                Score = s.Score,
                                RegisterDate = DateTime.Now,
                                HomeWorkId = request.HomeWorkId,
                                Delivered = s.Delivered,
                                AddNewAnswers = s.AddNewAnswers,
                                PartialEvaluation = s.SchedulingPartialId,
                                StatusHomework = s.StatusHomework
                            };

                            _homeWorkAnswerRepository.Add(answer);
                        }
                    }

                });

                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Tarea calificada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al calificar la tarea: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las respuestas del alumno por tarea
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByHomeWork(int homeWorkId, int userId)
        {

            var result = new Response();

            try
            {
                var data = await _homeWorkAnswerRepository.FindAllAsync(x => x.HomeWorkId == homeWorkId && x.UserId == userId);
                var files = data.SelectMany(x => x.HomeWorkAnswerFiles)
                                 .Select(f => new Entities.HomeWorkAnswerFile
                                 {
                                     Comment = f.Comment,
                                     Id = f.Id,
                                     HomeWorkAnswerId = f.HomeWorkAnswerId,
                                     Url = f.Url,
                                     RegisterDate= f.RegisterDate
                                 }).ToList();

                result.Data = files;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al obtener los archivos { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar una respuesta de tarea de un alumno
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAnswer(int homeWorkId, int userId, int answerId)
        {

            var result = new Response();

            try
            {
                var answer = await _homeWorkAnswerRepository.FindAsync(x => x.UserId == userId && x.HomeWorkId == homeWorkId);

                if (answer == null)
                {
                    throw new Exception("No se encontraron los datos de la tarea");
                }

                var file = await _homeWorkAnswerFileRepository.FindAsync(x => x.Id == answerId);

                if (file == null)
                {
                    throw new Exception("No se encontro el archivo");
                }

                await _fileUploader.FileRemove(file.Url, "homework");

                _homeWorkAnswerFileRepository.Delete(file);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Archivo eliminado correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al eliminar el archivo: {ex.Message}";
            }

            return result;
        }

        #endregion
    }
}
