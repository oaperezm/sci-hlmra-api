﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
   public class StatesVideoConference : IStatesVideoConference
    {
        private readonly IStatesVideoConferenceRepository _statesVideoConferece;       
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Método para agregar un nuevo statesVideoConferece
        /// </summary>
        /// <param name="statesVideoConferece"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public StatesVideoConference(IStatesVideoConferenceRepository statesVideoConferece, IUnitOfWork unitOfWork)
        {
            _statesVideoConferece = statesVideoConferece;
            _unitOfWork = unitOfWork;
        }

        public  async Task<Response> Add(StatesVideoConferenceSaveRequest request)
        {
            var result = new Response();

            try
            {
         

                Entities.StatesVideoConference statesVideoConference = new Entities.StatesVideoConference();

                statesVideoConference.Description = request.Description;
                _statesVideoConferece.Add(statesVideoConference);

                await this._unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos guardados correctamente";

                result.Data = statesVideoConference;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetAll()
        {
            
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _statesVideoConferece.GetAll().
                   Select(u => new {
                       Id = u.Id,
                       Description = u.Description,
                   }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

           
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _statesVideoConferece.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
    }
}
