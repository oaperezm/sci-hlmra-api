﻿using Project.Entities;
using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class ExamSchedule : IExamSchedule
    {
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly IExamScoreRepository _scoreRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;

        public ExamSchedule(IExamScheduleRepository examScheduleRepository,
                            IExamScoreRepository scoreRepository,
                            IStudentGroupRepository studentGroupRepository,
                            IPushNotificationService srvPushNotification,
                            ISchedulingPartialRepository schedulingPartialRepository,
                            IUnitOfWork unitOfWork)
        {
            this._examScheduleRepository = examScheduleRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
            this._scoreRepository = scoreRepository;
            this._srvPushNotification = srvPushNotification;
            this._studentGroupRepository = studentGroupRepository;
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Obtener el listado de cursos por profesor
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId)
        {

            var response = new Response();

            try
            {
                var data = (await _examScheduleRepository.FindAllAsync(x => x.UserId == userId))
                          .Select(x => new Entities.ExamSchedule
                          {
                              Id = x.Id,
                              SchedulingPartialId = x.SchedulingPartialId,
                              Description = x.Description,
                              ApplicationDate = x.BeginApplicationDate,
                              BeginApplicationDate = x.BeginApplicationDate,
                              EndApplicationDate = x.EndApplicationDate,
                              BeginApplicationTime = x.BeginApplicationTime,
                              EndApplicationTime = x.EndApplicationTime,
                              MinutesExam = x.MinutesExam,
                              Active = x.Active,
                              RegisterDate = x.RegisterDate,
                              ExamScheduleType = new ExamScheduleType
                              {
                                  Id = x.ExamScheduleType.Id,
                                  Description = x.ExamScheduleType.Description
                              },
                              Group = new Entities.StudentGroup
                              {
                                  Id = x.Group.Id,
                                  Description = x.Group.Description,
                              },
                              TeacherExam = x.TeacherExam == null ? new Entities.TeacherExam { Weighting = new Weighting() } : new Entities.TeacherExam
                              {
                                  Weighting = x.TeacherExam.Weighting == new Weighting() ? new Weighting() : new Weighting
                                  {
                                      Id = x.TeacherExam.WeightingId,
                                      Description = x.TeacherExam.Weighting.Description,
                                      Unit = x.TeacherExam.Weighting.Unit,
                                  }
                              }
                          });

                response.Data = data;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        /// <summary>
        /// Obtener programación de examenes por grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupId(int studentGroupId)
        {
            var response = new Response();

            try
            {
                var data = (await _examScheduleRepository.FindAllAsync(x => x.StudentGroupId == studentGroupId))
                          .Select(x => new Entities.ExamSchedule
                          {
                              Id = x.Id,
                              SchedulingPartialId = x.SchedulingPartialId,
                              Description = x.Description,
                              ApplicationDate = x.BeginApplicationDate,
                              BeginApplicationDate = x.BeginApplicationDate,
                              EndApplicationDate = x.EndApplicationDate,
                              BeginApplicationTime = x.BeginApplicationTime,
                              EndApplicationTime = x.EndApplicationTime,
                              MinutesExam = x.MinutesExam,
                              Active = x.Active,
                              RegisterDate = x.RegisterDate,
                              UserId = x.UserId,
                              StudentGroupId = x.StudentGroupId,
                              TeacherExamId = x.TeacherExamId,
                              TeacherExam = x.TeacherExam == null ? new Entities.TeacherExam { Weighting = new Weighting() } : new Entities.TeacherExam
                              {
                                  Id = x.TeacherExam.Id,
                                  TeacherExamTypeId = x.TeacherExam.TeacherExamTypeId,
                                  Description = x.TeacherExam.Description,
                                  TeacherExamTypes = new Entities.TeacherExamType
                                  {
                                      Id = x.TeacherExam.TeacherExamTypes.Id,
                                      Description = x.TeacherExam.TeacherExamTypes.Description
                                  },
                                  Weighting = x.TeacherExam.Weighting == null ? new Weighting() : new Weighting
                                  {
                                      Id = x.TeacherExam.WeightingId,
                                      Description = x.TeacherExam.Weighting.Description,
                                      Unit = x.TeacherExam.Weighting.Unit,
                                  }
                              },
                              ExamScheduleType = new ExamScheduleType
                              {
                                  Id = x.ExamScheduleType.Id,
                                  Description = x.ExamScheduleType.Description
                              },
                              Group = new Entities.StudentGroup
                              {
                                  Id = x.Group.Id,
                                  Description = x.Group.Description,
                              },
                              SchedulingPartial = new Entities.SchedulingPartial
                              {
                                  Id = x.SchedulingPartialId ?? 0,
                                  Description = x.SchedulingPartial != null ? x.SchedulingPartial.Description : "",
                              },

                          });


                if (data == null)
                    throw new Exception("No se encontraron programaciones de examenes para el grupo");

                response.Data = data.ToList();
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupIdAndUser(int studentGroupId, int userId)
        {
            var response = new Response();

            try
            {
                var data = await _examScheduleRepository.FindAllAsync(x => x.StudentGroupId == studentGroupId);
                List<ExamScheduleDTO> examSchedules = new List<ExamScheduleDTO>();
                foreach (var x in data)
                {
                    var test = x.Tests.FirstOrDefault(t => t.UserId == userId && t.ExamScheduleId == x.Id);
                    var dto = new ExamScheduleDTO
                    {
                        Id = x.Id,
                        SchedulingPartialId = x.SchedulingPartialId,
                        SchedulingPartialDescription = x.SchedulingPartial != null ? x.SchedulingPartial.Description : "",
                        Description = x.Description,
                        ApplicationDate = x.BeginApplicationDate,
                        BeginApplicationDate = x.BeginApplicationDate,
                        EndApplicationDate = x.EndApplicationDate,
                        BeginApplicationTime = x.BeginApplicationTime,
                        EndApplicationTime = x.EndApplicationTime,
                        MinutesExam = x.MinutesExam,
                        Active = x.Active,
                        RegisterDate = x.RegisterDate,
                        UserId = x.UserId,
                        StudentGroupId = x.StudentGroupId,
                        TeacherExamId = x.TeacherExamId,
                        TeacherExam = x.TeacherExam == null ? new Entities.TeacherExam() : new Entities.TeacherExam
                        {
                            Id = x.TeacherExam.Id,
                            TeacherExamTypeId = x.TeacherExam.TeacherExamTypeId,
                            Description = x.TeacherExam.Description,
                            TeacherExamTypes = new Entities.TeacherExamType
                            {
                                Id = x.TeacherExam.TeacherExamTypes.Id,
                                Description = x.TeacherExam.TeacherExamTypes.Description
                            }
                        },
                        ExamScheduleType = new ExamScheduleType
                        {
                            Id = x.ExamScheduleType.Id,
                            Description = x.ExamScheduleType.Description
                        },
                        Group = new Entities.StudentGroup
                        {
                            Id = x.Group.Id,
                            Description = x.Group.Description,
                        },
                        IsAnswered = test != null && test.IsCompleted ? true : false,
                        Score = test != null && test.IsCompleted ? new Entities.ExamScore
                        {
                            RegisterDate = test.RegisterDate,
                            CorrectAnswers = test.CorrectAnswers ?? 0,
                            Score = test.Score.ToString(),
                            Comments = test.Comments
                        } : null,
                        HasScore = test != null && test.IsCompleted,
                    };
                    examSchedules.Add(dto);
                }

                if (examSchedules == null)
                    throw new Exception("No se encontraron programaciones de examenes para el grupo");

                // Obtener las calificaciones del alumno de la programación de examenes 
                var ids = examSchedules.Select(x => x.Id).ToList();
                var scores = await _scoreRepository.FindAllAsync(s => s.UserId == userId && ids.Contains(s.ExamScheduleId));

                // Validar la calificación de examen por cada una de las programaciones existentes 
                examSchedules.ForEach(e =>
                {
                    var score = scores.FirstOrDefault(s => s.UserId == userId && s.ExamScheduleId == e.Id);

                    if (score != null)
                    {
                        e.HasScore = true;
                        e.Score = new Entities.ExamScore
                        {
                            RegisterDate = score.RegisterDate,
                            CorrectAnswers = score.CorrectAnswers,
                            Score = score.Score,
                            Comments = score.Comments
                        };
                    }
                    else
                    {

                    }
                });

                response.Data = examSchedules;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        /// <summary>
        /// Activar el estatus de la programación de exámenes
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int id, bool active)
        {

            var response = new Response();

            try
            {
                var data = await _examScheduleRepository.FindAsync(x => x.Id == id);

                if (data == null)
                {
                    throw new Exception(" No se econtrarón los datos de la programación del examen");
                }

                data.Active = active;
                data.UpdateDate = DateTime.Now;

                _examScheduleRepository.Update(data);
                await this._unitOfWork.SaveChangesAsync();

                response.Data = id;
                response.Success = true;
                response.Message = "Datos actualizados correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }

        /// <summary>
        /// Actualizar el estatus de la programación  de exámenes
        /// </summary>
        /// <param name="examScheduleId"></param>
        /// <param name="examScheduleTypeId"></param>
        /// <returns></returns>
        public async Task<Response> UpdateExamScheduleType(int examScheduleId, int examScheduleTypeId)
        {

            var response = new Response();

            try
            {
                var data = await _examScheduleRepository.FindAsync(x => x.Id == examScheduleId);

                if (data == null)
                {
                    throw new Exception(" No se econtrarón los datos de la programación del examen");
                }

                data.ExamScheduleTypeId = examScheduleTypeId;
                data.UpdateDate = DateTime.Now;

                _examScheduleRepository.Update(data);
                await this._unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos actualizados correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;

        }

        /// <summary>
        /// Método para agregar una nueva programación de examenes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ExamScheduleSaveRequest request, int userId, int systemId)
        {
            var result = new Response();

            try
            {
                if (systemId == (int)EnumSystems.RA)
                    if (request.SchedulingPartialId.HasValue)
                    {
                        var partial = await _schedulingPartialRepository.FindAsync(p => p.Id == request.SchedulingPartialId.Value);
                        if (partial == null)
                            throw new Exception("No selecciono el parcial al que corresponde");
                        if (partial.CoursePlanning.StudentGroupId != request.StudentGroupId)
                            throw new Exception("El parcial no corresponde al grupo");
                    }
                    else
                        throw new Exception("No selecciono el parcial al que corresponde");

                Entities.ExamSchedule examSchedule = new Entities.ExamSchedule();
                examSchedule.Active = true;
                examSchedule.SchedulingPartialId = request.SchedulingPartialId;
                examSchedule.Description = request.Description;
                examSchedule.BeginApplicationDate = request.BeginApplicationDate.Value;
                examSchedule.EndApplicationDate = request.EndApplicationDate.Value;
                examSchedule.BeginApplicationTime = request.BeginApplicationTime;
                examSchedule.EndApplicationTime = request.EndApplicationTime;
                examSchedule.MinutesExam = request.MinutesExam;
                examSchedule.StudentGroupId = request.StudentGroupId;
                examSchedule.UserId = userId;
                examSchedule.RegisterDate = DateTime.Now;
                examSchedule.ExamScheduleTypeId = (int)EnumExamScheduleType.Programmed;
                examSchedule.TeacherExamId = request.TeacherExamId == 0 ? null : request.TeacherExamId;

                if (examSchedule.TeacherExamId != null)
                    examSchedule.ExamScheduleTypeId = (int)EnumExamScheduleType.Associated;
                else
                    examSchedule.ExamScheduleTypeId = (int)EnumExamScheduleType.Programmed;

                _examScheduleRepository.Add(examSchedule);
                await this._unitOfWork.SaveChangesAsync();

                #region PUSHNOTIFICATION

                var group = await _studentGroupRepository.FindAsync(x => x.Id == examSchedule.StudentGroupId);
                var data = new
                {
                    groupName = group.Description,
                    homeWork = request.Description,
                    date = request.BeginApplicationDate.Value.ToShortDateString(),
                };
                var userIds = group.Students.Select(x => x.UserId).ToList();
                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewScheduleExamn, data);

                #endregion

                result.Data = examSchedule.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de una programación de exámenes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {

            var result = new Response();

            try
            {
                var examSchedule = await _examScheduleRepository.FindAsync(x => x.Id == id);

                if (examSchedule == null)
                {
                    throw new Exception(" No se econtrarón los datos de la programación del examen");
                }

                if (examSchedule.Tests.Count() > 0)
                {
                    throw new Exception("No se puede eliminar la programación de examen porque ya fue aplicado");
                }

                _examScheduleRepository.Delete(examSchedule);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método para actualizar los datos de una programación de examenes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> Update(ExamScheduleUpdateRequest request, int userId, int systemId)
        {
            var result = new Response();

            try
            {
                var examSchedule = await _examScheduleRepository.FindAsync(x => x.Id == request.Id);
                if (examSchedule == null)
                    throw new Exception(" No se econtraron los datos de la programación del examen");
                if (systemId == (int)EnumSystems.RA)
                    if (request.SchedulingPartialId.HasValue)
                    {
                        var partial = await _schedulingPartialRepository.FindAsync(p => p.Id == request.SchedulingPartialId.Value);
                        if (partial == null)
                            throw new Exception("No selecciono el parcial al que corresponde");
                        if (partial.CoursePlanning.StudentGroupId != request.StudentGroupId)
                            throw new Exception("El parcial no corresponde al grupo");
                    }
                    else
                        throw new Exception("No selecciono el parcial al que corresponde");
                examSchedule.SchedulingPartialId = request.SchedulingPartialId;
                examSchedule.Description = request.Description;
                examSchedule.BeginApplicationDate = request.BeginApplicationDate.Value;
                examSchedule.EndApplicationDate = request.EndApplicationDate.Value;
                examSchedule.BeginApplicationTime = request.BeginApplicationTime;
                examSchedule.EndApplicationTime = request.EndApplicationTime;
                examSchedule.MinutesExam = request.MinutesExam;
                examSchedule.UpdateDate = DateTime.Now;
                examSchedule.TeacherExamId = request.TeacherExamId == 0 ? null : request.TeacherExamId;

                if (examSchedule.TeacherExamId != null)
                    examSchedule.ExamScheduleTypeId = (int)EnumExamScheduleType.Associated;
                else
                    examSchedule.ExamScheduleTypeId = (int)EnumExamScheduleType.Programmed;

                _examScheduleRepository.Update(examSchedule);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = examSchedule.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

    }
}
