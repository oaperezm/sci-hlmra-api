﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;

namespace Project.Domain.Courses
{
    public class StudentStatus : IStudentStatus
    {
        private readonly IStudentStatusRepository _StudentStatusRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StudentStatus(IStudentStatusRepository StudentStatusRepository, IUnitOfWork unitOfWor)
        {

            this._StudentStatusRepository = StudentStatusRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddStudentStatus(StudentStatusSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.StudentStatus StudentStatus = new Entities.StudentStatus();

                StudentStatus.Description = request.Description;

                _StudentStatusRepository.Add(StudentStatus);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = StudentStatus;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public Task<Response> DeleteStudentStatus(int StudentStatusId)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _StudentStatusRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _StudentStatusRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateStudentStatus(StudentStatusUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.StudentStatus StudentStatus = new Entities.StudentStatus();

                StudentStatus.Id = request.Id;
                StudentStatus.Description = request.Description;

                _StudentStatusRepository.Update(StudentStatus);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = StudentStatus;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}