﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IQuestion
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddQuestion(QuestionSaveRequest request);
        Task<Response> UpdateQuestion(QuestionUpdateRequest request);
        Task<Response> DeleteQuestion(int questionId);        
    }
}
