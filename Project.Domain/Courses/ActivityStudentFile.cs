﻿using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class ActivityStudentFile: IActivityStudentFile
    { 
        private readonly IActivityStudentFileRepository _activityStudentFileRepository;
        private readonly ITeacherResourceRepository _teacherResourceRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileUploader _fileUploader;

        public ActivityStudentFile(IActivityStudentFileRepository ActivityStudentFileRepository,
                           ITeacherResourceRepository teacherResourceRepository,
                           IUnitOfWork unitOfWork)
        {

            this._activityStudentFileRepository = ActivityStudentFileRepository;
            this._teacherResourceRepository = teacherResourceRepository;
            this._unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Agregar una nueva respuesta del alumno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddFile(ActivityStudentFileSaveRequest request)
        {

            var result = new Response();
            string fileUrl = string.Empty;
            string container = "activitystudentfile";

            try
            {
                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);

                var answerFile = new Entities.ActivityStudentFile
                {
                    RegisterDate = DateTime.UtcNow,
                    Url = fileUrl,
                    Comment = request.Comment
                };

                var activitystudentfile = await _activityStudentFileRepository.FindAsync(x => x.UserId == request.UserId && x.ActivitiesId == request.ActivitiesId); 

                await _unitOfWork.SaveChangesAsync();

                result.Data = new { Id = answerFile.Id, Url = fileUrl };
                result.Success = true;
                result.Message = "Actividades del curso cargada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al registrar la actividad del curso: {ex.Message}";

                if (string.IsNullOrEmpty(fileUrl))
                {
                    await _fileUploader.FileRemove(fileUrl, container);
                }
            }

            return result;

        }


        /// <summary>
        /// Eliminar una respuesta de tarea de un alumno
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteFile(int activityStudentFileId, int userId)
        {

            var result = new Response();

            try
            {
                var answer = await _activityStudentFileRepository.FindAsync(x => x.UserId == userId && x.ActivitiesId == activityStudentFileId);

                if (answer == null)
                {
                    throw new Exception("No se encontraron los datos de la tarea");
                }

                var file = await _activityStudentFileRepository.FindAsync(x => x.Id == activityStudentFileId);

                if (file == null)
                {
                    throw new Exception("No se encontro el archivo");
                }

                await _fileUploader.FileRemove(file.Url, "activitystudentfile");

                _activityStudentFileRepository.Delete(file);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Archivo eliminado correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al eliminar el archivo: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las respuestas del alumno por tarea
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByActivity(int activityStudentFileId, int userId)
        {

            var result = new Response();

            try
            {
                var data = await _activityStudentFileRepository.FindAllAsync(x => x.ActivitiesId == activityStudentFileId && x.UserId == userId);
                var files = data 
                                 .Select(f => new Entities.ActivityStudentFile
                                 {
                                     Comment = f.Comment,
                                     Id = f.Id,
                                     Url = f.Url
                                 }).ToList();

                result.Data = files;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al obtener los archivos { ex.Message}";
            }

            return result;

        }
    }
}
