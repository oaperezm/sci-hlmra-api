﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Project.Domain.Helpers;
using Project.Entities;
using Project.Entities.Converters;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Management;
using Project.Repositories.Services;

namespace Project.Domain.Courses
{
    public class QuestionBank : IQuestionBank
    {
        private readonly IQuestionBankRepository _questionBankRepository;
        private readonly INodeRepository _nodeRepository;
        private readonly IUserRepository _userRepository;
        private readonly IFileUploader _fileUploader;
        private readonly IUnitOfWork _unitOfWork;

        TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

        public QuestionBank(IQuestionBankRepository questionBankRepository,
                            INodeRepository nodeRepository,
                            IFileUploader fileUploader,
                            IUserRepository userRepository,
                            IUnitOfWork unitOfWor)
        {
            _questionBankRepository = questionBankRepository;
            _nodeRepository         = nodeRepository;
            _unitOfWork             = unitOfWor;
            _fileUploader           = fileUploader;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Método para dar de alta un nuevo banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddQuestionBank(QuestionBankSaveRequest request, int userId)
        {
            var result = new Response();

            try
            {               
                Entities.QuestionBank questionBank = new Entities.QuestionBank();

                questionBank.Name           = request.Name;
                questionBank.Description    = request.Description;
                questionBank.Active         = true;                
                questionBank.NodeId         = request.NodeId;
                questionBank.RegisterDate   = DateTime.Now;
                questionBank.UserId         = userId;
                questionBank.SystemId       = request.SystemId;


                _questionBankRepository.Add(questionBank);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = questionBank;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Eliminar un banco de preguntas
        /// </summary>
        /// <param name="QuestionBankId"></param>
        /// <returns></returns>
        public Task<Response> DeleteQuestionBank(int QuestionBankId)
        {
            throw new NotImplementedException();
        }


        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int rolId, int userId, int systemId)
        {
            var result = new ResponsePagination();

            try
            {
                var pageData = default(List<Entities.QuestionBank>);
                var data     = default(List<Entities.QuestionBank>);

                if (rolId == (int)EnumRol.Teacher || rolId == (int)EnumRol.TeacherRA)
                    data = (await _questionBankRepository.FindAllAsync(x => x.UserId == userId && x.User.SystemId == systemId || (x.User.RoleId == (int)EnumRol.AdministratorRA))).ToList();
                else {

                    if (systemId == (int)EnumSystems.SALI)
                    {
                        data = (await _questionBankRepository.FindAllAsync(x => (x.User.RoleId == (int)EnumRol.Administrator)
                                                                           && x.User.SystemId == systemId)).ToList();
                    }
                    else {

                        data = (await _questionBankRepository.FindAllAsync(x => (x.User.RoleId == (int)EnumRol.AdministratorRA)
                                                                         && x.User.SystemId == systemId)).ToList();
                    }

                }
                    


                // FILTRAR LOS DATOS 
                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.ToLower();
                    data = data.Where(x => x.Name.ToLower().Contains(filter) || x.Description.ToLower().Contains(filter)).ToList();
                }

                // INICIALIZAR LOS DATOS PARA LA PAGINACION
                result.Pagination.SetData(data.Count, currentPage, sizePage);

                // OBTENER LOS DATOS DE LA PAGINA SOLICITADA
                pageData = data.OrderBy(x => x.Name)
                               .Skip((currentPage - 1) * result.Pagination.PageSize)
                               .Take(result.Pagination.PageSize)
                               .ToList();

                result.Data    = pageData.Select(u => u.New()).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }
                       

        /// <summary>
        /// Realiza la búsqueda de un banco de preguntas por el identificador principal
        /// </summary>
        /// <param name="id">Identificador unico del banco de prguntas</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id, bool converImageBase64 = false)
        {
            var result = new Response();

            try
            {
                var bank      = await _questionBankRepository.FindAsync(r => r.Id.Equals(id));
                var questions = bank.Questions.Select(q => new Entities.Question
                {
                     Active         = q.Active,
                     Content        = q.Content,
                     Explanation    = q.Explanation,
                     Id             = q.Id,
                     QuestionBankId = q.QuestionBankId,
                     QuestionTypeId = q.QuestionTypeId,
                     UrlImage       = q.UrlImage,
                     ImageHeight    = q.ImageHeight,
                     ImageWidth     = q.ImageWidth,
                     QuestionType   = new Entities.QuestionType { Id = q.QuestionType.Id, Description = q.QuestionType.Description  },
                     Answers        = q.Answers.Select( a => new Entities.Answer {
                                                            Active      = a.Active,
                                                            Description = a.Description,
                                                            IsCorrect   = a.IsCorrect,
                                                            Id          = a.Id,
                                                            QuestionId  = a.QuestionId,
                                                            RelationDescription = a.RelationDescription,
                                                            ImageHeight         = a.ImageHeight,
                                                            ImageWidth          = a.ImageWidth,
                                                         }).ToList()
                }).ToList();

                if (converImageBase64)
                {
                    foreach (var question in questions)
                    {

                        if (!string.IsNullOrEmpty(question.UrlImage))
                        {
                            string urlSimple     = HttpUtility.UrlDecode(question.UrlImage).Split('?').First();
                            string fname         = urlSimple.Split('/').Last();
                            MemoryStream meme    = await _fileUploader.FileDownload(fname, "exams");
                            question.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                        }

                        if (question.QuestionTypeId == (int)EnumQuestionType.ImageMultiple)
                        {
                            foreach (var answers in question.Answers)
                            {
                                string urlSimple    = HttpUtility.UrlDecode(answers.Description).Split('?').First();
                                string fname        = urlSimple.Split('/').Last();
                                MemoryStream meme   = await _fileUploader.FileDownload(fname, "teacher");
                                answers.ImageBase64 = Convert.ToBase64String(meme.ToArray());
                            }
                        }
                    }
                }

                bank.Questions = questions;

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data    = new Entities.QuestionBank {
                                    Active      = bank.Active,
                                    Description = bank.Description,
                                    Id          = bank.Id,
                                    Name        = bank.Name,
                                    NodeId      = bank.NodeId,
                                    Questions   = questions                    
                                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }


        /// <summary>
        /// Obtener todos los bancos de preguntas asígnados a un nodo
        /// </summary>
        /// <param name="nodeId">Identificador del nodo </param>
        /// /// <param name="userId">Identificador del usuario que inicia sesión </param>
        /// <returns></returns>
        public async Task<Response> GetByNodeId(int nodeId, int userId, int systemId)
        {
            var result = new Response();
            var banks = new List<Entities.QuestionBank>();
            try
            {
                if (systemId == (int)EnumSystems.SALI)
                {
                    banks = (await _questionBankRepository.FindAllAsync(r => (r.NodeId == nodeId && r.User.RoleId == (int)EnumRol.Administrator)
                                                                             || (r.NodeId == nodeId && r.UserId == userId))).ToList();
                }
                else
                {
                    banks = (await _questionBankRepository.FindAllAsync(r => (r.NodeId == nodeId && r.User.RoleId == (int)EnumRol.AdministratorRA)
                                                                             || (r.NodeId == nodeId && r.UserId == userId))).ToList();
                }

                var data = banks.OrderByDescending( r => r.RegisterDate)
                                .Select(qb => new Entities.QuestionBank
                {
                    Id = qb.Id,
                    Active = qb.Active,
                    Description = qb.Description,
                    Name = userId == qb.UserId ? $"{qb.Name} - (Propietario)" : qb.Name,
                    NodeId = qb.NodeId,
                    Questions = qb.Questions?.Select(q => new Entities.Question
                    {
                        Id             = q.Id,
                        Active         = q.Active,
                        Content        = q.Content,
                        Explanation    = q.Explanation,
                        QuestionBankId = q.QuestionBankId,
                        QuestionTypeId = q.QuestionTypeId,
                        UrlImage       = q.UrlImage,
                        ImageHeight    = q.ImageHeight,
                        ImageWidth     = q.ImageWidth,
                        QuestionType   = new Entities.QuestionType { Id = q.QuestionType.Id, Description = q.QuestionType.Description },
                        Answers = q.Answers?.Select(a => new Entities.Answer
                        {
                            Id                  = a.Id,
                            Active              = a.Active,
                            Description         = a.Description,
                            IsCorrect           = a.IsCorrect,
                            QuestionId          = a.QuestionId,
                            RelationDescription = a.RelationDescription,
                            ImageHeight         = a.ImageHeight,
                            ImageWidth          = a.ImageWidth
                        }).ToList()
                    }).ToList()
                });




                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = data;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Actualizar el estatus de un banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatusQuestionBank(QuestionBankStatusRequest request)
        {
            var result = new Response();

            try
            {                
                var bank    = await _questionBankRepository.FindAsync(x => x.Id == request.Id);
                bank.Active = request.Status;                

                _questionBankRepository.Update(bank);
                await this._unitOfWork.SaveChangesAsync();

                result.Data    = bank.Id;
                result.Success = true;
                result.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Método que permite actualizar los datos de un banco de imagen ya registrado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateQuestionBank(QuestionBankUpdateRequest request)
        {
            var result = new Response();

            try
            {
                var bank = await _questionBankRepository.FindAsync(x => x.Id == request.Id);

                if (bank == null)
                    throw new Exception("Los datos del banco de preguntas no fueron encontrados");

                bank.Id          = request.Id;
                bank.Name        = request.Name;
                bank.Description = request.Description;
                bank.NodeId      = request.NodeId;
                bank.UpdateDate  = DateTime.Now;                

                _questionBankRepository.Update(bank);
                await this._unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos guardados correctamente";
                result.Data    = bank.Id;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtiene la estructura en donde se encuentrén bancos de preguntas
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetStructuraWithQuestionBank(int userId, int systemId)
        {
            var result = new Response();

            try
            {
                var questionBanks = new List<Entities.QuestionBank>();

                var user          = await _userRepository.FindAsync(x => x.Id == userId);
                var structurew    = (await _nodeRepository.FindAllAsync(x => x.SystemId == systemId)).ToList();

                if (systemId == (int)EnumSystems.SALI)
                    questionBanks = (await _questionBankRepository.FindAllAsync(r => r.User.RoleId == (int)EnumRol.Administrator || r.UserId == userId)).ToList();
                else
                    questionBanks = (await _questionBankRepository.FindAllAsync(r => r.User.RoleId == (int)EnumRol.AdministratorRA || r.UserId == userId)).ToList();

                var nodes = new List<Entities.Node>();
                foreach (int id in questionBanks.Select(p => p.NodeId).Distinct())
                {
                    var n = questionBanks.First(p => p.NodeId == id).Node;
                    nodes.Add(new Node
                    {
                        Id = n.Id,
                        Active = n.Active,
                        Description = n.Description,
                        NodeTypeId = n.NodeTypeId,
                        ParentId = n.ParentId,
                        UrlImage = n.UrlImage,
                        Name = n.Name,
                        Numericalmapping = n.Numericalmapping
                    });
                }               
                
                if (systemId == (int)EnumSystems.RA  && user.RoleId == (int)EnumRol.TeacherRA) {
                    var accesLevel            = user.UserCCTs.Select(x => x.Node);
                    var nodesNumericalMapping = accesLevel.Select(n => n.Numericalmapping);
                    var nodesAccess           = new List<Entities.Node>();

                    foreach(var index in nodesNumericalMapping) {
                        nodesAccess.AddRange(nodes.Where(x => x.Numericalmapping.StartsWith(index)));
                    }
                    nodes = nodesAccess;
                }

                
               
                var fullStructure = NodeUtility.GetFullStructureByList(nodes, structurew);
                fullStructure     = fullStructure.Concat(nodes).Select(x => x).Distinct().ToList();

                result.Data    = fullStructure;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Realiza la búsqueda de un banco de preguntas por el identificador principal
        /// </summary>
        /// <param name="id">Identificador unico del banco de prguntas</param>
        /// <returns></returns>
        public async Task<Response> GetByListIds(List<int> ids)
        {
            var result = new Response();

            try
            {
                var banks = await _questionBankRepository.FindAllAsync(r => ids.Contains(r.Id));

                var data = banks.OrderByDescending(r => r.RegisterDate)
                              .Select(qb => new Entities.QuestionBank
                              {
                                  Id = qb.Id,
                                  Active = qb.Active,
                                  Description = qb.Description,
                                  Name = qb.Name,
                                  NodeId = qb.NodeId,
                                  Questions = qb.Questions?.Select(q => new Entities.Question
                                  {
                                      Id = q.Id,
                                      Active = q.Active,
                                      Content = q.Content,
                                      Explanation = q.Explanation,
                                      QuestionBankId = q.QuestionBankId,
                                      QuestionTypeId = q.QuestionTypeId,
                                      UrlImage = q.UrlImage,
                                      ImageHeight = q.ImageHeight,
                                      ImageWidth = q.ImageWidth,
                                      QuestionType = new Entities.QuestionType { Id = q.QuestionType.Id, Description = q.QuestionType.Description },
                                      Answers = q.Answers?.Select(a => new Entities.Answer
                                      {
                                          Id = a.Id,
                                          Active = a.Active,
                                          Description = a.Description,
                                          IsCorrect = a.IsCorrect,
                                          QuestionId = a.QuestionId,
                                          RelationDescription = a.RelationDescription,
                                          ImageHeight = a.ImageHeight,
                                          ImageWidth = a.ImageWidth
                                      }).ToList()
                                  }).ToList()
                              });

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = data;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Guardar los datos de un banco de preguntas cargado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveLoad(QuestionBankLoadRequest request) {

            var result = new Response();

            try
            {
                var questionBank = new Entities.QuestionBank();
                questionBank.Description  = request.Description;
                questionBank.Name         = request.Name;
                questionBank.NodeId       = request.NodeId;
                questionBank.Active       = true;
                questionBank.RegisterDate = DateTime.UtcNow.AddHours(offset.Hours);
                questionBank.SystemId     = request.SystemId;
                questionBank.UserId       = request.UserId;

                foreach (var q in request.Questions) {

                    var question = new Entities.Question
                    {
                        Content         = q.Content,
                        QuestionTypeId  = q.QuestionTypeId,
                        Explanation     = q.Explanation,                        
                    };

                    foreach (var a in q.Answers) {
                        var answers = new Entities.Answer
                        {
                            Description         = a.Description,
                            RelationDescription = a.RelationDescription,
                            IsCorrect           = a.IsCorrect,                            
                            Active              = true
                        };

                        question.Answers.Add(answers);
                    }

                    questionBank.Questions.Add(question);
                }


                _questionBankRepository.Add(questionBank);
                await _unitOfWork.SaveChangesAsync();

                result.Data    = questionBank.Id;
                result.Message = "Banco de preguntas cargado correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;

        }

        
    }
}