﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IExamScore
    {
        Task<Response> GetAllByUserId(int userId);
        Task<Response> GetAllByStudentGroupId(int studentGroupId);
        Task<Response> GetByExamUser(int ExamScheduleId, int UserId);
        Task<Response> SaveGroupScore(List<ExamScoreSaveRequest> scoreList);
        Task<Response> Add(ExamScoreSaveRequest request);
        Task<Response> Update(ExamScoreUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
