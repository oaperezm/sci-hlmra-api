﻿using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class CourseSection : ICourseSection
    {
        private readonly ICourseSectionRepository _CourseSectionRepository;        
        private readonly IUnitOfWork _unitOfWork;
        
        public CourseSection(ICourseSectionRepository CourseSectionRepository,
                       IUnitOfWork unitOfWor) {

            this._CourseSectionRepository = CourseSectionRepository;
            this._unitOfWork       = unitOfWor;            
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(CourseSectionSaveRequest request)
        {
            var response = new Response();

            try
            {
                var CourseSectionAdd = new Project.Entities.CourseSection
                {
                    Content      = request.Content,
                    Name         = request.Name,
                    Order        = request.Order,
                    CourseId     = request.CourseId
                };

                _CourseSectionRepository.Add(CourseSectionAdd);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
                response.Data    = CourseSectionAdd.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar una nueva sección del curso: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Obtener todos los cursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var response = new Response();

            try
            {
                var CourseSections =  _CourseSectionRepository.GetAll();

                if (CourseSections != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = CourseSections.Select(c => new Project.Entities.CourseSection
                    {
                        Id          = c.Id,
                        Name        = c.Name,
                        Content     = c.Content,
                        Order       = c.Order,
                        CourseId    = c.CourseId
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener las secciones del curso: { ex.Message} ";
            }

            return response;
        }

        /// <summary>
        /// Obtener los datos de un curso por el identificador principal
        /// </summary>
        /// <param name="id">Identificador del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var response = new Response();

            try
            {
                var c = _CourseSectionRepository.GetById(id);

                if (c != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data =  new Project.Entities.CourseSection
                    {
                        Id       = c.Id,
                        Name     = c.Name,
                        Content  = c.Content,
                        Order    = c.Order,
                        CourseId = c.CourseId
                    };
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos del curso: { ex.Message} ";
            }

            return response;
        }
        
        /// <summary>
        /// Método para actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseSectionUpdateRequest request)
        {
            var response = new Response();

            try
            {
                var CourseSectionUpdate = _CourseSectionRepository.GetById(request.Id);

                if (CourseSectionUpdate == null)
                    throw new Exception("No se encontraron datos del curso proporcionado");

                CourseSectionUpdate.Name     = request.Name;
                CourseSectionUpdate.Content  = request.Content;
                CourseSectionUpdate.Order    = request.Order;
                CourseSectionUpdate.CourseId = request.CourseId;                

                _CourseSectionRepository.Update(CourseSectionUpdate);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo curso: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Borrar la sección del curso
        /// </summary>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            var result = new Response();

            try
            {
                var courseSection = await _CourseSectionRepository.FindAsync(x => x.Id == id);

                if (courseSection == null)
                {
                    throw new Exception(" No se econtrarón los datos de la sección del curso");
                }

                _CourseSectionRepository.Delete(courseSection);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }


        /// <summary>
        /// Obtener todos los cursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetByCourseId(int courseId)
        {
            var response = new Response();

            try
            {
                var CourseSections = _CourseSectionRepository.GetAll().Where(cs => cs.CourseId.Equals(courseId)).ToList();

                if (CourseSections != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = CourseSections.Select(c => new 
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Content = c.Content,
                        Order = c.Order,
                        CourseId = c.CourseId,
                        ClassCount = c.CourseClasses.Count()
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener las secciones del curso: { ex.Message} ";
            }

            return response;
        }

    }
}
