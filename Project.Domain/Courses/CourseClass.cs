﻿using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class CourseClass : ICourseClass
    {
        private readonly ICourseClassRepository _CourseClassRepository;
        private readonly ITeacherResourceRepository _teacherResourceRepository;
        private readonly IUnitOfWork _unitOfWork;
        
        public CourseClass(ICourseClassRepository CourseClassRepository,
                           ITeacherResourceRepository teacherResourceRepository,
                           IUnitOfWork unitOfWor) {

            this._CourseClassRepository     = CourseClassRepository;
            this._teacherResourceRepository = teacherResourceRepository;
            this._unitOfWork                = unitOfWor;            
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(CourseClassSaveRequest request)
        {
            var response = new Response();

            try
            {
                var CourseClassAdd = new Project.Entities.CourseClass
                {
                    Content      = request.Content,
                    Name         = request.Name,
                    Order        = request.Order,
                    CourseSectionId     = request.CourseSectionId
                };

                _CourseClassRepository.Add(CourseClassAdd);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
                response.Data    = CourseClassAdd.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar una nueva sección del curso: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Obtener todos los cursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            var response = new Response();

            try
            {
                var CourseClasss =  _CourseClassRepository.GetAll();

                if (CourseClasss != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data = CourseClasss.Select(c => new Project.Entities.CourseClass
                    {
                        Id          = c.Id,
                        Name        = c.Name,
                        Content     = c.Content,
                        Order       = c.Order,
                        CourseSectionId    = c.CourseSectionId
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener las secciones del curso: { ex.Message} ";
            }

            return response;
        }

        /// <summary>
        /// Obtener los datos de un curso por el identificador principal
        /// </summary>
        /// <param name="id">Identificador del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            var response = new Response();

            try
            {
                var c = _CourseClassRepository.GetById(id);

                if (c != null)
                {

                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data =  new Project.Entities.CourseClass
                    {
                        Id       = c.Id,
                        Name     = c.Name,
                        Content  = c.Content,
                        Order    = c.Order,
                        CourseSectionId = c.CourseSectionId
                    };
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener los datos del curso: { ex.Message} ";
            }

            return response;
        }
        
        /// <summary>
        /// Método para actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseClassUpdateRequest request)
        {
            var response = new Response();

            try
            {
                var CourseClassUpdate = _CourseClassRepository.GetById(request.Id);

                if (CourseClassUpdate == null)
                    throw new Exception("No se encontraron datos del curso proporcionado");

                CourseClassUpdate.Name     = request.Name;
                CourseClassUpdate.Content  = request.Content;
                CourseClassUpdate.Order    = request.Order;
                // CourseClassUpdate.CourseSectionId = request.CourseSectionId;                

                _CourseClassRepository.Update(CourseClassUpdate);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Datos guardados correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar un nuevo curso: { ex.Message }";
            }

            return response;
        }

        /// <summary>
        /// Agregar archivos a una clase en especifico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddFiles(CourseClassFilesRequest request) {

            var response = new Response();

            try
            {
                var courseClass = await _CourseClassRepository.FindAsync(x => x.Id == request.Id);

                if (courseClass == null)
                    throw new Exception("No se encontraron datos de la clase");

                if(courseClass.Files.Count > 0)
                    courseClass.Files.Clear();

                var files = await _teacherResourceRepository.FindAllAsync(x => request.FilesId.Contains(x.Id));

                files.ToList().ForEach(x => courseClass.Files.Add(x));

                _CourseClassRepository.Update(courseClass);
                _unitOfWork.SaveChanges();

                response.Success = true;
                response.Message = "Archivos asignados correctamente";
                response.Data    = files.Select(f => new Entities.TeacherResources {
                    Id             = f.Id,
                    Description    = f.Description,
                    Name           = f.Name,
                    Url            = f.Url,
                    ResourceType   = f.ResourceType == null ? new Entities.ResourceType() :
                                                              new Entities.ResourceType { Id = f.ResourceType.Id, Description = f.ResourceType.Description }                   
                });

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al agregar archivos a la clase: { ex.Message }";
            }

            return response;

        }

        /// <summary>
        /// Remover un archivo de una clase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFiles(CourseClassFilesRequest request) {

            var response = new Response();

            try
            {
                var courseClass = await _CourseClassRepository.FindAsync(x => x.Id == request.Id);

                if (courseClass == null)
                    throw new Exception("No se encontraron datos de la clase");

                request.FilesId.ForEach( fileId =>
                {
                    var file = courseClass.Files.FirstOrDefault(x => x.Id == fileId );
                    courseClass.Files.Remove(file);
                });
                
                _CourseClassRepository.Update(courseClass);
                _unitOfWork.SaveChanges();

                response.Success = true;
                response.Message = "Se removieron los archivos correctamente de la clase";
               

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al eliminar archivos a la clase: { ex.Message }";
            }

            return response;
        }
        
        /// <summary>
        /// Borrar la sección del curso
        /// </summary>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            var result = new Response();

            try
            {
                var courseClass = await _CourseClassRepository.FindAsync(x => x.Id == id);

                if (courseClass == null)
                {
                    throw new Exception(" No se econtrarón los datos de la clase del curso");
                }

                _CourseClassRepository.Delete(courseClass);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al eliminar los datos: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener todos los cursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetBySectionId(int courseSectionId)
        {
            var response = new Response();

            try
            {
                var CourseClasss = _CourseClassRepository.GetAll().Where(cc => cc.CourseSectionId.Equals(courseSectionId)).ToList();

                if (CourseClasss != null)
                {
                    response.Success = true;
                    response.Message = "Datos obtenidos correctamente";
                    response.Data    = CourseClasss.Select(c => new Project.Entities.CourseClass
                    {
                        Id              = c.Id,
                        Name            = c.Name,
                        Content         = c.Content,
                        Order           = c.Order,
                        CourseSectionId = c.CourseSectionId,
                        Files = c.Files.Select( f=> new Entities.TeacherResources {
                            Id          = f.Id,
                            Name        = f.Name,
                            Description = f.Description,
                            Url         = f.Url                            
                        }).ToList()
                    });
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Error al obtener las secciones del curso: { ex.Message} ";
            }

            return response;
        }

    }
}
