﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IStudentStatus
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddStudentStatus(StudentStatusSaveRequest request);
        Task<Response> UpdateStudentStatus(StudentStatusUpdateRequest request);
        Task<Response> DeleteStudentStatus(int StudentStatusId);
    }
}
