﻿using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class LinkInterest: ILinkInterest
    {
        private readonly IUnitOfWork _unitWork;
        private readonly ILinkInterestRepository _linkInterestRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly ICourseRepository _courseRepository;

        public LinkInterest( IUnitOfWork unitWork,
                             ILinkInterestRepository linkInterestRepository,
                             IStudentRepository studentRepository,
                             ICourseRepository courseRepository )
        {
            _unitWork               = unitWork;
            _linkInterestRepository = linkInterestRepository;
            _studentRepository      = studentRepository;
            _courseRepository       = courseRepository;
        }

        /// <summary>
        /// Agregar un nuevo sitio de interes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(LinkInterestSaveRequest request) {

            var result = new Response();

            try
            {
                var course = await  _courseRepository.FindAsync(x => x.Id == request.CourseId);

                if (course == null)
                    throw new Exception("Datos del curso no encontrado");

                var link = new Entities.LinkInterest
                {
                    CourseId    = request.CourseId,
                    Description = request.Description,
                    Title       = request.Title,
                    Uri         = request.Uri
                };

                _linkInterestRepository.Add(link);
                await _unitWork.SaveChangesAsync();

                result.Data    = link.Id;
                result.Message = "Datos guardados correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar un nuevo sitio de interés: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Actualizar los datos de un sitio de interes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(LinkInterestUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var link = await _linkInterestRepository.FindAsync(x => x.Id == request.Id);

                if (link == null)
                    throw new Exception("Datos del sitio de interés no encontrados");
                
                link.Description = request.Description;
                link.Title       = request.Title;
                link.Uri         = request.Uri;              

                _linkInterestRepository.Update(link);
                await _unitWork.SaveChangesAsync();

                result.Data = link.Id;
                result.Message = "Datos acutalizados correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al actualizar el sitio de interés: { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Eliminar un sitio de interés existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id) {

            var result = new Response();

            try
            {
                var link = await _linkInterestRepository.FindAsync(x => x.Id == id);

                if (link == null)
                    throw new Exception("Datos del sitio de interés no encontrados");
                              
                _linkInterestRepository.Delete(link);
                await _unitWork.SaveChangesAsync();
                               
                result.Message = "Datos eliminados correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar el sitio de interés: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los sitios de interés de los cursos 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int userId, int nodeId) {

            var result = new Response();

            try
            {
                var studentGroup = await _studentRepository.FindAllAsync(x => x.UserId == userId);

                var groupIds     = studentGroup.Where( x => x.StudentGroup.Course.NodeId == nodeId)
                                               .Select(x => x.StudentGroup.CourseId)
                                               .ToList();

                var data         = await _linkInterestRepository.FindAllAsync(x => groupIds.Contains(x.CourseId));

                result.Data = data.Select(l => new Entities.LinkInterest
                {
                    Id          = l.Id,
                    Uri         = l.Uri,
                    CourseId    = l.CourseId,
                    Description = l.Description,
                    Title       = l.Title
                });

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas obtener los sitios de interés: { ex.Message }";
            }

            return result;
        }

        /// <summary>
        /// Obtener los sitios de interés de un curso
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Response> GetByCourse(int courseId) {

            var result = new Response();

            try
            {
                var data = await _linkInterestRepository.FindAllAsync(x => x.CourseId == courseId);

                result.Data = data.Select(l => new Entities.LinkInterest
                {
                    Id          = l.Id,
                    Uri         = l.Uri,
                    CourseId    = l.CourseId,
                    Description = l.Description,
                    Title       = l.Title
                });

                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas obtener los sitios de interés: { ex.Message }";
            }

            return result;
        }
    }
}
