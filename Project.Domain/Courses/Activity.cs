﻿using Project.Entities.DTO;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using Project.Repositories.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class Activity : IActivity
    {
        private readonly IActivityRepository _activitykRepository;
        private readonly IActivityAnswerRepository _activityAnswerRepository;
        private readonly IActivityFileRepository _activityFileRepository;
        private readonly ITeacherResourceRepository _teacherResourceRepository;
        private readonly IStudentGroupRepository _studentGroupRepository;
        private readonly IPushNotificationService _srvPushNotification;
        private readonly IFileUploader _fileUploader;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICoursePlanningRepository _coursePlanningRepository;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;

        public Activity(IActivityRepository ActivityRepository,
                        ITeacherResourceRepository teacherResourceRepository,
                        IActivityAnswerRepository activityAnswerRepository,
                        IActivityFileRepository activityFileRepository,
                        IStudentGroupRepository studentGroupRepository,
                        IPushNotificationService srvPushNotification,
                        IFileUploader fileUploader,
                        IUnitOfWork unitOfWor,
                        ICoursePlanningRepository coursePlanningRepository,
                        ISchedulingPartialRepository schedulingPartialRepository,
                        IExamScheduleRepository examScheduleRepository)
        {

            this._activitykRepository = ActivityRepository;
            this._teacherResourceRepository = teacherResourceRepository;
            this._studentGroupRepository = studentGroupRepository;
            this._srvPushNotification = srvPushNotification;
            this._fileUploader = fileUploader;
            this._unitOfWork = unitOfWor;
            this._activityAnswerRepository = activityAnswerRepository;
            this._activityFileRepository = activityFileRepository;
            this._coursePlanningRepository = coursePlanningRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
            this._examScheduleRepository = examScheduleRepository;
        }

        #region Activity
        /// <summary>
        /// Obtener todas las tareas de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByGroup(int id)
        {

            var result = new Response();

            try
            {
                var data = await _activitykRepository.FindAllAsync(x => x.StudentGroupId == id);
                var activities = data.Select(h => new Entities.Activity
                {
                    Id = h.Id,

                    TypeOfQualification = h.TypeOfQualification,
                    Description = h.Description,
                    Name = h.Name,
                    ScheduledDate = h.ScheduledDate,
                    OpeningDate = h.OpeningDate,
                    SchedulingPartialId = h.SchedulingPartialId,
                    RegisterDate = h.RegisterDate,
                    TypeScore =h.TypeScore,
                    AssignedQualification= h.AssignedQualification,
                    Files = h.Files.Select(f => new Entities.TeacherResources
                    {
                        Id = f.Id,
                        Description = f.Description,
                        Name = f.Name,
                        Url = f.Url,
                        ResourceType = f.ResourceType == null ? new Entities.ResourceType() : new Entities.ResourceType
                        {
                            Id = f.ResourceType.Id,
                            Description = f.ResourceType.Description
                        }
                    }).ToList()
                });

                result.Data = activities;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una tarea: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Agregar una nueva Actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ActivitySaveRequest request)
        {

            var result = new Response();

            try
            {
                var activity = new Entities.Activity
                {
                    Description = request.Description,
                    Name = request.Name,
                    RegisterDate = DateTime.Now,
                    ScheduledDate = request.ScheduledDate,
                    UserId = request.UserId,
                    OpeningDate = request.OpeningDate,
                    SchedulingPartialId = request.SchedulingPartialId,
                    StudentGroupId = request.StudentGroupId,
                    Qualified = request.Qualified,
                    TypeOfQualification = request.TypeOfQualification,
                    AssignedQualification = request.AssignedQualification,
                    TypeScore = request.TypeScore != null ? request.TypeScore.Value : false
            };

                if (request.FilesId != null && request.FilesId.Count > 0)
                {
                    var files = await _teacherResourceRepository.FindAllAsync(x => request.FilesId.Contains(x.Id));
                    files.ToList().ForEach(f => activity.Files.Add(f));
                }

                _activitykRepository.Add(activity);
                await _unitOfWork.SaveChangesAsync();

                #region PUSHNOTIFICATION

                var group = await _studentGroupRepository.FindAsync(x => x.Id == activity.StudentGroupId);
                var data = new
                {
                    groupName = group.Description,
                    homeWork = request.Name,
                    date = request.OpeningDate.ToShortDateString(),
                };
                var userIds = group.Students.Select(x => x.UserId).ToList();
                // await _srvPushNotification.CreateNotification(userIds, (int)EnumPush.NewActivity, data);

                #endregion

                result.Data = activity.Id;
                result.Success = true;
                result.Message = "Actividad agregada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una actividad: { ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Eliminar los datos de una Actividad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Remove(int id)
        {

            var result = new Response();

            try
            {
                var activity = await _activitykRepository.FindAsync(x => x.Id == id);
                activity.Files.Clear();

                _activitykRepository.Delete(activity);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Datos eliminados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al eliminar los datos; { ex.Message }";
            }

            return result;

        }

        /// <summary>
        /// Actualizar los datos de una Actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(ActivityUpdateRequest request)
        {

            var result = new Response();

            try
            {
                var activity = await _activitykRepository.FindAsync(x => x.Id == request.Id);

                activity.Description = request.Description;
                activity.Name = request.Name;
                activity.ScheduledDate = request.ScheduledDate;
                activity.OpeningDate = request.OpeningDate;
                activity.SchedulingPartialId = request.SchedulingPartialId;

                activity.UserId = request.UserId;
                activity.StudentGroupId = request.StudentGroupId;
                activity.Qualified = request.Qualified;
                activity.TypeOfQualification = request.TypeOfQualification;
                activity.AssignedQualification = request.AssignedQualification;
                activity.TypeScore = request.TypeScore != null ? request.TypeScore.Value : false;

                if (request.FilesId != null && request.FilesId.Count > 0)
                {
                    activity.Files.Clear();

                    var files = await _teacherResourceRepository.FindAllAsync(x => request.FilesId.Contains(x.Id));
                    files.ToList().ForEach(f => activity.Files.Add(f));
                }

                _activitykRepository.Update(activity);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Actividad actualizada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas actualizar los datos : { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar archivo de una Actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFile(ActivityRemoveFileRequest request)
        {

            var result = new Response();

            try
            {
                var activity = await _activitykRepository.FindAsync(x => x.Id == request.Id);
                var file = await _teacherResourceRepository.FindAsync(x => x.Id == request.TeacherResourceId);

                activity.Files.Remove(file);

                _activitykRepository.Update(activity);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Archivo agregado correctamente";
                result.Data = new Entities.TeacherResources
                {
                    Id = file.Id,
                    Description = file.Description,
                    Name = file.Name,
                    Url = file.Url
                };
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al agregar archivo : { ex.Message}";
            }

            return result;


        }

        /// <summary>
        /// Actualizar el estatus de la actividad programada
        /// </summary>
        /// <param name="activityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int activityId, bool status)
        {

            var result = new Response();

            try
            {
                var activity = await _activitykRepository.FindAsync(x => x.Id == activityId);

                activity.Qualified = status;
                _activitykRepository.Update(activity);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Actividad cerrada correctamente";

            }
            catch (Exception ex)
            {
                result.Message = $"Problemas al actualizar el estauts: { ex.Message }";
                result.Success = false;
            }

            return result;
        }

        /// <summary>
        /// Obtener todos las respuesta de los alumnos registrados
        /// </summary>
        /// <param name="studenGroupId"></param>
        /// <param name="activityId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsAnswers(int studenGroupId, int activityId)
        {

            var result = new Response();

            try {


                //var activityAnswers = activity.ActivityAnswers.ToList();
                var activity = await _activitykRepository.FindAsync(x => x.Id == activityId);
                var group = await _studentGroupRepository.FindAsync(x => x.Id == studenGroupId);
                var students = group.Students.ToList();
                    var answers = new List<ActivityScoreDTO>();
              
                
               var ActivityAnswer2 = _activityAnswerRepository.GetMany(x => x.ActivityId == activityId );

             

                students.ForEach(s =>
                {
                    
                    foreach (Entities.ActivityAnswer item in ActivityAnswer2)
                    {
                        if (item != null)
                        {
                            var studentHomework = new ActivityScoreDTO();

                            studentHomework.ActivityId = activityId;
                            studentHomework.UserId = s.UserId;
                            studentHomework.FullName = s.User.FullName;


                            Entities.ActivityAnswer ActivityAnswer1 = new Entities.ActivityAnswer();
                            studentHomework.Id = item.Id;
                            studentHomework.HasHomwRowk = true;
                            studentHomework.Score = item.Score;
                            studentHomework.Comment = item.Comment;
                            studentHomework.Delivered = item.Delivered;
                            studentHomework.AddNewAnswers = item.AddNewAnswers;
                            studentHomework.Files = item.ActivityAnswerFiles.Select(f => new Entities.ActivityFile
                            {
                                Id = f.Id,
                                Comment = f.Comment,
                                ActivityAnswerId = f.ActivityAnswerId,
                                Url = f.Url
                            }).ToList();
                            answers.Add(studentHomework);
                            
                        }
                       
                    }

                });





                result.Data = new
                    {
                        activity = new Entities.Activity
                        {
                            Id = activity.Id,
                            Name = activity.Name,
                            Qualified = activity.Qualified,
                            Description = activity.Description,
                            OpeningDate= activity.OpeningDate,
                            ScheduledDate = activity.ScheduledDate,
                            SchedulingPartialId = activity.SchedulingPartialId,
                            RegisterDate = activity.RegisterDate,
                            StudentGroupId = activity.StudentGroupId,
                            TypeOfQualification = activity.TypeOfQualification,
                            UserId=activity.UserId,
                            TypeScore=activity.TypeScore

                        },
                        students = answers.OrderBy(a => a.FullName).ToList()
                    };
                    result.Message = "Datos obtenidos correcamente";
                    result.Success = true;



                }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al obtener los alumnos y sus actividades: { ex.Message }";
            }

            return result;

        }


        #endregion

        #region Activity Answer

        /// <summary>
        /// Agregar una nueva respuesta del alumno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddAnswer(ActivityAnswerSaveRequest request)
        {

            var result = new Response();
            string fileUrl = string.Empty;
            string container = "activity";

            try
            {
                // En caso contener datos elarray de bytes se suben los datos al storage
                if (request.FileArray != null && request.FileArray.Length > 0)
                    fileUrl = await this._fileUploader.FileUpload(request.FileArray, request.FileName, container);

                var answerFile = new Entities.ActivityFile
                {
                    RegisterDate = DateTime.UtcNow,
                    Url = fileUrl,
                    Comment = request.Comment
                };

                var activityAnswer = await _activityAnswerRepository.FindAsync(x => x.StudentId == request.UserId && x.ActivityId == request.ActivityId);

                if (activityAnswer == null)
                {

                    activityAnswer = new Entities.ActivityAnswer
                    {
                        ActivityId = request.ActivityId,
                        StudentId = request.UserId,
                        RegisterDate = DateTime.UtcNow,
                        Score = string.Empty,
                        Delivered = request.Delivered,
                        AddNewAnswers = request.AddNewAnswers,
                        PartialEvaluation = request.PartialEvaluation,
                        StatusActivity = request.StatusActivity
                    };

                    activityAnswer.ActivityAnswerFiles.Add(answerFile);
                    _activityAnswerRepository.Add(activityAnswer);

                }
                else
                {

                    activityAnswer.ActivityAnswerFiles.Add(answerFile);
                    _activityAnswerRepository.Update(activityAnswer);
                }

                await _unitOfWork.SaveChangesAsync();

                result.Data = new { Id = answerFile.Id, Url = fileUrl };
                result.Success = true;
                result.Message = "Actividad agregada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al registrar la actividad: {ex.Message}";

                if (string.IsNullOrEmpty(fileUrl))
                {
                    await _fileUploader.FileRemove(fileUrl, container);
                }
            }

            return result;

        }


        /// <summary>
        /// Guardar las calificaciones
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveScore(ActivityScoreRequest request)
        {

            var result = new Response();

            try
            {
                var activity = await _activityAnswerRepository.FindAllAsync(x => x.ActivityId == request.ActivityId);

                request.Scores.ForEach(s =>
                {
                    var activityAnswer = activity.FirstOrDefault(h => h.Id == s.ActivityAnswerId);

                    if (activityAnswer != null)
                    {
                        activityAnswer.Score = s.Score;
                        activityAnswer.Comment = s.Comment;
                        activityAnswer.Delivered = s.Delivered;
                        activityAnswer.AddNewAnswers = s.AddNewAnswers;

                        _activityAnswerRepository.Update(activityAnswer);
                    }
                    else
                    {

                        if (s.Score != null)
                        {
                            var answer = new Entities.ActivityAnswer
                            {
                                StudentId = s.UserId,
                                Comment = s.Comment,
                                Score = s.Score,
                                RegisterDate = DateTime.Now,
                                ActivityId = request.ActivityId,
                                Delivered = s.Delivered,
                                AddNewAnswers = s.AddNewAnswers,
                                PartialEvaluation = s.SchedulingPartialId,
                                StatusActivity = s.StatusActivity
                            };

                            _activityAnswerRepository.Add(answer);
                        }
                    }

                });

                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Actividad calificada correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al calificar la actividad: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las respuestas del alumno por tarea
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByActivity(int activityId, int userId)
        {

            var result = new Response();

            try
            {
                var data = await _activityAnswerRepository.FindAllAsync(x => x.ActivityId == activityId && x.StudentId == userId);
                var files = data.SelectMany(x => x.ActivityAnswerFiles)
                                 .Select(f => new Entities.ActivityFile
                                 {
                                     Comment = f.Comment,
                                     Id = f.Id,
                                     ActivityAnswerId = f.ActivityAnswerId,
                                     Url = f.Url,
                                     RegisterDate= f.RegisterDate
                                     
                                 }).ToList();

                result.Data = files;
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problema al obtener los archivos { ex.Message}";
            }

            return result;

        }

        /// <summary>
        /// Eliminar una respuesta de tarea de un alumno
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAnswer(int activityId, int userId, int answerId)
        {

            var result = new Response();

            try
            {
                var answer = await _activityAnswerRepository.FindAsync(x => x.StudentId == userId && x.ActivityId == activityId);

                if (answer == null)
                {
                    throw new Exception("No se encontraron los datos de la tarea");
                }

                var file = await _activityFileRepository.FindAsync(x => x.Id == answerId);

                if (file == null)
                {
                    throw new Exception("No se encontro el archivo");
                }

                await _fileUploader.FileRemove(file.Url, "homework");

                _activityFileRepository.Delete(file);
                await _unitOfWork.SaveChangesAsync();

                result.Success = true;
                result.Message = "Archivo eliminado correctamente";

            }
            catch (Exception ex)
            {

                result.Success = false;
                result.Message = $"Problemas al eliminar el archivo: {ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// Obtener todas las tareas de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllWithScore(int studentGroupId, int userId)
        {

            var result = new Response();

            try
            {
                var answersActivity = await _activityAnswerRepository.FindAllAsync(x => x.Activity.StudentGroupId == studentGroupId && x.StudentId == userId);
                var data = await _activitykRepository.FindAllAsync(x => x.StudentGroupId == studentGroupId);

                var activities = data.Select(h => new ActivityDTO
                {
                    TotalAnswers = h.ActivityAnswers.Count,
                    Id = h.Id,
                    Description = h.Description,
                    Name = h.Name,
                    ScheduledDate = h.ScheduledDate,
                    StudentGroupId = h.StudentGroupId,
                    Score = "",
                    Comment = "",
                    OpeningDate = h.OpeningDate,
                    TypeOfQualification = h.TypeOfQualification ?? false,
                    AssignedQualification = h.AssignedQualification ?? 0,
                    SchedulingPartialId = h.SchedulingPartialId,
                    TypeScore = h.TypeScore ?? false,                    
                    Files = h.Files.Select(f => new Entities.TeacherResources
                    {
                        Id = f.Id,
                        Description = f.Description,
                        Name = f.Name,
                        Url = f.Url,
                        ResourceType = f.ResourceType == null ? new Entities.ResourceType() : new Entities.ResourceType
                        {
                            Id = f.ResourceType.Id,
                            Description = f.ResourceType.Description
                        }
                    }).ToList()
                }).ToList();

                //if (answersActivity.Count > 0)
                //{
                    activities.ForEach(hw =>
                    {
                        var dataACt = _schedulingPartialRepository.GetAll().Where(xx => xx.Id == hw.SchedulingPartialId);
                        dataACt.ToList().ForEach(xy =>
                        {
                            hw.SchedulingPartialDescription = xy.Description;
                        });

                        var a = answersActivity.FirstOrDefault(x => x.ActivityId == hw.Id);
                        if (a != null)
                        {
                            hw.Score = a.Score;
                            hw.Comment = a.Comment;
                            hw.Qualified = a.Score != "" ? true : false;
                            hw.Delivered = a.Delivered;
                            hw.AddNewActivity = a.AddNewAnswers;
                            hw.FileFound = a.ActivityAnswerFiles.Count > 0 ? true : false;
                            
                        }
                    });
                //}


                result.Data = activities;
                result.Message = "Datos obtenidos correctamente";
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Problemas al programar una actividad: { ex.Message}";
            }

            return result;
        }

        #endregion
    }
}
