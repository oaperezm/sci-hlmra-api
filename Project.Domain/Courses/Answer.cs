﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;

namespace Project.Domain.Courses
{
    public class Answer : IAnswer
    {
        private readonly IAnswerRepository _AnswerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Answer(IAnswerRepository AnswerRepository, IUnitOfWork unitOfWor)
        {

            this._AnswerRepository = AnswerRepository;
            this._unitOfWork = unitOfWor;
        }

        public async Task<Response> AddAnswer(AnswerSaveRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Answer Answer = new Entities.Answer();

                Answer.Description = request.Description;
                Answer.IsCorrect = request.IsCorrect;
                Answer.Order = request.Order;
                Answer.QuestionId = request.QuestionId;

                _AnswerRepository.Add(Answer);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Answer;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al guardar los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> DeleteAnswer(int AnswerId)
        {
            var response = new Response();

            try
            {
                var answer = await _AnswerRepository.FindAsync(x => x.Id == AnswerId);

                if (answer == null)
                    throw new Exception("No se encontraron datos de la respuesta");

                _AnswerRepository.Delete(answer);
                await _unitOfWork.SaveChangesAsync();

                response.Success = true;
                response.Message = "Respuesta eliminada correctamente";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = $"Problemas al eliminar la rspuesta: {ex.Message}";
            }

            return response;
        }

        public async Task<Response> GetAll()
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = _AnswerRepository.GetAll().
                    Select(u => new {
                        Id = u.Id,
                        Description = u.Description,
                        IsCorrect = u.IsCorrect,
                        Order = u.Order,
                        QuestionId = u.QuestionId
                    }).ToList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
                result.Data = await _AnswerRepository.FindAsync(r => r.Id.Equals(id));
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> UpdateAnswer(AnswerUpdateRequest request)
        {
            var result = new Response();

            try
            {
                result.Success = true;
                result.Message = "Datos guardados correctamente";

                Entities.Answer Answer = new Entities.Answer();

                Answer.Id = request.Id;
                Answer.Description = request.Description;
                Answer.IsCorrect = request.IsCorrect;
                Answer.Order = request.Order;
                Answer.QuestionId = request.QuestionId;

                _AnswerRepository.Update(Answer);
                await this._unitOfWork.SaveChangesAsync();

                result.Data = Answer;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }

            return result;
        }
        
    }
}