﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IAnswer
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddAnswer(AnswerSaveRequest request);
        Task<Response> UpdateAnswer(AnswerUpdateRequest request);
        Task<Response> DeleteAnswer(int AnswerId);
    }
}
