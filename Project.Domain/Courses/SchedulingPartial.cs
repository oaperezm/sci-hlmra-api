﻿using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class SchedulingPartial : ISchedulingPartial
    {
        private readonly ICoursePlanningRepository _coursePlanningRepository;
        private readonly ISchedulingPartialRepository _schedulingPartialRepository;
        private readonly IExamScheduleRepository _examScheduleRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="CoursePlanningRepository"></param>
        /// <param name="schedulingPartialRepository"></param>
        /// <param name="unitOfWork"></param>
        public SchedulingPartial(ICoursePlanningRepository coursePlanningRepository, ISchedulingPartialRepository schedulingPartialRepository,
            IExamScheduleRepository examScheduleRepository, IUnitOfWork unitOfWork)
        {
            this._coursePlanningRepository = coursePlanningRepository;
            this._schedulingPartialRepository = schedulingPartialRepository;
            this._examScheduleRepository = examScheduleRepository;
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response> Add(SchedulingPartialSaveRequest request)
        {
            var result = new Response();
            try
            {
                var cp = await _coursePlanningRepository.FindAsync(p => p.Id == request.CoursePlanningId);
                if (cp == null)
                    throw new Exception("No es posible guardar la información del parcial para la planeación el curso indicada");

                var entidad = new Entities.SchedulingPartial()
                {
                    Description = request.Description,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    FinalPartial = request.FinalPartial,
                    CoursePlanningId = request.CoursePlanningId
                };

                _schedulingPartialRepository.Add(entidad);
                await _unitOfWork.SaveChangesAsync();

                result.Data = entidad.Id;
                result.Success = true;
                result.Message = "Datos del parcial guardados correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }
            return result;
        }

        public async Task<Response> Update(SchedulingPartialUpdateRequest request)
        {
            var result = new Response();
            try
            {
                var cp = await _coursePlanningRepository.FindAsync(p => p.Id == request.CoursePlanningId);
                if (cp == null)
                    throw new Exception("No es posible actualizar la información del parcial para la planeación del curso");

                var data = await _schedulingPartialRepository.FindAsync(x => x.Id == request.Id);
                if (data == null)
                    throw new Exception("No es posible actualizar la información del parcial para la planeación del curso");

                data.Description = request.Description;
                data.StartDate = request.StartDate;
                data.EndDate = request.EndDate;
                data.FinalPartial = request.FinalPartial;
                data.UpdateDate = DateTime.Now;

                _schedulingPartialRepository.Update(data);
                _unitOfWork.SaveChanges();

                result.Data = data.Id;
                result.Success = true;
                result.Message = "Datos del parcial actualizados correctamente";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al actualizar los datos: { ex.Message }";
            }
            return result;
        }

        public async Task<Response> Delete(int id, int coursePlanningId)
        {
            var result = new Response();
            try
            {
                var cp = await _coursePlanningRepository.FindAsync(p => p.Id == coursePlanningId);
                if (cp == null)
                    throw new Exception("No es posible borrar la información del parcial para la planeación del curso");

                var data = await _schedulingPartialRepository.FindAsync(x => x.Id == id);
                if (data == null)
                    throw new Exception("No es posible borrar la información del parcial para la planeación del curso");

                var examSchedule = await _examScheduleRepository.FindAllAsync(p => p.SchedulingPartialId == id);
                if (examSchedule != null && examSchedule.Count > 0)
                    throw new Exception("No se puede borrar el parcial porque está asociado a una programación de exámen");

                _schedulingPartialRepository.Delete(data);
                _unitOfWork.SaveChanges();
                result.Success = true;
                result.Message = "Datos del parcial borrados";

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al borrar los datos: {ex.Message}";
            }
            return result;
        }

        public async Task<Response> GetAll(int coursePlanningId)
        {
            var result = new Response();
            try
            {
                var data = await _schedulingPartialRepository.FindAllAsync(p => p.CoursePlanningId == coursePlanningId);

                result.Data = data.OrderByDescending(x => x.StartDate).Select(p => new
                {
                    p.Id,
                    p.Description,
                    p.StartDate,
                    p.EndDate,
                    p.FinalPartial,
                    p.CoursePlanningId
                }).ToList();
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

        public async Task<Response> GetById(int id)
        {
            var result = new Response();
            try
            {
                var data = await _schedulingPartialRepository.FindAsync(p => p.Id == id);
                if (data == null)
                    throw new Exception("No se encontró registros del parcial");
                result.Data = new
                {
                    data.Id,
                    data.Description,
                    data.StartDate,
                    data.EndDate,
                    data.FinalPartial,
                    data.CoursePlanningId
                };
                result.Success = true;
                result.Message = "Datos obtenidos correctamente";
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }
            return result;
        }

        public async Task<Response> GetByStudentGroup(int StudentGroupId)
        {
            var result = new Response();
            try
            {
                var data = await _schedulingPartialRepository.FindAllAsync(p => p.CoursePlanning.StudentGroupId == StudentGroupId);
                //result.Data = data.OrderByDescending(x => x.StartDate).Select(p => new
                //{
                //    p.Id,
                //    p.Description,
                //    p.StartDate,
                //    p.EndDate,
                //    p.FinalPartial,
                //    p.CoursePlanningId
                //}).ToList();

                if (data.Count >0)
                 {
                    var sp = data.OrderByDescending(x => x.StartDate).Select(p => new
                    {
                        p.Id,
                        p.Description,
                        p.StartDate,
                        p.EndDate,
                        p.FinalPartial,
                        p.CoursePlanningId
                    }).ToList();

                    var sporderbyid = sp.OrderBy(x => x.Id).Where(x => x.Description != "Final").ToList();
                    var spFinal = sp.Where(x => x.Description == "Final");
                    var spunion = sporderbyid.Select(p => new
                    {
                        p.Id,
                        p.Description,
                        p.StartDate,
                        p.EndDate,
                        p.FinalPartial,
                        p.CoursePlanningId
                    }).Union(spFinal.Select(p => new
                    {
                        p.Id,
                        p.Description,
                        p.StartDate,
                        p.EndDate,
                        p.FinalPartial,
                        p.CoursePlanningId
                    })).ToList();

                    result.Data = spunion;
                    result.Success = true;
                    result.Message = "Datos obtenidos correctamente";
                }
                else
                {
                    throw new Exception("No se encontro ningun registro para este grupo");

                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error al obtener los datos: { ex.Message }";
            }

            return result;
        }

    }
}
