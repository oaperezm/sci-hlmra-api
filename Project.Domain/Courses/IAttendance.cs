﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface IAttendance
    {
        Task<Response> SaveAttendances(AttendanceSaveRequest request);
        Task<Response> GetAttendances(DateTime initDate, DateTime endDate, int studentGroupId);
    }
}
