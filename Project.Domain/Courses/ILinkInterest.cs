﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public interface ILinkInterest
    {
        Task<Response> Add(LinkInterestSaveRequest request);
        Task<Response> Update(LinkInterestUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetByCourse(int courseId);
        Task<Response> GetByStudent(int userId, int nodeId);
    }
}
