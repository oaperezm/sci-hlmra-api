﻿using Project.Entities.Responses;
using Project.Repositories.Core;
using Project.Repositories.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Domain.Courses
{
    public class ResourceType: IResourceType
    {
        private readonly IResourceTypeRepository _resourceType;
        private readonly IUnitOfWork _unitOfWork;

        public ResourceType(IResourceTypeRepository resourceType, IUnitOfWork unitOfWork)
        {
            this._resourceType = resourceType;
            this._unitOfWork   = unitOfWork;
        }

        /// <summary>
        /// Obtener todos los tipos de recursos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {

            var response = new Response();

            try
            {
                var data = (await _resourceType.GetAllAsync())
                           .OrderBy( x => x.Description)
                           .Select(x => new Entities.ResourceType {
                                Id = x.Id,
                                Description = x.Description,
                                Active = x.Active,
                                Order = x.Order,                             
                            });

                response.Data    = data;
                response.Success = true;
                response.Message = "Datos obtenidos correctamente";

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Problemas al obtener los datos";
            }

            return response;
        }


    }
}
