﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Test : IEntityBase
    {
        public int Id                    { get; set; }

        public DateTime RegisterDate     { get; set; }

        public decimal Score             { get; set; }

        public bool IsCompleted          { get; set; }

        public int ClientId              { get; set; }  
        
        public int? UserId               { get; set; }

        [ForeignKey("UserId")]
        public virtual User User         { get; set; }
                
        public int? ExamScheduleId       { get; set; }
        [ForeignKey("ExamScheduleId")]
        public virtual ExamSchedule ExamSchedule { get; set; }

        public int? CorrectAnswers       { get; set; }

        public string Comments           { get; set; }

        public DateTime? StartDate       { get; set; }

        public DateTime? LastResponseDate { get; set; }

        public bool? IsEvaluated       { get; set; }

        public DateTime? EvaluationDate   { get; set; }

        public virtual ICollection<TestAnswer> Answers { get; set; } = new List<TestAnswer>();
    }
}
