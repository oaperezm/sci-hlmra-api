﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class UserNotification : IEntityBase
    {
        /// <summary>
        /// Identificador de la notificación
        /// </summary>
        public int Id                { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId            { get; set; }

        /// <summary>
        /// Objeto del usuario relacionado
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User     { get; set; }

        /// <summary>
        /// Mensaje de la notificación
        /// </summary>
        public string Message        { get; set; }

        /// <summary>
        /// Titulo de la notificación
        /// </summary>
        public string Title          { get; set; }

        /// <summary>
        /// Tipo de notificacion
        /// </summary>
        public int NotificationTypeId { get; set; }

        /// <summary>
        /// Valor booleano que determina si esta leido 
        /// la notificación
        /// </summary>
        public bool Read             { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Fecha que fue leido la notificación
        /// </summary>
        public DateTime? ReadDate     { get; set; }
    }
}
