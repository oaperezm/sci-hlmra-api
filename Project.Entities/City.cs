﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class City: IEntityBase
    {
        public int Id                            { get; set; }

        public string Description                { get; set; }

        public virtual ICollection<State> States { get; set; } = new List<State>();


    }
}
