﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Rol : IEntityBase
    {
        public int Id                { get; set; }

        public string Name           { get; set; }

        public string Description    { get; set; }

        public bool Active           { get; set; }
      
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
