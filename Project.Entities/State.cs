﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class State: IEntityBase
    {
        public int Id            { get; set; }

        public string Name       { get; set; }

        public int CityId        { get; set; }

        public virtual City City { get; set; }
    }
}
