﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
   public class Events: IEntityBase
    {
        public int Id { get; set; }
        //Nombre del evento
        public string Name { get; set; }
        //Descripción del evento
        public string Description { get; set; }
        //Indicador de activo/desactivo de eventos
        public bool Active { get; set; }

    }
}
