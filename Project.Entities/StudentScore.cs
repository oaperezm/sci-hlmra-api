﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class StudentScore : IEntityBase
    {
        public int Id { get; set; }

        public float Homework { get; set; }

        public float Exam { get; set; }

        public float Assistance { get; set; }

        public float Activity { get; set; }

        public float Score { get; set; }

        public int Points { get; set; }

        public int Status { get; set; }

        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Comentario del profesor
        /// </summary>
        public string Comment { get; set; }

        [ForeignKey("StudentGroup")]
        public int StudentGroupId { get; set; }
        public virtual StudentGroup StudentGroup { get; set; }

        [ForeignKey("Student")]
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int? SchedulingPartialId { get; set; }
        [ForeignKey("SchedulingPartialId")]
        public virtual SchedulingPartial SchedulingPartial { get; set; }
    }
}
