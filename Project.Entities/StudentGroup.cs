﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class StudentGroup : IEntityBase
    {
        public int Id { get; set; }
        public string Description { get; set; }
        
        public bool Active { get; set; }
        public int Status { get; set; }

        public int CourseId { get; set; }
        public string Code { get; set; }
        [ForeignKey("CourseId")]        
        public virtual Course Course { get; set; }

        public virtual ICollection<Student> Students            { get; set; } = new List<Student>();

        public virtual ICollection<ExamSchedule> ExamSchedules  { get; set; } = new List<ExamSchedule>();

        public virtual ICollection<ScheduledEvent> Events       { get; set; } = new List<ScheduledEvent>();

        public virtual ICollection<ForumThread> Threads         { get; set; } = new List<ForumThread>();       
                
		public virtual ICollection<Announcement> Announcements 		{ get; set; } = new List<Announcement>();

    }
}
