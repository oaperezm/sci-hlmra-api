﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class UserDevice: IEntityBase
    {
        /// <summary>
        /// Identificador principal del dispositivo
        /// </summary>
        public int Id             { get; set; }

        /// <summary>
        /// Identificador del cliente donde se registra el token
        /// </summary>
        public int ClientId       { get; set; }
        
        /// <summary>
        /// Identificador unico para los dispositivos 
        /// </summary>
        public Guid DeviceId      { get; set; }

        /// <summary>
        /// Token de firebase para el dispositivo
        /// </summary>
        public string TokenDevice { get; set; }
        
        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId { get; set; }
        
        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Objeto del usuario
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
