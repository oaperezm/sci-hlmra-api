﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class LinkInterest: IEntityBase
    {
        /// <summary>
        /// Identificador de la clase
        /// </summary>
        public int Id               { get; set; }

        /// <summary>
        /// Titulo del enlace
        /// </summary>
        public string Title         { get; set; }

        /// <summary>
        /// Descripción del enlace
        /// </summary>
        public string Description   { get; set; }

        /// <summary>
        /// Url del link de interes
        /// </summary>
        public string Uri           { get; set; }

        /// <summary>
        /// Identificador del curso
        /// </summary>
        public int CourseId          { get; set; }
        
        /// <summary>
        /// Objeto del curso
        /// </summary>
        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }
    }
}
