﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class VReportExam : IEntityBase
    {
        public int Id { get; set; }
        public string cct { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string nameSchool { get; set; }
        public string level { get; set; }
        public int totalquestion { get; set; }
        public int questionbank { get; set; }

        public string signature { get; set; }
        public DateTime starDate { get; set; }
        //public DateTime endDate { get; set; }
    }
}
