﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class FileTypeSaveRequest
    {
        [Required]
        public string Description { get; set; }
    }

    public class FileTypeUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }

}
