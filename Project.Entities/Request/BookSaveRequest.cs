﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class BookSaveRequest
    {
        [Required]
        public string Description     { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name            { get; set; }

        [Required]
        [MaxLength(15)]
        public string ISBN            { get; set; }

        [Required]
        [MaxLength(20)]
        public string Codebar         { get; set; }
          
        public string Edition         { get; set; }

        public List<int>  AuthorId    { get; set; }

        public bool Active            { get; set; }

    }

    public class BookUpdateRequest
    {
        [Required]
        public int Id               { get; set; }

        [Required]
        public string Description   { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name          { get; set; }

        [Required]
        [MaxLength(15)]
        public string ISBN          { get; set; }

        [Required]
        [MaxLength(20)]
        public string Codebar       { get; set; }

        public string Edition       { get; set; }

        public List<int> AuthorId   { get; set; }

        public bool Active          { get; set; }
    }

}
