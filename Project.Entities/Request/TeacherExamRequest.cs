﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class TeacherExamSaveRequest
    {
        /// <summary>
        /// Identificador del tipo de examen
        /// </summary>
        [Required]
        public int TeacherExamTypeId { get; set; }

        /// <summary>
        /// Descripción del examen
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Estatus dle registro
        /// </summary>
        [Required]
        public bool Active { get; set; }

        /// <summary>
        /// Lisda te identificadores de preguntas
        /// </summary>
        [Required]
        public List<ExamQuestionRequest> Questions { get; set; }

        /// <summary>
        /// Identificador de ponderación
        /// </summary>        
        public string WeightingUnit { get; set; }

        /// <summary>
        /// Puntuación Máxima
        /// </summary>
        public int MaximumExamScore { get; set; }

        /// <summary>
        /// Se cálcula automáticamente el valor de cada pregunta
        /// </summary>
        public bool IsAutomaticValue { get; set; }

    }

    public class TeacherExamUpdateRequest
    {

        /// <summary>
        /// Identificador del examen 
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Identificador del tipo de examen
        /// </summary>
        [Required]
        public int TeacherExamTypeId { get; set; }

        /// <summary>
        /// Descripción del examen
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Estatus dle registro
        /// </summary>
        [Required]
        public bool Active { get; set; }

        /// <summary>
        /// Lisda te identificadores de preguntas
        /// </summary>
        [Required]
        public List<ExamQuestionRequest> Questions { get; set; }

        /// <summary>
        /// Identificador de ponderación
        /// </summary>        
        public string WeightingUnit { get; set; }

        /// <summary>
        /// Puntuación Máxima
        /// </summary>
        public int MaximumExamScore { get; set; }

        /// <summary>
        /// Se cálcula automáticamente el valor de cada pregunta
        /// </summary>
        public bool IsAutomaticValue { get; set; }
    }
}
