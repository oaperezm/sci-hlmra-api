﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class PublishingProfileUserSaveRequest
    {
        [Required]
        public int PublishingProfileId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public DateTime RegisterDate { get; set; }
    }

    public class PublishingProfileUserUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int PublishingProfileId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public DateTime RegisterDate { get; set; }
    }

    public class PublishingProfileUserNodesRequest
    {

        [Required]
        public int Id { get; set; }
        [Required]
        public List<int> Nodes { get; set; }
    }
}

