﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class StudentSaveRequest
    {
        [Required]
        public DateTime InvitationDate { get; set; }
        public DateTime CheckDate { get; set; }
        [Required]
        public int StudentGroupId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int StudentStatusId { get; set; }
        [Required]
        public string Codigo { get; set; }

    }

    public class StudentUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime InvitationDate { get; set; }
        public DateTime CheckDate { get; set; }
        [Required]
        public int StudentGroupId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int StudentStatusId { get; set; }
    }

    public class StudentSendInvitationRequest
    {
        [Required]
        public int StudentGroupId   { get; set; }

        [Required]
        public List<int> UserIds    { get; set; }

        public string homeUrl       { get; set; }

        public int SystemId         { get; set; }
    }
}
