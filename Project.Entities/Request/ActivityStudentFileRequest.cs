﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class ActivityStudentFileSaveRequest
    { 
        [Required]
        public int ActivitiesId { get; set; }
         
        public int UserId { get; set; } 

        [Required]
        public string Url { get; set; } 

        public string Comment { get; set; } 

        public string FileName { get; set; }

        [Required]
        public byte[] FileArray { get; set; }
    }

    public class ActivityStudentFileUpdateRequest
    { 
        [Required]
        public int Id { get; set; }

        [Required]
        public int ActivitiesId { get; set; }

        public int UserId { get; set; }

        [Required]
        public string Url { get; set; }

        public string Comment { get; set; }

        public string FileName { get; set; }

        [Required]
        public byte[] FileArray { get; set; }
    }

    public class ActivityStudentFileRemoveFileRequest
    {
        [Required]
        public int Id { get; set; }
    }

    public class ActivityStudentFileStatusRequest
    {
        [Required]
        public int ActivityId { get; set; }

        [Required]
        public bool Status { get; set; }
    }
}
