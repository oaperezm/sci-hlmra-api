﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class RoleSaveRequest
    {
        public int Id               { get; set; }
        [Required]
        public string Name          { get; set; }

        public string Description   { get; set; }

        public bool Active          { get; set; }

        public int SystemId         { get; set; }
    }

}
