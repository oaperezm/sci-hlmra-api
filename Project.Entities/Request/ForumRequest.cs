﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{

    /// <summary>
    /// Objeto para guardar un hilo
    /// </summary>
    public class ThreadSaveRequest
    {
        [Required]
        public string Title         { get; set; }

        [Required]
        public string Description   { get; set; }  

        [Required]
        public int CourseId         { get; set; }

        public List<int> Groups     { get; set; }

        public DateTime? InitDate   { get; set; }

        public DateTime? EndDate    { get; set; }
    }

    /// <summary>
    /// Objeto para actualizar los datos de un thread
    /// </summary>
    public class ThreadUpdateRequest
    {
        [Required]
        public int Id               { get; set; }

        [Required]
        public string Title         { get; set; }

        [Required]
        public string Description   { get; set; }

        [Required]
        public int CourseId         { get; set; }

        public List<int> Groups     { get; set; }

        public DateTime? InitDate   { get; set; }

        public DateTime? EndDate    { get; set; }
    }

    /// <summary>
    /// Objeto para guardar un post
    /// </summary>
    public class PostSaveRequest {

        [Required]
        public int ForumThreadId    { get; set; }

        
        public string Title         { get; set; }

        [Required]
        public string Description   { get; set; }

        public string Replay        { get; set; }
    }

    /// <summary>
    /// Objeto para actualizar los datos de un post
    /// </summary>
    public class PostUpdateRequest
    {

        [Required]
        public int Id               { get; set; }

        
        public string Title         { get; set; }

        [Required]
        public string Description   { get; set; }

        public string Replay        { get; set; }
    }

    /// <summary>
    /// Objeto para actualizar el estatus del comentario
    /// </summary>
    public class PostUpdateStatusRequest {

        [Required]
        public int Id     { get; set; }

        [Required]
        public bool Status { get; set; }
    }

    /// <summary>
    /// Objeto para agregar un voto al comentario
    /// </summary>
    public class PostUpdateVoteRequest {

        [Required]
        public int Id { get; set; }

        [Required]
        public int Vote { get; set; }

    }
}
