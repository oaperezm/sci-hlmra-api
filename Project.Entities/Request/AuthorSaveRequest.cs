﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class AuthorSaveRequest
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Comment { get; set; }
    }

    public class AuthorUpdateRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Comment { get; set; }
    }

}
