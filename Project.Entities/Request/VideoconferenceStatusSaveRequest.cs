﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
   public class VideoconferenceStatusSaveRequest
    {
        [Required]
        public string Description { get; set; }
    }
    public class VideoconferenceStatusUpdateRequest
    {
        [Required]
        public string Description { get; set; }
    }
}
