﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class ExamQuestionRequest
    {
        /// <summary>
        /// Identificador principal de la pregunta
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la pregunta 
        /// </summary>        
        public Decimal Value { get; set; }

        /// <summary>
        /// Identificador del examén del maestro
        /// </summary>        
        public bool UserEdited { get; set; }

    }
}


