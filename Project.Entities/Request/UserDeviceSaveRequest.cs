﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class UserDeviceSaveRequest
    {
        [Required]
        public string Token { get; set; }
        
        public string DeviceId { get; set; }

        [Required]
        public int ClientId { get; set; }
        public int SystemId { get; set; }
    }

    public class ReadNotificationRequest {

        [Required]
        public int Id { get; set; }

    }
}
