﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities.Request
{
  public  class CoursePlanningSaveRequest
    {
        public float Homework { get; set; }
        public float Exam { get; set; }
        public float Assistance { get; set; }
        public float Activity { get; set; }
        public int NumberPartial { get; set; }
        [Required]
        public int StudentGroupId { get; set; }
    }
    public class CoursePlanningUpdateRequest
    {
        //[Required]
        public int Id { get; set; }
        public float Homework { get; set; }
        public float Exam { get; set; }
        public float Assistance { get; set; }
        public float Activity { get; set; }
        public int NumberPartial { get; set; }
        [Required]
        public int StudentGroupId { get; set; }
    }
}
