﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class PublishingProfileSaveRequest
    {
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }

        public int SystemId { get; set; }

        public List<int> Nodes { get; set; }


    }

    public class PublishingProfileUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }

        public bool Active { get; set; }
        public List<int> Nodes { get; set; }
    }

    public class PublishingProfileNodesRequest
    {

        [Required]
        public int Id { get; set; }
        [Required]
        public List<int> Nodes { get; set; }
    }
}

