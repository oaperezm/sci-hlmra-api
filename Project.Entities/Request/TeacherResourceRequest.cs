﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class TeacherResourceSaveRequest
    {
        public string Name              { get; set; }

        public string Description       { get; set; }

        public int  UserId              { get; set; }

        public int TeacherResourceId    { get; set; }

        [Required]
        public string FileContentBase64 { get; set; }        

        public string FileName          { get; set; }

        public byte[] FileArray         { get; set; }
    }
}
