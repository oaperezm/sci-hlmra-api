﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class QuestionSaveRequest
    {
        [Required]
        public string Content       { get; set; }        
        [Required]
        public bool Active          { get; set; }
        [Required]
        public int Order            { get; set; }
        [Required]
        public int QuestionBankId   { get; set; }
        [Required]
        public int QuestionTypeId   { get; set; }

        public string Explanation   { get; set; }

        public List<Answer> Answers { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName { get; set; }

        public byte[] FileArray { get; set; }
    }

    public class QuestionUpdateRequest
    {
        [Required]
        public int Id               { get; set; }
        [Required]
        public string Content       { get; set; }
        [Required]
        public bool Active          { get; set; }
        [Required]
        public int Order            { get; set; }
        [Required]
        public int QuestionBankId   { get; set; }
        [Required]
        public int QuestionTypeId   { get; set; }

        public string Explanation   { get; set; }

        public List<Answer> Answers { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName { get; set; }

        public byte[] FileArray { get; set; }
    }
}
