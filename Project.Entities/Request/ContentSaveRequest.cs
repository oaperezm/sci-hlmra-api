﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Objeto que envia el cliente para agregar un nuevo contenido
    /// </summary>
    public class ContentSaveRequest
    {
        [Required]
        public string Name                  { get; set; }

        public string UrlContent            { get; set; }

        [Required]
        public bool IsPublic                { get; set; }

        [Required]
        public int ContentTypeId            { get; set; }

        [Required]
        public int FileTypeId               { get; set; }

        public string Thumbnails            { get; set; }

        public bool Active                  { get; set; }

        public string FileContentBase64     { get; set; }

        public string FileName              { get; set; }

        public byte[] FileArray             { get; set; }

        public int? TrainingFieldId         { get; set; }

        public int? PurposeId               { get; set; }

        public int? AreaId                  { get; set; }

        public int? FormativeFieldId        { get; set; }

        public int? ContentResourceTypeId   { get; set; }

        public int SystemId                 { get; set; }

        public List<TagRequest> Tags        { get; set; }

        public string Description           { get; set; }

        public bool IsRecommended           { get; set; }

        public int? BlockId                 { get; set; }

        public string UrlInteractive        { get; set; }

        public int? EducationLevelId { get; set; }

        public int? AreaPersonalSocialDevelopmentId { get; set; }

        public int? AreasCurriculumAutonomyId { get; set; }

        public int? AxisId { get; set; }

        public int? KeylearningId { get; set; }

        public int? LearningexpectedId { get; set; }

    }

    /// <summary>
    /// Objeto que envia el cliente para actualizar los datos de un contenido
    /// </summary>
    public class ContentUpdateRequest
    {
        [Required]
        public int Id                       { get; set; }

        [Required]
        public string Name                  { get; set; }
        
        [Required]
        public bool IsPublic                { get; set; }

        [Required]
        public bool Active                  { get; set; }

        [Required]
        public int ContentTypeId            { get; set; }

        [Required]
        public int FileTypeId               { get; set; }

        public string Thumbnails            { get; set; }

        public string FileContentBase64     { get; set; }

        public string FileName              { get; set; }

        public byte[] FileArray             { get; set; }

        public string UrlContent            { get; set; }

        public int? TrainingFieldId         { get; set; }

        public int? PurposeId               { get; set; }

        public int? AreaId                  { get; set; }

        public int? FormativeFieldId        { get; set; }

        public int? ContentResourceTypeId   { get; set; }

        public List<TagRequest> Tags        { get; set; }

        public string Description           { get; set; }

        public bool IsRecommended           { get; set; }

        public int BlockId                  { get; set; }

        public string UrlInteractive { get; set; }

        public int? EducationLevelId { get; set; }

        public int? AreaPersonalSocialDevelopmentId { get; set; }

        public int? AreasCurriculumAutonomyId { get; set; }

        public int? AxisId { get; set; }

        public int? KeylearningId { get; set; }

        public int? LearningexpectedId { get; set; }

    }

    /// <summary>
    /// Objeto que envia el cliente cuando quiere acutalizar 
    /// el estadus del contenido    
    /// </summary>
    public class ContentUpdateStatusRequest {

        // Identificador del contenido
        [Required]
        public int Id { get; set; }

        // Estatus del contenido que se va modificar
        [Required]
        public bool Status { get; set; }
    }

    /// <summary>
    /// Objeto para asignar los tags a un contenido
    /// </summary>
    public class ContentAssingTagsRequest {

        [Required]
        public int ContentiId   { get; set; }

        [Required]
        public List<int> TagsId { get; set; }
    }

    /// <summary>
    /// Objeto para actualizar tags del contenido
    /// </summary>
    public class ContentTagsRequest {

        [Required]
        public int Id               { get; set; }

        [Required]
        public List<TagRequest> Tags { get; set; }

    }

    /// <summary>
    /// Objeto de los tags
    /// </summary>
    public class TagRequest {

        public int Id      { get; set; }

        public string Name { get; set; }
    }

    /// <summary>
    /// Actualizar visitas de un contenido
    /// </summary>
    public class ContenteVisitsRequest {

        public int Id { get; set; }
    }
}
