﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class ActivitySaveRequest
    { 
        
        [Required]
        public DateTime ScheduledDate { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public int UserId { get; set; } 

        public List<int> FilesId { get; set; }

        public DateTime OpeningDate { get; set; }

        public int SchedulingPartialId { get; set; }
        
        public DateTime RegisterDate { get; set; }

        [Required]
        public int StudentGroupId { get; set; }

        public bool Qualified { get; set; }

        public bool TypeOfQualification { get; set; }

        public int AssignedQualification { get; set; }

        public string SchedulingPartialDescription { get; set; }

        public bool delivered { get; set; }

        public bool addNewActivity { get; set; }

        public bool? TypeScore { get; set; }
    }

    public class ActivityUpdateRequest
    {

        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public int UserId { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }

        [Required]
        public int StudentGroupId { get; set; }

        public List<int> FilesId { get; set; }

        public DateTime OpeningDate { get; set; } 

        public int SchedulingPartialId { get; set; }

        public bool Qualified { get; set; }

        public bool TypeOfQualification { get; set; }

        public int AssignedQualification { get; set; }

        public string SchedulingPartialDescription { get; set; }

        public bool delivered { get; set; }

        public bool addNewActivity { get; set; }

        public bool? TypeScore { get; set; }
    }

    public class ActivityRemoveFileRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int TeacherResourceId { get; set; } 
    }  

    public class ActivityUpdateStatusRequest
    { 
        [Required]
        public int ActivityId { get; set; }

        [Required]
        public bool Status { get; set; }
    }
      
}
