﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Objeto para guardar un link de interes
    /// </summary>
    public class LinkInterestSaveRequest
    {
        public int Id               { get; set; }
        [Required]
        public string Title         { get; set; }

        public string Description   { get; set; }

        [Required]
        public string Uri           { get; set; }

        [Required]
        public int CourseId         { get; set; }
    }

    /// <summary>
    /// Objeto para actualizar un link de interes
    /// </summary>
    public class LinkInterestUpdateRequest
    {
        [Required]
        public int Id               { get; set; }

        [Required]
        public string Title         { get; set; }

        public string Description   { get; set; }

        [Required]
        public string Uri           { get; set; }

        [Required]
        public int CourseId         { get; set; }
    }

}
