﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class QuestionBankSaveRequest
    {
        public string Name          { get; set; }
        [Required]
        public string Description   { get; set; }
        [Required]
        public int NodeId           { get; set; }  
        
        public int SystemId         { get; set; }
    }

    public class QuestionBankUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int NodeId { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool Active { get; set; }
    }

    /// <summary>
    /// Objeto utilizado para actualizar el estatus del banco de preguntas
    /// </summary>
    public class QuestionBankStatusRequest
    {  
        /// <summary>
        /// Identificador principal del banco de preguntas
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Estatus al cual se va actualizar el banco de preguntas
        /// </summary>
        [Required]
        public bool Status { get; set; }
        
    }

    public class QuestionBankLoadRequest {

        [Required]
        public string Name                      { get; set; }
        [Required]
        public string Description               { get; set; }
        [Required]
        public int NodeId                       { get; set; }

        public int SystemId                     { get; set; }

        public int UserId                       { get; set; }

        public List<QuestionRequest> Questions  { get; set; }



    }

    public class QuestionRequest {

        [Required]
        public string Content       { get; set; }               
        [Required]
        public int QuestionTypeId   { get; set; }

        public string QuestionType  { get; set; }

        public string Explanation   { get; set; }

        public int QuestionId       { get; set; }        

        public List<Answer> Answers { get; set; }

    }
}
