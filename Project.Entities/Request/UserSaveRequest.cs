﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class UserSaveRequest
    {
        [Required]
        public string FullName          { get; set; }
        [Required]
        public DateTime Birthday        { get; set; }
        [Required]
        public string Gender            { get; set; }
        [Required]
        public int StateId             { get; set; }
        [Required]
        public string Email             { get; set; }
        [Required]
        public string Password          { get; set; }                
        [Required]
        public int RoleId               { get; set; }
        [Required]
        public int SystemId             { get; set; }

        public int LevelId              { get; set; }

        public int AreaId               { get; set; }

        public int SubsystemId          { get; set; }

        public int Semester             { get; set; }

        public string Key               { get; set; }

        public string Institution       { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName          { get; set; }

        public byte[] FileArray         { get; set; }
    }

    public class UserUpdateRequest {

        [Required]
        public int Id                   { get; set; }

        [Required]
        public string FullName          { get; set; }

        [Required]
        public DateTime Birthday        { get; set; }

        [Required]
        public string Gender            { get; set; }

        [Required]
        public int StateId              { get; set; }

        [Required]
        public string Email             { get; set; }

        [Required]
        public int RoleId               { get; set; }

        public string Key               { get; set; }

        public string Institution       { get; set; }

        public string ProfileImage      { get; set; }

        public byte[] FileContentBase64 { get; set; }

        public string FileName          { get; set; }

        [Required]
        public int SystemId { get; set; }

    }

    public class UserSavePasswordRequest
    {
        [Required]
        public string Email { get; set; }

        public int SystemId { get; set; }
    }

    public class UserValidateRequest
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string systemId { get; set; }
    }

    public class UserNodesRequest
    {

        [Required]
        public int Id { get; set; }
        [Required]
        public List<int> Nodes { get; set; }
    }

    public class UserCopyProfileRequest
    {        
        [Required]
        public int ProfileId { get; set; }
        [Required]
        public int[] UserIds { get; set; }
    }

    public class UserUploadImgProfileRequest
    {
        
        public int Id                    { get; set; }

        public string ProfileImage       { get; set; }

        public string FileContentBase64  { get; set; }

        public string FileName           { get; set; }

        public byte[] FileArray          { get; set; }
    }

    public class UserChangePasswordRequest
    {
        [Required]
        public string Email         { get; set; }
        [Required]
        public string NewPassword   { get; set; }
        [Required]
        public string OldPassword   { get; set; }

    }
}
