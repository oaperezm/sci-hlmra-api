﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class AnnouncementSaveRequest
    {

        /// Descripción del aviso
        [Required]
        public string Title { get; set; }
        
        /// Descripción del aviso
        [Required]
        public string Description { get; set; }        

        [Required]
        public DateTime RegisterDate { get; set; }
 
        /// Estatus del registro
        [Required]
        public bool Active { get; set; }

        [Required]
        public int UserId { get; set; }

        // Lista de identificadores de grupos  
        [Required]
        public List<int> Groups { get; set; }

    }

    public class AnnouncementUpdateRequest
    {
        
        /// Identificador del examen         
        [Required]
        public int Id { get; set; }


        /// Descripción del aviso
        [Required]
        public string Title { get; set; }

        /// Descripción del aviso
        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime RegisterDate { get; set; }

        /// Estatus del registro
        [Required]
        public bool Active { get; set; }

        [Required]
        public List<int> Groups { get; set; }

    }
}
