﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class EventsReportRequest
    {
        
        //CCT del side
        public string CCT { get; set; }
        //Correo del usuario
        public string Email { get; set; }
        //Nombre del usuario
        public string Username { get; set; }
        //Nombre de la Escuela
        public string InstitutionName { get; set; }
        //Nivel de estudio
        [Required]
        public string Level { get; set; }
        //Nombre del estado
        public string State { get; set; }
        //Clasificación de escuela
        public string ClasificationSchool { get; set; }
        //Nombre de la zona
        public string Zone { get; set; }
        //Nombre del control
        public string Control { get; set; }
        //Nombre del Promotor
        public string Prometer { get; set; }
        //Fecha de ingreso
        public DateTime EntryDate { get; set; }
        //Tiempo de navegación
        public string TimeNavegation { get; set; }
        //Nombre de sección
        //public string Sections { get; set; }
        public int SectionId { get; set; }
        public string ResourceName { get; set; }
        //Nombre del grado
        public string Grade { get; set; }
        //Nombre del Campo formativo
        public string EducationField { get; set; }
        //Nobre de la asignature
        public string Subject { get; set; }
        // tipo del formato
        //public string Format { get; set; }
        public int FileTypeId { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        //id del usuario
        public int? UserId { get; set; }
        //Tipo de evento
        public int EventId { get; set; }
        //Registros específicos en los que se realizó la acción
        public string Element { get; set; }
        //Comentario
        public string Comment { get; set; }
        //Detalle de comentario
        public string CommentDetail { get; set; }

        public int EducationLevelId { get; set; }
        public int ContentTypeId { get; set; }
    }
    public class EventsReportUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        //CCT del side
        public string CCT { get; set; }
        //Correo del usuario
        public string Email { get; set; }
        //Nombre del usuario
        public string Username { get; set; }
        //Nombre de la Escuela
        public string InstitutionName { get; set; }
        //Nivel de estudio
        public string Level { get; set; }
        //Nombre del estado
        public string State { get; set; }
        //Clasificación de escuela
        public string ClasificationSchool { get; set; }
        //Nombre de la zona
        public string Zone { get; set; }
        //Nombre del control
        public string Control { get; set; }
        //Nombre del Promotor
        public string Prometer { get; set; }
        //Fecha de ingreso
        public DateTime EntryDate { get; set; }
        //Tiempo de navegación
        public string TimeNavegation { get; set; }
        //Nombre de sección
        //public string Sections { get; set; }
        public int SectionId { get; set; }
        //Nombre del grado
        public string Grade { get; set; }
        //Nombre del Campo formativo
        public string EducationField { get; set; }
        //Nobre de la asignature
        public string Subject { get; set; }
        // tipo del formato
        //public string Format { get; set; }
        public int FileTypeId { get; set; }

        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        //id del usuario
        public int? UserId { get; set; }
        //Tipo de evento
        public int EventId { get; set; }
        //Registros específicos en los que se realizó la acción
        public string Element { get; set; }
        //Comentario
        public string Comment { get; set; }
        //Detalle de comentario
        public string CommentDetail { get; set; }
        public int EducationLevelId { get; set; }

        public int ContentTypeId { get; set; }
        public string ResourceName { get; set; }
    }
}
