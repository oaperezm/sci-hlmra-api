﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class ExamScoreSaveRequest
    {
        [Required]
        public int ExamScheduleId { get; set; }

        [Required]
        public int UserId { get; set; }

        public string Score { get; set; }

        public int CorrectAnswers { get; set; }

        /// <summary>
        /// Descripción de la programación del exámen
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Fecha en la cual se va aplicar el exámen
        /// </summary>
        [Required]
        public DateTime RegisterDate { get; set; }

        public DateTime UpdateDate { get; set; }
    }

    public class ExamScoreUpdateRequest
    {
        public string Id { get; set; }

        [Required]
        public int ExamScheduleId { get; set; }

        [Required]
        public int UserId { get; set; }

        public string Score { get; set; }

        public int CorrectAnswers { get; set; }

        /// <summary>
        /// Descripción de la programación del exámen
        /// </summary>
        public string Comments { get; set; }

        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Fecha en la cual se actualizó la calificación
        /// </summary>
        [Required]
        public DateTime UpdateDate { get; set; }
    }
}
