﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Objeto para registrar una nueva asistencia
    /// </summary>
    public class AttendanceSaveRequest
    {
        [Required]
        public int StudentGroupId                     { get; set; }

        [Required]
        public List<AttendanceDayRequest> Attendances { get; set; }

    }

    /// <summary>
    /// Objeto del detalle de una asistencia
    /// </summary>
    public class AttendanceDetailRequest {

        [Required]
        public int UserId { get; set; }

        [Required]
        public bool AttendanceClass { get; set; }
    }

    /// <summary>
    /// Objeto de las asistencias de un dia
    /// </summary>
    public class AttendanceDayRequest {

        [Required]
        public DateTime AttendanceDate { get; set; }

        [Required]
        public List<AttendanceDetailRequest> StudentAttendances { get; set; }
    }

}
