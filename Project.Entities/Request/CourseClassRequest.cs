﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class CourseClassSaveRequest
    {
        [Required]
        public string Name { get; set; }
        public string Content { get; set; }

        [Required]
        public int Order { get; set; }
        [Required]
        public int CourseSectionId { get; set; }        
    }

    public class CourseClassUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Content { get; set; }

        [Required]
        public int Order { get; set; }
        [Required]
        public int CourseSectionId { get; set; }
    }

    public class CourseClassFilesRequest {

        [Required]
        public int Id { get; set; }

        [Required]
        public List<int> FilesId { get; set; }
    }
}
