﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    /// <summary>
    /// Agregar nueva tarea
    /// </summary>
    public class HomeWorkSaveRequest
    {
        [Required]
        public DateTime ScheduledDate   { get; set; }

        [Required]
        public string Name              { get; set; }

        [Required]
        public string Description       { get; set; }

      
        public int UserId               { get; set; }

        [Required]
        public int StudentGroupId       { get; set; }

        public List<int> FilesId        { get; set; }

        public DateTime OpeningDate { get; set; }

        public bool TypeOfQualification { get; set; }

        public int AssignedQualification { get; set; }

        public int SchedulingPartialId { get; set; }
        
        public bool? TypeScore { get; set; }

    }

    /// <summary>
    /// Actualizar datos de una tarea
    /// </summary>
    public class HomeWorkUpdateRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
              
        [Required]
        public int StudentGroupId { get; set; }

        public List<int> FilesId { get; set; }

        public DateTime OpeningDate { get; set; }

        public bool TypeOfQualification { get; set; }

        public int AssignedQualification { get; set; }

        public int SchedulingPartialId { get; set; }

        public bool? TypeScore { get; set; }

    }

    /// <summary>
    /// Calificar la tarea de un grupo de alumnos
    /// </summary>
    public class HomeWorkScoreRequest
    {
        [Required]
        public int HomeWorkId { get; set; }

        [Required]
        public List<HomeWorkAnswerScoreRequest> Scores { get; set; }
         
        public List<HomeWorkAnswerScoreRequest> Delivered { get; set; }

        public List<HomeWorkAnswerScoreRequest> AddNewAnswers { get; set; }
    }

    /// <summary>
    /// Calificación por alumno
    /// </summary>
    public class HomeWorkAnswerScoreRequest {

        /// <summary>
        /// Identificador de la respuesta de la tarea
        /// </summary>
        [Required]
        public int HomeWorkAnswerId { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        [Required]
        public int UserId           { get; set; }

        /// <summary>
        /// Calificación de la tarea
        /// </summary>
        public string Score         { get; set; }

        /// <summary>
        /// Comentario de la tarea
        /// </summary>
        public string Comment       { get; set; }  

        /// <summary>
        /// Comentario de la tarea
        /// </summary>
        public bool Delivered { get; set; }

        /// <summary>
        /// Comentario de la tarea
        /// </summary>
        public bool AddNewAnswers { get; set; }

        /// <summary>
        /// Indica el parcial para el cual aplica la tarea
        /// </summary>
        public int SchedulingPartialId { get; set; }

        /// <summary>
        /// Indica el estatus de la tarea
        /// </summary>
        public int StatusHomework { get; set; }
    }

    /// <summary>
    /// Agregar archivo a una tarea
    /// </summary>
    public class HomeWorkRemoveFileRequest {

        [Required]
        public int Id                { get; set; }

        [Required]
        public int TeacherResourceId { get; set; }
    }

    /// <summary>
    /// Actualizar el estatus de la tarea
    /// </summary>
    public class HomeWorkUpdateStatusRequest {

        /// <summary>
        /// Identificador de la tarea
        /// </summary>
        [Required]
        public int HomeWorkId { get; set; }

        /// <summary>
        /// Estatus de la tarea programada
        /// </summary>
        [Required]
        public bool Status { get; set; }
    }
}
