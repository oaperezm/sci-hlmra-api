﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class StudentGroupSaveRequest
    {
        [Required]
        public string Description { get; set; }               
        [Required]
        public int Status { get; set; }
        [Required]
        public int CourseId { get; set; }
        public string Code { get; set; }
    }

    public class StudentGroupUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Status { get; set; }
        [Required]
        public int CourseId { get; set; }
        public string Code { get; set; }
    }
}
