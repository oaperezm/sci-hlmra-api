﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{      /// <summary>
       /// Request para guardar un nuevo registro de video conferencia
       /// </summary>
    public class VideoConferenceSaveRequest
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Information { get; set; }

        public DateTime? DateTimeVideoConference { get; set; }
        [Required]
        public String Duration { get; set; }
        [Required]
        public int StatusId { get; set; }
        public string Commentary { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        ////public List<int> GuestIds { get; set; }
        public string RoomVideoConference { get; set; }
        public string GuestMail { get; set; }
        public int CourseId { get; set; }
        public int StudentGroupId { get; set; }
        public string emailsExternal { get; set; }

        public List<InvitedVideoConferenceDTO> Guests { get; set; }
        public List<InvitedVideoConferenceDTO> ExternalGuest { get; set; }

    }
    /// <summary>
    /// Request para actualizar un registro de video conferencia
    /// </summary>
    /// 
    public class VideoConferenceUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Information { get; set; }
        public DateTime? DateTimeVideoConference { get; set; }
        [Required]
        public String Duration { get; set; }
        //[Required]
        //public string Type { get; set; }

        ////[Required]
        ////public int BookId { get; set; }
        ////[Required]
        ////public bool Accepted { get; set; }
        [Required]
        public int StatusId { get; set; }
        //public int EditorId { get; set; }
        //public string Editor { get; set; }
        public string Commentary { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int CourseId { get; set; }
        public int StudentGroupId { get; set; }
        public string RoomVideoConference { get; set; }
        public string GuestMail { get; set; }
        public string emailsExternal { get; set; }
        public List<InvitedVideoConferenceDTO> Guests { get; set; }
        public List<InvitedVideoConferenceDTO> ExternalGuest { get; set; }

    }
    public class InvitedVideoConferenceSaveRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        public string FullEmail { get { return $"{Name}{ Email}"; } }

    }
}
