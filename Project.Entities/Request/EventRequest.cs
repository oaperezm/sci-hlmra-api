﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class EventRequest
    {
        public int Id { get; set; }
        //Nombre del evento
        [Required]
        public string Name { get; set; }
        //Descripción del evento
        [Required]
        public string Description { get; set; }
        //Indicador de activo/desactivo de eventos
        [Required]
        public bool Active { get; set; }

    }
}
