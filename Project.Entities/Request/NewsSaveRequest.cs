﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class NewsSaveRequest
    {
        [Required]
        public string Title             { get; set; }

        [Required]
        public string Description       { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName          { get; set; }

        public byte[] FileArray         { get; set; }

        public bool Published           { get; set; }

        public int SystemId             { get; set; }
    }

    public class NewsUpdateRequest
    {

        [Required]
        public int Id                   { get; set; }


        [Required]
        public string Title             { get; set; }

        [Required]
        public string Description       { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName          { get; set; }

        public byte[] FileArray         { get; set; }

        public bool Published           { get; set; }
    }

}
