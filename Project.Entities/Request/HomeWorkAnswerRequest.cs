﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class HomeWorkAnswerSaveRequest
    {
        /// <summary>
        /// Identificador de la tarea
        /// </summary>
        [Required]
        public int HomeWorkId   { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        public int UserId       { get; set; }

        /// <summary>
        /// Url del del archivo almacenado
        /// </summary>
        [Required]
        public string Url       { get; set; }

        /// <summary>
        /// Comentarios del alumno
        /// </summary>
        public string Comment   { get; set; }

        /// <summary>
        /// Indica si el alumno cumplió con la tarea
        /// </summary>
        public bool Delivered { get; set; } 

        /// <summary>
        /// Indica el parcial para el cual aplica la tarea
        /// </summary>
        public int PartialEvaluation { get; set; } 

        /// <summary>
        /// Indica el estatus de la tarea
        /// </summary>
        public int StatusHomework { get; set; }

        /// <summary>
        /// Indica si el alumno puede agregar nuevas tareas en caso que haya sido indicado que nó cumplió con la tarea
        /// </summary> 
        public bool AddNewAnswers { get; set; }

        /// <summary>
        /// Nombre del archivo
        /// </summary> 
        public string FileName { get; set; }

        /// <summary>
        /// Arreglo de bytes del archivo
        /// </summary>
        [Required]
        public byte[] FileArray { get; set; }

    }
}
