﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class SubsystemSaveRequest
    {
        [Required]
        public string Description { get; set; }
    }

    public class SubsystemUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }

}
