﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class SendMessageRequest
    {
        /// <summary>
        /// cuerpo del mensaje
        /// </summary>
        [Required]
        public string Message     { get; set; }

        /// <summary>
        /// Usuario que va recibir el mensaje
        /// </summary>
        [Required]
        public int ReceiverUserId { get; set; }

        /// <summary>
        /// Identificador del grupo al que pertenece
        /// </summary>
        public int ChatGroupId    { get; set; }

        /// <summary>
        /// Fecha del mensaje
        /// </summary>
        public DateTime? DateMessage { get; set; }

    }
}
