﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class CourseSectionSaveRequest
    {
        [Required]
        public string Name { get; set; }
        public string Content { get; set; }

        [Required]
        public int Order { get; set; }
        [Required]
        public int CourseId { get; set; }        
    }

    public class CourseSectionUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Content { get; set; }

        [Required]
        public int Order { get; set; }
        [Required]
        public int CourseId { get; set; }
    }
}
