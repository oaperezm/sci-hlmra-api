﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class QuestionTypeSaveRequest
    {
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }            
    }

    public class QuestionTypeUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
