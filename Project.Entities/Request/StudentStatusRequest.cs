﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities.Request
{
    public class StudentStatusSaveRequest
    {
        [Required]
        public string Description { get; set; }     
           
        [Required]
        public bool IsCorrect { get; set; }
        [Required]
        public int Order { get; set; }
        [Required]
        public int QuestionId { get; set; }        
    }

    public class StudentStatusUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool IsCorrect { get; set; }
        [Required]
        public int Order { get; set; }
        [Required]
        public int QuestionId { get; set; }
    }
}
