﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class ReportsRequest
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public bool Active { get; set; }
      
    }
    public class ReportsUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool Active { get; set; }
       
    }
}
