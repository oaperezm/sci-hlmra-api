﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Request para agregar una nueva aplicación del examen
    /// </summary>
    public class TestSaveRequest
    {

        public int UserId { get; set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        public int ExamScheduleId { get; set; }

        public List<TestAnswer> Answers { get; set; }
    }

    /// <summary>
    /// Request para actualizar los datos de una aplicación de examen
    /// </summary>
    public class TestUpdateRequest
    {
        [Required]
        public int ClientId { get; set; }

        public List<TestAnswer> Answers { get; set; }
    }

    /// <summary>
    /// Request para agregar respuesta a un examen
    /// </summary>
    public class TestAnswerRequest
    {
        [Required]
        public List<Answer> Answers { get; set; }
    }

    /// <summary>
    /// Request para agregar respuesta a un examen
    /// </summary>
    public class TestAnswerQuestionRequest
    {
        public int UserId { get; set; }

        [Required]
        public int ClientId { get; set; }

        [Required]
        public int ExamScheduleId { get; set; }

        [Required]
        public int QuestionId { get; set; }

        [Required]
        public int AnswerId { get; set; }

        public string Explanation { get; set; }

    }
}
