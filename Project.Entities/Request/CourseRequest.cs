﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Request para guardar un nuevo curso
    /// </summary>
    public class CourseSaveRequest
    {        
        [Required]
        public string Name              { get; set; }

        [Required]
        public string Description       { get; set; }

        [Required]
        public string Gol               { get; set; }

        [Required]
        public DateTime StartDate       { get; set; }

        [Required]
        public DateTime EndDate         { get; set; }
                
        [Required]
        public int NodeId               { get; set; }

        [Required]
        public int UserId               { get; set; }
                
        [Required]
        public bool Monday              { get; set; }

        [Required]
        public bool Tuesday             { get; set; }

        [Required]
        public bool Wednesday           { get; set; }

        [Required]
        public bool Thursday            { get; set; }

        [Required]
        public bool Friday              { get; set; }

        [Required]
        public bool Saturday            { get; set; }

        [Required]
        public bool Sunday              { get; set; }

        [Required]
        public string Subject           { get; set; }

        public DateTime? RegisterDate   { get; set; }

        public DateTime? UpdateDate     { get; set; }

        public bool Active              { get; set; }

        [Required]
        public string Institution       { get; set; }

    }

    /// <summary>
    /// Request para actualizar un nuevo curso
    /// </summary>
    public class CourseUpdateRequest
    {
        [Required]
        public int Id                   { get; set; }
        [Required]
        public string Name              { get; set; }
        [Required]
        public string Description       { get; set; }
        [Required]
        public string Gol               { get; set; }
        [Required]
        public DateTime StartDate       { get; set; }
        [Required]
        public DateTime EndDate         { get; set; }
        [Required]
        public int NodeId               { get; set; }        
        [Required]
        public bool Monday              { get; set; }
        [Required]
        public bool Tuesday             { get; set; }
        [Required]
        public bool Wednesday           { get; set; }
        [Required]
        public bool Thursday            { get; set; }
        [Required]
        public bool Friday              { get; set; }
        [Required]
        public bool Saturday            { get; set; }
        [Required]
        public bool Sunday              { get; set; }
        [Required]
        public string Subject           { get; set; }

        public DateTime? RegisterDate   { get; set; }

        public DateTime? UpdateDate     { get; set; }

        public bool Active              { get; set; }

        [Required]
        public string Institution       { get; set; }
    }

    
}
