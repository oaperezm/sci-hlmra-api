﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class PermissionSaveRequest
    {

        public int Id               { get; set; }
        [Required]
        public string Module        { get; set; }
        [Required]
        public string Description   { get; set; }
        public bool Active          { get; set; }
        public string Url           { get; set; }
        public string Icon          { get; set; }
        public int? ParentId        { get; set; }
        public bool ShowInMenu      { get; set; }
        public int SystemId         { get; set; }
    }

    public class RolPermissionAddRequest {

        [Required]
        public int Id { get; set; }
        [Required]
        public List<int> Permissions { get; set; }
    }

}
