﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class ScheduledEventSaveRequest
    {
        /// <summary>
        /// Nombre del evento
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Descripción del evento
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Fecha del evento
        /// </summary>
        [Required]
        public DateTime DateEvent { get; set; }

        /// <summary>
        /// Fecha y hora de termino del evento
        /// </summary>
        [Required]
        public DateTime EndEvent { get; set; }

        /// <summary>
        /// Horario del evento
        /// </summary>
        [Required]
        public string InitTime { get; set; }

        /// <summary>
        /// Duración del evento en minutos
        /// </summary>
        [Required]
        public int Duration { get; set; }

        /// <summary>
        /// Activar estatus del evento
        /// </summary>
        [Required]
        public bool Active { get; set; }

        /// <summary>
        /// Identificador del curso 
        /// </summary>
        [Required]
        public int CourseId { get; set; }

        /// <summary>
        /// Identificadores de grupo
        /// </summary>
        [Required]
        public List<int> Groups { get; set; }

        public int UserId { get; set; }
    }

    public class ScheduledEventUpdateRequest
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Nombre del evento
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Descripción del evento
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Fecha del evento
        /// </summary>
        [Required]
        public DateTime DateEvent { get; set; }

        /// <summary>
        /// Fecha y hora de termino del evento
        /// </summary>
        [Required]
        public DateTime EndEvent { get; set; }

        /// <summary>
        /// Horario del evento
        /// </summary>
        [Required]
        public string InitTime { get; set; }

        /// <summary>
        /// Duración del evento en minutos
        /// </summary>
        [Required]
        public int Duration { get; set; }

        /// <summary>
        /// Activar estatus del evento
        /// </summary>
        [Required]
        public bool Active { get; set; }

        /// <summary>
        /// Identificadores de grupo
        /// </summary>
        [Required]
        public List<int> Groups { get; set; }
    }
}
