﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Request utilizado para registrar un nuevo glorasio
    /// </summary>
    public class GlossarySaveRequest
    {        
        [Required]
        public string Name          { get; set; }

        [Required]
        public string Description   { get; set; }

        [Required]
        public bool Active          { get; set; }
        
        public int UserId           { get; set; }

    }

    /// <summary>
    /// Request para actulizar los datos de un glosario existente
    /// </summary>
    public class GlossaryUpdateRequest
    {
        [Required]
        public int Id               { get; set; }

        [Required]
        public string Name          { get; set; }

        [Required]
        public string Description   { get; set; }

        [Required]
        public bool Active          { get; set; }               

    }

    /// <summary>
    /// Request para asignar glosario a un nodo
    /// </summary>
    public class AssignNodeRequest {

        [Required]
        public int GlossaryId   { get; set; }

        [Required]
        public int NodeId       { get; set; }

        public int UserId       { get; set; }
    }

    /// <summary>
    /// Request utilizado para agregar un nuevo termino al glosario
    /// </summary>
    public class WordSaveRequest {

        [Required]
        public string Name          { get; set; }

        [Required]
        public string Description   { get; set; }

        [Required]
        public int GlossaryId       { get; set; }
    }


    /// <summary>
    /// Request utilizado para aactualizar los datos de un termino
    /// </summary>
    public class WordUpdateRequest
    {

        [Required]
        public int Id                { get; set; }

        [Required]
        public string Name           { get; set; }

        [Required]
        public string Description    { get; set; }

        
    }
}
