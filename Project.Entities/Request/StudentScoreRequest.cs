﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities.Request
{
    public class StudentScoreSaveRequest
    {
        public float Homework { get; set; }

        public float Exam { get; set; }

        public float Assistance { get; set; }

        public float Activity { get; set; }

        public float Score { get; set; }

        public int? SchedulingPartialId { get; set; }

        public int Points { get; set; }

        public int Status { get; set; }

        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Comentarios del alumno
        /// </summary>
        public string Comment { get; set; }

        [Required]
        public int StudentGroupId { get; set; }
        [Required]
        public int StudentId { get; set; }
        [Required]
        public int UserId { get; set; }
    }

    public class StudentScoreUpdateRequest
    {
        public int Id { get; set; }

        public float Homework { get; set; }

        public float Exam { get; set; }

        public float Assistance { get; set; }

        public float Activity { get; set; }

        public float Score { get; set; }

        public int? SchedulingPartialId { get; set; }

        public int Points { get; set; }

        public int Status { get; set; }

        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Comentarios del alumno
        /// </summary>
        public string Comment { get; set; }

        [Required]
        public int StudentGroupId { get; set; }
        [Required]
        public int StudentId { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}
