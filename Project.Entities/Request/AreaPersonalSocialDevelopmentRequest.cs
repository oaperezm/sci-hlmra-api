﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
   public  class AreaPersonalSocialDevelopmentRequest
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public int EducationLevelId { get; set; }
        [Required]
        public int FileTypeId { get; set; }
    }
    public class AreaPersonalSocialDevelopmentUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public int EducationLevelId { get; set; }
        [Required]
        public int FileTypeId { get; set; }

    }
}
