﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class NodeContentSaveRequest
    {
        
        [Required]
        public int NodeId               { get; set; }        
        [Required]
        public int ContentId            { get; set; } 
        [Required]
        public string NodeRoute         { get; set; }        

        public int SystemId { get; set; }

        
    }

    public class NodeContentUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int ContentId { get; set; }
        [Required]
        public int NodeId { get; set; }

        public string NodeRoute { get; set; }

        public int SystemId { get; set; }


    }

}
