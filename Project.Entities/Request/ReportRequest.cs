﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class ReportRequest
    {
        [Required]
        public int          reportId { get; set; }
        [Required]
        public string          cct { get; set; }
        [Required]
        public int          levelid { get; set; }
        [Required]
        public string       level { get; set; }
        //periodo inicio
       
        public DateTime?     startDate { get; set; }
        //periodo final
       
        public DateTime?     endDate { get; set; }
        [Required]
        public string       grade { get; set; }

        public int?       sectionId { get; set; }

        public int?       FileTypeId { get; set; }
        public string       resourcenName { get; set; }
        public string       username { get; set; }
        public int?         userid { get; set; }

        public int?         student { get; set; }
        public int?         contentTypeId { get; set; }






    }
}
