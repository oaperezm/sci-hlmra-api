﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class PaginationRequest
    {
        /// <summary>
        /// Tamaño de la pagina
        /// </summary>
        [Required]
        public int PageSize    { get; set; }

        /// <summary>
        /// Pagina actual
        /// </summary>
        [Required]
        public int CurrentPage { get; set; }

        /// <summary>
        /// Cadena de búsqueda
        /// </summary>
        public string Filter   { get; set; }

        /// <summary>
        /// Filtro para determinas si es activo o no
        /// </summary>
        public bool? Active { get; set; }

        /// <summary>
        /// Identificador del tipo por el cual se va filtrar
        /// </summary>
        public int Type { get; set; }       
    }

    public class PaginationRequestById
    {

        /// <summary>
        /// Identificador de entidad
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Tamaño de la pagina
        /// </summary>
        [Required]
        public int PageSize { get; set; }

        /// <summary>
        /// Pagina actual
        /// </summary>
        [Required]
        public int CurrentPage { get; set; }

        /// <summary>
        /// Cadena de búsqueda
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Filtro para determinas si es activo o no
        /// </summary>
        public bool? Active { get; set; }

        /// <summary>
        /// Identificador del tipo por el cual se va filtrar
        /// </summary>
        public int Type { get; set; }
    }

    public class PaginationContentRequest {

        /// <summary>
        /// Tamaño de la pagina
        /// </summary>
        [Required]
        public int PageSize { get; set; }

        /// <summary>
        /// Pagina actual
        /// </summary>
        [Required]
        public int CurrentPage { get; set; }

        /// <summary>
        /// Cadena de búsqueda
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Filtro para determinas si es activo o no
        /// </summary>
        public bool? Active { get; set; }

        /// <summary>
        /// Identificador del tipo por el cual se va filtrar
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Identificador del tipo de archivo,
        /// </summary>
        public int FileTypeId { get; set; }

        /// <summary>
        /// Tipo de contenido
        /// </summary>
        public int ContentResourceTypeId { get; set; }

        /// <summary>
        /// Campo formativo
        /// </summary>        
        public int FormativeFieldId { get; set; }

        /// <summary>
        /// Proposito
        /// </summary>       
        public int PurposeId { get; set; }

        public int ContentTypeId { get; set; }
    }

    public class PaginationRelationRequest
    {

        /// <summary>
        /// Identificador de entidad
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Tamaño de la pagina
        /// </summary>
        [Required]
        public int PageSize { get; set; }

        /// <summary>
        /// Pagina actual
        /// </summary>
        [Required]
        public int CurrentPage { get; set; }

        /// <summary>
        /// Cadena de búsqueda
        /// </summary>
        public string Filter { get; set; }
               
       /// <summary>
       /// Identificador del curso
       /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Identificador del curso
        /// </summary>
        public int CourseId { get; set; }
    }
}
