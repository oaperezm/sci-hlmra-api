﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class MailRequest
    {        
        public string FullName       { get; set; }
              
        public string Email          { get; set; }
                
        public string Subject        { get; set; }
               
        public string Message        { get; set; }
              
        public string ReceptionEmail { get; set; }

        public int SystemId          { get; set; }

        public string School         { get; set; }

        public string WorkPlace      { get; set; }
    }
}
