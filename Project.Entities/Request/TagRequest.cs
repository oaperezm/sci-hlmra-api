﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class TagSaveRequest
    {
        [Required]
        public string Name { get; set; }
    }

    public class TagUpdateRequest
    {
        [Required]
        public int Id      { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
