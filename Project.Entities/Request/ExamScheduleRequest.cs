﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    /// <summary>
    /// Objeto utilizado para agregar una nueva
    /// programación de exámenes
    /// programación de examenes    
    /// </summary>
    public class ExamScheduleSaveRequest
    {
        private DateTime? applicationDate;
        private DateTime? endApplicationDate;
        /// <summary>
        /// Descripción de la programación del exámen
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Identificador principal del grupo al cual estará relacionada
        /// la programación del exámen
        /// </summary>
        [Required]
        public int StudentGroupId { get; set; }

        /// <summary>
        /// Identificador del examen relacionado a la programación
        /// </summary>
        public int? TeacherExamId { get; set; }

        /// <summary>
        /// Número del módulo al que corresponde el examen
        /// </summary>        
        public int? SchedulingPartialId { get; set; }

        /// <summary>
        /// Fecha en la cual se va aplicar el exámen
        /// </summary>        
        [Required]
        public DateTime? ApplicationDate
        {
            get { return this.applicationDate; }
            set { this.applicationDate = value; }
        }

        /// <summary>
        /// Fecha de inicio para la aplicación del examen
        /// </summary>        
        [Required]
        public DateTime? BeginApplicationDate
        {
            get { return this.applicationDate; }
            set { this.applicationDate = value; }
        }

        /// <summary>
        /// Fecha fin para la aplicación del examen
        /// </summary>
        [Required]
        public DateTime? EndApplicationDate
        {
            get { return this.endApplicationDate ?? this.applicationDate; }
            set { this.endApplicationDate = value; }
        }

        /// <summary>
        /// Hora de inicio en la que estará disponible el examen para ser constatado
        /// </summary>       
        public DateTime? BeginApplicationTime { get; set; }

        /// <summary>
        /// Hora de fin en la que estará disponible el examen para ser constatado
        /// </summary>        
        public DateTime? EndApplicationTime { get; set; }

        /// <summary>
        /// Minutos disponibles para contestar el examen.
        /// </summary>       
        public int? MinutesExam { get; set; }
    }

    /// <summary>
    /// Objeto utilizado para actualizar los datos de la 
    /// programación de examenes    
    /// </summary>
    public class ExamScheduleUpdateRequest
    {
        private DateTime? applicationDate;
        private DateTime? endApplicationDate;

        /// <summary>
        /// Identificador principal de la programación del examen 
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Descripción de la programación del exámen
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Identificador principal del grupo al cual estará relacionada
        /// la programación del exámen
        /// </summary>
        [Required]
        public int StudentGroupId { get; set; }

        /// <summary>
        /// Identificador del examen relacionado a la programación
        /// </summary>
        public int? TeacherExamId { get; set; }

        /// <summary>
        /// Número del módulo al que corresponde el examen
        /// </summary>        
        public int? SchedulingPartialId { get; set; }

        /// <summary>
        /// Fecha en la cual se va aplicar el exámen
        /// </summary>        
        [Required]
        public DateTime? ApplicationDate
        {
            get { return this.applicationDate; }
            set { this.applicationDate = value; }
        }

        /// <summary>
        /// Fecha de inicio para la aplicación del examen
        /// </summary>        
        [Required]
        public DateTime? BeginApplicationDate
        {
            get { return this.applicationDate; }
            set { this.applicationDate = value; }
        }

        /// <summary>
        /// Fecha fin para la aplicación del examen
        /// </summary>
        [Required]
        public DateTime? EndApplicationDate
        {
            get { return this.endApplicationDate ?? this.applicationDate; }
            set { this.endApplicationDate = value; }
        }
        /// <summary>
        /// Hora de inicio en la que estará disponible el examen para ser constatado
        /// </summary>
        public DateTime? BeginApplicationTime { get; set; }

        /// <summary>
        /// Hora de fin en la que estará disponible el examen para ser constatado
        /// </summary>
        public DateTime? EndApplicationTime { get; set; }

        /// <summary>
        /// Minutos disponibles para contestar el examen.
        /// </summary>
        public int? MinutesExam { get; set; }
    }

    /// <summary>
    /// Objeto utilizado para actualizar el estatus de la 
    /// programación de examen
    /// </summary>
    public class ExamScheduleStatusUpdateRequest
    {

        /// <summary>
        /// Identificador de la programación de examen
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador del estatus de la programación de exámen
        /// </summary>
        public int ExamScheduleTypeId { get; set; }

    }

}
