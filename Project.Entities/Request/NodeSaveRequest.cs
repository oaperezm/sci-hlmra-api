﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Request
{
    public class NodeSaveRequest
    {
        [Required]
        public string Name { get; set; }        
        [Required]
        public int ParentId { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public int NodeTypeId { get; set; }

        public int BookId { get; set; }

        public string Description { get; set; }        
        
        public string FileContentBase64 { get; set; }

        public string FileName { get; set; }

        public byte[] FileArray { get; set; }

        public int SystemId { get; set; }

        public int Order { get; set; }

    }

    public class NodeUpdateRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int ParentId { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public int NodeTypeId { get; set; }

        public int BookId { get; set; }

        public string FileContentBase64 { get; set; }

        public string FileName { get; set; }

        public byte[] FileArray { get; set; }

        public int Order { get; set; }

        //Catalogos  nuevos 
        public int EducationLevelId { get; set; }

        public int AreaPersonalSocialDevelopmentId { get; set; }

        public int AreasCurriculumAutonomyId { get; set; }

        public int AxisId { get; set; }

        public int KeylearningId { get; set; }

        public int LearningexpectedId { get; set; }

    }

    public class NodeAllContentRequest {

        public int NodeId           { get; set; }

        public string FileTypeId    { get; set; }

        public int CurrentPage      { get; set; }

        public int SizePage         { get; set; }

        public int PurposeId        { get; set; }

        public int BlockId          { get; set; }

        public int FormativeFieldId { get; set; }

        public string Filter        { get; set; }

        public int Order { get; set; }

        //Catalogos  nuevos 

        public int EducationLevelId { get; set; }

        public int AreaPersonalSocialDevelopmentId { get; set; }

        public int AreasCurriculumAutonomyId { get; set; }

        public int AxisId { get; set; }

        public int KeylearningId { get; set; }

        public int LearningexpectedId { get; set; }

    }
}
