﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
   public class SchedulingPartial : IEntityBase
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public int FinalPartial { get; set; }
       
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        
        [ForeignKey("CoursePlanning")]
        public int CoursePlanningId { get; set; }
        public virtual CoursePlanning CoursePlanning { get; set; }


    }
}
