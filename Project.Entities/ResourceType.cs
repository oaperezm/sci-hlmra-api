﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ResourceType:IEntityBase
    {
        
        public int Id                { get; set; }
        public string Description    { get; set; }
        public DateTime RegisterDate { get; set; }
        public int Order             { get; set; }
        public bool Active           { get; set; }
                
    }
}
