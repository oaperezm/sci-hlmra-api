﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ForumPostVote: IEntityBase
    {
        public int Id                       { get; set; }

        public int ForumPostId              { get; set; }
        [ForeignKey("ForumPostId")]
        public virtual ForumPost ForumPost  { get; set; }

        public int UserId                   { get; set; }
        [ForeignKey("UserId")]
        public virtual User User            { get; set; }

        public bool Like                    { get; set; }

        public DateTime? RegisterDate       { get; set; }
    }
}
