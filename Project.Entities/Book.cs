﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class Book : IEntityBase
    {
        public int Id                  { get; set; }
        public string Name             { get; set; }
        public string Description      { get; set; }
        public bool Active             { get; set; }
        public string ISBN             { get; set; }
        public string Codebar          { get; set; }
        public string Edition          { get; set; }
        public DateTime  RegisterDate  { get; set; }
        public DateTime? UpdateDate    { get; set; }
        
        public virtual ICollection<Author> Authors { get; set; } = new List<Author>();
        public virtual ICollection<Node> Nodes     { get; set; } = new List<Node>();
    }
}
