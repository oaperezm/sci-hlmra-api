﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ExamScore : IEntityBase
    {
        /// <summary>
        /// Identificador principal de la calificación del examen 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador principal de la programación del examen 
        /// </summary>
        public int ExamScheduleId { get; set; }

        /// <summary>
        /// Identificador principal del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Calificación obtenida en el examen
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Aciertos en el examen
        /// </summary>
        public int CorrectAnswers { get; set; }

        /// <summary>
        /// Comentarios del profesor
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Fecha en la cual se realiza el registro en la base de datos
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Fecha en la cual se realiza alguna actualizaciónen los datos
        /// de la calificación del exámen
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        [ForeignKey("Test")]
        public int? TestId          { get; set; }

        public virtual Test Test                 { get; set; }

        public virtual User User                 { get; set; }

        public virtual ExamSchedule ExamSchedule { get; set; }
    }
}

