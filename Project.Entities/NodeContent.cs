﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Project.Entities
{
    public class NodeContent : IEntityBase
    {
        /// <summary>
        /// Identificador principal 
        /// </summary>
        public int Id                   { get; set; }

        /// <summary>
        /// Identificador del contenido
        /// </summary>
        public int? ContentId           { get; set; }
        [ForeignKey("ContentId")]
        public virtual Content Content  { get; set; }
        
        /// <summary>
        /// Identificaodr del nodo
        /// </summary>
        public int? NodeId              { get; set; }
        [ForeignKey("NodeId")]
        public virtual Node Node        { get; set; }
                
        /// <summary>
        ///  Ruta de la asignación del nodo
        /// </summary>
        public string NodeRoute         { get; set; }
        
        /// <summary>
        /// Permite saber si el nodo es publico o privado
        /// </summary>
        public bool IsPublic            { get; set; }
    }
}
