﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class AttendanceDTO
    {
        /// <summary>
        /// Fecha del día
        /// </summary>
        public DateTime Day { get; set; }

        /// <summary>
        /// Asistencia del día
        /// </summary>
        public List<AttendanceStudentDTO> Attendance { get; set; }
    }


    public class AttendanceStudentDTO {
        
        /// <summary>
        /// Identificador de la asistencia
        /// </summary>
        public int Id             { get; set; }

        /// <summary>
        /// Identificador del grupo
        /// </summary>
        public int StudentGroupId { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId         { get; set; }

        /// <summary>
        /// Asistencia del alumno del día
        /// </summary>
        public bool Attendance    { get; set; }
    }
}
