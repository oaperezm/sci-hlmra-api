﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
   public class StudentGroupDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public int Status { get; set; }

        public int CourseId { get; set; }

        //Datos de planeación del curso
        public int CoursePlanningId { get; set; }
        public float Homework { get; set; }
        public float Exam { get; set; }
        public float Assistance { get; set; }
        public float Activity { get; set; }
        public int CoursePlanningNumberPartial { get; set; }
        //Datos del parcial

        public int SchedulingPartialId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SchedulingPartialDescription { get; set; }
        public int FinalPartial { get; set; }

        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
