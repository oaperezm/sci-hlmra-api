﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class QuestionBankDTO
    {
        public int Id                   { get; set; }

        public string Name              { get; set; }

        public string Description       { get; set; }

        public bool Active              { get; set; }

        public int NodeId               { get; set; }

        public DateTime? RegisterDate   { get; set; }

        public DateTime? UpdateDate     { get; set; }
    }
}
