﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class InvitedVideoConferenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        //public string FullEmail { get { return $"{Name}{ Email}"; } }
    }
}
