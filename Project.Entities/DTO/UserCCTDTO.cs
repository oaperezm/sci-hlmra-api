﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class UserCCTDTO
    {
        public int Id { get; set; }
        /// <summary>
        /// Clave del centro de trabajo
        /// </summary>
        public string CCT { get; set; }

        public int UserId { get; set; }
    }
}
