﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class DetailsStudentFinalScoreDTO
    {
        public int StudentGroupId { get; set; }
        public string StudentGroupDescription { get; set; }
        public int StudentId { get; set; }
        public string StudenName { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public int PercentageExam { get; set; }
        public int PercentageHomework { get; set; }
        public int PercentageActivity { get; set; }
        public int PercentageAssitance { get; set; }
        public List<DetailsStudentScoreDTO> StudentScores { get; set; }
        public Dictionary<string, string> Errors { get; set; }
        public string EmailUser { get; set; } //Armand
    }
}
