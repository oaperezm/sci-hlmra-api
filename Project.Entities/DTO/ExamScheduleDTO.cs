﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ExamScheduleDTO
    {
        /// <summary>
        /// Identificador principal de la programación del examen 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Descripción de la programación del exámen
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Fecha en la cual se va aplicar el exámen
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Identificador principal del grupo al cual estará relacionada
        /// la programación del exámen
        /// </summary>
        public int StudentGroupId { get; set; }

        /// <summary>
        /// Identificador principal del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Identificador del estatus de programación de exámen
        /// </summary>
        public int ExamScheduleTypeId { get; set; }

        /// <summary>
        /// Fecha en la cual se realiza el registro en la base de datos
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Fecha en la cual se realiza alguna actualizaciónen los datos
        /// de la programación del exámen
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Determina si será activo el registro para visualizarse
        /// dentro del grupo
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Identificador principal del examen relacionado 
        /// </summary>
        public int? TeacherExamId { get; set; }

        public bool HasScore      { get; set; }

        public bool IsAnswered    { get; set; }

        public ExamScheduleType ExamScheduleType { get; set; }      

        public StudentGroup Group { get; set; }

        public TeacherExam TeacherExam { get; set; }

        public ExamScore Score { get;  set; }

        /// <summary>
        /// Número del módulo al que corresponde el examen
        /// </summary>
        public int? SchedulingPartialId { get; set; }

        /// <summary>
        /// Descripción del parcial
        /// </summary>
        public string SchedulingPartialDescription { get; set; }

        /// <summary>
        /// Fecha de inicio para la aplicación del examen
        /// </summary>
        public DateTime? BeginApplicationDate { get; set; }

        /// <summary>
        /// Fecha fin para la aplicación del examen
        /// </summary>
        public DateTime? EndApplicationDate { get; set; }

        /// <summary>
        /// Hora de inicio en la que estará disponible el examen para ser constatado
        /// </summary>
        public DateTime? BeginApplicationTime { get; set; }

        /// <summary>
        /// Hora de fin en la que estará disponible el examen para ser constatado
        /// </summary>
        public DateTime? EndApplicationTime { get; set; }

        /// <summary>
        /// Minutos disponibles para contestar el examen.
        /// </summary>
        public int? MinutesExam { get; set; }

    }
}
