﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class VReportProfileTeacherDTO
    {
        public int Id { get; set; }
        //Correo del Profesor
        public string Email { get; set; }
        //Nombre del Profesor
        public string FullName { get; set; }
        //Nombre de la escuela
        public string NameSchool { get; set; }
        //Nivel educativo
        public string Level { get; set; }
        //Nombre  de la asignatura
        public string Subject { get; set; }
        //gupos del profesor
        public string Groups { get; set; }
        //dias transcurridos del curso
        public int ElapsedDays { get; set; }

        public string Period { get; set; }
        //Valor porcentaje actividades
        public float vpActivity { get; set; }

        //Número Evaluaciones programadas 
        public int totalSchedulesExam { get; set; }

        //Número Reactivos base 
        public int BaseQuestion { get; set; }

        //Número Reactivos generados 
        public int GeneratedQuestion { get; set; }
        //  Total Reactivos
        public int totalQuestion { get; set; }
        //Número Actividades Programadas 
        public int ActivityScheduled { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //grade curso
        public string Grade { get; set; }

        public List<UserCCTDTO> UserCCT { get; set; }

        public float PHomework { get; set; }
        public float PExam { get; set; }
        //Porcentaje de Asintencia
        public float PAssistance { get; set; }
        //Porcentaje de actividad
        public float PActivity { get; set; }
        public string CCT { get; set; }
        public int userid { get; set; }
    }
}
