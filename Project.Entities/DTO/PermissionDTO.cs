﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class PermissionDTO
    {
        public int Id               { get; set; }

        public string Module        { get; set; }

        public string Description   { get; set; }

        public bool Active          { get; set; }

        public bool Assigned        { get; set; }

        public int? ParentId        { get; set; }

        public string Icon          { get; set; }

        public string Url           { get; set; }

        public bool ShowInMenu      { get; set; }
    }
}
