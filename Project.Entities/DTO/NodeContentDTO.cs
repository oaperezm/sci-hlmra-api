﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class NodeContentDTO
    {
        public int Id                       { get; set; }

        public int? ContentId               { get; set; }

        public int? NodeId                  { get; set; }

        public string UrlContent            { get; set; }

        public int? ContentTypeId           { get; set; }

        public string ContentType           { get; set; }

        public int? FileTypeId              { get; set; }

        public string FileType              { get; set; }

        public string Name                  { get; set; }

        public bool? Active                 { get; set; }
        
        public bool? IsPublic               { get; set; }

        public string Thumbnails            { get; set; }

        public string ContentResourceType   { get; set; }

        public int? ContentResourceTypeId   { get; set; }

        public string Description           { get; set; }

    }
}
