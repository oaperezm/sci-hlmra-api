﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ConversationDTO
    {
        /// <summary>
        /// Identificador del grupo
        /// </summary>
        public int ChatGroupId         { get; set; }

        /// <summary>
        /// Nombre del usuario quel mensaje
        /// </summary>
        public string ReceiverUserName { get; set; }

        /// <summary>
        /// Identificador del usuario de la conversación
        /// </summary>
        public int ReceiverUserId      { get; set; }

        /// <summary>
        /// Ultimo mensaje
        /// </summary>
        public string LastMessage      { get; set; }

        /// <summary>
        /// Fecha del ultimo mensaje
        /// </summary>
        public DateTime? LastMessageDate  { get; set; }

        /// <summary>
        /// Si aún no a leido el último mensaje
        /// </summary>
        public bool IsRead             { get; set; }

        /// <summary>
        /// Avatar del receptor
        /// </summary>
        public string ReceiverAvatar    { get; set;  }

        /// <summary>
        /// Correo electronico del usuario
        /// </summary>
        public string ReceiverEmail     { get; set; }

        /// <summary>
        /// Determina si el ultimo mensaje fue enviado por el usuario
        /// </summary>
        public bool Owner { get; set; }
    }
}
