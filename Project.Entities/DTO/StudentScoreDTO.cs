﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class StudentScoreDTO
    {
        public int Id { get; set; }

        public float Homework { get; set; }

        public float Exam { get; set; }

        public float Assistance { get; set; }

        public float Activity { get; set; }

        public float Score { get; set; }

        public int NumberPartial { get; set; }

        public int? SchedulingPartialId { get; set; }

        public int Points { get; set; }

        public int Status { get; set; }

        public int ExamScheduleId { get; set; }

        public DateTime RegisterDate { get; set; }

        public string Comment { get; set; }

        public int StudentGroupId { get; set; }

        public virtual StudentGroup StudentGroup { get; set; }

        public int StudentId { get; set; }

        public virtual Student Student { get; set; }

        public int UserId { get; set; }

        public string PartialPeriod { get; set; }

        public string PartialDecription { get; set; }

        public string StatusHomework { get; set; }

        public string StatusExam { get; set; }

        public string StatusAssistance { get; set; }

        public string StatusActivity { get; set; }
    }
}
