﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ResumeTestDTO
    {
        public ExamSchedule ExamSchedule   { get; set; }
        public StudentGroup Group          { get; set; }
        public TeacherExam TeacherExamn    { get; set; }

    }
}
