﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class HomeWorkScoreDTO
    {
        /// <summary>
        /// Identificador de la respuesta de la tarea
        /// </summary>
        public int Id           { get; set; }

        /// <summary>
        /// Identificador de la tarea
        /// </summary>
        public int HomeWorkId   { get; set; }

        /// <summary>
        /// Calificación de la tarea
        /// </summary>
        public string Score     { get; set; }

        /// <summary>
        /// Comentario por parte del maestro
        /// </summary>
        public string Comment   { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        public int UserId       { get; set; }

        /// <summary>
        /// Nombre completo del alumno
        /// </summary>
        public string FullName  { get; set; }

        /// <summary>
        /// Listado de archivos de la tarea
        /// </summary>
        public List<HomeWorkAnswerFile> Files { get; set; }

        public bool HasHomwRowk { get; set; }
        /// <summary>
        /// Indica si el alumno puede agregar nuevas actividad en caso que haya sido indicado que nó cumplió con la actividad
        /// </summary>
        public bool AddNewAnswers { get; set; }

        /// <summary>
        /// Indica si el alumno cumplió con la actividad
        /// </summary>
        public bool Delivered { get; set; }
    }
}
