﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class MessageDTO
    {
        /// <summary>
        /// Identificador del mensaje
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Contenido del mensaje
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Identificador del chat al que pertenece el mensaje
        /// </summary>
        public int ChatGroupId { get; set; }

        /// <summary>
        /// Orden de los mensajes
        /// </summary>
        public long MessageSequence { get; set; }

        /// <summary>
        /// Determina si fue leido o no el mensaje
        /// </summary>
        public bool Read { get; set; }

        /// <summary>
        /// Usuario que recibe el mensaje
        /// </summary>
        public ContactDTO ReceiverUser { get; set; }

        /// <summary>
        /// Usuario que envia el mensaje
        /// </summary>
        public ContactDTO SenderUser { get; set; }

        /// <summary>
        /// Fecha del emnsaje
        /// </summary>
        public DateTime SenderDate { get; set; }

        /// <summary>
        /// Determina si el mensaje fue enviado por el usuario actual
        /// </summary>
        public bool Owner { get; set; }
    }
}
