﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ContentDTO
    {
        public int Id                                   { get; set; }

        public string Name                              { get; set; }

        public string UrlContent                        { get; set; }

        public bool Active                              { get; set; }

        public bool IsPublic                            { get; set; }

        public int ContentTypeId                        { get; set; }

        public int FileTypeId                           { get; set; }

        public string ContentTypeDesc                   { get; set; }

        public string Thumbnails                        { get; set; }

        public string FileTypeDesc                      { get; set; }

        public Area Area                                { get; set; }

        public FormativeField FormativeField            { get; set; }

        public Purpose Purpose                          { get; set; }

        public Block Block                              { get; set; }

        public TrainingField TrainingField              { get; set; }

        public ContentResourceType ContentResourceType  { get; set;  }
        
        public CountContentDTO TotalContent             { get; set; }
        
        public string Description                       { get; set; }

        public int? PurposeId                           { get; set; }

        public int? Visits                              { get; set; }

        public DateTime RegisterDate                    { get; set; }

        public EducationLevel EducationLevel            { get; set; }

        public AreaPersonalSocialDevelopment AreaPersonalSocialDevelopment { get; set; }

        public AreasCurriculumAutonomy AreasCurriculumAutonomy { get; set; }

        public Axis Axis                                { get; set; }

        public Keylearning Keylearning                  { get; set; }

        public Learningexpected Learningexpected        { get; set; }

        public bool IsRecommended                       { get; set; }

        public string BlockDescription                  { get; set; }

        public List<NodeContent> Content                { get; set; } = new List<NodeContent>();

        public List<Tag> Tags                           { get; set; } = new List<Tag>();

    }
}
