﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class PartialRecordsDTO
    {
        public string PartialDescription { get; set; }
        public string PartialPeriod { get; set; }
        public int MaximumElementsCount { get; set; }
        public int idSchedulingPartial { get; set; }
    }
}
