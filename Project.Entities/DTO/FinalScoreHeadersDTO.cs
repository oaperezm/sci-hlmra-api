﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class FinalScoreHeadersDTO
    {
        public string CCT { get; set; }
        public string School { get; set; }
        public string Student { get; set; }
        public string Level { get; set; }
        public string Subject { get; set; }
        public int? Grade { get; set; }
        public string Group { get; set; }
        public string Turn { get; set; }
        public string SchoolCycle { get; set; }
        public string Professor { get; set; }
    }
}
