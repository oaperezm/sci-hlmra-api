﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class GroupStudentScoreDTO
    {
        public int Id                           { get; set; }
        public int StudentGroupId               { get; set; }
        public string StudentGroupDescription   { get; set; }
        public int StudentStatusId              { get; set; }
        public string StudentStatusDescription  { get; set; }
        public DateTime InvitationDate          { get; set; }
        public DateTime? CheckDate              { get; set; }
        public int UserId                       { get; set; }
        public string UserName                  { get; set; }
        public string UserMail                  { get; set; }
        public string Score                     { get; set; }
        public string Comments                  { get; set; }
        public int TotalQuestions               { get; set; }
        public int TotalCorrect                 { get; set; }
        public int? TestId                      { get; set; }
        public bool HasScore                    { get; set; }
        public bool IsAnswered                  { get; set; }
        public decimal TotalAnswerScore         { get; set; }
        public int MaximumExamScore             { get; set; }
        public int WeightingId                  { get; set; }
        public string WeightingDescription      { get; set; }
        public string WeightingUnit             { get; set; }
    }
}
