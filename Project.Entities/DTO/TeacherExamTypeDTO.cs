﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class TeacherExamTypeDTO
    {
        public int Id             { get; set; }

        public string Description { get; set; }
    }
}
