﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class AuthorDTO
    {
        public int Id          { get; set; }
        public string Name     { get; set; }
        public string LastName { get; set; }
        public string Comment  { get; set; }
        public string FullName { get {return $"{ Name} { LastName}"; }  }
    }
}
