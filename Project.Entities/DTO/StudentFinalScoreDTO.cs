﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class StudentFinalScoreDTO
    {
        public int Id { get; set; }
        public int StudentGroupId { get; set; }
        public string StudentGroupDescription { get; set; }
        public int StudentId { get; set; }
        public string StudenName { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public List<StudentScoreDTO> StudentScores { get; set; }
        public Dictionary<string, string> Errors { get; set; }
    }
}
