﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class TeacherExamDTO
    {
        public int Id                              { get; set; }

        public bool Active                         { get; set; }

        public string Description                  { get; set; }

        public int UserId                          { get; set; }

        public int TotalQuestions                  { get; set; }

        public TeacherExamTypeDTO TeacherExamTypes { get; set; }

        public WeightingDTO Weighting { get; set; }

        public bool IsAutomaticValue { get; set; }

        public int MaximumExamScore { get; set; }
    }
}
