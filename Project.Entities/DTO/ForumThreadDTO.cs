﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ForumThreadDTO
    {
        public int Id                   { get; set; }
        
        public string Title             { get; set; }

        public string CourseSubject     { get; set; }

        public string Description       { get; set; }

        public string CourseName        { get; set; }

        public List<string> Groups      { get; set; }

        public DateTime? InitDate       { get; set; }

        public DateTime? EndDate        { get; set; }

        public bool Active              { get; set; }

        public ForumPostDTO LastPost    { get; set; }

        public int? TotalPosts          { get; set; }

        public List<ForumPostDTO> Posts { get; set; }
            
        public ContactDTO Creator       { get; set; }

        public int Views                { get; set; }

        public int Votes                { get; set; }

        public bool Owner               { get; set; }
    }
}
