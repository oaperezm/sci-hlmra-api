﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class CountContentDTO
    {
        public int TotalVideo    { get; set; }

        public int TotalAudio    { get; set; }

        public int TotalImage    { get; set; }

        public int TotalDocument { get; set; }

        public int Total         { get { return TotalVideo + TotalAudio + TotalImage + TotalDocument; } }

    }
}
