﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class PublishingProfileDTO
    {
        public int Id               { get; set; }

        public string Description   { get; set; }

        public bool Active          { get; set; }

        public List<NodeDTO> Nodes  { get; set; } = new List<NodeDTO>();
    }
}
