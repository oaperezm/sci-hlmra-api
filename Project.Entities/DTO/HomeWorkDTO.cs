﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class HomeWorkDTO
    {
        public int Id                            { get; set; }

        public DateTime ScheduledDate            { get; set; }

        public string Name                       { get; set; }

        public string Description                { get; set; }

        public DateTime RegisterDate             { get; set; }

        public int StudentGroupId                { get; set; }

        public virtual StudentGroup StudentGroup { get; set; }

        public bool Qualified                    { get; set; }

        public List<TeacherResources> Files      { get; set; } = new List<TeacherResources>();

        public string Score                      { get; set; }

        public string Comment                    { get; set; }

        public DateTime OpeningDate              { get; set; }

        public bool TypeOfQualification           { get; set; }

        public int AssignedQualification         { get; set; }

        public int SchedulingPartialId           { get; set; }

        public string SchedulingPartialDescription { get; set; }

        public bool delivered { get; set; }

        public bool addNewAnswers { get; set; }
        /// <summary>
        /// tipo de tarea 
        /// </summary>
        public bool TypeScore { get; set; }

        public int TotalAnswers { get; set; }
        // Verifica si existen tareas contestadas
        public bool FileFound { get; set; }

       
    }
}
