﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class UserDTO
    {
        public int Id            { get; set; }

        public bool Active       { get; set; }

        public DateTime Birthday { get; set; }

        public string Email      { get; set; }

        public string FullName    { get; set; }

        public string Gender      { get; set; }

        public string Institution { get; set; }

        public Role Role          { get; set; }

        public string Password    { get; set; }

        public int StateId        { get; set; }

        public string Key         { get; set; }

        public int CityId         { get; set; }
        // CAMBIO REPORTE
        //public State State { get; set; }
        // CAMBIO REPORTE
        //public DateTime RegisterDate { get; set; }
        // CAMBIO REPORTE
        //public List<UserCCTDTO> UserCCT { get; set; } = new List<UserCCTDTO>();

    }
}
