﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ContactDTO
    {
        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int? Id         { get; set; }

        /// <summary>
        /// Nombre completo del usuario
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Correo del usuario
        /// </summary>
        public string Email    { get; set; }

        /// <summary>
        /// Imagen dle usuario
        /// </summary>
        public string Avatar   { get; set; }

        /// <summary>
        /// Rol del contacto
        /// </summary>
        public string Rol      { get; set; }
    }
}
