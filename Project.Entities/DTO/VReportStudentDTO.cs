﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities.DTO
{
    public class VReportStudentDTO
    {
        public int Id { get; set; }
        //public string cct { get; set; }
        //Id del profesor
        public int TeacherId { get; set; }
        //Nombre del profesor
        public string Teacher { get; set; }
        //Nombre del alumno
        public string Student { get; set; }
        //Email del alumno
        public string StudentEmail { get; set; }
        //Nombre de la Escuela
        public string Institution { get; set; }
        //userId del alumno
        public int StudentUserId { get; set; }
        //Id del grupo de estudio
        public int StudentGroupId { get; set; }
        //Nombre del grupo de estudio
        public string StudentGroupDescription { get; set; }
        //Id del curso
        public int CourseId { get; set; }
        //Materia asignada al curso del alumno
        public string Subject { get; set; }
        //Nivel educativo
        public string Level { get; set; }
        //Días transcurridos
        public int DaysPassed { get; set; }
        //Total de asintencia
        public int totalAttendances { get; set; }

        public DateTime CheckDate { get; set; }

        public float PHomework { get; set; }
        public float PExam { get; set; }
        //Porcentaje de Asintencia
        public float PAssistance { get; set; }
        //Porcentaje de actividad
        public float PActivity { get; set; }

        //grade curso
        public string Grade { get; set; }
        //public List<UserCCTDTO>   UserCCT { get; set; }
        public string CourseName { get; set; }

        public string UserCCT { get; set; }
        public virtual StudentFinalScoreDTO studentscore { get; set; }
        



    }
}
