﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities.DTO
{
    public class ActivityScoreDTO
    {
        /// <summary>
        /// Identificador de la respuesta de la actividad
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la actividad
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        /// Calificación de la actividad
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Comentario por parte del maestro
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Nombre completo del alumno
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Listado de archivos de la actividad
        /// </summary>
        
        public List<ActivityFile> Files { get; set; }

        public bool HasHomwRowk { get; set; }

        public List<ActivityAnswer> ActivityAnswers { get; set; }
        public bool Delivered { get; set; }
        public bool AddNewAnswers { get; set; }
        
    }
}
