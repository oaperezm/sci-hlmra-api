﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ForumPostDTO
    {
        /// <summary>
        /// Identificador de la respuesta
        /// </summary>
        public int? Id                { get; set; }

        /// <summary>
        /// Fecha de creación
        /// </summary>
        public DateTime? CreateDate   { get; set; }

        /// <summary>
        /// Usuario que creó la respuesta
        /// </summary>
        public ContactDTO Creator    { get; set; }

        /// <summary>
        /// Titulo de la respuesta
        /// </summary>
        public string Title          { get; set; }

        /// <summary>
        /// Descripción de la respuesta
        /// </summary>
        public string Description    { get; set; }

        /// <summary>
        /// Contestación de una respeusta existente
        /// </summary>
        public string Replay         { get; set; }

        /// <summary>
        /// Determina si es la mejor respuesta
        /// </summary>
        public bool BestAnswer       { get; set; }

        /// <summary>
        /// Total de votos
        /// </summary>
        public int Votes              { get; set; }

        /// <summary>
        /// Vistas del post
        /// </summary>
        public int Views              { get; set; }

        /// <summary>
        /// Si el usuario solicitante es propietario del comentario
        /// </summary>
        public bool Owner             { get; set; }

        /// <summary>
        /// Determina si puedes darle like al post
        /// </summary>
        public bool? CanLike           { get; set; }
    }
}
