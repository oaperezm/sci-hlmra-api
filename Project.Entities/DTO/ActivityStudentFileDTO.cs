﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class ActivityStudentFileDTO
    { 
        public int Id { get; set; }

        public string Url { get; set; }

        public string Comment { get; set; }

        public DateTime RegisterDate { get; set; }

        public int UserId { get; set; }

        public int ActivitiesId { get; set; }
    }
}
