﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class FinalScoreFileRequirementsDTO 
    {
        public int studentGroupId { get; set; }
        public string turn { get; set; }
        public string schoolCycle { get; set; }
    }
}
