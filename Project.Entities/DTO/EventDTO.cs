﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class EventDTO
    {
        public int Id                { get; set; }
        public string Name           { get; set; }
        public DateTime  DateEvent   { get; set; }
        public DateTime? EndEvent    { get; set; }
        public string Description    { get; set; }
        public int Duration          { get; set; }
        public string InitTime       { get; set; }

        public dynamic Teacher       { get; set; }
        public dynamic Course        { get; set; }
        public dynamic Group         { get; set; }
        public dynamic Files         { get; set; }
        public int EventType         { get; set; }
        
    }   
}
