﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.DTO
{
    public class NodeInventoryDTO
    {
        public int Id               { get; set; }

        public string Name          { get; set; }

        public string Description   { get; set; }

        public string UrlImage      { get; set; }

        public bool Active          { get; set; }

        public int ParentId         { get; set; }

        public int NodeTypeId       { get; set; }

        public CountContentDTO TotalContent     { get; set; }
    }
}
