﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Push
{
    public class NotificationBody
    {
        /// <summary>
        /// Titulo de la notificación
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Mensaje de la notifiación
        /// </summary>
        public string Body  { get; set; }

        /// <summary>
        /// Datos extras de la notificación
        /// </summary>
        public dynamic Data { get; set; }
    }
}
