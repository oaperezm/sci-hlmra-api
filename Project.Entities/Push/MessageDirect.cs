﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Push
{
    public class MessageDirect: Message
    {
        [JsonProperty(PropertyName = "registration_ids")]
        public string[] RegistrationIds { get; set; }
    }
}
