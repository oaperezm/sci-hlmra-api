﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Push
{
    public class Message
    {
        [JsonProperty(PropertyName = "notification")]
        public Notification Notification { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data               { get; set; }
    }
}
