﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Push
{
    public class MessageTopic: Message
    {
        [JsonProperty(PropertyName = "to")]
        public string To { get; set; }
    }
}
