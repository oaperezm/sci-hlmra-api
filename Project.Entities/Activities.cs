﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Activity : IEntityBase
    {
        public int Id { get; set; }

        [ForeignKey("SchedulingPartial")]
        public int SchedulingPartialId { get; set; }
        public virtual SchedulingPartial SchedulingPartial { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime OpeningDate { get; set; }

        public DateTime ScheduledDate { get; set; }

        public DateTime RegisterDate { get; set; }


        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        //[ForeignKey("StudentGroup")]
        //public int StudentGroupId { get; set; }

        [ForeignKey("StudentGroup")]
        public int StudentGroupId { get; set; }
        public virtual StudentGroup StudentGroup { get; set; }


        // se agregan nuevas columnas a la tabla
        public bool Qualified { get; set; }

        public bool? TypeOfQualification { get; set; }

        public int? AssignedQualification { get; set; }

        /// <summary>
        /// Listado de archivos de la actividad
        /// </summary>
        public virtual ICollection<TeacherResources> Files { get; set; } = new List<TeacherResources>();

        /// <summary>
        /// Listado de respuesta de la actividad
        /// </summary>


        public virtual ICollection<ActivityAnswer> ActivityAnswers { get; set; } = new List<ActivityAnswer>();
        /// <summary>
        /// tipo de calificación
        /// </summary>
        public bool? TypeScore { get; set; }
    }
}
