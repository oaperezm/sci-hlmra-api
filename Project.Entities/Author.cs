﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities
{
    public class Author : IEntityBase
    {
        public int Id          { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string Comment  { get; set; }

        public virtual ICollection<Book> Books { get; set; } = new List<Book>();
    }
}
