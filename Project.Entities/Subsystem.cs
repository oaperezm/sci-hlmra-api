﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities
{
    public class Subsystem : IEntityBase
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
