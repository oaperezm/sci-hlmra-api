﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Project.Entities
{
    public class Content : IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id            { get; set; }

        /// <summary>
        /// Nombre del contenido
        /// </summary>
        public string Name        { get; set; }

        /// <summary>
        /// Descripción del contenido
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Url donde se encuentra el contenido
        /// </summary>
        public string UrlContent { get; set; }

        /// <summary>
        /// Determina si el contenido es publico
        /// </summary>
        public bool IsPublic     { get; set; }

        /// <summary>
        /// Determina si el contenido esta activo 
        /// </summary>
        public bool Active       { get; set; }

        /// <summary>
        /// Imagen miniatura para el video que se optiene de vimeo
        /// </summary>
        public string Thumbnails { get; set; }

        /// <summary>
        /// Tipo de contenido
        /// </summary>
        [ForeignKey("ContentType")]
        public int ContentTypeId { get; set; }
        public virtual ContentType ContentType { get; set; }

        /// <summary>
        /// Tipo de archivo
        /// </summary>
        [ForeignKey("FileType")]
        public int FileTypeId { get; set; }
        public virtual FileType FileType { get; set; }
        
        /// <summary>
        /// Campo formativo
        /// </summary>
        [ForeignKey("FormativeField")]
        public int? FormativeFieldId { get; set; }
        public virtual FormativeField FormativeField { get; set; }

        /// <summary>
        /// Area a la que pertenice
        /// </summary>
        [ForeignKey("Area")]
        public int? AreaId { get; set; }
        public virtual Area Area { get; set; }

        /// <summary>
        /// Proposito
        /// </summary>
        [ForeignKey("Purpose")]
        public int? PurposeId { get; set; }
        public virtual Purpose Purpose { get; set; }

        /// <summary>
        /// Campo de entrenamiento
        /// </summary>
        [ForeignKey("TrainingField")]
        public int? TrainingFieldId { get; set; }
        public virtual TrainingField TrainingField { get; set; }

        /// <summary>
        /// Tipo de contenido 
        /// </summary>
        [ForeignKey("ContentResourceType")]
        public int? ContentResourceTypeId { get; set; }
        public virtual ContentResourceType ContentResourceType { get; set; }

        /// <summary>
        /// Sistema al cual pertenece
        /// </summary>
        public int SystemId             { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System    { get; set; }

        /// <summary>
        /// Bloque al cual pertenece
        /// </summary>
        public int? BlockId              { get; set; }
        [ForeignKey("BlockId")]
        public virtual Block Bloack     { get; set; }
        
        /// <summary>
        /// Visitas realizadas a un contenido
        /// </summary>
        public int Visits               { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate    { get; set; }

        /// <summary>
        /// Determina si es un contenido recomendado
        /// </summary>
        public bool IsRecommended       { get; set; }

        /// <summary>
        /// Campo nivel de educacion 
        /// </summary>
        [ForeignKey("EducationLevel")]
        public int? EducationLevelId { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }

        /// <summary>
        /// Campo area de desarrollo per. soc. 
        /// </summary>
        [ForeignKey("AreaPersonalSocialDevelopment")]
        public int? AreaPersonalSocialDevelopmentId { get; set; }
        public virtual AreaPersonalSocialDevelopment AreaPersonalSocialDevelopment { get; set; }

        /// <summary>
        /// Campo ambito de autonomia curricular 
        /// </summary>
        [ForeignKey("AreasCurriculumAutonomy")]
        public int? AreasCurriculumAutonomyId { get; set; }
        public virtual AreasCurriculumAutonomy AreasCurriculumAutonomy { get; set; }

        /// <summary>
        /// Campo eje/Ambito 
        /// </summary>
        [ForeignKey("Axis")]
        public int? AxisId { get; set; }
        public virtual Axis Axis { get; set; }

        /// <summary>
        /// Campo de aprendizaje esperado
        /// </summary>
        [ForeignKey("Keylearning")]
        public int? KeylearningId { get; set; }
        public virtual Keylearning Keylearning { get; set; }

        /// <summary>
        /// Campo de aprendizaje esperado
        /// </summary>
        [ForeignKey("Learningexpected")]
        public int? LearningexpectedId { get; set; }
        public virtual Learningexpected Learningexpected { get; set; }

        /// <summary>
        /// Listado de nodos asignados 
        /// </summary>
        public virtual ICollection<NodeContent> NodeContents { get; set; } = new List<NodeContent>();

        /// <summary>
        /// Listados de tag relacionados en contenido
        /// </summary>
        public virtual ICollection<Tag> Tags                 { get; set; } = new List<Tag>();

    }
}
