﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class News : IEntityBase
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id                   { get; set; }

        /// <summary>
        /// Titulo de la noticia
        /// </summary>
        public string Title             { get; set; }

        /// <summary>
        /// Descripción de la noticia
        /// </summary>
        public string Description       { get; set; }

        /// <summary>
        /// Fecha de registro de la noticia
        /// </summary>
        public DateTime RegisterDate    { get; set; }

        /// <summary>
        /// Imagen de la noticia
        /// </summary>
        public string UrlImage          { get; set; }

        /// <summary>
        /// Determina si se publica en la página principal o no
        /// </summary>
        public bool Published           { get; set; }

        /// <summary>
        /// Identificador del sistema
        /// </summary>
        public int SystemId             { get; set; }
    }
}
