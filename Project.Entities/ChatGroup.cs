﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ChatGroup: IEntityBase
    {
        /// <summary>
        /// Identificador del grupo
        /// </summary>
        public int Id                   { get; set; }

        /// <summary>
        /// Identificador del usuario qeu creo el grupo
        /// </summary>
        public int CreateUserId          { get; set; }
        [ForeignKey("CreateUserId")]
        public virtual User CreateUser   { get; set; }

        /// <summary>
        /// Fecha en la que fue creado el grupo
        /// </summary>
        public DateTime CreateDate        { get; set; }

        /// <summary>
        /// Ultima actualización del grupo
        /// </summary>
        public DateTime UpdateDate         { get; set; }

        public virtual ICollection<ChatGroupUser> Users  { get; set; } = new List<ChatGroupUser>();
        public virtual ICollection<ChatMessage> Messages { get; set; } = new List<ChatMessage>();
    }
}
