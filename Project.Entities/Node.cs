﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project.Entities
{
    public class Node : IEntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UrlImage { get; set; }
        public int ParentId { get; set; }
        public bool Active { get; set; }
        public int Depth { get; set; }
        public int Pathindex { get; set; }
        public string Numericalmapping { get; set; }
        public bool Public { get; set; }


        public int Order { get; set; }


        public int SystemId                 { get; set; }
        [ForeignKey("SystemId")]
        public System System                { get; set; }

        [ForeignKey("Book")]
        public int? BookId                  { get; set; }
        public virtual Book Book            { get; set; }

        [ForeignKey("NodeType")]
        public int NodeTypeId               { get; set; }
        public virtual NodeType NodeType    { get; set; }

        [NotMapped]
        public int NodeAccessTypeId         { set; get; }

        [NotMapped]
        public CountContentDTO TotalContent             { set; get; }

        public virtual ICollection<PublishingProfile> PublishingProfiles { get; set; } = new List<PublishingProfile>();
        public virtual ICollection<Glossary> Glossaries                  { get; set; } = new List<Glossary>();
        public virtual ICollection<User> Users                           { get; set; } = new List<User>();
        public virtual ICollection<NodeContent> Contents                 { get; set; } = new List<NodeContent>();

    }
}
