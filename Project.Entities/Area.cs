﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class Area : IEntityBase
    {
        public int Id { get; set; }
        public string Description { get; set; }


    }
}
