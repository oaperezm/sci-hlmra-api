﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Weighting : IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id                 { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description     { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate  { get; set; }

        /// <summary>
        /// Fecha de actualización del registro
        /// </summary>        
        public DateTime? UpdateDate   { get; set; }
    }
}
