﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class CourseClass : IEntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }


        public int CourseSectionId { get; set; }
        [ForeignKey("CourseSectionId")]
        public virtual CourseSection CourseSection         { get; set; }

        public virtual ICollection<TeacherResources> Files { get; set; } = new List<TeacherResources>();


    }
}
