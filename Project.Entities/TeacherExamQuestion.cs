﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    /// <summary>
    /// Preguntas del exámen generado por el maestro
    /// </summary>
    public class TeacherExamQuestion : IEntityBase
    {
        /// <summary>
        /// Identificador principal de la pregunta
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador del examén del maestro
        /// </summary>        
        public int TeacherExamId { get; set; }

        /// <summary>
        /// Identificador de la pregunta 
        /// </summary>        
        public int QuestionId { get; set; }

        /// <summary>
        /// Valor de la pregunta 
        /// </summary>        
        public Decimal Value { get; set; }

        /// <summary>
        /// Valor editado por el Profesor
        /// </summary>
        public bool? UserEdited { get; set; }

        [ForeignKey("TeacherExamId")]
        public virtual TeacherExam TeacherExam { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }

    }
}
