﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ForumPost:IEntityBase
    {
        /// <summary>
        /// Identificador principal de los post del foro
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador del hilo al que pertenece el post
        /// </summary>
        public int ForumThreadId { get; set; }

        /// <summary>
        /// Objeto del hilo al que pertenece el post
        /// </summary>
        [ForeignKey("ForumThreadId")]
        public virtual ForumThread ForumThread { get; set; }

        /// <summary>
        /// Identificador del usuario que realizo el post
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Objeto del usuario que generó el post
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Fecha que se realiza el registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Titulo de la contestación
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Descripción del post
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Descripción de la contestación
        /// </summary>
        public string Replay { get; set; }

        /// <summary>
        /// Determina si es la mejor respuesta
        /// </summary>
        public bool BestAnswer { get; set; }

        /// <summary>
        /// Votos del post
        /// </summary>
        public int Votes   { get; set; }

        /// <summary>
        /// Obtener todos los votos del post para el usuario
        /// </summary>
        public virtual ICollection<ForumPostVote> ForumPostVotes { get; set; } = new List<ForumPostVote>();
    }
}
