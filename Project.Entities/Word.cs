﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Word:IEntityBase
    {
        public int Id                 { get; set; }
        public string Name            { get; set; }
        public string Description     { get; set; }
        public DateTime RegisterDate  { get; set; }
        public DateTime? UpdateDate   { get; set; }
        public bool Active            { get; set; }
        
        [ForeignKey("Glossary")]
        public int GlossaryId         { get; set; }
        public Glossary Glossary      { get; set; }
    }
}
