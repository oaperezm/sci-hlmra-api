﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class NotificationMessages: IEntityBase
    {
        /// <summary>
        /// Identificador unico
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Modulo donde se ejecutara la notifiacación
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// Accción que dispara la notificación
        /// </summary>
        public string Action   { get; set; }

        /// <summary>
        /// Quien genera la notificación
        /// </summary>
        public string Generate { get; set; }

        /// <summary>
        /// Rol que recibe la notificación
        /// </summary>
        public string Receiver { get; set; }

        /// <summary>
        /// Titulo de la notificación
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Cuerpo de la notificación
        /// </summary>
        public string Message { get; set; }
    }
}
