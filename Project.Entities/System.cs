﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class System: IEntityBase
    {
        public int Id                { get; set; }

        public string Description    { get; set; }

        public string Name           { get; set; }

        public string Uri            { get; set; }

        public DateTime RegisterDate { get; set; }

        public bool Active           { get; set; }
    }
}
