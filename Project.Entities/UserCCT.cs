﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class UserCCT: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id              { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId          { get; set; }
        [ForeignKey("UserId")]
        public virtual User User   { get; set; }

        /// <summary>
        /// Temporada
        /// </summary>
        public string Season       { get; set; }

        /// <summary>
        /// Sub-temporada 
        /// </summary>
        public string SubSeason    { get; set; }

        /// <summary>
        /// Identificador del nodo
        /// </summary>
        public int NodeId          { get; set; }
        [ForeignKey("NodeId")]
        public virtual Node Node   { get; set; }

        /// <summary>
        /// Identificador del rol
        /// </summary>
        public int RolId           { get; set; }
        [ForeignKey("RolId")]
        public virtual Role Role    { get; set; }

        /// <summary>
        /// Clave del centro de trabajo
        /// </summary>
        public string CCT          { get; set; }

        /// <summary>
        /// Fecha de inicio
        /// </summary>
        public DateTime StartDate  { get; set; }

        /// <summary>
        /// Fecha de termino 
        /// </summary>
        public DateTime EndDate    { get; set; }



        ///Campos nuevos
        /// <summary>
        //Clasificación de escuela
        /// </summary>
        public string ClasificationSchool { get; set; }

        /// <summary>
        //Entidad
        /// </summary>
        public string State { get; set; }

        /// <summary>
        //Zona
        /// </summary>
        public string Zone { get; set; }


        /// <summary>
        //Control
        /// </summary>
        public string Control { get; set; }


        /// <summary>
        //Promotor
        /// </summary>
        public string Promoter { get; set; }

        


    }
}
