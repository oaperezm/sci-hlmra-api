﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ActivityStudentFile : IEntityBase
    {
        /// <summary>
        /// Identificador principal 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Url del archivo en azure
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Comentario del archivo
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        [ForeignKey("User")]
        public int UserId { get; set; } 
        public virtual User User { get; set; }

        [ForeignKey("Activity")]
        public int ActivitiesId { get; set; }
        public virtual Activity Activity { get; set; } 
    }
}
