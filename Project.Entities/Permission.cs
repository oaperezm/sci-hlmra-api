﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Permission : IEntityBase
    {
        public int Id                { get; set; }
        public string Module         { get; set; }
        public string Description    { get; set; }
        public string Url            { get; set; }
        public string Icon           { get; set; }
        public bool Active           { get; set; }     
        public int? ParentId         { get; set; }
        public bool ShowInMenu       { get; set; }
        public int Order             { get; set; }

        public int SystemId          { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System { get; set; }

        public virtual ICollection<Role> Roles { get; set; } = new List<Role>();
    }
}
