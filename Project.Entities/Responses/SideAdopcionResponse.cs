﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Response
{
    public class SideAdopcionResponse
    {        
        public List<Adopcion> Libros { get; set; }
    }

    public class Adopcion {
        [JsonProperty(PropertyName = "title")]
        public string Title   { get; set; }

        [JsonProperty(PropertyName = "autor")]
        public string Autor   { get; set; }

        [JsonProperty(PropertyName = "codebar")]
        public string Codebar { get; set; }

        [JsonProperty(PropertyName = "isbn")]
        public string ISBN    { get; set; }

    }
}
