﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class SideSeasonResponse
    {
        public List<Season> Seasons { get; set; }
    }

    public class Season
    {
        [JsonProperty(PropertyName = "temporada")]
        public string SeasonName { get; set; }

        [JsonProperty(PropertyName = "subtemporada")]
        public string SubSeason { get; set; }

        [JsonProperty(PropertyName = "fecha_inicial")]
        public string StartDate { get; set; }

        [JsonProperty(PropertyName = "fecha_final")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "nivel")]
        public string Level { get; set; }

        [JsonProperty(PropertyName = "cct")]
        public string Cct { get; set; }

    }
}
