﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Responses
{
    public class Pagination
    {
        public int MazPageSize       { get; set; }
        public int PageSize          { get; set; } = 10;
        public int PageNumber        { get; set; } = 1;
        public int Total             { get; set; }
        public int TotalPage         { get; set; } = 0;
        public bool ShowNextPage     { get; set; }
        public bool ShowPreviousPage { get; set; }
    }
}
