﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Responses
{
    public class SideCCTResponse
    {
        public List<CCT> Seasons { get; set; }
    }
    public class CCT
    {
        [JsonProperty(PropertyName = "temporada")]
        public string SeasonName { get; set; }

        [JsonProperty(PropertyName = "subtemporada")]
        public string SubSeason { get; set; }

        [JsonProperty(PropertyName = "fecha_inicial")]
        public string StartDate { get; set; }

        [JsonProperty(PropertyName = "fecha_final")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "nivel")]
        public string Level { get; set; }

        [JsonProperty(PropertyName = "cct")]
        public string Cct { get; set; }


        //nuevos

        [JsonProperty(PropertyName = "e_mail")]
        public string email { get; set; }

        [JsonProperty(PropertyName = "contacto_activo")]
        public string stateuser { get; set; }

        [JsonProperty(PropertyName = "tiposiide")]
        public string typeside { get; set; }

        [JsonProperty(PropertyName = "siide_nombre")]
        public string SideName { get; set; }
        [JsonProperty(PropertyName = "nombrecct")]
        public string CctName { get; set; }

        [JsonProperty(PropertyName = "nombreentidad")]
        public string EntityName { get; set; }
        [JsonProperty(PropertyName = "nombrecontrol")]
        public string ControlName { get; set; }

        [JsonProperty(PropertyName = "nombre_zona")]
        public string ZoneName { get; set; }

        [JsonProperty(PropertyName = "clasificacion_escuela")]
        public string clasificationschool { get; set; }

        [JsonProperty(PropertyName = "nombre_promotor")]
        public string PromotorName { get; set; }
    }
}
