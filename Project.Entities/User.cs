﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class User: IEntityBase
    {
        public int Id                 { get; set; }
        public string FullName        { get; set; }
        public DateTime Birthday      { get; set; }
        public string Gender          { get; set; }               
        public string Email           { get; set; }
        public string Password        { get; set; }
        public string Key             { get; set; }
        public string Institution     { get; set; }
        public DateTime RegisterDate  { get; set; }
        public DateTime? UpdateDate   { get; set; }
        public bool Active            { get; set; }
        public string RefreshToken    { get; set; }
        public string ClientId        { get; set; }
        public string ProtectedTicket { get; set; }
        // public bool Status            { get; set; }
        public int? LevelId           { get; set; }
        public int? AreaId            { get; set; }
        public int? SubsystemId       { get; set; }
        public int? Semester          { get; set; }
        public string UrlImage        { get; set; }
        //public bool IsResetPasword  { get; set; }

        [ForeignKey("Role")]
        public int RoleId             { get; set; }
        public virtual Role Role      { get; set; }

        public int SystemId           { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System  { get; set; }

        public int StateId { get; set; }
        [ForeignKey("StateId")]
        public virtual State State    { get; set; }

        public virtual ICollection<Node> Nodes                                   { get; set; } = new List<Node>();
        public virtual ICollection<Course> Courses                               { get; set; } = new List<Course>();
        public virtual ICollection<Student> Students                             { get; set; } = new List<Student>();
        public virtual ICollection<PublishingProfileUser> PublishingProfileUsers { get; set; } = new List<PublishingProfileUser>();
        public virtual ICollection<UserDevice> Devices                           { get; set; } = new List<UserDevice>();
        public virtual ICollection<UserNotification> Notifications               { get; set; } = new List<UserNotification>();
        public virtual ICollection<UserCCT> UserCCTs                             { get; set; } = new List<UserCCT>();
    }
}
