﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Views
{
    public class VNodesTotalFileTypesRA: IEntityBase
    {
        public int Id               { get; set; }

        public string Name          { get; set; }

        public string Description   { get; set; }

        public string UrlImage      { get; set; }

        public int ParentId         { get; set; }

        public bool Active          { get; set; }

        public int NodeTypeId       { get; set; }

        public int Videos           { get; set; }

        public int Audios           { get; set; }

        public int Documents        { get; set; }

        public int Images           { get; set; }


        [NotMapped]
        public CountContentDTO TotalContent { set; get; }
    }
}
