﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Views
{
    public class VCoordinatorReport : IEntityBase
    {
        public int Id { get; set; }
        public string cct  { get; set; }
        public string email { get; set; }
        public string userName { get; set; }
        public string nameSchool { get; set; }
        public string level { get; set; }

        public string grade { get; set; }
        public string Subjet { get; set; }
        public string course { get; set; }
        public string group { get; set; }
        public int    totalStudents { get; set; }
        public int    totalActivities { get; set; }
        public int    totalScheduledExams { get; set; }
        public int    totalQuestionsBanks { get; set; }
        public int    totalQuestionTeacher { get; set; }
        public int    totalQuestions { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }


    }
}
