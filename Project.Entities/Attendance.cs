﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Attendance: IEntityBase
    {
        /// <summary>
        /// Identificador de la asistencia
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador del grupo
        /// </summary>
        public int StudentGroupId { get; set; }
        
        /// <summary>
        /// Objeto del grupo
        /// </summary>
        [ForeignKey("StudentGroupId")]
        public virtual StudentGroup StudentGroup { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Objeto del usurio relacionado
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        /// <summary>
        /// Fecha de la asistencia
        /// </summary>
        public DateTime AttendanceDate { get; set; }

        /// <summary>
        /// Determina si el alumno asistio o no a la clase
        /// </summary>
        public bool AttendanceClass { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }
    }
}
