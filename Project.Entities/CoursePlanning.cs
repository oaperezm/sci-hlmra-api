﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
   public class CoursePlanning : IEntityBase
    {
        public int Id { get; set; }
        public float Homework { get; set; }
        public float Exam { get; set; }
        public float Assistance { get; set; }
        public float Activity { get; set; }
        public int NumberPartial { get; set; }
        //public int CourseSectionId { get; set; }
        [ForeignKey("StudentGroup")]
        public int StudentGroupId { get; set; }
        public virtual StudentGroup StudentGroup { get; set; }
        public virtual ICollection<SchedulingPartial> SchedulingPartials { get; set; } = new List<SchedulingPartial>();
    }
}
