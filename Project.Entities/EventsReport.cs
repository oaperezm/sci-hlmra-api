﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
   public class EventsReport: IEntityBase
    {
        public int Id { get; set; }
        //CCT del side
        public string CCT { get; set; }
        //Correo del usuario
        public string Email { get; set; }
        //Nombre del usuario
        public string Username { get; set; }
        //Nombre de la Escuela
        public string InstitutionName { get; set; }

        public string Level { get; set; }

        [ForeignKey("EducationLevel")]
        public int EducationLevelId { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }

        //Nombre del estado
        public string State { get; set; }
        //Clasificación de escuela
        public string ClasificationSchool { get; set; }
        //Nombre de la zona
        public string Zone { get; set; }
        //Nombre del control
        public string Control { get; set; }
        //Nombre del Promotor
        public string Prometer { get; set; }
        //Fecha de ingreso
        public DateTime EntryDate { get; set; }
        //Tiempo de navegación
        public string TimeNavegation { get; set; }
       
         //Nombre del grado
        public string Grade { get; set; }
         //Nombre del Campo formativo
        public string EducationField { get; set; }
        //Nobre de la asignature
        public string Subject { get; set; }
        
        //Nombre del recurso 
        public string ResourcenName { get; set; }

        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        //Tipo de evento
        [ForeignKey("Events")]
        public int EventId { get; set; }
        public virtual Events Events { get; set; }
        //Registros específicos en los que se realizó la acción
        public string Element { get; set; }
        //Comentario
        public string Comment { get; set; }
        //Detalle de comentario
        public string CommentDetail { get; set; }


        // tipo del formato
        //public string Format { get; set; }
        [ForeignKey("FileType")]
        public int FileTypeId { get; set; }
        public virtual FileType FileType { get; set; }

        //Nombre de sección
        //public string Sections { get; set; }
        [ForeignKey("Sections")]
        public int SectionId { get; set; }
        public virtual Sections Sections { get; set; }

        //Nombre de contenido
       
        [ForeignKey("ContentType")]
        public int ContentTypeId { get; set; }
        public virtual ContentType ContentType { get; set; }

    }
}
