﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities
{
    public class PublishingProfile : IEntityBase
    {
        public int Id                { get; set; }

        public string Description    { get; set; }

        public bool Active           { get; set; }

        public int SystemId          { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System { get; set; }

        public virtual ICollection<Node> Nodes { get; set; } = new List<Node>();  
    }
}
