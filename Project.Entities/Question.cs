﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class Question : IEntityBase
    {
        public int Id                               { get; set; }
        public string Content                       { get; set; }        
        public bool Active                          { get; set; }
        public int Order                            { get; set; }
        public string Explanation                   { get; set; }
        public string UrlImage                      { get; set; }

        [NotMapped]
        public string ImageBase64                   { get; set; }

        public int QuestionBankId                   { get; set; }
        [ForeignKey("QuestionBankId")]
        public virtual QuestionBank QuestionBank    { get; set; } 
               
        public int QuestionTypeId                   { get; set; }
        [ForeignKey("QuestionTypeId")]
        public virtual QuestionType QuestionType    { get; set; }
        
        public int? ImageWidth                      { get; set; }

        public int? ImageHeight                     { get; set; }

        public virtual ICollection<Answer> Answers  { get; set; } = new List<Answer>();

        [NotMapped]
        public virtual ICollection<Answer> QuestionAnswers { get; set; } = new List<Answer>();

        [NotMapped]
        public bool IsMultiple                      { get; set; }

    }
}
