﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Project.Entities
{
    public class NodeContents : IEntityBase
    {
        public int Id { get; set; }

        [ForeignKey("Contents")]
        public int ContentId { get; set; }
        public virtual Content Content { get; set; }
        
        [ForeignKey("Nodes")]
        public int NodeId { get; set; }
        public virtual Node Node { get; set; }

        [ForeignKey("Levels")]
        public int LevelId { get; set; }
        public virtual Level Level { get; set; }

        [ForeignKey("Subsystems")]
        public int SubsystemId { get; set; }
        public virtual Subsystem Subsystem { get; set; }

        [ForeignKey("TrainingFields")]
        public int TrainingFieldId { get; set; }
        public virtual TrainingField TrainingField { get; set; }

        [ForeignKey("FormativeFields")]
        public int FormativeFieldId { get; set; }
        public virtual FormativeField FormativeField { get; set; }

        [ForeignKey("Grades")]
        public int GradeId { get; set; }
        public virtual Grade Grade { get; set; }

        [ForeignKey("Blocks")]
        public int BlockId { get; set; }
        public virtual Block Block { get; set; }

        [ForeignKey("Areas")]
        public int AreaId { get; set; }
        public virtual Area Area { get; set; }

        [ForeignKey("Purposes")]
        public int PurposeId { get; set; }
        public virtual Purpose Purpose { get; set; }

        [ForeignKey("Locations")]
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }

                
    }
}
