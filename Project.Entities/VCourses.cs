﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class VCourses : IEntityBase
    {
        public int Id               { get; set; }

        public string Institution   { get; set; }

        public string CourseName    { get; set; }

        public string Publication   { get; set; }

        public string Subject       { get; set; }

        public int NodeId           { get; set; }

        public DateTime? StartDate  { get; set; }

        public DateTime? EndDate    { get; set; }

        public int StudentCount     { get; set; }

        public string Groups        { get; set; }

        public int TeachId          { get; set; }

        

    }
}
