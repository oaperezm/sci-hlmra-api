﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ScheduledEvent: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id              { get; set; }

        /// <summary>
        /// Nombre del evento
        /// </summary>
        public string Name         { get; set; }

        /// <summary>
        /// Descripción del evento
        /// </summary>
        public string Description  { get; set; }

        /// <summary>
        /// Fecha del evento
        /// </summary>
        public DateTime DateEvent  { get; set; }

        /// <summary>
        /// Horario del evento
        /// </summary>
        public string InitTime     { get; set; }

        /// <summary>
        /// Duración del evento en minutos
        /// </summary>
        public int Duration     { get; set; }

        /// <summary>
        /// Fecha y hora de termino del evento
        /// </summary>
        public DateTime? EndEvent { get; set; }

        /// <summary>
        /// Activar estatus del evento
        /// </summary>
        public bool Active { get; set; }
        
        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Usuario que registro el evento
        /// </summary>
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public int CourseId { get; set; }
        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }
               
        /// <summary>
        /// Listado de grupos relacionados
        /// </summary>
        public virtual ICollection<StudentGroup> Groups { get; set; } = new List<StudentGroup>();

    }
}
