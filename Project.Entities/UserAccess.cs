﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class UserAccess: IEntityBase
    {
        /// <summary>
        /// Identificador unico
        /// </summary>
        public int Id                { get; set; }

        /// <summary>
        /// Usuario qeu accede a la aplicación
        /// </summary>
        public int UserId            { get; set; }
        [ForeignKey("UserId")]
        public virtual User User     { get; set; }

        /// <summary>
        /// Fecha y hora de acceso del usuario
        /// </summary>
        public DateTime AccessDate   { get; set; }
        
        /// <summary>
        /// Centros de trabajos a los que pertenece el usuario
        /// </summary>
        public string CCTs           { get; set; }

        /// <summary>
        /// Roles a los que tiene acceso el usuario
        /// </summary>
        public string Roles          { get; set; }
    }
}
