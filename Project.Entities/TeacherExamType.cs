﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    /// <summary>
    /// Tipo de exámen del maestro
    /// </summary>
    public class TeacherExamType: IEntityBase
    {
        /// <summary>
        /// Identificador del typo de examén del maestro
        /// </summary>
        public int Id                   { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description       { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate    { get; set; }

        /// <summary>
        /// Fecha de actualización
        /// </summary>
        public DateTime? UpdateDate     { get; set; }

        /// <summary>
        /// Estatus del registro
        /// </summary>
        public bool Active              { get; set; }
    }
}
