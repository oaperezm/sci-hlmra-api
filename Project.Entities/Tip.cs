﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Tip : IEntityBase
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id                { get; set; }

        /// <summary>
        /// Nombre del tip
        /// </summary>
        public string Name           { get; set; }

        /// <summary>
        /// Descripción del tip
        /// </summary>
        public string Description    { get; set; }

        /// <summary>
        /// Fecha de registro del tip
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Imagen del tip
        /// </summary>
        public string UrlImage       { get; set; }

        /// <summary>
        /// Determina si se publica en la página principal o no
        /// </summary>
        public bool Published        { get; set; }

        /// <summary>
        /// Identificador del sistema
        /// </summary>
        public int SystemId          { get; set; }

    }
}
