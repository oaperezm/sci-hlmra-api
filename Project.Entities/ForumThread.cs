﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ForumThread:IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador del usuaio que genera el
        /// hilo del foro
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Objeto del usuario 
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        /// <summary>
        /// Titulo del hilo del foro
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Descripción del hilo
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Fecha de inicio 
        /// </summary>
        public DateTime? InitDate { get; set; }

        /// <summary>
        /// Fecha de termino
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Identificador principal del curso
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Objeto del curso asociado
        /// </summary>
        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }

        /// <summary>
        /// Activa o desactiva el hilo del foro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Vistas del hilo
        /// </summary>
        public int? Views { get; set; }

        /// <summary>
        /// Listado de grupos asignados
        /// </summary>
        public virtual ICollection<StudentGroup> Groups { get; set; } = new List<StudentGroup>();

        /// <summary>
        /// Listado de post 
        /// </summary>
        public virtual ICollection<ForumPost> Posts     { get; set; } = new List<ForumPost>();

    }
}
