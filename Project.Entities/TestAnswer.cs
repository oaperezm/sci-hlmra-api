﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class TestAnswer: IEntityBase
    {
        public int Id               { get; set; }

        [ForeignKey("Test")]
        public int TestId            { get; set; }
        public virtual Test Test     { get; set; }

        [ForeignKey("Answer")]
        public int AnswerId          { get; set; }
        public virtual Answer Answer { get; set; }

        public string Explanation    { get; set; }

        public DateTime? ResponseDate { get; set; }

        public int? TeacherExamQuestionId { get; set; }

        [ForeignKey("TeacherExamQuestionId")]
        public virtual TeacherExamQuestion TeacherExamQuestion { get; set; }
    }
}
