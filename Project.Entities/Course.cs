﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Course : IEntityBase
    {
        public int Id                   { get; set; }
        public string Name              { get; set; }
        public string Description       { get; set; }
        public string Gol               { get; set; }
        public DateTime StartDate       { get; set; }
        public DateTime EndDate         { get; set; }
        public bool Active              { get; set; }        
        public string Subject           { get; set; }
        public string Institution       { get; set; }

        [ForeignKey("Node")]
        public int NodeId               { get; set; }
        public virtual Node Node        { get; set; }

        [ForeignKey("User")]
        public int UserId               { get; set; }
        public virtual User User        { get; set; }

        public DateTime? RegisterDate   { get; set; }
        public DateTime? UpdateDate     { get; set; }

        public bool Monday              { get; set; }
        public bool Tuesday             { get; set; }
        public bool Wednesday           { get; set; }
        public bool Thursday            { get; set; }
        public bool Friday              { get; set; }
        public bool Saturday            { get; set; }
        public bool Sunday              { get; set; }

        public virtual ICollection<StudentGroup> StudentGroups   { get; set; } = new List<StudentGroup>();

        public virtual ICollection<CourseSection> CourseSections { get; set; } = new List<CourseSection>();

        public virtual ICollection<LinkInterest> LinkInterests   { get; set; } = new List<LinkInterest>();

    }
}
