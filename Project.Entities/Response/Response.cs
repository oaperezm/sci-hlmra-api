﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Response
    {
        /// <summary>
        /// Datos resultantes por la ejecución de la operación
        /// </summary>
        public dynamic Data   { get; set; }

        /// <summary>
        /// Mensaje del resultado de la operación ejecutada
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Determina si fue existosa la operación o no
        /// </summary>
        public bool Success   { get; set; }
        
    }
}
