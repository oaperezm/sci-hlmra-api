﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class QuestionBank : IEntityBase
    {
        public int Id                   { get; set; }
        public string Name              { get; set; }
        public string Description       { get; set; }        
        public DateTime? RegisterDate   { get; set; }
        public DateTime? UpdateDate     { get; set; }
        public bool Active              { get; set; }
        public int NodeId               { get; set; }

        [ForeignKey("NodeId")]
        public virtual Node Node        { get; set; }

        [ForeignKey("User")]
        public int? UserId              { get; set; }        
        public User User                { get; set; }

        /// <summary>
        /// Sistema al cual pertenece
        /// </summary>
        public int SystemId             { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System    { get; set; }

        public virtual ICollection<Question> Questions { get; set; } = new List<Question>();


    }
}
