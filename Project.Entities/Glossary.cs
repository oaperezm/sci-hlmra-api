﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Glossary: IEntityBase
    {
        public int Id                { get; set; }
        public string Name           { get; set; }
        public string Description    { get; set; }
        public bool Active           { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime? UpdateDate  { get; set; }

        [ForeignKey("User")]
        public int UserId            { get; set; }
        public virtual User User     { get; set; }
        
        public virtual ICollection<Word> Words { get; set; } = new List<Word>();
        public virtual ICollection<Node> Nodes { get; set; } = new List<Node>();
        
    }
}
