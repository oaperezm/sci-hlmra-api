﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    /// <summary>
    /// Examén del Maestro
    /// </summary>
    public class TeacherExam: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id                   { get; set; }

        /// <summary>
        /// Identificador del tipo de examen
        /// </summary>
        public int TeacherExamTypeId    { get; set; }

        /// <summary>
        /// Descripción del examen
        /// </summary>
        public string Description       { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public int UserId               { get; set; }

        /// <summary>
        /// Estatus dle registro
        /// </summary>
        public bool Active              { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate    { get; set; }

        /// <summary>
        /// Fecha de actualización del registro
        /// </summary>
        public DateTime? UpdateDate     { get; set; }

        /// <summary>
        /// Identificador de Tipo de Ponderación
        /// </summary>
        public int WeightingId { get; set; }

        [ForeignKey("WeightingId")]        
        public virtual Weighting Weighting { get; set; }

        /// <summary>
        /// Puntuación Máxima
        /// </summary>
        public int MaximumExamScore       { get; set; }

        /// <summary>
        /// Se cálcula automáticamente el valor de cada pregunta
        /// </summary>
        public bool IsAutomaticValue      { get; set; }

        public virtual User User                                                { get; set; }
        public virtual TeacherExamType TeacherExamTypes                         { get; set; }
        public virtual ICollection<TeacherExamQuestion> TeacherExamQuestion     { get; set; } = new List<TeacherExamQuestion>();
        public virtual ICollection<ExamSchedule> ExamSchedules                  { get; set; } = new List<ExamSchedule>();
    }
}
