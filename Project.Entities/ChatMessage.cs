﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ChatMessage: IEntityBase
    {
        /// <summary>
        /// Identificador del mensaje
        /// </summary>
        public int Id                      { get; set; }

        /// <summary>
        /// Contenido dle mensaje
        /// </summary>
        public string Message               { get; set; }

        /// <summary>
        /// Url de contenido del mensaje
        /// </summary>
        public string Url                   { get; set; }

        /// <summary>
        /// Identificador del usuario que envia el mensaje
        /// </summary>
        public int SenderUserId             { get; set; }
        [ForeignKey("SenderUserId")]
        public virtual User SenderUser      { get; set; }

        /// <summary>
        /// Identificador del usuario que recibe el mensaje
        /// </summary>
        public int ReceiverUserId           { get; set; }
        [ForeignKey("ReceiverUserId")]
        public virtual User ReceiverUser    { get; set; }

        /// <summary>
        /// Grupo de mensaje
        /// </summary>
        public int ChatGroupId              { get; set; }
        [ForeignKey("ChatGroupId")]
        public virtual ChatGroup ChatGroup  { get; set; }

        /// <summary>
        /// Indica si el mensaje fue leido
        /// </summary>
        public bool Read                    { get; set; }

        /// <summary>
        /// Indica la sequencia del mensaje que se estará guardando
        /// </summary>
        public long MessageSequence         { get; set; }

        /// <summary>
        /// Fecha que fue enviado el mensaje
        /// </summary>
        public DateTime RegisterDate        { get; set; }
    }
}
