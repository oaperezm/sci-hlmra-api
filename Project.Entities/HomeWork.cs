﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class HomeWork: IEntityBase
    {
        public int Id                           { get; set; }

        public DateTime ScheduledDate           { get; set; }

        public string Name                      { get; set; }

        public string Description               { get; set; }

        public DateTime RegisterDate            { get; set; }

        [ForeignKey("User")]
        public int UserId                       { get; set; }  
        public virtual User User                { get; set; }

        [ForeignKey("StudentGroup")]
        public int StudentGroupId                { get; set; }
        public virtual StudentGroup StudentGroup { get; set; }

        public bool Qualified                    { get; set; }


        // new columns
        public DateTime OpeningDate             { get; set; }

        public bool TypeOfQualification           { get; set; }

        public int AssignedQualification         { get; set; }


        [ForeignKey("SchedulingPartial")]
        public int SchedulingPartialId { get; set; }
        public virtual SchedulingPartial SchedulingPartial { get; set; }

        //public virtual ICollection<SchedulingPartial> schedulePartial { get; set; } = new List<SchedulingPartial>();

        /// <summary>
        /// Listado de archivos de la tarea
        /// </summary>
        public virtual ICollection<TeacherResources> Files { get; set; } = new List<TeacherResources>();
        
        /// <summary>
        /// Listado de respuesta de la tarea
        /// </summary>
        public virtual ICollection<HomeWorkAnswer> Answers { get; set; } = new List<HomeWorkAnswer>();
        /// <summary>
        /// tipo de tarea 
        /// </summary>
        public bool TypeScore { get; set; }

    }
}
