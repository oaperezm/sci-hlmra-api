﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class HomeWorkAnswer: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la tarea relacionada
        /// </summary>
        public int HomeWorkId { get; set; }

        /// <summary>
        /// Objeto de la relación con Tareas
        /// </summary>
        [ForeignKey("HomeWorkId")]
        public virtual HomeWork HomeWork { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Objeto relacionado con el usaurio
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        
        /// <summary>
        /// Calificacion de la tarea
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Comentario del profesor
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Indica si el alumno cumplió con la tarea
        /// </summary>
        public bool Delivered { get; set; } 

        /// <summary>
        /// Indica el parcial para el cual aplica la tarea
        /// </summary>
        public int PartialEvaluation { get; set; } 

        /// <summary>
        /// Indica el estatus de la tarea
        /// </summary>
        public int StatusHomework { get; set; }

        /// <summary>
        /// Indica si el alumno puede agregar nuevas tareas en caso que haya sido indicado que nó cumplió con la tarea
        /// </summary>
        public bool AddNewAnswers { get; set; }

        /// <summary>
        /// Listado de archivos
        /// </summary>
        public virtual ICollection<HomeWorkAnswerFile> HomeWorkAnswerFiles { get; set; } = new List<HomeWorkAnswerFile>();
    }
}
