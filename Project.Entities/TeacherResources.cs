﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class TeacherResources : IEntityBase
    {
        public int Id                            { get; set; }
        public string Name                       { get; set; }
        public string Description                { get; set; }        
        public string Url                        { get; set; }

        [ForeignKey("ResourceType")]
        public int ResourceTypeId                { get; set; }
        public virtual ResourceType ResourceType { get; set; }

        [ForeignKey("User")]
        public int UserId                        { get; set; }
        public virtual User User                 { get; set; }

        public DateTime RegisterDate             { get; set; }

        public virtual ICollection<CourseClass> CourseClasses { get; set; } = new List<CourseClass>();
        public virtual ICollection<HomeWork> HomeWorks { get; set; }        = new List<HomeWork>();
        public virtual ICollection<Activity> Activities { get; set; } = new List<Activity>();
    }
}
