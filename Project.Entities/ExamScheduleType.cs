﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ExamScheduleType: IEntityBase
    {
        /// <summary>
        /// Identificador principal del estatus de la programación de exámenes
        /// </summary>
        public int Id               { get; set; }

        /// <summary>
        /// Descripción del tipo de la programación de exámenes
        /// </summary>
        public string Description   { get; set; }

        /// <summary>
        /// Estatus del tipo de la programación de exámen
        /// </summary>
        public bool Active          { get; set; }

        /// <summary>
        /// Fecha de registro del estatus de la programación de exámen
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Fecha de registro de la actualización de la información de el estatus de exámen
        /// </summary>
        public DateTime? UpdateDate { get; set; }
    }
}
