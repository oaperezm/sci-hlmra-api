﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities
{
    public class VStudents : IEntityBase
    {
        public int Id { get; set; }
        public string Teacher { get; set; }
        public int TeacherId { get; set; }
        public int StudentId { get; set; }
        public string Student { get; set; }
        public string StudentEmail { get; set; }
        public string StudentUrlImage { get; set; }
        public int StudentSystemId { get; set; }
        public string StudentSystem { get; set; }
        public int StudentUserId { get; set; }
        public int StudentStatusId { get; set; }
        public string StudentStatusDescription { get; set; }
        public int StudentGroupId { get; set; }
        public string StudentGroupDescription { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public string CourseSubject { get; set; }
    }
}
