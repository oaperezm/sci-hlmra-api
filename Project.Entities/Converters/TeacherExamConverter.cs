﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class TeacherExamConverter
    {
        public static TeacherExamDTO New( this Entities.TeacherExam entity ) {

            return new TeacherExamDTO
            {
                Id             = entity.Id,
                Active         = entity.Active,
                Description    = entity.Description,
                UserId         = entity.UserId,
                TotalQuestions = entity.TeacherExamQuestion.Count(),
                TeacherExamTypes = new TeacherExamTypeDTO {
                     Id          = entity.TeacherExamTypes != null ? entity.TeacherExamTypes.Id : 0,
                     Description = entity.TeacherExamTypes?.Description
                },
                Weighting = new WeightingDTO {
                       Id = entity.Weighting.Id,
                       Description = entity.Weighting.Description,
                       Unit =  entity.Weighting.Unit
                },
                IsAutomaticValue = entity.IsAutomaticValue,
                MaximumExamScore = entity.MaximumExamScore                
            };
        }
    }
}
