﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class ForumThreadConverter
    {
        public static ForumThreadDTO New(this ForumThread entity, int userId)
        {
            var lastPost = entity.Posts?.LastOrDefault();

            if (lastPost == null)
                lastPost = new ForumPost();

            return new ForumThreadDTO
            {
                Id            = entity.Id,
                Title         = entity.Title,
                Description   = entity.Description,
                CourseName    = entity.Course.Name,
                CourseSubject = entity.Course.Subject,
                Groups        = entity.Course.StudentGroups.Select( g => g.Description).ToList(),
                InitDate      = entity.InitDate,
                EndDate       = entity.EndDate,
                Active        = entity.Active,
                TotalPosts    = entity.Posts?.Count,
                Views         = entity.Views.HasValue ? entity.Views.Value : 0,
                Votes         = entity.Posts.Sum( x => x.Votes),
                Owner         = entity.User.Id == userId,
                LastPost      =  new ForumPostDTO
                {
                    Id           = lastPost?.Id,
                    Title        = lastPost?.Title,
                    Description  = lastPost?.Description,
                    CreateDate   = lastPost?.RegisterDate,
                    BestAnswer   = lastPost.BestAnswer,
                    Views        = lastPost.Votes,
                    Creator      = new ContactDTO
                    {
                        Id       = lastPost?.User?.Id,
                        Avatar   = lastPost?.User?.UrlImage,
                        Email    = lastPost?.User?.Email,
                        FullName = lastPost?.User?.FullName,
                        Rol      = lastPost?.User?.Role.Name
                    }
                },
                Creator = new ContactDTO
                {
                    Id       = entity.User.Id,
                    Avatar   = entity.User.UrlImage,
                    Email    = entity.User.Email,
                    FullName = entity.User.FullName,
                    Rol      = entity.User.Role.Name                    
                }
            };
        }

        public static ForumThreadDTO NewWithDetail(this ForumThread entity, int userId)
        {
            return new ForumThreadDTO
            {
                Id            = entity.Id,
                Title         = entity.Title,
                Description   = entity.Description,
                CourseName    = entity.Course.Name,
                CourseSubject = entity.Course.Subject,
                Groups        = entity.Course.StudentGroups.Select(g => g.Description).ToList(),
                InitDate      = entity.InitDate,
                EndDate       = entity.EndDate,
                Active        = entity.Active,
                TotalPosts    = entity.Posts?.Count,
                Votes         = entity.Views.HasValue ? entity.Views.Value : 0,
                Posts         = entity.Posts?.Select(p => new ForumPostDTO
                {
                    Id          = p.Id,
                    Title       = p.Title,
                    Description = p.Description,
                    CreateDate  = p.RegisterDate,
                    Views       = p.Votes,
                    BestAnswer  = p.BestAnswer,
                    Owner       = p.User.Id == userId,
                    CanLike     = !p.ForumPostVotes?.Any( c => c.UserId == userId),
                    Creator     = new ContactDTO
                    {
                        Id       = p.User?.Id,
                        Avatar   = p.User?.UrlImage,
                        Email    = p.User?.Email,
                        FullName = p.User?.FullName,
                        Rol      = p.User?.Role.Name
                    }
                }).ToList(),
                Creator = new ContactDTO
                {
                    Id       = entity.User.Id,
                    Avatar   = entity.User.UrlImage,
                    Email    = entity.User.Email,
                    FullName = entity.User.FullName,
                    Rol      = entity.User.Role.Name
                }
            };
        }
    }
}
