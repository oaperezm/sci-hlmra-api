﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class RoleConverter
    {
        public static RoleDTO New(this Entities.Role entity) {

            return new RoleDTO
            {
                Id          = entity.Id,
                Active      = entity.Active,
                Description = entity.Description,
                Name        = entity.Name
            };
        }
    }
}
