﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class ContentConverter
    {
        public static ContentDTO NewDTO(this Content entity) {
            
            return new DTO.ContentDTO
            {
                Id              = entity.Id,
                Name            = entity.Name,
                UrlContent      = entity.UrlContent,
                Active          = entity.Active,
                IsPublic        = entity.IsPublic,
                ContentTypeId   = entity.ContentTypeId,
                FileTypeId      = entity.FileTypeId,
                ContentTypeDesc = entity.ContentType?.Description,
                Thumbnails      = entity.Thumbnails,
                FileTypeDesc    = entity.FileType?.Description,
                Description     = entity.Description,
                Visits          = entity.Visits,
                RegisterDate    = entity.RegisterDate,   
                IsRecommended   = entity.IsRecommended,
                Block = new Block {
                     Id          = entity.Bloack != null ? entity.Bloack.Id : 0,
                     Description = entity.Bloack?.Description
                },                
                Area            = new Area
                {
                    Id          = entity.Area != null ? entity.Area.Id : 0,
                    Description = entity.Area?.Description
                },
                FormativeField  = new FormativeField
                {
                    Id = entity.FormativeField != null ? entity.FormativeField.Id : 0,
                    Description = entity.FormativeField?.Description
                },
                Purpose         = new Purpose
                {
                    Id = entity.Purpose != null ? entity.Purpose.Id : 0,
                    Description = entity.Purpose?.Description
                },
                TrainingField   = new TrainingField
                {
                    Id = entity.TrainingField != null ? entity.TrainingField.Id : 0,
                    Description = entity.TrainingField?.Description
                },
                EducationLevel = new EducationLevel
                {
                    Id = entity.EducationLevel != null ? entity.EducationLevel.Id : 0,
                    Description = entity.EducationLevel?.Description
                },

                AreaPersonalSocialDevelopment = new AreaPersonalSocialDevelopment
                {
                    Id = entity.AreaPersonalSocialDevelopment != null ? entity.AreaPersonalSocialDevelopment.Id : 0,
                    Description = entity.AreaPersonalSocialDevelopment?.Description
                },

                AreasCurriculumAutonomy = new AreasCurriculumAutonomy
                {
                    Id = entity.AreasCurriculumAutonomy != null ? entity.AreasCurriculumAutonomy.Id : 0,
                    Description = entity.AreasCurriculumAutonomy?.Description
                },
                Axis = new Axis
                {
                    Id = entity.Axis != null ? entity.Axis.Id : 0,
                    Description = entity.Axis?.Description
                },
                Keylearning = new Keylearning
                {
                    Id = entity.Keylearning != null ? entity.Keylearning.Id : 0,
                    Description = entity.Keylearning?.Description
                },
                Learningexpected = new Learningexpected
                {
                    Id = entity.Learningexpected != null ? entity.Learningexpected.Id : 0,
                    Description = entity.Learningexpected?.Description
                },
                ContentResourceType = new ContentResourceType
                {
                    Id = entity.ContentResourceType != null ? entity.ContentResourceType.Id : 0,
                    Description = entity.ContentResourceType?.Description
                },
                Tags = entity.Tags.Select( t => new Entities.Tag {
                    Id   = t.Id,
                    Name = t.Name
                }).ToList(),
                Content = entity.NodeContents.Select(n => new NodeContent
                {
                    Id        = n.Id,
                    NodeRoute = n.NodeRoute,
                    Node      = new Entities.Node { Id = n.Node != null ? n.Node.Id : 0, Description = n.Node?.Description }
                }).ToList()
            };

        }

        public static ContentDTO NewSimpleDTO(this Content entity)
        {

            return new DTO.ContentDTO
            {
                Id              = entity.Id,
                Name            = entity.Name,
                UrlContent      = entity.UrlContent,
                Active          = entity.Active,
                IsPublic        = entity.IsPublic,
                ContentTypeId   = entity.ContentTypeId,
                FileTypeId      = entity.FileTypeId,
                ContentTypeDesc = entity.ContentType?.Description,
                Thumbnails      = entity.Thumbnails,
                FileTypeDesc    = entity.FileType?.Description,
                Description     = entity.Description,
                PurposeId       = entity.PurposeId,
                Visits          = entity.Visits,
                RegisterDate    = entity.RegisterDate,
                IsRecommended   = entity.IsRecommended,
                BlockDescription = entity.Bloack?.Description

            };

        }
    }
}
