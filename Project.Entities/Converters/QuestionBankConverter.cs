﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class QuestionBankConverter
    {
        public static QuestionBankDTO New(this Entities.QuestionBank entity) {

            return new QuestionBankDTO
            {
                Id           = entity.Id,
                Name         = entity.Name,
                Description  = entity.Description,
                Active       = entity.Active,
                NodeId       = entity.NodeId,
                RegisterDate = entity.RegisterDate,
                UpdateDate   = entity.UpdateDate
            };
        }
    }
}
