﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class GlossaryConverter
    {
        public static Entities.Glossary New(this Entities.Glossary entity, bool withDetail = false) {

            if (withDetail)
            {

                return new Entities.Glossary
                {
                    Id = entity.Id,
                    Description = entity.Description,
                    Name = entity.Name,
                    Active = entity.Active,
                    Nodes = entity.Nodes.Select(n => new Entities.Node
                    {
                        Id = n.Id,
                        Description = n.Description,
                        Name = n.Name,
                        Active = n.Active
                    }).ToList(),
                    Words = entity.Words?.Select(w => new Entities.Word
                    {
                        Id          = w.Id,
                        Description = w.Description,
                        Name        = w.Name,
                        Active      = w.Active,
                        GlossaryId  = w.GlossaryId
                    }).ToList()
                };

            }
            else {

                return new Entities.Glossary
                {
                    Id          = entity.Id,
                    Description = entity.Description,
                    Name        = entity.Name,
                    Active      = entity.Active
                };
            }
        }
    }
}
