﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class UserConverter
    {
        public static UserDTO New(this Entities.User entity) {

            return new UserDTO
            {
                Active      = entity.Active,
                Birthday    = entity.Birthday,
                Email       = entity.Email,
                FullName    = entity.FullName,
                Gender      = entity.Gender,
                Id          = entity.Id,
                Institution = entity.Institution,
                Key         = entity.Key,
                Password    = entity.Password,
                StateId     = entity.StateId,
                CityId      = entity.State.CityId,
                Role        = new Role {
                  Id   = entity.Role.Id,
                  Name = entity.Role.Name
                },
                  
            };
        }
    }
}
