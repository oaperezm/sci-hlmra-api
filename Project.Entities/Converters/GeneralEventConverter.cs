﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class GeneralEventConverter
    {
        public static Entities.GeneralEvent New(this Entities.GeneralEvent entity) {

            return new Entities.GeneralEvent
            {
                Id = entity.Id,
                UrlImage = entity.UrlImage,
                Description = entity.Description,
                EventDate = entity.EventDate,
                Hours = entity.Hours,
                Name = entity.Name,
                Place = entity.Place,
                Published = entity.Published,
                RegisterDate = entity.RegisterDate
            };

        }
    }
}
