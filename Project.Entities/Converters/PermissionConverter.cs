﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class PermissionConverter
    {
        public static PermissionDTO New(this Permission entity) {

            return new PermissionDTO
            {
                Id           = entity.Id,
                Module       = entity.Module,
                Description  = entity.Description,
                Active       = entity.Active,
                ParentId     = entity.ParentId,
                Icon         = entity.Icon,
                Url          = entity.Url,
                ShowInMenu   = entity.ShowInMenu
            };
        }
    }
}
