﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class UserNotificationConverter
    {
        public static UserNotification New(this UserNotification entity) {

            return new Entities.UserNotification
            {
                Id                 = entity.Id,
                Message            = entity.Message,
                NotificationTypeId = entity.NotificationTypeId,
                Read               = entity.Read,
                ReadDate           = entity.ReadDate,
                Title              = entity.Title,
                RegisterDate       = entity.RegisterDate
            };

        }
    }
}
