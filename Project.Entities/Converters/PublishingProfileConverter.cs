﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class PublishingProfileConverter
    {
        public static PublishingProfileDTO New(this Entities.PublishingProfile entity)
        {

            return new PublishingProfileDTO
            {
                Id = entity.Id,
                Description = entity.Description,
                Active = entity.Active,
                Nodes = entity.Nodes.Select(n => new NodeDTO
                {
                    Id = n.Id,
                    Name = n.Name,
                    Description = n.Description
                }).ToList()
            };
        }
    }
}
