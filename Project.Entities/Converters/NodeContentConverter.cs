﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Converters
{
    public static class NodeContentConverter
    {
        public static NodeContentDTO New(this Entities.NodeContent entity) {

            return new NodeContentDTO {
                Id                      = entity.Id,
                ContentId               = entity.ContentId,
                NodeId                  = entity.NodeId,
                UrlContent              = entity.Content?.UrlContent,
                ContentTypeId           = entity.Content?.ContentTypeId,
                ContentType             = entity.Content?.ContentType.Description,
                FileTypeId              = entity.Content?.FileTypeId,
                ContentResourceType     = entity.Content?.ContentResourceType?.Description,
                ContentResourceTypeId   = entity.Content?.ContentResourceTypeId,
                FileType                = entity.Content?.FileType.Description,
                Name                    = entity.Content?.Name,
                Active                  = entity.Content?.Active,
                IsPublic                = entity.Content?.IsPublic,
                Thumbnails              = entity.Content?.Thumbnails,
                Description             = entity.Content.Description,
            };
        }
    }
}
