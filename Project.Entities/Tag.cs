﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Tag: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del tag
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Listado de contenidos
        /// </summary>
        public virtual ICollection<Content> Contents { get; set; } = new List<Content>();
    }
}
