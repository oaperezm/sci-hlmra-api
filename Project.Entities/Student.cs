﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class Student : IEntityBase
    {
        public int Id { get; set; }
        
        public DateTime InvitationDate { get; set; }
        public DateTime? CheckDate { get; set; }
        public string Codigo { get; set; }

        public int StudentGroupId { get; set; }
        [ForeignKey("StudentGroupId")]        
        public virtual StudentGroup StudentGroup { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public int StudentStatusId { get; set; }
        [ForeignKey("StudentStatusId")]
        public virtual StudentStatus StudentStatus { get; set; }
        
    }
}
