﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumNodeAccessType
    {
        Public   = 1,
        Private  = 2,
        Owner    = 3
    }
}
