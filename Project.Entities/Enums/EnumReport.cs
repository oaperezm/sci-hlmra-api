﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
   public enum EnumReport
    {
        Functionality=1,
        CoordinatorProfile=2,
        TeacherProfile=3,
        StudentProfile=4,
        Course=5,
        Exam=6
    }
}
