﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumQuestionType
    {
        MultipleQuestion    = 1,
        OpenQuestion        = 2,
        RelationalQuestion  = 3,
        TrueOrFalseQuestion = 4,
        ImageMultiple       = 5
    }
}
