﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumExamScheduleType
    {

        Programmed = 1,
        Associated = 3,
        Published  = 4,
        Applied    = 5,
        Qualified  = 6,
    }
}
