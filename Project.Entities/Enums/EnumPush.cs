﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumPush
    {
        NewQuestionBank     = 1,
        NewScheduleExamn    = 2,
        NewScheduleHomeWork = 3,
        NewScheduleEvent    = 4,
        AnswerExam          = 5,
        AnswerHomeWork      = 6,
        ScoreExam           = 7,
        ScoreHomeWork       = 8,
        NewGlosarry         = 9,
        NewContent          = 10,
        NewForum            = 11,
        AnswerForum         = 12,
        NewMessage          = 13,
        NewContentAdmin     = 16,
        NewAnnouncement     = 17,
        NewActivity         = 18,
        NewScheduledCourse  = 19,
        NewFinalScore       = 20
    }
}
