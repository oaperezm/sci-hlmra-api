﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumEventType
    {
        Event    = 1,
        HomeWork = 2,
        Exam     = 3

    }
}
