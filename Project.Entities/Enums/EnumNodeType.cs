﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumNodeType
    {
        Level = 1,
        EducationSystem = 2,
        Semester = 3,
        Area = 4,
        Book = 5,
        Block = 6
    }
}
