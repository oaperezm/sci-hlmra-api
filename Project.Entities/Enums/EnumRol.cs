﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Enums
{
    public enum EnumRol
    {
        NotAuthorized   = 0,
        Administrator   = 1,
        Teacher         = 2,
        Student         = 3,
        AdministratorRA = 71,
        TeacherRA       = 72,
        StudentRA       = 73
    }
}
