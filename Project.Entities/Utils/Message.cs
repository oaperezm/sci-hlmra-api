﻿using Project.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities.Utils
{
    public class Message
    {
        public Message(string to, string from, string subject, string template, int systemId)
        {
            this.To       = to;
            this.From     = from;
            this.Subject  = subject;
            this.Template = template;
            this.SystemId = systemId;
        }

        public string To       { get; set; }
        public string From     { get; set; }
        public string Subject  { get; set; }
        public string Template { get; set; }
        public int SystemId    { get; set; }

        /// <summary>
        /// Obtener el full path de los templates
        /// </summary>
        public string FullTemplate { get {

                string url = "";

                switch (SystemId) {
                    case (int)EnumSystems.SALI:
                        url = $"SALI/{ this.Template }";
                        break;
                    case (int)EnumSystems.RA:
                        url = $"RA/{ this.Template }";
                        break;
                };

                return url;
            }
        }

        /// <summary>
        /// Obtener el nombre del sistema 
        /// </summary>
        public string SystemName {
            get
            {
                string systemName = "";

                switch (SystemId)
                {
                    case (int)EnumSystems.SALI:
                        systemName = ConfigurationManager.AppSettings["SALI.APP.NAME"];
                        break;
                    case (int)EnumSystems.RA:
                        systemName = ConfigurationManager.AppSettings["RA.APP.NAME"];
                        break;
                };

                return systemName;
            }
        }

        /// <summary>
        /// Obtener la URL del sistema
        /// </summary>
        public string SystemUrl
        {
            get
            {
                string systemURL = "";

                switch (SystemId)
                {
                    case (int)EnumSystems.SALI:
                        systemURL = ConfigurationManager.AppSettings["SALI.APP.HOME"];
                        break;
                    case (int)EnumSystems.RA:
                        systemURL = ConfigurationManager.AppSettings["RA.APP.HOME"];
                        break;
                };

                return systemURL;
            }
        }
    }

    public class PwdResetMessage: Message
    {
        public PwdResetMessage(string to, string from, string subject, string template, string userName, string newPassword, int systemId)
            :base(to,from, subject,template, systemId)
        {
            this.To = to;
            this.From = from;
            this.Subject = subject;
            this.Template = template;
            this.UserName = userName;
            this.NewPassword = newPassword;
        }
        
        public string UserName    { get; set; }
        public string NewPassword { get; set; }
    }
        
    public class PwdChangeMessage : Message
    {
        public PwdChangeMessage(string to, string from, string subject, string template, string userName, int systemId)
            : base(to, from, subject, template, systemId)
        {
            this.To = to;
            this.From = from;
            this.Subject = subject;
            this.Template = template;
            this.UserName = userName;
        }

        public string UserName { get; set; }
    }

    public class WelcomeMessage : Message
    {
        public WelcomeMessage(string to, string from, string subject, string template, string userName, int systemId, string password, string email, int UserId)
            : base(to, from, subject, template, systemId) {
            
            this.UserName = userName;
            this.Password = password;
            this.Email = email;
            this.UserId = UserId;
        }

        public string UserName { get; set; }      
        public string Password { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }
    }

    public class InvitationMessage : Message
    {
        public InvitationMessage(string to, string from, string subject, string template, string body, string courseName, string courseLink, string userName, int systemId)
            : base(to, from, subject, template, systemId)
        {
            this.To = to;
            this.From = from;
            this.Subject = subject;
            this.Template = template;
            this.Body = body;
            this.CourseName = courseName;
            this.CourseLink = courseLink;
            this.UserName = userName;
        }

        public string Body { get; set; }
        public string CourseName { get; set; }
        public string CourseLink { get; set; }
        public string UserName { get; set; }
    }

    public class ContactMessage : Message {

        public ContactMessage(string to, string from, string subject, string message, string fullName, string template, int systemId, string school, string workplace)
           : base(to, from, subject, template, systemId)
        {
            this.To        = to;
            this.From      = from;
            this.Subject   = subject;
            this.Message   = message;
            this.FullName  = fullName;
            this.Template  = template;
            this.School    = school;
            this.WorkPlace = workplace;
        }

        public string FullName  { get; set; }
        public string Message   { get; set; }
        public string School    { get; set; }
        public string WorkPlace { get; set; }
    }
}
