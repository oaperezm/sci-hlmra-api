﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class CourseSection : IEntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }        
        public int Order { get; set; }
        

        public int CourseId { get; set; }
        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }
        
        public virtual ICollection<CourseClass> CourseClasses { get; set; } = new List<CourseClass>();

    }
}
