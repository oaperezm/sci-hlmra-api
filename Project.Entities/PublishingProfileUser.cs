﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class PublishingProfileUser : IEntityBase
    {
        public int Id { get; set; }

        [ForeignKey("PublishingProfile")]
        public int? PublishingProfileId { get; set; }
        public virtual PublishingProfile PublishingProfile { get; set; }

        [ForeignKey("User")]
        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public DateTime RegisterDate { get; set; }

    }
}
