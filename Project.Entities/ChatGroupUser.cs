﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ChatGroupUser: IEntityBase
    {
        /// <summary>
        /// Identificador principañ
        /// </summary>
        public int Id                       { get; set; }

        /// <summary>
        /// Identificador del usuario 
        /// </summary>
        public int UserId                   { get; set; }
        [ForeignKey("UserId")]
        public virtual User User            { get; set; }

        public int ChatGroupId              { get; set; }
        [ForeignKey("ChatGroupId")]
        public virtual ChatGroup ChatGroup  { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate    { get; set; }

        /// <summary>
        /// Fecha cuando archivo la conversación
        /// </summary>
        public DateTime? ArchiveDate     { get; set; }

        /// <summary>
        /// Fechan en la que eliminó la conversación
        /// </summary>
        public DateTime? DeleteDate      { get; set; }
    }
}
