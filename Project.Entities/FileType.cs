﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Entities
{
    public class FileType : IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Descripción del tipo de archivo
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Identificador del sistema
        /// </summary>
        public int SystemId { get; set; }
        [ForeignKey("SystemId")]
        public virtual System System { get; set; }
               
    }
}
