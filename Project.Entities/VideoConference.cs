﻿using Project.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
  public  class VideoConference : IEntityBase
    {
        public int Id           { get; set; }
        public string Title     { get; set; }
        public string Information { get; set; }
        public DateTime? DateTimeVideoConference { get; set; }
        public string Duration { get; set; }
      
        public int StatusId { get; set; }
        public string Commentary { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string  RoomVideoConference { get; set; }
        public string GuestMail { get; set; }
        public string emailsExternal { get; set; }
        //[ForeignKey("StudentGroup")]
        public int StudentGroupId { get; set; }
        //public virtual StudentGroup StudentGroup { get; set; }
        public int CourseId { get; set; }
        //[ForeignKey("CourseId")]
        //public virtual Course Course { get; set; }
     
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }


        public virtual ICollection<InvitedVideoConferenceDTO> Guests { get; set; } = new List<InvitedVideoConferenceDTO>();
        public virtual ICollection<InvitedVideoConferenceDTO> ExternalGuest { get; set; } = new List<InvitedVideoConferenceDTO>();
    }
}
