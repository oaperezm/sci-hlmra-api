﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ActivityAnswer : IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la actividad relacionada
        /// </summary>
        public int ActivityId { get; set; }
        /// <summary>
        /// Objeto de la relación con actividad
        /// </summary>
        [ForeignKey("ActivityId")]
        public virtual Activity Activity { get; set; }

        /// <summary>
        /// Identificador del alumno
        /// </summary>
        //[ForeignKey("User")]
        public int StudentId { get; set; }
        //public virtual User User { get; set; }

        /// <summary>
        /// Calificacion de la actividad
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Comentario del profesor
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Indica si el alumno cumplió con la actividad
        /// </summary>
        public bool Delivered { get; set; }

        /// <summary>
        /// Indica el parcial para el cual aplica la actividad
        /// </summary>
        public int PartialEvaluation { get; set; }

        /// <summary>
        /// Indica el estatus de la actividad
        /// </summary>
        public int StatusActivity { get; set; }

        /// <summary>
        /// Indica si el alumno puede agregar nuevas actividad en caso que haya sido indicado que nó cumplió con la actividad
        /// </summary>
        public bool AddNewAnswers { get; set; }

        /// <summary>
        /// Listado de archivos
        /// </summary>
        public virtual ICollection<ActivityFile> ActivityAnswerFiles { get; set; } = new List<ActivityFile>();
    }
}
