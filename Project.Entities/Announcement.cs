﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    /// Aviso del Maestro
    public class Announcement: IEntityBase
    {

        /// Identificador principal
        public int Id                   { get; set; }

        /// Descripción del aviso
        public string Title { get; set; }

        /// Descripción del aviso
        public string Description       { get; set; }

        /// Fecha de registro
        public DateTime RegisterDate { get; set; }       

        /// Identificador del usuario
        public int UserId               { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        /// Estatus del registro
        public bool Active              { get; set; }
        
        /// Fecha de actualización del registro
        public DateTime? UpdateDate     { get; set; }
                       
        public virtual ICollection<StudentGroup> StudentGroup { get; set; } = new List<StudentGroup>();

    }
}
