﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class GeneralEvent: IEntityBase
    {
        /// <summary>
        /// Identificador principal
        /// </summary>
        public int Id               { get; set; }

        /// <summary>
        /// Nombre del evento
        /// </summary>
        public string Name          { get; set; }

        /// <summary>
        /// Descripción del evento
        /// </summary>
        public string Description   { get; set; }

        /// <summary>
        /// Lugar del evento
        /// </summary>
        public string Place         { get; set; }

        /// <summary>
        /// Fecha del evento
        /// </summary>
        public DateTime EventDate   { get; set; }

        /// <summary>
        /// Hora del evento
        /// </summary>
        public string Hours         { get; set; }

        /// <summary>
        /// Determina si se publica o no
        /// </summary>
        public bool Published        { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Url de la imagen
        /// </summary>
        public string UrlImage       { get; set; }

        /// <summary>
        /// Identificador del sistema
        /// </summary>
        public int SystemId         { get; set; }
    }
}
