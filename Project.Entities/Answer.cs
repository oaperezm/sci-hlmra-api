﻿using Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Entities
{
    public class Answer : IEntityBase
    {
        public int Id                       { get; set; }

        public string Description           { get; set; }

        public string RelationDescription   { get; set; }

        [NotMapped]
        public string ImageBase64           { get; set; }
        
        public bool IsCorrect               { get; set; }

        public bool Active                  { get; set; }

        public int Order                    { get; set; }

        public int QuestionId               { get; set; }

        [ForeignKey("QuestionId")]        
        public Question Question            { get; set; }

        public int? ImageWidth              { get; set; }

        public int? ImageHeight             { get; set; }
    }
}
