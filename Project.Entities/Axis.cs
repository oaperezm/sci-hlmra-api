﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class Axis: IEntityBase
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        [ForeignKey("EducationLevel")]
        public int EducationLevelId { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
    }
}
