﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class ActivityFile: IEntityBase
    {
        /// <summary>
        /// Identificador principal 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la respuesta de la tarea
        /// </summary>
        public int ActivityAnswerId { get; set; }

        /// <summary>
        /// Objeto relacionado con la respuesta de la tarea
        /// </summary>
        [ForeignKey("ActivityAnswerId")]
        public ActivityAnswer ActivityAnswer { get; set; }

        /// <summary>
        /// Url del archivo en azure
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Comentario del archivo
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Fecha de registro
        /// </summary>
        public DateTime RegisterDate { get; set; }

    }
}
