﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Entities
{
    public class VReportCourse : IEntityBase
    {
        public int Id { get; set; }
        public string cct { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string nameSchool { get; set; }
        public string level { get; set; }
        public DateTime courseRegisterDate { get; set; }
        public int courseId { get; set; }
        public string courseName { get; set; }
        public string group { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
