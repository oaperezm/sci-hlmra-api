﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class EducationLevelApplication: IEducationLevelApplication
    {
        private readonly IEducationLevel _educationLevel;

        public EducationLevelApplication(IEducationLevel educationLevel)
        {
            this._educationLevel = educationLevel;
        }

        public Task<Response> Add(EducationLevelRequest request)
        {
            return this._educationLevel.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._educationLevel.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._educationLevel.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._educationLevel.GetById(id);
        }

        public Task<Response> Update(EducationLevelUpdateRequest request)
        {
            return this._educationLevel.Update(request);
        }
    }
}
