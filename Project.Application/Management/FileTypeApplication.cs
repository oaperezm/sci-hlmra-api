﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class FileTypeApplication : IFileTypeApplication
    {
        private readonly IFileType _FileType;

        public FileTypeApplication(IFileType FileType)
        {

            this._FileType = FileType;
        }

        /// <summary>
        /// Agregar un tipo de archivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddFileType(FileTypeSaveRequest request)
        {
            return this._FileType.AddFileType(request);
        }

        /// <summary>
        /// Eliminar un tipo de archivo existentes
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<Response> DeleteFileType(int userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtenet todos los tipos de archivos
        /// </summary>
        /// <returns></returns>
        public Task<Response> GetAll(int systemId)
        {
            return this._FileType.GetAll(systemId);
        }

        /// <summary>
        /// Obtener tipo de archivo por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return this._FileType.GetById(id);
        }
        
        /// <summary>
        /// Actualizar el tipo de archivos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateFileType(FileTypeUpdateRequest request)
        {
            return this._FileType.UpdateFileType(request);
        }

    }
}
