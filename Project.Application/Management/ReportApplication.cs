﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class ReportApplication: IReportApplication
    {
        private readonly IReport _report;

        public ReportApplication(IReport report)
        {
            this._report = report;
        }

        public Task<Response> Add(ReportsRequest request)
        {
            return this._report.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._report.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._report.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._report.GetById(id);
        }

        public Task<Response> Update(ReportsUpdateRequest request)
        {
            return this._report.Update(request);
        }
    }
}