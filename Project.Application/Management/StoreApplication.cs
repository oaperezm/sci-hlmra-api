﻿using System.Collections.Generic;
using Application.Management;
using Domain.Management;
using Entities;


namespace Application.Management
{
    public class StoreApplication : IStoreApplication
    {
        private readonly IStore _store;

        public StoreApplication(IStore store)
        {
            this._store = store;
        }

        public Product Add(Product product)
        {
            return this._store.Add(product);
        }

        public List<Product> List(Product product)
        {
            return this._store.List(product);
        }

        public Product Remove(Product product)
        {
            return this._store.Remove(product);
        }

        public Product Update(Product product)
        {
            return this._store.Update(product);
        }
    }
}
