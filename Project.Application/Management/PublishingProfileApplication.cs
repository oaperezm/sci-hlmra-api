﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PublishingProfileApplication : IPublishingProfileApplication
    {
        private readonly IPublishingProfile _PublishingProfile;

        public PublishingProfileApplication(IPublishingProfile PublishingProfile)
        {

            this._PublishingProfile = PublishingProfile;
        }

        public Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request)
        {
            return this._PublishingProfile.AddPublishingProfile(request);
        }

        public Task<Response> DeletePublishingProfile(int id)
        {
            return this._PublishingProfile.DeletePublishingProfile(id);
        }

        public Task<Response> GetAll()
        {
            return this._PublishingProfile.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._PublishingProfile.GetById(id);
        }

        public Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request)
        {
            return this._PublishingProfile.UpdatePublishingProfile(request);
        }

        public Task<Response> GetNodes(int id)
        {
            return this._PublishingProfile.GetNodes(id);
        }

        public Task<Response> AddNodes(int id, List<int> nodes)
        {
            return this._PublishingProfile.AddNodes(id, nodes);
        }

        public Task<Response> GetLinkedUsers(int profileId)
        {
            return this._PublishingProfile.GetLinkedUsers(profileId);
        }

    }
}
