﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class CityApplication : ICityApplication
    {
        private readonly ICity _city;

        public CityApplication(ICity city) {
            _city = city;
        }

        /// <summary>
        /// Obtener todos las ciudades
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _city.GetAll();
        }

        /// <summary>
        /// Obtener estados por identificador de ciudad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetStateByCity(int id)
        {
            return await _city.GetStateByCity(id);
        }
    }
}
