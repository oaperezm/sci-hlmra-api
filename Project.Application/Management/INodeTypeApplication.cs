﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface INodeTypeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNodeType(NodeTypeSaveRequest request);
        Task<Response> UpdateNodeType(NodeTypeUpdateRequest request);
        Task<Response> DeleteNodeType(int roleId);        
    }
}
