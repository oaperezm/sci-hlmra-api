﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class BookApplication : IBookApplication
    {
        private readonly IBook _Book;

        public BookApplication(IBook Book)
        {

            this._Book = Book;
        }

        /// <summary>
        /// Agregar un nuevo libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddBook(BookSaveRequest request)
        {
            return this._Book.AddBook(request);
        }

        /// <summary>
        /// Eliminar un libro registrado
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteBook(int bookId)
        {
            return  await _Book.DeleteBook(bookId);
        }

        /// <summary>
        /// Obtener todos los libros registrados por paginación
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="justActive"></param>
        /// <param name="getAll"></param>
        /// <returns></returns>
        public Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter)
        {
            return this._Book.GetAll(curentPage, sizePage, filter);
        }

        /// <summary>
        /// Obtener todos los libros activos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _Book.GetAll();
        }

        /// <summary>
        /// Obtener los datos de un libro por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return this._Book.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un libro en especifico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateBook(BookUpdateRequest request)
        {
            return this._Book.UpdateBook(request);
        }

    }
}
