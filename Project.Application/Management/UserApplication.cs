﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class UserApplication : IUserApplication
    {
        private readonly IUser _user;

        public UserApplication(IUser user) {

            this._user = user;
        }

        public Task<Response> RegisterUser(UserSaveRequest request)
        {
            return this._user.RegisterUser(request);
        }

        public async Task<Response> DeleteUser(int userId)
        {
            return await _user.DeleteUser(userId);
        }

        public Task<Response> GetAll()
        {
            return this._user.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Response> RecoveryPassword(string email, string bodyHTML)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> ResetPassword(UserSavePasswordRequest request)
        {
            return await _user.ResetPassword(request);
        }

        public async Task<Response> UpdateUser(UserUpdateRequest request)
        {
            return await _user.UpdateUser(request);
        }

        public async Task<Response> ValidateUser(string username, string password)
        {
            return await _user.ValidateUser(username, password);
        }

        public Task<Response> GetNodes(int id)
        {
            return this._user.GetNodes(id);
        }

        public Task<Response> AddNodes(int id, List<int> nodes)
        {
            return this._user.AddNodes(id, nodes);
        }

        public Task<Response> CopyProfile(int profileId, int[] userIds)
        {
            return this._user.CopyProfile(profileId, userIds);
        }

        public async Task<Response> GetUserProfile(int userId){

            return await this._user.GetUserProfile(userId);

        }

        public async Task<Response> GetByMail(string eMail)
        {
            return await this._user.GetByMail(eMail);
        }

        public async Task<Response> GetByPartialMail(string partialMail)
        {
            return await this._user.GetByPartialMail(partialMail);
        }

        public async Task<Response> GetByPartialMailRole(string partialMail, int roleId)
        {
            return await this._user.GetByPartialMailRole(partialMail, roleId);
        }

        public Task<Response> LinkProfile(int profileId, int[] userIds)
        {
            return this._user.LinkProfile(profileId, userIds);
        }

        
        public Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId)
        {
            return this._user.GetByMailRoleCourse(partialMail, roleId, courseId);
        }

        /// <summary>
        /// Guardar el refresht token en la base de datos
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="refreshToken"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public Task<bool> SaveRefreshToken(int userId, string refreshToken, string client, string protectedTicket)
        {
            return this._user.SaveRefreshToken(userId, refreshToken, client, protectedTicket);
        }

        /// <summary>
        /// Obtenerusuario por el refresh token
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public async Task<Entities.User> GetByRegreshToken(string refreshToken)
        {
            return await this._user.GetByRegreshToken(refreshToken);
        }

        /// <summary>
        /// Obtener los datos de un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUserProfileData(int userId)
        {
            return await this._user.GetUserProfileData(userId);
        }

        /// <summary>
        /// Permite cambiar la contraseña del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> ChangePassword(UserChangePasswordRequest request)
        {
            return await _user.ChangePassword(request);
        }

        /// <summary>
        /// Permite actualizar la imagen de perfil del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UploadImgProfile(UserUploadImgProfileRequest request)
        {
            return await _user.UploadImgProfile(request);
        }
        /// <summary>
        /// Elimina los datos de sesión del usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> LogOut(int userId)
        {
            return await this._user.LogOut(userId);
        }
    }
}
