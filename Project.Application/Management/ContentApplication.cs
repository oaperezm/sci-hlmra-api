﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class ContentApplication : IContentApplication
    {
        private readonly IContent _content;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="content"></param>
        public ContentApplication(IContent content)
        {

            this._content = content;
        }

        /// <summary>
        /// Agregar un nuevo contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddContent(ContentSaveRequest request)
        {
            return await this._content.AddContent(request);
        }

        /// <summary>
        /// Método para eliminar los datos de un contenido de manera permanente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteContent(int id)
        {
            return await _content.DeleteContent(id);
        }

        /// <summary>
        /// Método para descargar contenido sin usar Azure
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DownloadContent(int id)
        {
            return await _content.DownloadContent(id);
        }

        /// <summary>
        /// Obtener todos los contenido
        /// </summary>
        /// <returns></returns>
        public Task<Response> GetAll()
        {
            return this._content.GetAll();
        }

        /// <summary>
        /// Obtener los datos de un contenido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return this._content.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateContent(ContentUpdateRequest request)
        {
            return this._content.UpdateContent(request);
        }

        /// <summary>
        /// Método que permite activar o desactivar un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateActiveStatus(ContentUpdateStatusRequest request) {

            return await _content.UpdateActiveStatus(request.Id, request.Status);
        }

        /// <summary>
        /// Obtener todos los registros de contenido por paginacoón
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, PaginationContentRequest request, int systemId)
        {
            return await _content.GetAll(currentPage, sizePage, request, systemId);
        }

        /// <summary>
        /// Asignar tags a contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AssignatedTags(ContentAssingTagsRequest request)
        {
            return await _content.AssignatedTags(request);
        }

        /// <summary>
        /// Actualizar los tags de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateTags(ContentTagsRequest request)
        {
            return await _content.UpdateTags(request);
        }

        /// <summary>
        /// Obtener el contenido relacionado de un contenido seleccionado
        /// </summary>
        /// <param name="contentId"> Contenido dle identificador </param>
        /// <param name="currentPage"> Página actual </param>
        /// <param name="sizePage"> Tamaño de la página </param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetRelatedContent(int contentId, int currentPage, int sizePage, int systemId)
        {
            return await _content.GetRelatedContent(contentId, currentPage, sizePage, systemId);
        }

        /// <summary>
        /// Actualizar la cantidad de visitas de un contenido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> UpdateVisits(int id)
        {
            return await _content.UpdateVisits(id);
        }
    }
}
