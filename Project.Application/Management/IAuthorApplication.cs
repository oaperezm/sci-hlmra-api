﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IAuthorApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddAuthor(AuthorSaveRequest request);
        Task<Response> UpdateAuthor(AuthorUpdateRequest request);
        Task<Response> DeleteAuthor(int roleId);        
    }
}
