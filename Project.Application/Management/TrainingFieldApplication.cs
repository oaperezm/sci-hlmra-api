﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class TrainingFieldApplication : ITrainingFieldApplication
    {
        private readonly ITrainingField _TrainingField;

        public TrainingFieldApplication(ITrainingField TrainingField)
        {

            this._TrainingField = TrainingField;
        }

        public Task<Response> AddTrainingField(TrainingFieldSaveRequest request)
        {
            return this._TrainingField.AddTrainingField(request);
        }

        public Task<Response> DeleteTrainingField(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._TrainingField.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._TrainingField.GetById(id);
        }

        public Task<Response> UpdateTrainingField(TrainingFieldUpdateRequest request)
        {
            return this._TrainingField.UpdateTrainingField(request);
        }

    }
}
