﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class TagApplication : ITagApplication
    {
        private readonly ITag _tag;

        public TagApplication(ITag tag) {
            _tag = tag;
        }

        /// <summary>
        /// Agregar un nuevo tag
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TagSaveRequest request)
        {
            return await _tag.Add(request);
        }

        /// <summary>
        /// Eliminar un tag existente que no este relacionado
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int tagId)
        {
            return await _tag.Delete(tagId);
        }

        /// <summary>
        /// Obteniendo todos los tag registrados paginados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter)
        {
            return await _tag.GetAll(currentPage, sizePage, filter);
        }

        /// <summary>
        /// Actualizar los datos de un tag existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(TagUpdateRequest request)
        {
            return await _tag.Update(request);
        }
    }
}
