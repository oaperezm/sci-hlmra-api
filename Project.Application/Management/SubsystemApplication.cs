﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class SubsystemApplication : ISubsystemApplication
    {
        private readonly ISubsystem _Subsystem;

        public SubsystemApplication(ISubsystem Subsystem)
        {

            this._Subsystem = Subsystem;
        }

        public Task<Response> AddSubsystem(SubsystemSaveRequest request)
        {
            return this._Subsystem.AddSubsystem(request);
        }

        public Task<Response> DeleteSubsystem(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Subsystem.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Subsystem.GetById(id);
        }

        public Task<Response> UpdateSubsystem(SubsystemUpdateRequest request)
        {
            return this._Subsystem.UpdateSubsystem(request);
        }

    }
}
