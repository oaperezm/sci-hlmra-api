﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class AreasCurriculumAutonomyApplication: IAreasCurriculumAutonomyApplication
    {
        private readonly IAreasCurriculumAutonomy _areasCurriculumAutonomy;

        public AreasCurriculumAutonomyApplication(IAreasCurriculumAutonomy areasCurriculumAutonomy )
        {
            this._areasCurriculumAutonomy = areasCurriculumAutonomy;
        }

        public Task<Response> Add(AreasCurriculumAutonomyRequest request)
        {
            return this._areasCurriculumAutonomy.Add(request);
        }

        public Task<Response> Delete(int Id)
        {
            return this.Delete(Id);
        }

        public Task<Response> GetAll()
        {
            return this._areasCurriculumAutonomy.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._areasCurriculumAutonomy.GetById(id);
        }

        public Task<Response> Update(AreasCurriculumAutonomyUpdateRequest request)
        {
            return this._areasCurriculumAutonomy.Update(request);
        }
    }
}
