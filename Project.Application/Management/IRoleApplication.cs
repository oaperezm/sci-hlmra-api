﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IRoleApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddRole(RoleSaveRequest request);
        Task<Response> UpdateRole(RoleSaveRequest request);
        Task<Response> DeleteRole(int roleId);
        Task<Response> GetRolePermissions(int id);
        Task<Response> UpdatePermissions(int id, List<int> permissions);
    }
}
