﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface INodeContentApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNodeContent(NodeContentSaveRequest request);
        Task<Response> UpdateNodeContent(NodeContentUpdateRequest request);
        Task<Response> DeleteNodeContent(int nodeContentId);
    }
}
