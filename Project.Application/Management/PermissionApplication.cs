﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PermissionApplication : IPermissionApplication
    {
        private readonly IPermission _permission;

        public PermissionApplication(IPermission permission)
        {

            this._permission = permission;
        }

        public Task<Response> AddPermission(PermissionSaveRequest request)
        {
            return this._permission.AddPermission(request);
        }

        public Task<Response> DeletePermission(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._permission.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._permission.GetById(id);
        }

        public Task<Response> UpdatePermission(PermissionSaveRequest request)
        {
            return this._permission.UpdatePermission(request);
        }

    }
}
