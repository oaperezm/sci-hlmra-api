﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PublishingProfileUserApplication : IPublishingProfileUserApplication
    {
        private readonly IPublishingProfileUser _PublishingProfileUser;

        public PublishingProfileUserApplication(IPublishingProfileUser PublishingProfileUser)
        {

            this._PublishingProfileUser = PublishingProfileUser;
        }

        public Task<Response> AddPublishingProfileUser(PublishingProfileUserSaveRequest request)
        {
            return this._PublishingProfileUser.AddPublishingProfileUser(request);
        }

        public Task<Response> DeletePublishingProfileUser(int id)
        {
            return this._PublishingProfileUser.DeletePublishingProfileUser(id);
        }

        public Task<Response> GetAll()
        {
            return this._PublishingProfileUser.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._PublishingProfileUser.GetById(id);
        }

        public Task<Response> UpdatePublishingProfileUser(PublishingProfileUserUpdateRequest request)
        {
            return this._PublishingProfileUser.UpdatePublishingProfileUser(request);
        }

        public Task<Response> GetByUserId(int userId)
        {
            return this._PublishingProfileUser.GetByUserId(userId);
        }

    }
}
