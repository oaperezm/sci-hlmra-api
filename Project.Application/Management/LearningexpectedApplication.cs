﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class LearningexpectedApplication: ILearningexpectedApplication
    {
        private readonly ILearningexpected _learningexpected;

        public LearningexpectedApplication(ILearningexpected learningexpected)
        {
            this._learningexpected = learningexpected;
        }

        public Task<Response> Add(LearningexpectedRequest request)
        {
            return this._learningexpected.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._learningexpected.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._learningexpected.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._learningexpected.GetById(id);
        }

        public Task<Response> Update(LearningexpectedUpdateRequest request)
        {
            return this._learningexpected.Update(request);
        }
    }
}
