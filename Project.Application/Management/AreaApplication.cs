﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class AreaApplication : IAreaApplication
    {
        private readonly IArea _Area;

        public AreaApplication(IArea Area)
        {

            this._Area = Area;
        }

        public Task<Response> AddArea(AreaSaveRequest request)
        {
            return this._Area.AddArea(request);
        }

        public Task<Response> DeleteArea(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Area.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Area.GetById(id);
        }

        public Task<Response> UpdateArea(AreaUpdateRequest request)
        {
            return this._Area.UpdateArea(request);
        }

    }
}
