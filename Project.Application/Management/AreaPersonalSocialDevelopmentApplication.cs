﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class AreaPersonalSocialDevelopmentApplication : IAreaPersonalSocialDevelopmentApplication
    {
        private readonly IAreaPersonalSocialDevelopment _areaPersonalSocialDevelopment;

        public AreaPersonalSocialDevelopmentApplication(IAreaPersonalSocialDevelopment areaPersonalSocialDevelopment)
        {

            this._areaPersonalSocialDevelopment = areaPersonalSocialDevelopment;
        }

        public Task<Response> Add(AreaPersonalSocialDevelopmentRequest request)
        {
            return this._areaPersonalSocialDevelopment.Add(request);
        }

        public Task<Response> Delete(int AreaPSDId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._areaPersonalSocialDevelopment.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._areaPersonalSocialDevelopment.GetById(id);
        }

        public Task<Response> Update(AreaPersonalSocialDevelopmentUpdateRequest request)
        {
            return this._areaPersonalSocialDevelopment.Update(request);
        }
    }
}
