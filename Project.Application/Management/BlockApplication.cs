﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class BlockApplication : IBlockApplication
    {
        private readonly IBlock _Block;

        public BlockApplication(IBlock Block)
        {

            this._Block = Block;
        }

        public Task<Response> AddBlock(BlockSaveRequest request)
        {
            return this._Block.AddBlock(request);
        }

        public Task<Response> DeleteBlock(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Block.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Block.GetById(id);
        }

        public Task<Response> UpdateBlock(BlockUpdateRequest request)
        {
            return this._Block.UpdateBlock(request);
        }

    }
}
