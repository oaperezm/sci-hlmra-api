﻿using Entities;
using System.Collections.Generic;

namespace Application.Management
{
    public interface IStoreApplication
    {
        Product Add(Product product);
        Product Update(Product product);
        Product Remove(Product product);
        List<Product> List(Product product);
    }
}
