﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class LevelApplication : ILevelApplication
    {
        private readonly ILevel _Level;

        public LevelApplication(ILevel Level)
        {

            this._Level = Level;
        }

        public Task<Response> AddLevel(LevelSaveRequest request)
        {
            return this._Level.AddLevel(request);
        }

        public Task<Response> DeleteLevel(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Level.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Level.GetById(id);
        }

        public Task<Response> UpdateLevel(LevelUpdateRequest request)
        {
            return this._Level.UpdateLevel(request);
        }

    }
}
