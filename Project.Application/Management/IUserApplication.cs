﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IUserApplication
    {
        Task<Response> ValidateUser(string username, string password);
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> RegisterUser(UserSaveRequest request);
        Task<Response> UpdateUser(UserUpdateRequest request);
        Task<Response> DeleteUser(int userId);
        Task<Response> RecoveryPassword(string email, string bodyHTML);
        Task<Response> ResetPassword(UserSavePasswordRequest request);
        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
        Task<Response> CopyProfile(int profileId, int[] userId);
        Task<Response> GetUserProfile(int userId);
    }
}
