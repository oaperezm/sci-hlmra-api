﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface INodeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddNode(NodeSaveRequest request);
        Task<Response> UpdateNode(NodeUpdateRequest request);
        Task<Response> DeleteNode(int roleId);    
        Task<Response> GetSubnodes(int id);
        Task<Response> GetNodeContens(int id);
        Task<Response> RemoveImage(int id);
        Task<Response> GetNodeStructureByParentId(int id);
    }
}
