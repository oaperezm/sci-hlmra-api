﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class AxisApplication:IAxisApplication
    {
        private readonly IAxis _axis;

        public AxisApplication(IAxis axis)
        {
            this._axis = axis;
        }

        public Task<Response> Add(AxisRequest request)
        {
            return this._axis.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._axis.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._axis.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._axis.GetById(id);
        }

        public Task<Response> Update(AxisUpdateRequest request)
        {
            return this._axis.Update(request);
        }
    }
}
