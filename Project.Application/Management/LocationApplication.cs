﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class LocationApplication : ILocationApplication
    {
        private readonly ILocation _Location;

        public LocationApplication(ILocation Location)
        {

            this._Location = Location;
        }

        public Task<Response> AddLocation(LocationSaveRequest request)
        {
            return this._Location.AddLocation(request);
        }

        public Task<Response> DeleteLocation(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Location.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Location.GetById(id);
        }

        public Task<Response> UpdateLocation(LocationUpdateRequest request)
        {
            return this._Location.UpdateLocation(request);
        }

    }
}
