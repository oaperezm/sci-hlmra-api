﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class KeylearningApplication: IKeylearningApplication
    {
        private readonly IKeylearning _keylearning;

        public KeylearningApplication(IKeylearning keylearning)
        {
            this._keylearning = keylearning;
        }

        public Task<Response> Add(KeylearningRequest request)
        {
            return this._keylearning.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._keylearning.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._keylearning.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._keylearning.GetById(id);
        }

        public Task<Response> Update(KeylearningUpdateRequest request)
        {
            return this._keylearning.Update(request);
        }
    }
}
