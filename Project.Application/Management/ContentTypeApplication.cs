﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class ContentTypeApplication : IContentTypeApplication
    {
        private readonly IContentType _ContentType;

        public ContentTypeApplication(IContentType ContentType)
        {

            this._ContentType = ContentType;
        }

        public Task<Response> AddContentType(ContentTypeSaveRequest request)
        {
            return this._ContentType.AddContentType(request);
        }

        public Task<Response> DeleteContentType(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._ContentType.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._ContentType.GetById(id);
        }

        public Task<Response> UpdateContentType(ContentTypeUpdateRequest request)
        {
            return this._ContentType.UpdateContentType(request);
        }

    }
}
