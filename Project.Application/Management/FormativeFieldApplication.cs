﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class FormativeFieldApplication : IFormativeFieldApplication
    {
        private readonly IFormativeField _FormativeField;

        public FormativeFieldApplication(IFormativeField FormativeField)
        {

            this._FormativeField = FormativeField;
        }

        public Task<Response> AddFormativeField(FormativeFieldSaveRequest request)
        {
            return this._FormativeField.AddFormativeField(request);
        }

        public Task<Response> DeleteFormativeField(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._FormativeField.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._FormativeField.GetById(id);
        }

        public Task<Response> UpdateFormativeField(FormativeFieldUpdateRequest request)
        {
            return this._FormativeField.UpdateFormativeField(request);
        }

    }
}
