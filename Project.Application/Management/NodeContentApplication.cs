﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class NodeContentApplication : INodeContentApplication
    {
        private readonly INodeContent _NodeContent;

        public NodeContentApplication(INodeContent NodeContent)
        {

            this._NodeContent = NodeContent;
        }

        public Task<Response> AddNodeContent(NodeContentSaveRequest request)
        {
            return this._NodeContent.AddNodeContent(request);
        }

        public async Task<Response> DeleteNodeContent(int nodeContentId)
        {
            return await _NodeContent.DeleteNodeContent(nodeContentId);
        }

        public Task<Response> GetAll()
        {
            return this._NodeContent.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._NodeContent.GetById(id);
        }

        public Task<Response> UpdateNodeContent(NodeContentUpdateRequest request)
        {
            return this._NodeContent.UpdateNodeContent(request);
        }

    }
}
