﻿using Project.Application.Core;

using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;
using Project.Domain.Core;

namespace Project.Application.Management
{
    public class VideoConferenceApplication: IVideoConferenceApplication
    {
        private readonly IVideoconference _videoConference;

        public VideoConferenceApplication(IVideoconference videoconference)
        {
            this._videoConference = videoconference;

        }
        /// <summary>
        /// Método para agregar un  nuevo registro de video conferencia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(VideoConferenceSaveRequest request,int systemId, int userId)
        {
            return _videoConference.Add(request, systemId, userId);
        }
        /// <summary>
        /// Método que elimina de manera permanente un registro de video conferencia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id, int systemId)
        {
            return await _videoConference.Delete(id,systemId);

        }
        /// <summary>
        /// Método para obtener todos los registros de video conferencia 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll(int systemId, int UserId)
        {
            return await _videoConference.GetAll(systemId, UserId);
        }
       

        /// <summary>
        /// Obtener los datos de un registro de video conferencia
        /// </summary>
        /// <param name="id">Identificador principal del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await _videoConference.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(VideoConferenceUpdateRequest request, int systemId)
        {
            return await _videoConference.Update(request, systemId);

        }
        /// <summary>
        /// Método para obtener todos los registros nodos de acuerdo  al tipo de libro
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetNodo()
        {
            return await _videoConference.GetNodo();
        }

        public async Task<Response> Add(VideoConferenceSaveRequest request, InvitedVideoConferenceSaveRequest invited, InvitedVideoConferenceSaveRequest externalguest)
        {          
          return await _videoConference.Add(request,invited, externalguest);
        }
    }
}
