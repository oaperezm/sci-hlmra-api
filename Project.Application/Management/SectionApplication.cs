﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class SectionAppnlication: ISectionApplication
    {
        private readonly ISection _section;

        public SectionAppnlication(ISection section)
        {
            this._section = section;
        }

        public Task<Response> Add(SectionsRequest request)
        {
            return this._section.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._section.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._section.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._section.GetById(id);
        }

        public Task<Response> Update(SectionsUpdateRequest request)
        {
            return this._section.Update(request);
        }
    }
}
