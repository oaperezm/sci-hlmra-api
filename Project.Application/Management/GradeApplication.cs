﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class GradeApplication : IGradeApplication
    {
        private readonly IGrade _Grade;

        public GradeApplication(IGrade Grade)
        {

            this._Grade = Grade;
        }

        public Task<Response> AddGrade(GradeSaveRequest request)
        {
            return this._Grade.AddGrade(request);
        }

        public Task<Response> DeleteGrade(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Grade.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Grade.GetById(id);
        }

        public Task<Response> UpdateGrade(GradeUpdateRequest request)
        {
            return this._Grade.UpdateGrade(request);
        }

    }
}
