﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IFileTypeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddFileType(FileTypeSaveRequest request);
        Task<Response> UpdateFileType(FileTypeUpdateRequest request);
        Task<Response> DeleteFileType(int roleId);        
    }
}
