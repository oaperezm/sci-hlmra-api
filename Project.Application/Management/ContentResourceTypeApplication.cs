﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class ContentResourceTypeApplication : IContentResourceTypeApplication
    {
        private readonly IContentResourceType _contentResourceType;

        public ContentResourceTypeApplication(IContentResourceType contentResourceType) {
            _contentResourceType = contentResourceType;
        }

        /// <summary>
        /// Obtener todos los registros
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _contentResourceType.GetAll();
        }
    }
}
