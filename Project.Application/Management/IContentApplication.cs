﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IContentApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddContent(ContentSaveRequest request);
        Task<Response> UpdateContent(ContentUpdateRequest request);
        Task<Response> DeleteContent(int roleId);
        Task<Response> DownloadContent(int roleId);
        Task<Response> UpdateActiveStatus(ContentUpdateStatusRequest request);
    }
}
