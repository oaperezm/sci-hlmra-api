﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PurposeApplication : IPurposeApplication
    {
        private readonly IPurpose _Purpose;

        public PurposeApplication(IPurpose Purpose)
        {

            this._Purpose = Purpose;
        }

        public Task<Response> AddPurpose(PurposeSaveRequest request)
        {
            return this._Purpose.AddPurpose(request);
        }

        public Task<Response> DeletePurpose(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Purpose.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Purpose.GetById(id);
        }

        public Task<Response> UpdatePurpose(PurposeUpdateRequest request)
        {
            return this._Purpose.UpdatePurpose(request);
        }

    }
}
