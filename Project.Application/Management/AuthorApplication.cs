﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class AuthorApplication : IAuthorApplication
    {
        private readonly IAuthor _author;

        public AuthorApplication(IAuthor author)
        {

            _author = author;
        }

        /// <summary>
        /// Agregar un nuevo autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddAuthor(AuthorSaveRequest request)
        {
            return this._author.AddAuthor(request);
        }

        /// <summary>
        /// Eliminar un autor registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> DeleteAuthor(int id)
        {
            return _author.DeleteAuthor(id);
        }

        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter)
        {
            return  await _author.GetAll(curentPage,sizePage,filter);
        }

        /// <summary>
        /// Obtener todos los autores registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _author.GetAll();
        }

        /// <summary>
        /// Obtener autor por indentificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return _author.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateAuthor(AuthorUpdateRequest request)
        {
            return _author.UpdateAuthor(request);
        }

    }
}
