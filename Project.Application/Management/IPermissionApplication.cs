﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IPermissionApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPermission(PermissionSaveRequest request);
        Task<Response> UpdatePermission(PermissionSaveRequest request);
        Task<Response> DeletePermission(int roleId);        
    }
}
