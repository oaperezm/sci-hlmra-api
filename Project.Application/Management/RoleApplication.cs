﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class RoleApplication : IRoleApplication
    {
        private readonly IRole _role;

        public RoleApplication(IRole role)
        {

            this._role = role;
        }

        public Task<Response> AddRole(RoleSaveRequest request)
        {
            return this._role.AddRole(request);
        }

        public Task<Response> DeleteRole(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._role.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._role.GetById(id);
        }

        public Task<Response> UpdateRole(RoleSaveRequest request)
        {
            return this._role.UpdateRole(request);
        }

        public Task<Response> GetRolePermissions(int id)
        {
            return this._role.GetRolePermissions(id);
        }

        public Task<Response> UpdatePermissions(int id, List<int> permissions)
        {
            return this._role.UpdatePermissions(id, permissions);
        }
    }
}
