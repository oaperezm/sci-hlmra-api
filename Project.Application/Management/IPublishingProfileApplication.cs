﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IPublishingProfileApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request);
        Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request);
        Task<Response> DeletePublishingProfile(int id);
        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
    }
}
