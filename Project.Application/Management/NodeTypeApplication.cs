﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class NodeTypeApplication : INodeTypeApplication
    {
        private readonly INodeType _NodeType;

        public NodeTypeApplication(INodeType NodeType)
        {

            this._NodeType = NodeType;
        }

        public Task<Response> AddNodeType(NodeTypeSaveRequest request)
        {
            return this._NodeType.AddNodeType(request);
        }

        public Task<Response> DeleteNodeType(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._NodeType.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._NodeType.GetById(id);
        }

        public Task<Response> UpdateNodeType(NodeTypeUpdateRequest request)
        {
            return this._NodeType.UpdateNodeType(request);
        }

    }
}
