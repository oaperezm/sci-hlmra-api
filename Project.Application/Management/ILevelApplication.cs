﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface ILevelApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddLevel(LevelSaveRequest request);
        Task<Response> UpdateLevel(LevelUpdateRequest request);
        Task<Response> DeleteLevel(int roleId);        
    }
}
