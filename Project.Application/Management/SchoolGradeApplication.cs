﻿using Project.Domain.Management;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public class SchoolGradeApplication:ISchoolGradeApplication
    {
        private readonly ISchoolGrade _schoolGrade;

        public SchoolGradeApplication(ISchoolGrade schoolGrade)
        {
            this._schoolGrade = schoolGrade;
        }

        public Task<Response> Add(SchoolGradeRequest request)
        {
            return this._schoolGrade.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return this._schoolGrade.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return this._schoolGrade.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._schoolGrade.GetById(id);
        }

        public Task<Response> Update(SchoolGradeUpdateRequest request)
        {
            return this._schoolGrade.Update(request);
        }
    }
}
