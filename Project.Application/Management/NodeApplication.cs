﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class NodeApplication : INodeApplication
    {
        private readonly INode _node;

        public NodeApplication(INode node)
        {

            this._node = node;
        }


        /// <summary>
        /// Agregar un nuevo nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddNode(NodeSaveRequest request)
        {
            return this._node.AddNode(request);
        }

        /// <summary>
        /// Método para eliminar un nodo de acuerdo al identificador enviado
        /// </summary>
        /// <param name="id"> Identificador del nodo </param>
        /// <returns></returns>
        public Task<Response> DeleteNode(int id)
        {
            return this._node.DeleteNode(id);
        }

        /// <summary>
        /// Obtener todos los nodos
        /// </summary>
        /// <returns></returns>
        public Task<Response> GetAll()
        {
            return this._node.GetAll();
        }

        /// <summary>
        /// Obtener un nodo por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return this._node.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateNode(NodeUpdateRequest request)
        {
            return this._node.UpdateNode(request);
        }

        /// <summary>
        /// Obtener los nodos a partir de una estructura
        /// </summary>
        /// <param name="id"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public Task<Response> GetSubnodes(int id, int systemId)
        {
            return this._node.GetSubnodes(id, systemId);
        }

        /// <summary>
        /// Obtener el contenido de un nodo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rolId"></param>
        /// <returns></returns>
        public Task<Response> GetNodeContens(int id, int rolId)
        {
            return this._node.GetNodeContents(id, rolId);
        }

        /// <summary>
        /// Eliminar la imagen asignada a uin nodo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> RemoveImage(int id)
        {
            return await this._node.RemoveImage(id);
        }

        /// <summary>
        /// Obtener la estructura apartir de un nodo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetNodeStructureByParentId(int id)
        {
            return await _node.GetNodeStructureByParentId(id);
        }

        /// <summary>
        /// Obtener los libros por nodo
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetBooksByNodeId(int nodeId, int curentPage, int sizePage, int userId)
        {
            return await _node.GetBooksByNodeId(nodeId, curentPage, sizePage, userId);
        }

        /// <summary>
        /// Obtener inventario de los nodos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetInventoryFiles(int id, int systemId)
        {
            return await _node.GetInventoryFiles(id, systemId);
        }

        /// <summary>
        /// Obtener todos los archivos por de un nodo y filtrado por tipo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetInventoryFileTypes(int id, int fileType, int currentPage, int sizePage, int systemId)
        {
            return await _node.GetInventoryFileTypes(id,fileType, currentPage, sizePage, systemId);
        }

        /// <summary>
        /// Obtener el contenido recursivo en base a un nodo
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="fileTypeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetNodeContentRecursive(int nodeId, List<int> fileTypeId, int purposeId, int blockId, int formativeFieldId, 
                                                                      int currentPage, int sizePage, int systemId, string filter, int rolId
                                                                     ,int EducationLevelId, int AreaPersonalSocialDevelopmentId, int AreasCurriculumAutonomyId,
                                                                      int AxisId, int KeylearningId, int LearningexpectedId)
        {
            return await _node.GetNodeContentRecursive(nodeId, fileTypeId, purposeId, blockId, formativeFieldId, currentPage, sizePage, systemId, filter, rolId
                                                        ,EducationLevelId, AreaPersonalSocialDevelopmentId,  AreasCurriculumAutonomyId,AxisId,  KeylearningId,  LearningexpectedId);
        }

        #region RA

        /// <summary>
        /// Obtener los recursos filtrados por recientes, más visitados y relacionados
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="searchId"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetContentByTypeSearch(int nodeId, int currentPage, int sizePage, int searchId, int systemId)
        {
            return await _node.GetContentByTypeSearch(nodeId, currentPage, sizePage, searchId, systemId);
        }

        #endregion


    }
}
