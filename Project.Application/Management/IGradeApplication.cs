﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IGradeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddGrade(GradeSaveRequest request);
        Task<Response> UpdateGrade(GradeUpdateRequest request);
        Task<Response> DeleteGrade(int roleId);        
    }
}
