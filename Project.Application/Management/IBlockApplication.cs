﻿using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IBlockApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddBlock(BlockSaveRequest request);
        Task<Response> UpdateBlock(BlockUpdateRequest request);
        Task<Response> DeleteBlock(int roleId);        
    }
}
