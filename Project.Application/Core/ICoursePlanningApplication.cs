﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface ICoursePlanningApplication
    {
        Task<Response> Add(CoursePlanningSaveRequest request);
        Task<Response> Update(CoursePlanningUpdateRequest request);
        Task<Response> GetById(int id);
        Task<Response> GetAll();
        Task<Response> Delete(int id);
        Task<Response> GetByGroup(int id);
        Task<Response> GetByStudentId(int StudentId, int StudentGroupId);

    }
}
