﻿using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IVReportCourseAppliction
    {
        Task<Response> GetAll();
    }
}
