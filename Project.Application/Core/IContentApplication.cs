﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IContentApplication
    {
        Task<Response> GetAll();

        /// <summary>
        /// Obtener todos los contenidos registrados paginados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="request"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, PaginationContentRequest request, int systemId);

        /// <summary>
        /// Obtener un contenido por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Response> GetById(int id);

        /// <summary>
        /// Agregar un nuevo contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Response> AddContent(ContentSaveRequest request);

        /// <summary>
        /// Actualizar los datos de un contenido existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Response> UpdateContent(ContentUpdateRequest request);

        /// <summary>
        /// Eliminar un contenido existente
        /// </summary>
        /// <param name="ContentId"></param>
        /// <returns></returns>
        Task<Response> DeleteContent(int ContentId);

        /// <summary>
        /// Descargar el archivo de un contenido
        /// </summary>
        /// <param name="ContentId"></param>
        /// <returns></returns>
        Task<Response> DownloadContent(int ContentId);

        /// <summary>
        /// Actualizar el estatus de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Response> UpdateActiveStatus(ContentUpdateStatusRequest request);

        /// <summary>
        /// Asignar tags a un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Response> AssignatedTags(ContentAssingTagsRequest request);

        /// <summary>
        /// Actualizar los tags de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Response> UpdateTags(ContentTagsRequest request);

        /// <summary>
        /// Obtener contenido relacionado
        /// </summary>
        /// <param name="contentId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        Task<ResponsePagination> GetRelatedContent(int contentId, int currentPage, int sizePage, int systemId);

        /// <summary>
        /// Actualizar las visitas de un contenido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Response> UpdateVisits(int id);
    }
}
