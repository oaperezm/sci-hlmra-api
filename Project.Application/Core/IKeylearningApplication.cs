﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IKeylearningApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(KeylearningRequest request);
        Task<Response> Update(KeylearningUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
