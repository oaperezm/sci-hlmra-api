﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface ISchedulingPartialApplication
    {
        Task<Response> Add(SchedulingPartialSaveRequest request);
        Task<Response> Update(SchedulingPartialUpdateRequest request);
        Task<Response> GetById(int id);
        Task<Response> GetAll(int coursePlanningId);
        Task<Response> GetByStudentGroup(int StudentGroupId);
        Task<Response> Delete(int id, int coursePlanningId);        
    }
}
