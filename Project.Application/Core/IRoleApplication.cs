﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IRoleApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddRole(RoleSaveRequest request);
        Task<Response> UpdateRole(RoleSaveRequest request);
        Task<Response> DeleteRole(int roleId);
        Task<Response> GetRolePermissions(int id);
        Task<Response> UpdatePermissions(int id, List<int> permissions);
    }
}
