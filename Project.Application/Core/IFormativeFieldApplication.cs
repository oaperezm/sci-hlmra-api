﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IFormativeFieldApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddFormativeField(FormativeFieldSaveRequest request);
        Task<Response> UpdateFormativeField(FormativeFieldUpdateRequest request);
        Task<Response> DeleteFormativeField(int roleId);        
    }
}
