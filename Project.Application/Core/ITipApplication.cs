﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface ITipApplication
    {
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId);
        Task<Response> AddTip(TipSaveRequest request);
        Task<Response> UpdateTip(TipUpdateRequest request);
        Task<Response> DeleteTip(int id);
        Task<Response> GetById(int id);
    }
}
