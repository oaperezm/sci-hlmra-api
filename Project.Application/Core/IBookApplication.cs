﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IBookApplication
    {
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter);
        Task<Response> GetById(int id);
        Task<Response> AddBook(BookSaveRequest request);
        Task<Response> UpdateBook(BookUpdateRequest request);
        Task<Response> DeleteBook(int roleId);        
    }
}
