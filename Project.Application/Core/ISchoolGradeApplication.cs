﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface ISchoolGradeApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(SchoolGradeRequest request);
        Task<Response> Update(SchoolGradeUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
