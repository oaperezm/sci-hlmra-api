﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface IEventReportApplication
    {
        Task<Response> Add(EventsReportUpdateRequest request);
        Task<Response> Update(EventsReportUpdateRequest request);
        Task<Response> GetById(int id);
        Task<Response> GetAll();
        //Task<Response> GetReport(int Reportid,
        //                                      string report,
        //                                      string cct,
        //                                      string level,
        //                                      DateTime startDate,
        //                                      DateTime endDate,
        //                                      string section = "",
        //                                      string format = "",
        //                                      string resourcenName = "",
        //                                      string username = "");
        Task<Response> GetReport(ReportRequest request);
    }
}
