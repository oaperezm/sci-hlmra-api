﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IBlockApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddBlock(BlockSaveRequest request);
        Task<Response> UpdateBlock(BlockUpdateRequest request);
        Task<Response> DeleteBlock(int roleId);        
    }
}
