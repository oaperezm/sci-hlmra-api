﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IContentTypeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddContentType(ContentTypeSaveRequest request);
        Task<Response> UpdateContentType(ContentTypeUpdateRequest request);
        Task<Response> DeleteContentType(int roleId);        
    }
}
