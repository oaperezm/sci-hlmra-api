﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface ISectionApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(SectionsRequest request);
        Task<Response> Update(SectionsUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
