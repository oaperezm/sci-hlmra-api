﻿using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface IContentResourceTypeApplication
    {
        Task<Response> GetAll();
    }
}
