﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface ICourseApplication
    {
        Task<Response> GetAll();
        Task<Response> GetAllByIdUser(int userId);
        Task<Response> GetById(int id);
        Task<Response> Add(CourseSaveRequest request);
        Task<Response> Update(CourseUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetAllByIdUser(int userId, int syatemId);
        Task<Response> GetAllCourseGroup();
    }
}
