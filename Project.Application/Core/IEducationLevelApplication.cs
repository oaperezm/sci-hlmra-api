﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public  interface IEducationLevelApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(EducationLevelRequest request);
        Task<Response> Update(EducationLevelUpdateRequest request);
        Task<Response> Delete(int id);
    }
}
