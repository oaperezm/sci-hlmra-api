﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IPublishingProfileUserApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPublishingProfileUser(PublishingProfileUserSaveRequest request);
        Task<Response> UpdatePublishingProfileUser(PublishingProfileUserUpdateRequest request);
        Task<Response> DeletePublishingProfileUser(int id);
        Task<Response> GetByUserId(int userId);
    }
}
