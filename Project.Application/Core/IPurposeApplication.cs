﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IPurposeApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddPurpose(PurposeSaveRequest request);
        Task<Response> UpdatePurpose(PurposeUpdateRequest request);
        Task<Response> DeletePurpose(int roleId);        
    }
}
