﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface ILocationApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddLocation(LocationSaveRequest request);
        Task<Response> UpdateLocation(LocationUpdateRequest request);
        Task<Response> DeleteLocation(int roleId);        
    }
}
