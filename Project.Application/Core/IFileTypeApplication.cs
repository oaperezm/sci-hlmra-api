﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IFileTypeApplication
    {    
        Task<Response> GetAll(int systemId);
        Task<Response> GetById(int id);
        Task<Response> AddFileType(FileTypeSaveRequest request);
        Task<Response> UpdateFileType(FileTypeUpdateRequest request);
        Task<Response> DeleteFileType(int roleId);        
    }
}
