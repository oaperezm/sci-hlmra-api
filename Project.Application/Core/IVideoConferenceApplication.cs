﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
   public interface IVideoConferenceApplication
    {
              
        Task<Response> GetById(int id);
        Task<Response> GetNodo();
        Task<Response> Add(VideoConferenceSaveRequest request,int systemId, int userId);
        Task<Response> Update(VideoConferenceUpdateRequest request, int systemId);
        Task<Response> Delete(int id, int systemId);
        Task<Response> GetAll(int systemId, int UserId);
        Task<Response> Add(VideoConferenceSaveRequest request, InvitedVideoConferenceSaveRequest invited, InvitedVideoConferenceSaveRequest externalguest);
    }
}
