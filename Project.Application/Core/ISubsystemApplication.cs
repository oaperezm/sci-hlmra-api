﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface ISubsystemApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddSubsystem(SubsystemSaveRequest request);
        Task<Response> UpdateSubsystem(SubsystemUpdateRequest request);
        Task<Response> DeleteSubsystem(int roleId);        
    }
}
