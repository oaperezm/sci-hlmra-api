﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface ITrainingFieldApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddTrainingField(TrainingFieldSaveRequest request);
        Task<Response> UpdateTrainingField(TrainingFieldUpdateRequest request);
        Task<Response> DeleteTrainingField(int roleId);        
    }
}
