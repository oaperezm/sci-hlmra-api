﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Core
{
    public interface IGeneralEventApplication
    {
        Task<Response> Add(GeneralEventSaveRequest request);
        Task<Response> Update(GeneralEventUpdateRequest request);
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId);
        Task<Response> GetById(int Id);
        Task<Response> Delete(int id);
    }
}
