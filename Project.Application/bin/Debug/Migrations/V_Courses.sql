IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[V_Courses]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[V_Courses]
AS
select c.Id , 
	   c.Subject, 
	   c.NodeId, 
	   c.StartDate, 
	   c.EndDate, 
	   ( select count(Id) from Students s where StudentGroupId in (select Id from StudentGroups sg where CourseId = c.Id)) StudentCount, 
	   (SELECT STRING_AGG(Description, '', '') 
		FROM StudentGroups 
		WHERE CourseId = c.Id) Groups, 
	   c.UserId teachId,
	   c.Institution,
	   c.Name CourseName,
	   N.Name Publication
from Courses c
INNER JOIN Nodes N ON N.Id = c.NodeId'