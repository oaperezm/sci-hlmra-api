﻿using Project.Application.Core;

using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;
using Project.Domain.Core;

namespace Project.Application.VideoConference
{
    public class VideoConferenceApplication: IVideoConferenceApplication
    {
        private readonly IVideoconference _videoConference;

        public VideoConferenceApplication(IVideoconference videoconference)
        {
            this._videoConference = videoconference;
        }
        /// <summary>
        /// Método para agregar un  nuevo registro de video conferencia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(VideoConferenceSaveRequest request)
        {
            return _videoConference.Add(request);
        }
        /// <summary>
        /// Método que elimina de manera permanente un registro de video conferencia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _videoConference.Delete(id);
        }
        /// <summary>
        /// Método para obtener todos los registros de video conferencia 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _videoConference.GetAll();
        }

        

        /// <summary>
        /// Obtener los datos de un registro de video conferencia
        /// </summary>
        /// <param name="id">Identificador principal del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await _videoConference.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(VideoConferenceUpdateRequest request)
        {
            return await _videoConference.Update(request);

        }
    }
}
