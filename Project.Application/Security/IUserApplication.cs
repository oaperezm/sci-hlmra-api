﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IUserApplication
    {
        Task<Response> ConfirmSuscripcion(int email);
        Task<Response> ValidateUser(string username, string password, int systemId);
        Task<Response> GetAll();
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId);
        Task<ResponsePagination> GetAllwithSIIDE(int currentPage, int sizePage, int systemId);
        Task<Response> GetById(int id);
        Task<Response> RegisterUser(UserSaveRequest request);
        Task<Response> UpdateUser(UserUpdateRequest request);
        Task<Response> DeleteUser(int userId);
        Task<Response> RecoveryPassword(string email, string bodyHTML);
        Task<Response> ResetPassword(UserSavePasswordRequest request);
        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
        Task<Response> CopyProfile(int profileId, int[] userId);
        Task<Response> GetUserProfileData(int userId);
        Task<Response> GetByMail(string eMail);
        Task<Response> GetByPartialMail(string partialMail);
        Task<Response> GetByPartialMailRole(string partialMail, int roleId);
        Task<Response> LinkProfile(int profileId, int[] userId);
        Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId, int systemId);
        Task<bool> SaveRefreshToken(int userId, string refreshToken, string client, string protectedTicket);
        Task<Entities.User> GetByRegreshToken(string refreshToken);
        Task<Response> GetUserProfile(int userId, int systemId, int origin);
        Task<Response> ChangePassword(UserChangePasswordRequest request);
        Task<Response> UploadImgProfile(UserUploadImgProfileRequest request);
        Task<Response> LogOut(int userId);
        Task<Response> GetRolesByUserEmail(string email);
        Task<Response> ValidateUserSIDE(string email);
        Task<Response> ValidateCCTUserSIDE(string email);
        

        Task<Response> RegisterDevice(UserDeviceSaveRequest request, int userId);
        Task<Response> GetUnReadNotifications(int userId);
        Task<Response> GetAllNotifications(int userId);
        Task<ResponsePagination> GetAllNotifications(int userId, int currentPage, int sizePage);
        Task<Response> CheckReadNotification(int notificationId, int userId);


        #region cct

        Task<Response> GetCCT();
        #endregion
    }
}
