﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PermissionApplication : IPermissionApplication
    {
        private readonly IPermission _permission;

        public PermissionApplication(IPermission permission)
        {

            this._permission = permission;
        }

        /// <summary>
        /// Agregar un permiso nuevo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddPermission(PermissionSaveRequest request)
        {
            return this._permission.AddPermission(request);
        }

        /// <summary>
        /// Eliminar un permiso existente que no este asignado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeletePermission(int id)
        {
            return await _permission.DeletePermission(id);
        }

        /// <summary>
        /// Obtener el listado de los permisos
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll(int systemId)
        {
            return await this._permission.GetAll(systemId);
        }

        /// <summary>
        /// Obtener todos los permisos por paginación
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            return await _permission.GetAll(currentPage, sizePage, filter, systemId);
        }

        /// <summary>
        /// Obtener permiso por identificaodr
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return this._permission.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un permiso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdatePermission(PermissionSaveRequest request)
        {
            return this._permission.UpdatePermission(request);
        }

    }
}
