﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class UserApplication : IUserApplication
    {
        private readonly IUser _user;

        public UserApplication(IUser user) {

            this._user = user;
        }
        public Task<Response> ConfirmSuscripcion(int email)
        {
            return this._user.ConfirmSuscripcion(email);
        }
        public Task<Response> RegisterUser(UserSaveRequest request)
        {
            return this._user.RegisterUser(request);
        }

        public async Task<Response> DeleteUser(int userId)
        {
            return await _user.DeleteUser(userId);
        }

        public async Task<Response> GetAll()
        {
            return await this._user.GetAll();
        }

        public async Task<ResponsePagination> GetAllwithSIIDE(int currentPage, int sizePage, int systemId)
        {
            return await _user.GetAllwithSIIDE(currentPage, sizePage, systemId);
        }

        public async Task<Response> GetById(int id)
        {
            return await _user.GetById(id);
        }

        public Task<Response> RecoveryPassword(string email, string bodyHTML)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> ResetPassword(UserSavePasswordRequest request)
        {
            return await _user.ResetPassword(request);
        }

        public async Task<Response> UpdateUser(UserUpdateRequest request)
        {
            return await _user.UpdateUser(request);
        }

        public async Task<Response> ValidateUser(string username, string password, int systemId)
        {
            return await _user.ValidateUser(username, password, systemId);
        }

        public Task<Response> GetNodes(int id)
        {
            return this._user.GetNodes(id);
        }

        public Task<Response> AddNodes(int id, List<int> nodes)
        {
            return this._user.AddNodes(id, nodes);
        }

        public Task<Response> CopyProfile(int profileId, int[] userIds)
        {
            return this._user.CopyProfile(profileId, userIds);
        }

        public async Task<Response> GetUserProfile(int userId, int systemId, int origin)
        {

            return await this._user.GetUserProfile(userId, systemId,origin);

        }

        public async Task<Response> GetByMail(string eMail)
        {
            return await this._user.GetByMail(eMail);
        }

        public async Task<Response> GetByPartialMail(string partialMail)
        {
            return await this._user.GetByPartialMail(partialMail);
        }

        public async Task<Response> GetByPartialMailRole(string partialMail, int roleId)
        {
            return await this._user.GetByPartialMailRole(partialMail, roleId);
        }

        public Task<Response> LinkProfile(int profileId, int[] userIds)
        {
            return this._user.LinkProfile(profileId, userIds);
        }

        
        public Task<Response> GetByMailRoleCourse(string partialMail, int roleId, int courseId, int systemId)
        {
            return this._user.GetByMailRoleCourse(partialMail, roleId, courseId, systemId);
        }

        /// <summary>
        /// Guardar el refresht token en la base de datos
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="refreshToken"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public Task<bool> SaveRefreshToken(int userId, string refreshToken, string client, string protectedTicket)
        {
            return this._user.SaveRefreshToken(userId, refreshToken, client, protectedTicket);
        }

        /// <summary>
        /// Obtenerusuario por el refresh token
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public async Task<Entities.User> GetByRegreshToken(string refreshToken)
        {
            return await this._user.GetByRegreshToken(refreshToken);
        }

        /// <summary>
        /// Obtener los datos de un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUserProfileData(int userId)
        {
            return await this._user.GetUserProfileData(userId);
        }

        /// <summary>
        /// Permite cambiar la contraseña del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> ChangePassword(UserChangePasswordRequest request)
        {
            return await _user.ChangePassword(request);
        }

        /// <summary>
        /// Permite actualizar la imagen de perfil del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UploadImgProfile(UserUploadImgProfileRequest request)
        {
            return await _user.UploadImgProfile(request);
        }
       
        /// <summary>
        /// Elimina los datos de sesión del usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> LogOut(int userId)
        {
            return await this._user.LogOut(userId);
        }

        /// <summary>
        /// Obtener roles por sistema
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Response> GetRolesByUserEmail(string email)
        {
            return await _user.GetRolesByUserEmail(email);
        }

        #region SIDE

        /// <summary>
        /// Obtener datos del usuario desde SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Response> ValidateUserSIDE(string email)
        {
            return await _user.ValidateUserSIDE(email);
        }

        /// <summary>
        /// Obtener datos del usuario desde SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Response> ValidateCCTUserSIDE(string email)
        {
            return await _user.ValidateCCTUserSIDE(email);
        }

        #endregion

        #region NOTIFICATIONS

        /// <summary>
        /// Agregar un nuevo dispositivos
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> RegisterDevice(UserDeviceSaveRequest request, int userId)
        {
            return await _user.RegisterDevice(request, userId);
        }

        /// <summary>
        /// Obtener todas las notificaciones no leidas por el usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetUnReadNotifications(int userId)
        {
            return await _user.GetUnReadNotifications(userId);
        }

        /// <summary>
        /// Obtener todas las notificaciones de un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllNotifications(int userId)
        {
            return await _user.GetAllNotifications(userId);
        }

        /// <summary>
        /// Obtener todas las notificaciones del usuario con paginación
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAllNotifications(int userId, int currentPage, int sizePage)
        {
            return await _user.GetAllNotifications(userId, currentPage, sizePage);
        }

        /// <summary>
        /// Obtener todos los usuarios con paginación
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            return await _user.GetAll(currentPage, sizePage, filter, systemId);
        }

        /// <summary>
        /// Actualizar el estatus de los mensajes
        /// </summary>
        /// <param name="notificationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> CheckReadNotification(int notificationId, int userId)
        {
            return await _user.CheckReadNotification(notificationId, userId);
        }

        public async Task<Response> GetCCT()
        {
            return await _user.GetCCT();
        }

        #endregion
    }
}
