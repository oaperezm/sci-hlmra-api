﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.Management
{
    public interface IPublishingProfileApplication
    {    
        Task<Response> GetAll(int systemId);
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, bool? active, int systemId);
        Task<Response> GetById(int id);
        Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request);
        Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request);
        Task<Response> DeletePublishingProfile(int id);
        Task<Response> GetNodes(int id);
        Task<Response> AddNodes(int id, List<int> nodes);
        Task<Response> GetLinkedUsers(int profileId, int systemId);
    }
}
