﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class PublishingProfileApplication : IPublishingProfileApplication
    {
        private readonly IPublishingProfile _PublishingProfile;

        public PublishingProfileApplication(IPublishingProfile PublishingProfile)
        {

            this._PublishingProfile = PublishingProfile;
        }

        public Task<Response> AddPublishingProfile(PublishingProfileSaveRequest request)
        {
            return this._PublishingProfile.AddPublishingProfile(request);
        }

        public Task<Response> DeletePublishingProfile(int id)
        {
            return this._PublishingProfile.DeletePublishingProfile(id);
        }

        public Task<Response> GetAll(int systemId)
        {
            return this._PublishingProfile.GetAll(systemId);
        }

        public Task<Response> GetById(int id)
        {
            return this._PublishingProfile.GetById(id);
        }

        public Task<Response> UpdatePublishingProfile(PublishingProfileUpdateRequest request)
        {
            return this._PublishingProfile.UpdatePublishingProfile(request);
        }

        public Task<Response> GetNodes(int id)
        {
            return this._PublishingProfile.GetNodes(id);
        }

        public Task<Response> AddNodes(int id, List<int> nodes)
        {
            return this._PublishingProfile.AddNodes(id, nodes);
        }

        public Task<Response> GetLinkedUsers(int profileId, int systemId)
        {
            return this._PublishingProfile.GetLinkedUsers(profileId, systemId);
        }

        /// <summary>
        /// Obtener todos los perfiles de publicación registrados paginados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="active"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, bool? active, int systemId)
        {
            return await _PublishingProfile.GetAll(currentPage, sizePage, filter, active, systemId);
        }
    }
}
