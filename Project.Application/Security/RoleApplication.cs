﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Management
{
    public class RoleApplication : IRoleApplication
    {
        private readonly IRole _role;

        public RoleApplication(IRole role)
        {

            this._role = role;
        }

        public Task<Response> AddRole(RoleSaveRequest request)
        {
            return this._role.AddRole(request);
        }

        public  async Task<Response> DeleteRole(int id)
        {
            return await _role.DeleteRole(id);
        }

        public Task<Response> GetAll(int systemId)
        {
            return this._role.GetAll(systemId);
        }

        public Task<Response> GetById(int id)
        {
            return this._role.GetById(id);
        }

        public Task<Response> UpdateRole(RoleSaveRequest request)
        {
            return this._role.UpdateRole(request);
        }

        public Task<Response> GetRolePermissions(int id, int systemId)
        {
            return this._role.GetRolePermissions(id, systemId);
        }

        public Task<Response> UpdatePermissions(int id, List<int> permissions)
        {
            return this._role.UpdatePermissions(id, permissions);
        }

        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            return await _role.GetAll(currentPage, sizePage, filter, systemId);
        }
    }
}
