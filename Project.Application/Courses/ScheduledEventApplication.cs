﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class ScheduledEventApplication: IScheduledEventApplication
    {
        private readonly IScheduledEvent _scheduledEvent;

        public ScheduledEventApplication(IScheduledEvent scheduledEvent) {
            this._scheduledEvent = scheduledEvent;
        }

        /// <summary>
        /// Agregar una programacion de evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ScheduledEventSaveRequest request)
        {
            return await _scheduledEvent.Add(request);
        }

        /// <summary>
        /// Eliminar los datos de una programacion de evento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _scheduledEvent.Delete(id);
        }

        /// <summary>
        /// Obtener todos los eventos de un curso
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Response> GetByCourse(int courseId)
        {
            return await _scheduledEvent.GetByCourse(courseId);
        }

        /// <summary>
        /// Obtener eventos del estudiante
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int userId, DateTime month)
        {
            return await _scheduledEvent.GetByStudent(userId, month);
        }

        /// <summary>
        /// Obtener eventos del maestro
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<Response> GetByTeacher(int userId, DateTime month)
        {
            return await _scheduledEvent.GetByTeacher(userId, month);            
        }

        /// <summary>
        /// Actualizar los datos de un evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(ScheduledEventUpdateRequest request)
        {
            return await _scheduledEvent.Update(request);
        }
    }
}
