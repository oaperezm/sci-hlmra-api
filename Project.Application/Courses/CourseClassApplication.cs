﻿using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class CourseClassApplication : ICourseClassApplication
    {
        private readonly ICourseClass _CourseClass;

        public CourseClassApplication(ICourseClass CourseClass)
        {
            this._CourseClass = CourseClass;
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(CourseClassSaveRequest request)
        {
            return _CourseClass.Add(request);
        }


        public async Task<Response> Delete(int id)
        {
            return await _CourseClass.Delete(id);
        }

        /// <summary>
        /// Método para obtener todos los cursos registrados 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _CourseClass.GetAll();
        }

        /// <summary>
        /// Obtener los datos de un curso 
        /// </summary>
        /// <param name="id">Identificador principal del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await _CourseClass.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseClassUpdateRequest request)
        {
            return await _CourseClass.Update(request);

        }

        /// <summary>
        /// Método para obtener todos los cursos registrados 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetBySectionId(int courseSectionId)
        {
            return await _CourseClass.GetBySectionId(courseSectionId);
        }

        /// <summary>
        /// Asignar archivos a la clase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddFiles(CourseClassFilesRequest request)
        {
            return await _CourseClass.AddFiles(request);
        }

        /// <summary>
        /// Eliminar archivos asignados a la clase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFiles(CourseClassFilesRequest request)
        {
            return await _CourseClass.RemoveFiles(request);
        }
    }
}
