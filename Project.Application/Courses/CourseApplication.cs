﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class CourseApplication : ICourseApplication
    {
        private readonly ICourse _course;

        public CourseApplication(ICourse course) {
            this._course = course;
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(CourseSaveRequest request)
        {
            return _course.Add(request);
        }


        /// <summary>
        /// Método que elimina de manera permanente un curso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _course.Delete(id);
        }

        /// <summary>
        /// Método para obtener todos los cursos registrados 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _course.GetAll();
        }

        /// <summary>
        /// Método para obtener todos los cursos dados de alta por un profesor
        /// </summary>
        /// <param name="userId">Identificador del profesor</param>
        /// <returns></returns>
        public  async Task<Response> GetAllByIdUser(int userId)
        {
            return await _course.GetAllByIdUser(userId);
        }

        public async Task<Response> GetAllByIdUser(int userId, int syatemId)
        {
            return await _course.GetAllByIdUser(userId, syatemId);
        }

        public async Task<Response> GetAllCourseGroup()
        {
            return await _course.GetAllCourseGroup();
        }

        /// <summary>
        /// Obtener los datos de un curso 
        /// </summary>
        /// <param name="id">Identificador principal del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await _course.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseUpdateRequest request)
        {
            return await _course.Update(request);

        }

    }
}
