﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class EventApplication: IEventApplication
    {
        private readonly IEvent _events;

        public EventApplication(IEvent events)
        {
            this._events = events;
        }
        /// <summary>
        /// Método para agregar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public  Task<Response> Add(EventRequest request)
        {
            return _events.Add(request);
        }
        /// <summary>
        /// Método para eliminar un evento
        /// </summary>
        /// <param name="id"
        /// <returns></returns>
        public Task<Response> Delete(int id)
        {
            return _events.Delete(id);
        }
    }
}
