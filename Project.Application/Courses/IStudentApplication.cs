﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IStudentApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddStudent(StudentSaveRequest request);
        Task<Response> UpdateStudent(StudentUpdateRequest request);
        Task<Response> DeleteStudent(int roleId);
        Task<Response> GetStudentsByGroup(int studentGroupId, int examScheduleId);
        Task<Response> SendInvitation(StudentSendInvitationRequest request);
        Task<Response> GetCourseByStudentAndBlock(int usuarioId, int bloqueId);
        Task<Response> ConfirmInvitation(string codigo);
        Task<Response> AddInvitation(StudentSendInvitationRequest request, int id);
    }
}
