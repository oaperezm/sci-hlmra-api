﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IStudentGroupApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddStudentGroup(StudentGroupSaveRequest request);
        Task<Response> UpdateStudentGroup(StudentGroupUpdateRequest request);
        Task<Response> DeleteStudentGroup(int roleId);
        Task<Response> GetGroupsByCourse(int courseId);
        Task<Response> AddStudentGroupSystem(StudentGroupSaveRequest request, int systemId);
        Task<Response> DeleteStudentGroupSystem(int id, int systemId);


    }
}
