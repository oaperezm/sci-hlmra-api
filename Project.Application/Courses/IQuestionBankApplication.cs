﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IQuestionBankApplication
    {
        Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int rolId, int userId, int systemId);
        Task<Response> GetById(int id, bool converImageBase64 = false);
        Task<Response> AddQuestionBank(QuestionBankSaveRequest request, int userId);
        Task<Response> UpdateQuestionBank(QuestionBankUpdateRequest request);
        Task<Response> DeleteQuestionBank(int roleId);
        Task<Response> UpdateStatusQuestionBank(QuestionBankStatusRequest request);
        Task<Response> GetByNodeId(int nodeId, int userId, int systemId);
        Task<Response> GetStructuraWithQuestionBank(int userId, int systemId);
        Task<Response> GetByListIds(List<int> ids);
        Task<Response> SaveLoad(QuestionBankLoadRequest request);
    }
}
