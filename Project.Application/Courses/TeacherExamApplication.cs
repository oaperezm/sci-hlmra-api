﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class TeacherExamApplication : ITeacherExamApplication
    {
        private readonly ITeacherExam _teacherExam;

        public TeacherExamApplication(ITeacherExam teacherExam)
        {
            this._teacherExam = teacherExam;
        }

        /// <summary>
        /// Agregar un nuevo examen 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TeacherExamSaveRequest request)
        {
            return await this._teacherExam.Add(request);
        }

        /// <summary>
        /// Eliminar los datos de un examen de manera definitiva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> Delete(int id)
        {
            return this._teacherExam.Delete(id);
        }

        /// <summary>
        /// Obtener los examenes registrado por profesor paginados
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="type"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<ResponsePagination> GetAllByUserPagination(int currentPage, int sizePage, string filter, int type, int userId)
        {
            return _teacherExam.GetAllByUserPagination(currentPage, sizePage, filter, type, userId);
        }

        /// <summary>
        /// Obtener toda la información de un exámen por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id, bool converImageBase64 = false)
        {
            return await this._teacherExam.GetById(id, converImageBase64);
        }

        /// <summary>
        /// Obtener toda la información de un exámen programado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetExamSchedule(int id, int examScheduleId,int userid, bool converImageBase64 = false)
        {
            return await this._teacherExam.GetExamSchedule(id, examScheduleId, userid, converImageBase64);

        }

        /// <summary>
        /// Iniciar un exámen programado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> StartExamSchedule(int id, int examScheduleId, int userId, bool converImageBase64 = false)
        {
            return await this._teacherExam.StartExamSchedule(id, examScheduleId, userId, converImageBase64);

        }

        /// <summary>
        /// Obtener todos los examenes dados de alta por un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserId(int userId)
        {
            return await this._teacherExam.GetByUserId(userId);
        }

        /// <summary>
        /// Obtener los examenes registrado por profesor
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserIdShortInfo(int userId)
        {
            return await this._teacherExam.GetByUserIdShortInfo(userId);
        }

        /// <summary>
        /// Actualizar los datos de un exámen existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(TeacherExamUpdateRequest request)
        {
            return await _teacherExam.Update(request);
        }

        /// <summary>
        /// Actualiza la ponderación del examen y preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateWeighting(TeacherExamUpdateRequest request)
        {
            return await _teacherExam.UpdateWeighting(request);
        }
    }
}
