﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class LinkInterestApplication : ILinkInterestApplication
    {
        private readonly ILinkInterest _linkInterest;

        public LinkInterestApplication(ILinkInterest linkInterest) {
            _linkInterest = linkInterest;
        }

        /// <summary>
        /// Agregar un nuevo sitio de interés 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(LinkInterestSaveRequest request)
        {
            return await _linkInterest.Add(request);
        }

        /// <summary>
        /// Eliminar un sitio de interés
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _linkInterest.Delete(id);
        }

        /// <summary>
        /// Obtener todos los sitios de interés de por curso
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Response> GetByCourse(int courseId)
        {
            return await _linkInterest.GetByCourse(courseId);
        }

        /// <summary>
        /// Obtener todos los sitios de interés de un libro
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int userId, int nodeId)
        {
            return await _linkInterest.GetByStudent(userId, nodeId);
        }

        /// <summary>
        /// Actualizar los datos de un sitio de interés 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(LinkInterestUpdateRequest request)
        {
            return await _linkInterest.Update(request);
        }
    }
}
