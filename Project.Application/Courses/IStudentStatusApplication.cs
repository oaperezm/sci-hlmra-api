﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IStudentStatusApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddStudentStatus(StudentStatusSaveRequest request);
        Task<Response> UpdateStudentStatus(StudentStatusUpdateRequest request);
        Task<Response> DeleteStudentStatus(int roleId);        
    }
}
