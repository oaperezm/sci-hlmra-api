﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class ActivityApplication : IActivityApplication
    {
        private readonly IActivity _activity;

        public ActivityApplication(IActivity activity)
        {
            this._activity = activity;
        }

        /// <summary>
        /// Obtener todas las actividades de un profesor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByGroup(int id)
        {
            return await _activity.GetAllByGroup(id);
        }

        #region ACTIVITY
        /// <summary>
        /// Agregar una nueva actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(ActivitySaveRequest request)
        {
            return await _activity.Add(request);
        }

        /// <summary>
        /// Actualizar los datos de una actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(ActivityUpdateRequest request)
        {
            return await _activity.Update(request);
        }

        /// <summary>
        /// Eliminar una programación de actividad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Remove(int id)
        {
            return await _activity.Remove(id);
        }

        /// <summary>
        /// Remover un archivo de una actividad
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFile(ActivityRemoveFileRequest request)
        {
            return await _activity.RemoveFile(request);
        }
        #endregion

        #region ANSWER

        /// <summary>
        /// Agregar archivo a la respuesta de la tarea del alumnio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddAnswer(ActivityAnswerSaveRequest request)
        {
            return await _activity.AddAnswer(request);
        }

        /// <summary>
        /// Eliminar el archivo de la respuesta de una tarea del alumno
        /// </summary>
        /// <param name="activityId"></param>
        /// <param name="userId"></param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAnswer(int activityId, int userId, int answerId)
        {
            return await _activity.DeleteAnswer(activityId, userId, answerId);
        }
         
        /// <summary>
        /// Obtener todas las calificaciones
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllWithScore(int studentGroupId, int userId)
        {
            return await _activity.GetAllWithScore(studentGroupId, userId);
        }

        /// <summary>
        /// Guardar las calificaciones de la tarea de un grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveScore(ActivityScoreRequest request)
        {
            return await _activity.SaveScore(request);
        }

        /// <summary>
        /// Obtener los archivos del alumno de una tarea
        /// </summary>
        /// <param name="activityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByActivity(int activityId, int userId)
        {
            return await _activity.GetStundentFileByActivity(activityId, userId);
        }


        /// <summary>
        /// Obtener los alumnos y sus respuestas d euna tarea en especifico
        /// </summary>
        /// <param name="studenGroupId"></param>
        /// <param name="activityId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsAnswers(int studenGroupId, int activityId)
        {
            return await _activity.GetStudentsAnswers(studenGroupId, activityId);
        }

        /// <summary>
        /// Cerrar la tarea programada para no dejar calificar 
        /// </summary>
        /// <param name="activityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int activityId, bool status)
        {
            return await _activity.UpdateStatus(activityId, status);
        }

        #endregion

    }
}
