﻿using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IStudentScoreApplication
    {
        Task<Response> GetAllByGroup(int studentGroupId);
        Task<Response> GetAllByGroupDetailsExcel(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO);//Ricardo - Agregado
        Task<Response> GetByPartialId(int studentGroupId, int Partialid, int userID);
        Task<Response> GetAllPartials(int studentGroupId, int userID);
        Task<Response> UpdateStudentScore(StudentScoreUpdateRequest request);
        Task<Response> GetStudentsScoresByGroup(int studentGroupId, int userID);
        Task<Response> SaveStudentScore(StudentScoreSaveRequest request);
        List<StudentFinalScoreDTO> GetScoreGeneral(int studentGroupId, int userId);


    }
}
