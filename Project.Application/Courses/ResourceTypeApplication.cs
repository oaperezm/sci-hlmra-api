﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class ResourceTypeApplication : IResourceTypeApplication
    {
        private readonly IResourceType _resourceType;

        public ResourceTypeApplication(IResourceType resourceType) {
            this._resourceType = resourceType;
        }

        /// <summary>
        /// Obtener todos los tipos de recursos registrados
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await this._resourceType.GetAll();
        }
    }
}
