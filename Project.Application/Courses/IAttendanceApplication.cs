﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IAttendanceApplication
    {
        Task<Response> SaveAttendances(AttendanceSaveRequest request);
        Task<Response> GetAttendances(DateTime initDate, DateTime endDate, int studentGroupId);
    }
}
