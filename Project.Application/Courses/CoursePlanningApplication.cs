﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class CoursePlanningApplication : ICoursePlanningApplication
    {
        private readonly ICoursePlanning _coursePlanning;

        public CoursePlanningApplication(ICoursePlanning coursePlanning)
        {
            this._coursePlanning = coursePlanning;
        }
        /// <summary>
        /// Método para agregar un nuevo plan
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(CoursePlanningSaveRequest request)
        {
            return _coursePlanning.Add(request);
        }

        public Task<Response> Delete(int id)
        {
            return _coursePlanning.Delete(id);
        }

        public Task<Response> GetAll()
        {
            return _coursePlanning.GetAll();
        }

        public Task<Response> GetByGroup(int id)
        {
            return _coursePlanning.GetByGroup(id);
        }

        public Task<Response> GetById(int id)
        {
            return _coursePlanning.GetById(id);
        }

        public Task<Response> GetByStudentId(int StudentId, int StudentGroupId)
        {
            return _coursePlanning.GetByStudentId(StudentId, StudentGroupId);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CoursePlanningUpdateRequest request)
        {
            return await _coursePlanning.Update(request);

        }
    }
}
