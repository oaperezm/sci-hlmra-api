﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class StudentGroupApplication : IStudentGroupApplication
    {
        private readonly IStudentGroup _StudentGroup;

        public StudentGroupApplication(IStudentGroup StudentGroup)
        {

            this._StudentGroup = StudentGroup;
        }

        public Task<Response> AddStudentGroup(StudentGroupSaveRequest request)
        {
            return this._StudentGroup.AddStudentGroup(request);
        }

        /// <summary>
        /// Eliminar los datos de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteStudentGroup(int id)
        {
            return await _StudentGroup.DeleteStudentGroup(id);
        }

        public Task<Response> GetAll()
        {
            return this._StudentGroup.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._StudentGroup.GetById(id);
        }

        public Task<Response> UpdateStudentGroup(StudentGroupUpdateRequest request)
        {
            return this._StudentGroup.UpdateStudentGroup(request);
        }

        public Task<Response> GetGroupsByCourse(int courseId)
        {
            return this._StudentGroup.GetGroupsByCourse(courseId);
        }
        /// <summary>
        /// Eliminar los datos de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> AddStudentGroupSystem(StudentGroupSaveRequest request, int systemId)
        {
            return this._StudentGroup.AddStudentGroupSystem(request, systemId);
        }

        public async Task<Response> DeleteStudentGroupSystem(int id, int systemId)
        {
            return await _StudentGroup.DeleteStudentGroupSystem(id, systemId);
        }
    }
}
