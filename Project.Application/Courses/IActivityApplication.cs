﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IActivityApplication
    {
        Task<Response> GetAllByGroup(int id);
        Task<Response> Add(ActivitySaveRequest request);
        Task<Response> Update(ActivityUpdateRequest request);
        Task<Response> RemoveFile(ActivityRemoveFileRequest request);
        Task<Response> Remove(int id);
        Task<Response> GetStudentsAnswers(int studenGroupId, int activityId);
        Task<Response> UpdateStatus(int activityId, bool status);

        // ANSWER
        Task<Response> AddAnswer(ActivityAnswerSaveRequest request);
        Task<Response> DeleteAnswer(int activityId, int userId, int answerId);
        Task<Response> GetStundentFileByActivity(int activityId, int userId);
        Task<Response> SaveScore(ActivityScoreRequest request); 
        Task<Response> GetAllWithScore(int studentGroupId, int userId);
    }
}
