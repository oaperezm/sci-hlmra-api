﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface ICourseSectionApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(CourseSectionSaveRequest request);
        Task<Response> Update(CourseSectionUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetByCourseId(int courseId);
    }
}
