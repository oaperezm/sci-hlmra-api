﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class QuestionApplication : IQuestionApplication
    {
        private readonly IQuestion _Question;

        public QuestionApplication(IQuestion Question)
        {

            this._Question = Question;
        }

        public Task<Response> AddQuestion(QuestionSaveRequest request)
        {
            return this._Question.AddQuestion(request);
        }

        public async Task<Response> DeleteQuestion(int questionId)
        {
            return await this._Question.DeleteQuestion(questionId);
        }

        public Task<Response> GetAll()
        {
            return this._Question.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Question.GetById(id);
        }

        public Task<Response> UpdateQuestion(QuestionUpdateRequest request)
        {
            return this._Question.UpdateQuestion(request);
        }

    }
}
