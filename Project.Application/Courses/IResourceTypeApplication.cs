﻿using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IResourceTypeApplication
    {
        Task<Response> GetAll();
    }
}
