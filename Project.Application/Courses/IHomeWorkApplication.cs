﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IHomeWorkApplication
    {
        Task<Response> GetAllByGroup(int id);
        Task<Response> Add(HomeWorkSaveRequest request);
        Task<Response> Update(HomeWorkUpdateRequest request);
        Task<Response> RemoveFile(HomeWorkRemoveFileRequest request);
        Task<Response> Remove(int id);
        Task<Response> GetStudentsAnswers(int studenGroupId, int homeworkId);
        Task<Response> UpdateStatus(int homeworkId, bool status);

        // ANSWER
        Task<Response> AddAnswer(HomeWorkAnswerSaveRequest request);
        Task<Response> DeleteAnswer(int homeWorkId, int userId, int answerId);
        Task<Response> GetStundentFileByHomeWork(int homeWorkId, int userId);
        Task<Response> SaveScore(HomeWorkScoreRequest request);

        Task<Response> GetAllWithScore(int studentGroupId, int userId);
    }
}
