﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class MessagesApplication: IMessagesApplication
    {
        private readonly IMessages _message;

        public MessagesApplication(IMessages message) {
            _message = message;
        }

        /// <summary>
        /// Obtiene todas las conversaciones dle usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<Response> GetAllConversations(int userId)
        {
            return _message.GetAllConversations(userId);
        }

        /// <summary>
        /// Obtener los mensajes por grupo
        /// </summary>
        /// <param name="chatGroupId"></param>
        /// <returns></returns>
        public  async Task<Response> GetByChatGroup(int chatGroupId, int userIdd)
        {
            return await _message.GetByChatGroup(chatGroupId,userIdd);
        }

        /// <summary>
        /// Obtener los contactos del usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rolId"></param>
        /// <returns></returns>
        public Task<Response> GetContact(int userId, int rolId)
        {
            return _message.GetContact(userId, rolId);
        }

        /// <summary>
        /// Marcar los mensajes leidos
        /// </summary>
        /// <param name="reciverUserId"></param>
        /// <param name="read"></param>
        /// <returns></returns>
        public async Task<Response> MarkRead(int reciverUserId, bool read)
        {
            return await _message.MarkRead(reciverUserId, read);
        }

        /// <summary>
        /// Enviar un mensaje
        /// </summary>
        /// <param name="senderUserId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SendMessage(int senderUserId, SendMessageRequest request)
        {
            return await _message.SendMessage(senderUserId, request);
        }
    }
}
