﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IAnswerApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddAnswer(AnswerSaveRequest request);
        Task<Response> UpdateAnswer(AnswerUpdateRequest request);
        Task<Response> DeleteAnswer(int roleId);        
    }
}
