﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class SchedulingPartialApplication : ISchedulingPartialApplication
    {
        private readonly ISchedulingPartial _schedulingPartial;

        /// <summary>
        /// Constructor de la Clase
        /// </summary>
        /// <param name="schedulingPartial"></param>
        public SchedulingPartialApplication(ISchedulingPartial schedulingPartial)
        {
            this._schedulingPartial = schedulingPartial;
        }

        /// <summary>
        /// Agrega la configuración del parcial de grupo y curso indicado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(SchedulingPartialSaveRequest request)
        {
            return await this._schedulingPartial.Add(request);
        }

        /// <summary>
        /// Actualiza la configuración del parcial de grupo y curso indicado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(SchedulingPartialUpdateRequest request)
        {
            return await this._schedulingPartial.Update(request);
        }

        /// <summary>
        /// Obtiene la lista de parciales del curso y grupo indicado
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll(int coursePlanningId)
        {
            return await this._schedulingPartial.GetAll(coursePlanningId);
        }

        /// <summary>
        /// Obtiene la información del parcial indicado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await this._schedulingPartial.GetById(id);
        }

        /// <summary>
        /// Borra la configuración del parcial del curso y grupo indicado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id, int coursePlanningId)
        {
            return await this._schedulingPartial.Delete(id, coursePlanningId);
        }

        /// <summary>
        /// Obtiene la lista de parciales del grupo indicado
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetByStudentGroup(int StudentGroupId)
        {
            return await this._schedulingPartial.GetByStudentGroup(StudentGroupId);

        }
        
    }
}
