﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IAnnouncementApplication
    {
        Task<Response> Add(AnnouncementSaveRequest request);
        Task<Response> Update(AnnouncementUpdateRequest request);
        Task<Response> Delete(int id);       
        Task<Response> GetById(int userId, int announcementId);
        Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId);


    }
}
