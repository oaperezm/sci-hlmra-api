﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class AnswerApplication : IAnswerApplication
    {
        private readonly IAnswer _Answer;

        public AnswerApplication(IAnswer Answer)
        {

            this._Answer = Answer;
        }

        public Task<Response> AddAnswer(AnswerSaveRequest request)
        {
            return this._Answer.AddAnswer(request);
        }

        public async Task<Response> DeleteAnswer(int id)
        {
            return  await this._Answer.DeleteAnswer(id);
        }

        public Task<Response> GetAll()
        {
            return this._Answer.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Answer.GetById(id);
        }

        public Task<Response> UpdateAnswer(AnswerUpdateRequest request)
        {
            return this._Answer.UpdateAnswer(request);
        }

    }
}
