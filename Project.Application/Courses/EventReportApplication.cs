﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
   public class EventReportApplication : IEventReportApplication
    {
        private readonly IEventsReport _eventReport;

        public EventReportApplication(IEventsReport eventsReport)
        {
            this._eventReport = eventsReport;
        }

        public Task<Response> Add(EventsReportUpdateRequest request)
        {
            return _eventReport.Add(request);
        }

        public Task<Response> GetAll()
        {
            return _eventReport.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return _eventReport.GetById(id);
        }

        public Task<Response> GetReport(ReportRequest request)
        {
            return _eventReport.GetReport(request);
        }

        public Task<Response> Update(EventsReportUpdateRequest request)
        {
            return _eventReport.Update(request);
        }
    }
}
