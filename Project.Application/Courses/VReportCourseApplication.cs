﻿using Project.Domain.Courses;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VReportCourseApplication: IVReportCourseAppliction
    {
        private readonly IVReportCourse _vReportCourse;

        public VReportCourseApplication(IVReportCourse vReportCourse)
        {
            _vReportCourse = vReportCourse;
        }

        public Task<Response> GetAll()
        {
            return this._vReportCourse.GetAll();
        }
    }
}
