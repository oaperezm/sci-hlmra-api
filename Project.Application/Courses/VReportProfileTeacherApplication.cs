﻿using Project.Domain.Courses;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VReportProfileTeacherApplication: IVReportProfileTeacherApplication
    {

        private readonly IVReportProfileTeacher iVReportProfileTeacher;

        public VReportProfileTeacherApplication(IVReportProfileTeacher vReportProfileTeacher)
        {
            iVReportProfileTeacher = vReportProfileTeacher;
        }

        public Task<Response> GetAll()
        {
            return this.iVReportProfileTeacher.GetAll();
        }
    }
}
