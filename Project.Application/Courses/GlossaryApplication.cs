﻿using System;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class GlossaryApplication : IGlossaryApplication
    {

        private readonly IGlossary _glossary;

        public GlossaryApplication(IGlossary glossary) {
            _glossary = glossary;
        }

        /// <summary>
        /// Agregar un nuevo glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(GlossarySaveRequest request)
        {
            return await _glossary.Add(request);
        }

        /// <summary>
        /// Agregar un nuevo termino al glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddWord(WordSaveRequest request)
        {
            return await _glossary.AddWord(request);
        }
        
        /// <summary>
        /// Asignar un nodo a un glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AssignToNode(AssignNodeRequest request)
        {
            return await _glossary.AssignToNode(request);
        }

        /// <summary>
        /// Eliminar un glosario existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _glossary.Delete(id);
        }

        /// <summary>
        /// Eliminar un termino de un glosario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteWord(int id)
        {
            return await _glossary.DeleteWord(id);
        }

        /// <summary>
        /// Obtener todos los glosarios de un usuario
        /// paginados y filtrados por nombre y descripción
        /// </summary>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId)
        {
            return await _glossary.GetAll(curentPage, sizePage, filter, userId);
        }

        /// <summary>
        /// Obtener glosario por identificador
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="glossaryId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllById(int userId, int glossaryId)
        {
            return await _glossary.GetAllById(userId, glossaryId);
        }

        /// <summary>
        /// Obtener todos los glosarios de un usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullData"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId, bool fullData = false)
        {
            return await _glossary.GetAllByUserId(userId, fullData);
        }

        /// <summary>
        /// Obtener los glosarios de los cursos a los cuales esta inscrito un 
        /// alumno dependiendo de la publicación
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByStudent(int nodeId, int userId)
        {
            return await _glossary.GetByStudent(nodeId, userId);
        }

        /// <summary>
        /// Eliminar glorario de un nodo
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="glosaryId"></param>
        /// <returns></returns>
        public async Task<Response> RemoveToNode(int nodeId, int glosaryId)
        {
            return await _glossary.RemoveToNode(nodeId, glosaryId);
        }

        /// <summary>
        /// Actualizar los datos de un glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(GlossaryUpdateRequest request)
        {
            return await _glossary.Update(request);
        }

        /// <summary>
        /// Actualizar datos de un termino
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateWord(WordUpdateRequest request)
        {
            return await _glossary.UpdateWord(request);
        }
    }
}
