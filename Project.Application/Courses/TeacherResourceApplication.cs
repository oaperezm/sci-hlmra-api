﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class TeacherResourceApplication : ITeacherResourceApplication
    {
        public readonly ITeacherResource _teacherResource;

        public TeacherResourceApplication(ITeacherResource teacherResource) {
            this._teacherResource = teacherResource;
        }

        /// <summary>
        /// Agregar un nuevo recurso del maestro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TeacherResourceSaveRequest request)
        {
            return await this._teacherResource.Add(request);
        }

        /// <summary>
        /// Eliminar un recurso existende de un profesor
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int resourceId, int userId)
        {
            return await _teacherResource.Delete(resourceId, userId);
        }

        /// <summary>
        /// Obtener todos los recursos de un profesor
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUser(int userId)
        {
            return await _teacherResource.GetByUser(userId);
        }
    }
}
