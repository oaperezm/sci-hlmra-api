﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface ITeacherResourceApplication
    {
        Task<Response> Add(TeacherResourceSaveRequest request);
        Task<Response> GetByUser(int userId);
        Task<Response> Delete(int resourceId, int userId);
    }
}
