﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VCoordinatorReportApplication : IVCoordinatorReportApplication
    {

        private readonly IVCoordinatorReport _vCoordinatorReport;

        public VCoordinatorReportApplication(IVCoordinatorReport vCoordinatorReport)
        {
            _vCoordinatorReport = vCoordinatorReport;
        }

        public Task<Response> GetAll()
        {
            return this._vCoordinatorReport.GetAll();
        }
    }
}
