﻿using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class CourseSectionApplication : ICourseSectionApplication
    {
        private readonly ICourseSection _CourseSection;

        public CourseSectionApplication(ICourseSection CourseSection) {
            this._CourseSection = CourseSection;
        }

        /// <summary>
        /// Método para agregar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> Add(CourseSectionSaveRequest request)
        {
            return _CourseSection.Add(request);
        }


        public async Task<Response> Delete(int id)
        {
            return await this._CourseSection.Delete(id);
        }

        /// <summary>
        /// Método para obtener todos los cursos registrados 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetAll()
        {
            return await _CourseSection.GetAll();
        }

        /// <summary>
        /// Obtener los datos de un curso 
        /// </summary>
        /// <param name="id">Identificador principal del curso</param>
        /// <returns></returns>
        public async Task<Response> GetById(int id)
        {
            return await _CourseSection.GetById(id);
        }

        /// <summary>
        /// Actualizar los datos de un curso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(CourseSectionUpdateRequest request)
        {
            return await _CourseSection.Update(request);

        }

        /// <summary>
        /// Método para obtener todos los cursos registrados 
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetByCourseId(int courseId)
        {
            return await _CourseSection.GetByCourseId(courseId);
        }

    }
}
