﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VReportExamApplication: IVReportExamApplication
    {
        private readonly IVReportExam _vReportExam;

        public VReportExamApplication(IVReportExam vReportExam)
        {
            _vReportExam = vReportExam;
        }

        public Task<Response> GetAll()
        {
           return this._vReportExam.GetAll();
        }
    }
}
