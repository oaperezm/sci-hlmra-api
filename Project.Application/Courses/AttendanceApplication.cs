﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class AttendanceApplication: IAttendanceApplication
    {
        public readonly IAttendance _attendance;

        public AttendanceApplication(IAttendance attendance) {
            _attendance = attendance;
        }

        /// <summary>
        /// Obtener las asistencia por grupo y rango de fecha
        /// </summary>
        /// <param name="initDate"></param>
        /// <param name="endDate"></param>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAttendances(DateTime initDate, DateTime endDate, int studentGroupId)
        {
            return await _attendance.GetAttendances(initDate, endDate, studentGroupId);
        }

        /// <summary>
        /// Registrar asistencia 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveAttendances(AttendanceSaveRequest request)
        {
            return await _attendance.SaveAttendances(request);
        }
    }
}
