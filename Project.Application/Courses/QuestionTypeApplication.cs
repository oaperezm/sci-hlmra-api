﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class QuestionTypeApplication : IQuestionTypeApplication
    {
        private readonly IQuestionType _QuestionType;

        public QuestionTypeApplication(IQuestionType QuestionType)
        {

            this._QuestionType = QuestionType;
        }

        public Task<Response> AddQuestionType(QuestionTypeSaveRequest request)
        {
            return this._QuestionType.AddQuestionType(request);
        }

        public Task<Response> DeleteQuestionType(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._QuestionType.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._QuestionType.GetById(id);
        }

        public Task<Response> UpdateQuestionType(QuestionTypeUpdateRequest request)
        {
            return this._QuestionType.UpdateQuestionType(request);
        }

    }
}
