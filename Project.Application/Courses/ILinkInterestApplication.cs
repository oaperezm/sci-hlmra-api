﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface ILinkInterestApplication
    {
        Task<Response> Add(LinkInterestSaveRequest request);
        Task<Response> Update(LinkInterestUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetByCourse(int courseId);
        Task<Response> GetByStudent(int userId, int nodeId);
    }
}
