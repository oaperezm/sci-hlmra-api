﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class AnnouncementApplication: IAnnouncementApplication
    {
        private readonly IAnnouncement _announcement;

        public AnnouncementApplication(IAnnouncement announcement) {
            _announcement = announcement;
        }

        /// <summary>
        /// Agregar un aviso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(AnnouncementSaveRequest request)
        {
            return await _announcement.Add(request);
        }

        /// <summary>
        /// Actualizar los datos de un aviso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(AnnouncementUpdateRequest request)
        {
            return await _announcement.Update(request);
        }

        /// <summary>
        /// Eliminar los datos de un aviso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _announcement.Delete(id);
        }


        /// <summary>
        /// Obtener avisos por identificador
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="avisoId"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int userId, int announcementId)
        {
            return await _announcement.GetById(userId, announcementId);
        }

        /// <summary>
        /// Obtener todos los avisos de un usuario
        /// paginados y filtrados por nombre y descripción
        /// </summary>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int userId)
        {
            return await _announcement.GetAll(curentPage, sizePage, filter, userId);
        }
    }
}
