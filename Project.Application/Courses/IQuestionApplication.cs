﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IQuestionApplication
    {    
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> AddQuestion(QuestionSaveRequest request);
        Task<Response> UpdateQuestion(QuestionUpdateRequest request);
        Task<Response> DeleteQuestion(int roleId);        
    }
}
