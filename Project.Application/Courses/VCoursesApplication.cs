﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VCoursesApplication : IVCoursesApplication
    {
        private readonly IVCourses _vCourses;

        public VCoursesApplication(IVCourses vCourses)
        {
            this._vCourses = vCourses;
        }

        public async Task<ResponsePagination> GetAll(PaginationRequestById request)
        {
            return await _vCourses.GetAll(request);
        }

    }
}
