﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class ActivityStudentFileApplication: IActivityStudentFileApplication
    {
        private readonly IActivityStudentFile _activityStudentFile;

        public ActivityStudentFileApplication(IActivityStudentFile activityStudentFile)
        {
            this._activityStudentFile = activityStudentFile;
        }

        /// <summary>
        /// Agregar archivo a la respuesta de la tarea del alumnio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddFile(ActivityStudentFileSaveRequest request)
        {
            return await _activityStudentFile.AddFile(request);
        }

        /// <summary>
        /// Obtener los archivos del alumno de una tarea
        /// </summary>
        /// <param name="activityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByActivity(int activityId, int userId)
        {
            return await _activityStudentFile.GetStundentFileByActivity(activityId, userId);
        }

        /// <summary>
        /// Remover un archivo de una tarea
        /// </summary>
        /// <param name="activityStudentFileId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteFile(int activityStudentFileId, int userId)
        {
            return await _activityStudentFile.DeleteFile(activityStudentFileId, userId);
        }
    }
}
