﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class StudentApplication : IStudentApplication
    {
        private readonly IStudent _Student;

        public StudentApplication(IStudent Student)
        {

            this._Student = Student;
        }

        public Task<Response> AddStudent(StudentSaveRequest request)
        {
            return this._Student.AddStudent(request);
        }

        public Task<Response> DeleteStudent(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._Student.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._Student.GetById(id);
        }

        public Task<Response> UpdateStudent(StudentUpdateRequest request)
        {
            return this._Student.UpdateStudent(request);
        }

        public Task<Response> GetStudentsByGroup(int studentGroupId, int examScheduleId)
        {
            return this._Student.GetStudentsByGroup(studentGroupId, examScheduleId);
        }

        public Task<Response> SendInvitation(StudentSendInvitationRequest request)
        {
            return this._Student.SendInvitation(request);
        }

        public Task<Response> AddInvitation(StudentSendInvitationRequest request, int id)
        {
            return this._Student.AddInvitation(request, id);
        }
        /// <summary>
        /// Método para obtener los cursos que esta inscrito un usuario por bloque
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="blockId"></param>
        /// <returns></returns>
        public async Task<Response> GetCourseByStudentAndBlock(int userId, int blockId )
        {
            return  await this._Student.GetCourseByStudentAndBlock(userId, blockId);
        }
        public Task<Response> ConfirmInvitation(string codigo)
        {
            return this._Student.ConfirmInvitation(codigo);
        }

    }
}
