﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IStatesVideoConferenceApplication
    {
        Task<Response> Add(StatesVideoConferenceSaveRequest request);
        Task<Response> GetAll();
    }
}
