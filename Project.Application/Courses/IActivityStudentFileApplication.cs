﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;


namespace Project.Application.Courses
{
    public interface IActivityStudentFileApplication
    { 
        Task<Response> AddFile(ActivityStudentFileSaveRequest request);
        Task<Response> GetStundentFileByActivity(int activityId, int userId);
        Task<Response> DeleteFile(int activityStudentFileId, int userId);
    }
}
