﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class TestApplication : ITestApplication
    {
        private readonly ITest _test;

        public TestApplication(ITest test)
        {
            _test = test;
        }

        /// <summary>
        /// Agregar los datos de un nuevo examen aplicado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(TestSaveRequest request, EnumRol rol)
        {
            return await _test.Add(request, rol);
        }

        /// <summary>
        /// Obtener las respuestas de un estudiante
        /// </summary>
        /// <param name="examScheduleId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUserAndExamSchedule(int examScheduleId, int userId, EnumRol rol)
        {
            return await _test.GetByUserAndExamSchedule(examScheduleId, userId, rol);
        }

        public async Task<Response> SaveAnswerQuestion(TestAnswerQuestionRequest request, EnumRol rol)
        {
            return await _test.SaveAnswerQuestion(request, rol);
        }
    }
}
