﻿using Project.Application.Courses;
using Project.Domain.Core;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
   public class StatesVideoConferenceApplication: IStatesVideoConferenceApplication
    {
        private readonly IStatesVideoConference _statesVideoConferece;

        public StatesVideoConferenceApplication(IStatesVideoConference statesVideoConferece)
        {
           this. _statesVideoConferece = statesVideoConferece;
        }

        Task<Response> IStatesVideoConferenceApplication.Add(StatesVideoConferenceSaveRequest request)
        {
             return _statesVideoConferece.Add(request);
        }

        Task<Response> IStatesVideoConferenceApplication.GetAll()
        {
            return _statesVideoConferece.GetAll();
        }
    }
}
