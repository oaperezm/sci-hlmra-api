﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class QuestionBankApplication : IQuestionBankApplication
    {
        private readonly IQuestionBank _QuestionBank;

        public QuestionBankApplication(IQuestionBank QuestionBank)
        {

            this._QuestionBank = QuestionBank;
        }

        /// <summary>
        /// Agregar un nuevo banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<Response> AddQuestionBank(QuestionBankSaveRequest request, int userId)
        {
            return this._QuestionBank.AddQuestionBank(request, userId);
        }

        /// <summary>
        /// Eliminar un banco de preguntas existente
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<Response> DeleteQuestionBank(int userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtener todos los bancos de preguntas paginados y por usuario
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <param name="rolId"></param>
        /// <param name="userId"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int rolId, int userId, int systemId)
        {
            return await this._QuestionBank.GetAll(currentPage, sizePage, filter, rolId, userId, systemId);
        }

        /// <summary>
        /// Obtener banco de preguntas por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="converImageBase64"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id, bool converImageBase64 = false)
        {
            return this._QuestionBank.GetById(id, converImageBase64);
        }

        /// <summary>
        /// Obtener bancos de preguntas por nodo asignado
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="userId"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public async Task<Response> GetByNodeId(int nodeId, int userId, int systemId)
        {
            return await this._QuestionBank.GetByNodeId(nodeId, userId, systemId);
        }

        /// <summary>
        /// Obtener la estructura de los bancos de preguntas asignados
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        public Task<Response> GetStructuraWithQuestionBank(int userId, int systemId)
        {
            return this._QuestionBank.GetStructuraWithQuestionBank(userId, systemId );
        }

        /// <summary>
        /// Actualizar los datos de un banco de pregunta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateQuestionBank(QuestionBankUpdateRequest request)
        {
            return this._QuestionBank.UpdateQuestionBank(request);
        }

        /// <summary>
        /// Activar el estatus del banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatusQuestionBank(QuestionBankStatusRequest request)
        {
            return await this._QuestionBank.UpdateStatusQuestionBank(request);
        }

        /// <summary>
        /// Obtener listado de bancos de preguntas
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<Response> GetByListIds(List<int> ids)
        {
            return await this._QuestionBank.GetByListIds(ids);
        }

        /// <summary>
        /// Cargar un banco de pregunta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SaveLoad(QuestionBankLoadRequest request)
        {
            return await _QuestionBank.SaveLoad(request);
        }
    }
}
