﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class ExamScoreApplication : IExamScoreApplication
    {
        private readonly IExamScore _examScore;

        public ExamScoreApplication(IExamScore examScore) {

            this._examScore = examScore;
        }

        /// <summary>
        /// Agregar una nueva programación de exámenes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async  Task<Response> Add(ExamScoreSaveRequest request)
        {
            return await this._examScore.Add(request);
        }

        /// <summary>
        /// Eliminar de manera permanente los datos de una programación de exámenes
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámenes</param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await this._examScore.Delete(id);
        }

        /// <summary>
        /// Obtener todas las programaciones de exámenes de un grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupId(int studentGroupId)
        {
            return await this._examScore.GetAllByStudentGroupId(studentGroupId);
        }

        /// <summary>
        /// Obtener todas las programaciones de exámenes de un profesor
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId)
        {
            return await this._examScore.GetAllByUserId(userId);
        }
        
        /// <summary>
        /// Actualizar los datos de una programación de exámenes eziatentes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> Update(ExamScoreUpdateRequest request)
        {
            return await this._examScore.Update(request);
        }

        public async Task<Response> GetByExamUser(int ExamScheduleId, int UserId)
        {
            return await this._examScore.GetByExamUser(ExamScheduleId, UserId);
        }

        public async Task<Response> SaveGroupScore(List<ExamScoreSaveRequest> scoreList)
        {
            return await this._examScore.SaveGroupScore(scoreList);
        }



    }
}
