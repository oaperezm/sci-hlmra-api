﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.DTO;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class StudentScoreApplication : IStudentScoreApplication
    {
        private readonly IStudentScore _StudentScore;

        public StudentScoreApplication(IStudentScore StudentScore)
        {
            this._StudentScore = StudentScore;
        }

        public Task<Response> GetAllByGroup(int studentGroupId)
        {
            return this._StudentScore.GetAllByGroup(studentGroupId);
        }
        public Task<Response> GetAllByGroupDetailsExcel(FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO)
        {
            return this._StudentScore.GetAllByGroupDetailsExcel(finalScoreFileRequirementsDTO);
        }
        public Task<Response> GetByPartialId(int studentGroupId, int Partialid, int userID)
        {
            return this._StudentScore.GetByPartialId(studentGroupId, Partialid, userID);
        }

        public Task<Response> GetAllPartials(int studentGroupId, int userID)
        {
            return this._StudentScore.GetAllPartials(studentGroupId, userID);
        }

        public Task<Response> UpdateStudentScore(StudentScoreUpdateRequest request)
        {
            return this._StudentScore.UpdateStudentScore(request);
        }

        public Task<Response> GetStudentsScoresByGroup(int studentGroupId, int userId)
        {
            return this._StudentScore.GetStudentsScoresByGroup(studentGroupId, userId);
        }

        public Task<Response> SaveStudentScore(StudentScoreSaveRequest request)
        {
            return this._StudentScore.SaveStudentScore(request);
        }

        public List<StudentFinalScoreDTO> GetScoreGeneral(int studentGroupId, int userId)
        {
            return this.GetScoreGeneral(studentGroupId, userId);
        }
    }
}
