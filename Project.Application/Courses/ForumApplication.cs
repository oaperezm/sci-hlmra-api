﻿using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class ForumApplication: IForumApplication
    {
        private readonly IForum _forum;

        public ForumApplication(IForum forum) {
            _forum = forum;
        }

        /// <summary>
        /// Agregar nuevo post
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddPost(int userId, PostSaveRequest request)
        {
            return await _forum.AddPost(userId, request);
        }

        /// <summary>
        /// Agregar nuevo hilo
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddThread(int userId, ThreadSaveRequest request)
        {
            return await _forum.AddThread(userId, request);
        }

        /// <summary>
        /// Eliminar un post existente
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns></returns>
        public async Task<Response> DeletePost(int userId, int postId)
        {
            return await _forum.DeletePost(userId, postId);
        }

        /// <summary>
        /// Obtener hilos por usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rolId"></param>
        /// <returns></returns>
        public async Task<Response> GetByUser(int userId, int rolId, int systemId)
        {
            return await _forum.GetByUser(userId, rolId, systemId);
        }

        /// <summary>
        /// Obtener hilo por identificador
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GeThreadById(int userId, int id, int rolId)
        {
            return await _forum.GeThreadById(userId, id, rolId);
        }

        /// <summary>
        /// Actualizar los datos de un post existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdatePost(PostUpdateRequest request)
        {
            return await _forum.UpdatePost(request);
        }

        /// <summary>
        /// Actualizar el estatus del post
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(PostUpdateStatusRequest request)
        {
            return await _forum.UpdateStatus(request);
        }

        /// <summary>
        /// Actualizar un hilo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> UpdateThread(ThreadUpdateRequest request)
        {
            return await _forum.UpdateThread(request);
        }
        
        /// <summary>
        /// Actualizar los votos de un post
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="vote"></param>
        /// <returns></returns>
        public async Task<Response> UpdateVote(int postId, int userId, int vote)
        {
            return await _forum.UpdateVote(postId, userId, vote);
        }
    }
}
