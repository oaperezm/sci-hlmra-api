﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class VStudentsApplication : IVStudentsApplication
    {
        private readonly IVStudents _vStudetns;

        public VStudentsApplication(IVStudents vStudetns)
        {
            _vStudetns = vStudetns;
        }

        public async Task<Response> GetAllByIdTeacher(int teacherId)
        {
            return await _vStudetns.GetAllByIdTeacher(teacherId);
        }

        public async Task<ResponsePagination> GetAll(PaginationRelationRequest request)
        {
            return await _vStudetns.GetAll(request);
        }
    }
}
