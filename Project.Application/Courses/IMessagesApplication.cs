﻿using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface IMessagesApplication
    {
        Task<Response> SendMessage(int senderUserId, SendMessageRequest request);
        Task<Response> GetByChatGroup(int chatGroupId, int userId);
        Task<Response> MarkRead(int reciverUserId, bool read);
        Task<Response> GetAllConversations(int userId);
        Task<Response> GetContact(int userId, int rolId);
    }
}
