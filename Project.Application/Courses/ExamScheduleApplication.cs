﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class ExamScheduleApplication : IExamScheduleApplication
    {
        private readonly IExamSchedule _examSchedule;

        public ExamScheduleApplication(IExamSchedule examSchedule) {

            this._examSchedule = examSchedule;
        }

        /// <summary>
        /// Agregar una nueva programación de exámenes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async  Task<Response> Add(ExamScheduleSaveRequest request, int userId, int systemId)
        {
            return await this._examSchedule.Add(request, userId, systemId);
        }

        /// <summary>
        /// Eliminar de manera permanente los datos de una programación de exámenes
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámenes</param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await this._examSchedule.Delete(id);
        }

        /// <summary>
        /// Obtener todas las programaciones de exámenes de un grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupId(int studentGroupId)
        {
            return await this._examSchedule.GetAllByStudentGroupId(studentGroupId);
        }

        /// <summary>
        /// Obtener todas las programaciones de examenes y sus calificaciones
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByStudentGroupIdAndUser(int studentGroupId, int userId)
        {
            return await _examSchedule.GetAllByStudentGroupIdAndUser(studentGroupId, userId);
        }

        /// <summary>
        /// Obtener todas las programaciones de exámenes de un profesor
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByUserId(int userId)
        {
            return await this._examSchedule.GetAllByUserId(userId);
        }
        
        /// <summary>
        /// Actualizar los datos de una programación de exámenes eziatentes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> Update(ExamScheduleUpdateRequest request, int userId, int systemId)
        {
            return await this._examSchedule.Update(request, userId, systemId);
        }

        /// <summary>
        /// Actualizar el estatus de la programación de exámen
        /// </summary>
        /// <param name="examScheduleId"></param>
        /// <param name="examScheduleTypeId"></param>
        /// <returns></returns>
        public async Task<Response> UpdateExamScheduleType(int examScheduleId, int examScheduleTypeId)
        {
            return await this._examSchedule.UpdateExamScheduleType(examScheduleId, examScheduleTypeId);
        }

        /// <summary>
        /// ACtualizar estatus de la programación de exámenes
        /// </summary>
        /// <param name="id"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int id, bool active)
        {
            return await this._examSchedule.UpdateStatus(id, active);
        }
    }
}
