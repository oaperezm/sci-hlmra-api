﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public class VReportTeacherApplication: IVReportTeacherApplication
    {

        private readonly IVReportTeacher _vReportTeacher;

        public VReportTeacherApplication(IVReportTeacher vReportTeacher)
        {
            _vReportTeacher = vReportTeacher;
        }
        public Task<Response> GetAll()
        {
            return this._vReportTeacher.GetAll();
        }
    }
}
