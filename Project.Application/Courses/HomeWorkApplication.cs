﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class HomeWorkApplication : IHomeWorkApplication
    {
        private readonly IHomeWork _homeWork;

        public HomeWorkApplication(IHomeWork homeWork) {
            this._homeWork = homeWork;
        }

        /// <summary>
        /// Agregar una nueva tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(HomeWorkSaveRequest request)
        {
            return await _homeWork.Add(request);
        }

        /// <summary>
        /// Agregar archivo a la respuesta de la tarea del alumnio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AddAnswer(HomeWorkAnswerSaveRequest request)
        {
            return await _homeWork.AddAnswer(request);
        }

        /// <summary>
        /// Eliminar el archivo de la respuesta de una tarea del alumno
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAnswer(int homeWorkId, int userId, int answerId)
        {
            return await _homeWork.DeleteAnswer(homeWorkId, userId, answerId);
        }

        /// <summary>
        /// Obtener todas las tareas de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetAllByGroup(int id)
        {
            return await _homeWork.GetAllByGroup(id);
        }

        /// <summary>
        /// Obtener todas las calificaciones
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetAllWithScore(int studentGroupId, int userId)
        {
            return await _homeWork.GetAllWithScore(studentGroupId, userId);
        }

        /// <summary>
        /// Obtener los alumnos y sus respuestas d euna tarea en especifico
        /// </summary>
        /// <param name="studenGroupId"></param>
        /// <param name="homeworkId"></param>
        /// <returns></returns>
        public async Task<Response> GetStudentsAnswers(int studenGroupId, int homeworkId)
        {
            return await _homeWork.GetStudentsAnswers(studenGroupId, homeworkId);
        }

        /// <summary>
        /// Obtener los archivos del alumno de una tarea
        /// </summary>
        /// <param name="homeWorkId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response> GetStundentFileByHomeWork(int homeWorkId, int userId)
        {
            return await _homeWork.GetStundentFileByHomeWork(homeWorkId, userId);
        }

        /// <summary>
        /// Eliminar una programación de tareas
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Remove(int id)
        {
            return await _homeWork.Remove(id);
        }

        /// <summary>
        /// Remover un archivo de una tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> RemoveFile(HomeWorkRemoveFileRequest request)
        {
            return await _homeWork.RemoveFile(request);
        }

        /// <summary>
        /// Guardar las calificaciones de la tarea de un grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async  Task<Response> SaveScore(HomeWorkScoreRequest request)
        {
            return await _homeWork.SaveScore(request);
        }

        /// <summary>
        /// Actualizar los datos de una tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(HomeWorkUpdateRequest request)
        {
            return await _homeWork.Update(request);
        }

        /// <summary>
        /// Cerrar la tarea programada para no dejar calificar 
        /// </summary>
        /// <param name="homeworkId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStatus(int homeworkId, bool status)
        {
            return await _homeWork.UpdateStatus(homeworkId, status);
        }
    }
}
