﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Domain.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace Project.Application.Courses
{
    public class StudentStatusApplication : IStudentStatusApplication
    {
        private readonly IStudentStatus _StudentStatus;

        public StudentStatusApplication(IStudentStatus StudentStatus)
        {

            this._StudentStatus = StudentStatus;
        }

        public Task<Response> AddStudentStatus(StudentStatusSaveRequest request)
        {
            return this._StudentStatus.AddStudentStatus(request);
        }

        public Task<Response> DeleteStudentStatus(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<Response> GetAll()
        {
            return this._StudentStatus.GetAll();
        }

        public Task<Response> GetById(int id)
        {
            return this._StudentStatus.GetById(id);
        }

        public Task<Response> UpdateStudentStatus(StudentStatusUpdateRequest request)
        {
            return this._StudentStatus.UpdateStudentStatus(request);
        }

    }
}
