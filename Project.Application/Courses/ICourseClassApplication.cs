﻿using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System.Threading.Tasks;

namespace Project.Application.Courses
{
    public interface ICourseClassApplication
    {
        Task<Response> GetAll();
        Task<Response> GetById(int id);
        Task<Response> Add(CourseClassSaveRequest request);
        Task<Response> Update(CourseClassUpdateRequest request);
        Task<Response> Delete(int id);
        Task<Response> GetBySectionId(int courseSectionId);
        Task<Response> AddFiles(CourseClassFilesRequest request);
        Task<Response> RemoveFiles(CourseClassFilesRequest request);
    }
}
