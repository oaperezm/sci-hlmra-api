﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Home
{
    public class GeneralEventApplication : IGeneralEventApplication
    {
        private readonly IGeneralEvent _generalEvent;

        public GeneralEventApplication(IGeneralEvent generalEvent) {
            _generalEvent = generalEvent;
        }

        /// <summary>
        /// Agregar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Add(GeneralEventSaveRequest request)
        {
            return await _generalEvent.Add(request);
        }

        /// <summary>
        /// Eliminar un evento existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> Delete(int id)
        {
            return await _generalEvent.Delete(id);
        }

        /// <summary>
        /// Obtener todos los eventos registrados paginados
        /// </summary>
        /// <param name="curentPage"></param>
        /// <param name="sizePage"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int currentPage, int sizePage, string filter, int systemId)
        {
            return  await _generalEvent.GetAll(currentPage, sizePage, filter, systemId);
        }

        /// <summary>
        /// Obtener evento por identificador
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(int Id)
        {
            return await _generalEvent.GetById(Id);
        }

        /// <summary>
        /// Actuaizar los datos de un evento existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> Update(GeneralEventUpdateRequest request)
        {
            return await _generalEvent.Update(request);
        }
    }
}
