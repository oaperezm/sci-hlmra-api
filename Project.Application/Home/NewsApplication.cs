﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Home
{
    public class NewsApplication : INewsApplication
    {
        private readonly INews _news;

        public NewsApplication(INews news)
        {
            this._news = news;
        }

        /// <summary>
        /// Agregar una nueva noticia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddNews(NewsSaveRequest request)
        {
            return this._news.AddNews(request);
        }

        public async Task<Response> GetAll()
        {
            return await _news.GetAll();
        }

        /// <summary>
        /// Obtener todos los tips registrados paginados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId)
        {
            return await _news.GetAll(curentPage, sizePage, filter, systemId);
        }

        /// <summary>
        /// Actualizar los datos de un tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateNews(NewsUpdateRequest request)
        {
            return _news.UpdateNews(request);
        }

        /// <summary>
        /// Eliminar un tip registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> DeleteNews(int id)
        {
            return _news.DeleteNews(id);
        }

        /// <summary>
        /// Obtener tip por indentificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return _news.GetById(id);
        }
    }
}
