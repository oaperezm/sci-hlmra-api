﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Home
{
    public class TipApplication : ITipApplication
    {

        private readonly ITip _tip;

        public TipApplication(ITip tip)
        {
            this._tip = tip;
        }

        /// <summary>
        /// Agregar un nuevo tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> AddTip(TipSaveRequest request)
        {
            return this._tip.AddTip(request);
        }

        public async Task<Response> GetAll()
        {
            return await _tip.GetAll();
        }

        /// <summary>
        /// Obtener todos los tips registrados paginados
        /// </summary>
        /// <returns></returns>
        public async Task<ResponsePagination> GetAll(int curentPage, int sizePage, string filter, int systemId)
        {
            return await _tip.GetAll(curentPage, sizePage, filter, systemId);
        }

        /// <summary>
        /// Actualizar los datos de un tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Response> UpdateTip(TipUpdateRequest request)
        {
            return _tip.UpdateTip(request);
        }

        /// <summary>
        /// Eliminar un tip registrado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> DeleteTip(int id)
        {
            return _tip.DeleteTip(id);
        }

        /// <summary>
        /// Obtener tip por indentificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Response> GetById(int id)
        {
            return _tip.GetById(id);
        }
    }
}
