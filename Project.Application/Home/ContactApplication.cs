﻿using Project.Application.Core;
using Project.Domain.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Application.Home
{
    public class ContactApplication : IContactApplication
    {
        private readonly IContact _contact;

        public ContactApplication(IContact contact) {
            _contact = contact;
        }

        /// <summary>
        /// Enviar correo de contacto 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> SendContactMail(MailRequest request)
        {
            return await _contact.SendContactMail(request);
        }
    }
}
