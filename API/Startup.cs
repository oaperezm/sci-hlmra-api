﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using API.Auth;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Project.Application.Management;
using Project.Integration.Management;
using Project.Integration.Courses;
using System.Configuration;
using System.Web;
using Swashbuckle.Application;
using System.IO;
using System.Linq;
using Project.Integration.Home;
using Project.Integration.VideoConference;

[assembly: OwinStartup(typeof(API.Startup))]

namespace API
{
    /// <summary>
    /// Archivo de inicio de aplicación
    /// </summary>
    public class Startup
    {
        private static IContainer ContainerIoC;

        /// <summary>
        /// Inicializar la configuración de la aplicación
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            #region SWAGGER 
            
            var baseDirectory    = AppDomain.CurrentDomain.BaseDirectory + @"\bin\";
            var commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            var commentsFile     = Path.Combine(baseDirectory, commentsFileName);

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration
                  .EnableSwagger(c =>
                  {
                      c.SingleApiVersion("v1", "SALI API")                      
                       .Description("API utilizada para generar implementación de SALI")
                       .Contact(cc => cc
                           .Name("Integra IT Soluciones")
                           .Url("https://www.integrait.com.mx/")
                           .Email("orlando.rangel@integrait.com.mx"));                         
                      c.IncludeXmlComments(commentsFile);
                      c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                  })
              .EnableSwaggerUi();

            #endregion

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            #region AUTOFACT CONFIG

            var builder = new ContainerBuilder();
            // Create and assign a dependency resolver for Web API to use.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new UserModule());
            builder.RegisterModule(new RoleModule());
            builder.RegisterModule(new PermissionModule());
            builder.RegisterModule(new CityModule());

            builder.RegisterModule(new AreaModule());
            builder.RegisterModule(new AuthorModule());
            builder.RegisterModule(new BlockModule());
            builder.RegisterModule(new BookModule());
            builder.RegisterModule(new ContentModule());
            builder.RegisterModule(new ContentTypeModule());
            builder.RegisterModule(new FileTypeModule());
            builder.RegisterModule(new FormativeFieldModule());
            builder.RegisterModule(new GradeModule());
            builder.RegisterModule(new LevelModule());
            builder.RegisterModule(new LocationModule());
            builder.RegisterModule(new NodeTypeModule());
            builder.RegisterModule(new PurposeModule());
            builder.RegisterModule(new SubsystemModule());
            builder.RegisterModule(new TrainingFieldModule());

            builder.RegisterModule(new NodeModule());
            builder.RegisterModule(new NodeContentModule());
            builder.RegisterModule(new PublishingProfileModule());

            builder.RegisterModule(new CourseModule());

            builder.RegisterModule(new QuestionBankModule());
            builder.RegisterModule(new QuestionModule());
            builder.RegisterModule(new QuestionTypeModule());
            builder.RegisterModule(new AnswerModule());

            builder.RegisterModule(new StudentGroupModule());
            builder.RegisterModule(new StudentStatusModule());
            builder.RegisterModule(new StudentModule());

            builder.RegisterModule(new PublishingProfileUserModule());

            builder.RegisterModule(new ExamScheduleModule());
            builder.RegisterModule(new ExamScoreModule());

            builder.RegisterModule(new CourseSectionModule());
            builder.RegisterModule(new CourseClassModule());

            builder.RegisterModule(new TeacherModule());
            builder.RegisterModule(new GlossaryModule());

            builder.RegisterModule(new ServicesModule());

            builder.RegisterModule(new MessageModule());
            builder.RegisterModule(new ForumModule());
            builder.RegisterModule(new VStudentsModule());
            builder.RegisterModule(new VCoursesModule());

            builder.RegisterModule(new TipModule());
            builder.RegisterModule(new NewsModule());
            builder.RegisterModule(new GeneralEventModule());
            builder.RegisterModule(new ContactModule());
            builder.RegisterModule(new VideoConferenceModule());

            builder.RegisterModule(new CoursePlanningModule());
            builder.RegisterModule(new StatesVideoConferenceModule());
			builder.RegisterModule(new AnnouncementModule());		
            builder.RegisterModule(new SchedulingPartialModule());

            //20190627 gestrada, se agregan entradas para modulos de actividades-profesor, actividades-alumnos, calificaciones
            builder.RegisterModule(new ActivityModule());
            builder.RegisterModule(new ActivityStudentFileModule());
            builder.RegisterModule(new StudentScoreModule());


            builder.RegisterModule(new EventReportModule());
            builder.RegisterModule(new EventModule());

            builder.RegisterModule(new AreaPersonalSocialDevelopmentModule());
            builder.RegisterModule(new AreasCurriculumAutonomyModule());
            builder.RegisterModule(new EducationLevelModule());
            builder.RegisterModule(new AxisModule());
            builder.RegisterModule(new KeylearningModule());
            builder.RegisterModule(new LearningexpectedModule());
            builder.RegisterModule(new SchoolGradeModule());
            builder.RegisterModule(new ReportModule());

            builder.RegisterModule(new VCoordinatorReportModule());
            builder.RegisterModule(new VReportCourseModule());
            builder.RegisterModule(new VReportExamModule());



            builder.RegisterModule(new SectionModule());
            builder.RegisterModule(new VReportTeacherModule());

            builder.RegisterModule(new VReportProfileTeacherModule());


            ContainerIoC = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(ContainerIoC);
            app.UseAutofacMiddleware(ContainerIoC);

            #endregion

            ConfigureAuth(app);
            app.UseWebApi(config);

            ConfigurationManager.AppSettings.Set("ServerPath", System.Web.Hosting.HostingEnvironment.MapPath("~"));
        }

        /// <summary>
        /// Inicializar la configuración de autenticación OWIN
        /// </summary>
        /// <param name="app"></param>
        private void ConfigureAuth(IAppBuilder app)
        {
            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new ApplicationOAuthServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(120),
                AllowInsecureHttp = true,
                RefreshTokenProvider = new RefreshTokenProvider()
            };

            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());            
        }
    }
}
