﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrar roles
    /// </summary>
    [RoutePrefix("api/roles")]
    public class RoleController : BaseController
    {
        private readonly IRoleApplication _role;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="role"></param>
        public RoleController(IRoleApplication role)
        {
            _role = role;            
        }

        /// <summary>
        /// Obtener todos los elementos sin paginación
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _role.GetAll(this.SystemId);
            return Ok(result);
        }
        
        /// <summary>
        /// Obtener todos los datos registrados paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _role.GetAll(request.CurrentPage, request.PageSize, request.Filter, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Agregar un nuevo rol
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post(RoleSaveRequest request)
        {
            var idUser = UserId;

            if(request.Id == 0)
            {
                request.SystemId = this.SystemId;

                var result = await _role.AddRole(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de un rol existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put(RoleSaveRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                request.SystemId = this.SystemId;
                var result = await _role.UpdateRole(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Eliminar un rol existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest("Debes seleccionar un rol valido");

                var result = await _role.DeleteRole(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Obtener los permisos de los roles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/obtenerPermisos")]
        public async Task<IHttpActionResult> GetRolePermissions(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _role.GetRolePermissions(id,this.SystemId);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Agregar permisos a un rol
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/addroles")]
        public async Task<IHttpActionResult> AddRoles([FromBody]RolPermissionAddRequest request)
        {

            var result = new Response();

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                result = await _role.UpdatePermissions(request.Id, request.Permissions);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }

    }
}

