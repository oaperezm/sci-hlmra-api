﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador 
    /// </summary>
    [RoutePrefix("api/permissions")]
    public class PermissionController : BaseController
    {
        private readonly IPermissionApplication _permission;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="permission"></param>
        public PermissionController(IPermissionApplication permission)
        {
            this._permission = permission;
        }

        /// <summary>
        /// Obtiene todos los permisos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _permission.GetAll(this.SystemId);
            return Ok(result);
        }

        /// <summary>
        /// Obtener todos los registros paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _permission.GetAll(request.CurrentPage, request.PageSize, request.Filter, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener los datos de un permiso 
        /// </summary>
        /// <param name="id">Identificador unico del permiso</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _permission.GetById(id);
            return Ok(result);
        }
             

        /// <summary>
        /// Agregar nuevo permiso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Permission([FromBody]PermissionSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                request.SystemId = this.SystemId;
                
                var result = await _permission.AddPermission(request);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }   
        }

        /// <summary>
        /// ACtualizar los datos d eun permiso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] PermissionSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _permission.UpdatePermission(request);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }          
        }

        /// <summary>
        /// Eliminar un permiso existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Debes seleccionar un rol valido");

                var result = await _permission.DeletePermission(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
