﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrar perfil de publicación
    /// </summary>
    [RoutePrefix("api/publishingprofiles")]
    public class PublishingProfileController : BaseController
    {
        private readonly IPublishingProfileApplication _PublishingProfile;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PublishingProfile"></param>
        public PublishingProfileController(IPublishingProfileApplication PublishingProfile)
        {
            this._PublishingProfile = PublishingProfile;
        }

        /// <summary>
        /// Obtener todos los perfiles de publicación registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _PublishingProfile.GetAll(this.SystemId);
            return Ok(result);
        }
             
        /// <summary>
        /// Agregar un nuevo perfil de publicación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]PublishingProfileSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                request.SystemId = this.SystemId;

                var result = await _PublishingProfile.AddPublishingProfile(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualiza los datos del perfil de publicación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] PublishingProfileUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _PublishingProfile.UpdatePublishingProfile(request);
                return Ok(request);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Obtener todos los usuarios paginados y filtrados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _PublishingProfile.GetAll(request.CurrentPage, request.PageSize, request.Filter, request.Active, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// GetNodes: Get profile's associated nodes
        /// </summary>
        /// <param name="id">PublishingProfile's Id</param>
        /// <returns>A list of node entities</returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/nodes")]
        public async Task<IHttpActionResult> GetNodes(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _PublishingProfile.GetNodes(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// AddNodes: Rewrite profile's associated node list
        /// </summary>
        /// <param name="request">PublishingProfile's Id and list of nodes</param>
        /// <returns>Success message and 200 code</returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/nodes")]
        public async Task<IHttpActionResult> AddNodes([FromBody]PublishingProfileNodesRequest request)
        {

            var result = new Response();

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                result = await _PublishingProfile.AddNodes(request.Id, request.Nodes);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Eliminar un perfil de publicación
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = await _PublishingProfile.DeletePublishingProfile(id);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileId"></param>        
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{profileId}/linkedusers")]
        public async Task<IHttpActionResult> GetLinkedUsers(int profileId)
        {
            var result = await _PublishingProfile.GetLinkedUsers(profileId, this.SystemId);
            return Ok(result);
        }
    }
}
