﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/publishingprofileusers")]
    public class PublishingProfileUserController : BaseController
    {
        private readonly IPublishingProfileUserApplication _PublishingProfileUser;

        public PublishingProfileUserController(IPublishingProfileUserApplication PublishingProfileUser)
        {
            this._PublishingProfileUser = PublishingProfileUser;
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _PublishingProfileUser.GetAll();
            return Ok(result);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _PublishingProfileUser.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]PublishingProfileUserSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _PublishingProfileUser.AddPublishingProfileUser(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }


        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] PublishingProfileUserUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _PublishingProfileUser.UpdatePublishingProfileUser(request);
                return Ok(request);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        
        [HttpDelete]
        [AllowAnonymous]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = await _PublishingProfileUser.DeletePublishingProfileUser(id);
            return Ok(result);
        }
    }
}
