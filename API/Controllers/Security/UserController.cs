﻿using Project.Application.Management;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Project.Entities.Request;
using Project.Entities.Responses;

namespace API.Controllers
{
    /// <summary>
    /// API para administración de usuarios
    /// </summary>
    [RoutePrefix("api/users")]
    public class UserController : BaseController
    {
        private readonly IUserApplication _user;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user"></param>
        public UserController(IUserApplication user) {
            this._user = user;
        }


        /// <summary>
        /// Confirma la suscripcion del usuario
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("confirmSuscripcion")]
        public async Task<IHttpActionResult> ConfirmSuscripcion(int email)
        {
            try
            {
                var result = await _user.ConfirmSuscripcion(email);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _user.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Registrar un nuevo usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] UserSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);

                if (this.SystemId > 0)
                    request.SystemId = this.SystemId;
                
                var result = await _user.RegisterUser(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Actualizar los datos de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] UserUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (!string.IsNullOrEmpty(request.ProfileImage))
                {
                    request.FileContentBase64 = System.Convert.FromBase64String(request.ProfileImage);
                }

                var result = await _user.UpdateUser(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Eliminar un usuario existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return BadRequest("Debes seleccionar un usuario valido");

                var result = await _user.DeleteUser(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var result = await _user.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Actualizar la contraseña del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("resetpassword")]
        public async Task<IHttpActionResult> ResetPassword(UserSavePasswordRequest request)
        {
            var idUser = UserId;

            var result = await _user.ResetPassword(request);
            return Ok(result);
        }

        /// <summary>
        /// Obtener el detalle de la cuenta del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("profile")]
        public async Task<IHttpActionResult> GetProfile(int origin) {

            try
            {
                var userId    = this.UserId;
                var rolId     = this.RolId;
                var responser = await this._user.GetUserProfile(UserId, this.SystemId, origin);

                return Ok(responser);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }

        }

        /// <summary>
        /// Obtener todos los usuarios por paginación
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _user.GetAll(request.CurrentPage, request.PageSize, request.Filter, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }
                
        /// <summary>
        /// Obtener los nodos asignados  un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/nodes")]
        public async Task<IHttpActionResult> GetNodes(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _user.GetNodes(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// AddNodes to user's access control list
        /// </summary>
        /// <param name="request">User Id & Nodes list</param>
        /// <returns>Success value, message and 200 code</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("{id}/nodes")]
        public async Task<IHttpActionResult> AddNodes([FromBody]UserNodesRequest request)
        {

            var result = new Response();

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                result = await _user.AddNodes(request.Id, request.Nodes);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// CopyProfile: Copy nodes from a profile to user's access control list
        /// </summary>
        /// <param name="request">UserId/ProfileId</param>
        /// <returns>Success value, message and 200 code</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("copyprofile")]
        public async Task<IHttpActionResult> CopyProfile([FromBody]UserCopyProfileRequest request)
        {
            var result = new Response();

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (request.UserIds.Count() > 0)
                {
                    result = await _user.CopyProfile(request.ProfileId, request.UserIds);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Solicitud incorrecta");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{eMail}/GetByMail")]
        public async Task<IHttpActionResult> GetByMail(string eMail)
        {
            var result = await _user.GetByMail(eMail);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialMail"></param>        
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{partialMail}/GetByPartialMail")]
        public async Task<IHttpActionResult> GetByPartialMail(string partialMail)
        {
            var result = await _user.GetByPartialMail(partialMail);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialMail"></param>
        /// <param name="roleId"></param>        
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByPartialMailRole")]
        public async Task<IHttpActionResult> GetByPartialMailRole(string partialMail, int roleId)
        {
            var result = await _user.GetByPartialMailRole(partialMail, roleId);
            return Ok(result);
        }

        /// <summary>
        /// Find users by mail, roleid and courseid
        /// </summary>
        /// <param name="partialMail"></param>
        /// <param name="roleId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("getbymailrolecourse")]
        public async Task<IHttpActionResult> GetByPartialMailRoleCourse(string partialMail, int roleId, int courseId)
        {
            var result = await _user.GetByMailRoleCourse(partialMail, roleId, courseId, this.SystemId);
            return Ok(result);
        }

        /// <summary>
        /// CopyProfile: Copy nodes from a profile to user's access control list
        /// </summary>
        /// <param name="request">UserId/ProfileId</param>
        /// <returns>Success value, message and 200 code</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("linkprofile")]
        public async Task<IHttpActionResult> LinkProfile([FromBody]UserCopyProfileRequest request)
        {
            var result = new Response();

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (request.UserIds.Count() > 0)
                {
                    result = await _user.LinkProfile(request.ProfileId, request.UserIds);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Solicitud incorrecta");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Método que permite validar la sessión actual del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("validateSession")]
        public async Task<IHttpActionResult> ValidateSession() => Ok();

        /// <summary>
        /// Get user data infomation
        /// </summary>
        /// <param></param>
        /// <returns>Success value, message and User information<returns>
        [HttpGet]
        [Authorize]
        [Route("profiledata")]
        public async Task<IHttpActionResult> GetUserProfileData()
        {

            try
            {
                var userId = this.UserId;
                var result = await this._user.GetUserProfileData(UserId);

                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }

        }

        /// <summary>
        /// Change the user's password with old and new passwords
        /// </summary>
        /// <param name="request">eMail/NewPassword/OldPassword</param>
        /// <returns>Success value, message and 200 code</returns>
        [HttpPut]
        [Authorize]
        [Route("changepassword")]
        public async Task<IHttpActionResult> ChangePassword(UserChangePasswordRequest request)
        {
            try
            {               

                var result = await _user.ChangePassword(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }

        }

        /// <summary>
        /// Change user image profile
        /// </summary>
        /// <param name="request">UserId/ProfileImage/FileContentBase64/FileName/FileArray list</param>
        /// <returns>Success value, message and 200 code</returns>
        [HttpPut]
        [Authorize]
        [Route("uploadImgProfile")]
        public async Task<IHttpActionResult> UploadImgProfile(UserUploadImgProfileRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (!string.IsNullOrEmpty(request.ProfileImage))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.Id = this.UserId;
                var result = await _user.UploadImgProfile(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        ///  Permite eliminar los datos de token 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("logout")]
        public async Task<IHttpActionResult> LogOut() {

            try
            {
                var userId = this.UserId;

                if (userId == 0)
                    throw new Exception("No existe el usuario en sesión");

                var result = await _user.LogOut(userId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Obtener los roles de un usuario por correo
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("roles")]
        public async Task<IHttpActionResult> GetRoles( string email)
        {
            try
            {
                if (email == "")
                    return BadRequest();

                var result = await _user.GetRolesByUserEmail(email);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Obtener los datos del usuario en SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("side")]
        public async Task<IHttpActionResult> GetDataSIDE(string email) {

            try
            {
                if (email == "")
                    return BadRequest();

                email = email.Trim();

                var result = await _user.ValidateUserSIDE(email);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        /// <summary>
        /// Obtener los datos del CCT en SIDE
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("cctside")]
        public async Task<IHttpActionResult> GetDataSIDECCT(string email)
        {

            try
            {
                if (email == "")
                    return BadRequest();

                email = email.Trim();

                var result = await _user.ValidateCCTUserSIDE(email);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }


        #region NOTIFICATIONS

        /// <summary>
        /// Registrar el dispositivo del usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("registerDevice")]
        public async Task<IHttpActionResult> RegisterDevice(UserDeviceSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _user.RegisterDevice(request, this.UserId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Obtener los mensajes no leidos del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("unread")]
        public async Task<IHttpActionResult> GetUnreadNotifications()
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest();

                var result = await _user.GetUnReadNotifications(this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Obtener los mensajes no leidos del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all-notifications")]
        public async Task<IHttpActionResult> GetAllNotifications()
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest();

                var result = await _user.GetAllNotifications(this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Obtener todas las notificaciones del usuario con paginacions
        /// </summary>
        /// <param name="page">Número de la página actual</param>
        /// <param name="sizePage">Tamaño de la página</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all-notifications")]
        public async Task<IHttpActionResult> GetAllNotifications(int page, int sizePage)
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest();

                var result = await _user.GetAllNotifications(this.UserId, page, sizePage);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        [Route("all-siide")]
        public async Task<IHttpActionResult> GetAllwithSIIDE(int currentPage, int sizePage)
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest();

                var result = await _user.GetAllwithSIIDE(currentPage, sizePage, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Marcar un mensaje como leido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("read")]
        public async Task<IHttpActionResult> ReadNotification(ReadNotificationRequest request) {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _user.CheckReadNotification(request.Id, this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        #endregion

        #region cct
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("cct")]
        public async Task<IHttpActionResult> GetCCT()
        {
            try
            {
                var result = await _user.GetCCT();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        #endregion
    }
}
