﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Home
{
    /// <summary>
    /// Controlador de eventos generales
    /// </summary>
    [RoutePrefix("api/general-events")]
    public class GeneralEventController : BaseController
    {
        private readonly IGeneralEventApplication _event;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="eventGeneral"></param>
        public GeneralEventController(IGeneralEventApplication eventGeneral) {
            _event = eventGeneral;
        }

        /// <summary>
        /// Obtener todos los eventos registrados paginados
        /// </summary>
        /// <param name="request"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request, int systemId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();
                
                if (this.SystemId > 0)
                    systemId = this.SystemId;

                var result = await _event.GetAll(request.CurrentPage, request.PageSize, request.Filter, systemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Actualizar los datos de un evento existentes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] GeneralEventUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _event.Update(request);
                return Ok(result);
            }

            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]GeneralEventSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.SystemId = this.SystemId;

                var result = await _event.Add(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar un evento existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _event.Delete(id);
                return Ok(result);
            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener un evento por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var result = await _event.GetById(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}
