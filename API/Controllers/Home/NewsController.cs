﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Home
{
    /// <summary>
    /// Controlador para administrar las noticias de la pagina principal
    /// </summary>
    [RoutePrefix("api/news")]
    public class NewsController : BaseController
    {
        private readonly INewsApplication _news;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="news"></param>       
        public NewsController(INewsApplication news)
        {
            _news = news;
        }

        /// <summary>
        /// Obtener todas las noticias registradas
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                var result = await _news.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener todas las noticias paginadas
        /// </summary>
        /// <param name="request"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request, int systemId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();


                if (this.SystemId > 0)
                    systemId = this.SystemId;

                var result = await _news.GetAll(request.CurrentPage, request.PageSize, request.Filter, systemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Actualizar las datos de una noticia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] NewsUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _news.UpdateNews(request);
                return Ok(result);
            }

            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Agregar una nueva noticia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> News([FromBody]NewsSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.SystemId = this.SystemId;

                var result = await _news.AddNews(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar una noticia existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _news.DeleteNews(id);
                return Ok(result);
            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener una noticia por su identificador
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var result = await _news.GetById(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}
