﻿using Project.Application.Core;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Home
{
    /// <summary>
    /// Controlador para envio de correos
    /// </summary>
    [RoutePrefix("api/mails")]
    public class MailController : ApiController
    {
        private readonly IContactApplication _contactApplication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contactApplication"></param>
        public MailController(IContactApplication contactApplication) {
            _contactApplication = contactApplication;
        }

        /// <summary>
        /// Agregar una nueva noticia
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("send")]
        public async Task<IHttpActionResult> Send([FromBody]MailRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                request.ReceptionEmail = ConfigurationManager.AppSettings["SALI.Contact.Mail"];
                var result =  await _contactApplication.SendContactMail(request);
               
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}
