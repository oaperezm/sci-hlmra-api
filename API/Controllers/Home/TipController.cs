﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Home
{
    /// <summary>
    /// Controlador para tips
    /// </summary>
    [RoutePrefix("api/tips")]
    public class TipController : BaseController
    {
        private readonly ITipApplication _tip;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tip"></param>
        public TipController(ITipApplication tip)
        {
            _tip = tip;
        }

        /// <summary>
        /// Obtener todos los tips registrados
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                var result = await _tip.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener todos los tips paginados
        /// </summary>
        /// <param name="request"></param>
        /// <param name="systemId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request, int systemId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (this.SystemId > 0)
                    systemId = this.SystemId;

                var result = await _tip.GetAll(request.CurrentPage, request.PageSize, request.Filter, systemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Actualizar los datos de un tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] TipUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _tip.UpdateTip(request);
                return Ok(result);
            }

            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// Agregar un nuevo tip
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Tip([FromBody]Project.Entities.Request.TipSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.SystemId = this.SystemId;
                var result = await _tip.AddTip(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar un tip existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _tip.DeleteTip(id);
                return Ok(result);
            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener un tip por identificador
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var result = await _tip.GetById(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }          
        }


    }
}
