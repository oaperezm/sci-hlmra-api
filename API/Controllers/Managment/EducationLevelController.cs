﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    /// <summary>
    /// Controlador para el catalogo EducationLevel
    /// </summary>
    [RoutePrefix("api/EducationLevel")]
    public class EducationLevelController : BaseController
    {
        private readonly IEducationLevelApplication _educationLevel;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="educationLevel"></param>
        public EducationLevelController(IEducationLevelApplication educationLevel)
        {
            this._educationLevel = educationLevel;
        }
        /// <summary>
        /// Obtener todos los niveles de educación 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _educationLevel.GetAll();
            return Ok(result);
        }
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] EducationLevelRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _educationLevel.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] EducationLevelUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _educationLevel.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

    }
}
