﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controllador para administrar libros
    /// </summary>
    [RoutePrefix("api/books")]
    public class BookController : BaseController
    {
        private readonly IBookApplication _Book;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Book"></param>
        public BookController(IBookApplication Book)
        {
            this._Book = Book;
        }

        /// <summary>
        /// Obtener todos los libros activos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var result = await _Book.GetById(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }          
        }

        /// <summary>
        /// Obtener todos los libros registrados por paginación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _Book.GetAll(request.CurrentPage, request.PageSize, request.Filter);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener todos los libros activos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _Book.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar un nuevo libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Book([FromBody]BookSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _Book.AddBook(request);
                return Content(HttpStatusCode.Created, result);               
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }           
        }

        /// <summary>
        /// Actualizar los datos d eun libro existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] BookUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _Book.UpdateBook(request);
                return Content(HttpStatusCode.OK, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar un libro por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _Book.DeleteBook(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

    }
}
