﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/blocks")]
    public class BlockController : BaseController
    {
        private readonly IBlockApplication _Block;

        public BlockController(IBlockApplication Block)
        {
            this._Block = Block;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Block.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Block.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Block([FromBody]BlockSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _Block.AddBlock(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] BlockUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _Block.UpdateBlock(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
