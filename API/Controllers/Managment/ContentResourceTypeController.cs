﻿using Project.Application.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    /// <summary>
    /// Api controller para el  tupo de tecursos
    /// </summary>
    [RoutePrefix("api/contentsresourcetypes")]
    public class ContentResourceTypeController : BaseController
    {
        private readonly IContentResourceTypeApplication _content;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="content"></param>
        public ContentResourceTypeController(IContentResourceTypeApplication content) {
            _content = content;
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _content.GetAll();
            return Ok(result);
        }
    }
}
