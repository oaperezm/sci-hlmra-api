﻿using Project.Application.Core;
using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    [RoutePrefix("api/section")]
    public class SectionController : BaseController
    {
        private readonly ISectionApplication _section;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="section"></param>
        public SectionController(ISectionApplication section)
        {
            this._section = section;
        }
        /// <summary>
        /// Obtener todas las secciones
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _section.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Ingresar  una sección
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] SectionsRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _section.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
        /// <summary>
        /// Actualizar los datos de una sección
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] SectionsUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _section.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
