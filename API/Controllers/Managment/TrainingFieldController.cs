﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/trainingfields")]
    public class TrainingFieldController : BaseController
    {
        private readonly ITrainingFieldApplication _TrainingField;

        public TrainingFieldController(ITrainingFieldApplication TrainingField)
        {
            this._TrainingField = TrainingField;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _TrainingField.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _TrainingField.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]TrainingFieldSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _TrainingField.AddTrainingField(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] TrainingFieldUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _TrainingField.UpdateTrainingField(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
