﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/areas")]
    public class AreaController : BaseController
    {
        private readonly IAreaApplication _Area;

        public AreaController(IAreaApplication Area)
        {
            this._Area = Area;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Area.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Area.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]AreaSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _Area.AddArea(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AreaUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _Area.UpdateArea(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
