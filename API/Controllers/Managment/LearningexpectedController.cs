﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    [RoutePrefix("api/Learningexpected")]
    public class LearningexpectedController : BaseController
    {
        private readonly ILearningexpectedApplication _learningexpected;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="learningexpected"></param>
        public LearningexpectedController(ILearningexpectedApplication learningexpected)
        {
            this._learningexpected = learningexpected;
        }
        /// <summary>
        /// Obtenerel catalogo de Aprendizaje esperado
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _learningexpected.GetAll();
            return Ok(result);
        }
        /// <summary>
        /// Ingresa un registro al catalogo de Aprendizaje esperado
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] LearningexpectedRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _learningexpected.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] LearningexpectedUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _learningexpected.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
