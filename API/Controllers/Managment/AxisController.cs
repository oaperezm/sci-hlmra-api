﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{   
    [RoutePrefix("api/axis")]
    public class AxisController : BaseController
    {
        private readonly IAxisApplication _axis;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="axis"></param>
        public AxisController(IAxisApplication axis)
        {
            this._axis = axis;
        }
        /// <summary>
        /// Obtener todos los niveles de educación 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _axis.GetAll();
            return Ok(result);
        }
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] AxisRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _axis.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AxisUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _axis.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
