﻿using Project.Application.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    /// <summary>
    /// Controlador para ciudades
    /// </summary>
    [RoutePrefix("api/cities")]
    public class CityController : BaseController
    {
        private readonly ICityApplication _city;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="city"></param>
        public CityController( ICityApplication city  ) {
            _city = city;
        }

        /// <summary>
        /// Obtener el catálogo de ciudades
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _city.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
            
        }

        /// <summary>
        /// Obtener los estados de una ciudad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/states")]
        public async Task<IHttpActionResult> GetStates(int id)
        {
            try
            {
                var result = await _city.GetStateByCity(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

            
        }
    }
}