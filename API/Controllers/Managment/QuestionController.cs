﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    /// <summary>
    /// Controlador que engloba todas las API's requeridas para el manejo de
    /// preguntas contenidas en un banco de preguntas
    /// </summary>
    [RoutePrefix("api/questions")]
    public class QuestionController : BaseController
    {
        private readonly IQuestionApplication _question;

        /// <summary>
        /// Constructor de controlador
        /// </summary>
        /// <param name="question"></param>
        public QuestionController(IQuestionApplication question)
        {
            this._question = question;
        }

        /// <summary>
        /// Obtener todas las preguntas registradas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _question.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Obtener los datos de una pregunta en especifico
        /// </summary>
        /// <param name="id">Identificador de la pregunta</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var idUser = UserId;

            var result = await _question.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Agregar una pregunta y sus respuestas al banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]QuestionSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _question.AddQuestion(request);
                return Content(HttpStatusCode.Created, result);                
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }            
        }

        /// <summary>
        /// Actualizar los datos de una pregunta existente
        /// </summary>
        /// <param name="request">ñ</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody] QuestionUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64)) {                 
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _question.UpdateQuestion(request);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }                        
        }

        /// <summary>
        /// Eliminar una pregunta del banco de preguntas de manera permanente
        /// </summary>
        /// <param name="id">Identificador principal de la pregunra</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return Content(HttpStatusCode.BadRequest, "El identificador de la pregunta es requerido");

                var result = await _question.DeleteQuestion(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

    }
}
