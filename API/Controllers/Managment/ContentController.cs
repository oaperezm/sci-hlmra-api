﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrador de contenidos
    /// </summary>
    [RoutePrefix("api/contents")]
    public class ContentController : BaseController
    {
        private readonly IContentApplication _content;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="content"></param>
        public ContentController(IContentApplication content)
        {
            this._content = content;
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _content.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// API para obtener todos los contenidos registrados paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationContentRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _content.GetAll(request.CurrentPage, request.PageSize, request, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// API que permite obtener un contenido dependiendo del identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var result = await _content.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// API que permite activar o desactivar un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Content([FromBody]ContentSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.SystemId = this.SystemId;
                var result = await _content.AddContent(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        ///  API que permite actualizar los datos de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] ContentUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _content.UpdateContent(request);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

        }

        /// <summary>
        /// Actualizar los tags de un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/tags")]
        public async Task<IHttpActionResult> UpdateTags([FromBody] ContentTagsRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _content.UpdateTags(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite activar o desactivar un contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("{id}/status")]
        public async Task<IHttpActionResult> UpdateStatuw([FromBody] ContentUpdateStatusRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _content.UpdateActiveStatus(request);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        /// <summary>
        /// API que permite eliminar de manera permanente un contenido
        /// </summary>
        /// <param name="id">Identificador unico del contenido</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Debe seleccionar un contenido para eliminar");

                var result = await _content.DeleteContent(id);

                return Ok(result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Asignar tags a un contenido existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("tags")]
        public async Task<IHttpActionResult> AssignTag(ContentAssingTagsRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _content.AssignatedTags(request);

                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

        }

        /// <summary>
        /// Descargar archivo del contenido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/download")]
        public async Task<HttpResponseMessage> DownloadFile(int id)
        {
            HttpResponseMessage response = Request.CreateResponse();
            try
            {
                if (id == 0)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Solicitud incorrecta");
                }

                var result = await _content.DownloadContent(id);
                string fname = result.Message.Split('|').Last();

                response.Headers.AcceptRanges.Add("bytes");
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StreamContent(result.Data);
                //response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                //response.Content.Headers.ContentDisposition.FileName = fname;                
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/" + fname.Split('.').Last());
                response.Content.Headers.ContentLength = result.Data.Length;


            }
            catch (Exception ex)
            {

                Request.CreateErrorResponse(HttpStatusCode.OK, "Solicitud incorrecta");
            }
            //return Ok(result);
            return response;
        }


        /// <summary>
        /// Agregar una nueva ODA con tamaño largo
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("create")]
        public async Task<IHttpActionResult> CreateContent()
        {

            try
            {
                var httpRequest = HttpContext.Current.Request;
                var countFile = httpRequest.Files.Count;
                var fileName = "";
                var tagsList = new List<TagRequest>();
                int id = int.Parse(HttpContext.Current.Request.Params.Get("id"));
                var result = default(Response);

                if (countFile > 0)
                {
                    fileName = httpRequest.Files[0].FileName;
                }
                else
                {
                    if (id == 0)
                        return BadRequest();
                }

                string name = HttpContext.Current.Request.Params.Get("name");
                string description = HttpContext.Current.Request.Params.Get("description");
                string thumbnails = HttpContext.Current.Request.Params.Get("thumbnails");
                string tags = HttpContext.Current.Request.Params.Get("tags");

                int.TryParse(HttpContext.Current.Request.Params.Get("contentResourceTypeId"), out int ContentResourceTypeId);
                int.TryParse(HttpContext.Current.Request.Params.Get("purposeId"), out int purposeId);
                int.TryParse(HttpContext.Current.Request.Params.Get("contentTypeId"), out int contentTypeId);
                bool.TryParse(HttpContext.Current.Request.Params.Get("isRecommended"), out bool isRecommended);
                int.TryParse(HttpContext.Current.Request.Params.Get("blockId"), out int blockId);
                int.TryParse(HttpContext.Current.Request.Params.Get("formativeFieldId"), out int formativeFieldId);
                int.TryParse(HttpContext.Current.Request.Params.Get("fileTypeId"), out int fileTypeId);
                bool.TryParse(HttpContext.Current.Request.Params.Get("active"), out bool active);
                bool.TryParse(HttpContext.Current.Request.Params.Get("isPublic"), out bool isPublic);


                int.TryParse(HttpContext.Current.Request.Params.Get("EducationLevellockId"), out int EducationLevelId);
                int.TryParse(HttpContext.Current.Request.Params.Get("AreaPersonalSocialDevelopmentId"), out int AreaPersonalSocialDevelopmentId);
                int.TryParse(HttpContext.Current.Request.Params.Get("AreasCurriculumAutonomyId"), out int AreasCurriculumAutonomyId);
                int.TryParse(HttpContext.Current.Request.Params.Get("AxisId"), out int AxisId);
                int.TryParse(HttpContext.Current.Request.Params.Get("KeylearningId"), out int KeylearningId);
                int.TryParse(HttpContext.Current.Request.Params.Get("LearningexpectedId"), out int LearningexpectedId);

                // DEsserializar los tags 
                if (!string.IsNullOrEmpty(tags))
                {
                    var tagsObjects = tags.Split(',');

                    foreach (var t in tagsObjects)
                    {
                        if (!string.IsNullOrEmpty(t))
                        {
                            var tagArray = t.Split('-');
                            tagsList.Add(new TagRequest { Id = int.Parse(tagArray[0]), Name = tagArray[1] });
                        }
                    }
                }

                string comment = HttpContext.Current.Request.Params.Get("comment");
                var fileStream = await this.ReadLargeStream();

                if (id == 0)
                {
                    var request = new ContentSaveRequest
                    {
                        FileTypeId = fileTypeId,
                        FileName = fileName,
                        IsPublic = isPublic,
                        Active = active,
                        ContentTypeId = contentTypeId,
                        PurposeId = purposeId,
                        Description = description,
                        Tags = tagsList,
                        Name = name,
                        Thumbnails = thumbnails,
                        SystemId = this.SystemId,
                        FileArray = fileStream,
                        BlockId = blockId,
                        IsRecommended = isRecommended,
                        FormativeFieldId = formativeFieldId,
                        EducationLevelId = EducationLevelId,
                        AreaPersonalSocialDevelopmentId = AreaPersonalSocialDevelopmentId,
                        AreasCurriculumAutonomyId = AreasCurriculumAutonomyId,
                        AxisId = AxisId,
                        KeylearningId = KeylearningId,
                        LearningexpectedId = LearningexpectedId,
                        ContentResourceTypeId = ContentResourceTypeId


                    };

                    result = await _content.AddContent(request);
                }
                else
                {
                    var request = new ContentUpdateRequest
                    {
                        FileTypeId = fileTypeId,
                        FileName = fileName,
                        IsPublic = isPublic,
                        Active = active,
                        ContentTypeId = contentTypeId,
                        PurposeId = purposeId,
                        Description = description,
                        Tags = tagsList,
                        Name = name,
                        Thumbnails = thumbnails,
                        FileArray = fileStream,
                        Id = id,
                        EducationLevelId = EducationLevelId,
                        AreaPersonalSocialDevelopmentId = AreaPersonalSocialDevelopmentId,
                        AreasCurriculumAutonomyId = AreasCurriculumAutonomyId,
                        AxisId = AxisId,
                        KeylearningId = KeylearningId,
                        LearningexpectedId = LearningexpectedId,
                        ContentResourceTypeId = ContentResourceTypeId,
                        FormativeFieldId = formativeFieldId,
                    };

                    result = await _content.UpdateContent(request);
                }

                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

        }

        /// <summary>
        /// Obtener contenido relacionado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/relation")]
        public async Task<IHttpActionResult> Relation(int id, int currentPage, int pageSize)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _content.GetRelatedContent(id, currentPage, pageSize, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Actualizar el número de visitas de un contenido 
        /// </summary>
        /// <param name="request"></param>       
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/visits")]
        public async Task<IHttpActionResult> UpdateVisits([FromBody]ContenteVisitsRequest request)
        {

            try
            {
                if (request.Id == 0)
                    return BadRequest();

                var result = await _content.UpdateVisits(request.Id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        #region PRIVATE METHODS

        /// <summary>
        /// Función para leer archivos grandes
        /// </summary>
        /// <returns></returns>
        private async Task<byte[]> ReadLargeStream()
        {
            byte[] fileBytes = null;
            string root = Path.GetTempPath();
            var provider = new MultipartFormDataStreamProvider(root);

            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.FileData)
            {
                var path = file.LocalFileName;
                byte[] content = File.ReadAllBytes(path);
                File.Delete(path);
                var streamMemory = new MemoryStream(content);

                fileBytes = streamMemory.ToArray();
            }

            return fileBytes;
        }

        #endregion
    }
}