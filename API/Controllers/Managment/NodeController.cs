﻿using Project.Application.Management;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para la dministración de nodos
    /// </summary>
    [RoutePrefix("api/nodes")]
    public class NodeController : BaseController
    {
        private readonly INodeApplication _Node;
      
        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="Node"></param>
        public NodeController(INodeApplication Node)
        {
            this._Node = Node;
        }

        /// <summary>
        /// Obtener todos los nodos registrados en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Node.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Obtener la información de un nodo en especifico
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Node.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene los nodos hijos en base a un identificador principal del padre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/subnodes")]
        public async Task<IHttpActionResult> GetSubnodesById(int id)
        {
            var idUser = UserId;

            var result = await _Node.GetSubnodes(id, this.SystemId);
            return Ok(result);
        }
        
        /// <summary>
        /// Permite registrar un nuevo nodo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]NodeSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.SystemId = this.SystemId;
                var result       = await _Node.AddNode(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }          
        }

        /// <summary>
        /// Permite actualizar los datos de un nodo existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] NodeUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                var result = await _Node.UpdateNode(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }          
        }

        /// <summary>
        /// Elimina los datos de un nodo existente de manera permanente
        /// </summary>
        /// <param name="id">Identificador principal dle nodo</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    throw new Exception("Debe seleccionar un nodo para eliminar");

                var result = await _Node.DeleteNode(id);
                return Content(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Obtiene el contenido de un nodo en especifico
        /// </summary>
        /// <param name="id">Identificador del nodo</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/contents")]
        public async Task<IHttpActionResult> GetNodeContens(int id)
        {
            
            var result = await _Node.GetNodeContens(id, this.RolId);
            return Ok(result);
        }


        /// <summary>
        /// Obtener el inventario de los nodos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("inventory")]
        public async Task<IHttpActionResult> GetInventory(int id) {

            try
            {
                if (id == 0)
                    return BadRequest();
                                
                var response = await _Node.GetInventoryFiles(id, this.SystemId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener los archivos de un tipo por nodo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileTypeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="sizePage"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("inventory-files")]
        public async Task<IHttpActionResult> GetInventoryFiles(int id,int fileTypeId, int currentPage, int sizePage)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _Node.GetInventoryFileTypes(id, fileTypeId, currentPage, sizePage, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }


        /// <summary>
        /// Obtener el contenido de un nodo de manera recursiva
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/all-contents")]
        public async Task<IHttpActionResult> GetContentsRecursive([FromUri]NodeAllContentRequest request)
        {

            try
            {
                if (request.NodeId == 0)
                    return BadRequest();

                var fileTypesId = new List<int>();
                fileTypesId     = request.FileTypeId?.Split(',').Select(x => int.Parse(x)).ToList();

                if (fileTypesId.Count == 1) {
                    if (fileTypesId[0] == 0)
                        fileTypesId = new List<int>();
                }

                var result = await _Node.GetNodeContentRecursive(request.NodeId,
                                                                    fileTypesId,
                                                                    request.PurposeId, 
                                                                    request.BlockId, 
                                                                 request.FormativeFieldId,
                                                                 request.CurrentPage,
                                                                 request.SizePage, 
                                                                 this.SystemId, 
                                                                 request.Filter,
                                                                 this.RolId
                                                                 , request.EducationLevelId
                                                                 , request.AreaPersonalSocialDevelopmentId
                                                                 , request.AreasCurriculumAutonomyId
                                                                 , request.AxisId
                                                                 , request.KeylearningId
                                                                 , request.LearningexpectedId
                                                                 );
                        return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener los libros de un nodo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/books")]
        public async Task<IHttpActionResult> GetBokks(int id, int page, int size)
        {

            var result = await _Node.GetBooksByNodeId(id,page, size,this.UserId);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene la estructura en cascada de un nodo proporcionado
        /// </summary>
        /// <param name="id">Identificador principal del nodo padre</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/structure")]
        public async Task<IHttpActionResult> GetStructureNode(int id) {

            try
            {
                if (id == 0)
                {
                    if (this.SystemId == (int)EnumSystems.SALI)
                        id = int.Parse(ConfigurationManager.AppSettings["SALI.Parent.Node"]);
                    else
                        id = int.Parse(ConfigurationManager.AppSettings["RA.Parent.Node"]);
                }
                    
                
                var result =  await _Node.GetNodeStructureByParentId(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Elimninar la imagen de un nodo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/image")]
        public async Task<IHttpActionResult> RemoveImage(int id)
        {
            try
            {
                if (id == 0)
                    throw new Exception("Debe seleccionar un nodo para eliminar");

                var result = await _Node.RemoveImage(id);
                return Content(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

            
            
        }

        /// <summary>
        /// Obtener el contenido por más visitados, los más recientes o los relacionados
        /// </summary>
        /// <param name="id"> Identificador del nodo</param>
        /// <param name="currentPage"> Página actual</param>
        /// <param name="sizePage">Cantidad de elementos por página</param>
        /// <param name="searchId">Tipo de búsqueda</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/academic-manager")]
        public async Task<IHttpActionResult> AcademicManager(int id, int currentPage, int sizePage, int searchId)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                if (searchId == 0)
                    searchId = (int)EnumContentSearch.Recent;

                var result = await _Node.GetContentByTypeSearch(id, currentPage, sizePage, searchId, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }
    }
}
