﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    [RoutePrefix("api/SchoolGrade")]
    public class SchoolGradeController : BaseController
    {
        private readonly ISchoolGradeApplication _schoolGrade;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="schoolGrade"></param>
        public SchoolGradeController(ISchoolGradeApplication schoolGrade)
        {
            this._schoolGrade = schoolGrade;
        }

        /// <summary>
        /// Obtiene el catalogo de grado de estudio
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _schoolGrade.GetAll();
            return Ok(result);
        }
        /// <summary>
        /// ActualizIngresaar un grado de estudio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] SchoolGradeRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _schoolGrade.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
        /// <summary>
        /// Actualizar los datos de un grado de estudio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] SchoolGradeUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _schoolGrade.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
