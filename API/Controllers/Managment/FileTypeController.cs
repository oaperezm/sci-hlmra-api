﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para actualizar 
    /// </summary>
    [RoutePrefix("api/filetypes")]
    public class FileTypeController : BaseController
    {
        private readonly IFileTypeApplication _fileType;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileType"></param>
        public FileTypeController(IFileTypeApplication fileType)
        {
            this._fileType = fileType;
        }

        /// <summary>
        /// Obtener todos los tipos de archivos ordenados por descripción
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _fileType.GetAll(this.SystemId);
            return Ok(result);
        }
        
        /// <summary>
        /// Agregar un nuevo tipo de archivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]FileTypeSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _fileType.AddFileType(request);
                return Ok(result);
            } else
                return Content(HttpStatusCode.BadRequest, "Solicitud incompleta");

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar el tipo de archivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] FileTypeUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _fileType.UpdateFileType(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
