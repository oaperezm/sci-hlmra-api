﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    [RoutePrefix("api/Keylearning")]
    public class KeylearningController : BaseController
    {
        private readonly IKeylearningApplication _keylearning;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="keylearning"></param>
        public KeylearningController(IKeylearningApplication keylearning)
        {
            this._keylearning = keylearning;
        }
        /// <summary>
        /// Obtine el catalogo de aprendizaje clave
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _keylearning.GetAll();
            return Ok(result);
        }
        /// <summary>
        /// Ingresa un registro al catalogo de aprendizaje clave
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] KeylearningRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _keylearning.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] KeylearningUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _keylearning.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
