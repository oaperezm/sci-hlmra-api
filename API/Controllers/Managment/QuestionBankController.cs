﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador que concentra todas las API's requeridas para obtener los 
    /// datos de un banco de preguntas
    /// </summary>
    [RoutePrefix("api/questionbanks")]
    public class QuestionBankController : BaseController
    {
        private readonly IQuestionBankApplication _QuestionBank;

        /// <summary>
        /// Controlador que contiene todas las API's requeridas para el CRUD 
        /// de bando de preguntas 
        /// </summary>
        /// <param name="QuestionBank"></param>
        public QuestionBankController(IQuestionBankApplication QuestionBank)
        {
            this._QuestionBank = QuestionBank;
        }
        
        /// <summary>
        /// Obtener los bancos de preguntas por paginación
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _QuestionBank.GetAll(request.CurrentPage, request.PageSize, request.Filter,this.RolId, this.UserId, this.SystemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener los datos de un banco de preguntas por identificador principal
        /// </summary>
        /// <param name="id">Identificador unico del banco de preguntas</param>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id, bool image = false)
        {
            try
            {
                var result = await _QuestionBank.GetById(id, image);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// API para registrar un nuevo banco de preguntas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]QuestionBankSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                              
                var userId       = this.UserId;
                request.SystemId = this.SystemId;

                var result = await _QuestionBank.AddQuestionBank(request, userId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// API para actualizar los datos de un banco de pregunta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] QuestionBankUpdateRequest request)
        {
            try
            {
                var result = await _QuestionBank.UpdateQuestionBank(request);
                return Content(HttpStatusCode.OK, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// API para actualizar los datos de un banco de pregunta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("{id}/status")]
        public async Task<IHttpActionResult> UpdateStatus([FromBody] QuestionBankStatusRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");

                var result = await _QuestionBank.UpdateStatusQuestionBank(request);
                return Ok(result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// OBtener todos los bancos de preguntas asignados a un nodo en especifico
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("node")]
        public async Task<IHttpActionResult> GetByNodeId(int nodeId)
        {
            try
            {
                var userId = this.UserId;
                var result = await _QuestionBank.GetByNodeId(nodeId, userId, this.SystemId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener la estructura de todos los bancos de preguntas que tiene el usuario acceso
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("structure")]
        public async Task<IHttpActionResult> GetStructure()
        {
            try
            {
                var userId = this.UserId;
                var result = await _QuestionBank.GetStructuraWithQuestionBank(userId, this.SystemId);

                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener un listado de bancos de preguntas por identificador
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("list")]
        public async Task<IHttpActionResult> GetQuestionBanks(string ids)
        {

            try
            {
                if (ids == "")
                    return BadRequest();

                var bankQuestionsIds = ids.Split(',').Select(x => int.Parse(x)).ToList();
                var result = await _QuestionBank.GetByListIds(bankQuestionsIds);

                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Guardar los datos de un banco de pregunta cargado con CVS
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("load")]
        public async Task<IHttpActionResult> Load(QuestionBankLoadRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                request.SystemId = this.SystemId;
                request.UserId   = this.UserId;
                
                var result = await _QuestionBank.SaveLoad(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener información de un archivo CSV y convertida en un objeto
        /// tipo de objeto QuestionBank
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("parse-file")]
        public async Task<IHttpActionResult> ParseFile()
        {

            var response = new Response();

            try
            {
                var httpRequest         = HttpContext.Current.Request;
                var countFile           = httpRequest.Files.Count;
                var fileName            = "";
                List<string> searchList = new List<string>();

                if (countFile > 0)
                {
                    fileName = httpRequest.Files[0].FileName;

                    if(fileName.ToLower().IndexOf(".csv") == -1) {
                        return BadRequest("Extensión de archivo no permitida");
                    }
                }
                else
                {
                   return BadRequest();
                }

                var fileStream = await this.ReadLargeStream();

                if (fileStream == null)
                    return BadRequest();

                using (var reader = new StreamReader(fileStream, Encoding.GetEncoding("iso-8859-1")))
                {                   
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        searchList.Add(line);
                    }
                }

                if (searchList.Count > 0)
                    response.Data =  await ParseFile(searchList);

                response.Success = true;
                response.Message = "Preguntas obtenidas correctamente";
                
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }           
        }
        
            
        #region PRIVATE METHODS
        
        /// <summary>        
        /// Función para leer archivos grandes
        /// </summary>
        /// <returns></returns>
        private async Task<MemoryStream> ReadLargeStream()
        {
            
            var streamMemory = default(MemoryStream);
            string root = Path.GetTempPath();
            var provider = new MultipartFormDataStreamProvider(root);

            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.FileData)
            {
                var path = file.LocalFileName;
                byte[] content = File.ReadAllBytes(path);
                File.Delete(path);

                streamMemory = new MemoryStream(content);
            }

            return streamMemory;
        }

        /// <summary>
        /// Convertir datos de la cadena
        /// </summary>
        /// <returns></returns>
        private async Task<QuestionBankLoadRequest> ParseFile(List<string> searchList) {

            try
            {
                var questionBank = new QuestionBankLoadRequest();
                var questions    = new List<QuestionRequest>();
                var answers      = new List<Answer>();

                searchList = searchList.Skip(1).ToList();

                searchList.ForEach(s =>
                {
                    var stringItems = s.Split(',');

                    if(stringItems.Length == 0)
                        stringItems = s.Split(';');

                    if (stringItems.Length > 0) {

                        if (stringItems[0].ToLower().Contains("pregunta"))
                        {
                            var question = new QuestionRequest
                            {
                                QuestionId  = int.Parse(stringItems[1]),
                                Content     = stringItems[4],
                                Explanation = stringItems[5],
                            };

                            switch (stringItems[3]?.ToLower()) {
                                case "multiple":
                                case "múltiple":
                                    question.QuestionTypeId = 1;
                                    question.QuestionType = "Múltiple";
                                    break;
                                case "relacional":
                                    question.QuestionTypeId = 3;
                                    question.QuestionType = "Relacional";
                                    break;
                                case "v/f":
                                    question.QuestionTypeId = 4;
                                    question.QuestionType = "Verdadero / Falso";
                                    break;
                                default: // Abierta
                                    question.QuestionTypeId = 2;
                                    question.QuestionType = "Abierta";
                                    break;
                            }
                            questions.Add(question);

                        } else {

                            var answer = new Answer
                            {
                                QuestionId  = int.Parse(stringItems[2]),
                                Description = stringItems[4]                                
                            };

                            if (stringItems[3].ToLower().Equals("multiple") || stringItems[3].ToLower().Equals("v/f")) {
                                if (stringItems[6].ToLower().Equals("c"))
                                    answer.IsCorrect = true; 
                            } else if (stringItems[3].ToLower().Equals("relacional")) {
                                answer.RelationDescription = stringItems[6];
                            }

                            answers.Add(answer);
                        }
                    }                   

                });

                questions.ForEach(q => q.Answers = answers.Where( x=> x.QuestionId == q.QuestionId).ToList() );
                questionBank.Questions = questions;
                
                return questionBank;
            }
            catch (Exception)
            {

                return new QuestionBankLoadRequest();
            }
        }

        #endregion
    }
}
