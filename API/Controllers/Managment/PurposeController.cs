﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/purposes")]
    public class PurposeController : BaseController
    {
        private readonly IPurposeApplication _Purpose;

        public PurposeController(IPurposeApplication Purpose)
        {
            this._Purpose = Purpose;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Purpose.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Purpose.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]PurposeSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _Purpose.AddPurpose(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] PurposeUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _Purpose.UpdatePurpose(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
