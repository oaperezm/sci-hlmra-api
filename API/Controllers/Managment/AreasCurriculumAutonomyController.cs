﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{   /// <summary>
    /// Controlador para el catalogo AreasCurriculumAutonomy 
    /// </summary>
    [RoutePrefix("api/AreasCurriculumAutonomy")]
    public class AreasCurriculumAutonomyController : BaseController
    {
        private readonly IAreasCurriculumAutonomyApplication _areasCurriculumAutonomy;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="areasCurriculumAutonomy"></param>
        public AreasCurriculumAutonomyController(IAreasCurriculumAutonomyApplication areasCurriculumAutonomy)
        {
            this._areasCurriculumAutonomy = areasCurriculumAutonomy;
        }
        /// <summary>
        /// Obtener todos los tipos de contenido
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _areasCurriculumAutonomy.GetAll();
            return Ok(result);
        }


        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] AreasCurriculumAutonomyRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _areasCurriculumAutonomy.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AreasCurriculumAutonomyUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _areasCurriculumAutonomy.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
