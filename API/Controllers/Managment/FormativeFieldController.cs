﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/formativefields")]
    public class FormativeFieldController : BaseController
    {
        private readonly IFormativeFieldApplication _FormativeField;

        public FormativeFieldController(IFormativeFieldApplication FormativeField)
        {
            this._FormativeField = FormativeField;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _FormativeField.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _FormativeField.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]FormativeFieldSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _FormativeField.AddFormativeField(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] FormativeFieldUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _FormativeField.UpdateFormativeField(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
