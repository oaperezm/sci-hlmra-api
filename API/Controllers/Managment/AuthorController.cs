﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrar autores
    /// </summary>
    [RoutePrefix("api/authors")]
    public class AuthorController : BaseController
    {
        private readonly IAuthorApplication _author;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="author"></param>
        public AuthorController(IAuthorApplication author)
        {
            _author = author;
        }

        /// <summary>
        /// Obtener todos los autores por paginación
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var result = await _author.GetById(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }          
        }
        
        /// <summary>
        /// Obtener todos los autores activos registrados
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                var result = await _author.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// Agregar un nuevo autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Author([FromBody]AuthorSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _author.AddAuthor(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }           
        }

        /// <summary>
        /// Obtener todos los registros paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _author.GetAll(request.CurrentPage, request.PageSize, request.Filter);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }


        /// <summary>
        /// Actualizar los datos de un autor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AuthorUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _author.UpdateAuthor(request);
                return Ok(result);
            }

            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }           
        }

        /// <summary>
        /// Eliminar un author existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _author.DeleteAuthor(id);
                return Ok(result);
            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

    }
}
