﻿using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para el tipo de contenido
    /// </summary>
    [RoutePrefix("api/contenttypes")]
    public class ContentTypeController : BaseController
    {
        private readonly IContentTypeApplication _contentType;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contentType"></param>
        public ContentTypeController(IContentTypeApplication contentType)
        {
            this._contentType = contentType;
        }

        /// <summary>
        /// Obtener todos los tipos de contenido
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _contentType.GetAll();
            return Ok(result);
        }

           
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] ContentTypeSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _contentType.AddContentType(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] ContentTypeUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _contentType.UpdateContentType(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
