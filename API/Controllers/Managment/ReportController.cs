﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{
    [RoutePrefix("api/report")]
    public class ReportController : BaseController
    {
        private readonly IReportApplication _report;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="report"></param>
        public ReportController(IReportApplication report)
        {
            this._report = report;
        }
        /// <summary>
        /// Obtener todos los reportes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _report.GetAll();
            return Ok(result);
        }
        /// <summary>
        /// Ingresa un nuevo reporte
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] ReportsRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _report.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualiza un registro de reporte
        /// </summary>
        /// <returns></returns>


        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] ReportsUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _report.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }
    }
}
