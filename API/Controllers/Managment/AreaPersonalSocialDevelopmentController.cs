﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Managment
{    /// <summary>
     /// Controlador para el AreaPersonalSocialDevelopment 
     /// </summary>
    [RoutePrefix("api/AreaPersonalSocialDevelopment")]
    public class AreaPersonalSocialDevelopmentController : BaseController
    {
        private readonly IAreaPersonalSocialDevelopmentApplication _areaPersonalSocialDevelopment;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="areaPersonalSocialDevelopment"></param>
        public AreaPersonalSocialDevelopmentController(IAreaPersonalSocialDevelopmentApplication areaPersonalSocialDevelopment)
        {
            this._areaPersonalSocialDevelopment = areaPersonalSocialDevelopment;
        }
        /// <summary>
        /// Obtener todos los tipos de contenido
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _areaPersonalSocialDevelopment.GetAll();
            return Ok(result);
        }


        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] AreaPersonalSocialDevelopmentRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _areaPersonalSocialDevelopment.Add(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de un tipo de contenido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AreaPersonalSocialDevelopmentUpdateRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _areaPersonalSocialDevelopment.Update(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

    }
}
