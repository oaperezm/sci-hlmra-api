﻿using Project.Application.Management;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/nodecontents")]
    public class NodeContentController : BaseController
    {
        private readonly INodeContentApplication _NodeContent;

        public NodeContentController(INodeContentApplication NodeContent)
        {
            this._NodeContent = NodeContent;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _NodeContent.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _NodeContent.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]NodeContentSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                request.SystemId = this.SystemId;

                var result = await _NodeContent.AddNodeContent(request);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }            
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] NodeContentUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _NodeContent.UpdateNodeContent(request);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }            
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    throw new Exception("El identificador del contenido es requerido");

                var result = await _NodeContent.DeleteNodeContent(id);
                return Ok(result);


            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message}");
            }
        }


    }
}
