﻿using Project.Application.Core;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{   /// <summary>
    /// Controller encargado de administrar los cursos planeados
    /// </summary>
    [RoutePrefix("api/courseplanning")]
    public class CoursePlanningController : BaseController
    {
        private readonly ICoursePlanningApplication _courseplannig;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="courseplanning"></param>
        public CoursePlanningController(ICoursePlanningApplication courseplanning)
        {
            this._courseplannig = courseplanning;
        }
        /// <summary>
        /// API que permite registrar un nuevo plan de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]CoursePlanningSaveRequest request)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseplannig.Add(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
        /// <summary>
        /// API que permite actualiza un nuevo plan de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]CoursePlanningUpdateRequest request)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseplannig.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite obtener todos los cursos planeados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseplannig.GetAll();

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Eliminar un curso de manera permanente, siempre y cuando no tenga
        /// grupos relacionados ni secciones
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _courseplannig.Delete(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
        /// <summary>
        /// API que permite obtener todos los cursos planeados por grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("Getgroup")]
        public async Task<IHttpActionResult> Getgroup(int Id)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseplannig.GetByGroup(Id);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite obtener todos los cursos planeados por grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("Student")]
        public async Task<IHttpActionResult> GetByStudentId(int StudentGroupId)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseplannig.GetByStudentId(this.UserId, StudentGroupId);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
    }
}
