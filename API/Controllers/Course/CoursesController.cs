﻿using Project.Application.Core;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controller encargado de administrar los cursos 
    /// </summary>
    [RoutePrefix("api/courses")]
    public class CoursesController : BaseController
    {
        private readonly ICourseApplication _course;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="course"></param>
        public CoursesController(ICourseApplication course)
        {
            this._course = course;
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _course.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }
        }

        /// <summary>
        /// API que permite obtener los datos de un curso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var result = await _course.GetById(id);
                return Ok(result);
            }              
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }           
        }


        /// <summary>
        /// API que permite obtener los cursos dados de alta por un profesor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        public async Task<IHttpActionResult> GetByUserId()
        {
            try
            {

                var syatemId = this.SystemId;
                var userId = this.UserId;
                var result = await _course.GetAllByIdUser(userId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite registrar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]CourseSaveRequest request)
        {
            try
            {
                request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);                             

                var result = await _course.Add(request);

                if( result.Success )
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite actualizar los datos de nu curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]CourseUpdateRequest request)
        {
            try
            {               

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _course.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.OK, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Eliminar un curso de manera permanente, siempre y cuando no tenga
        /// grupos relacionados ni secciones
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _course.Delete(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite obtener todos los cursoS y sus respectivos grupos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("coursegroup")]
        public async Task<IHttpActionResult> coursegroup()
        {
            try
            {
                var result = await _course.GetAllCourseGroup();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }

    }

}
