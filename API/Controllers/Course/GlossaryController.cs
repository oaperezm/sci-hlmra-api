﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrar los glosarios
    /// </summary>    
    [RoutePrefix("api/glossaries")]
    public class GlossaryController : BaseController
    {
        private readonly IGlossaryApplication _glossary;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="glossary"></param>
        public GlossaryController(IGlossaryApplication glossary) {
            _glossary = glossary;
        }

        #region Glossary

        /// <summary>
        /// Obtener todos los glosarios de un profesor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        public async Task<IHttpActionResult> GetByUser([FromUri] PaginationRequest request)
        {
            try
            {
                var userId = this.UserId;

                if (userId == 0)
                    return BadRequest();

                var result = await _glossary.GetAllByUserId(userId);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtener los datos de un glosario por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Geit(int id)
        {
            try
            {
                var userId = this.UserId;

                if (userId == 0)
                    return BadRequest();

                var result = await _glossary.GetAllById(userId, id);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Registra un nuevo glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post(GlossarySaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                request.UserId = this.UserId;
                var result = await _glossary.Add(request);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Actualizar los datos generales del glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put(GlossaryUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _glossary.Update(request);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Registra un nuevo glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/node")]
        public async Task<IHttpActionResult> AssignNode(AssignNodeRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                request.UserId = this.UserId;

                var result = await _glossary.AssignToNode(request);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Elimina la asignación del glosario a un nodo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/node")]
        public async Task<IHttpActionResult> RemoveNode(int id, int nodeId)
        {
            try
            {
                var result = await _glossary.RemoveToNode(nodeId, id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar de manera permanente los datos de un glosario y sus terminos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");

                var result = await _glossary.Delete(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Obtener los glosarios de los grupos a los cuales esta inscrito el 
        /// alumno
        /// </summary>
        /// <param name="nodeId">Identificador del libro</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("student")]
        public async Task<IHttpActionResult> GetByStudent(int nodeId) {

            try
            {
                if (nodeId == 0)
                    return BadRequest();

                if (this.UserId == 0)
                    return BadRequest();

                var result = await _glossary.GetByStudent(nodeId,this.UserId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtener todos los glosarios de un usuario paginados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _glossary.GetAll(request.CurrentPage, request.PageSize, request.Filter,this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        #endregion

        #region Words

        /// <summary>
        /// Agregar un nuevo termino a un glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/word")]
        public async Task<IHttpActionResult> AddWord(WordSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                                
                var result = await _glossary.AddWord(request);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar un nuevo termino a un glosario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("{id}/word")]
        public async Task<IHttpActionResult> UpdateWord(WordUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _glossary.UpdateWord(request);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

         /// <summary>
         /// Eliminar un termino de un glosario
         /// </summary>
         /// <param name="id"></param>
         /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/word")]
        public async Task<IHttpActionResult> DeleteWord(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _glossary.DeleteWord(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        #endregion
    }
}
