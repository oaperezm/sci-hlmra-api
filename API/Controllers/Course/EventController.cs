﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{    /// <summary>
     /// Controller encargado de administrar el catalogo de eventos eventos
     /// </summary>
    [RoutePrefix("api/Event")]
    public class EventController : BaseController
    {
        private readonly IEventApplication _event;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="events"></param>
        public EventController(IEventApplication events)
        {
            this._event = events;
        }
        /// <summary>
        /// API que permite registrar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post(EventRequest request)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _event.Add(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
        /// <summary>
        /// Eliminar un evento de manera permanente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _event.Delete(id);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
    }
}
