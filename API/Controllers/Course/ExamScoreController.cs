﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    /// <summary>
    /// Controlador utilizado para la programación de exámenes
    /// </summary>
    [RoutePrefix("api/examscores")]
    public class ExamScoreController : BaseController
    {
        private readonly IExamScoreApplication _examScore;

        /// <summary>
        /// Inicializador del objeto
        /// </summary>
        /// <param name="examScore"></param>
        public ExamScoreController(IExamScoreApplication examScore) {
            this._examScore = examScore;
        }

        /// <summary>
        /// Obtener las calificaciones de examen por alumno
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [Route("student")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByTeacher()
        {
            try
            {
                var result = await _examScore.GetAllByUserId(this.UserId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
            
        }

        /// <summary>
        /// Obtener las programaciones de examen por profesor
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [Route("group")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByGroup(int groupId)
        {
            try
            {
                if (groupId == 0)
                    return BadRequest("Es requerido el identificador del grupo");

                var result = await _examScore.GetAllByStudentGroupId(groupId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar una nueva calificación de exámenes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]ExamScoreSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("Es requerido el identificador del alumno");

                var result = await _examScore.Add(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar una nueva calificación de exámenes
        /// </summary>
        /// <param name="scoreList">Lista de calificaciones a guardar</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost]
        //[Authorize]
        [Route("groupscore")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]List<ExamScoreSaveRequest> scoreList)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("El parámetro scoreList no es correcto");

                Response result = await _examScore.SaveGroupScore(scoreList);                
              
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar los datos de una programación de exámenes existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]ExamScoreUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                
                var result = await _examScore.Update(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Permite eliminar el registro de una programación de exámenes
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámen</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Es requerido el identificador de la programación de exámen");

                var result = await _examScore.Delete(id);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

    }
}
