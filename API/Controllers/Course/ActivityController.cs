﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller Activity
    /// </summary>
    [RoutePrefix("api/activities")]
    public class ActivityController : BaseController
    { 
        private readonly IActivityApplication _activity;
        private readonly IActivityStudentFileApplication _activityStudentFileApplication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="activity"></param> 
        public ActivityController(IActivityApplication activity)
        {
            this._activity = activity;
        }

        #region Activity

        /// <summary>
        /// Agregar una nueva programación de tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]ActivitySaveRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                request.UserId = this.UserId;

                var result = await _activity.Add(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar los datos de una programación de tarea
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]ActivityUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _activity.Update(request);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener las tareas por grupo
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("group")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByStudentGroup(int studentGroupId)
        { 
            try
            {
                if (studentGroupId == 0)
                    return BadRequest();

                var result = await _activity.GetAllByGroup(studentGroupId);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Eliminar una programación de tarea
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _activity.Remove(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Obtener las tareas de un grupo con su calificación
        /// </summary>
        /// <param name="id"></param>
        /// <param name="studentGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("student")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetStudentScore(int studentGroupId)
        {

            try
            {
                if (studentGroupId == 0)
                    return BadRequest();

                var result = await _activity.GetAllWithScore(studentGroupId, this.UserId);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }


        /// <summary>
        /// Obtener todas las respuestas de una tarea 
        /// </summary>
        /// <param name="id">Identificador de la programación de la tarea</param>
        /// <param name="studentGroupId">Identificador del gupo</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("score")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetAnswers(int id, int studentGroupId)
        {

            try
            {
                if (id == 0 || studentGroupId == 0)
                    return BadRequest();

                var result = await _activity.GetStudentsAnswers(studentGroupId, id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }


        /// <summary>
        /// Realizar la calificación de un grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("score")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> SaveScore([FromBody] ActivityScoreRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _activity.SaveScore(request);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Cerrar una tarea ya calificada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("close")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdateStatus([FromBody] ActivityUpdateStatusRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _activity.UpdateStatus(request.ActivityId, request.Status);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }



        #endregion

        #region Activities Student Files

        /// <summary>
        /// Agregar una archivo de respuesta a una actividad
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("uploadStudentFile")]
        public async Task<IHttpActionResult> UploadStudentFile()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var countFile = httpRequest.Files.Count;
                var fileName = "";

                if (countFile > 0)
                    fileName = httpRequest.Files[0].FileName;
                else
                    return BadRequest();

                int.TryParse(HttpContext.Current.Request.Params.Get("activityId"), out int activityId);
                string comment = HttpContext.Current.Request.Params.Get("comment");
                var fileStream = await this.ReadLargeStream();

                var request = new ActivityStudentFileSaveRequest
                {
                    Comment = comment,
                    UserId = this.UserId,
                    ActivitiesId = activityId,
                    FileArray = fileStream,
                    FileName = fileName
                };

                var result = await _activityStudentFileApplication.AddFile(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Eliminar un archivo de la actividad de alumno
        /// </summary>
        /// <param name="id">Identificador de la tarea</param>
        /// <param name="answerId">Identificador del archivo de la respuesta</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/deleteStudentFile")]
        public async Task<IHttpActionResult> DeleteStudentFile(int id)
        { 
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _activityStudentFileApplication.DeleteFile(id, this.UserId);
                return Content(HttpStatusCode.OK, result);  
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// Obtener los archivos de un alumno de una actividad 
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/studentFile")]
        public async Task<IHttpActionResult> GetStudentFile(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _activityStudentFileApplication.GetStundentFileByActivity(id, this.UserId);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        #endregion


        #region ACTIVITY ANSWER

        /// <summary>
        /// Agregar una archivo de respuesta a una actividad
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("uploadAnswer")]
        public async Task<IHttpActionResult> UploadAnswer()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var countFile = httpRequest.Files.Count;
                var fileName = "";

                if (countFile > 0)
                    fileName = httpRequest.Files[0].FileName;
                else
                    return BadRequest();

                int.TryParse(HttpContext.Current.Request.Params.Get("activityId"), out int homeWorkId);
                string comment = HttpContext.Current.Request.Params.Get("comment");
                var fileStream = await this.ReadLargeStream();

                var request = new ActivityAnswerSaveRequest
                {
                    Comment = comment,
                    UserId = this.UserId,
                    ActivityId = homeWorkId,
                    FileArray = fileStream,
                    FileName = fileName
                };

                var result = await _activity.AddAnswer(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Eliminar un archivo de la respuesta dle alumno
        /// </summary>
        /// <param name="id">Identificador de la tarea</param>
        /// <param name="answerId">Identificador del archivo de la respuesta</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/deleteAnswer")]
        public async Task<IHttpActionResult> DeleteAnswer(int id, int answerId)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                if (answerId == 0)
                    return BadRequest();

                var result = await _activity.DeleteAnswer(id, this.UserId, answerId);
                return Content(HttpStatusCode.OK, result);


            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// Obtener los archivos de un alumno de una actividad 
        /// </summary>
        /// <param name="id">Identificador de la tarea</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/answers")]
        public async Task<IHttpActionResult> GetAnswers(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _activity.GetStundentFileByActivity(id, this.UserId);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        #endregion
        #region PRIVATE METHODS

        /// <summary>
        /// Función para leer archivos grandes
        /// </summary>
        /// <returns></returns>
        private async Task<byte[]> ReadLargeStream()
        {
            byte[] fileBytes = null;
            string root = Path.GetTempPath();
            var provider = new MultipartFormDataStreamProvider(root);

            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.FileData)
            {
                var path = file.LocalFileName;
                byte[] content = File.ReadAllBytes(path);
                File.Delete(path);
                var streamMemory = new MemoryStream(content);

                fileBytes = streamMemory.ToArray();
            }

            return fileBytes;
        }

        #endregion
    }
}