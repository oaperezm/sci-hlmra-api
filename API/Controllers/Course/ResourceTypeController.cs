﻿using Project.Application.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controlador para administración de tipo de recursos
    /// </summary>
    [RoutePrefix("api/resourcestypes")]
    public class ResourceTypeController : BaseController
    {

        private readonly IResourceTypeApplication _resourceType;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="resourceType"></param>
        public ResourceTypeController(IResourceTypeApplication resourceType) {
            this._resourceType = resourceType;
        }

        /// <summary>
        /// Obtener todos los tipos de recursos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _resourceType.GetAll();
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

      
    }
}