﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller para administrar la programación de eventos
    /// </summary>
    [RoutePrefix("api/events")]
    public class ScheduledEventController : BaseController
    {
        private readonly IScheduledEventApplication _scheduledEventApplication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scheduledEventApplication"></param>
        public ScheduledEventController(IScheduledEventApplication scheduledEventApplication) {
            this._scheduledEventApplication = scheduledEventApplication;
        }

        /// <summary>
        /// Agregar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]ScheduledEventSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                request.UserId = this.UserId;

                var result = await _scheduledEventApplication.Add(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar los datos de un evento existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]ScheduledEventUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _scheduledEventApplication.Update(request);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Eliminar un evento existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _scheduledEventApplication.Delete(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Obtener los eventos por curso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("course")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByCourse(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _scheduledEventApplication.GetByCourse(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Obtener los eventos por fecha
        /// </summary>
        /// <param name="date">Fecha de los eventos</param>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("student")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByStudent(DateTime? date)
        {
            try
            {
                if (date == null)
                    return BadRequest();

                var result = await _scheduledEventApplication.GetByStudent(this.UserId, date.Value);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener los eventos por fecha
        /// </summary>
        /// <param name="date">Fecha de los eventos</param>        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByTeacher(DateTime? date)
        {
            try
            {
                if (date == null)
                    return BadRequest();

                var result = await _scheduledEventApplication.GetByTeacher(this.UserId, date.Value);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}
