﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller encargado de administrar los cursos planeados
    /// </summary>
    [RoutePrefix("api/schedulingpartial")]
    public class SchedulingPartialController : BaseController
    {
        private readonly ISchedulingPartialApplication _schedulingPartial;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="schedulingPartial"></param>
        public SchedulingPartialController(ISchedulingPartialApplication schedulingPartial)
        {
            this._schedulingPartial = schedulingPartial;
        }

        /// <summary>
        /// Obtiene los parciales asociados al plan del curso por grupo              
        /// </summary>
        /// <param name="coursePlanningId"></param>     
        [HttpGet]
        [Authorize]
        [Route("GetAll")]
        public async Task<IHttpActionResult> GetAll(int coursePlanningId)
        {
            try
            {
                if (coursePlanningId <= 0)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.GetAll(coursePlanningId);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Obtiene los parciales asociados al plan del curso por grupo              
        /// </summary>
        /// <param name="coursePlanningId"></param>     
        [HttpGet]
        [Authorize]
        [Route("GetByStudentGroup")]
        public async Task<IHttpActionResult> GetByStudentGroup(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.GetByStudentGroup(id);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Obtiene la información del parcial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.GetById(id);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

        }

        /// <summary>
        /// Agrega una configuración de partial para el curso y grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]SchedulingPartialSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.Add(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Actualiza una configuración de partial para el curso y grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]SchedulingPartialUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="coursePlanningId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id, int coursePlanningId)
        {
            try
            {
                if (id <= 0 || coursePlanningId <= 0)
                    return BadRequest(ModelState);

                var result = await this._schedulingPartial.Delete(id, coursePlanningId);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
    }
}