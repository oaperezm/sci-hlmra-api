﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controlador ara sitios de interés
    /// </summary>
    [RoutePrefix("api/linkinterests")]
    public class LinkInterestController : BaseController
    {

        private readonly ILinkInterestApplication _linkInterest;

        /// <summary>
        /// Constructor del controlador sitio de interés
        /// </summary>
        /// <param name="linkInterest"></param>
        public LinkInterestController(ILinkInterestApplication linkInterest) {
            _linkInterest = linkInterest;
        }

        /// <summary>
        /// Obtener todas las ligas de interes  
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetByCourse(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("El identificador del curso es requerido");

                var result = await _linkInterest.GetByCourse(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Obtener todos los sitios de interés por el libro
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetByNodeUser(int nodeId)
        {
            try
            {
                if (nodeId == 0)
                    return BadRequest("El identificador del libro es requerido");

                var result = await _linkInterest.GetByStudent(this.UserId, nodeId);
             
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Guardar los datos de un nuevo link e interés
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post(LinkInterestSaveRequest request) {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _linkInterest.Add(request);
                return Ok(result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Actualizar los datos de un link de interés
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put(LinkInterestUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _linkInterest.Update(request);
                return Ok(result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Permite eliminar un link de interés 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id) {

            try
            {
                if (id == 0)
                    return BadRequest("El identificador del link de interes");

                var result = await _linkInterest.Delete(id);
                return Ok(result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

    }    
}