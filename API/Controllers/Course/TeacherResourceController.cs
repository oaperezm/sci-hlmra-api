﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Administración de los recursos del maestro en la paltaforma
    /// </summary>
    [RoutePrefix("api/resources")]
    public class TeacherResourceController : BaseController
    {
        public readonly ITeacherResourceApplication _teacherResourceApplication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="teacherResourceApplication"></param>
        public TeacherResourceController(ITeacherResourceApplication teacherResourceApplication) {
            this._teacherResourceApplication = teacherResourceApplication;
        }


        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {            
            return Ok();
        }

        /// <summary>
        /// Agregar un nuevo recurso del profesor (recomendado para archivos básicos y en base64)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]TeacherResourceSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (!string.IsNullOrEmpty(request.FileContentBase64))
                {
                    request.FileArray = System.Convert.FromBase64String(request.FileContentBase64);
                }

                request.UserId = this.UserId;
                var result     = await _teacherResourceApplication.Add(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Obtener todos los recursos registrados de un profesor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        public async Task<IHttpActionResult> GetByUser() {

            try
            {
                if(this.UserId == 0)
                    return BadRequest("Usuario no valido");

                var result = await _teacherResourceApplication.GetByUser(this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }

        }
        
        /// <summary>
        /// Eliminar los datos de un recurso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
       
                if (this.UserId == 0 || id == 0)
                    return BadRequest("Usuario no valido");

                var result = await _teacherResourceApplication.Delete(id, this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// Subir archivos del maestro
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("Upload")]
        public async Task<IHttpActionResult> UploadFile()
        {
            try
            {                               
                var httpRequest = HttpContext.Current.Request;
                var countFile   = httpRequest.Files.Count;
                var fileName    = "";
                
                if (countFile > 0)                
                    fileName = httpRequest.Files[0].FileName;                
                else 
                    return BadRequest();
                
                int.TryParse(HttpContext.Current.Request.Params.Get("teacherResourceId"), out int teacherResourceId);

                string name           = HttpContext.Current.Request.Params.Get("name");
                string description    = HttpContext.Current.Request.Params.Get("description");
                var fileStream        = await this.ReadLargeStream();

                var request = new TeacherResourceSaveRequest
                {
                    Name              = name,
                    Description       = description,
                    UserId            = this.UserId,
                    FileArray         = fileStream,
                    FileName          = fileName,
                    TeacherResourceId = teacherResourceId
                };

                var result = await _teacherResourceApplication.Add(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Función para leer archivos grandes
        /// </summary>
        /// <returns></returns>
        private async Task<byte[]> ReadLargeStream()
        {
            byte[] fileBytes = null;
            string root      = Path.GetTempPath();
            var provider     = new MultipartFormDataStreamProvider(root);

            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.FileData)
            {
                var path = file.LocalFileName;
                byte[] content = File.ReadAllBytes(path);
                File.Delete(path);
                var streamMemory = new MemoryStream(content);

                fileBytes = streamMemory.ToArray();
            }

            return fileBytes;
        }
    }
}
