﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;


using Project.Entities.DTO;
using System.Net.Http.Headers;

namespace API.Controllers
{
    /// <summary>
    /// Controller para administrar a todos los estudiantes
    /// </summary>
    [RoutePrefix("api/students")]
    public class StudentController : BaseController
    {
        private readonly IStudentApplication _Student;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Student"></param>
        public StudentController(IStudentApplication Student)
        {
            this._Student = Student;
        }
        
        /// <summary>
        /// Obtener todos los studiantes registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Student.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Student.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Obtener los cursos por bloque a los cuales se encuentra inscrito el alumno
        /// </summary>
        /// <param name="blockId">Identificador único del bloque, en caso de ser cero no realiza el filtrado del bloque</param>
        /// <returns>Listado de cursos</returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(Response))]
        [Route("courses")]
        public async Task<IHttpActionResult> GetCourses(int blockId)
        {
            try
            {
                var result = await _Student.GetCourseByStudentAndBlock(this.UserId, blockId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
            
        }

        /// <summary>
        /// Agrega un nuevo registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]StudentSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _Student.AddStudent(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Actualizar los datos de uyn registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] StudentUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _Student.UpdateStudent(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Obtener los datos de un grupo 
        /// </summary>
        /// <param name="studentGroupId"></param>
        /// <param name="examScheduleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("getbygroup")]
        public async Task<IHttpActionResult> GetStudentsByGroup(int studentGroupId, int examScheduleId)
        {
            var result = await _Student.GetStudentsByGroup(studentGroupId, examScheduleId);
            return Ok(result);
        }

        /// <summary>
        /// Enviar invitación al alumno para incluirlo al curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("sendinvitation")]
        public async Task<IHttpActionResult> SendInvitation(StudentSendInvitationRequest request)
        {            
            request.homeUrl  = this.SystemId == (int)EnumSystems.SALI ? ConfigurationManager.AppSettings["SALI.APP.HOME"] : ConfigurationManager.AppSettings["RA.APP.HOME"];
            request.SystemId = this.SystemId;
            var result       = await _Student.SendInvitation(request);

            return Ok(result);
        }

        /// <summary>
        /// Enviar invitación al alumno para incluirlo al curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("addinvitation")]
        public async Task<IHttpActionResult> AddInvitation(StudentSendInvitationRequest request)
        {
            request.homeUrl = this.SystemId == (int)EnumSystems.SALI ? ConfigurationManager.AppSettings["SALI.APP.HOME"] : ConfigurationManager.AppSettings["RA.APP.HOME"];
            request.SystemId = this.SystemId;
            var idUser = UserId;
            // var invitation = await _Student.AddInvitation(request, idUser);
            //  var codigo = invitation.Data.Codigo;
            var result = await _Student.AddInvitation(request, idUser);

            return Ok(result);
        }

        /// <summary>
        /// Confirmación de la invitación al curso
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("confirminvitation")]
        public async Task<IHttpActionResult> ConfirmInvitation(string codigo)
        {
            var idUser = UserId;
            var result = await _Student.ConfirmInvitation(codigo);
            return Ok(result);           
        }
    }

    /// <summary>
    /// Obtiene el excel con la calificación de todos los alumnos del curso
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [Authorize]
    [Route("Excel")]
    public async Task<HttpResponseMessage> GetAllByGroupExcel(int studentGroupId, string turn, string schoolCycle)
    {
        FinalScoreFileRequirementsDTO finalScoreFileRequirementsDTO = new FinalScoreFileRequirementsDTO()
        {
            turn = turn,
            studentGroupId = studentGroupId,
            schoolCycle = schoolCycle
        };


        HttpResponseMessage response = Request.CreateResponse();
        try
        {
            if (finalScoreFileRequirementsDTO.studentGroupId == 0)

                throw new Exception("");

            EnumRol enumRol = this.Rol;

            if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA))

                throw new Exception("");

            var result = await _StudentScore.GetAllByGroupDetailsExcel(finalScoreFileRequirementsDTO);

            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(result.Data);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octec-stream");
            response.Content.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");

        }
        catch (Exception ex)
        {
            Request.CreateErrorResponse(HttpStatusCode.OK, "Solicitud incorrecta");
        }
        return response;
    }

}
