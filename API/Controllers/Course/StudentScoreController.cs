﻿using Project.Application.Courses;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller para administrar a las calificaciones de los estudiantes
    /// </summary>
    [RoutePrefix("api/scores")]
    public class StudentScoreController : BaseController
    {
        private readonly IStudentScoreApplication _StudentScore;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="StudentScore"></param>
        public StudentScoreController(IStudentScoreApplication StudentScore)
        {
            this._StudentScore = StudentScore;
        }


        /// <summary>
        /// Obtener todos los studiantes registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("group")]
        public async Task<IHttpActionResult> GetAllByGroup(int studentGroupId)
        {
            if (studentGroupId == 0)
                return BadRequest();

            EnumRol enumRol = this.Rol;

            if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA))
                throw new Exception("");

            var result = await _StudentScore.GetAllByGroup(studentGroupId);
            return Ok(result);
        }

        /// <summary>
        /// Obtener todos los studiantes registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("partial/student")]
        public async Task<IHttpActionResult> GetByPartialId(int studentGroupId, int Partialid, int StudentId)
        {
            var result = await _StudentScore.GetByPartialId(studentGroupId, Partialid, StudentId);
            return Ok(result);
        }

        /// <summary>
        /// Obtener todos los studiantes registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("student")]
        public async Task<IHttpActionResult> GetAllPartials(int studentGroupId)
        {
            var UserId = this.UserId;
            var result = await _StudentScore.GetAllPartials(studentGroupId, UserId);
            return Ok(result);
        }

        /// <summary>
        /// Guarda los datos de calificación final o parcial de un alumno 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody] StudentScoreSaveRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            EnumRol enumRol = this.Rol;

            if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA))
                throw new Exception("Authorize");

            var result = await _StudentScore.SaveStudentScore(request);
            return Ok(result);
        }

        /// <summary>
        /// Actualizar los datos de uyn registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] StudentScoreUpdateRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            EnumRol enumRol = this.Rol;

            if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA))
                throw new Exception("Authorize");
            
            var result = await _StudentScore.UpdateStudentScore(request);
            return Ok(result);
        }

        /// <summary>
        /// Obtener todos los studiantes registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("students/group")]
        public async Task<IHttpActionResult> GetStudentsScoresByGroup(int studentGroupId, int studentId)
        {

            if (studentGroupId == 0)
                return BadRequest();

            EnumRol enumRol = this.Rol;

            if (enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student)
                studentId = this.UserId;

            if (studentId == 0)
                throw new Exception("");

            if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA
                || enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student))
                throw new Exception("");
            var result = await _StudentScore.GetStudentsScoresByGroup(studentGroupId, studentId);
            return Ok(result);
        }
    }
}