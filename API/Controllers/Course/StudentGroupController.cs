﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/studentgroups")]
    public class StudentGroupController : BaseController
    {
        private readonly IStudentGroupApplication _StudentGroup;

        public StudentGroupController(IStudentGroupApplication StudentGroup)
        {
            this._StudentGroup = StudentGroup;
        }

        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _StudentGroup.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _StudentGroup.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]StudentGroupSaveRequest request)
        {
            var idUser = UserId;
            int systemId = SystemId;



            if (request != null )
            {
                //var result = await _StudentGroup.AddStudentGroup(request);
                var result = await _StudentGroup.AddStudentGroupSystem(request,systemId);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] StudentGroupUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _StudentGroup.UpdateStudentGroup(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Eliminar los datos de un curso de manera permanente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        //[Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            int  systemId = this.SystemId;

            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _StudentGroup.DeleteStudentGroupSystem(id, systemId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        [HttpGet]
        [Authorize]
        [Route("{courseId}/getbycourse")]
        public async Task<IHttpActionResult> GetGroupsByCourse(int courseId)
        {
            var result = await _StudentGroup.GetGroupsByCourse(courseId);
            return Ok(result);
        }
                
    }
}
