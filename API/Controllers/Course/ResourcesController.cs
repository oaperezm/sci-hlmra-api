﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    [RoutePrefix("api/resources")]
    public class ResourcesController : BaseController
    {
        private readonly IVCoursesApplication _vCoursesApplication;

        public ResourcesController(IVCoursesApplication vCoursesApplication)
        {
            this._vCoursesApplication = vCoursesApplication;
        }

        /// <summary>
        /// Obtener todos los cursos por identificador de maestro paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAll([FromUri] PaginationRequestById request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (request.Id == 0)
                    request.Id = this.UserId;


                var result = await _vCoursesApplication.GetAll(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

    }
}
