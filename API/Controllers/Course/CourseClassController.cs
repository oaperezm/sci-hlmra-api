﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    /// <summary>
    /// Controlador para administrar clases
    /// </summary>
    [RoutePrefix("api/courseClasses")]
    public class CourseClassController : BaseController
    {
        private readonly ICourseClassApplication _courseClass;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="courseClass"></param>
        public CourseClassController(ICourseClassApplication courseClass)
        {
            this._courseClass = courseClass;
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _courseClass.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }
        }

        /// <summary>
        /// API que permite obtener los datos de un curso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var result = await _courseClass.GetById(id);
                return Ok(result);
            }              
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }           
        }


        /// <summary>
        /// API que permite registrar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]CourseClassSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);                             

                var result = await _courseClass.Add(request);

                if( result.Success )
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite actualizar los datos de nu curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]CourseClassUpdateRequest request)
        {
            try
            {               
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseClass.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.OK, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{courseSectionId}/getBySection")]
        public async Task<IHttpActionResult> GetBySectionId(int courseSectionId)
        {
            try
            {
                var result = await _courseClass.GetBySectionId(courseSectionId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }

        /// <summary>
        /// Permite eliminar el registro de una sección de un curso
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámen</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Es requerido el identificador de la clase del curso");

                var result = await _courseClass.Delete(id);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Asignar archivos a la clase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id}/files")]
        public async Task<IHttpActionResult> AddFiles(CourseClassFilesRequest request) {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _courseClass.AddFiles(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Asignar archivos a la clase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id}/files")]
        public async Task<IHttpActionResult> removeFiles(CourseClassFilesRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _courseClass.RemoveFiles(request);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }

}
