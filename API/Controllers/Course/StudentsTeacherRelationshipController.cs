﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    [RoutePrefix("api/relationship")]
    public class StudentsTeacherRelationshipController : BaseController
    {
        private readonly IVStudentsApplication _vStudentsAplication;

        public StudentsTeacherRelationshipController(IVStudentsApplication vStudentsAplication)
        {
            _vStudentsAplication = vStudentsAplication;
        }

        
        /// <summary>
        /// Obtener estudiantes por el identificador del profesor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                if (id == 0)
                    id = this.UserId;

                var result = await _vStudentsAplication.GetAllByIdTeacher(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener todos los estudiantes por identificador de maestro paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRelationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (request.Id == 0)
                    request.Id = this.UserId;

                var result = await _vStudentsAplication.GetAll(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }


    }
}
