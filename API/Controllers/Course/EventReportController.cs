﻿using Project.Application.Core;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller encargado de administrar los eventos de reportes
    /// </summary>
    [RoutePrefix("api/EventReport")]
    public class EventReportController : BaseController
    {
        private readonly IEventReportApplication _eventReport;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="eventReport"></param>
        public EventReportController(IEventReportApplication eventReport)
        {
            this._eventReport = eventReport;
        }
        /// <summary>
        /// API que permite obtener todos los eventos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _eventReport.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }
         /// <summary>
        /// API que permite registrar un nuevo evento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]EventsReportUpdateRequest request)
        {
            try
            {
                request.UserId = this.UserId;
                //var userId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _eventReport.Add(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
        /// <summary>
        /// API que permite actualiza un nuevo evento de curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]EventsReportUpdateRequest request)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _eventReport.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }
        /// <summary>
        /// API que permite obtener todos los cursos eventos por id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("eventReport")]
        public async Task<IHttpActionResult> Getevent(int Id)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _eventReport.GetById(Id);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite obtener todos los eventos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("Report")]
        public async Task<IHttpActionResult> GetReport([FromUri]  ReportRequest request)
        //public async Task<IHttpActionResult> GetReport([FromUri]  ReportRequest request)
        {
            try
            {
                request.userid = this.UserId;
                var result = await _eventReport.GetReport(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }
    }
}
