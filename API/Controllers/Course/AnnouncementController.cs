﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controlador para administrar avisos
    /// </summary>
    [RoutePrefix("api/announcements")]
    public class AnnouncementController : BaseController
    {
        private readonly IAnnouncementApplication _announcement;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="announcementApplication"></param>
        public AnnouncementController(IAnnouncementApplication announcementApplication) {
            this._announcement = announcementApplication;
        }


        #region Métodos Get


        /// <summary>
        /// Obtener todos los avisos de un usuario paginados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllPagination([FromUri] PaginationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _announcement.GetAll(request.CurrentPage, request.PageSize, request.Filter, this.UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtener los datos de un glosario por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var userId = this.UserId;

                if (userId == 0)
                    return BadRequest();

                var result = await _announcement.GetById(userId, id);


                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }
        
        #endregion

        /// <summary>
        /// Agregar un nuevo aviso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]AnnouncementSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                request.UserId = this.UserId;

                var result = await _announcement.Add(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar los datos de un aviso existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]AnnouncementUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _announcement.Update(request);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// Eliminar un aviso existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest();

                var result = await _announcement.Delete(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {

                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
        
    }
}
