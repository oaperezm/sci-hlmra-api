﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controlador administrador de mensajes
    /// </summary>
    [RoutePrefix("api/messages")]
    public class MessageController : BaseController
    {
        private readonly IMessagesApplication _messages;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="messages"></param>
        public MessageController(IMessagesApplication messages) {
            _messages = messages;
        }

        /// <summary>
        /// Permite enviar un nuevo mensaje 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]SendMessageRequest request)
        {           
            try
            {
                var senderUserId = UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                
                if (senderUserId == 0)
                    return BadRequest("Es requerido el identificador del usuario que envia el mensaje");

                var result = await _messages.SendMessage(senderUserId, request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener las conversaciones de un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(int id) {

            try
            {
                var senderUserId = UserId;                                

                if (senderUserId == 0)
                    return BadRequest("Es requerido el identificador del usuario que envia el mensaje");

                var result = await _messages.GetByChatGroup(id, senderUserId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener todas las conversaciones del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("conversations")]
        public async Task<IHttpActionResult> GetAllConversations()
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest("Es requerido el identificador del usuario que envia el mensaje");

                var result = await _messages.GetAllConversations(this.UserId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener la lista de contactos del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("contacts")]
        public async Task<IHttpActionResult> GetContacts() {

            try
            {
                if (this.UserId == 0)
                    return BadRequest("Es requerido el identificador del usuario que envia el mensaje");

                var result = await _messages.GetContact(this.UserId, this.RolId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}