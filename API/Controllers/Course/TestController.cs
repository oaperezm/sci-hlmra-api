﻿using Project.Application.Courses;
using Project.Entities.Enums;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Administración de examenes aplicados a los alumnos
    /// </summary>
    [RoutePrefix("api/tests")]
    public class TestController : BaseController
    {
        private readonly ITestApplication _testApplication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="testApplication"></param>
        public TestController(ITestApplication testApplication)
        {
            _testApplication = testApplication;
        }

        /// <summary>
        ///  API que permite guardar los datos de un exámen contestado por el alumno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]TestSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                EnumRol enumRol = this.Rol;

                if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA
                    || enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student))
                    throw new Exception("");

                request.UserId = this.UserId;
                var result = await _testApplication.Add(request, enumRol);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtener la respuesta del alumno 
        /// </summary>
        /// <param name="id">identificador del examen </param>
        /// <param name="studentId">identificador del estudiante</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/student")]
        public async Task<IHttpActionResult> GetStudentAnswers(int id, int studentId)
        {

            try
            {
                if (id == 0)
                    return BadRequest();

                EnumRol enumRol = this.Rol;

                if (enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student)
                    studentId = this.UserId;

                if (studentId == 0)
                    throw new Exception("");

                if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA
                    || enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student))
                    throw new Exception("");


                var result = await _testApplication.GetByUserAndExamSchedule(id, studentId, enumRol);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("SaveStudentAnswer")]
        public async Task<IHttpActionResult> SaveStudentAnswer(TestAnswerQuestionRequest request)
        {

            try
            {
                EnumRol enumRol = this.Rol;
                if (enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student)
                    request.UserId = this.UserId;

                if (!(enumRol == EnumRol.Teacher || enumRol == EnumRol.TeacherRA
                    || enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student))
                    throw new Exception("");


                var result = await _testApplication.SaveAnswerQuestion(request, enumRol);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

    }
}
