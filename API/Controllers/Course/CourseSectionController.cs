﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    [RoutePrefix("api/coursesections")]
    public class CourseSectionController : BaseController
    {
        private readonly ICourseSectionApplication _courseSection;

        public CourseSectionController(ICourseSectionApplication courseSection)
        {
            this._courseSection = courseSection;
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _courseSection.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }
        }

        /// <summary>
        /// API que permite obtener los datos de un curso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id}/detail")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var result = await _courseSection.GetById(id);
                return Ok(result);
            }              
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");                
            }           
        }


        /// <summary>
        /// API que permite registrar un nuevo curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]CourseSectionSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);                             

                var result = await _courseSection.Add(request);

                if( result.Success )
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite actualizar los datos de nu curso
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]CourseSectionUpdateRequest request)
        {
            try
            {               
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _courseSection.Update(request);

                if (result.Success)
                    return Content(HttpStatusCode.OK, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// API que permite obtener todas las secciones de un curso
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{courseId}/getByCourse")]
        public async Task<IHttpActionResult> GetByCourseId(int courseId)
        {
            try
            {
                var result = await _courseSection.GetByCourseId(courseId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }

        /// <summary>
        /// Permite eliminar el registro de una sección de un curso
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámen</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Es requerido el identificador de la sección del curso");

                var result = await _courseSection.Delete(id);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

    }

}
