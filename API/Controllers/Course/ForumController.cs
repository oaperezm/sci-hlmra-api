﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controller para administrar foro
    /// </summary>
    [RoutePrefix("api/forums")]
    public class ForumController : BaseController
    {
        private readonly IForumApplication _forum;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="forum"></param>
        public ForumController(IForumApplication forum) {
            _forum = forum;
        }

        #region THREAD

        /// <summary>
        /// Obtener un hilo por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("thread")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest("Usuario no valido");

                var result = await _forum.GeThreadById(this.UserId, id, this.RolId);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// Obtener todos los hilos del usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("thread")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                if (this.UserId == 0)
                    return BadRequest("Usuario no valido");

                var result = await _forum.GetByUser(this.UserId, this.RolId, this.SystemId);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Agregar un nuevo hilo 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("thread")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> SaveThread(ThreadSaveRequest request) {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("El usuario no es valido");

                var result = await _forum.AddThread(this.UserId, request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {               
                return BadRequest("Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// Actualizar los datos de un hilo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("thread")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdateThread(ThreadUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("El usuario no es valido");

                var result = await _forum.UpdateThread(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }

        #endregion

        #region POST

        /// <summary>
        /// Agregar un nuevo post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("post")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> SavePost(PostSaveRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("El usuario no es valido");

                var result = await _forum.AddPost(this.UserId, request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }
        
        /// <summary>
        /// Actualizar los datos d eun post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("post")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdatePost(PostUpdateRequest request)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("El usuario no es valido");

                var result = await _forum.UpdatePost(request);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar el post como mejor respuesta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("post/{id}/status")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdateStatusPost(PostUpdateStatusRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                
                var result = await _forum.UpdateStatus(request);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Eliminar un post existente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("post")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> DeletePost(int id) {

            try
            {
                if (id == 0)
                    return BadRequest("Identificador del post no valido");

                if (this.UserId == 0)
                    return BadRequest("El usuario no es valido");

                var result = await _forum.DeletePost(this.UserId, id);
                return Ok(result);
            }
            catch (Exception)
            {

                return BadRequest("Solicitud incorrecta");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("post/{id}/vote")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdateVotePost(PostUpdateVoteRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("Usuario no valido");
                
                var result = await _forum.UpdateVote(request.Id,this.UserId,  request.Vote);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Solicitud incorrecta");
            }
        }

        #endregion
    }
}