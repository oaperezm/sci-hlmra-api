﻿using Project.Application.Courses;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers.Course
{
    /// <summary>
    /// Controlador para administrar asistencias
    /// </summary>
    [RoutePrefix("api/attendances")]
    public class AttendanceController : BaseController
    {
        private readonly IAttendanceApplication _attendance;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="attendance"></param>
        public AttendanceController(IAttendanceApplication attendance) {
            _attendance = attendance;
        }
              
        /// <summary>
        /// Guardar asistencia de los alumnos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]AttendanceSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _attendance.SaveAttendances(request);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Obtener la asistencia de los alumnos por rango de fecha y grupo
        /// </summary>
        /// <param name="init">Fecha de inicio de asistencia </param>
        /// <param name="end">Fecha de termino de la asistencia </param>
        /// <param name="studentGroupId">Identificador del grupo</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get(DateTime? init, DateTime? end, int studentGroupId)
        {
            try
            {
                if (init == null || end == null)
                    return BadRequest("La fecha de inicio y termino es obligatorio");

                if(studentGroupId == 0)
                    return BadRequest("El identificador del grupo es obligatorio");

                var result = await _attendance.GetAttendances(init.Value, end.Value, studentGroupId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}