﻿using Project.Application.Courses;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers.Course
{     /// <summary>
      /// Controller encargado de administrar los cursos planeados
      /// </summary>
    [RoutePrefix("api/StatesVideoConferece")]
    public class StatesVideoConfereceController : BaseController
    {
        private readonly IStatesVideoConferenceApplication _statesVideoConferece;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="statesVideoConferece"></param>
        public StatesVideoConfereceController(IStatesVideoConferenceApplication statesVideoConferece)
        {
            this._statesVideoConferece = statesVideoConferece;
        }
        /// <summary>
        /// API que permite registrar un nuevo estado de videoconference
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        ////[Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]StatesVideoConferenceSaveRequest request)
        {
            try
            {
                //request.UserId = this.UserId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _statesVideoConferece.Add(request);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// Obtener todos los estados de videoconference
        /// </summary>
        /// <returns></returns>
        [HttpGet]
       
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _statesVideoConferece.GetAll();
            return Ok(result);
        }

    }
}
