﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    [RoutePrefix("api/answers")]
    public class AnswerController : BaseController
    {
        private readonly IAnswerApplication _Answer;

        public AnswerController(IAnswerApplication Answer)
        {
            this._Answer = Answer;
        }

        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await _Answer.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Authorize]
        [Route("{id}/obtener")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var idUser = UserId;

            var result = await _Answer.GetById(id);
            return Ok(result);
        }
             

        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]AnswerSaveRequest request)
        {
            var idUser = UserId;

            if (request != null)
            {
                var result = await _Answer.AddAnswer(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody] AnswerUpdateRequest request)
        {
            var idUser = UserId;

            if (request.Id != 0)
            {
                var result = await _Answer.UpdateAnswer(request);
                return Ok(result);
            }

            return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
        }

        /// <summary>
        /// Elimina el registro de una respuesta d emanera permanente
        /// </summary>
        /// <param name="id">Identificador de la respuesta</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    return Content(HttpStatusCode.BadRequest, "El identificador de la pregunta es requerido");

                var result = await _Answer.DeleteAnswer(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }
    }
}
