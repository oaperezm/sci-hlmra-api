﻿using Project.Application.Courses;
using Project.Entities;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    /// <summary>
    /// Controlador utilizado para la programación de exámenes
    /// </summary>
    [RoutePrefix("api/examchedules")]
    public class ExamScheduleController : BaseController
    {
        private readonly IExamScheduleApplication _examSchedule;

        /// <summary>
        /// Inicializador del objeto
        /// </summary>
        /// <param name="examSchedule"></param>
        public ExamScheduleController(IExamScheduleApplication examSchedule)
        {
            this._examSchedule = examSchedule;
        }

        /// <summary>
        /// Obtener las programaciones de examen por profesor
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByTeacher()
        {
            try
            {
                var result = await _examSchedule.GetAllByUserId(this.UserId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtener las programaciones de examen por profesor
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [Authorize]
        [Route("group")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByGroup(int groupId)
        {
            try
            {
                if (groupId == 0)
                    return BadRequest("Es requerido el identificador del grupo");

                var result = await _examSchedule.GetAllByStudentGroupId(groupId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }


        /// <summary>
        /// Obtener las programaciones de examenes por grupo y calificaciones del alumno
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("student")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByGroupAndUser(int groupId)
        {
            try
            {
                if (groupId == 0)
                    return BadRequest("Es requerido el identificador del grupo");

                var result = await _examSchedule.GetAllByStudentGroupIdAndUser(groupId, this.UserId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Agregar una nueva programación de exámenes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]ExamScheduleSaveRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (this.UserId == 0)
                    return BadRequest("Es requerido el identificador del progesor");

                var result = await _examSchedule.Add(request, this.UserId, this.SystemId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Actualizar los datos de una programación de exámenes existente
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]ExamScheduleUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _examSchedule.Update(request, this.UserId, this.SystemId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("updateStatus")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpdateStatus([FromBody]ExamScheduleStatusUpdateRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _examSchedule.UpdateExamScheduleType(request.Id, request.ExamScheduleTypeId);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// Permite eliminar el registro de una programación de exámenes
        /// </summary>
        /// <param name="id">Identificador principal de la programación de exámen</param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id == 0)
                    return BadRequest("Es requerido el identificador de la programación de exámen");

                var result = await _examSchedule.Delete(id);
                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

    }
}
