﻿using Project.Application.Core;
using Project.Entities;
using Project.Entities.Enums;
using Project.Entities.Request;
using Project.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    [RoutePrefix("api/teacherexams")]
    public class TeacherExamController : BaseController
    {

        private readonly ITeacherExamApplication _teacherExam;

        /// <summary>
        /// Constructor del controller
        /// </summary>
        /// <param name="teacherExam"></param>
        public TeacherExamController(ITeacherExamApplication teacherExam)
        {
            this._teacherExam = teacherExam;
        }

        /// <summary>
        /// Obtener el detalle de un examen
        /// </summary>
        /// <param name="id">Identificador principal del examen</param>
        /// <param name="image"> Determina si se el resultado incluira las imagenes en base64</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Get(int id, bool image = false)
        {
            try
            {
                var result = await this._teacherExam.GetById(id, image);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }




        /// <summary>
        /// Obtener el detalle de un examen programado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="examScheduleId"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("GetBySchedule")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetBySchedule(int id, int examScheduleId, bool image = false)
        {
            try
            {
                int studentId = 0;
                EnumRol enumRol = this.Rol;
                if (enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student)
                    studentId = this.UserId;

                if (studentId == 0)
                    throw new Exception("");
                var result = await this._teacherExam.GetExamSchedule(id, examScheduleId, studentId, image);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Iniciar un examen programado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="examScheduleId"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("StartExamSchedule")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> StartExamSchedule(int id, int examScheduleId, bool image = false)
        {
            try
            {
                int studentId = 0;
                EnumRol enumRol = this.Rol;
                if (enumRol == EnumRol.StudentRA || enumRol == EnumRol.Student)
                    studentId = this.UserId;

                if (studentId == 0)
                    throw new Exception("");

                var result = await this._teacherExam.StartExamSchedule(id, examScheduleId, studentId, image);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtener todos los examenes registrados por un usuario
        /// </summary>
        /// <param name="userdId">Identificador unico del usuario</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByUser(int userdId)
        {

            try
            {
                if (userdId == 0)
                    userdId = this.UserId;

                var result = await this._teacherExam.GetByUserId(userdId);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtiene el listado de examenes sin preguntas ni respuestas
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userdId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("all")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByUserShortInfo([FromUri] PaginationRequest request)
        {
            try
            {
                var result = await this._teacherExam.GetAllByUserPagination(request.CurrentPage, request.PageSize, request.Filter, request.Type, this.UserId);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// Obtiene el listado de examenes sin preguntas ni respuestas
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userdId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("teacher-short")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> GetByUserShortInfo(int userdId)
        {

            try
            {
                if (userdId == 0)
                    userdId = this.UserId;

                var result = await this._teacherExam.GetByUserIdShortInfo(userdId);
                return Content(HttpStatusCode.Created, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// API para agregar un nuevo exámen del profesor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Post([FromBody]TeacherExamSaveRequest request)
        {
            try
            {
                if (this.UserId == 0)
                    throw new Exception("Debe seleccionar un usuario valido para continuar");

                request.UserId = this.UserId;
                var result = await this._teacherExam.Add(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// API para agregar un nuevo exámen del profesor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Put([FromBody]TeacherExamUpdateRequest request)
        {
            try
            {
                if (this.UserId == 0)
                    throw new Exception("Debe seleccionar un uusuario valido para continuar");

                request.UserId = this.UserId;
                var result = await this._teacherExam.Update(request);

                return Content(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// API para eliminar los datos de un examen de manera permanentemente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                if (id == 0)
                    throw new Exception("Debe seleccionar un examen");

                var result = await this._teacherExam.Delete(id);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

        /// <summary>
        /// API para actualizar la ponderación del examen
        /// </summary>
        /// <param name="teacherExam"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("weighting")]
        [ResponseType(typeof(Response))]
        public async Task<IHttpActionResult> UpateExamWeighting(TeacherExamUpdateRequest teacherExam)
        {
            try
            {
                if (teacherExam.Id == 0)
                    throw new Exception("Debe seleccionar un examen");

                var result = await this._teacherExam.UpdateWeighting(teacherExam);
                return Content(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }
        }

    }
}
