﻿using Project.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace API.Controllers
{
    /// <summary>
    /// Controlador base 
    /// </summary>
    public class BaseController : ApiController
    {
        /// <summary>
        /// Identificador principal del usuario
        /// </summary>
        protected int UserId
        {
            get
            {
                try
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var claimId = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid);
                    var claimResult = int.TryParse(claimId.Value, out int userId);

                    return claimResult ? userId : 0;
                }
                catch (Exception)
                {

                    return 0;
                }                
            }
        }

        /// <summary>
        /// Identificador del rol asignado al alumno
        /// </summary>
        protected int RolId
        {
            get
            {
                try
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var claimId        = claimsIdentity.Claims.FirstOrDefault(x => x.Type == "rol_id");
                    var claimResult    = int.TryParse(claimId.Value, out int rolId);

                    return claimResult ? rolId : 0;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }
        
        /// <summary>
        /// Identificador del sistema del usuario que se esta logueando
        /// </summary>
        protected int SystemId
        {
            get
            {
                try
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var claimId = claimsIdentity.Claims.FirstOrDefault(x => x.Type == "system_id");
                    var claimResult = int.TryParse(claimId.Value, out int systemId);

                    return claimResult ? systemId : 0;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Enumerador del ROL de sistema del usuario que se esta logueando
        /// </summary>
        protected EnumRol Rol
        {
            get
            {
                try
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var claimId = claimsIdentity.Claims.FirstOrDefault(x => x.Type == "rol_id");
                    var claimResult = int.TryParse(claimId.Value, out int rolId);
                    foreach (EnumRol rol in Enum.GetValues(typeof(EnumRol)))
                        if ((int)rol == rolId)
                            return rol;
                    return EnumRol.NotAuthorized;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }
    }
}