﻿using Project.Application.Core;
using Project.Application.Management;
using Project.Entities;
using Project.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{    /// <summary>
     /// Controller encargado de administrar las video conferencias
     /// </summary>
    [RoutePrefix("api/VideoConference")]
    public class VideoConferenceController : BaseController
    {
        private readonly IVideoConferenceApplication _videoconference;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="videoConference"></param>
        public VideoConferenceController(IVideoConferenceApplication videoConference)
        {
            this._videoconference = videoConference;
        }
        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var userId = this.UserId;
                int systemId = SystemId;
                var result = await _videoconference.GetAll(systemId, UserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }
        /// <summary>
        /// API que permite registrar una  nueva video conference
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]VideoConferenceSaveRequest request)
        {
            try
            {
                int systemId = SystemId;
                var userId = this.UserId;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _videoconference.Add(request, systemId, UserId);

                if (result.Success)
                    return Content(HttpStatusCode.Created, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }


        /// <summary>
        /// Eliminar un registro de video conference
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(int id)
        {

            try
            {
                int systemId = SystemId;
                if (id == 0)
                    return BadRequest();

                var result = await _videoconference.Delete(id, systemId);
                return Ok(result);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, "Solicitud incorrecta");
            }

        }

        /// <summary>
        /// API que permite actualizar registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Put([FromBody]VideoConferenceUpdateRequest request)
        {
            try
            {
                int systemId = SystemId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _videoconference.Update(request,systemId);

                if (result.Success)
                    return Content(HttpStatusCode.OK, result);
                else
                    return Content(HttpStatusCode.InternalServerError, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite obtener todos los contenidos registrados
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("GetNodo")]
        public async Task<IHttpActionResult> GetNodo()
        {
            try
            {
                var result = await _videoconference.GetNodo();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Error al obtener los datos: { ex.Message }");
            }
        }

        /// <summary>
        /// API que permite registrar una  nueva video conference
        /// </summary>
        /// <param name="request"></param>
        /// <param name="invited"></param>
        /// <param name="externalguest"></param>
        /// <returns></returns>
        //[HttpPost]
        //[Route("PostVideoConference")]
        //////public async Task<IHttpActionResult> PostVideoConference([FromBody]VideoConferenceSaveRequest request, [FromBody]InvitedVideoConferenceSaveRequest invited, [FromBody]InvitedVideoConferenceSaveRequest externalguest)
        //public async Task<IHttpActionResult> PostVideoConference([FromBody]VideoConferenceSaveRequest request)}
        //{
        //    try
        //    {


        //        if (!ModelState.IsValid)
        //            return BadRequest(ModelState);

        //        var result = await _videoconference.Add(request, invited,externalguest);

        //        if (result.Success)
        //            return Content(HttpStatusCode.Created, result);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, result);

        //    }
        //    catch (Exception ex)
        //    {
        //        return Content(HttpStatusCode.BadRequest, $"Solicitud incorrecta: { ex.Message }");
        //    }
        //}

    }
}
