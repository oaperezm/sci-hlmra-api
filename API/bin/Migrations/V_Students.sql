IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[V_Students]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[V_Students]
AS
SELECT
	s.Id Id, 
	  c.UserId TeacherId, 
	  uA.FullName Teacher,
      s.Id StudentId,
	  u.fullname Student,
	  u.email StudentEmail, 
	  u.UrlImage StudentUrlImage, 
	  u.SystemId StudentSystemId,
	  sy.Description StudentSystem,
      s.UserId StudentUserId,
      s.StudentStatusId,	 
	  ss.Description  StudentStatusDescription,
	  sg.Id  StudentGroupId, 
	  sg.Description StudentGroupDescription,
	  c.Id CourseId, 
	  c.Name CourseName, 
	  c.Description CourseDescription, 
	  c.Subject CourseSubject	    
  FROM Students s
  INNER JOIN Users u
  ON s.UserId = u.Id
  INNER JOIN StudentGroups sg
  ON s.StudentGroupId = sg.Id
  INNER JOIN Courses c
  ON sg.CourseId = c.Id
  INNER JOIN Users uA
  ON uA.Id = c.UserId
  INNER JOIN StudentStatuses ss
  ON s.StudentStatusId = ss.Id
  INNER JOIN Systems sy
  ON u.SystemId = sy.Id
'




