﻿using Autofac;
using Autofac.Integration.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Project.Application.Management;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace API.Auth
{
    /// <summary>
    /// Implementación del refreshtoken
    /// </summary>
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();
        private IUserApplication _user;

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var guid                 = Guid.NewGuid().ToString();
            var parameters           = await context.Request.ReadFormAsync();
            var clientid             = "";
            var autofacLifetimeScope = OwinContextExtensions.GetAutofacLifetimeScope(context.OwinContext);
           

            _user = autofacLifetimeScope.Resolve<IUserApplication>();

            var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
            {
                IssuedUtc  = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddYears(1)
            };

            var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);
            var claimId            = context.Ticket.Identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid);
            var claimResult        = int.TryParse(claimId.Value, out int userId);            
            var protectedTicket    = context.SerializeTicket();

            if (parameters != null) {
                clientid = (parameters.Get("client_Id") == "") ? string.Empty : parameters.Get("client_Id");
            }

            context.Ticket.Properties.IssuedUtc  = refreshTokenProperties.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = refreshTokenProperties.ExpiresUtc;

            _refreshTokens.TryAdd(guid, refreshTokenTicket);

            if ( await _user.SaveRefreshToken(userId, guid, clientid, protectedTicket) )
                context.SetToken(guid);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            AuthenticationTicket ticket;
            if (_refreshTokens.TryRemove(context.Token, out ticket))
            {
                context.SetTicket(ticket);
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {            
            string hashedTokenId     = context.Token;
            var autofacLifetimeScope = OwinContextExtensions.GetAutofacLifetimeScope(context.OwinContext);
            _user                    = autofacLifetimeScope.Resolve<IUserApplication>();

            var user = await _user.GetByRegreshToken(hashedTokenId);

            if (user != null) {
                context.DeserializeTicket(user.ProtectedTicket);
            }
        }
    }
}