﻿using Autofac;
using Autofac.Integration.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Project.Application.Management;
using Project.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace API.Auth
{
    public class ApplicationOAuthServerProvider : OAuthAuthorizationServerProvider
    {
        private IUserApplication _user;
        private string clientId = string.Empty;
        private string clientSecret = string.Empty;

        public ApplicationOAuthServerProvider() {            
        }
                
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var autofacLifetimeScope = OwinContextExtensions.GetAutofacLifetimeScope(context.OwinContext);
            _user                    = autofacLifetimeScope.Resolve<IUserApplication>();


            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }


            await Task.FromResult(context.Validated());
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var parameters = await context.Request.ReadFormAsync();
                int systemId;
                
                Int32.TryParse(parameters.Get("origen_id"), out systemId);

                if (systemId == 0) {
                    context.SetError("invalid_system", "El sistema de origen es requerido");                  
                    return;
                } 
                
                if (context.UserName == "" || context.Password == "")
                {
                    context.SetError("invalid_user", "Usuario y/0 contraseña son requeridos");
                    context.SetError("validation_user", "Usuario y/o contraseña son obligatorios");
                    return;
                }

                var responseUsuario = await _user.ValidateUser(context.UserName, context.Password, systemId);

                if (!responseUsuario.Success) {
                    context.SetError(responseUsuario.Message);
                    return;
                }

                var user = (User)responseUsuario.Data;

                ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role?.Description));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.FullName));
                identity.AddClaim(new Claim("rol_id", user.RoleId.ToString()));
                identity.AddClaim(new Claim("system_id", systemId.ToString()));

                var clientId = (parameters.Get("client_Id") == "" ) ? string.Empty : parameters.Get("client_Id");

                AuthenticationProperties properties = CreateProperties(user, clientId);
                AuthenticationTicket ticket         = new AuthenticationTicket(identity, properties);

                context.Validated(ticket);
            }
            catch (Exception es)
            {
                throw;
            }

        }

        public static AuthenticationProperties CreateProperties(User user, string clientId)
        {
            IDictionary<string, string> data = new Dictionary<string, string>{
                { "userName", user.Email },
                { "email", user.Email },
                { "fullName", $"{user.FullName}" },
                { "rol", user.Role.Description },
                { "as:client_id",  $"{clientId} - { ConfigurationManager.AppSettings["API.ENV"] }" }
            };

            return new AuthenticationProperties(data);
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if (context.Request.Method == "OPTIONS")
            {
                context.RequestCompleted();
                return Task.FromResult(0);
            }

            return base.MatchEndpoint(context);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

    }
}